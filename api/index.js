const express = require('express')

const router = express.Router()

router.use('/temp', ({ method, originalUrl }, res) =>
  res.json(
    {firstName: 'Mark', lastName: 'Otto', userid: '@mdo'},
    {firstName: 'Jacob', lastName: 'Thornton', userid: '@fat'},
    {firstName: 'Mark', lastName: 'Otto', userid: '@mdo'},
    {firstName: 'Mark', lastName: 'Otto', userid: '@mdo'})
)

// anything not handled needs a 404 - otherwise we fall through to the UI
router.use('/', ({ method, originalUrl }, res) =>
  res.status(404).send(`cannot ${method} ${originalUrl}`)
)

module.exports = router
