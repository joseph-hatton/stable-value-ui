import http from '../../src/actions/http'
import {apiPath} from '../../src/config'
import { browserHistory } from 'react-router'
import {
    loadAllContacts,
    saveContact,
    clearEditContactState,
    loadContact,
    populateCompanyAddress,
    deleteContact,
    selectContact,
    selectContracts,
    selectContractRow,
    queueAssignContracts,
    queueUnassignContracts,
    assignContactToContract,
    unAssignContactFromContract,
    saveBufferedAssignUnassignActions,
    resetAssignUnassign
} from '../../src/contacts/ContactActions'

jest.mock('../../src/actions/http', () => ({
    get: jest.fn().mockReturnValue('get'),
    put: jest.fn().mockReturnValue('put'),
    post: jest.fn().mockReturnValue('post'),
    delete: jest.fn().mockReturnValue('delete')
}))

jest.mock('react-router', () => ({
    browserHistory: {
        push: jest.fn()
    }
}))
const createGetState = (state) => jest.fn().mockImplementation(() => state)

describe('Contacts Actions', () => {
    let dispatch, getState, state
    beforeEach(() => {
        state = {form: {}}
        dispatch = jest.fn()
        getState = jest.fn().mockReturnValue(state)
    })
    it('clearEditContactState ', () => {
        const result = clearEditContactState()
        expect(result.type).toBe('CLEAR_EDIT_CONTACT')
    })

    it('loadContact Action ', () => {
        loadContact(2)(dispatch)
        const result = dispatch.mock.calls[0][0]
        expect(result.type).toBe('LOAD_CONTACT')
        expect(result.payload).toBe('get')
    })

    it('deleteContact Action true  ', () => {
        const param = {contactId: 1, contracts: []}
        deleteContact(param)(dispatch)
        const result = dispatch.mock.calls[0][0]
        expect(result.type).toBe('DELETE_CONTACT')
        expect(result.payload).toBe('delete')
    })

    it('loadAllContacts Action  ', async () => {
        const contacts = [{contactId: 1}, {contactId: 2}, {contactId: 3}]
        http.get.mockReturnValue(Promise.resolve(contacts))
        loadAllContacts()(dispatch, getState)
        const result = dispatch.mock.calls[0][0]
        expect(await result.type).toBe('LOAD_ALL_CONTACTS')
        expect(await result.payload).toBe(contacts)
    })

    it('save existing Contact Action  ', async () => {
        const contact = {contactId: 111, contactName: 'New contact'}
        const AddEditContactForm = {values: {contactId: 123, companyName: 'Test'}}
        state = {form: {AddEditContactForm}, contacts: {selectedContact: contact}}
        http.put.mockReturnValue(Promise.resolve(contact))
        getState.mockReturnValue(state)
        saveContact()(dispatch, getState)
        const result = dispatch.mock.calls[0][0]
        expect(await result.type).toBe('SAVE_CONTACT')
        expect(await result.payload).toBe(contact)
    })

    it('saveContact Action new contact   ', async () => {
        const contact = {contactName: 'New contact'}
        const AddEditContactForm = {values: {companyName: 'Test'}}
        state = {form: {AddEditContactForm}, contacts: {selectedContact: contact}}
        http.post.mockReturnValue(Promise.resolve(contact))
        getState.mockReturnValue(state)
        saveContact()(dispatch, getState)
        const result = dispatch.mock.calls[0][0]
        expect(await result.type).toBe('SAVE_CONTACT')
        expect(await result.payload).toBe(contact)
    })

    it('populateCompanyAddres  ', () => {
        const results = [{name: 'Company Test'}, {name: 'abc'}, {name: 'abc'}]

        state = {companies: {allCompanies: {results}}}
        getState.mockReturnValue(state)
        populateCompanyAddress('Company Test')(dispatch, getState)
        const result = dispatch.mock.calls[0][0]
        expect(result.type).toBe('@@redux-form/CHANGE')
    })
  it('selectContracts should sent an action with the contracts to be selected', () => {
    expect(selectContracts({contracts: 'contracts'})).toEqual({
      type: 'SELECT_CONTRACTS',
      contracts: 'contracts'
    })
  })
  it('selectContact should sent an action with the contact to be selected', () => {
    expect(selectContact('contact')).toEqual({
      type: 'SELECTED_CONTACT',
      contact: 'contact'
    })
  })
  it('selectContractRow should sent an action with the contract row to be selected', () => {
    expect(selectContractRow('contracts')).toEqual({
      type: 'SELECT_CONTRACT_ROW',
      contracts: 'contracts'
    })
  })
  it('queueUnassignContracts should dispatch the current selected contracts', () => {
    state.contacts = {selectedContracts: 'selectedContracts'}
    queueUnassignContracts()(dispatch, getState)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'QUEUE_UNASSIGN_CONTRACTS',
      payload: 'selectedContracts'
    })
  })
  it('queueAssignContracts should dispatch the current selected contracts', () => {
    state.contacts = {selectedContracts: 'selectedContracts'}
    queueAssignContracts()(dispatch, getState)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'QUEUE_ASSIGN_CONTRACTS',
      payload: 'selectedContracts'
    })
  })
  it('assignContactToContract should should hit the API to assign the contact to the contract', () => {
    const contractId = '123'
    const contactId = '321'
    const selectedContact = { receiveInvoice: false, receiveScheduleA: false, receiveStatement: false }
    assignContactToContract(contractId, contactId, selectedContact)
    expect(http.post).toHaveBeenCalledWith(`${apiPath}/contracts/${contractId}/contacts`,
      {contractId: 123, contactId: 321, receiveInvoice: false, receiveStatement: false, receiveScheduleA: false})
  })
  it('unAssignContactFromContract should should hit the API to unassign the contact from the contract', () => {
    const contractId = '123'
    const contactContractId = '321'
    unAssignContactFromContract(contractId, contactContractId)
    expect(http.delete).toHaveBeenCalledWith(`${apiPath}/contracts/${contractId}/contacts/${contactContractId}`, {})
  })
})
