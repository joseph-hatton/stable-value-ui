import React from 'react'
import renderer from 'react-test-renderer'
import mockConnect from '../MockConnect'
import mockComponent from '../MockComponent'
import {shallow} from 'enzyme'
import {connect} from 'react-redux'

import AssignUnassignContracts from '../../src/contacts/AssignUnassignContracts'

jest.mock('react-redux', () => mockConnect())
jest.mock('material-ui/RaisedButton', () => mockComponent('RaisedButton'))
jest.mock('material-ui/Card/Card', () => mockComponent('Card'))
jest.mock('../../src/components/redux-dialog/redux-dialog', () => mockComponent('ReduxDialog'))
jest.mock('../../src/components/redux-dialog', () => ({
  openDialog: 'openDialog',
  closeDialog: 'closeDialog'
}))
jest.mock('material-ui/Toolbar', () => ({
  Toolbar: mockComponent('Toolbar'),
  ToolbarGroup: mockComponent('ToolbarGroup')
}))
jest.mock('../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn().mockReturnValue('CreateGridConfig')
}))
jest.mock('../../src/contacts/ContactActions', () => ({
  selectContractRow: 'selectContractRow',
  queueUnassignContracts: 'queueUnassignContracts',
  resetAssignUnassign: 'resetAssignUnassign',
  saveBufferedAssignUnassignActions: 'saveBufferedAssignUnassignActions'
}))
jest.mock('../../src/contacts/AssignUnassignContractsColumns', () => ({
  AssignUnassignContractsColumns: () => 'AssignUnassignContractsColumns'
}))

describe('AssignUnassignContracts', () => {
  let props
  beforeEach(() => {
    props = {
      selectedContact: {
        firstName: 'firstName',
        lastName: 'lastName',
        contracts: 'contracts'
      },
      openDialog: jest.fn(),
      closeDialog: jest.fn(),
      selectContractRow: 'selectContractRow',
      resetAssignUnassign: 'resetAssignUnassign',
      saveBufferedAssignUnassignActions: 'saveBufferedAssignUnassignActions',
      queueUnassignContracts: 'queueUnassignContracts'
    }
  })
  it('should render correctly', () => {
    expect(renderer.create(<AssignUnassignContracts {...props}/>).toJSON()).toMatchSnapshot()
  })
  it('should render correctly when firstname is not included', () => {
    props.selectedContact.firstName = null
    expect(renderer.create(<AssignUnassignContracts {...props}/>).toJSON()).toMatchSnapshot()
  })
  it('onClick of Cancel should close the dialog', () => {
    shallow(<AssignUnassignContracts {...props}/>).prop('actions')[1].props['onClick']()
    expect(props.closeDialog).toHaveBeenCalledWith('AssignUnassignContracts')
  })
  it('onClick of Assign Button should open the assignContract dialog', () => {
    shallow(<AssignUnassignContracts {...props}/>).find('#AssignUnassignDialog-AssignContractsButton').prop('onClick')()
    expect(props.openDialog).toHaveBeenCalledWith('AssignContracts')
  })
  it('mapStateToProps should map correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][0]({
      contacts: { selectedContact: 'selectedContact' }
    })).toEqual({
      selectedContact: 'selectedContact'
    })
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][1]).toEqual({
      openDialog: 'openDialog',
      closeDialog: 'closeDialog',
      selectContractRow: 'selectContractRow',
      queueUnassignContracts: 'queueUnassignContracts',
      resetAssignUnassign: 'resetAssignUnassign',
      saveBufferedAssignUnassignActions: 'saveBufferedAssignUnassignActions'
    })
  })
})
