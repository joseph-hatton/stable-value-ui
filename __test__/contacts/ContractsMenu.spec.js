import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from '../MockComponent'
import mockConnect from '../MockConnect'
import {browserHistory} from 'react-router'
import {connect} from 'react-redux'

import ContractsMenu, {ContractsMenuLink} from '../../src/contacts/ContractsMenu'

jest.mock('react-redux', () => mockConnect())
jest.mock('material-ui', () => ({
  Popover: mockComponent('Popover'),
  Menu: mockComponent('Menu'),
  MenuItem: mockComponent('MenuItem')
}))
jest.mock('../../src/contacts/ContactActions', () => ({
  selectContracts: 'selectContracts'
}))
jest.mock('../../src/components/redux-dialog/redux-dialog', () => ({
  openDialog: 'openDialog',
  closeDialog: 'closeDialog'
}))
jest.mock('react-router', () => ({
  browserHistory: {
    push: jest.fn()
  }
}))

describe('ContractsMenu', () => {
  let props
  beforeEach(() => {
    props = {
      selectedContracts: [{contractId: 'contractId', contractNumber: 'contractNumber'}],
      open: 'open',
      closeDialog: jest.fn(),
      openDialog: jest.fn()
    }
    browserHistory.push.mockClear()
  })
  it('should render correctly', () => {
    const comp = renderer.create(<ContractsMenu {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('should render correctly with multiple selectedContracts', () => {
    props.selectedContracts.push({contractId: 'contractId2', contractNumber: 'contractNumber2'})
    const comp = renderer.create(<ContractsMenu {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('onRequestClose should close the dialog contractsMenu', () => {
    const comp = shallow(<ContractsMenu {...props} />)
    comp.prop('onRequestClose')()
    expect(props.closeDialog).toHaveBeenCalledWith('contractsMenu')
  })
  it('onClick of list item should close dialog and redirect to the right contract', () => {
    const comp = shallow(<ContractsMenu {...props} />)
    comp.find('#ContractsMenu-MenuItem-0').prop('onClick')()
    expect(props.closeDialog).toHaveBeenCalledWith('contractsMenu')
    expect(browserHistory.push).toHaveBeenCalledWith('/contracts/contractId/specifications')
  })
  it('mapStateToProps should map correctly', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[1][0]({
      dialog: {contractsMenu: {anchorEl: 'anchorEl'}},
      contacts: {selectedContracts: 'selectedContracts'}
    })).toEqual({
      open: true,
      anchorEl: 'anchorEl',
      selectedContracts: 'selectedContracts'
    })
  })
  it('mapStateToProps should map correctly when  is not set', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[1][0]({
      dialog: {},
      contacts: {selectedContracts: 'selectedContracts'}
    })).toEqual({
      open: false,
      anchorEl: null,
      selectedContracts: 'selectedContracts'
    })
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[1][1]).toEqual({
      closeDialog: 'closeDialog',
      openDialog: 'openDialog'
    })
  })
})

describe('ContractsMenuLink', () => {
  let props
  beforeEach(() => {
    props = {
      cell: 'cell',
      row: {contracts: ['test']},
      selectContracts: jest.fn(),
      openDialog: jest.fn()
    }
  })
  it('should render correctly', () => {
    const comp = renderer.create(<ContractsMenuLink {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('on onClick shoud prevent default, select contracts and open dialog', () => {
    const comp = shallow(<ContractsMenuLink {...props} />)
    const e = {nativeEvent: {preventDefault: jest.fn()}, currentTarget: 'currentTarget'}
    comp.prop('onClick')(e)
    expect(e.nativeEvent.preventDefault).toHaveBeenCalled()
    expect(props.selectContracts).toHaveBeenCalledWith({contracts: ['test']})
    expect(props.openDialog).toHaveBeenCalledWith('contractsMenu', {anchorEl: 'currentTarget'})
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[0][1]).toEqual({
      openDialog: 'openDialog',
      selectContracts: 'selectContracts'
    })
  })
})
