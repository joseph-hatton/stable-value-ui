import React from 'react'
import mockComponent from '../MockComponent'
import mockConnect from '..//MockConnect'
import {shallow} from 'enzyme'
import renderer from 'react-test-renderer'
import {connect} from 'react-redux'
import _ from 'lodash'
import { hasEntitlement } from '../../src/entitlements'

import AddEditContact, {mapStateToProps, AddEditContactForm} from '../../src/contacts/AddEditContactComponent'

jest.mock('react-redux', () => mockConnect())
jest.mock('../../src/components/forms/Validators', () => ({Required: mockComponent('Required'),
    IsValidZipCode: mockComponent('IsValidZipCode'),
    Email: mockComponent('Email'),
    NormalizePhone: mockComponent('NormalizePhone'),
    phoneNumber: mockComponent('phoneNumber')
    }))
jest.mock('../../src/components/redux-dialog', () => mockComponent('ReduxDialog'))
jest.mock('../../src/components/redux-dialog/redux-dialog', () => ({
    closeDialog: 'closeDialog'
}))
jest.mock('../../src/components/forms/StatesFormField', () => (mockComponent('StatesFormField')))
jest.mock('../../src/components/forms/ContactTypesFormField', () => (mockComponent('ContactTypesFormField')))
jest.mock('../../src/components/forms/BooleanSelectField', () => (mockComponent('BooleanSelectField')))
jest.mock('../../src/components/forms/CaseInSensitiveFilter', () => (mockComponent('CaseInSensitiveFilter')))
jest.mock('material-ui/Card', () => ({Card: mockComponent('Card')}))
jest.mock('material-ui/IconButton', () => (mockComponent('IconButton')))
jest.mock('material-ui/svg-icons/content/send', () => (mockComponent('SendIcon')))
jest.mock('material-ui/RaisedButton', () => mockComponent('RaisedButton'))
jest.mock('redux-form', () => ({Field: mockComponent('Field'), reduxForm: () => (comp) => comp}))
jest.mock('material-ui/Toolbar', () => ({ Toolbar: mockComponent('Toolbar'), ToolbarGroup: mockComponent('ToolbarGroup') }))
jest.mock('material-ui/AutoComplete', () => ({fuzzyFilter: mockComponent('fuzzyFilter')}))
jest.mock('../../src/styles/styles', () => ({borderTop: 'solid 1px #e0e0e0', backgroundColor: '#f8f9fa', marginRight: '50px', padding: '25px'}))
jest.mock('redux-form-material-ui', () => ({AutoComplete: mockComponent('AutoComplete'), TextField: mockComponent('TextField')}))

describe('Add Edit Contact Form', () => {
    it('Renders correctly Contact', () => {
        const props = {
            companies: [{companyId: 1}, {companyId: 2}, {companyId: 3}],
            saveContact: jest.fn(),
            populateCompanyAddress: jest.fn(),
            submitting: true,
            asyncValidating: true,
            initialValues: {contactId: 1, contactName: 'abc'},
            handleSubmit: jest.fn(),
            emailAddress: ''
        }
        props.handleSubmit.mockReturnValueOnce('saveContact')
        const component = renderer.create(<AddEditContactForm {...props}/>).toJSON()
        expect(component).toMatchSnapshot()
        expect(props.handleSubmit).toHaveBeenCalledWith(props.saveContact)
    })
})

describe('Add Edit Contact onCompanyChange', () => {
    it('onCompanyChange click', () => {
        const props = {
            companies: [{companyId: 1}, {companyId: 2}, {companyId: 3}],
            saveContact: jest.fn(),
            populateCompanyAddress: jest.fn(),
            submitting: true,
            asyncValidating: true,
            initialValues: {contactId: 1, contactName: 'abc'},
            handleSubmit: jest.fn(),
            emailAddress: ''
        }
        props.handleSubmit.mockReturnValueOnce('saveContact')
        const component = shallow(<AddEditContactForm {...props}/>)
        const onCompanyChange = component.find({id: 'companyNameId'}).prop('onChange')
        onCompanyChange()
        expect(props.populateCompanyAddress).toHaveBeenCalled()
    })
})

it('mapStateToProps should map correctly', () => {
    expect(mapStateToProps({
        form: {AddEditContactForm: {values: {emailAddress: 'emailAddress'}}},
        contacts: {selectedContact: 'selectedContact'},
        companies: {allCompanies: {results: [{name: 'name1'}, {name: 'name2'}]}}
    })).toEqual({
        emailAddress: 'emailAddress',
        selectedContact: 'selectedContact',
        initialValues: 'selectedContact',
        companies: [
            'name1',
            'name2'
        ]
    })
})
