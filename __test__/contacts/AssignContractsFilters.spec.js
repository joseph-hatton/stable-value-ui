import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../MockComponent'
import mockConnect from '../MockConnect'
import mockReduxForm from '../MockReduxForm'
import {connect} from 'react-redux'
import {reset} from 'redux-form'

import AssignContractsFilters from '../../src/contacts/AssignContractsFilters'

jest.mock('react-redux', () => mockConnect())
jest.mock('material-ui/RaisedButton', () => mockComponent('RaisedButton'))
jest.mock('material-ui/Card/Card', () => mockComponent('Card'))
jest.mock('redux-form', () => mockReduxForm())
jest.mock('redux-form-material-ui', () => ({
  TextField: mockComponent('TextField')
}))
jest.mock('../../src/components/forms/ContractStatusesSelectField', () => mockComponent('ContractStatusesSelectField'))
jest.mock('../../src/components/forms/MaterialSelectField', () => mockComponent('MaterialSelectField'))
jest.mock('../../src/components/forms/ContactTypesFormField', () => mockComponent('ContactTypesFormField'))

describe('AssignContractsFilters', () => {
  let props
  beforeEach(() => {
    props = {
      clearAll: 'clearAll',
      companies: 'companies'
    }
  })
  it('should render correctly', () => {
    expect(renderer.create(<AssignContractsFilters {...props} />).toJSON()).toMatchSnapshot()
  })
  it('actions clearAll should reset form', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    const dispatch = jest.fn()
    expect(connect.mock.calls[0][1].clearAll()(dispatch))
    expect(dispatch).toHaveBeenCalled()
    expect(reset).toHaveBeenCalledWith('AssignContractsFiltersForm')
  })
  it('mapStateToProps should map companies in a text-value array', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][0]({companies: {allCompanies: {results: [{name: 'name1'}, {name: 'name2'}]}}})).toEqual({
      companies: [
        {text: 'name1', value: 'name1'},
        {text: 'name2', value: 'name2'}
      ]
    })
  })
})
