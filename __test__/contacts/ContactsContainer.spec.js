import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import {loadAllContacts} from '../../src/contacts/ContactActions'

import { ContactsContainer, mapStateToProps } from '../../src/contacts/ContactsContainer'
jest.mock('../../src/contacts/ContactActions', () => ({
    loadAllContacts: jest.fn()
}))
jest.mock('../../src/contacts/ContactsComponent', () => {
    return mockComponent('ContactsComponent')
})

describe('ContactsContainer Specs', () => {
    it('Renders Correctly ContactsContainer', () => {
        const props = {
            loadAllContacts,
            Contacts: [{}, {}, {}],
            editContact: {contactId: 1, contactName: 'abx'},
            saveResult: null,
            clearEditContactState: jest.fn(),
            clearSaveContactResult: jest.fn()
        }
        const ContactsContainerRender = renderer.create(
            <ContactsContainer {...props} />
        ).toJSON()
        expect(ContactsContainerRender).toMatchSnapshot()
        expect(props.loadAllContacts).not.toHaveBeenCalled()
    })

    it('Call loadAllContacts', () => {
        const props = {
            loadAllContacts,
            Contacts: null,
            editContact: {contactId: 1, contactName: 'abx'},
            saveResult: {},
            clearEditContactState: jest.fn(),
            clearSaveContactResult: jest.fn()
        }
        renderer.create(<ContactsContainer {...props} />)
        expect(props.loadAllContacts).toHaveBeenCalled()
    })

    it('Call clearSaveContactResult', () => {
        const props = {
            loadAllContacts,
            Contacts: null,
            editContact: {contactId: 1, contactName: 'abx'},
            saveResult: {name: 'abc'},
            clearEditContactState: jest.fn(),
            clearSaveContactResult: jest.fn()
        }
        const component = renderer.create(<ContactsContainer {...props} />)
        component.getInstance().componentWillUnmount()
        expect(props.clearSaveContactResult).toHaveBeenCalled()
    })
    it('not Call clearSaveContactResult', () => {
        const props = {
            loadAllContacts,
            Contacts: null,
            editContact: {contactId: 1, contactName: 'abx'},
            saveResult: null,
            clearEditContactState: jest.fn(),
            clearSaveContactResult: jest.fn()
        }
        const component = renderer.create(<ContactsContainer {...props} />)
        component.getInstance().componentWillUnmount()
        expect(props.clearSaveContactResult).not.toHaveBeenCalled()
    })
})

describe('ContactsContainer mapStateToProps', () => {
    it('return mapStateToProps', () => {
        const contacts = {
            allContacts: [{}, {}, {}],
            error: 'This is test',
            saveResult: {contactId: 1},
            editContact: {contactId: 1}
        }
        const result = mapStateToProps({contacts})
        expect(result).toEqual({'contacts': [{}, {}, {}], 'editContact': {'contactId': 1}, 'error': 'This is test', 'saveResult': {'contactId': 1}})
    })
})
