import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from '../MockComponent'
import mockConnect from '../MockConnect'
import {browserHistory} from 'react-router'
import {connect} from 'react-redux'

import ContactMenu, {ContactMenuLink} from '../../src/contacts/ContactMenu'

jest.mock('react-redux', () => mockConnect())
jest.mock('material-ui', () => ({
  Popover: mockComponent('Popover'),
  Menu: mockComponent('Menu'),
  MenuItem: mockComponent('MenuItem')
}))
jest.mock('../../src/contacts/ContactActions', () => ({
  selectContact: 'selectContact',
  deleteContact: 'deleteContact'
}))
jest.mock('../../src/components/redux-dialog/redux-dialog', () => ({
  openDialog: 'openDialog',
  closeDialog: 'closeDialog',
  openConfirmationAlert: 'openConfirmationAlert'
}))
jest.mock('react-router', () => ({
  browserHistory: {
    push: jest.fn()
  }
}))

describe('ContactsMenu', () => {
  let props
  beforeEach(() => {
    props = {
      selectedContact: {contactId: 123, contracts: []},
      open: 'open',
      closeDialog: jest.fn(),
      openDialog: jest.fn(),
      deleteContact: jest.fn(),
      openConfirmationAlert: jest.fn()
    }
    browserHistory.push.mockClear()
  })
  it('should render correctly', () => {
    const comp = renderer.create(<ContactMenu {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('onClick of the View Edit Button should close the menu and redirect', () => {
    shallow(<ContactMenu {...props} />).find('#ContactMenu-viewEditContactButton').prop('onClick')()
    expect(props.closeDialog).toHaveBeenCalledWith('contactMenu')
    expect(props.openDialog).toHaveBeenCalledWith('addEditContact')
  })
  it('onClick of the Assign Unassign Button should close the menu and open the assign unassign dialog', () => {
    shallow(<ContactMenu {...props} />).find('#ContactMenu-AssignUnassignContractsButton').prop('onClick')()
    expect(props.closeDialog).toHaveBeenCalledWith('contactMenu')
    expect(props.openDialog).toHaveBeenCalledWith('AssignUnassignContracts')
  })
  it('onClick of the Delete Button should close the menu and open the assign unassign dialog', () => {
    shallow(<ContactMenu {...props} />).find('#ContactMenu-DeleteContactButton').prop('onClick')()
    expect(props.closeDialog).toHaveBeenCalledWith('contactMenu')
    expect(props.openConfirmationAlert).toHaveBeenCalledTimes(1)
    expect(props.openConfirmationAlert.mock.calls[0][0].message)
      .toEqual('Are you sure you want to delete this contact?')
    props.openConfirmationAlert.mock.calls[0][0].onOk()
    expect(props.deleteContact).toHaveBeenCalled()
  })
  it('mapStateToProps should map correctly when contactMenu is set', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[1][0]({
      dialog: {contactMenu: {anchorEl: 'anchorEl'}},
      contacts: {selectedContact: 'selectedContact'}
    })).toEqual({
      open: true,
      anchorEl: 'anchorEl',
      selectedContact: 'selectedContact'
    })
  })
  it('mapStateToProps should map correctly when contactMenu is not set', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[1][0]({
      dialog: {},
      contacts: {selectedContact: 'selectedContact'}
    })).toEqual({
      open: false,
      anchorEl: null,
      selectedContact: 'selectedContact'
    })
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[1][1]).toEqual({
      closeDialog: 'closeDialog',
      openDialog: 'openDialog',
      deleteContact: 'deleteContact',
      openConfirmationAlert: 'openConfirmationAlert'
    })
  })
})

describe('ContactMenuLink', () => {
  let props
  beforeEach(() => {
    props = {
      cell: 'cell',
      row: 'row',
      selectContact: jest.fn(),
      openDialog: jest.fn()
    }
  })
  it('should render correctly', () => {
    const comp = renderer.create(<ContactMenuLink {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('on onClick shoud prevent default, select contracts and open dialog', () => {
    const comp = shallow(<ContactMenuLink {...props} />)
    const e = {nativeEvent: {preventDefault: jest.fn()}, currentTarget: 'currentTarget'}
    comp.prop('onClick')(e)
    expect(e.nativeEvent.preventDefault).toHaveBeenCalled()
    expect(props.selectContact).toHaveBeenCalledWith('row')
    expect(props.openDialog).toHaveBeenCalledWith('contactMenu', {anchorEl: 'currentTarget'})
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[0][1]).toEqual({
      openDialog: 'openDialog',
      selectContact: 'selectContact'
    })
  })
})
