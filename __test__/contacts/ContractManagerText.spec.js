import React from 'react'
import renderer from 'react-test-renderer'
import mockConnect from '../MockConnect'
import {connect, connectChildFn} from 'react-redux'

import ConnectedContractManagerText, {
  ContractManagerText,
  mapStateToProps
} from '../../src/contacts/ContractManagerText'

jest.mock('react-redux', () => mockConnect())

const managers = [
  {
    value: 500,
    text: 'Invesco'
  },
  {
    value: 505,
    text: 'Bank of Milan'
  }
]

describe('Contract Manager Text', () => {
  it('Renders Contract Manager Name for a give manager id', () => {
    const component = renderer.create(<ContractManagerText managerId="505" managers={managers}/>)

    expect(component.toJSON()).toMatchSnapshot()

    expect(connect).toHaveBeenCalledWith(mapStateToProps)
    expect(connectChildFn).toHaveBeenCalledWith(ContractManagerText)
    expect(ConnectedContractManagerText).toEqual(ContractManagerText)
  })

  it('maps State To Props', () => {
    const actualManagers = mapStateToProps({contacts: {managers}})

    expect(actualManagers).toEqual({managers})
  })
})
