import React from 'react'
import mockComponent from '../MockComponent'
import mockConnect from '../MockConnect'
import renderer from 'react-test-renderer'
import Contacts from '../../src/contacts/ContactsComponent'

jest.mock('react-redux', () => mockConnect())
jest.mock('../../src/components/grid/material-ui-grid/Grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn()
}))
jest.mock('material-ui/FloatingActionButton', () => mockComponent('FloatingActionButton'))
jest.mock('material-ui/svg-icons/content/add', () => mockComponent('ContentAdd'))
jest.mock('../../src/contacts/ContactColumns', () => ({
  ContactColumns: () => []
}))
jest.mock('../../src/contacts/ContractsMenu', () => mockComponent('ContractsMenu'))
jest.mock('../../src/contacts/ContactMenu', () => mockComponent('ContactMenu'))
jest.mock('../../src/contacts/AssignUnassignContracts', () => mockComponent('AssignUnassignContracts'))
jest.mock('../../src/contacts/AssignContracts', () => mockComponent('AssignContracts'))
jest.mock('../../src/contacts/AddEditContactComponent', () => mockComponent('AddEditContactComponent'))
jest.mock('../../src/contacts/ContactColumns', () => ({
  ContactColumns: 'ContactColumns'
}))

describe('Contacts Component', () => {
  it('renders correctly', () => {
    const props = {
      contacts: [{}, {}, {}]
    }
    const component = renderer.create(<Contacts {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders correctly when there are no contacts', () => {
    const props = {
      contacts: null
    }
    const component = renderer.create(<Contacts {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
})
