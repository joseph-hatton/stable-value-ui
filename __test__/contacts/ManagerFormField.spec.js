import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../MockComponent'
import {shallow} from 'enzyme'
import { loadAllContacts } from '../../src/contacts/ContactActions'
import { ManagersFormField, mapStateToProps } from '../../src/contacts/ManagersFormField'
jest.mock('../../src/components/forms/MaterialSelectField', () => mockComponent('MaterialSelectField'))
jest.mock('../../src/contacts/ContactActions', () => ({loadAllContacts: jest.fn()}))
jest.mock('react-redux', () => ({connect: () => (comp) => comp}))

describe('ManagersFormField', () => {
    it('should render correctly', () => {
        const props = {
            name: 'the name',
            options: [{}, {}],
            loadAllContacts
        }
        const component = renderer.create(<ManagersFormField {...props} />)
        expect(component.toJSON()).toMatchSnapshot()
    })

    it('should loadAllContacts Not call', () => {
        const props = {
            name: 'the name',
            options: [{}, {}, {}],
            loadAllContacts
        }
        renderer.create(<ManagersFormField {...props} />)
        expect(props.loadAllContacts).not.toHaveBeenCalled()
    })

    it('should loadAllContacts correctly', () => {
        const props = {
            name: 'the name',
            options: [],
            loadAllContacts
        }
        renderer.create(<ManagersFormField {...props} />)
        expect(props.loadAllContacts).toHaveBeenCalled()
    })
})

describe('mapStateToProps', () => {
    it('should mapStateToProps correctly', () => {
        const state = {
            contacts: {
                managers: [{}, {}, {}]
            }
        }
        const result = mapStateToProps(state)
        expect(result.options).toBe(state.contacts.managers)
    })
})
