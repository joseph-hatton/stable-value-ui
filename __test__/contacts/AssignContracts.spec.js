import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../MockComponent'
import mockConnect from '../MockConnect'
import {connect} from 'react-redux'

import AssignContracts from '../../src/contacts/AssignContracts'

jest.mock('react-redux', () => mockConnect())
jest.mock('material-ui/RaisedButton', () => mockComponent('RaisedButton'))
jest.mock('../../src/components/redux-dialog/redux-dialog', () => mockComponent('ReduxDialog'))
jest.mock('../../src/components/redux-dialog', () => ({
  openDialog: 'openDialog',
  closeDialog: 'closeDialog'
}))
jest.mock('../../src/contacts/AssignContractsFilters', () => mockComponent('AssignContractsFilters'))
jest.mock('../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn().mockReturnValue('CreateGridConfig')
}))
jest.mock('../../src/contacts/ContactActions', () => ({
  selectContractRow: 'selectContractRow',
  queueAssignContracts: 'queueAssignContracts',
  loadAllContractsWithContactCompanies: 'loadAllContractsWithContactCompanies'
}))
jest.mock('../../src/companies/CompaniesActions', () => ({
  loadAllCompanies: 'loadAllCompanies'
}))
jest.mock('../../src/contacts/AssignUnassignContractsColumns', () => ({
  AssignUnassignContractsColumns: () => 'AssignUnassignContractsColumns'
}))

describe('AssignContracts', () => {
  let props
  beforeEach(() => {
    props = {
      selectedContact: {firstName: 'firstName', lastName: 'lastName'},
      contracts: null,
      companies: null,
      closeDialog: jest.fn(),
      selectContractRow: jest.fn(),
      loadAllContractsWithContactCompanies: jest.fn(),
      loadAllCompanies: jest.fn(),
      queueAssignContracts: jest.fn()
    }
  })
  it('should render correctly', () => {
    expect(renderer.create(<AssignContracts {...props} />).toJSON()).toMatchSnapshot()
  })
  it('loadAllContracts should be called when contracts is not set', () => {
    renderer.create(<AssignContracts {...props} />)
    expect(props.loadAllContractsWithContactCompanies).toHaveBeenCalled()
  })
  it('loadAllContracts should not be called when contracts is set', () => {
    props.contracts = [{}]
    renderer.create(<AssignContracts {...props} />)
    expect(props.loadAllContractsWithContactCompanies).not.toHaveBeenCalled()
  })
  it('loadAllCompanies should be called when companies is not set', () => {
    renderer.create(<AssignContracts {...props} />)
    expect(props.loadAllCompanies).toHaveBeenCalled()
  })
  it('loadAllCompanies should not be called when companies is set', () => {
    props.companies = [{}]
    renderer.create(<AssignContracts {...props} />)
    expect(props.loadAllCompanies).not.toHaveBeenCalled()
  })
  describe('mapStateToProps', () => {
    it('should map correctly when there are no filters', () => {
      expect(connect).toHaveBeenCalledTimes(1)
      expect(connect.mock.calls[0][0]({
        form: {},
        contacts: {selectedContact: 'selectedContact', allContracts: 'contracts'},
        companies: {allCompanies: {results: 'companies'}}
      })).toEqual({
        selectedContact: 'selectedContact',
        companies: 'companies',
        contracts: 'contracts'
      })
    })
    it('should map correctly when filtering out via status', () => {
      expect(connect).toHaveBeenCalledTimes(1)
      const allContracts = [
        {status: 'ACTIVE'},
        {status: 'TERMINATED'}
      ]
      expect(connect.mock.calls[0][0]({
        form: {AssignContractsFiltersForm: {values: {status: 'ACTIVE'}}},
        contacts: {selectedContact: 'selectedContact', allContracts},
        companies: {allCompanies: {results: 'companies'}}
      })).toEqual({
        selectedContact: 'selectedContact',
        companies: 'companies',
        contracts: [{status: 'ACTIVE'}]
      })
    })
    it('should map correctly when filtering out via shortPlanName ignoring case and including partial matches', () => {
      expect(connect).toHaveBeenCalledTimes(1)
      const allContracts = [
        {shortPlanName: 'ADM'},
        {shortPlanName: 'Agilent'}
      ]
      expect(connect.mock.calls[0][0]({
        form: {AssignContractsFiltersForm: {values: {shortPlanName: 'ad'}}},
        contacts: {selectedContact: 'selectedContact', allContracts},
        companies: {allCompanies: {results: 'companies'}}
      })).toEqual({
        selectedContact: 'selectedContact',
        companies: 'companies',
        contracts: [{shortPlanName: 'ADM'}]
      })
    })
    it('should map correctly when filtering out via companyName', () => {
      expect(connect).toHaveBeenCalledTimes(1)
      const allContracts = [
        {contractId: 321, compName: 'At&t'},
        {contractId: 123, compName: 'Verizon'}
      ]
      const allContacts = [
        {companyName: 'Verizon', contracts: [{contractId: 123, compName: 'Verizon'}]},
        {companyName: 'At&t', contracts: [{contractId: 321, compName: 'At&t'}]}
      ]
      expect(connect.mock.calls[0][0]({
        form: {AssignContractsFiltersForm: {values: {companyName: 'Verizon'}}},
        contacts: {selectedContact: 'selectedContact', allContacts, allContracts},
        companies: {allCompanies: {results: 'companies'}}
      })).toEqual({
        selectedContact: 'selectedContact',
        companies: 'companies',
        contracts: [{compName: 'Verizon', contractId: 123}]
      })
    })
    it('should map correctly when filtering out via contactType', () => {
      expect(connect).toHaveBeenCalledTimes(1)
      const allContracts = [
        {contractId: 321, contactType: 'Broker'},
        {contractId: 123, contactType: 'Manager'}
      ]
      const allContacts = [
        {contactType: 'Manager', contracts: [{contractId: 123, compName: 'Verizon'}]},
        {contactType: 'Broker', contracts: [{contractId: 321, compName: 'At&t'}]}
      ]
      expect(connect.mock.calls[0][0]({
        form: {AssignContractsFiltersForm: {values: {contactType: 'Manager'}}},
        contacts: {selectedContact: 'selectedContact', allContacts, allContracts},
        companies: {allCompanies: {results: 'companies'}}
      })).toEqual({
        selectedContact: 'selectedContact',
        companies: 'companies',
        contracts: [{contactType: 'Manager', contractId: 123}]
      })
    })
    it('should map correctly when filtering out via lastName ignoring case and including partial matches', () => {
      expect(connect).toHaveBeenCalledTimes(1)
      const allContracts = [
        {contractId: 321, lastName: 'Grant'},
        {contractId: 123, lastName: 'Hamm'}
      ]
      const allContacts = [
        {lastName: 'Hamm', contracts: [{contractId: 123, lastName: 'Hamm'}]},
        {lastName: 'Grant', contracts: [{contractId: 321, lastName: 'Grant'}]}
      ]
      expect(connect.mock.calls[0][0]({
        form: {AssignContractsFiltersForm: {values: {lastName: 'ha'}}},
        contacts: {selectedContact: 'selectedContact', allContacts, allContracts},
        companies: {allCompanies: {results: 'companies'}}
      })).toEqual({
        selectedContact: 'selectedContact',
        companies: 'companies',
        contracts: [{contractId: 123, lastName: 'Hamm'}]
      })
    })
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][1]).toEqual({
      openDialog: 'openDialog',
      closeDialog: 'closeDialog',
      selectContractRow: 'selectContractRow',
      loadAllContractsWithContactCompanies: 'loadAllContractsWithContactCompanies',
      loadAllCompanies: 'loadAllCompanies',
      queueAssignContracts: 'queueAssignContracts'
    })
  })
})
