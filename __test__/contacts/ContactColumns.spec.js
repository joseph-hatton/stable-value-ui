import React from 'react'
import _ from 'lodash'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'

import {
  ContactColumns
} from '../../src/contacts/ContactColumns'

const getRenderer = (field) => ContactColumns.find(column => column.field === field).renderer

jest.mock('react-router', () => ({
  Link: mockComponent('Link')
}))

jest.mock('../../src/contacts/ContractsMenu', () => ({
  ContractsMenuLink: mockComponent('ContractsMenuLink')
}))

jest.mock('../../src/contacts/ContactMenu', () => ({
  ContactMenuLink: mockComponent('ContactMenuLink')
}))

describe('Contact Columns', () => {
  let row
  beforeEach(() => {
    row = {
      city: 'Charlotte',
      companyId: 620,
      companyName: 'Bank of America, N.A. Trustee for the Stable Value Master Trust',
      contactId: 481
    }
  })
  it('Defines all ContactColumns columns', () => {
    expect(ContactColumns.length).toEqual(8)
    expect(_.map(ContactColumns, 'field')).toEqual([
      'id',
      'name',
      'contactType',
      'companyName',
      'receiveStatement',
      'receiveInvoice',
      'receiveScheduleA',
      'contracts'
    ])
  })
  it('renders name', () => {
    const comp = renderer.create(getRenderer('name')('cell', row))
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('renders contracts', () => {
    const comp = renderer.create(getRenderer('contracts')([], row))
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('renders receiveScheduleA', () => {
    expect(getRenderer('receiveScheduleA')(true)).toEqual('Yes')
  })
})
