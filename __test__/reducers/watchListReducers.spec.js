import watchListReducers from '../../src/reducers/watchListReducers'

describe('watchListReducers', () => {
  let state, action
  beforeEach(() => {
    state = {
      all: [],
      reasons: [],
      actionSteps: [],
      currentWatchList: {},
      selectedWatchList: {}
    }
    action = {}
  })
  it('all watchlists should map', () => {
    action.type = 'LOAD_ALL_WATCH_LISTS_FULFILLED'
    action.payload = {results: 'results'}

    const res = watchListReducers(state, action)

    expect(res.all).toBe('results')
  })
  it('reasons should map', () => {
    action.type = 'LOAD_REASONS_FULFILLED'
    action.payload = {results: 'results'}

    const res = watchListReducers(state, action)

    expect(res.reasons).toBe('results')
  })
  it('action steps should map', () => {
    action.type = 'LOAD_ACTION_STEPS_FULFILLED'
    action.payload = {results: 'results'}

    const res = watchListReducers(state, action)

    expect(res.actionSteps).toBe('results')
  })
  it('current watchlist should map', () => {
    action.type = 'LOAD_WATCH_LIST_FULFILLED'
    action.payload = 'results'

    const res = watchListReducers(state, action)

    expect(res.currentWatchList).toBe('results')
  })
  it('selected watchlist should map', () => {
    action.type = 'SELECT_WATCH_LIST'
    action.payload = 'results'

    const res = watchListReducers(state, action)

    expect(res.selectedWatchList).toBe('results')
  })
  it('delete watchlist should map', () => {
    state.all = [{watchListId: 123}, {watchListId: 321}, {watchListId: 456}]
    action.type = 'DELETE_WATCH_LIST_FULFILLED'
    action.payload = {id: 123}

    const res = watchListReducers(state, action)

    expect(res.all.length).toBe(2)
    expect(res.all.some((i) => i.watchListId === 123)).toBe(false)
    expect(res.all.some((i) => i.watchListId === 321)).toBe(true)
    expect(res.all.some((i) => i.watchListId === 456)).toBe(true)
  })
  it('delete reason should map', () => {
    state.reasons = [{reasonId: 123}, {reasonId: 321}]
    action.type = 'DELETE_REASON_FULFILLED'
    action.payload = {id: 123}

    const res = watchListReducers(state, action)

    expect(res.reasons.length).toBe(1)
    expect(res.reasons[0].reasonId).toBe(321)
  })
  it('selected reason should map', () => {
    action.type = 'SELECT_REASON'
    action.payload = 'results'

    const res = watchListReducers(state, action)

    expect(res.selectedReason).toBe('results')
  })
  it('save reason should map', () => {
    state.reasons = [{reasonId: 123}, {reasonId: 321}]
    action.type = 'SAVE_REASON_FULFILLED'
    action.payload = {reasonId: 123, new: true}

    const res = watchListReducers(state, action)

    expect(res.reasons).toEqual([{reasonId: 123, new: true}, {reasonId: 321}])
  })
  it('delete action step should map', () => {
    state.actionSteps = [{actionStepId: 123}, {actionStepId: 321}]
    action.type = 'DELETE_ACTION_STEP_FULFILLED'
    action.payload = {id: 123}

    const res = watchListReducers(state, action)

    expect(res.actionSteps.length).toBe(1)
    expect(res.actionSteps[0].actionStepId).toBe(321)
  })
  it('selected action step should map', () => {
    action.type = 'SELECT_ACTION_STEP'
    action.payload = 'results'

    const res = watchListReducers(state, action)

    expect(res.selectedActionStep).toBe('results')
  })
  it('save action step should map', () => {
    state.actionSteps = [{actionStepId: 123}, {actionStepId: 321}]
    action.type = 'SAVE_ACTION_STEP_FULFILLED'
    action.payload = {actionStepId: 123, new: true}

    const res = watchListReducers(state, action)

    expect(res.actionSteps).toEqual([{actionStepId: 123, new: true}, {actionStepId: 321}])
  })
  it('change current tab should map', () => {
    action.type = 'CHANGE_CURRENT_TAB'
    action.payload = 'results'

    const res = watchListReducers(state, action)

    expect(res.currentTab).toBe('results')
  })
})
