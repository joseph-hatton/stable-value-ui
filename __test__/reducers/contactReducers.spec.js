import contactReducer from '../../src/reducers/contactReducers'

describe('contact reducers', () => {
  const defaultContact = {
    receiveStatement: false,
    receiveInvoice: false,
    receiveScheduleA: false
  }

  const initialState = {
    allContacts: [],
    allContracts: [],
    editContact: {...defaultContact},
    managers: [],
    selectedContracts: [],
    selectedContact: {},
    originalCopySelectedContact: {},
    bufferedActions: {
      assignContracts: [],
      unassignContracts: []
    }
  }

  it('get default state with unknown action type', () => {
    expect(contactReducer(undefined, {})).toEqual(initialState)
  })

  describe('QUEUE_ASSIGN_CONTRACTS', () => {
    let state, action
    beforeEach(() => {
      state = {
        bufferedActions: {
          unassignContracts: [],
          assignContracts: []
        },
        selectedContact: {
          contracts: []
        },
        originalCopySelectedContact: {
          contracts: []
        }
      }
      action = {
        type: 'QUEUE_ASSIGN_CONTRACTS',
        payload: []
      }
    })
    it('should do nothing if the payload is in the original list and in the current list', () => {
      const contract = {contractId: 123}
      state.selectedContact.contracts = [contract]
      state.originalCopySelectedContact.contracts = [contract]
      action.payload = [contract]

      expect(contactReducer(state, action)).toEqual({
        bufferedActions: {
          unassignContracts: [],
          assignContracts: []
        },
        selectedContact: {
          contracts: [contract]
        },
        originalCopySelectedContact: {
          contracts: [contract]
        }
      })
    })
    it('should remove the contract from unassign buffer and add to current list if the payload is in the original list, not in the current list and in the unassign queue', () => {
      const contract = {contractId: 123}
      state.originalCopySelectedContact.contracts = [contract]
      state.bufferedActions.unassignContracts = [contract]
      action.payload = [contract]

      expect(contactReducer(state, action)).toEqual({
        bufferedActions: {
          unassignContracts: [],
          assignContracts: []
        },
        selectedContact: {
          contracts: [contract]
        },
        originalCopySelectedContact: {
          contracts: [contract]
        }
      })
    })
    it('should do nothing if the payload is not in the original list and in the current list', () => {
      const contract = {contractId: 123}
      state.selectedContact.contracts = [contract]
      action.payload = [contract]

      expect(contactReducer(state, action)).toEqual({
        bufferedActions: {
          unassignContracts: [],
          assignContracts: []
        },
        selectedContact: {
          contracts: [contract]
        },
        originalCopySelectedContact: {
          contracts: []
        }
      })
    })
    it('should add the contract to the action queue and to the current list if the payload is not in the original list and not in the current list', () => {
      const contract = {contractId: 123}
      action.payload = [contract]

      expect(contactReducer(state, action)).toEqual({
        bufferedActions: {
          unassignContracts: [],
          assignContracts: [contract]
        },
        selectedContact: {
          contracts: [contract]
        },
        originalCopySelectedContact: {
          contracts: []
        }
      })
    })
  })
  describe('QUEUE_UNASSIGN_CONTRACTS', () => {
    let state, action
    beforeEach(() => {
      state = {
        bufferedActions: {
          unassignContracts: [],
          assignContracts: []
        },
        selectedContact: {
          contracts: []
        },
        originalCopySelectedContact: {
          contracts: []
        }
      }
      action = {
        type: 'QUEUE_UNASSIGN_CONTRACTS',
        payload: []
      }
    })
    it('should add the payload to the unassign queue and remove it from the current list if the payload is in the original list', () => {
      const contract = {contractId: 123}
      state.originalCopySelectedContact.contracts = [contract]
      state.selectedContact.contracts = [contract]
      action.payload = [contract]

      expect(contactReducer(state, action)).toEqual({
        bufferedActions: {
          unassignContracts: [contract],
          assignContracts: []
        },
        selectedContact: {
          contracts: []
        },
        originalCopySelectedContact: {
          contracts: [contract]
        }
      })
    })
    it('should remove the payload from the assign queue and remove it from the current list if the payload is not in the original list', () => {
      const contract = {contractId: 123}
      state.selectedContact.contracts = [contract]
      state.bufferedActions.assignContracts = [contract]
      action.payload = [contract]

      expect(contactReducer(state, action)).toEqual({
        bufferedActions: {
          unassignContracts: [],
          assignContracts: []
        },
        selectedContact: {
          contracts: []
        },
        originalCopySelectedContact: {
          contracts: []
        }
      })
    })
  })
})
