import moment from 'moment'
import bookValuesReducers from '../../src/reducers/bookValuesReducers'

describe('bookValuesReducers', () => {
  let state, action
  beforeEach(() => {
    state = {
      allFees: [],
      loadedFee: [],
      filtered: false,
      dateFilter: moment(),
      filterOnDay: false
    }
    action = {}
  })
  it('all fees should map', () => {
    action.type = 'LOAD_ALL_BOOK_VALUES_FULFILLED'
    action.payload = {results: 'results'}

    const res = bookValuesReducers(state, action)

    expect(res.filtered).toBe(false)
    expect(res.allBookValues).toBe('results')
  })
  it('filtered fees should map', () => {
    action.type = 'FILTER_ALL_BOOK_VALUES_FULFILLED'
    action.payload = {results: 'results'}

    const res = bookValuesReducers(state, action)

    expect(res.filtered).toBe(true)
    expect(res.allBookValues).toBe('results')
  })
  it('fees by contract should map', () => {
    action.type = 'SELECT_BOOK_VALUES_BY_CONTRACT_ID_FULFILLED'
    action.payload = [{results: 'results'}, {contract: 'contractNumber'}]

    const res = bookValuesReducers(state, action)

    expect(res.filtered).toBe(false)
   // expect(res.loadedBookValue).toBe('results')
  })
  it('filtered fees by contract should map', () => {
    action.type = 'FILTER_BOOK_VALUES_BY_CONTRACT_ID_FULFILLED'
    action.payload = {results: 'results'}

    const res = bookValuesReducers(state, action)

    expect(res.filtered).toBe(true)
    expect(res.loadedBookValue).toBe('results')
  })
  it('contract id should map', () => {
    action.type = 'SET_CONTRACT_ID'
    action.payload = 123

    const res = bookValuesReducers(state, action)

    expect(res.currentContractId).toBe(123)
  })
  it('date filter should map', () => {
    action.type = 'SET_DATE_FILTER'
    action.payload = 123

    const res = bookValuesReducers(state, action)

    expect(res.dateFilter).toBe(123)
  })
  it('filter on day should map', () => {
    action.type = 'SET_FILTER_ON_DAY'
    action.payload = 'filterOnDay'

    const res = bookValuesReducers(state, action)

    expect(res.filterOnDay).toBe('filterOnDay')
  })
})
