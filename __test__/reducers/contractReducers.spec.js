import contractReducer, {insertUpdatedContract} from '../../src/reducers/contractReducers'

describe('contract reducer', () => {
    const defaultContract = {
        status: 'PENDING'
      }
      const initialState = {
        allContracts: {results: undefined},
        contract: undefined,
        currentContractFormId: undefined,
        derivativeTypeData: [],
        activeUnderwritingTab: 'Model Inputs',
        creditingRates: [],
        canLeaveWithoutSaving: false
      }
      it('get default state', () => {
        expect(contractReducer(undefined, {})).toEqual(initialState)
      })
      describe('INITIALIZE_CONTRACT', () => {
        let state, action
        beforeEach(() => {
            state = { contract: defaultContract }

      action = {
        type: 'INITIALIZE_CONTRACT',
        payload: []
        }
      })
      it('should do nothing id the payload ', () => {
        expect(contractReducer(state, action)).toEqual({'contract': {'status': 'PENDING'}, 'contractContacts': [], 'contractLifeCycleStatus': undefined})
      })
    })

    describe('insertUpdatedContract', () => {
        it('should replace in the contracts list with the updated one and return a clone', () => {
            const savedContract = {
                contractId: 123,
                testData: 'this is test data'
            }
            const contracts = [
                {contractId: 123, oldData: 'should go away'},
                {contractId: 321, currentData: 'should not go away'}
            ]

            expect(insertUpdatedContract(savedContract, contracts)).toEqual([
                {contractId: 123, testData: 'this is test data'},
                {contractId: 321, currentData: 'should not go away'}
            ])
        })
        it('should append to the contracts list with the new one and return a clone', () => {
            const savedContract = {
                contractId: 456,
                testData: 'this is test data'
            }
            const contracts = [
                {contractId: 123, currentData: 'should not go away'},
                {contractId: 321, currentData: 'should not go away'}
            ]

            expect(insertUpdatedContract(savedContract, contracts)).toEqual([
                {contractId: 123, currentData: 'should not go away'},
                {contractId: 321, currentData: 'should not go away'},
                {contractId: 456, testData: 'this is test data'}
            ])
        })
        it('should sort the array based on contractNumber when the new item is added', () => {
            const savedContract = {
                contractId: 456,
                testData: 'this is test data',
                contractNumber: 'abc-122'
            }
            const contracts = [
                {contractId: 123, contractNumber: 'abc-123', currentData: 'should not go away'},
                {contractId: 321, contractNumber: 'abc-124', currentData: 'should not go away'}
            ]

            expect(insertUpdatedContract(savedContract, contracts)).toEqual([
                {contractId: 456, contractNumber: 'abc-122', testData: 'this is test data'},
                {contractId: 123, contractNumber: 'abc-123', currentData: 'should not go away'},
                {contractId: 321, contractNumber: 'abc-124', currentData: 'should not go away'}
            ])
        })
    })

    describe('SET_CURRENT_CONTRACT_FORM', () => {
        let state, action
        beforeEach(() => {
            state = { contract: defaultContract }

      action = {
        type: 'SET_CURRENT_CONTRACT_FORM',
        payload: [],
        contractFormId: 'contractForm'
        }
      })
      it('current form ', () => {
          const result = contractReducer(state, action)
        expect(result.currentContractFormId).toEqual('contractForm')
      })
    })

    describe('SAVE_CONTRACT_FULFILLED', () => {
        let state, action
        const contractSave = jest.fn()
        const defautlContractSave = defaultContract
        beforeEach(() => {
            defautlContractSave.contractType = 'SINGLE_PLAN'
            defautlContractSave.effectiveDate = '2018-04-26T00:00:00.000Z'
            defautlContractSave.shortPlanName = 'test ldpn'
            state = { contract: defautlContractSave, allContracts: {results: []} }

      action = {
        type: 'SAVE_CONTRACT_FULFILLED',
        payload: defautlContractSave
        }
      })
      it('should call contract save function ', () => {
          const result = contractReducer(state, action)
          expect(result.contract.shortPlanName).toEqual('test ldpn')
        expect(contractSave).toHaveBeenCalled
      })
    })
    describe('UPDATE_CONTRACT_STATUS_FULFILLED', () => {
        let state, action
        const contractSave = jest.fn()
        const defautlContractUpdate = defaultContract
        defautlContractUpdate.contractType = 'SINGLE_PLAN'
        defautlContractUpdate.effectiveDate = '2018-04-26T00:00:00.000Z'
        defautlContractUpdate.shortPlanName = 'updating'
        beforeEach(() => {
            state = { contract: defautlContractUpdate, allContracts: {results: []} }

      action = {
        type: 'UPDATE_CONTRACT_STATUS_FULFILLED',
        payload: defautlContractUpdate
        }
      })
      it('should update contract and call save function ', () => {
          const result = contractReducer(state, action)
        expect(result.contract.contractType).toEqual('SINGLE_PLAN')
        expect(contractSave).toHaveBeenCalled
      })
    })
    describe('LOAD_CONTRACT_FULFILLED', () => {
        let state, action
        const contractSave = jest.fn()
        const results = [{lastname: 'abc', firstName: '123'}, {lastname: 'abc', firstName: '123'}, {lastname: 'abc', firstName: '123'}]
        const defautlContractLoad = defaultContract
        beforeEach(() => {
            defautlContractLoad.shortPlanName = 'load'
       state = { contract: defautlContractLoad }

      action = {
        type: 'LOAD_CONTRACT_FULFILLED',
        payload: [{defautlContractLoad}, {results}]
        }
      })
      it('should clear contract ', () => {
          const result = contractReducer(state, action)
          expect(result.contract.defautlContractLoad.shortPlanName).toEqual('load')
      })
    })
    describe('LOAD_MODEL_INPUTS_FULFILLED', () => {
        let state, action
        const contractSave = jest.fn()
        const results = [{lastname: 'abc', firstName: '123'}, {lastname: 'abc', firstName: '123'}, {lastname: 'abc', firstName: '123'}]
        const defautlContractClear = defaultContract
        defautlContractClear.contractId = 123
        beforeEach(() => {
       state = { contract: defautlContractClear }

      action = {
        type: 'LOAD_MODEL_INPUTS_FULFILLED',
        payload: {results: [ {}, {}, {} ]}
        }
      })
      it('should model inputs load ', () => {
          const result = contractReducer(state, action)
          expect(result.modelInputs.length).toEqual(3)
      })
    })
    describe('SAVE_MODEL_INPUTS_FULFILLED', () => {
        let state, action
        const contractSave = jest.fn()
        const results = [{lastname: 'abc', firstName: '123'}, {lastname: 'abc', firstName: '123'}, {lastname: 'abc', firstName: '123'}]
        const defautlContractClear = defaultContract
        defautlContractClear.contractId = 123
        beforeEach(() => {
       state = { contract: defautlContractClear, modelInputs: [ {}, {} ] }

      action = {
        type: 'SAVE_MODEL_INPUTS_FULFILLED',
        payload: [ {}, {}, {} ]
        }
      })
      it('should save modelinputs  ', () => {
          const result = contractReducer(state, action)
          expect(result.modelInputSaved).toEqual(true)
          expect(result.modelInputs.length).toEqual(5)
      })
    })
    describe('SELECT_MODEL_INPUT', () => {
        let state, action
        const contractSave = jest.fn()
        const results = [{lastname: 'abc', firstName: '123'}, {lastname: 'abc', firstName: '123'}, {lastname: 'abc', firstName: '123'}]
        const defautlContractClear = defaultContract
        defautlContractClear.contractId = 123
        beforeEach(() => {
       state = { contract: defautlContractClear, modelInputs: [ {}, {} ] }

      action = {
        type: 'SELECT_MODEL_INPUT',
        payload: { modelInputid: 123 }
        }
      })
      it('should select modelinputs  ', () => {
          const result = contractReducer(state, action)
          expect(result.modelInputSaved).toEqual(undefined)
          expect(result.modelInput.modelInputid).toEqual(123)
      })
    })

    describe('DELETE_MODEL_INPUT_FULFILLED', () => {
        let state, action
        const contractSave = jest.fn()
        const results = [{lastname: 'abc', firstName: '123'}, {lastname: 'abc', firstName: '123'}, {lastname: 'abc', firstName: '123'}]
        const defautlContractClear = defaultContract
        defautlContractClear.contractId = 123
        beforeEach(() => {
       state = { contract: defautlContractClear, modelInputs: [ { underwritingId: 123 }, {} ] }

      action = {
        type: 'DELETE_MODEL_INPUT_FULFILLED',
        payload: { id: 123 }
        }
      })
      it('should delete modelinputs  ', () => {
          const result = contractReducer(state, action)
          expect(result.modelInputs.length).toEqual(1)
      })
    })
    describe('LOAD_STABLE_VALUE_FUNDS_FULFILLED', () => {
        let state, action
        const contractSave = jest.fn()
        const results = [{lastname: 'abc', firstName: '123'}, {lastname: 'abc', firstName: '123'}, {lastname: 'abc', firstName: '123'}]
        const defautlContractClear = defaultContract
        defautlContractClear.contractId = 123
        beforeEach(() => {
       state = { contract: defautlContractClear }

      action = {
        type: 'LOAD_STABLE_VALUE_FUNDS_FULFILLED',
        payload: { total: 123, results: [ {}, {}, {} ] }
        }
      })
      it('should load stable value funds  ', () => {
          const result = contractReducer(state, action)
          expect(result.stableValueFunds.length).toEqual(3)
      })
    })
    describe('SAVE_STABLE_VALUE_FUND_FULFILLED', () => {
        let state, action
        const contractSave = jest.fn()
        beforeEach(() => {
       state = { contract: defaultContract, stableValueFunds: [{stableValueFundId: 1}, {stableValueFundId: 2}] }
      action = {
        type: 'SAVE_STABLE_VALUE_FUND_FULFILLED',
        payload: { svBookValue: 1, svMarketValue: 1 }
        }
      })
      it('should save stable value funds  ', () => {
          const result = contractReducer(state, action)
          expect(result.stableValueFunds.length).toEqual(3)
      })
    })
    describe('SELECT_STABLE_VALUE_FUND', () => {
        let state, action
        const contractSave = jest.fn()
        beforeEach(() => {
       state = { contract: defaultContract, stableValueFunds: [{stableValueFundId: 1}, {stableValueFundId: 2}] }
      action = {
        type: 'SELECT_STABLE_VALUE_FUND',
        payload: { svBookValue: 1, svMarketValue: 1 }
        }
      })
      it('should select stable value funds  ', () => {
          const result = contractReducer(state, action)
          expect(result.stableValueFund.svBookValue).toEqual(1)
      })
    })
    describe('DELETE_STABLE_VALUE_FUND_FULFILLED', () => {
        let state, action
        const contractSave = jest.fn()
        beforeEach(() => {
       state = { contract: defaultContract, stableValueFunds: [{stableValueFundId: 1}, {stableValueFundId: 2}] }
      action = {
        type: 'DELETE_STABLE_VALUE_FUND_FULFILLED',
        payload: { id: 1 }
        }
      })
      it('should delete stable value funds  ', () => {
          const result = contractReducer(state, action)
          expect(result.stableValueFunds.length).toEqual(1)
      })
    })
    describe('ACTIVATE_UNDERWRITING_TAB', () => {
        let state, action
        beforeEach(() => {
       state = { contract: defaultContract, stableValueFunds: [{stableValueFundId: 1}, {stableValueFundId: 2}] }
      action = {
        type: 'ACTIVATE_UNDERWRITING_TAB',
        label: 'Model Inputs'
        }
      })
      it('should activate tab  ', () => {
          const result = contractReducer(state, action)
          expect(result.activeUnderwritingTab).toEqual('Model Inputs')
      })
    })
    describe('LOAD_CONTRACT_CONTACTS_FULFILLED', () => {
        let state, action
        const results = {results: [{lastname: 'abc', firstName: '123'}, {lastname: 'abc', firstName: '123'}, {lastname: 'abc', firstName: '123'}]}
        beforeEach(() => {
       state = { contract: defaultContract, stableValueFunds: [{stableValueFundId: 1}, {stableValueFundId: 2}] }
      action = {
        type: 'LOAD_CONTRACT_CONTACTS_FULFILLED',
        payload: results
        }
      })
      it('should load contract contact  ', () => {
          const result = contractReducer(state, action)
          expect(result.contractContacts.length).toEqual(3)
      })
    })
    describe('SELECT_CONTACT', () => {
        let state, action
        beforeEach(() => {
       state = { contract: defaultContract }
      action = {
        type: 'SELECT_CONTACT',
        payload: {firstName: 'Lucia', lastname: 'DP'}
        }
    })
      it('should select contact  ', () => {
          const result = contractReducer(state, action)
          expect(result.newContact.firstName).toBe('Lucia')
      })
    })
    describe('SELECT_CONTRACT_CONTACT', () => {
        let state, action
        beforeEach(() => {
       state = { contract: defaultContract }
      action = {
        type: 'SELECT_CONTRACT_CONTACT',
        payload: {contractContact: defaultContract, contact: {firstName: 'Lucia', lastname: 'DP'}}
        }
    })
      it('should select contractContacts  ', () => {
          const result = contractReducer(state, action)
          expect(result.selectedContractContact).toEqual(defaultContract)
      })
    })
    describe('SELECT_AUDITABLE_CONTRACT_FIELD', () => {
        let state, action
        beforeEach(() => {
       state = { contract: defaultContract }
      action = {
        type: 'SELECT_AUDITABLE_CONTRACT_FIELD',
        selectedAuditableField: 'test'
        }
    })
      it('should select aditable field  ', () => {
          const result = contractReducer(state, action)
          expect(result.selectedAuditableField).toEqual('test')
      })
    })
    describe('LOAD_DERIVATIVE_TYPES_FULFILLED', () => {
        let state, action
        beforeEach(() => {
       state = { contract: defaultContract }
      action = {
        type: 'LOAD_DERIVATIVE_TYPES_FULFILLED',
        payload: {results: [ {}, {}, {} ]}
        }
    })
      it('should load derivate types  ', () => {
          const result = contractReducer(state, action)
          expect(result.derivativeTypeData.length).toEqual(3)
      })
    })
    describe('DELETE_CONTRACT_HISTORY', () => {
        let state, action
        beforeEach(() => {
            defaultContract.editHistories = [ { effectiveDate: '2018-04-26T00:00:00.000Z', field: 'test' }, {effectiveDate: '2018-02-03', field: 'noTest'} ]
       state = { contract: defaultContract }
      action = {
        type: 'DELETE_CONTRACT_HISTORY',
        effectiveDate: '2018-04-26T00:00:00.000Z',
        field: 'test'
        }
    })
      it('Delete contract history  ', () => {
          const result = contractReducer(state, action)
          expect(result.contract.editHistories[0].test).toEqual(null)
      })
    })
    describe('LOAD_CONTRACT_CREDITING_RATES_FULFILLED', () => {
        let state, action
        beforeEach(() => {
            defaultContract.editHistories = [ { effectiveDate: '2018-04-26T00:00:00.000Z', field: 'test' }, {effectiveDate: '2018-02-03', field: 'noTest'} ]
       state = { contract: defaultContract }
      action = {
        type: 'LOAD_CONTRACT_CREDITING_RATES_FULFILLED',
        payload: [{}, {}, {}]
        }
    })
      it('load contract crediting rate', () => {
          const result = contractReducer(state, action)
          expect(result.creditingRates.length).toEqual(3)
      })
    })
    describe('LOAD_CONTRACT_RISK_SCORECARDS_FULFILLED', () => {
        let state, action
        beforeEach(() => {
            defaultContract.editHistories = [ { effectiveDate: '2018-04-26T00:00:00.000Z', field: 'test' }, {effectiveDate: '2018-02-03', field: 'noTest'} ]
       state = { contract: defaultContract }
      action = {
        type: 'LOAD_CONTRACT_RISK_SCORECARDS_FULFILLED',
        payload: [{}, {}, {}]
        }
    })
      it('load riskScorecards', () => {
          const result = contractReducer(state, action)
          expect(result.riskScorecards.length).toEqual(3)
      })
    })

    describe('SELECT_RISK_SCORECARD_INPUT', () => {
        let state, action
        beforeEach(() => {
            defaultContract.editHistories = [ { effectiveDate: '2018-04-26T00:00:00.000Z', field: 'test' }, {effectiveDate: '2018-02-03', field: 'noTest'} ]
       state = { contract: defaultContract }
      action = {
        type: 'SELECT_RISK_SCORECARD_INPUT',
        payload: { riskScorecardId: 123 }
        }
    })
      it('select risk Score card', () => {
          const result = contractReducer(state, action)
          expect(result.riskScorecardId).toEqual(123)
      })
    })
    describe('DELETE_RISK_SCORECARD', () => {
        let state, action
        beforeEach(() => {
            defaultContract.editHistories = [ { effectiveDate: '2018-04-26T00:00:00.000Z', field: 'test' }, {effectiveDate: '2018-02-03', field: 'noTest'} ]
       state = { riskScorecards: [ { riskScorecardId: 123 }, {} ] }
      action = {
        type: 'DELETE_RISK_SCORECARD',
        payload: { riskScorecardId: 123 }
        }
    })
      it('delete risk Score card', () => {
          const result = contractReducer(state, action)
          expect(result.riskScorecards.length).toEqual(1)
      })
    })
    describe('SAVE_RISK_SCORECARD_INPUTS_FULFILLED', () => {
        let state, action
        beforeEach(() => {
            defaultContract.editHistories = [ { effectiveDate: '2018-04-26T00:00:00.000Z', field: 'test' }, {effectiveDate: '2018-02-03', field: 'noTest'} ]
       state = { riskScorecards: [ { riskScorecardId: 123 }, {} ] }
      action = {
        type: 'SAVE_RISK_SCORECARD_INPUTS_FULFILLED',
        payload: { riskScorecardId: 123 }
        }
    })
      it('SAVE_RISK  Score card', () => {
          const result = contractReducer(state, action)
          expect(result.riskScorecards.length).toEqual(2)
      })
    })
    describe('SELECT_CREDITING_RATE', () => {
        let state, action
        beforeEach(() => {
            defaultContract.editHistories = [ { effectiveDate: '2018-04-26T00:00:00.000Z', field: 'test' }, {effectiveDate: '2018-02-03', field: 'noTest'} ]
       state = { riskScorecards: [ { riskScorecardId: 123 }, {} ] }
      action = {
        type: 'SELECT_CREDITING_RATE',
        selectedCreditingRate: {},
        editCreditingRate: {},
        calculateCreditingRateAfterSavingEffectiveDate: false

        }
    })
      it('select risk  Score card', () => {
          const result = contractReducer(state, action)
          expect(result.selectedCreditingRate).toEqual({})
      })
    })

    describe('NET_CREDITING_RATE_ADJUSTED', () => {
        let state, action
        beforeEach(() => {
            defaultContract.editHistories = [ { effectiveDate: '2018-04-26T00:00:00.000Z', field: 'test' }, {effectiveDate: '2018-02-03', field: 'noTest'} ]
       state = { editCreditingRate: [ { creditingRateId: 123 }, { creditingRateId: 124 } ] }
      action = {
        type: 'NET_CREDITING_RATE_ADJUSTED',
        netCreditingRate: {},
        isNetCreditingRateLessThanMinimum: false

        }
    })
      it('net crediting rate adj', () => {
          const result = contractReducer(state, action)
          expect(result.editCreditingRate.isNetCreditingRateLessThanMinimum).toEqual(false)
      })
    })

    describe('GET_BALANCE_FOR_REFERENCE_DATE_FULFILLED', () => {
        let state, action
        beforeEach(() => {
            const results = { results: { array: [{}, {}, {}], total: 3 } }
       state = { editCreditingRate: [ { creditingRateId: 123 }, { creditingRateId: 124 } ] }
      action = {
        type: 'GET_BALANCE_FOR_REFERENCE_DATE_FULFILLED',
        payload: results
        }
    })
      it('get balance', () => {
          const result = contractReducer(state, action)
     //     expect(result).toEqual(3)
      })
    })
})
