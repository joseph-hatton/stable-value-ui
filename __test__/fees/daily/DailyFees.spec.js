import React from 'react'
import mockComponent from '../../MockComponent'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'

import DailyFeesFilter from '../../../src/fees/daily/DailyFeesFilter'
import {DailyFeesColumns} from '../../../src/fees/daily/DailyFeesColumns'
import {Grid, CreateGridConfig} from '../../../src/components/grid'
import {selectDailyFees, setContractId, filterDailyFees} from '../../../src/fees/FeeActions'

import DailyFees from '../../../src/fees/daily/DailyFees'

jest.mock('react-redux', () => ({
  connect: jest.fn(() => jest.fn(component => component))
}))
jest.mock('../../../src/fees/FeeActions', () => ({
  selectDailyFees: jest.fn(),
  setContractId: jest.fn(),
  filterDailyFees: jest.fn()
}))
jest.mock('../../../src/fees/daily/DailyFeesFilter', () => mockComponent('DailyFeesFilter'))
jest.mock('../../../src/fees/daily/DailyFeesColumns', () => ({
  DailyFeesColumns: () => []
}))
jest.mock('../../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn().mockReturnValue({})
}))

describe('Daily Fees', () => {
  let props
  beforeEach(() => {
    props = {
      loadedFees: [],
      params: {contractId: 123},
      selectDailyFees: jest.fn(),
      setContractId: jest.fn(),
      filterDailyFees: jest.fn()
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<DailyFees {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should call selectDailyFees', () => {
    props.filtered = false

    const component = renderer.create(<DailyFees {...props} />)

    expect(props.selectDailyFees.mock.calls.length).toBe(1)
    expect(props.filterDailyFees.mock.calls.length).toBe(0)
  })
  it('should call selectDailyFees', () => {
    props.filtered = true

    const component = renderer.create(<DailyFees {...props} />)

    expect(props.selectDailyFees.mock.calls.length).toBe(0)
    expect(props.filterDailyFees.mock.calls.length).toBe(1)
  })
})
