import {currencyFormat, dateFormat} from '../../../src/components/grid'

import {DailyFeesColumns} from '../../../src/fees/daily/DailyFeesColumns'

jest.mock('../../../src/components/grid', () => ({
  currencyFormat: 'currencyFormat',
  dateFormat: 'dateFormat'
}))

describe('DailyFeesColumns', () => {
  let cols
  beforeAll(() => {
    cols = DailyFeesColumns()
  })
  it('should call currencyFormat', () => {
    const currencyFormatCols = [
      'beginningFee',
      'accruedFee',
      'accruedFeeMtd',
      'feeReceived',
      'feeAdjustments',
      'endingFee'
    ]

    currencyFormatCols.forEach((col) => {
      const element = cols.find((i) => i.field === col)
      expect(element.renderer).toBe('currencyFormat')
    })
  })
  it('should call dateFormat', () => {
    const currencyFormatCols = [
      'effectiveDate'
    ]

    currencyFormatCols.forEach((col) => {
      const element = cols.find((i) => i.field === col)
      expect(element.renderer).toBe('dateFormat')
    })
  })
  it('should comma delimit invoices', () => {
    const element = cols.find((i) => i.field === 'invoiceNumbers')

    const result = element.renderer(['1', '2', '3'])

    expect(result).toBe('1, 2, 3')
  })
})
