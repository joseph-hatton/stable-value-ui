import http from '../../src/actions/http'
import moment from 'moment'

import {apiPath} from '../../src/config'
import {
  loadAllMonthlyFees,
  selectDailyFees,
  filterMonthly,
  filterDailyFees,
  setContractId,
  clearContractId,
  setDateFilter,
  setFilterOnDay
} from '../../src/fees/FeeActions'

let mockGet

jest.mock('../../src/actions/http', () => {
  return {
    get: jest.fn()
  }
})

describe('FeeActions', () => {
  let dispatch, getState, state
  beforeEach(() => {
    state = {fees: {currentContractId: 321, filterOnDay: false, dateFilter: moment()}}
    http.get.mockClear()
    dispatch = jest.fn()
    getState = jest.fn(() => state)
  })
  it('loadAllMonthlyFees should return action', () => {
    const res = loadAllMonthlyFees()

    expect(res.type).toBe('LOAD_ALL_FEES')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/balances/monthly`)
  })
  it('selectDailyFees should return action with passed in contractId', () => {
    selectDailyFees(123)(dispatch, getState)

    const res = dispatch.mock.calls[0][0]

    expect(res.type).toBe('SELECT_FEES_BY_CONTRACT_ID')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/balances/daily`)
    expect(http.get.mock.calls[0][1]).toEqual({contractId: 123})
  })
  it('selectDailyFees should return action with contractId in state', () => {
    selectDailyFees()(dispatch, getState)

    const res = dispatch.mock.calls[0][0]

    expect(res.type).toBe('SELECT_FEES_BY_CONTRACT_ID')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/balances/daily`)
    expect(http.get.mock.calls[0][1]).toEqual({contractId: 321})
  })
  it('filterMonthly should return action without filterOnDay', () => {
    filterMonthly()(dispatch, getState)

    const res = dispatch.mock.calls[0][0]

    expect(res.type).toBe('FILTER_ALL_FEES')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/balances/monthly`)
    expect(http.get.mock.calls[0][1]).toEqual({dateFilter: state.fees.dateFilter.format()})
  })
  it('filterMonthly should return action with filterOnDay', () => {
    state.fees.filterOnDay = true
    filterMonthly()(dispatch, getState)

    const res = dispatch.mock.calls[0][0]

    expect(res.type).toBe('FILTER_ALL_FEES')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/balances/monthly`)
    expect(http.get.mock.calls[0][1]).toEqual({dateFilter: state.fees.dateFilter.format(), filterOnDay: true})
  })
  it('filterDailyFees should return action', () => {
    filterDailyFees()(dispatch, getState)

    const res = dispatch.mock.calls[0][0]

    expect(res.type).toBe('FILTER_FEES_BY_CONTRACT_ID')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/balances/daily`)
    expect(http.get.mock.calls[0][1]).toEqual({contractId: 321, dateFilter: state.fees.dateFilter.format()})
  })
  it('setContractId should return action', () => {
    const res = setContractId(123)

    expect(res.type).toBe('SET_CONTRACT_ID')
    expect(res.payload).toBe(123)
  })
  it('clearContractId should return action', () => {
    const res = clearContractId()

    expect(res.type).toBe('SET_CONTRACT_ID')
    expect(res.payload).toBe(null)
  })
  it('setDateFilter should return action', () => {
    const res = setDateFilter('date')

    expect(res.type).toBe('SET_DATE_FILTER')
    expect(res.payload).toBe('date')
  })
  it('setFilterOnDay should return action', () => {
    const res = setFilterOnDay('day')

    expect(res.type).toBe('SET_FILTER_ON_DAY')
    expect(res.payload).toBe('day')
  })
})
