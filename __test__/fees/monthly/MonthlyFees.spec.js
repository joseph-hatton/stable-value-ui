import React from 'react'
import mockComponent from '../../MockComponent'
import renderer from 'react-test-renderer'

import MonthlyFeesFilter from '../../../src/fees/monthly/MonthlyFeesFilter'
import {MonthlyFeeColumns} from '../../../src/fees/monthly/MonthlyFeeColumns'
import {Grid, CreateGridConfig} from '../../../src/components/grid'
import {loadAllMonthlyFees} from '../../../src/fees/FeeActions'

import MonthlyFees from '../../../src/fees/monthly/MonthlyFees'

jest.mock('react-redux', () => ({
  connect: jest.fn(() => jest.fn(component => component))
}))
jest.mock('../../../src/fees/FeeActions', () => ({
  loadAllMonthlyFees: jest.fn()
}))
jest.mock('../../../src/fees/monthly/MonthlyFeesFilter', () => mockComponent('DailyFeesFilter'))
jest.mock('../../../src/fees/monthly/MonthlyFeeColumns', () => ({
  MonthlyFeeColumns: () => []
}))
jest.mock('../../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn().mockReturnValue({})
}))

describe('Monthly Fees', () => {
  let props
  beforeEach(() => {
    props = {
      loadedFees: [],
      params: {contractId: 123},
      loadAllMonthlyFees: jest.fn()
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<MonthlyFees {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should call loadAllMonthlyFees if not filtered', () => {
    props.filtered = false

    const component = renderer.create(<MonthlyFees {...props} />)

    expect(props.loadAllMonthlyFees.mock.calls.length).toBe(1)
  })
  it('should call loadAllMonthlyFees if filtered', () => {
    props.filtered = true

    const component = renderer.create(<MonthlyFees {...props} />)

    expect(props.loadAllMonthlyFees.mock.calls.length).toBe(0)
  })
})
