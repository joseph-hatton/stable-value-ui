import React from 'react'
import mockComponent from '../../MockComponent'
import renderer from 'react-test-renderer'
import moment from 'moment'

import {change, reset} from 'redux-form'
import {connect} from 'react-redux'
import {filterMonthly, loadAllMonthlyFees, setDateFilter, setFilterOnDay} from '../../../src/fees/FeeActions'
import YearFormField from '../../../src/components/forms/YearFormField'
import MonthFormField from '../../../src/components/forms/MonthFormField'
import DayFormField from '../../../src/components/forms/DayFormField'

import MonthlyFeesFilter from '../../../src/fees/monthly/MonthlyFeesFilter'

jest.mock('react-redux', () => ({
  connect: jest.fn(() => jest.fn(component => component))
}))
jest.mock('redux-form', () => ({
  change: jest.fn(),
  reset: jest.fn(),
  reduxForm: () => (comp) => comp
}))
jest.mock('../../../src/fees/FeeActions', () => ({
  filterMonthly: jest.fn(),
  setDateFilter: jest.fn(),
  setFilterOnDay: jest.fn(),
  loadAllMonthlyFees: jest.fn()
}))

jest.mock('../../../src/components/forms/YearFormField', () => mockComponent('YearFormField'))
jest.mock('../../../src/components/forms/MonthFormField', () => mockComponent('MonthFormField'))
jest.mock('../../../src/components/forms/DayFormField', () => mockComponent('DayFormField'))
jest.mock('material-ui/RaisedButton', () => mockComponent('RaisedButton'))

jest.mock('material-ui/Card', () => ({
  Card: mockComponent('Card')
}))

jest.mock('material-ui/Toolbar', () => ({
  Toolbar: mockComponent('Toolbar'),
  ToolbarGroup: mockComponent('ToolbarGroup')
}))

describe('Monthly Fees', () => {
  let props
  beforeEach(() => {
    props = {
      clearFilter: jest.fn(),
      yearChange: jest.fn(),
      monthChange: jest.fn(),
      yearFilter: 2017,
      monthFilter: 3
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<MonthlyFeesFilter {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  describe('actions', () => {
    let actions, dispatch, getState, state
    beforeEach(() => {
      const component = renderer.create(<MonthlyFeesFilter {...props} />)
      actions = connect.mock.calls[0][1]
      dispatch = jest.fn()
      state = {fees: {dateFilter: moment()}}
      getState = jest.fn().mockReturnValue(state)
      change.mockClear()
      reset.mockClear()
      setDateFilter.mockClear()
      setFilterOnDay.mockClear()
      loadAllMonthlyFees.mockClear()
      filterMonthly.mockClear()
    })
    it('clearFilter should dispatch', () => {
      actions.clearFilter()(dispatch, getState)

      expect(dispatch.mock.calls.length).toBe(4)
      expect(reset.mock.calls.length).toBe(1)
      expect(setDateFilter.mock.calls.length).toBe(1)
      expect(setFilterOnDay.mock.calls.length).toBe(1)
      expect(loadAllMonthlyFees.mock.calls.length).toBe(1)
    })
    it('clearFilter should reset filterDate', () => {
      state.fees.dateFilter.year(2016)
      state.fees.dateFilter.month(2)

      actions.clearFilter()(dispatch, getState)

      expect(setDateFilter.mock.calls[0][0].year()).toBe(moment().year())
      expect(setDateFilter.mock.calls[0][0].month()).toBe(moment().month())
    })
    it('yearChange should dispatch', () => {
      actions.yearChange({}, 2016)(dispatch, getState)

      expect(dispatch.mock.calls.length).toBe(3)
      expect(change.mock.calls.length).toBe(1)
      expect(setDateFilter.mock.calls.length).toBe(1)
      expect(setDateFilter.mock.calls[0][0].year()).toBe(2016)
      expect(filterMonthly.mock.calls.length).toBe(1)
    })
    it('monthChange should dispatch', () => {
      actions.monthChange({}, 3)(dispatch, getState)

      expect(dispatch.mock.calls.length).toBe(3)
      expect(change.mock.calls.length).toBe(1)
      expect(setDateFilter.mock.calls.length).toBe(1)
      expect(setDateFilter.mock.calls[0][0].month()).toBe(2)
      expect(filterMonthly.mock.calls.length).toBe(1)
    })
    it('dayChange should dispatch', () => {
      actions.dayChange({}, 20)(dispatch, getState)

      expect(dispatch.mock.calls.length).toBe(4)
      expect(change.mock.calls.length).toBe(1)
      expect(setDateFilter.mock.calls.length).toBe(1)
      expect(setDateFilter.mock.calls[0][0].date()).toBe(20)
      expect(filterMonthly.mock.calls.length).toBe(1)
      expect(setFilterOnDay.mock.calls.length).toBe(1)
    })
  })
  describe('mapStateToProps', () => {
    let mapStateToProps, dispatch, getState, state
    beforeEach(() => {
      const component = renderer.create(<MonthlyFeesFilter {...props} />)
      mapStateToProps = connect.mock.calls[0][0]
      state = {form: {MonthlyFeesFilterForm: {values: {yearFilter: 2016, monthFilter: 3}}}, fees: {dateFilter: moment()}}
    })
    it('should load correctly', () => {
      const result = mapStateToProps(state)

      expect(result.yearFilter).toBe(2016)
      expect(result.monthFilter).toBe(3)
      expect(result.initialValues.yearFilter).toBe(moment().year())
      expect(result.initialValues.monthFilter).toBe(moment().month() + 1)
    })
  })
})
