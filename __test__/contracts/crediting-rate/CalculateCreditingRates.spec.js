import {
  calculateNetCreditingRate
} from '../../../src/contracts/crediting-rate/CalculateCreditingRates'

describe('Calculate Crediting Rate', () => {
  it('Calculates Crediting Rate', () => {
    const newCreditingRate = calculateNetCreditingRate(2.79, -1.55, 12, 10, 10, 2)
    expect(newCreditingRate).toEqual(0.92)
  })
})
