import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from '../../MockComponent'
import CreditingRateMenu, {
  CreditingRatesMenuLink,
  mapStateToProps,
  CreditingRateMenus,
  validateAndCalculateCreditingRate
} from '../../../src/contracts/crediting-rate/CreditingRatesMenu'

jest.mock('react-redux', () => {
  return {
    connect: () => (component) => {
      return component
    }
  }
})

jest.mock('material-ui', () => {
  return {
    Popover: mockComponent('Popover'),
    Menu: mockComponent('Menu'),
    MenuItem: mockComponent('MenuItem')
  }
})

jest.mock('material-ui/svg-icons/navigation/apps', () => {
  return mockComponent('AppsIcon')
})

jest.mock('../../../src/contracts/crediting-rate/CreditingRatesRules', () => ({
  canDelete: jest.fn(),
  canViewCreditingRate: jest.fn(),
  canDeleteCreditingRate: jest.fn(),
  canApproveCreditingRate: jest.fn(),
  canCalculateCreditingRate: jest.fn(),
  canClearCreditingRate: jest.fn()
}))

describe('Crediting Rate Menu Sepcs', () => {
  it('renders Crediting Rate Menu', () => {
    const wrapper = renderer.create(<CreditingRateMenu closeDialog={jest.fn()} anchorEl={{name: 'link'}}
                                                       open={true}/>).toJSON()

    expect(wrapper).toMatchSnapshot()
  })

  it('closes the menu dialog on popover close callback', () => {
    const closeDialog = jest.fn()

    const wrapper = shallow(<CreditingRateMenu closeDialog={closeDialog} anchorEl={{name: 'link'}}
                                               open={true}/>)

    wrapper.find({id: 'CreditingRateMenu-Popover'}).prop('onRequestClose')()

    expect(closeDialog).toHaveBeenCalled()
  })

  it('renders Crediting Rate Menu Link', () => {
    const cell = 'Approved'

    const openDialog = jest.fn()

    const wrapper = renderer.create(
      <CreditingRatesMenuLink cell={cell} openDialog={openDialog}/>
    ).toJSON()

    expect(wrapper).toMatchSnapshot()
  })

  it('it opens Crediting Rates Menu on click', () => {
    const cell = 'Approved'
    const event = {
      currentTarget: 'Anchor Element',
      nativeEvent: {
        preventDefault: jest.fn()
      }
    }
    const openDialog = jest.fn()

    const wrapper = shallow(
      <CreditingRatesMenuLink cell={cell} openDialog={openDialog}/>
    )

    wrapper.find('a').simulate('click', event)

    expect(event.nativeEvent.preventDefault).toHaveBeenCalled()
    expect(openDialog).toBeCalledWith('creditingRateMenu', {
      anchorEl: event.currentTarget
    })
  })

  it('calls onClick of the Menu Item with all props, when Menu Item is clicked', () => {
    const cell = 'Approved'
    const addEditMarketValue = jest.fn()
    const closeDialog = jest.fn()

    const creditingRate = {}

    const wrapper = shallow(
      <CreditingRateMenu cell={cell} closeDialog={closeDialog} addEditMarketValue={addEditMarketValue}
                         open={true} creditingRate={creditingRate}/>
    )

    wrapper.find({primaryText: 'View/Edit Market Value'}).simulate('click')

    expect(addEditMarketValue).toHaveBeenCalledWith(creditingRate, false)
    expect(closeDialog).toHaveBeenCalledWith('creditingRateMenu')
  })

  it('map dialog props from state to props when dialog is opened', () => {
    const state = {
      dialog: {
        creditingRateMenu: {
          anchorEl: 'XYZ'
        }
      },
      contracts: {}
    }

    const props = mapStateToProps(state)

    expect(props.anchorEl).toEqual('XYZ')
  })

  it('map dialog props from state to props when dialog is closed', () => {
    const state = {
      dialog: false,
      contracts: {contract: 'contract'}
    }

    const props = mapStateToProps(state)

    expect(props.anchorEl).toBeFalsy()
    expect(props.open).toBe(false)
    expect(props.contract).toBe('contract')
  })
})

const getMenu = (label) => CreditingRateMenus.find(menu => menu.primaryText === label)

describe('Crediting Rate Menus', () => {
  it('View/Edit Market Value Menu opens AddEditMarketValue Dialog', () => {
    const ViewEditMarketValueMenu = getMenu('View/Edit Market Value')
    const addEditMarketValue = jest.fn()
    const creditingRate = {creditingRateId: 2878}

    ViewEditMarketValueMenu.onClick({creditingRate, addEditMarketValue})

    expect(addEditMarketValue).toHaveBeenCalledWith(creditingRate, false)
  })

  it('Delete Market Value onClick calls delete Market Value Action', () => {
    const DeleteMarketValueMenu = getMenu('Delete Market Value')
    const deleteMarketValue = jest.fn()
    const creditingRate = {creditingRateId: 2878}

    DeleteMarketValueMenu.onClick({creditingRate, deleteMarketValue})

    expect(deleteMarketValue).toHaveBeenCalledWith(creditingRate)
  })

  it('Calculate Crediting Rate onClick calls Calculate Crediting Rate Action', () => {
    const CalculateCreditingRateMenu = getMenu('Calculate Crediting Rate')
    const calculateCreditingRate = jest.fn()
    const creditingRate = {creditingRateId: 2878, effectiveDate: new Date()}

    CalculateCreditingRateMenu.onClick({creditingRate, calculateCreditingRate})

    expect(calculateCreditingRate).toHaveBeenCalledWith(creditingRate)
  })

  it('Calculate Crediting Rate onClick displays error if Effective Date is not set for Crediting Rate and Opens Market value dialog', () => {
    const calculateCreditingRate = jest.fn()
    const addEditMarketValue = jest.fn()

    const creditingRate = {creditingRateId: 2878}

    validateAndCalculateCreditingRate({creditingRate, addEditMarketValue, calculateCreditingRate})

    expect(addEditMarketValue).toHaveBeenCalledWith(creditingRate, true)

    expect(calculateCreditingRate).toHaveBeenCalledTimes(0)
  })
})
