import React from 'react'
import mockComponent from '../../MockComponent'
import moment from 'moment'
import renderer from 'react-test-renderer'
import {createReferenceDataTextRenderer} from '../../../src/components/renderers/ReferenceDataTextRenderer'
import {CreditingRatesMenuLink} from '../../../src/contracts/crediting-rate/CreditingRatesMenu'
import {dateFormat} from '../../../src/components/grid'
import {CreditingRatesColumns, CreditRatePercentFormat, percentFormatMK, currencyFormatMV, OptionalPercentFormat} from '../../../src/contracts/crediting-rate/CreditingRatesColumns'

jest.mock('../../../src/components/renderers/ReferenceDataTextRenderer', () => {
    return {createReferenceDataTextRenderer: jest.fn().mockImplementation(refDataKey => refDataKey)}
})

jest.mock('../../../src/components/grid', () => ({currencyFormat: 'currencyFormat', dateFormat: 'dateFormat', percentFormat: 'percentFormat'}))
const getRenderer = (field) => CreditingRatesColumns.find(column => column.field === field).renderer

describe('CreditingRatesColumns', () => {
    let cols
    beforeAll(() => {
        cols = CreditingRatesColumns
    })
    it('length', () => {
        expect(cols.length).toBe(12)
    })
    it('should call currencyFormatMV', () => {
        const currencyFormatCols = ['marketValue', 'bookValue']
            currencyFormatCols.forEach((col) => {
            const element = cols.find((i) => i.field === col)
            expect(element.renderer).toBe(currencyFormatMV)
        })
    })

    it('should call percentFormatMK', () => {
        const currencyFormatCols = ['mvbvRatio']
            currencyFormatCols.forEach((col) => {
            const element = cols.find((i) => i.field === col)
            expect(element.renderer).toBe(percentFormatMK)
        })
    })

    it('renders annualEffectiveYield', () => {
        const currencyFormatCols = ['annualEffectiveYield']
        currencyFormatCols.forEach((col) => {
            const element = cols.find((i) => i.field === col)
            element.renderer()
            expect(OptionalPercentFormat(2)).toBe('2.000%')
        })
    })
    it('renders duration', () => {
            const element = cols.find((i) => i.field === 'duration')
            const result = element.renderer(2.000000000)
            expect(result).toBe('2.00')
    })
})
