import ValidateMarketValue from '../../../src/contracts/crediting-rate/ValidateMarketValue'

describe('ValidateMarketValue', () => {
  let values, props
  beforeEach(() => {
    values = {
      creditingRateId: 321,
      referenceDate: '2016-01-15'
    }
    props = {
      creditingRates: [{
        creditingRateId: 123,
        referenceDate: '2016-01-15'
      }]
    }
  })
  it('should report an error if there is a crediting rate with the same reference date', () => {
    expect(ValidateMarketValue(values, props)).toEqual({
      referenceDate: 'Reference date must be unique.'
    })
  })
  it('should not report an error if there is not a crediting rate with the same reference date', () => {
    props.creditingRates[0].referenceDate = '2016-01-20'
    expect(ValidateMarketValue(values, props)).toEqual({})
  })
  it('should not report an error if the ids are the same', () => {
    props.creditingRates[0].creditingRateId = 321
    expect(ValidateMarketValue(values, props)).toEqual({})
  })
})
