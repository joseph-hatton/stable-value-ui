import React from 'react'
import renderer from 'react-test-renderer'
import mockConnect from '../../MockConnect'
import mockComponent from '../../MockComponent'
import {connect} from 'react-redux'
import {reduxForm, reduxFormChildFn, reduxedForm, touch} from 'redux-form'
import {
  AddEditMarketValue,
  mapStateToProps,
  actions,
  AddEditMarketValueForm
} from '../../../src/contracts/crediting-rate/AddEditMarketValue'
import {exists, Required, FormatCurrency} from '../../components/forms/Validators.spec'

jest.mock('react-redux', () => mockConnect())
jest.mock('../../../src/contracts/crediting-rate/CreditingRatesRules', () => ({
  canEdit: jest.fn(),
  dateFormat: 'dateFormat'
}))
jest.mock('../../../src/components/utilities/Formatters', () => ({toUtc: 'toUtc', dateFormat: 'dateFormat'}))
jest.mock('../../../src/contracts/crediting-rate/ValidateMarketValue', () => jest.fn())
jest.mock('material-ui/RaisedButton', () => mockComponent('RaisedButton'))
jest.mock('redux-form', () => {
  const reduxForm = jest.fn()
  const reduxedForm = jest.fn()
  const reduxFormChildFn = jest.fn()
  reduxForm.mockReturnValueOnce(reduxFormChildFn)
  reduxFormChildFn.mockReturnValueOnce(reduxedForm)

  return {
    Field: mockComponent('Field'),
    touch: jest.fn(),
    reduxForm,
    reduxFormChildFn,
    reduxedForm
  }
})
jest.mock('redux-form-material-ui', () => ({DatePicker: mockComponent('DatePicker')}))
jest.mock('../../../src/components/redux-dialog', () => mockComponent('ReduxDialog'))
jest.mock('../../../src/contracts/NumberField', () => mockComponent('NumberField'))
jest.mock('../../../src/contracts/ContractPicker', () => mockComponent('ContractPicker'))
jest.mock('../../../src/contracts/crediting-rate/ValidateMarketValue', () => 'ValidateMarketValue')
jest.mock('../../../src/contracts/crediting-rate/CreditingRatesActions', () => ({
  getBalanceForReferenceDate: jest.fn(),
clearAddEditMarketValue: jest.fn(),
  saveMarketValue: jest.fn(),
calculateAnnualEffectiveYield: jest.fn()
}))
jest.mock('../../../src/contracts/ContractForms', () => ({isEditable: 'isEditable'}))

describe('AddEditMarketValue', () => {
  it('should render correctly', () => {
    const props = {
      handleSubmit: jest.fn(),
      initialValues: {},
      bookValue: 234,
      createdDate: '',
      disabled: false,
      calculateCreditingRateAfterSavingEffectiveDate: false,
      getBalanceForReferenceDate: jest.fn(),
      clearAddEditMarketValue: jest.fn(),
      saveMarketValue: jest.fn(),
      calculateAnnualEffectiveYield: jest.fn(),
      touchEffectiveDateToTriggerValidation: jest.fn(),
      creditingRates: [{}, {}, {}],
      marketValueView: false,
      contractEffectiveDate: jest.fn(),
      referenceDate: jest.fn()
    }
    const component = renderer.create(<AddEditMarketValue {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
    expect(connect).toHaveBeenCalledTimes(1)
    expect(AddEditMarketValueForm).toBe(reduxedForm)
    expect(reduxFormChildFn).toHaveBeenCalledWith(AddEditMarketValue)
    expect(reduxForm).toHaveBeenCalledWith({
      form: 'AddEditMarketValueForm',
      validate: 'ValidateMarketValue'
    })
  })
})
describe('AddEditMarketValue actions', () => {
  let dispatch, getState, state
  beforeEach(() => {
    dispatch = jest.fn()
  })
  it('should call actions correctly', () => {
    actions.touchEffectiveDateToTriggerValidation()(dispatch)
    expect(touch).toHaveBeenCalled()
    expect(touch).toHaveBeenCalledWith('AddEditMarketValueForm', 'effectiveDate')
  })
})

describe('AddEditMarketValue mapStateToProps ', () => {
  it('mapStateToProps ', () => {
    const state = {
      contracts: {
        contract: {contractId: 123, effectiveDate: new Date()},
        creditingRates: [{}, {}, {}],
        selectedCreditingRate: {contractId: 123},
        balance: 123,
        calculateCreditingRateAfterSavingEffectiveDate: false,
        marketValueView: false
      }
    }
    const result = mapStateToProps(state)
    expect(result.creditingRates).toBe(state.contracts.creditingRates)
    expect(result.initialValues).toEqual({'contractId': 123})
  })
})
