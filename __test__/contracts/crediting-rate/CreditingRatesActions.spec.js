import { loadContractCreditingRates, addEditMarketValue, viewEditCreditingRate } from '../../../src/contracts/crediting-rate/CreditingRatesActions'
import http from '../../../src/actions/http'
import { apiPath } from '../../../src/config'
import {openDialog, openConfirmationAlert} from '../../../src/components/redux-dialog'
jest.mock('../../../src/components/redux-dialog', () => ({
    openDialog: jest.fn(),
    openConfirmationAlert: jest.fn()
}))

let mockCreditingRateRecord
let mockCreditingRateRecordWithUnknownStatus
let mockCreditingRateRecordWithEmptyBookValue

jest.mock('../../../src/actions/http', () => {
  return {
    get: jest.fn().mockImplementation(() => Promise.resolve({
      results: [mockCreditingRateRecord, mockCreditingRateRecordWithUnknownStatus, mockCreditingRateRecordWithEmptyBookValue]
    }))
  }
})

describe('Crediting Rates Actions Specs', () => {
    let dispatch, getState, state
    beforeEach(() => {
        state = {form: {}}
        dispatch = jest.fn()
        getState = jest.fn().mockReturnValue(state)
    })
  it('loads crediting rates', () => {
    mockCreditingRateRecord = {
      'creditingRateId': 6521,
      'contractId': 539,
      'annualEffectiveYield': 1.49,
      'duration': 2.97,
      'grossRate': 2.21,
      'rate': 2,
      'adjustment': null,
      'referenceDate': '2015-05-31T00:00:00.000Z',
      'effectiveDate': '2015-07-01T00:00:00.000Z',
      'status': 'APPROVED',
      'createdDate': '2014-08-01T14:20:54.052Z',
      'version': 5,
      'premiumRate': 0.21,
      'managerFeeRate': null,
      'advisorFeeRate': null,
      'creditingRateRoundDecimals': 2,
      'creditingRateCalcMethod': 'COMPOUND_AMORTIZATION_METHOD',
      'minimumNetCreditingRate': 0,
      'creditingRateType': 'NET',
      'netOfThirdPartyFees': false,
      'creditingRateManagerFee': false,
      'creditingRateAdvisorFee': false,
      'createdId': null,
      'modifiedDate': '2015-08-03T14:55:00.352Z',
      'modifiedId': null,
      'calculatedBy': 'Rogers, Chris',
      'bookValue': 33451283.97,
      'locked': true,
      'marketValue': 34163605.55,
      'creditingRateRateComments': null
    }
    mockCreditingRateRecordWithUnknownStatus = { status: 'Unknown', marketValue: 100, bookValue: 100 }
    mockCreditingRateRecordWithEmptyBookValue = { status: 'FORECAST', marketValue: 100, bookValue: undefined }

    const disptach = jest.fn().mockImplementation(action => action)
    const getState = jest.fn().mockImplementation(() => ({
      referenceData: {
        creditingRateStatuses: [
          {id: 'APPROVED', text: 'Approved'},
          {id: 'FORECAST', text: 'Forecast'}
        ]
      }
    }))

    loadContractCreditingRates(100)(disptach, getState)

    const action = disptach.mock.calls[0][0]

    expect(action.type).toEqual('LOAD_CONTRACT_CREDITING_RATES')
    expect(http.get).toHaveBeenCalledWith(`${apiPath}/contracts/100/crediting-rates`)

    return action.payload.then(creditingRates => {
      expect(creditingRates.length).toEqual(3)
      expect(creditingRates[0]).toEqual(
        {
          ...mockCreditingRateRecord,
          'statusText': 'Approved',
          'mvbvRatio': 1.0212942971229095
        }
      )
      expect(creditingRates[1]).toEqual(
        {
          ...mockCreditingRateRecordWithUnknownStatus,
          'mvbvRatio': 1,
          'statusText': 'Unknown'
        }
      )
      expect(creditingRates[2]).toEqual(
        {
          ...mockCreditingRateRecordWithEmptyBookValue,
          'statusText': 'Forecast',
          'mvbvRatio': null
        }
      )
    })
  })

    it('addEditMarketValue', async () => {
        const selectedCreditingRate = {annualEffectiveYield: 2.5, bookValue: 45741884.39, calculatedBy: null, contractId: 530}
        const calculateCreditingRateAfterSavingEffectiveDate = false
        addEditMarketValue(selectedCreditingRate, calculateCreditingRateAfterSavingEffectiveDate)(dispatch)
        const result = dispatch.mock.calls[0][0]
        expect(result).toEqual({'calculateCreditingRateAfterSavingEffectiveDate': false, 'selectedCreditingRate': {'annualEffectiveYield': 2.5, 'bookValue': 45741884.39, 'calculatedBy': null, 'contractId': 530}, 'type': 'SELECT_CREDITING_RATE'})
    })

    it('viewEditCreditingRate ', async () => {
        const selectedCreditingRate = {annualEffectiveYield: 2.5, bookValue: 45741884.39, calculatedBy: null, contractId: 530}
        viewEditCreditingRate(selectedCreditingRate)(dispatch, getState)
        const result = dispatch.mock.calls[0][0]
        expect(result.type).toEqual('SELECT_CREDITING_RATE')
        expect(result.selectedCreditingRate).toEqual({'annualEffectiveYield': 2.5, 'bookValue': 45741884.39, 'calculatedBy': null, 'contractId': 530})
        expect(result.editCreditingRate).toEqual({'canEditGrossRate': true, 'netCreditingRate': undefined})
    })
})
