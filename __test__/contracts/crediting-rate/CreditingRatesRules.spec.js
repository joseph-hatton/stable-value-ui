import {hasEntitlement} from '../../../src/entitlements'

import {
  canAdd,
  canEdit,
  canDelete,
  canViewCreditingRate,
  canEditCreditingRate,
  canClearCreditingRate,
  canApproveCreditingRate,
  canCalculateCreditingRate,
  canCalculateCreditingRateWithoutEffectiveDate
} from '../../../src/contracts/crediting-rate/CreditingRatesRules'

const mockState = {}

jest.mock('../../../src/store/store', () => ({
  getState: () => mockState
}))

jest.mock('../../../src/entitlements', () => ({
  hasEntitlement: jest.fn()
}))

let props = {
  contract: {},
  creditingRate: {}
}

describe('CreditingRatesRules', () => {
  describe('canAdd', () => {
    it('should evaluate correctly', () => {
      hasEntitlement.mockReturnValue(true)

      expect(canAdd('ACTIVE')).toBe(true)
    })
    it('should evaluate correctly', () => {
      hasEntitlement.mockReturnValue(false)

      expect(canAdd('ACTIVE')).toBe(false)
    })
    it('should evaluate correctly', () => {
      hasEntitlement.mockReturnValue(true)

      expect(canAdd('NOT_ACTIVE')).toBe(false)
    })
  })
  describe('canEdit', () => {
    beforeEach(() => {
      hasEntitlement.mockReturnValue(true)
      props.contract.status = 'ACTIVE'
      props.creditingRate.status = 'AWAITING'
      props.creditingRate.creditingRateId = 555
    })
    it('should allow edit when adding new crediting rate', () => {
      expect(canEdit({...props, creditingRate: {}})).toBe(true)
    })
    it('should evaluate correctly', () => {
      expect(canEdit(props)).toBe(true)
    })
    it('should evaluate correctly', () => {
      props.creditingRate.status = 'FORECAST'

      expect(canEdit(props)).toBe(true)
    })
    it('should evaluate correctly', () => {
      props.creditingRate.status = 'NEITHER'

      expect(canEdit(props)).toBe(false)
    })
    it('should evaluate correctly', () => {
      props.contract.status = 'NOT_ACTIVE'

      expect(canEdit(props)).toBe(false)
    })
  })
  describe('canDelete', () => {
    it('should be the same as canEdit', () => {
      expect(canEdit).toBe(canDelete)
    })
  })
  describe('canViewCreditingRate', () => {
    beforeEach(() => {
      hasEntitlement.mockReturnValue(true)
    })
    it('canViewCreditingRate should evaluate correctly', () => {
      expect(canViewCreditingRate('PENDING')).toBe(true)
      expect(canViewCreditingRate('APPROVED')).toBe(true)
    })
    it('canViewCreditingRate should evaluate correctly', () => {
      expect(canViewCreditingRate('AWAITING')).toBe(false)
    })
    it('canViewCreditingRate should evaluate correctly', () => {
      expect(canViewCreditingRate('FORECAST')).toBe(false)
    })
  })
  describe('canEditCreditingRate', () => {
    beforeEach(() => {
      hasEntitlement.mockReturnValue(true)
      mockState.currentUser = {id: 123}
      props.creditingRate.status = 'PENDING'
      props.creditingRate.modifiedId = 123
      props.contract.status = 'ACTIVE'
    })
    it('should enable edit crediting rate if the rate status is Pending', () => {
      expect(canEditCreditingRate(props)).toBe(true)
    })
    it('should disable edit crediting rate if the rate status is Forecast', () => {
      props.creditingRate.status = 'FORECAST'

      expect(canEditCreditingRate(props)).toBe(false)
    })
    it('should disable edit crediting rate if entitlement is invalid', () => {
      hasEntitlement.mockReturnValue(false)

      expect(canEditCreditingRate(props)).toBe(false)
    })
    it('should disable edit crediting rate if user is different than the one calculated it', () => {
      mockState.currentUser = {id: 321}
      props.creditingRate.status = 'PENDING'

      expect(canEditCreditingRate(props)).toBe(false)
    })
  })
  describe('canClearCreditingRate', () => {
    beforeEach(() => {
      hasEntitlement.mockReturnValue(true)
      props.contract.status = 'ACTIVE'
      props.creditingRate.status = 'PENDING'
      props.creditingRate.locked = false
    })
    it('should evaluate correctly', () => {
      expect(canClearCreditingRate(props)).toBe(true)
    })
    it('should evaluate correctly', () => {
      props.contract.status = 'NOT_ACTIVE'

      expect(canClearCreditingRate(props)).toBe(false)
    })
    it('should evaluate correctly', () => {
      hasEntitlement.mockReturnValue(false)

      expect(canClearCreditingRate(props)).toBe(false)
    })
    it('should evaluate correctly', () => {
      props.creditingRate.status = 'AWAITING'

      expect(canClearCreditingRate(props)).toBe(false)
    })
    it('should evaluate correctly', () => {
      props.creditingRate.status = 'FORECAST'

      expect(canClearCreditingRate(props)).toBe(false)
    })
    it('should evaluate correctly', () => {
      props.creditingRate.locked = true

      expect(canClearCreditingRate(props)).toBe(false)
    })
  })
  describe('canApproveCreditingRate', () => {
    beforeEach(() => {
      props.contract.status = 'ACTIVE'
      hasEntitlement.mockReturnValue(true)
      props.creditingRate.status = 'PENDING'
      props.creditingRate.modifiedId = 123
      mockState.currentUser = {id: 321}
    })
    it('should evaluate correctly', () => {
      expect(canApproveCreditingRate(props)).toBe(true)
    })
    it('should evaluate correctly', () => {
      props.contract.status = 'NOT_ACTIVE'

      expect(canApproveCreditingRate(props)).toBe(false)
    })
    it('should evaluate correctly', () => {
      hasEntitlement.mockReturnValue(false)

      expect(canApproveCreditingRate(props)).toBe(false)
    })
    it('should evaluate correctly', () => {
      props.creditingRate.status = 'NOT_PENDING'

      expect(canApproveCreditingRate(props)).toBe(false)
    })
    it('should evaluate correctly', () => {
      props.creditingRate.modifiedId = 321

      expect(canApproveCreditingRate(props)).toBe(false)
    })
  })
})

describe('canCalculateCreditingRate', () => {
  beforeEach(() => {
    props.contract.status = 'ACTIVE'
    hasEntitlement.mockReturnValue(true)
    props.creditingRate.status = 'AWAITING'
    props.creditingRate.marketValue = 2434234.323
  })

  it('can calculate crediting rate for awaiting status', () => {
    expect(canCalculateCreditingRate(props)).toBe(true)
  })

  it('can calculate crediting rate for forecast status', () => {
    props.creditingRate.status = 'FORECAST'

    expect(canCalculateCreditingRate(props)).toBe(true)
  })

  it('cannot calculate crediting rate if market value is not defined.', () => {
    props.creditingRate.marketValue = null

    expect(canCalculateCreditingRate(props)).toBe(false)
  })

  it('cannot calculate crediting rate if not entitled.', () => {
    hasEntitlement.mockReturnValue(false)

    expect(canCalculateCreditingRate(props)).toBe(false)
  })

  it('cannot calculate crediting rate from market value tab if effective Date is not defined.', () => {
    expect(canCalculateCreditingRateWithoutEffectiveDate(props)).toBe(false)
  })

  it('can calculate crediting rate from market value tab if effective Date is defined.', () => {
    props.creditingRate.marketValue = new Date()

    expect(canCalculateCreditingRateWithoutEffectiveDate(props)).toBe(false)
  })
})
