import createRefDataFormFieldComponent from '../../src/components/forms/createRefDataFormFieldComponent'
import mockComponent from '../MockComponent'

import * as contractRefDataFields from '../../src/contracts/ContractRefDataFields'

jest.mock('../../src/components/forms/createRefDataFormFieldComponent', () => jest.fn((label, data) => ({label, data})))

const itemsToRun = [
  {method: 'AdditionalDeposits', label: 'additionalDeposits', text: 'Additional Deposit'},
  {method: 'CreditingRateCalcMethods', label: 'creditingRateCalculationMethods', text: '* Calculation Method'},
  {method: 'CalculationMethodInputDefinitions', label: 'calculationMethodInputDefinitions', text: '* Calculation Method Input Definition'},
  {method: 'CreditingRateRoundingDecimals', label: 'creditingRateRoundingDecimals', text: '* Rounding'},
  {method: 'CreditingRateInputBases', label: 'creditingRateInputBases', text: '* Input Basis'},
  {method: 'CreditingRateFrequencies', label: 'creditingRateFrequencies', text: '* Frequency'},
  {method: 'CreditingRateMonths', label: 'creditingRateMonths', text: '* First Month'},
  {method: 'CreditingRateTypes', label: 'creditingRateTypes', text: '* Crediting Rate'},
  {method: 'SPRatings', label: 'planSponsorRatings', text: '* S&P Rating'},
  {method: 'FitchRatings', label: 'planSponsorRatings', text: '* Fitch'},
  {method: 'MoodyRatings', label: 'planSponsorMoodyRatings', text: '* Corridor Option Limits-Calculation Method'},
  {method: 'OverallRatings', label: 'planSponsorOverallRatings', text: '* Overall Rating'},
  {method: 'Sectors', label: 'sectors', text: 'Sector'},
  {method: 'CorridorOptionCalculationMethods', label: 'corridorOptionCalculationMethods', text: '* Corridor Option Limits-Calculation Method'},
  {method: 'AdviceServiceRemedy', label: 'adviceServiceRemedies', text: 'Advice Service Remedy'}
]

describe('ContractRefDataFields', () => {
  itemsToRun.forEach(({method, label, text}) => {
    it(`${method} should call createRefDataFormFieldComponent with the correct params`, () => {
      expect(contractRefDataFields[method]).toEqual({
        data: {
          floatingLabelText: text,
          type: 'text'
        },
        label
      })
      expect(createRefDataFormFieldComponent).toHaveBeenCalledWith(label, {
        floatingLabelText: text,
        type: 'text'
      })
    })
  })
})
