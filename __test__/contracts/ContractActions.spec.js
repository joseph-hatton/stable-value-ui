import {apiPath} from '../../src/config'
import http from '../../src/actions/http'
import {closeDialog, openSnackbar, openConfirmationAlert} from '../../src/components/redux-dialog/redux-dialog'
import {destroy, change, initialize} from 'redux-form'
import { SPECIFICATIONS, BASE_INFO } from '../../src/contracts/ContractForms'
import {createContractSavePayload} from '../../src/contracts/ContractPayloadMapper'

import {
  toEditHistory,
  loadAllContracts,
  initializeContract,
  setCurrentContractForm,
  saveContract,
  updateFormStateWithNewEditHistory,
  removeEditHistory,
  loadContract,
  jurisdictionSelected,
  planAnniversaryMonthChanged,
  planAnniversaryDayChanged,
  loadModelInputs,
  saveModelInput,
  selectModelInputRow,
  deleteModelInputRow,
  clearModelInputEditForm,
  prepRiskScorecard,
  saveRiskScorecardInput,
  selectRiskScorecardInputRow,
  clearRiskScorecardInputEditForm,
  gradeRiskScorecard,
  deleteRiskScorecard,
  saveStableValueFund,
  loadStableValueFunds,
  selectStableValueFund,
  selectStableValueFundRow,
  deleteStableValueFundRow,
  clearStableValueFundEditForm,
  activateUnderwritingTab,
  loadContractContacts,
  selectContact,
  selectContactContract,
  assignContactToContract,
  unAssignContactFromContract,
  standardErrorMessageRetriever,
  updateContractStatus,
  loadDerivativeTypes,
  hasEditHistory,
  selectAuditableField,
  clearAuditableFieldChange,
  deleteContractEditHistory,
  canNavigateAwayFromContracts,
  isClean,
  areEditHistoriesEqual,
  askToNotSaveContract
} from '../../src/contracts/ContractActions'

jest.mock('../../src/actions/http', () => ({
  get: jest.fn(),
  post: jest.fn(),
  put: jest.fn(),
  delete: jest.fn()
}))

jest.mock('redux-form', () => ({
  destroy: jest.fn().mockReturnValue('destroy'),
  change: jest.fn().mockReturnValue('change'),
  initialize: jest.fn().mockReturnValue('initialize')
}))

jest.mock('../../src/contracts/ContractPayloadMapper', () => ({
  createNewContractSavePayload: jest.fn((form) => form),
  createContractSavePayload: jest.fn((contract, form, derivativeTypes) => ({contract, form, derivativeTypes}))
}))

jest.mock('../../src/components/redux-dialog/redux-dialog', () => ({
  closeDialog: jest.fn().mockReturnValue('closeDialog'),
  openConfirmationAlert: jest.fn().mockReturnValue('openConfirmationAlert'),
  openSnackbar: jest.fn().mockReturnValue('openSnackbar')
}))

jest.mock('../../src/contracts/ContractForms', () => ({
  getEffectiveDateProp: (i) => `getEffectiveDateProp(${i})`
}))

const CREATE_CONTRACT_URL = `${apiPath}/contracts`
const CONTRACT_SAVED_MESSAGE = {
  successMessage: 'Contract Saved Successfully'
}
const MODEL_INPUT_SAVED_MESSAGE = {
  successMessage: 'Model Input Saved Successfully'
}
const CONTRACT_RISK_SCORECARD_INPUT_SAVED_MESSAGE = {
  successMessage: 'Risk Scorecard Input Saved Successfully'
}
const CONTRACT_RISK_SCORECARD_INPUT_UPDATED_MESSAGE = {
  successMessage: 'Risk Scorecard Input Updated Successfully'
}
const CONTRACT_RISK_SCORECARD_INPUT_DELETED_MESSAGE = {
  successMessage: 'Risk Scorecard Input Deleted Successfully'
}
const CONTRACT_STABLE_VALUE_SAVED_MESSAGE = {
  successMessage: 'Contract Stable Value Successfully'
}
const STABLE_VALUE_FUND_DELETED_MESSAGE = {
  successMessage: 'Stable Value Fund Deleted Successfully'
}
const CONTRACT_CONTACT_ASSIGNED_MESSAGE = {
  successMessage: 'Contact Assigned Successfully'
}
const CONTRACT_CONTACT_UNASSIGNED_MESSAGE = {
  successMessage: 'Contact Unassigned Successfully'
}
const CONTRACT_STATUS_UPDATE_MESSAGE = {
  successMessage: 'Contract Status updated successfully',
  errorMessage: standardErrorMessageRetriever
}
describe('ContractActions', () => {
  let dispatch, getState, state
  beforeEach(() => {
    dispatch = jest.fn()
    state = {contracts: {}, referenceData: {}}
    getState = jest.fn().mockReturnValue(state)
    http.get.mockClear()
    http.post.mockClear()
    http.put.mockClear()
    http.delete.mockClear()
    closeDialog.mockClear()
    openSnackbar.mockClear()
    openConfirmationAlert.mockClear()
    destroy.mockClear()
    change.mockClear()
    initialize.mockClear()
  })
  it('loadAllContracts should dispatch correctly with get response of /contracts', async () => {
    state.referenceData = {contractStatuses: [{id: '1', text: 'text1'}, {id: '2', text: 'text2'}]}
    http.get.mockReturnValue(Promise.resolve({results: [{test: 'test', status: '2'}]}))

    loadAllContracts()(dispatch, getState)

    expect(dispatch.mock.calls[0][0].type).toBe('LOAD_ALL_CONTRACTS')
    expect(await dispatch.mock.calls[0][0].payload).toEqual({
      results: [{
        test: 'test',
        statusText: 'text2',
        status: '2'
      }]
    })
  })
  it('initializeContract should return action to init contract', async () => {
    expect(initializeContract().type).toBe('INITIALIZE_CONTRACT')
  })
  it('setCurrentContractForm should return action with the form id', async () => {
    const result = setCurrentContractForm(123)
    expect(result.type).toBe('SET_CURRENT_CONTRACT_FORM')
    expect(result.contractFormId).toBe(123)
  })
  describe('saveContract', () => {
    beforeEach(() => {
      state.contracts = {contract: {}}
      state.form = 'form'
      state.referenceData = {derivativeTypes: 'derivativeTypes'}
    })
    it('should call createNewContractMapper and save it if there is no contractId', () => {
      http.post.mockReturnValue('posting...')

      saveContract()(dispatch, getState)

      expect(dispatch.mock.calls[0][0].type).toBe('SAVE_CONTRACT')
      expect(dispatch.mock.calls[0][0].payload).toBe('posting...')
      expect(http.post).toHaveBeenCalledWith(CREATE_CONTRACT_URL, 'form', CONTRACT_SAVED_MESSAGE)
    })
    it('should call createContractSavePayload and save it if status is pending', () => {
      state.contracts.contract.contractId = 321
      state.contracts.contract.contractNumber = 123
      state.contracts.contract.status = 'PENDING'
      http.put.mockReturnValue('putting...')

      saveContract()(dispatch, getState)

      expect(dispatch.mock.calls[0][0].type).toBe('SAVE_CONTRACT')
      expect(dispatch.mock.calls[0][0].payload).toBe('putting...')
      expect(http.put).toHaveBeenCalledWith(`${CREATE_CONTRACT_URL}/321`, {
        contract: state.contracts.contract,
        derivativeTypes: 'derivativeTypes',
        form: 'form'
      }, CONTRACT_SAVED_MESSAGE)
    })
    it('should call createContractSavePayload and save it if status is active', () => {
      state.contracts.contract.contractId = 321
      state.contracts.contract.contractNumber = 123
      state.contracts.contract.status = 'ACTIVE'
      http.put.mockReturnValue('putting...')

      saveContract()(dispatch, getState)

      expect(dispatch.mock.calls[0][0].type).toBe('SAVE_CONTRACT')
      expect(dispatch.mock.calls[0][0].payload).toBe('putting...')
      expect(http.put).toHaveBeenCalledWith(`${CREATE_CONTRACT_URL}/321`, {
        contract: state.contracts.contract,
        derivativeTypes: 'derivativeTypes',
        form: 'form'
      }, CONTRACT_SAVED_MESSAGE)
    })
    it('should show error message if status isn\'t pending or active', () => {
      state.contracts.contract.contractNumber = 123
      state.contracts.contract.status = 'NONE OF THE ABOVE'

      saveContract()(dispatch, getState)

      expect(dispatch.mock.calls[0][0]).toBe('openSnackbar')
      expect(openSnackbar.mock.calls[0][0]).toEqual({
        type: 'error',
        message: `Cannot modify contract in NONE OF THE ABOVE Status.`,
        autoHideDuration: 5000
      })
    })
  })
  it('updateFormStateWithNewEditHistory should update field state and effective date state', () => {
    state.contracts = {contract: {}}
    updateFormStateWithNewEditHistory('formId', 'field', dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(3)
    expect(change).toHaveBeenCalledTimes(3)
    expect(change).toHaveBeenCalledWith('formId', 'field', '')
    expect(change).toHaveBeenCalledWith('formId', 'getEffectiveDateProp(field)', null)
  })
  it('updateFormStateWithNewEditHistory should update field state if set and effective date state', () => {
    state.contracts = {contract: {field: 'field'}}
    updateFormStateWithNewEditHistory('formId', 'field', dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(3)
    expect(change).toHaveBeenCalledTimes(3)
    expect(change).toHaveBeenCalledWith('formId', 'field', 'field')
    expect(change).toHaveBeenCalledWith('formId', 'getEffectiveDateProp(field)', null)
  })
  it('removeEditHistory should save contract and update the form state', async () => {
    state.contracts = {contract: {contractId: 123}}
    http.put.mockReturnValue(Promise.resolve('result'))

    removeEditHistory('formId', 'field')(dispatch, getState)

    expect(dispatch.mock.calls[0][0].type).toBe('SAVE_CONTRACT')
    expect(await dispatch.mock.calls[0][0].payload).toBe('result')
    expect(http.put).toHaveBeenCalledWith(`${CREATE_CONTRACT_URL}/123`, {contractId: 123}, CONTRACT_SAVED_MESSAGE)
  })
  it('loadContract should load contract and its contacts', async () => {
    http.get.mockImplementation((url) => Promise.resolve(url))

    loadContract(123)(dispatch)

    expect(dispatch.mock.calls[0][0].type).toBe('LOAD_CONTRACT')
    expect(await dispatch.mock.calls[0][0].payload).toEqual([
      `${apiPath}/contracts/123`,
      `${apiPath}/contracts/123/contacts`
    ])
  })
  it('jurisdictionSelected should dispatch assignmentState if it is falsy', () => {
    state.form = {specifications: {values: { assignmentState: null }}}
    jurisdictionSelected(null, 'value')(dispatch, getState)
    expect(dispatch).toHaveBeenCalledWith('change')
    expect(change).toHaveBeenCalledTimes(1)
    expect(change).toHaveBeenCalledWith(SPECIFICATIONS, 'assignmentState', 'value')
  })
  it('jurisdictionSelected should not dispatch assignmentState if it is truthy', () => {
    state.form = {specifications: {values: { assignmentState: 'val' }}}
    jurisdictionSelected(null, 'value')(dispatch, getState)
    expect(dispatch).not.toHaveBeenCalled()
    expect(change).not.toHaveBeenCalled()
  })
  it('planAnniversaryMonthChanged should dispatch planAnniversary', () => {
    state.form = {baseInfo: {values: {planAnniversaryDay: 'planAnniversaryDay'}}}
    planAnniversaryMonthChanged(null, 'value')(dispatch, getState)
    expect(dispatch).toHaveBeenCalled()
    expect(change).toHaveBeenCalledWith(BASE_INFO, 'planAnniversary', 'value planAnniversaryDay')
  })
  it('planAnniversaryDayChanged should dispatch planAnniversary', () => {
    state.form = {baseInfo: {values: {planAnniversaryMonth: 'planAnniversaryMonth'}}}
    planAnniversaryDayChanged(null, 'value')(dispatch, getState)
    expect(dispatch).toHaveBeenCalled()
    expect(change).toHaveBeenCalledWith(BASE_INFO, 'planAnniversary', 'planAnniversaryMonth value')
  })
  it('loadModelInputs should dispatch load model inputs', () => {
    http.get.mockReturnValue('get')
    loadModelInputs(123)(dispatch)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'LOAD_MODEL_INPUTS',
      payload: 'get'
    })
    expect(http.get).toHaveBeenCalledWith(`${apiPath}/contracts/123/underwriting`)
  })
  it('saveModelInputs should dispatch save model inputs with put when underwriting id is set', () => {
    state.form = {AddEditModelInputForm: {values: {test: 'test', underwritingId: 321}}}
    state.contracts = {contract: {contractId: 123}}
    http.put.mockReturnValue('put')
    saveModelInput()(dispatch, getState)
    expect(http.put).toHaveBeenCalledWith(`${apiPath}/contracts/123/underwriting/321`, {
      test: 'test',
      underwritingId: 321,
      contractId: 123
    }, MODEL_INPUT_SAVED_MESSAGE)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SAVE_MODEL_INPUTS',
      payload: 'put'
    })
  })
  it('saveModelInputs should dispatch save model inputs with post when underwriting id is not set', () => {
    state.form = {AddEditModelInputForm: {values: {test: 'test'}}}
    state.contracts = {contract: {contractId: 123}}
    http.post.mockReturnValue('post')
    saveModelInput()(dispatch, getState)
    expect(http.post).toHaveBeenCalledWith(`${apiPath}/contracts/123/underwriting`, {
      test: 'test',
      contractId: 123
    }, MODEL_INPUT_SAVED_MESSAGE)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SAVE_MODEL_INPUTS',
      payload: 'post'
    })
  })
  it('selectModelInputRow should dispatch select model input', () => {
    expect(selectModelInputRow('modelInput')).toEqual({
      type: 'SELECT_MODEL_INPUT',
      payload: 'modelInput'
    })
  })
  it('deleteModelInputRow should dispatch delete model input', () => {
    http.delete.mockReturnValue('delete')
    deleteModelInputRow({underwritingId: 123, contractId: 321})(dispatch, getState)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'DELETE_MODEL_INPUT',
      payload: 'delete'
    })
    expect(http.delete).toHaveBeenCalledWith(`${apiPath}/contracts/321/underwriting/123`, null, {
      successMessage: 'Model Input Deleted Successfully'
    })
  })
  it('clearModelInputEditForm should dispatch destroy on the form and clear select model input', () => {
    clearModelInputEditForm()(dispatch)
    expect(dispatch).toHaveBeenCalledWith('destroy')
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SELECT_MODEL_INPUT'
    })
  })
  it('prepRiskScorecard should set planSponsor, planSvFundBalances and planDesign to score 0 w/ POOLED_PLAN', () => {
    const rs = {values: {}}
    prepRiskScorecard(rs, 'POOLED_PLAN')
    const score = {score: 0}
    expect(rs.values).toEqual({
      planSponsor: score,
      planSvFundBalances: score,
      planDesign: score
    })
  })
  it('prepRiskScorecard should set pooledFundBalances and pooledFundDesign to score 0 w/o POOLED_PLAN', () => {
    const rs = {values: {}}
    prepRiskScorecard(rs, 'NOT_POOLED_PLAN')
    const score = {score: 0}
    expect(rs.values).toEqual({
      pooledFundBalances: score,
      pooledFundDesign: score
    })
  })
  it('saveRiskScorecardInput should dispatch update risk scorecard with put if risk scorecard id is set', () => {
    state.form = {RiskScorecardAddEdit: {values: {}}}
    state.contracts = {
      contract: {contractId: 321, contractType: 'contractType'},
      riskScorecard: {riskScorecardId: 123}
    }
    http.put.mockReturnValueOnce({then: (cb) => cb('then(put)')}) // eslint-disable-line standard/no-callback-literal
    saveRiskScorecardInput({riskScorecardId: 123})(dispatch, getState)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SAVE_RISK_SCORECARD_INPUTS',
      payload: 'then(put)'
    })
    expect(http.put).toHaveBeenCalledWith(`${apiPath}/contracts/321/risk-scorecards/123`, {
      contractId: 321,
      pooledFundBalances: {score: 0},
      pooledFundDesign: {score: 0}
    }, CONTRACT_RISK_SCORECARD_INPUT_UPDATED_MESSAGE, 'riskScorecardInput')
  })
  it('saveRiskScorecardInput should dispatch save risk scorecard with post if risk scorecard id is not set', () => {
    state.form = {RiskScorecardAddEdit: {values: {}}}
    state.contracts = {
      contract: {contractId: 321, contractType: 'contractType'},
      riskScorecard: {}
    }
    http.post.mockReturnValue({then: (cb) => cb('then(post)')}) // eslint-disable-line standard/no-callback-literal
    saveRiskScorecardInput({})(dispatch, getState)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SAVE_RISK_SCORECARD_INPUTS',
      payload: 'then(post)'
    })
    expect(http.post).toHaveBeenCalledWith(`${apiPath}/contracts/321/risk-scorecards`, {
      contractId: 321,
      pooledFundBalances: {score: 0},
      pooledFundDesign: {score: 0}
    }, CONTRACT_RISK_SCORECARD_INPUT_SAVED_MESSAGE, 'riskScorecardInput')
  })
  it('selectRiskScorecardInputRow should return action with type select risk scorecard and payload risk scorecard', () => {
    expect(selectRiskScorecardInputRow('riskScorecard')).toEqual({
      type: 'SELECT_RISK_SCORECARD_INPUT',
      payload: 'riskScorecard'
    })
  })
  it('clearRiskScorecardInputEditForm should destroy form and clear select risk scorecard', () => {
    clearRiskScorecardInputEditForm()(dispatch)
    expect(dispatch).toHaveBeenCalledWith('destroy')
    expect(destroy).toHaveBeenCalledWith('RiskScorecardAddEdit')
  })
  it('gradeRiskScorecard should get grade form api and dispatch form and select with the new values', async () => {
    state.contracts = {contract: {contractId: 123, contractType: 'contractType'}}
    state.form = {RiskScorecardAddEdit: {values: {riskScorecardId: 321, test: {score: 56}}}}
    http.put.mockReturnValue(Promise.resolve({test: 'test'}))
    await gradeRiskScorecard({target: {name: 'test.1.2', value: 58}})(dispatch, getState)
    expect(http.put).toHaveBeenCalledWith(`${apiPath}/contracts/123/risk-scorecards/calculate-adjusted`, {
      pooledFundBalances: {score: 0},
      pooledFundDesign: {score: 0},
      contractType: 'contractType',
      test: {score: 58},
      riskScorecardId: 321
    })
    expect(initialize).toHaveBeenCalledWith('RiskScorecardAddEdit', {
      test: 'test',
      riskScorecardId: 321
    })
    expect(dispatch).toHaveBeenCalledWith('initialize')
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SELECT_RISK_SCORECARD_INPUT',
      payload: {
        test: 'test',
        riskScorecardId: 321
      }
    })
  })
  it('deleteRiskScorecard should delete then dispatch delete risk scorecard and clear out select risk scorecard input', async () => {
    http.delete.mockReturnValue(Promise.resolve('data'))
    await deleteRiskScorecard({contractId: 123, riskScorecardId: 321})(dispatch)
    expect(http.delete).toHaveBeenCalledWith(`${apiPath}/contracts/123/risk-scorecards/321`, null, CONTRACT_RISK_SCORECARD_INPUT_DELETED_MESSAGE)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'DELETE_RISK_SCORECARD',
      payload: 'data'
    })
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SELECT_RISK_SCORECARD_INPUT'
    })
  })
  it('saveStableValueFund should save stable value fund with put if stable value fund id is set', () => {
    state.form = {AddEditStableValueFundForm: {values: {stableValueFundId: 321}}}
    state.contracts = {contract: {contractId: 123}}
    http.put.mockReturnValue('put')
    saveStableValueFund()(dispatch, getState)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SAVE_STABLE_VALUE_FUND',
      payload: 'put'
    })
    expect(http.put).toHaveBeenCalledWith(`${apiPath}/contracts/123/stable-value-funds/321`, {
      contractId: 123,
      stableValueFundId: 321
    }, CONTRACT_STABLE_VALUE_SAVED_MESSAGE, 'stableValueFund')
  })
  it('saveStableValueFund should save stable value fund with post if stable value fund id is not set', () => {
    state.form = {AddEditStableValueFundForm: {values: {}}}
    state.contracts = {contract: {contractId: 123}}
    http.post.mockReturnValue('post')
    saveStableValueFund()(dispatch, getState)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SAVE_STABLE_VALUE_FUND',
      payload: 'post'
    })
    expect(http.post).toHaveBeenCalledWith(`${apiPath}/contracts/123/stable-value-funds`, {
      contractId: 123
    }, CONTRACT_STABLE_VALUE_SAVED_MESSAGE, 'stableValueFund')
  })
  it('loadStableValueFunds should dispatch load stable value funds', () => {
    loadStableValueFunds(123)(dispatch)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'LOAD_STABLE_VALUE_FUNDS',
      payload: 'get'
    })
    expect(http.get).toHaveBeenCalledWith(`${apiPath}/contracts/123/stable-value-funds`)
  })
  it('selectStableValueFund should dispatch select stable value fund', () => {
    state.contracts = {stableValueFunds: [
      {stableValueFundId: 123},
      {stableValueFundId: 321}
    ]}
    selectStableValueFund(123)(dispatch, getState)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SELECT_STABLE_VALUE_FUND',
      payload: {stableValueFundId: 123}
    })
  })
  it('selectStableValueFundRow should dispatch select stable value fund', () => {
    expect(selectStableValueFundRow('stableValueFund')).toEqual({
      type: 'SELECT_STABLE_VALUE_FUND',
      payload: 'stableValueFund'
    })
  })
  it('deleteStableValueFundRow should dispatch delete stable value fund', () => {
    http.delete.mockReturnValue('delete')
    deleteStableValueFundRow({contractId: 123, stableValueFundId: 321})(dispatch)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'DELETE_STABLE_VALUE_FUND',
      payload: 'delete'
    })
    expect(http.delete).toHaveBeenCalledWith(`${apiPath}/contracts/123/stable-value-funds/321`, null, STABLE_VALUE_FUND_DELETED_MESSAGE)
  })
  it('clearStableValueFundEditForm should destroy the form and the select stable value fund', () => {
    clearStableValueFundEditForm()(dispatch)
    expect(dispatch).toHaveBeenCalledWith('destroy')
    expect(destroy).toHaveBeenCalledWith('AddEditStableValueFundForm')
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SELECT_STABLE_VALUE_FUND'
    })
  })
  it('activateUnderwritingTab should return action activate underwriting tab and label', () => {
    expect(activateUnderwritingTab('label')).toEqual({
      type: 'ACTIVATE_UNDERWRITING_TAB',
      label: 'label'
    })
  })
  it('loadContractContacts should dispatch load contract contacts', () => {
    loadContractContacts(123)(dispatch)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'LOAD_CONTRACT_CONTACTS',
      payload: 'get'
    })
    expect(http.get).toHaveBeenCalledWith(`${apiPath}/contracts/123/contacts`)
  })
  it('selectContact should select contact', () => {
    expect(selectContact({contact: 'contact'})).toEqual({
      type: 'SELECT_CONTACT',
      payload: 'contact'
    })
  })
  it('selectContactContract should dispatch select contract contact with contact if contractContact is set', () => {
    state.contacts = {allContacts: [
      {contactId: 123},
      {contactId: 321}
    ]}
    selectContactContract({contactId: 123})(dispatch, getState)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SELECT_CONTRACT_CONTACT',
      payload: {
        contractContact: {contactId: 123},
        contact: {contactId: 123}
      }
    })
  })
  it('selectContactContract should dispatch select contract contact without contact if contractContact is not set', () => {
    state.contacts = {allContacts: [
      {contactId: 123},
      {contactId: 321}
    ]}
    selectContactContract()(dispatch, getState)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SELECT_CONTRACT_CONTACT',
      payload: {
        contact: null
      }
    })
  })
  it('assignContactToContract should connect the contact the contract with put if contact contract id is set', async () => {
    state.form = {AssignContact: {values: {contactContractId: 123, contractId: 321}}}
    http.put.mockReturnValue('put')
    dispatch.mockReturnValue(Promise.resolve('response'))
    expect(await assignContactToContract()(dispatch, getState)).toBe('response')
    expect(dispatch).toHaveBeenCalledTimes(3)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'ASSIGN_CONTRACT_CONTACT',
      payload: 'put'
    })
    expect(dispatch).toHaveBeenCalledWith('closeDialog')
    expect(closeDialog).toHaveBeenCalledWith('assignContactToContract')
    expect(http.put).toHaveBeenCalledWith(`${apiPath}/contracts/321/contacts/123`, {
      contactContractId: 123,
      contractId: 321
    }, CONTRACT_CONTACT_ASSIGNED_MESSAGE)
  })
  it('assignContactToContract should connect the contact the contract with post if contact contract id is not set', async () => {
    state.form = {AssignContact: {values: {contractId: 321}}}
    http.post.mockReturnValue('post')
    dispatch.mockReturnValue(Promise.resolve('response'))
    expect(await assignContactToContract()(dispatch, getState)).toBe('response')
    expect(dispatch).toHaveBeenCalledTimes(3)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'ASSIGN_CONTRACT_CONTACT',
      payload: 'post'
    })
    expect(dispatch).toHaveBeenCalledWith('closeDialog')
    expect(closeDialog).toHaveBeenCalledWith('assignContactToContract')
    expect(http.post).toHaveBeenCalledWith(`${apiPath}/contracts/321/contacts`, {
      contractId: 321
    }, CONTRACT_CONTACT_ASSIGNED_MESSAGE)
  })
  it('unAssignContactFromContract should delete the relationship', async () => {
    http.delete.mockReturnValue('delete')
    dispatch.mockReturnValue(Promise.resolve('response'))
    expect(await unAssignContactFromContract(123, 321)(dispatch)).toBe('response')
    expect(dispatch).toHaveBeenCalledTimes(2)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'UNASSIGN_CONTRACT_CONTACT',
      payload: 'delete'
    })
    expect(http.delete).toHaveBeenCalledWith(`${apiPath}/contracts/123/contacts/321`, {}, CONTRACT_CONTACT_UNASSIGNED_MESSAGE)
  })
  it('standardErrorMessageRetriever should get message from error', () => {
    expect(standardErrorMessageRetriever({response: {body: {message: 'message'}}})).toBe('message')
  })
  it('updateContractStatus should update contract status with path', () => {
    http.put.mockReturnValue('put')
    state.contracts = {contract: {contractId: 123}}
    updateContractStatus('path')(dispatch, getState)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'UPDATE_CONTRACT_STATUS',
      payload: 'put'
    })
    expect(http.put).toHaveBeenCalledWith(`${apiPath}/contracts/123/path`, {}, CONTRACT_STATUS_UPDATE_MESSAGE)
  })
  it('loadDerivativeTypes should load derivatve types from api', () => {
    http.get.mockReturnValue('get')
    loadDerivativeTypes()(dispatch)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'LOAD_DERIVATIVE_TYPES',
      payload: 'get'
    })
    expect(http.get).toHaveBeenCalledWith(`${apiPath}/derivative-types`)
  })
  it('hasEditHistory to evaluate to true if active', () => {
    expect(hasEditHistory({status: 'ACTIVE'})).toBeTruthy()
  })
  it('hasEditHistory to evaluate to false if not active', () => {
    expect(hasEditHistory({status: 'TERMINATED'})).toBeFalsy()
  })
  it('hasEditHistory to evaluate to false if no contract', () => {
    expect(hasEditHistory()).toBeFalsy()
  })
  it('toEditHistory should not remove manager rates set to 0', () => {
    const editHistories = [
      {
        'contractEditHistoryId': 1581,
        'effectiveDate': '2016-04-01T00:00:00.000Z',
        'managerFeeRate': 8
      },
      {
        'contractEditHistoryId': 1684,
        'effectiveDate': '2016-06-01T00:00:00.000Z',
        'managerFeeRate': 0
      }
    ]

    const result = toEditHistory('managerFeeRate', {editHistories}, [])

    expect(result).toEqual([
      {
        'effectiveDate': '2016-04-01T00:00:00.000Z',
        'value': 8,
        'future': false
      },
      {
        'active': true,
        'effectiveDate': '2016-06-01T00:00:00.000Z',
        'value': 0,
        'future': false
      }
    ])
  })
  it('selectAuditableField should dispatch select auditable contract field', () => {
    state.contracts = {contract: 'contract'}
    state.contacts = {managers: 'managers'}
    selectAuditableField('formId', 'field', 'label')(dispatch, getState)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SELECT_AUDITABLE_CONTRACT_FIELD',
      selectedAuditableField: {
        formId: 'formId',
        field: 'field',
        label: 'label',
        editHistory: []
      }
    })
  })
  it('clearAuditableFieldChange should change field and effective date field if originalValue is set', () => {
    state.contracts = {contract: {field: 'field'}}
    clearAuditableFieldChange('formId', 'field')(dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(2)
    expect(dispatch).toHaveBeenCalledWith('change')
    expect(change).toHaveBeenCalledWith('formId', 'field', 'field')
    expect(change).toHaveBeenCalledWith('formId', 'fieldEffectiveDate', null)
  })
  it('clearAuditableFieldChange should clear field and effective date field if originalValue is not set', () => {
    state.contracts = {contract: {}}
    clearAuditableFieldChange('formId', 'field')(dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(2)
    expect(dispatch).toHaveBeenCalledWith('change')
    expect(change).toHaveBeenCalledWith('formId', 'field', '')
    expect(change).toHaveBeenCalledWith('formId', 'fieldEffectiveDate', null)
  })
  it('deleteContractEditHistory should return action delete contract history', () => {
    expect(deleteContractEditHistory('effectiveDate', 'field')).toEqual({
      type: 'DELETE_CONTRACT_HISTORY',
      effectiveDate: 'effectiveDate',
      field: 'field'
    })
  })
  it('isClean should return false if there are differences between the new data and the old data', () => {
    const contract = {
      test: 'old'
    }
    createContractSavePayload.mockReturnValue({
      test: 'new'
    })
    expect(isClean({contract})).toBe(false)
  })
  it('isClean should return true if there are no differences between the new data and the old data', () => {
    const contract = {
      test: 'new'
    }
    createContractSavePayload.mockReturnValue({
      test: 'new'
    })
    expect(isClean({contract})).toBe(true)
  })
  it('isClean should null out blanks for comparison so that \'\' is equal to null', () => {
    const contract = {
      test: null
    }
    createContractSavePayload.mockReturnValue({
      test: ''
    })
    expect(isClean({contract})).toBe(true)
  })
  it('isClean should sort derivativeTypes because order doesn\'t matter', () => {
    const contract = {
      derivativeTypes: [
        {derivativeTypeId: 123},
        {derivativeTypeId: 321}
      ]
    }
    createContractSavePayload.mockReturnValue({
      derivativeTypes: [
        {derivativeTypeId: 321},
        {derivativeTypeId: 123}
      ]
    })
    expect(isClean({contract})).toBe(true)
  })
  it('canNavigateAwayFromContracts should return true if contract is not loaded', () => {
    createContractSavePayload.mockReturnValue({})
    state.contracts = {}
    state.form = {}
    state.referenceData = {derivativeTypes: []}
    expect(canNavigateAwayFromContracts()(dispatch, getState)).toBe(true)
  })
  it('canNavigateAwayFromContracts should return true if contract is loaded but isClean is true', () => {
    createContractSavePayload.mockReturnValue({})
    state.contracts = {contract: {}}
    state.form = {}
    state.referenceData = {derivativeTypes: []}
    expect(canNavigateAwayFromContracts()(dispatch, getState)).toBe(true)
  })
  it('canNavigateAwayFromContracts should return false if contract is loaded but isClean is false', () => {
    createContractSavePayload.mockReturnValue({test: 'test'})
    state.contracts = {contract: {}}
    state.form = {}
    state.referenceData = {derivativeTypes: []}
    expect(canNavigateAwayFromContracts()(dispatch, getState)).toBe(false)
  })
  it('areEditHistoriesEqual should loop through new and compare those values to old', () => {
    const oldEditHistories = [{
      premiumRate: 18,
      assignmentState: 'IL',
      jurisdiction: 'IL',
      test: 'other'
    }]
    const newEditHistories = [{
      premiumRate: 18,
      assignmentState: 'IL',
      jurisdiction: 'IL'
    }]
    expect(areEditHistoriesEqual(oldEditHistories, newEditHistories)).toBe(true)
  })
  it('areEditHistoriesEqual should return true if they are equal', () => {
    const oldEditHistories = [{
      premiumRate: 18,
      assignmentState: 'IL',
      jurisdiction: 'IL',
      test: 'other'
    }]
    const newEditHistories = [{
      premiumRate: 18,
      assignmentState: 'IL',
      jurisdiction: 'IL'
    }]
    expect(areEditHistoriesEqual(oldEditHistories, newEditHistories)).toBe(true)
  })
  it('areEditHistoriesEqual should return false if they are not equal', () => {
    const oldEditHistories = [{
      premiumRate: 17,
      assignmentState: 'IL',
      jurisdiction: 'IL',
      test: 'other'
    }]
    const newEditHistories = [{
      premiumRate: 18,
      assignmentState: 'IL',
      jurisdiction: 'IL'
    }]
    expect(areEditHistoriesEqual(oldEditHistories, newEditHistories)).toBe(false)
  })
  describe('askToNotSaveContract', () => {
    let router
    beforeEach(() => {
      router = {
        push: jest.fn()
      }
      state.contracts.contract = {}
    })
    it('should return true if canLeaveWithoutSaving is true and reseLeaveWithoutSaving should be called', () => {
      state.contracts.canLeaveWithoutSaving = true
      expect(askToNotSaveContract(router)(dispatch, getState)()).toBeTruthy()
      expect(dispatch.mock.calls[0][0]).toEqual({
        type: 'RESET_LEAVE_WITHOUT_SAVING'
      })
    })
    it('should return false if canLeaveWithoutSaving is false and open dialog', () => {
      state.contracts.canLeaveWithoutSaving = false
      expect(askToNotSaveContract(router)(dispatch, getState)()).toBeFalsy()
      expect(dispatch).toHaveBeenCalledWith('openConfirmationAlert')
      expect(openConfirmationAlert).toHaveBeenCalled()
      expect(openConfirmationAlert.mock.calls[0][0].message).toEqual('Your changes are not saved. Leaving this contract will erase your changes.')
      expect(openConfirmationAlert.mock.calls[0][1]).toEqual('navigateAwayFromContract')
    })
    it('onOk should set canLeaveWithoutSaving to true and redirect', () => {
      askToNotSaveContract(router)(dispatch, getState)('nextLocation')
      openConfirmationAlert.mock.calls[0][0].onOk()
      expect(dispatch).toHaveBeenCalledWith({
        type: 'LEAVE_WITHOUT_SAVING'
      })
      expect(router.push).toHaveBeenCalledWith('nextLocation')
    })
  })
})
