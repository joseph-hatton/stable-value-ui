import React from 'react'
import mockConnect from '../MockConnect'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import {connect} from 'react-redux'
import {shallow} from 'enzyme'

import ContractPicker from '../../src/contracts/ContractPicker'

jest.useFakeTimers()

jest.mock('react-redux', () => mockConnect())
jest.mock('../../src/contracts/ContractActions', () => ({
  loadAllContracts: 'loadAllContracts',
  loadContract: 'loadContract'
}))
jest.mock('../../src/components/forms/MaterialSelectField', () => mockComponent('MaterialSelectField'))

describe('ContractPicker', () => {
  let props
  beforeEach(() => {
    props = {
      contracts: [],
      loadAllContracts: jest.fn(),
      loadContract: jest.fn()
    }
    setTimeout.mockClear()
  })
  it('should render correctly', () => {
    const comp = renderer.create(<ContractPicker {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('should loadAllContracts if contracts is empty', () => {
    props.contracts = []
    const comp = renderer.create(<ContractPicker {...props} />)
    expect(setTimeout).toHaveBeenCalledTimes(1)
    setTimeout.mock.calls[0][0]()
    expect(props.loadAllContracts).toHaveBeenCalled()
  })
  it('should not loadAllContracts if contracts aren\'t empty', () => {
    props.contracts = [{}]
    const comp = renderer.create(<ContractPicker {...props} />)
    expect(setTimeout).toHaveBeenCalledTimes(0)
    expect(props.loadAllContracts).not.toHaveBeenCalled()
  })
  it('should load the contract if load flag set and there is a selected contract id', () => {
    props.loadSelectedContract = 'loadSelectedContract'
    props.selectedContractId = 'selectedContractId'
    const comp = renderer.create(<ContractPicker {...props} />)
    expect(props.loadContract).toHaveBeenCalledWith('selectedContractId')
  })
  describe('loadContract', () => {
    it('should call loadContract if loadSelectedContract is truthy', () => {
      props.loadSelectedContract = true
      const comp = shallow(<ContractPicker {...props} />)
      comp.prop('onChange')('event', 'contractId')
      expect(props.loadContract).toHaveBeenCalled()
    })
    it('should not call loadContract if loadSelectedContract is falsy', () => {
      props.loadSelectedContract = false
      const comp = shallow(<ContractPicker {...props} />)
      comp.prop('onChange')('event', 'contractId')
      expect(props.loadContract).not.toHaveBeenCalled()
    })
    it('should call onChange if onChange is set', () => {
      props.onChange = jest.fn()
      const comp = shallow(<ContractPicker {...props} />)
      comp.prop('onChange')('event', 'contractId')
      expect(props.onChange).toHaveBeenCalledWith('event', 'contractId')
    })
  })
  it('Options are setup correctly when filterContract is not set', () => {
    props.contracts = [{contractNumber: 'contractNumber', shortPlanName: 'shortPlanName', status: 'status', contractId: 'contractId'}]
    const comp = shallow(<ContractPicker {...props} />)
    expect(comp.prop('options')).toEqual([{
      text: 'contractNumber (shortPlanName)  (status)',
      value: 'contractId'
    }])
  })
  it('Options are setup correctly when filterContract is set', () => {
    props.contracts = [
      {contractNumber: 'contractNumber', shortPlanName: 'shortPlanName', status: 'status', contractId: 'contractId'},
      {contractNumber: 'contractNumber', shortPlanName: 'shortPlanName', status: 'status', contractId: 123}
    ]
    props.filterContract = (i) => i.contractId !== 123
    const comp = shallow(<ContractPicker {...props} />)
    expect(comp.prop('options')).toEqual([{
      text: 'contractNumber (shortPlanName)  (status)',
      value: 'contractId'
    }])
  })
  it('mapStateToProps should map results if set', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][0]({contracts: {allContracts: {results: 'results'}}})).toEqual({
      contracts: 'results'
    })
  })
  it('mapStateToProps should map an empty array if result is not set', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][0]({contracts: {allContracts: {}}})).toEqual({
      contracts: []
    })
  })
  it('actions should setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][0]({contracts: {allContracts: {}}})).toEqual({
      contracts: []
    })
  })
})
