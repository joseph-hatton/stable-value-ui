import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'
import mockConnect from '../../MockConnect'
import {shallow} from 'enzyme'
import {isEditable} from '../../../src/contracts/ContractForms'
import {connect} from 'react-redux'

import ModelInputs from '../../../src/contracts/underwriting/ModelInputs'

jest.mock('react-redux', () => mockConnect())

jest.mock('../../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn().mockReturnValue('options')
}))

jest.mock('../../../src/contracts/ContractForms', () => ({
  isEditable: jest.fn().mockReturnValue(true)
}))

jest.mock('../../../src/contracts/underwriting/ModelInputColumns', () => ({
  ModelInputColumns: 'ModelInputColumns'
}))

jest.mock('../../../src/contracts/ContractActions', () => ({
  loadModelInputs: 'loadModelInputs',
  saveModelInput: 'saveModelInput',
  clearModelInputEditForm: 'clearModelInputEditForm',
  selectModelInputRow: 'selectModelInputRow'
}))

jest.mock('../../../src/watchLists/watchListAlert/WatchListAlertActions', () => ({
  checkWatchList: 'checkWatchList'
}))

jest.mock('../../../src/components/redux-dialog/redux-dialog', () => ({
  openDialog: 'openDialog',
  closeDialog: 'closeDialog'
}))

jest.mock('material-ui/Menu', () => ({
  Menu: mockComponent('Menu'),
  MenuItem: mockComponent('MenuItem')
}))

jest.mock('../../../src/components/common/MultiActionMenu', () => mockComponent('MultiActionMenu'))
jest.mock('../../../src/contracts/underwriting/AddEditModelInput', () => mockComponent('AddEditModelInput'))
jest.mock('material-ui/FloatingActionButton', () => mockComponent('FloatingActionButton'))
jest.mock('material-ui/svg-icons/content/add', () => mockComponent('ContentAdd'))
jest.mock('../../../src/watchLists/watchListAlert/WatchListAlert', () => mockComponent('WatchListAlert'))

describe('ModelInputs', () => {
  let props
  beforeEach(() => {
    props = {
      contract: {},
      modelInputs: ['modelInput'],
      loadModelInputs: jest.fn(),
      openDialog: jest.fn(),
      closeDialog: jest.fn(),
      modelInputSaved: false,
      clearModelInputEditForm: jest.fn(),
      checkWatchList: jest.fn(),
      selectModelInputRow: jest.fn()
    }
  })
  it('should render correctly', () => {
    const comp = renderer.create(<ModelInputs {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('should not render grid when modelInputs is empty', () => {
    props.modelInputs = []
    const comp = renderer.create(<ModelInputs {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('should render disabled add button if isEditable isn\'t true', () => {
    isEditable.mockReturnValueOnce(false)
    const comp = renderer.create(<ModelInputs {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('should not call loadModelInputs with no contractId', () => {
    const comp = renderer.create(<ModelInputs {...props} />)
    expect(props.loadModelInputs).not.toHaveBeenCalled()
  })
  it('should not call loadModelInputs modelInputs not right', () => {
    props.contract.contractId = 123
    props.modelInputs = [{contractId: 123}]
    const comp = renderer.create(<ModelInputs {...props} />)
    expect(props.loadModelInputs).not.toHaveBeenCalled()
  })
  it('should call loadModelInputs when modelInputs is empty', () => {
    props.contract.contractId = 123
    props.modelInputs = []
    const comp = renderer.create(<ModelInputs {...props} />)
    expect(props.loadModelInputs).toHaveBeenCalled()
  })
  it('should call loadModelInputs when modelInputs is the wrong ones', () => {
    props.contract.contractId = 123
    props.modelInputs = [{contractId: 321}]
    const comp = renderer.create(<ModelInputs {...props} />)
    expect(props.loadModelInputs).toHaveBeenCalled()
  })
  it('should call closeDialog and clearModelInputEditForm on props update', () => {
    props.modelInputSaved = true
    const comp = shallow(<ModelInputs {...props} />)
    expect(props.closeDialog).not.toHaveBeenCalled()
    expect(props.clearModelInputEditForm).not.toHaveBeenCalled()
    props.modelInputSaved = true
    comp.setProps(props)
    expect(props.closeDialog).toHaveBeenCalled()
    expect(props.clearModelInputEditForm).toHaveBeenCalled()
  })
  it('should not call closeDialog and clearModelInputEditForm on props update when modelInputSaved is false', () => {
    props.modelInputSaved = false
    const comp = shallow(<ModelInputs {...props} />)
    expect(props.closeDialog).not.toHaveBeenCalled()
    expect(props.clearModelInputEditForm).not.toHaveBeenCalled()
    props.modelInputSaved = false
    comp.setProps(props)
    expect(props.closeDialog).not.toHaveBeenCalled()
    expect(props.clearModelInputEditForm).not.toHaveBeenCalled()
  })
  it('should call selectModelInputRow and openDialog on onRowDoubleClick', () => {
    const comp = shallow(<ModelInputs {...props} />)
    comp.children('#ModelInputs').prop('onRowDoubleClick')()
    expect(props.openDialog).toHaveBeenCalled()
    expect(props.selectModelInputRow).toHaveBeenCalled()
  })
  it('should call closeDialog and clearModelInputEditForm on onClose of AddEditModelInput', () => {
    const comp = shallow(<ModelInputs {...props} />)
    comp.children('#ModelInputs-AddEditModelInput').prop('onClose')()
    expect(props.closeDialog).toHaveBeenCalled()
    expect(props.clearModelInputEditForm).toHaveBeenCalled()
  })
  it('should call closeDialog and clearModelInputEditForm on onClose of AddEditModelInput', () => {
    const comp = shallow(<ModelInputs {...props} />)
    comp.children('#ModelInputs-AddEditModelInput').prop('onClose')()
    expect(props.closeDialog).toHaveBeenCalled()
    expect(props.clearModelInputEditForm).toHaveBeenCalled()
  })
  it('should call openDialog and checkWatchList on onClick of the add button', () => {
    const comp = shallow(<ModelInputs {...props} />)
    comp.children('#ModelInputs-AddButton').prop('onClick')()
    expect(props.openDialog).toHaveBeenCalled()
    expect(props.checkWatchList).toHaveBeenCalled()
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][1]).toEqual({
      loadModelInputs: 'loadModelInputs',
      saveModelInput: 'saveModelInput',
      openDialog: 'openDialog',
      closeDialog: 'closeDialog',
      clearModelInputEditForm: 'clearModelInputEditForm',
      checkWatchList: 'checkWatchList',
      selectModelInputRow: 'selectModelInputRow'
    })
  })
  it('mapStateToProps should map correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    const state = {
      contracts: {
        modelInputs: 'modelInputs',
        contract: 'contract',
        modelInputSaved: 'modelInputSaved'
      }
    }
    expect(connect.mock.calls[0][0](state)).toEqual({
      modelInputs: 'modelInputs',
      contract: 'contract',
      modelInputSaved: 'modelInputSaved'
    })
  })
})
