import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'

import EquityWash from '../../../src/contracts/underwriting/EquityWash'

jest.mock('redux-form-material-ui', () => ({
  Checkbox: mockComponent('Checkbox')
}))

jest.mock('redux-form', () => ({
  Field: mockComponent('Field')
}))

jest.mock('../../../src/contracts/RateField', () => mockComponent('RateField'))
jest.mock('../../../src/components/forms/YesNoContingent', () => mockComponent('YesNoContingent'))

describe('Equity Wash', () => {
  it('should render correctly', () => {
    const children = [
      {props: {switch: 'switch'}, type: 'switch'},
      {props: {equityWash: 'equityWash'}, type: 'equityWash'},
      {props: {equityWashContingent: 'equityWashContingent'}, type: 'equityWashContingent'}
    ]
    const component = renderer.create(<EquityWash>{children}</EquityWash>)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should render correctly when there\'s no children', () => {
    const children = []
    const component = renderer.create(<EquityWash>{children}</EquityWash>)
    expect(component.toJSON()).toMatchSnapshot()
  })
})
