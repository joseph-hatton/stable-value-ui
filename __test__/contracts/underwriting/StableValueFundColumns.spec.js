import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'

import {StableValueFundColumns} from '../../../src/contracts/underwriting/StableValueFundColumns'

jest.mock('../../../src/components/utilities/Formatters', () => ({
  dateFormat: (i) => `dateFormat(${i})`,
  percentFormat: (i) => `percentFormat(${i})`,
  currencyFormat: 'currencyFormat'
}))

jest.mock('../../../src/contracts/ContractActions', () => ({
  selectStableValueFundRow: 'selectStableValueFundRow'
}))

describe('StableValueFundColumns', () => {
  it('svBookValue and svMarketValue should use currencyFormat', () => {
    expect(StableValueFundColumns
      .find((i) => i.field === 'svBookValue')
      .renderer).toBe('currencyFormat')
    expect(StableValueFundColumns
      .find((i) => i.field === 'svMarketValue')
      .renderer).toBe('currencyFormat')
  })
  it('calculatedMvBv should pass into percentFormat', () => {
    expect(StableValueFundColumns
      .find((i) => i.field === 'calculatedMvBv')
      .renderer('calculatedMvBv')).toBe('percentFormat(calculatedMvBv)')
  })
  it('svCreditedRate and svCashBuffer should  divide by 100 and pass into percentFormat', () => {
    expect(StableValueFundColumns
      .find((i) => i.field === 'svCreditedRate')
      .renderer(99)).toBe('percentFormat(0.99)')
    expect(StableValueFundColumns
      .find((i) => i.field === 'svCashBuffer')
      .renderer(89)).toBe('percentFormat(0.89)')
  })
  it('should be the correct size', () => {
    expect(StableValueFundColumns.length).toBe(8)
  })
})
