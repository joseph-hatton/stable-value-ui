import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'
import mockConnect from '../../MockConnect'
import {shallow} from 'enzyme'
import {connect} from 'react-redux'

import StableValueFunds from '../../../src/contracts/underwriting/StableValueFunds'

jest.mock('react-redux', () => mockConnect())

jest.mock('../../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn().mockReturnValue('options')
}))

jest.mock('../../../src/contracts/ContractForms', () => ({
  isEditable: jest.fn().mockReturnValue(true)
}))

jest.mock('../../../src/contracts/underwriting/StableValueFundColumns', () => ({
  StableValueFundColumns: 'StableValueFundColumns'
}))

jest.mock('../../../src/contracts/ContractActions', () => ({
  loadStableValueFunds: 'loadStableValueFunds',
  saveStableValueFund: 'saveStableValueFund',
  clearStableValueFundEditForm: 'clearStableValueFundEditForm',
  selectStableValueFundRow: 'selectStableValueFundRow'
}))

jest.mock('../../../src/components/redux-dialog/redux-dialog', () => ({
  openDialog: 'openDialog',
  closeDialog: 'closeDialog'
}))

jest.mock('react-intl', () => ({
  IntlProvider: mockComponent('IntlProvider')
}))

jest.mock('material-ui/Menu', () => ({
  Menu: mockComponent('Menu'),
  MenuItem: mockComponent('MenuItem')
}))

jest.mock('../../../src/components/common/MultiActionMenu', () => mockComponent('MultiActionMenu'))
jest.mock('../../../src/contracts/underwriting/AddEditStableValueFund', () => mockComponent('AddEditStableValueFund'))
jest.mock('material-ui/FloatingActionButton', () => mockComponent('FloatingActionButton'))
jest.mock('material-ui/svg-icons/content/add', () => mockComponent('ContentAdd'))

describe('StableValueFunds', () => {
  let props
  beforeEach(() => {
    props = {
      contract: {},
      stableValueFunds: [],
      loadStableValueFunds: jest.fn(),
      openDialog: jest.fn(),
      closeDialog: jest.fn(),
      clearStableValueFundEditForm: jest.fn(),
      selectStableValueFundRow: jest.fn()
    }
  })
  it('should render correctly', () => {
    const comp = renderer.create(<StableValueFunds {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('shouldn\'t call loadStableValueFunds when contractId isn\'t set', () => {
    const comp = renderer.create(<StableValueFunds {...props} />)
    expect(props.loadStableValueFunds).not.toHaveBeenCalled()
  })
  it('should call loadStableValueFunds when contractId is set', () => {
    props.contract.contractId = 123
    const comp = renderer.create(<StableValueFunds {...props} />)
    expect(props.loadStableValueFunds).toHaveBeenCalledWith(123)
  })
  it('should call selectStableValueFundRow and openDialog on onRowDoubleClick', () => {
    const comp = shallow(<StableValueFunds {...props} />)
    comp.find('#StableValueFunds').prop('onRowDoubleClick')()
    expect(props.selectStableValueFundRow).toHaveBeenCalled()
    expect(props.openDialog).toHaveBeenCalled()
  })
  it('should call closeDialog and clearStableValueFundEditForm on onClose of the add edit popup', () => {
    const comp = shallow(<StableValueFunds {...props} />)
    comp.find('#StableValueFunds-AddEdit').prop('onClose')()
    expect(props.closeDialog).toHaveBeenCalled()
    expect(props.clearStableValueFundEditForm).toHaveBeenCalled()
  })
  it('should call openDialog on onClick of the add button', () => {
    const comp = shallow(<StableValueFunds {...props} />)
    comp.find('#StableValueFunds-AddButton').prop('onClick')()
    expect(props.openDialog).toHaveBeenCalled()
  })
  it('mapStateToProps should map correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    const state = {
      contracts: {
        stableValueFunds: 'stableValueFunds',
        contract: 'contract'
      }
    }
    expect(connect.mock.calls[0][0](state)).toEqual({
      stableValueFunds: 'stableValueFunds',
      contract: 'contract'
    })
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][1]).toEqual({
      loadStableValueFunds: 'loadStableValueFunds',
      saveStableValueFund: 'saveStableValueFund',
      openDialog: 'openDialog',
      closeDialog: 'closeDialog',
      clearStableValueFundEditForm: 'clearStableValueFundEditForm',
      selectStableValueFundRow: 'selectStableValueFundRow'
    })
  })
})
