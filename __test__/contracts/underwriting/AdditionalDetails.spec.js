import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'
import {connect} from 'react-redux'
import {isEditable} from '../../../src/contracts/ContractForms'
import {change} from 'redux-form'
import moment from 'moment'

import AdditionalDetails from '../../../src/contracts/underwriting/AdditionalDetails'

jest.mock('react-redux', () => ({
  connect: jest.fn(() => (comp) => comp)
}))

jest.mock('../../../src/contracts/ContractForms', () => ({
  ADDITIONAL_DETAIL: 'ADDITIONAL_DETAIL'
}))

jest.mock('redux-form', () => ({
  change: jest.fn().mockReturnValue('change'),
  Field: mockComponent('Field')
}))

jest.mock('redux-form-material-ui', () => ({
  DatePicker: mockComponent('DatePicker'),
  TextField: mockComponent('TextField'),
  Checkbox: mockComponent('Checkbox')
}))

jest.mock('material-ui/Card', () => ({
  Card: mockComponent('Card'),
  CardTitle: mockComponent('CardTitle'),
  CardText: mockComponent('CardText')
}))

jest.mock('../../../src/components/forms/Validators', () => ({
  Required: 'Required',
  FormatCurrency: 'FormatCurrency',
  toNumber: 'toNumber',
  MaxLength: () => 'MaxLength',
  isInteger: 'isInteger',
  toInteger: 'toInteger'
}))

jest.mock('../../../src/contracts/ContractRefDataFields', () => ({
  Sectors: mockComponent('Sectors'),
  SPRatings: mockComponent('SPRatings'),
  FitchRatings: mockComponent('FitchRatings'),
  MoodyRatings: mockComponent('MoodyRatings'),
  OverallRatings: mockComponent('OverallRatings'),
  AdviceServiceRemedy: mockComponent('AdviceServiceRemedy')
}))

jest.mock('../../../src/contracts/RateField', () => mockComponent('RateField'))
jest.mock('../../../src/contracts/CreateContractForm', () => jest.fn().mockReturnValue(mockComponent('AdditionalDetailsForm')))
jest.mock('../../../src/contracts/edithistory/AuditableContractField', () => mockComponent('AuditableContractField'))
jest.mock('material-ui/Slider', () => mockComponent('Slider'))
jest.mock('../../../src/components/forms/YesNoContingent', () => mockComponent('YesNoContingent'))
jest.mock('../../../src/contracts/underwriting/EquityWash', () => mockComponent('EquityWash'))

describe('AdditonalDetails', () => {
  let props
  beforeEach(() => {
    props = {
      contract: {},
      brokerageWindowChanged: jest.fn(),
      moneyMarketChanged: jest.fn(),
      otherChanged: jest.fn(),
      brokerageWindowChecked: false,
      moneyMarketChecked: false,
      otherChecked: false,
      brokerageEquityWash: 'brokerageEquityWash',
      moneyMarketEquityWash: 'moneyMarketEquityWash',
      otherEquityWash: 'otherEquityWash',
      brokerageEquityWashChanged: jest.fn(),
      moneyMarketEquityWashChanged: jest.fn(),
      otherEquityWashChanged: jest.fn(),
      adviceService: false,
      adviceServiceRemedy: 'adviceServiceRemedy',
      adviceServiceChanged: jest.fn(),
      fundActiveRateChanged: jest.fn(),
      fundActiveRate: 0.0
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<AdditionalDetails {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should render correctly', () => {
    props.brokerageWindowChecked = true
    props.moneyMarketChecked = true
    props.otherChecked = true
    props.adviceService = true
    const component = renderer.create(<AdditionalDetails {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('connect should be called once', () => {
    expect(connect).toHaveBeenCalledTimes(1)
  })
  describe('actions', () => {
    let actions, dispatch, getState, state
    beforeEach(() => {
      actions = connect.mock.calls[0][1]
      dispatch = jest.fn()
      state = {contracts: {}}
      getState = jest.fn().mockReturnValue(state)
      change.mockClear()
    })
    it('brokerageWindowChanged should dispatch', () => {
      state.contracts.contract = {brokerageEquityWash: 'brokerageEquityWash'}

      actions.brokerageWindowChanged(null, true)(dispatch, getState)

      expect(dispatch.mock.calls[0][0]).toBe('change')
      expect(change.mock.calls[0][0]).toBe('ADDITIONAL_DETAIL')
      expect(change.mock.calls[0][1]).toBe('brokerageEquityWash')
      expect(change.mock.calls[0][2]).toBe('brokerageEquityWash')
    })
    it('brokerageWindowChanged should dispatch', () => {
      state.contracts.contract = {brokerageEquityWash: 'brokerageEquityWash'}

      actions.brokerageWindowChanged(null, false)(dispatch, getState)

      expect(dispatch.mock.calls[0][0]).toBe('change')
      expect(change.mock.calls[0][0]).toBe('ADDITIONAL_DETAIL')
      expect(change.mock.calls[0][1]).toBe('brokerageEquityWash')
      expect(change.mock.calls[0][2]).toBe(null)
    })
    it('brokerageEquityWashChanged should dispatch', () => {
      actions.brokerageEquityWashChanged(null, 'CONTINGENT')(dispatch)

      expect(dispatch).not.toHaveBeenCalled()
    })
    it('brokerageEquityWashChanged should dispatch', () => {
      actions.brokerageEquityWashChanged(null, 'NOT_CONTINGENT')(dispatch)

      expect(dispatch).toHaveBeenCalled()
      expect(change.mock.calls[0][0]).toBe('ADDITIONAL_DETAIL')
      expect(change.mock.calls[0][1]).toBe('brokerageEquityWashRate')
      expect(change.mock.calls[0][2]).toBe(0)
    })
    it('moneyMarketChanged should dispatch', () => {
      state.contracts.contract = {moneyMarketEquityWash: 'moneyMarketEquityWash'}

      actions.moneyMarketChanged(null, true)(dispatch, getState)

      expect(dispatch).toHaveBeenCalled()
      expect(change.mock.calls[0][0]).toBe('ADDITIONAL_DETAIL')
      expect(change.mock.calls[0][1]).toBe('moneyMarketEquityWash')
      expect(change.mock.calls[0][2]).toBe('moneyMarketEquityWash')
    })
    it('moneyMarketChanged should dispatch', () => {
      state.contracts.contract = {moneyMarketEquityWash: 'moneyMarketEquityWash'}

      actions.moneyMarketChanged(null, false)(dispatch, getState)

      expect(dispatch).toHaveBeenCalled()
      expect(change.mock.calls[0][0]).toBe('ADDITIONAL_DETAIL')
      expect(change.mock.calls[0][1]).toBe('moneyMarketEquityWash')
      expect(change.mock.calls[0][2]).toBe(null)
    })
    it('moneyMarketEquityWashChanged should dispatch', () => {
      actions.moneyMarketEquityWashChanged(null, 'NOT_CONTINGENT')(dispatch)

      expect(dispatch).toHaveBeenCalled()
      expect(change.mock.calls[0][0]).toBe('ADDITIONAL_DETAIL')
      expect(change.mock.calls[0][1]).toBe('moneyMarketEquityWashRate')
      expect(change.mock.calls[0][2]).toBe(0)
    })
    it('moneyMarketEquityWashChanged should dispatch', () => {
      actions.moneyMarketEquityWashChanged(null, 'CONTINGENT')(dispatch)

      expect(dispatch).not.toHaveBeenCalled()
    })
    it('otherChanged should dispatch', () => {
      state.contracts.contract = {otherEquityWash: 'otherEquityWash'}

      actions.otherChanged(null, true)(dispatch, getState)

      expect(dispatch).toHaveBeenCalled()
      expect(change.mock.calls[0][0]).toBe('ADDITIONAL_DETAIL')
      expect(change.mock.calls[0][1]).toBe('otherEquityWash')
      expect(change.mock.calls[0][2]).toBe('otherEquityWash')
    })
    it('otherChanged should dispatch', () => {
      state.contracts.contract = {otherEquityWash: 'otherEquityWash'}

      actions.otherChanged(null, false)(dispatch, getState)

      expect(dispatch).toHaveBeenCalled()
      expect(change.mock.calls[0][0]).toBe('ADDITIONAL_DETAIL')
      expect(change.mock.calls[0][1]).toBe('otherEquityWash')
      expect(change.mock.calls[0][2]).toBe(null)
    })
    it('otherEquityWashChanged should dispatch', () => {
      actions.otherEquityWashChanged(null, 'NOT_CONTINGENT')(dispatch)

      expect(dispatch).toHaveBeenCalled()
      expect(change.mock.calls[0][0]).toBe('ADDITIONAL_DETAIL')
      expect(change.mock.calls[0][1]).toBe('otherEquityWashRate')
      expect(change.mock.calls[0][2]).toBe(0)
    })
    it('otherEquityWashChanged should dispatch', () => {
      actions.otherEquityWashChanged(null, 'CONTINGENT')(dispatch)

      expect(dispatch).not.toHaveBeenCalled()
    })
    it('adviceServiceChanged should dispatch', () => {
      actions.adviceServiceChanged(null, false)(dispatch)

      expect(dispatch).toHaveBeenCalledTimes(3)

      expect(change.mock.calls[0][0]).toBe('ADDITIONAL_DETAIL')
      expect(change.mock.calls[0][1]).toBe('adviceServiceProvider')
      expect(change.mock.calls[0][2]).toBe(null)

      expect(change.mock.calls[1][0]).toBe('ADDITIONAL_DETAIL')
      expect(change.mock.calls[1][1]).toBe('adviceServiceRemedy')
      expect(change.mock.calls[1][2]).toBe(null)

      expect(change.mock.calls[2][0]).toBe('ADDITIONAL_DETAIL')
      expect(change.mock.calls[2][1]).toBe('tradeLimits')
      expect(change.mock.calls[2][2]).toBe(null)
    })
    it('adviceServiceChanged should dispatch', () => {
      actions.adviceServiceChanged(null, true)(dispatch)

      expect(dispatch).not.toHaveBeenCalled()
      expect(change).not.toHaveBeenCalled()
    })
    it('fundActiveRateChanged should dispatch', () => {
      actions.fundActiveRateChanged(null, 'fundActiveRate')(dispatch)

      expect(dispatch).toHaveBeenCalled()
      expect(change.mock.calls[0][0]).toBe('ADDITIONAL_DETAIL')
      expect(change.mock.calls[0][1]).toBe('fundActiveRate')
      expect(change.mock.calls[0][2]).toBe('fundActiveRate')
    })
  })
})
