import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'

import SliderWithLabel from '../../../src/contracts/underwriting/SliderWithLabel'

jest.mock('material-ui/Slider', () => mockComponent('Slider'))

describe('SliderWithLabel', () => {
  it('should render correctly', () => {
    const props = {
      onChange: jest.fn(),
      label: 'label',
      value: 0,
      max: 100,
      style: {},
      textClassName: 'textClassName'
    }
    const comp = renderer.create(<SliderWithLabel {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
})
