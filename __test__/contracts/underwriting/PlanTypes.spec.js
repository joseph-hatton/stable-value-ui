import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'
import mockConnect from '../../MockConnect'
import {change} from 'redux-form'
import {connect} from 'react-redux'

import PlanTypes, {mapFormProps, validate, calcTotal} from '../../../src/contracts/underwriting/PlanTypes'

jest.mock('react-redux', () => mockConnect())
jest.mock('../../../src/contracts/CreateContractForm', () => () => mockComponent('PlanTypesForm'))
jest.mock('../../../src/contracts/underwriting/SliderWithLabel', () => mockComponent('SliderWithLabel'))
jest.mock('material-ui/Divider', () => mockComponent('Divider'))
jest.mock('../../../src/contracts/RateField', () => mockComponent('RateField'))
jest.mock('redux-form', () => ({
  change: jest.fn()
}))

describe('PlanTypes', () => {
  it('should render correctly', () => {
    const props = {}
    const comp = renderer.create(<PlanTypes {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('mapFormProps should map correctly', () => {
    expect(mapFormProps({contracts: {contract: {
      plan401ARate: 'plan401ARate',
      plan401KRate: 'plan401KRate',
      plan403BRate: 'plan403BRate',
      plan457Rate: 'plan457Rate',
      plan501CRate: 'plan501CRate',
      plan529Rate: 'plan529Rate',
      planTaftRate: 'planTaftRate'
    }}})).toEqual({
      initialValues: {
        plan401ARate: 'plan401ARate',
        plan401KRate: 'plan401KRate',
        plan403BRate: 'plan403BRate',
        plan457Rate: 'plan457Rate',
        plan501CRate: 'plan501CRate',
        plan529Rate: 'plan529Rate',
        planTaftRate: 'planTaftRate'
      }
    })
  })
  it('mapFormProps should default null values to 0', () => {
    expect(mapFormProps({contracts: {contract: {
      plan401ARate: null,
      plan401KRate: 'plan401KRate',
      plan403BRate: 'plan403BRate',
      plan457Rate: 'plan457Rate',
      plan501CRate: 'plan501CRate',
      plan529Rate: 'plan529Rate',
      planTaftRate: 'planTaftRate'
    }}})).toEqual({
      initialValues: {
        plan401ARate: 0,
        plan401KRate: 'plan401KRate',
        plan403BRate: 'plan403BRate',
        plan457Rate: 'plan457Rate',
        plan501CRate: 'plan501CRate',
        plan529Rate: 'plan529Rate',
        planTaftRate: 'planTaftRate'
      }
    })
  })
  it('validate should return an empty object when values and initial values are equal', () => {
    const props = {
      initialValues: {
        test: 'test'
      }
    }
    const values = {
      test: 'test'
    }
    expect(validate(values, props)).toEqual({})
  })
  it('validate should return an empty object when the newly entered amount is equal to 100', () => {
    const props = {
      initialValues: {
        plan401ARate: 0,
        plan401KRate: 0,
        plan403BRate: 0,
        plan457Rate: 0,
        plan501CRate: 0,
        plan529Rate: 0,
        planTaftRate: 0
      }
    }
    const values = {
      plan401ARate: 50,
      plan401KRate: 40,
      plan403BRate: 10,
      plan457Rate: 0,
      plan501CRate: 0,
      plan529Rate: 0,
      planTaftRate: 0
    }
    expect(validate(values, props)).toEqual({})
  })
  it('validate should return a full object when the newly entered amount is less than 100', () => {
    const props = {
      initialValues: {
        plan401ARate: 0,
        plan401KRate: 0,
        plan403BRate: 0,
        plan457Rate: 0,
        plan501CRate: 0,
        plan529Rate: 0,
        planTaftRate: 0
      }
    }
    const values = {
      plan401ARate: 50,
      plan401KRate: 40,
      plan403BRate: 0,
      plan457Rate: 0,
      plan501CRate: 0,
      plan529Rate: 0,
      planTaftRate: 0
    }
    const msg = 'The total must add up to 100%'
    expect(validate(values, props)).toEqual({
      plan401ARate: msg,
      plan401KRate: msg,
      plan403BRate: msg,
      plan457Rate: msg,
      plan501CRate: msg,
      plan529Rate: msg,
      planTaftRate: msg
    })
  })
  it('validate should return a full object when the newly entered amount is greater than 100', () => {
    const props = {
      initialValues: {
        plan401ARate: 0,
        plan401KRate: 0,
        plan403BRate: 0,
        plan457Rate: 0,
        plan501CRate: 0,
        plan529Rate: 0,
        planTaftRate: 0
      }
    }
    const values = {
      plan401ARate: 50,
      plan401KRate: 40,
      plan403BRate: 100,
      plan457Rate: 0,
      plan501CRate: 0,
      plan529Rate: 0,
      planTaftRate: 0
    }
    const msg = 'The total must add up to 100%'
    expect(validate(values, props)).toEqual({
      plan401ARate: msg,
      plan401KRate: msg,
      plan403BRate: msg,
      plan457Rate: msg,
      plan501CRate: msg,
      plan529Rate: msg,
      planTaftRate: msg
    })
  })
  it('calcTotal should sum up correctly', () => {
    expect(calcTotal({
      plan401ARate: 15,
      plan401KRate: 25,
      plan403BRate: 45,
      plan457Rate: 10,
      plan501CRate: 21,
      plan529Rate: 2,
      planTaftRate: 11
    })).toBe(129)
  })
  it('calcTotal should default non numbers to 0', () => {
    expect(calcTotal({
      plan401ARate: 0,
      plan401KRate: 0,
      plan403BRate: 0,
      plan457Rate: 'a',
      plan501CRate: 0,
      plan529Rate: 0,
      planTaftRate: 0
    })).toBe(0)
  })
  it('calcTotal should default to 0 if values isn\'t set', () => {
    expect(calcTotal()).toBe(0)
  })
  it('mapStateToProps should map correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    const state = {form: {
      planType: {
        values: {
          plan401ARate: 'plan401ARate',
          plan401KRate: 'plan401KRate',
          plan403BRate: 'plan403BRate',
          plan457Rate: 'plan457Rate',
          plan501CRate: 'plan501CRate',
          plan529Rate: 'plan529Rate',
          planTaftRate: 'planTaftRate'
        }
      }
    }}
    expect(connect.mock.calls[0][0](state)).toEqual({
      values: {
        plan401ARate: 'plan401ARate',
        plan401KRate: 'plan401KRate',
        plan403BRate: 'plan403BRate',
        plan457Rate: 'plan457Rate',
        plan501CRate: 'plan501CRate',
        plan529Rate: 'plan529Rate',
        planTaftRate: 'planTaftRate'
      }
    })
  })
})
