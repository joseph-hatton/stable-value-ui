import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'

import {ModelInputColumns} from '../../../src/contracts/underwriting/ModelInputColumns'

jest.mock('../../../src/components/utilities/Formatters', () => ({
  dateFormat: (i) => `dateFormat(${i})`
}))

describe('ModelInputColumns', () => {
  it('should have all the items', () => {
    expect(ModelInputColumns.length).toBe(8)
  })
})
