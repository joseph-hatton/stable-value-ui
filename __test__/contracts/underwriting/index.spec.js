import UnderwritingTabs from '../../../src/contracts/underwriting/index'

jest.mock('../../../src/contracts/underwriting/ModelInputs', () => 'ModelInputs')
jest.mock('../../../src/contracts/underwriting/StableValueFunds', () => 'StableValueFunds')
jest.mock('../../../src/contracts/underwriting/AdditionalDetails', () => 'AdditionalDetails')
jest.mock('../../../src/contracts/underwriting/PlanTypes', () => 'PlanTypes')

describe('underwriting index', () => {
  it('ModelInputs should be setup', () => {
    const item = UnderwritingTabs.find((i) => i.label === 'Model Inputs')
    expect(item).toEqual({
      label: 'Model Inputs',
      ContractTab: 'ModelInputs',
      loadDynamically: true,
      url: 'model-inputs'
    })
  })
  it('StableValueFund should be setup', () => {
    const item = UnderwritingTabs.find((i) => i.label === 'Stable Value Fund')
    expect(item).toEqual({
      label: 'Stable Value Fund',
      ContractTab: 'StableValueFunds',
      loadDynamically: true,
      url: 'stable-value-fund'
    })
  })
  it('AdditionalDetails should be setup', () => {
    const item = UnderwritingTabs.find((i) => i.label === 'Additional Details')
    expect(item).toEqual({
      label: 'Additional Details',
      ContractTab: 'AdditionalDetails',
      loadDynamically: false,
      url: 'additional-details'
    })
  })
  it('PlanTypes should be setup', () => {
    const item = UnderwritingTabs.find((i) => i.label === 'Plan Types')
    expect(item).toEqual({
      label: 'Plan Types',
      ContractTab: 'PlanTypes',
      loadDynamically: false,
      url: 'plan-types'
    })
  })
})
