import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'
import {connect} from 'react-redux'
import {isEditable} from '../../../src/contracts/ContractForms'
import {reduxForm} from 'redux-form'
import moment from 'moment'

import AddEditModelInput, {formId} from '../../../src/contracts/underwriting/AddEditModelInput'

jest.mock('react-redux', () => ({
  connect: jest.fn(() => (comp) => comp)
}))

jest.mock('redux-form', () => ({
  reduxForm: jest.fn(() => (comp) => comp),
  Field: mockComponent('Field')
}))

jest.mock('redux-form-material-ui', () => ({
  TextField: mockComponent('TextField'),
  DatePicker: mockComponent('DatePicker')
}))

jest.mock('../../../src/components/forms/Validators', () => ({
  MaxLength: jest.fn(() => 'MaxLength'),
  isInteger: 'isInteger',
  toInteger: 'toInteger',
  Required: 'Required'
}))

jest.mock('../../../src/components/utilities/Formatters', () => ({
  toUtc: 'toUtc',
  dateFormat: 'dateFormat'
}))

jest.mock('../../../src/contracts/ContractActions', () => ({
  saveModelInput: jest.fn()
}))

jest.mock('../../../src/contracts/ContractForms', () => ({
  isEditable: jest.fn()
}))

jest.mock('../../../src/components/redux-dialog', () => mockComponent('ReduxDialog'))
jest.mock('material-ui/RaisedButton', () => mockComponent('RaisedButton'))
jest.mock('../../../src/contracts/RateField', () => mockComponent('RateField'))

describe('AddEditModelInput', () => {
  let props
  beforeEach(() => {
    props = {
      onClose: jest.fn(),
      handleSubmit: jest.fn().mockReturnValue('handleSubmit'),
      saveModelInput: jest.fn(),
      initialValues: {},
      contract: {}
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<AddEditModelInput {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should render correctly', () => {
    props.initialValues.underwritingId = 123
    const component = renderer.create(<AddEditModelInput {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should render correctly', () => {
    props.contract.contractType = 'POOLED_PLAN'
    const component = renderer.create(<AddEditModelInput {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should render correctly', () => {
    isEditable.mockReturnValue(true)
    const component = renderer.create(<AddEditModelInput {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should call connect once', () => {
    expect(connect).toHaveBeenCalledTimes(1)
  })
  it('should call reduxForm once', () => {
    expect(reduxForm).toHaveBeenCalledTimes(1)
    reduxForm.mock.calls[0][0].form = formId
  })
  it('should validate appropriately', () => {
    const effectiveDate = moment('2015-01-31')

    const values = {effectiveDate, underwritingId: 123}
    const props = {modelInputs: [{underwritingId: 321, effectiveDate: effectiveDate}]}

    const result = reduxForm.mock.calls[0][0].validate(values, props)

    expect(result).toEqual({effectiveDate: 'Effective Date is already used'})
  })
  it('should validate appropriately', () => {
    const effectiveDate = moment('2015-01-31')

    const values = {effectiveDate, underwritingId: 123}
    const props = {modelInputs: [{underwritingId: 123, effectiveDate: effectiveDate}]}

    const result = reduxForm.mock.calls[0][0].validate(values, props)

    expect(result).toEqual(null)
  })
  it('should validate appropriately', () => {
    const values = {effectiveDate: '2015-01-31', underwritingId: 123}
    const props = {modelInputs: [{underwritingId: 321, effectiveDate: moment('2015-01-01')}]}

    const result = reduxForm.mock.calls[0][0].validate(values, props)

    expect(result).toEqual(null)
  })
  it('should validate appropriately', () => {
    const values = {effectiveDate: '2015-01-31', underwritingId: 123}
    const props = {modelInputs: []}

    const result = reduxForm.mock.calls[0][0].validate(values, props)

    expect(result).toEqual(undefined)
  })
  it('should validate appropriately', () => {
    expect(reduxForm).toHaveBeenCalledTimes(1)

    const values = {effectiveDate: '2015-01-31', underwritingId: 123}
    const props = {modelInputs: []}

    const result = reduxForm.mock.calls[0][0].validate(values, props)

    expect(result).toEqual(undefined)
  })
  it('mapStateToProps should map correctly', () => {
    const state = {contracts: {modelInput: 'modelInput', modelInputs: 'modelInputs', contract: 'contract'}}
    const result = connect.mock.calls[0][0](state)

    expect(result.contract).toBe('contract')
    expect(result.initialValues).toBe('modelInput')
    expect(result.modelInputs).toBe('modelInputs')
  })
})
