import React from 'react'
import mockConnect from '../MockConnect'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import {connect} from 'react-redux'
import {shallow} from 'enzyme'

import ContractContacts from '../../src/contracts/ContractContacts'

jest.mock('react-redux', () => mockConnect())
jest.mock('../../src/contracts/ContractActions', () => ({
  loadContractContacts: 'loadContractContacts',
  assignContactToContract: 'assignContactToContract',
  selectContact: 'selectContact',
  selectContactContract: 'selectContactContract'
}))
jest.mock('../../src/contacts/ContactActions', () => ({
  loadAllContacts: 'loadAllContacts'
}))
jest.mock('../../src/contracts/contacts/AssignContact', () => mockComponent('AssignContact'))
jest.mock('material-ui/svg-icons/content/send', () => mockComponent('SendIcon'))
jest.mock('material-ui', () => ({
  Card: mockComponent('Card'),
  AutoComplete: mockComponent('AutoComplete'),
  RaisedButton: mockComponent('RaisedButton')
}))
jest.mock('../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: () => 'options'
}))
jest.mock('../../src/contracts/contacts/AssignContactColumns', () => 'ContractContactColumns')
jest.mock('../../src/contacts/AddEditContactComponent', () => 'AddEditContactComponent')
jest.mock('../../src/contracts/contacts/AssignContactMenu', () => 'AssignContactMenu')
jest.mock('../../src/components/forms/CaseInSensitiveFilter', () => 'CaseInSensitiveFilter')
jest.mock('../../src/components/redux-dialog/redux-dialog', () => ({
  openSnackbar: 'openSnackbar',
  openDialog: 'openDialog'
}))

describe('ContractContacts', () => {
  let props
  beforeEach(() => {
    props = {
      contractId: 123,
      contractContacts: [],
      loadContractContacts: jest.fn(),
      loadAllContacts: jest.fn(),
      contractNumber: 'contractNumber',
      allContacts: [],
      openSnackbar: jest.fn(),
      openDialog: jest.fn(),
      selectContact: 'selectContact',
      selectContactContract: jest.fn(),
      newContact: {}
    }
  })
  it('should render correctly', () => {
    const comp = renderer.create(<ContractContacts {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('should load contract contacts if contract id is set and contractContacts is empty', () => {
    const comp = renderer.create(<ContractContacts {...props} />)
    expect(props.loadContractContacts).toHaveBeenCalled()
  })
  it('should not load contract contacts if contract id is not set and contractContacts is empty', () => {
    props.contractId = undefined
    const comp = renderer.create(<ContractContacts {...props} />)
    expect(props.loadContractContacts).not.toHaveBeenCalled()
  })
  it('should not load contract contacts if contract id is set and contractContacts already loaded', () => {
    props.contractContacts = [{contractId: 123}]
    const comp = renderer.create(<ContractContacts {...props} />)
    expect(props.loadContractContacts).not.toHaveBeenCalled()
  })
  it('should not load contract contacts if contractContacts already loaded are for the wrong contract', () => {
    props.contractContacts = [{contractId: 321}]
    const comp = renderer.create(<ContractContacts {...props} />)
    expect(props.loadContractContacts).toHaveBeenCalled()
  })
  it('should load contacts if the contacts array is empty', () => {
    const comp = renderer.create(<ContractContacts {...props} />)
    expect(props.loadAllContacts).toHaveBeenCalled()
  })
  it('should not load contacts if the contacts array isn\'t empty', () => {
    props.allContacts = [{}]
    const comp = renderer.create(<ContractContacts {...props} />)
    expect(props.loadAllContacts).not.toHaveBeenCalled()
  })
  describe('Clicking on Assign Contact', () => {
    it('should open snackbar if there is an existing contact', () => {
      props.contractContacts = [{contactId: 123}]
      props.newContact.contactId = 123
      const comp = shallow(<ContractContacts {...props} />)
      comp.find('#ContractContacts-AssignContactButton').prop('onClick')()
      expect(props.openSnackbar).toHaveBeenCalledWith({
        type: 'error',
        message: 'Contact is already assigned to the contract.',
        autoHideDuration: 5000
      })
    })
    it('should open dialog if there is not an existing contact', () => {
      props.contractContacts = [{contactId: 123}]
      props.newContact.contactId = 321
      const comp = shallow(<ContractContacts {...props} />)
      comp.find('#ContractContacts-AssignContactButton').prop('onClick')()
      expect(props.openDialog).toHaveBeenCalledWith('assignContactToContract')
    })
    it('should do nothing if newContact isn\'t set', () => {
      props.newContact = undefined
      const comp = shallow(<ContractContacts {...props} />)
      comp.find('#ContractContacts-AssignContactButton').prop('onClick')()
      expect(props.openDialog).not.toHaveBeenCalled()
      expect(props.openSnackbar).not.toHaveBeenCalled()
    })
  })
  it('mapStateToProps should map state correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][0]({
      referenceData: {contactTypes: [{id: 'id', text: 'text'}]},
      contracts: {
        contractContacts: [{id: 'id', contactType: 'id'}],
        contract: {contractId: 123},
        newContact: 'newContact'
      },
      contacts: {allContacts: [{
        name: 'name',
        companyName: 'companyName',
        contactTypeName: 'contactTypeName',
        contactId: 321
      }]}
    })).toEqual({
      contractId: 123,
      contractContacts: [{
        contactType: 'text',
        id: 'id'
      }],
      allContacts: [{
        contact: {
          name: 'name',
          companyName: 'companyName',
          contactTypeName: 'contactTypeName',
          contactId: 321
        },
        text: 'name - contactTypeName ( companyName )',
        value: 321
      }],
      newContact: 'newContact'
    })
  })
  it('mapStateToProps should map state correctly when contractContacts isn\'t set', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][0]({
      referenceData: {contactTypes: [{id: 'id', text: 'text'}]},
      contracts: {
        contract: {contractId: 123},
        newContact: 'newContact'
      },
      contacts: {allContacts: [{
        name: 'name',
        companyName: 'companyName',
        contactTypeName: 'contactTypeName',
        contactId: 321
      }]}
    })).toEqual({
      contractId: 123,
      contractContacts: [],
      allContacts: [{
        contact: {
          name: 'name',
          companyName: 'companyName',
          contactTypeName: 'contactTypeName',
          contactId: 321
        },
        text: 'name - contactTypeName ( companyName )',
        value: 321
      }],
      newContact: 'newContact'
    })
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][1]).toEqual({
      loadContractContacts: 'loadContractContacts',
      loadAllContacts: 'loadAllContacts',
      openSnackbar: 'openSnackbar',
      openDialog: 'openDialog',
      selectContact: 'selectContact',
      selectContactContract: 'selectContactContract'
    })
  })
})
