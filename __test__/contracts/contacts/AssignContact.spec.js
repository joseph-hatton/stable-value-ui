import React from 'react'
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'
import ReduxDialog from '../../../src/components/redux-dialog'
import { assignContactToContract, selectContactContract } from '../../../src/contracts/ContractActions'
import YesNo from '../../../src/components/forms/YesNo'

import AssignContact from '../../../src/contracts/contacts/AssignContact'

jest.mock('react-redux', () => {
  return {
    connect: () => (component) => {
      return component
    }
  }
})

jest.mock('material-ui', () => ({
  RaisedButton: mockComponent('RaisedButton'),
  Card: mockComponent('Card')
}))

jest.mock('redux-form', () => ({
  reduxForm: () => (component) => component
}))

const mockAssignContactToContract = jest.fn()
const mockSelectContactContract = jest.fn()
jest.mock('../../../src/contracts/ContractActions', () => ({
  assignContactToContract: mockAssignContactToContract,
  selectContactContract: mockSelectContactContract
}))

jest.mock('../../../src/components/redux-dialog', () => mockComponent('redux-dialog'))

jest.mock('../../../src/components/forms/YesNo', () => mockComponent('YesNo'))

let contact = {name: 'name', contactTypeName: 'contactTypeName'}

let closeDialog, componentProps, component

describe('AssignContact', () => {
  beforeEach(() => {
    closeDialog = jest.fn()
    componentProps = {closeDialog, contact, handleSubmit: jest.fn(), assignContactToContract: mockAssignContactToContract, selectContactContract: mockSelectContactContract}
  })
  it('should match snapshot', () => {
    component = renderer.create(<AssignContact {...componentProps}></AssignContact>)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should call closeDialog and selectContactContract on Cancel', () => {
    component = shallow(<AssignContact {...componentProps}></AssignContact>)
    component.prop('actions').find((i) => i.props.id === 'AssignContact-Cancel').props.onClick()

    expect(closeDialog.mock.calls.length).toBe(1)
    expect(mockSelectContactContract.mock.calls.length).toBe(1)
  })
})
