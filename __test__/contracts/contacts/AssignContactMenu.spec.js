import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from '../../MockComponent'
import mockConnect from '../../MockConnect'
import {browserHistory} from 'react-router'
import {connect} from 'react-redux'

import AssignContactMenu, {AssignContactMenuLink} from '../../../src/contracts/contacts/AssignContactMenu'

jest.mock('react-redux', () => mockConnect())
jest.mock('material-ui', () => ({
  Popover: mockComponent('Popover'),
  Menu: mockComponent('Menu'),
  MenuItem: mockComponent('MenuItem')
}))
jest.mock('../../../src/contracts/ContractActions', () => ({
  unAssignContactFromContract: 'unAssignContactFromContract',
  selectContactContract: 'selectContactContract'
}))
jest.mock('../../../src/contacts/ContactActions', () => ({
  selectAndLoadContact: 'selectAndLoadContact'
}))
jest.mock('../../../src/components/redux-dialog/redux-dialog', () => ({
  openDialog: 'openDialog',
  closeDialog: 'closeDialog',
  openConfirmationAlert: 'openConfirmationAlert'
}))
jest.mock('react-router', () => ({
  browserHistory: {
    push: jest.fn()
  }
}))

describe('AssignContactsMenu', () => {
  let props
  beforeEach(() => {
    props = {
      selectedContractContact: {contactId: 123, contracts: [], contractId: 456, contactContractId: 321, lastName: 'lastName', firstName: 'firstName'},
      open: 'open',
      closeDialog: jest.fn(),
      openDialog: jest.fn(),
      openConfirmationAlert: jest.fn(),
      unAssignContactFromContract: jest.fn(),
      selectAndLoadContact: jest.fn()
    }
    browserHistory.push.mockClear()
  })
  it('should render correctly', () => {
    const comp = renderer.create(<AssignContactMenu {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('onClick of the View Edit Button should close the menu and redirect', () => {
    shallow(<AssignContactMenu {...props} />).find('#ContactMenu-viewEditContactButton').prop('onClick')()
    expect(props.closeDialog).toHaveBeenCalledWith('assignContactMenu')
    expect(props.openDialog).toHaveBeenCalledWith('addEditContact')
  })
  it('onClick of the edit assign info Button should close the menu and open the assign info dialog', () => {
    shallow(<AssignContactMenu {...props} />).find('#ContactMenu-viewEditContactContractButton').prop('onClick')()
    expect(props.closeDialog).toHaveBeenCalledWith('assignContactMenu')
    expect(props.openDialog).toHaveBeenCalledWith('assignContactToContract')
  })
  it('onClick of the unassign Button should close the menu and open the unassign dialog', () => {
    shallow(<AssignContactMenu {...props} />).find('#ContactMenu-UnassignContactButton').prop('onClick')()
    expect(props.closeDialog).toHaveBeenCalledWith('assignContactMenu')
    expect(props.openConfirmationAlert).toHaveBeenCalledTimes(1)
    expect(props.openConfirmationAlert.mock.calls[0][0].message)
      .toEqual(`This will unassign lastName, firstName from the contract. Do you want to proceed?`)
    props.openConfirmationAlert.mock.calls[0][0].onOk()
    expect(props.unAssignContactFromContract).toHaveBeenCalledWith(456, 321)
  })
  it('mapStateToProps should map correctly when assignContactMenu is set', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[1][0]({
      dialog: {assignContactMenu: {anchorEl: 'anchorEl'}},
      contracts: {selectedContractContact: 'selectedContractContact'}
    })).toEqual({
      open: true,
      anchorEl: 'anchorEl',
      selectedContractContact: 'selectedContractContact'
    })
  })
  it('mapStateToProps should map correctly when contactMenu is not set', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[1][0]({
      dialog: {},
      contracts: {selectedContractContact: 'selectedContractContact'}
    })).toEqual({
      open: false,
      anchorEl: null,
      selectedContractContact: 'selectedContractContact'
    })
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[1][1]).toEqual({
      closeDialog: 'closeDialog',
      openDialog: 'openDialog',
      openConfirmationAlert: 'openConfirmationAlert',
      unAssignContactFromContract: 'unAssignContactFromContract'
    })
  })
})

describe('AssignContactMenuLink', () => {
  let props
  beforeEach(() => {
    props = {
      cell: 'cell',
      row: 'row',
      selectContactContract: jest.fn(),
      selectAndLoadContact: jest.fn(),
      openDialog: jest.fn()
    }
  })
  it('should render correctly', () => {
    const comp = renderer.create(<AssignContactMenuLink {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('on onClick shoud prevent default, select contracts and open dialog', () => {
    const comp = shallow(<AssignContactMenuLink {...props} />)
    const e = {nativeEvent: {preventDefault: jest.fn()}, currentTarget: 'currentTarget'}
    comp.prop('onClick')(e)
    expect(e.nativeEvent.preventDefault).toHaveBeenCalled()
    expect(props.selectContactContract).toHaveBeenCalledWith('row')
    expect(props.selectAndLoadContact).toHaveBeenCalledWith('row')
    expect(props.openDialog).toHaveBeenCalledWith('assignContactMenu', {anchorEl: 'currentTarget'})
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[0][1]).toEqual({
      openDialog: 'openDialog',
      selectContactContract: 'selectContactContract',
      selectAndLoadContact: 'selectAndLoadContact'
    })
  })
})
