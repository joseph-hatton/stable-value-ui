import React from 'react'
import mockComponent from '../../MockComponent'
import {shallow} from 'enzyme'
import renderer from 'react-test-renderer'
import ContractScheduleTabs from '../../../src/contracts/schedule/index'
jest.mock('../../../src/contracts/schedule/Funding', () => 'Funding')
jest.mock('../../../src/contracts/schedule/ScheduleCreditingRate', () => 'ScheduleCreditingRate')
jest.mock('../../../src/contracts/schedule/Withdrawal', () => 'Withdrawal')
jest.mock('../../../src/contracts/schedule/InvestmentGuidelines', () => 'InvestmentGuidelines')
jest.mock('../../../src/contracts/schedule/Termination', () => 'Termination')

describe('ContractScheduleTabs', () => {
    it('Funding should be setup', () => {
        const item = ContractScheduleTabs.find((i) => i.label === 'Funding')
        expect(item).toEqual({'ContractTab': 'Funding', 'label': 'Funding', 'url': 'funding'})
    })
    it('ScheduleCreditingRate should be setup', () => {
        const item = ContractScheduleTabs.find((i) => i.label === 'Crediting Rate')
        expect(item).toEqual({'ContractTab': 'ScheduleCreditingRate', 'label': 'Crediting Rate', 'url': 'crediting-rate'})
    })
    it('Withdrawal should be setup', () => {
        const item = ContractScheduleTabs.find((i) => i.label === 'Withdrawal')
        expect(item).toEqual({'ContractTab': 'Withdrawal', 'label': 'Withdrawal', 'url': 'withdrawal'})
    })
    it('InvestmentGuidelines should be setup', () => {
        const item = ContractScheduleTabs.find((i) => i.label === 'Investment Guidelines')
        expect(item).toEqual({'ContractTab': 'InvestmentGuidelines', 'label': 'Investment Guidelines', 'url': 'investment-guidelines'})
    })
    it('Termination should be setup', () => {
        const item = ContractScheduleTabs.find((i) => i.label === 'Termination')
        expect(item).toEqual({'ContractTab': 'Termination', 'label': 'Termination', 'url': 'termination'})
    })
})
