import React from 'react'
import mockComponent from '../../MockComponent'
import {shallow} from 'enzyme'
import renderer from 'react-test-renderer'
import {SCHEDULE_FUNDING} from '../../../src/contracts/ContractForms'
import {change} from 'redux-form'
import {loadDerivativeTypes} from '../../../src/contracts/ContractActions'
import { InvestmentGuidelines, mapStateToProps, actions } from '../../../src/contracts/schedule/InvestmentGuidelines'
jest.mock('../../../src/components/forms/Validators', () => ({MaxLength: jest.fn(() => 'MaxLength'), toInteger: jest.fn(() => 'toInteger')}))
jest.mock('../../../src/components/forms/AverageCreditQualities', () => mockComponent('AverageCreditQualities'))
jest.mock('../../../src/contracts/RateField', () => mockComponent('RateField'))
jest.mock('redux-form-material-ui', () => ({DatePicker: mockComponent('DatePicker'), TextField: mockComponent('TextField'), Checkbox: mockComponent('Checkbox')}))
jest.mock('redux-form', () => ({Field: mockComponent('Field'), change: jest.fn().mockReturnValue('change')}))
jest.mock('../../../src/contracts/ContractForms', () => ({SCHEDULE_FUNDING: mockComponent('SCHEDULE_FUNDING')}))
jest.mock('../../../src/contracts/edithistory/AuditableContractField', () => mockComponent('AuditableContractField'))
jest.mock('../../../src/contracts/ContractActions', () => ({loadDerivativeTypes: jest.fn()}))

describe('InvestmentGuidelines', () => {
      it('should render InvestmentGuidelines', () => {
        const props = {
            contract: {},
            derivativeTypesRefData: [{}, {}, {}],
            derivativeTypes: [{}, {}, {}],
            loadDerivativeTypes: jest.fn(),
            checkDerivativeTypeChanged: jest.fn(),
            securitiesLendingAllowedChanged: jest.fn(),
            securitiesLendingAllowed: true,
            repurchaseAgreementsAllowedChanged: jest.fn(),
            repurchaseAgreementsAllowed: true
        }
        const component = renderer.create(<InvestmentGuidelines {...props}/>).toJSON()
        expect(component).toMatchSnapshot()
    })

    it('should call the function when click CreditingRate', () => {
        const props = {
            contract: {},
            derivativeTypesRefData: [{}, {}, {}],
            derivativeTypes: [{}, {}, {}],
            loadDerivativeTypes: jest.fn(),
            checkDerivativeTypeChanged: jest.fn(),
            securitiesLendingAllowedChanged: jest.fn(),
            securitiesLendingAllowed: true,
            repurchaseAgreementsAllowedChanged: jest.fn(),
            repurchaseAgreementsAllowed: true
        }
        const component = shallow(<InvestmentGuidelines {...props}/>)
        expect(props.securitiesLendingAllowedChanged).not.toHaveBeenCalled()
        component.find({id: 'securitiesLendingAllowedId'}).prop('onChange')()
        expect(props.securitiesLendingAllowedChanged).toHaveBeenCalled()
    })
})

describe('InvestmentGuidelines - actions ', () => {
    let dispatch, getState, state
    beforeEach(() => {
        state = {form: {}}
        dispatch = jest.fn()
        getState = jest.fn().mockReturnValue(state)
    })
    it('should InvestmentGuidelines checkDerivativeTypeChanged', () => {
        const e = {preventDefault: jest.fn()}
        const dtype = ''
        const contract = {contract: {}}
        state = {contracts: {contract}}
        getState.mockReturnValue(state)
        actions.checkDerivativeTypeChanged(e, dtype)(dispatch, getState)
        expect(change).toHaveBeenCalled()
        expect(change).toHaveBeenCalledWith(SCHEDULE_FUNDING, 'TREASURY_FUTURES', 'checked')
    })

    it('should InvestmentGuidelines securitiesLendingAllowedChanged', () => {
        const e = {preventDefault: jest.fn()}
        const securitiesLendingAllowed = null
        const contract = {contract: {}}
        state = {contracts: {contract}}
        getState.mockReturnValue(state)
        actions.securitiesLendingAllowedChanged(e, securitiesLendingAllowed)(dispatch, getState)
        expect(change).toHaveBeenCalled()
        expect(change).toHaveBeenCalledWith(SCHEDULE_FUNDING, 'securitiesLendingAllowed', null)
    })

    it('should InvestmentGuidelines repurchaseAgreementsAllowedChanged', () => {
        const e = {preventDefault: jest.fn()}
        const repurchaseAgreementsAllowed = null
        const contract = {contract: {}}
        state = {contracts: {contract}}
        getState.mockReturnValue(state)
        actions.repurchaseAgreementsAllowedChanged(e, repurchaseAgreementsAllowed)(dispatch, getState)
        expect(change).toHaveBeenCalled()
        expect(change).toHaveBeenCalledWith(SCHEDULE_FUNDING, 'repurchaseAgreementsAllowed', repurchaseAgreementsAllowed)
    })
})

describe('InvestmentGuidelines - mapStateToProps', () => {
    it('mapStateToProps', () => {
        const derivativeTypes = [{}, {}, {}]

        const state = {form: {scheduleFunding: {values: {securitiesLendingAllowed: 'test', repurchaseAgreementsAllowed: 'test'}}},
             referenceData: {derivativeTypes}}
        const props = mapStateToProps(state)
        expect(props.derivativeTypesRefData).toEqual(derivativeTypes)
        expect(props.repurchaseAgreementsAllowed).toEqual('test')
        expect(props.securitiesLendingAllowed).toEqual('test')
    })
})
