import React from 'react'
import mockComponent from '../../MockComponent'
import {shallow} from 'enzyme'
import renderer from 'react-test-renderer'
import { SCHEDULE_FUNDING } from '../../../src/contracts/ContractForms'
import { Withdrawal, mapStateToProps, actions } from '../../../src/contracts/schedule/Withdrawal'
import {change} from 'redux-form'
jest.mock('redux-form', () => ({Field: mockComponent('Field'), change: jest.fn().mockReturnValue('change')}))
jest.mock('redux-form-material-ui', () => ({TextField: mockComponent('TextField')}))
jest.mock('../../../src/components/forms/Validators', () => ({MaxLength: jest.fn(() => 'MaxLength'), toInteger: jest.fn(() => 'toInteger')}))
jest.mock('../../../src/contracts/RateField', () => mockComponent('RateField'))
jest.mock('../../../src/contracts/ContractForms', () => ({SCHEDULE_FUNDING: mockComponent('SCHEDULE_FUNDING')}))
jest.mock('../../../src/contracts/ContractRefDataFields', () => ({CorridorOptionCalculationMethods: mockComponent('CorridorOptionCalculationMethods')}))

describe('Withdrawal', () => {
    let dispatch, getState, state
    beforeEach(() => {
        state = {form: {}}
        dispatch = jest.fn()
        getState = jest.fn().mockReturnValue(state)
    })
    it('should render Withdrawal', () => {
        const props = {
            corrOption: true,
            corrOptionChanged: jest.fn(),
            corrOptionCalcMethod: '',
            corrOptionCalcMethodChanged: jest.fn()
        }

        const component = renderer.create(<Withdrawal {...props}/>).toJSON()
        expect(component).toMatchSnapshot()
    })

    it('should corrOptionChanged onChange', () => {
        const props = {
            corrOption: true,
            corrOptionChanged: jest.fn(),
            corrOptionCalcMethod: '',
            corrOptionCalcMethodChanged: jest.fn()
        }

        const component = shallow(<Withdrawal {...props}/>)
        expect(props.corrOptionChanged).not.toHaveBeenCalled()
        component.find({id: 'yesNoId'}).prop('onChange')()
        expect(props.corrOptionChanged).toHaveBeenCalled()
    })
    it('should corrOptionCalcMethodChanged onChange', () => {
        const corrOptionCalcMethod = 'ROLLING'
        const props = {
            corrOption: true,
            corrOptionChanged: jest.fn(),
            corrOptionCalcMethodChanged: jest.fn(),
            corrOptionCalcMethod
        }

        const component = shallow(<Withdrawal {...props}/>)
        expect(props.corrOptionCalcMethodChanged).not.toHaveBeenCalled()
        component.find({id: 'corrOptionRollingMonthsId'}).prop('onChange')()
        expect(props.corrOptionCalcMethodChanged).toHaveBeenCalled()
    })
})

describe('Withdrawal - Actions', () => {
    let dispatch, getState, state
    beforeEach(() => {
        state = {form: {}}
        dispatch = jest.fn()
        getState = jest.fn().mockReturnValue(state)
    })

    it('actions corrOptionChanged corrOption', () => {
        const e = {preventDefault: jest.fn()}
        const corrOption = 'test'
        const contract = {contract: {}}
        state = {contracts: {contract}}
        getState.mockReturnValue(state)
        actions.corrOptionChanged(e, corrOption)(dispatch, getState)
        expect(change).toHaveBeenCalled()
        expect(change).toHaveBeenCalledWith(SCHEDULE_FUNDING, 'corrOptionRollingMonths', undefined)
        expect(change).toHaveBeenCalledWith(SCHEDULE_FUNDING, 'corrOptionCalcMethod', undefined)
    })
    it('actions corrOptionCalcMethodChanged corrOptionCalcMethod', () => {
        const e = {preventDefault: jest.fn()}
        const corrOptionCalcMethod = 'ROLLING'
        const contract = {contract: {}}
        state = {contracts: {contract}}
        getState.mockReturnValue(state)
        actions.corrOptionCalcMethodChanged(e, corrOptionCalcMethod)(dispatch, getState)
        expect(change).toHaveBeenCalled()
        expect(change).toHaveBeenCalledWith(SCHEDULE_FUNDING, 'corrOptionRollingMonths', undefined)
    })
})

describe('CreditingRate - mapStateToProps', () => {
    it('mapStateToProps', () => {
        const state = {
            form: {scheduleFunding: {values: {corrOption: 1, corrOptionCalcMethod: 'TYPE'}}}
        }
        const props = mapStateToProps(state)
        expect(props).toEqual({'corrOption': 1, 'corrOptionCalcMethod': 'TYPE'})
    })
})
