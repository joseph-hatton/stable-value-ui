import React from 'react'
import mockComponent from '../../MockComponent'
import {shallow} from 'enzyme'
import renderer from 'react-test-renderer'
import { change } from 'redux-form'
import { connect } from 'react-redux'
import NumberField from '../../../src/contracts/NumberField'
import { Funding, mapStateToProps, actions } from '../../../src/contracts/schedule/Funding'
jest.mock('redux-form', () => ({Field: mockComponent('Field'), change: jest.fn().mockReturnValue('change')}))
jest.mock('../../../src/components/forms/Validators', () => ({MaxLength: jest.fn(() => 'MaxLength'), Required: jest.fn(() => 'Required')}))
jest.mock('../../../src/contracts/ContractRefDataFields', () => ({AdditionalDeposits: mockComponent('AdditionalDeposits')}))
jest.mock('redux-form-material-ui', () => ({TextField: mockComponent('TextField')}))
jest.mock('../../../src/contracts/NumberField', () => mockComponent('NumberField'))

describe('Funding', () => {
    it('should Funding when it has deposit limit ', () => {
        const props = {
            contract: {},
            additionalDeposits: 'UP_TO_LIMIT',
            depositLimit: 3000000,
            additionalDepositsChange: jest.fn()
        }
        const component = renderer.create(<Funding {...props}/>).toJSON()
        expect(component).toMatchSnapshot()
    })
    it('should Funding when it has not deposit limit ', () => {
        const props = {
            contract: {},
            additionalDeposits: 'ROLLING',
            depositLimit: null,
            additionalDepositsChange: jest.fn()
        }
        const component = renderer.create(<Funding {...props}/>).toJSON()
        expect(component).toMatchSnapshot()
    })
})
describe('Funding -mapStateToProps ', () => {
    it('should mapStateToProps Map Corr ', () => {
        const state = {form: {scheduleFunding: {values: {additionalDeposits: 'UP_TO_LIMIT', depositLimit: 30020000}}}}
        const props = mapStateToProps(state)
        expect(props.additionalDeposits).toEqual('UP_TO_LIMIT')
        expect(props.depositLimit).toEqual(30020000)
    })
})
describe('Funding - actions ', () => {
    let dispatch, getState, state
    beforeEach(() => {
        state = {form: {}}
        dispatch = jest.fn()
        getState = jest.fn().mockReturnValue(state)
    })

    it('action-additionalDepositsChange ', () => {
        state = {form: {scheduleFunding: {values: {additionalDeposits: 'UP_TO_LIMIT', depositLimit: 30020000}}}}
        const e = {preventDefault: jest.fn()}
        const value = 'ROLLING'
        const contract = {contract: {}}
        state = {contracts: {contract}}
        getState.mockReturnValue(state)
        actions.additionalDepositsChange(e, value)(dispatch)
        expect(change).toHaveBeenCalled()
        expect(change).toHaveBeenCalledWith('scheduleFunding', 'depositLimit', null)
    })

    it('action-additionalDepositsChange  UP_TO_LIMIT', () => {
        state = {form: {scheduleFunding: {values: {additionalDeposits: 'UP_TO_LIMIT', depositLimit: 30020000}}}}
        const e = {preventDefault: jest.fn()}
        const value = 'UP_TO_LIMIT'
        const contract = {contract: {}}
        state = {contracts: {contract}}
        change.mockClear()
        getState.mockReturnValue(state)
        actions.additionalDepositsChange(e, value)(dispatch)
        expect(change).not.toHaveBeenCalled()
    })
})
