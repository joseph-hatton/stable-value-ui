import React from 'react'
import mockComponent from '../../MockComponent'
import {shallow} from 'enzyme'
import renderer from 'react-test-renderer'
import Termination from '../../../src/contracts/schedule/Termination'
jest.mock('redux-form', () => ({Field: mockComponent('Field')}))
jest.mock('redux-form-material-ui', () => ({TextField: mockComponent('TextField')}))
jest.mock('../../../src/contracts/RateField', () => mockComponent('RateField'))
jest.mock('../../../src/components/forms/Validators', () => ({MaxLength: jest.fn(() => 'MaxLength')}))
jest.mock('../../../src/components/forms/CalendarTypes', () => mockComponent('CalendarTypes'))

describe('Termination', () => {
    it('should render Termination', () => {
        const component = renderer.create(<Termination />).toJSON()
        expect(component).toMatchSnapshot()
    })
})
