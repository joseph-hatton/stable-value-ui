import React from 'react'
import mockComponent from '../../MockComponent'
import {shallow} from 'enzyme'
import renderer from 'react-test-renderer'
import { CreditingRate, mapStateToProps, actions } from '../../../src/contracts/schedule/ScheduleCreditingRate'
import {change} from 'redux-form'
import {SCHEDULE_FUNDING} from '../../../src/contracts/ContractForms'
jest.mock('react-redux', () => ({connect: jest.fn(() => jest.fn(component => component))}))
jest.mock('redux-form', () => ({Field: mockComponent('Field'), change: jest.fn().mockReturnValue('change')}))
jest.mock('redux-form-material-ui', () => ({DatePicker: mockComponent('DatePicker'), TextField: mockComponent('TextField')}))
jest.mock('../../../src/components/forms/Validators', () => ({MaxLength: jest.fn(() => 'MaxLength')}))
jest.mock('../../../src/contracts/RateField', () => mockComponent('RateField'))
jest.mock('../../../src/contracts/edithistory/AuditableContractField', () => mockComponent('AuditableContractField'))
jest.mock('../../../src/contracts/ContractForms', () => ({SCHEDULE_FUNDING: mockComponent('SCHEDULE_FUNDING')}))

jest.mock('../../../src/contracts/ContractRefDataFields', () => ({
    CreditingRateCalcMethods: mockComponent('CreditingRateCalcMethods'),
    CalculationMethodInputDefinitions: mockComponent('CalculationMethodInputDefinitions'),
    CreditingRateRoundingDecimals: mockComponent('CreditingRateRoundingDecimals'),
    CreditingRateInputBases: mockComponent('CreditingRateInputBases'),
    CreditingRateFrequencies: mockComponent('CreditingRateFrequencies'),
    CreditingRateMonths: mockComponent('CreditingRateMonths'),
    CreditingRateTypes: mockComponent('CreditingRateTypes')
}))

describe('CreditingRate', () => {
    it('should render CreditingRate', () => {
       const props = {
            creditingRateFrequency: 'Freq',
            creditingRateFrequencyChanged: jest.fn(),
            creditingRateType: 'Type',
            creditingRateTypeChanged: jest.fn(),
            netOfThirdPartyFeesChanged: jest.fn(),
            netOfThirdPartyFees: false
        }
        const component = renderer.create(<CreditingRate {...props}/>).toJSON()
        expect(component).toMatchSnapshot()
    })

    it('should creditingRateFrequencyChanged on change', () => {
        const props = {
            creditingRateFrequency: 'Freq',
            creditingRateFrequencyChanged: jest.fn(),
            creditingRateType: 'Type',
            creditingRateTypeChanged: jest.fn(),
            netOfThirdPartyFeesChanged: jest.fn(),
            netOfThirdPartyFees: false
        }
        const component = shallow(<CreditingRate {...props}/>)
        expect(props.creditingRateFrequencyChanged).not.toHaveBeenCalled()
        component.find({id: 'creditingRateFrequencyId'}).prop('onChange')()
        expect(props.creditingRateFrequencyChanged).toHaveBeenCalled()
    })

    it('should netOfThirdPartyFeesChanged on change', () => {
        const props = {
            creditingRateFrequency: 'Freq',
            creditingRateFrequencyChanged: jest.fn(),
            creditingRateType: 'Type',
            creditingRateTypeChanged: jest.fn(),
            netOfThirdPartyFeesChanged: jest.fn(),
            netOfThirdPartyFees: false
        }
        const component = shallow(<CreditingRate {...props}/>)
        expect(props.netOfThirdPartyFeesChanged).not.toHaveBeenCalled()
        component.find({id: 'yesNoId'}).prop('onChange')()
        expect(props.netOfThirdPartyFeesChanged).toHaveBeenCalled()
    })
})
describe('CreditingRate - actions', () => {
    let dispatch, getState, state
    beforeEach(() => {
        state = {form: {}}
        dispatch = jest.fn()
        getState = jest.fn().mockReturnValue(state)
    })

    it('actions creditingRateFrequencyChanged', () => {
        const e = {preventDefault: jest.fn()}
        const frequency = 'DAILY'
        const contract = {contract: {}}
         state = {contracts: {contract}}
        getState.mockReturnValue(state)
        actions.creditingRateFrequencyChanged(e, frequency)(dispatch, getState)
        expect(change).toHaveBeenCalled()
        expect(change).toHaveBeenCalledWith(SCHEDULE_FUNDING, 'creditingRateMonth', null)
    })

    it('actions creditingRateFrequencyChanged NO DAILY', () => {
        const e = {preventDefault: jest.fn()}
        const frequency = 'NO-DAILY'
        const contract = {contract: {}}
        state = {contracts: {contract}}
        getState.mockReturnValue(state)
        actions.creditingRateFrequencyChanged(e, frequency)(dispatch, getState)
        expect(change).toHaveBeenCalled()
        expect(change).toHaveBeenCalledWith(SCHEDULE_FUNDING, 'creditingRateFirstReset', undefined)
    })

    it('actions creditingRateTypeChanged GROSS', () => {
        const e = {preventDefault: jest.fn()}
        const creditingRateType = 'GROSS'
        const contract = {contract: {}}
        state = {contracts: {contract}}
        getState.mockReturnValue(state)
        actions.creditingRateTypeChanged(e, creditingRateType)(dispatch, getState)
        expect(change).toHaveBeenCalled()
        expect(change).toHaveBeenCalledWith(SCHEDULE_FUNDING, 'netOfThirdPartyFees', null)
    })

    it('actions creditingRateTypeChanged no GROSS', () => {
        const e = {preventDefault: jest.fn()}
        const creditingRateType = 'no GROSS'
        const contract = {contract: {}}
        state = {contracts: {contract}}
        getState.mockReturnValue(state)
        actions.creditingRateTypeChanged(e, creditingRateType)(dispatch, getState)
        expect(change).toHaveBeenCalled()
        expect(change).toHaveBeenCalledWith(SCHEDULE_FUNDING, 'netOfThirdPartyFees', undefined)
    })

    it('actions netOfThirdPartyFeesChanged sw', () => {
        change.mockClear()
        const e = {preventDefault: jest.fn()}
        const sw = {exist: true}
        const contract = {contract: {}}
        state = {contracts: {contract}}
        getState.mockReturnValue(state)
        actions.netOfThirdPartyFeesChanged(e, sw)(dispatch)
        expect(change).not.toHaveBeenCalled()
    })
    it('actions netOfThirdPartyFeesChanged sw called change', () => {
        const e = {preventDefault: jest.fn()}
        const sw = false
        const contract = {contract: {}}
        state = {contracts: {contract}}
        getState.mockReturnValue(state)
        actions.netOfThirdPartyFeesChanged(e, sw)(dispatch)
        expect(change).toHaveBeenCalled()
    })
})

describe('CreditingRate - mapStateToProps', () => {
    it('mapStateToProps', () => {
        const state = {
        form: {scheduleFunding: {values: {creditingRateFrequency: 1, creditingRateType: 'TYPE', netOfThirdPartyFees: 123}}}
    }
    const props = mapStateToProps(state)
    expect(props).toEqual({'creditingRateFrequency': 1, 'creditingRateType': 'TYPE', 'netOfThirdPartyFees': 123})
    })
})
