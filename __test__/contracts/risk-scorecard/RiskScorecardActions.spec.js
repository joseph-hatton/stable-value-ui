import http from '../../../src/actions/http'
import {apiPath} from '../../../src/config'

import {loadContractRiskScorecards} from '../../../src/contracts/risk-scorecard/RiskScorecardActions'

jest.mock('../../../src/actions/http', () => ({
  get: jest.fn()
}))

describe('RiskScorecardActions', () => {
  beforeEach(() => {
    http.get.mockReturnValue({then: (cb) => cb({results: 'results'})}) // eslint-disable-line standard/no-callback-literal
  })
  it('loadContractRiskScorecards should dispatch', () => {
    const dispatch = jest.fn()

    loadContractRiskScorecards(123)(dispatch)

    expect(dispatch.mock.calls[0][0].type).toBe('LOAD_CONTRACT_RISK_SCORECARDS')
    expect(dispatch.mock.calls[0][0].payload).toBe('results')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/contracts/123/risk-scorecards`)
  })
})
