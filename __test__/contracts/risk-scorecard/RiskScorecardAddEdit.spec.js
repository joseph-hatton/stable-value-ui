import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'
import canModify from '../../../src/contracts/risk-scorecard/canModify'

import RiskScorecardAddEdit from '../../../src/contracts/risk-scorecard/RiskScorecardAddEdit'

jest.mock('react-redux', () => ({
  connect: jest.fn(() => (comp) => comp)
}))

jest.mock('redux-form', () => ({
  Field: mockComponent('Field'),
  reduxForm: () => (comp) => comp,
  FormSection: mockComponent('FormSection')
}))

jest.mock('material-ui/RaisedButton', () => mockComponent('RaisedButton'))
jest.mock('../../../src/components/redux-dialog', () => mockComponent('ReduxDialog'))
jest.mock('../../../src/contracts/risk-scorecard/RiskScorecardEditRow', () => mockComponent('RiskScorecardEditRow'))
jest.mock('../../../src/contracts/risk-scorecard/canModify', () => jest.fn())

describe('RiskScorecardAddEdit', () => {
  let props
  beforeEach(() => {
    props = {
      handleSubmit: jest.fn(),
      onClose: jest.fn(),
      saveRiskScorecardInput: jest.fn(),
      status: true,
      contractType: 'contractType',
      overallUnadjustedScore: 85,
      overallUnadjustedGrade: 'B',
      overallAdjustedScore: 95,
      overallAdjustedGrade: 'A'
    }
    canModify.mockReturnValue(true)
  })
  it('should render correctly', () => {
    const component = renderer.create(<RiskScorecardAddEdit {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should disable correctly', () => {
    canModify.mockReturnValue(false)
    const component = renderer.create(<RiskScorecardAddEdit {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should render correctly for contractType Pooled Plan', () => {
    props.contractType = 'POOLED_PLAN'
    const component = renderer.create(<RiskScorecardAddEdit {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
})
