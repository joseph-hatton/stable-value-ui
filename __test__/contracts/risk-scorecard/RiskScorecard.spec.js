import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'
import {shallow} from 'enzyme'
import canModify from '../../../src/contracts/risk-scorecard/canModify'

import RiskScorecard from '../../../src/contracts/risk-scorecard/RiskScorecard'

jest.mock('react-redux', () => ({
  connect: jest.fn(() => (comp) => comp)
}))

jest.mock('../../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn()
}))

jest.mock('../../../src/contracts/ContractActions', () => ({
  clearRiskScorecardInputEditForm: jest.fn(),
  selectRiskScorecardInputRow: jest.fn()
}))

jest.mock('../../../src/contracts/risk-scorecard/RiskScorecardActions', () => ({
  loadContractRiskScorecards: jest.fn()
}))

jest.mock('../../../src/contracts/risk-scorecard/RiskScorecardColumns', () => ({
  RiskScorecardColumns: jest.fn().mockReturnValue('RiskScorecardColumns')
}))

jest.mock('../../../src/contracts/risk-scorecard/canModify', () => jest.fn())

jest.mock('../../../src/contracts/risk-scorecard/RiskScorecardAddEdit', () => mockComponent('RiskScorecardAddEdit'))
jest.mock('material-ui/FloatingActionButton', () => mockComponent('FloatingActionButton'))
jest.mock('../../../src/contracts/risk-scorecard/RiskScorecardMenu', () => mockComponent('RiskScorecardMenu'))
jest.mock('../../../src/watchLists/watchListAlert/WatchListAlert', () => mockComponent('WatchListAlert'))
jest.mock('material-ui/svg-icons/content/add', () => mockComponent('ContentAdd'))

describe('RiskScorecard', () => {
  let props
  beforeEach(() => {
    props = {
      contractId: 123,
      status: true,
      contractType: 'contractType',
      riskScorecards: [],
      loadContractRiskScorecards: jest.fn(),
      clearRiskScorecardInputEditForm: jest.fn(),
      selectRiskScorecardInputRow: jest.fn(),
      openDialog: jest.fn(),
      closeDialog: jest.fn(),
      destroy: jest.fn(),
      checkWatchList: jest.fn()
    }
    canModify.mockReturnValue(true)
  })
  it('should render correctly', () => {
    const component = renderer.create(<RiskScorecard {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should load correctly', () => {
    const component = renderer.create(<RiskScorecard {...props} />)
    expect(props.loadContractRiskScorecards).toHaveBeenCalled()
  })
  it('should respond correctly when closing addEdit', () => {
    const component = shallow(<RiskScorecard {...props} />)

    component.children('#RiskScorecard-RiskScorecardAddEdit').prop('onClose')()

    expect(props.clearRiskScorecardInputEditForm).toHaveBeenCalled()
    expect(props.destroy).toHaveBeenCalled()
    expect(props.closeDialog).toHaveBeenCalled()
  })
  it('should hide fab correctly', () => {
    canModify.mockReturnValue(false)
    const component = renderer.create(<RiskScorecard {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should hide fab correctly', () => {
    const component = shallow(<RiskScorecard {...props} />)

    component.children('#RiskScorecard-FAB').prop('onClick')()

    expect(props.selectRiskScorecardInputRow).toHaveBeenCalled()
    expect(props.openDialog).toHaveBeenCalled()
    expect(props.checkWatchList).toHaveBeenCalled()
  })
})
