import {hasEntitlement} from '../../../src/entitlements'

import canModify from '../../../src/contracts/risk-scorecard/canModify'

jest.mock('../../../src/entitlements', () => ({
  hasEntitlement: jest.fn().mockReturnValue(true)
}))

describe('canModify', () => {
  it('should evauluate correctly', () => {
    expect(canModify({status: 'ACTIVE'})).toBe(true)
  })
  it('should evauluate correctly', () => {
    expect(canModify({status: 'PENDING'})).toBe(true)
  })
  it('should evauluate correctly', () => {
    expect(canModify({status: 'ANYTHING'})).toBe(false)
  })
  it('should evauluate correctly', () => {
    hasEntitlement.mockReturnValue(false)
    expect(canModify({status: 'ACTIVE'})).toBe(false)
  })
})
