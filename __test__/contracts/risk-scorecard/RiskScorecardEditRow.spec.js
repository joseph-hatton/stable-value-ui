import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'

import RiskScorecardEditRow from '../../../src/contracts/risk-scorecard/RiskScorecardEditRow'

jest.mock('react-redux', () => ({
  connect: jest.fn(() => (comp) => comp)
}))

jest.mock('redux-form', () => ({
  Field: mockComponent('Field'),
  FormSection: mockComponent('FormSection')
}))

jest.mock('../../../src/contracts/RateField', () => mockComponent('RateField'))

describe('RiskScorecardEditRow', () => {
  let props
  beforeEach(() => {
    props = {
      label: 'label',
      name: 'name',
      hidden: false,
      gradeRiskScorecard: jest.fn(),
      riskScorecard: {}
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<RiskScorecardEditRow {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should render correctly if hidden', () => {
    props.hidden = true
    const component = renderer.create(<RiskScorecardEditRow {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
})
