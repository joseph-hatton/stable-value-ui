import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from '../../MockComponent'
import canModify from '../../../src/contracts/risk-scorecard/canModify'
import {connect} from 'react-redux'

import RiskScorecardMenu, {RiskScorecardMenuLink} from '../../../src/contracts/risk-scorecard/RiskScorecardMenu'

jest.mock('react-redux', () => ({
  connect: jest.fn(() => (comp) => comp)
}))

jest.mock('../../../src/watchLists/watchListAlert/WatchListAlertActions', () => ({
  checkWatchList: jest.fn()
}))

jest.mock('../../../src/contracts/ContractActions', () => ({
  selectRiskScorecardInputRow: jest.fn(),
  deleteRiskScorecard: jest.fn()
}))

jest.mock('../../../src/components/redux-dialog/redux-dialog', () => ({
  openDialog: jest.fn(),
  closeDialog: jest.fn()
}))

jest.mock('material-ui', () => ({
  Popover: mockComponent('Popover'),
  Menu: mockComponent('Menu'),
  MenuItem: mockComponent('MenuItem')
}))

jest.mock('material-ui/svg-icons/editor/mode-edit', () => mockComponent('EditIcon'))
jest.mock('material-ui/svg-icons/action/delete', () => mockComponent('DeleteIcon'))
jest.mock('../../../src/contracts/risk-scorecard/canModify', () => jest.fn())

describe('RiskScorecardMenu', () => {
  let props
  beforeEach(() => {
    props = {
      contract: {contractId: 123},
      open: true,
      closeDialog: jest.fn(),
      openDialog: jest.fn(),
      selectRiskScorecardInputRow: jest.fn(),
      deleteRiskScorecard: jest.fn(),
      checkWatchList: jest.fn(),
      riskScorecard: 'riskScorecard',
      anchorEl: 'anchorEl'
    }
    canModify.mockReturnValue(true)
  })
  it('should render correctly', () => {
    const component = renderer.create(<RiskScorecardMenu {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('onRequestClose should run correctly', () => {
    const component = shallow(<RiskScorecardMenu {...props} />)

    component.prop('onRequestClose')()

    expect(props.closeDialog).toHaveBeenCalled()
    expect(props.closeDialog.mock.calls[0][0]).toBe('riskScorecardMenu')
  })
  it('onClick Edit should run correctly', () => {
    const component = shallow(<RiskScorecardMenu {...props} />)

    component.find('#rsMenu-viewEditRiskScorecardButton').prop('onClick')()

    expect(props.closeDialog).toHaveBeenCalled()
    expect(props.closeDialog.mock.calls[0][0]).toBe('riskScorecardMenu')
    expect(props.openDialog).toHaveBeenCalled()
    expect(props.openDialog.mock.calls[0][0]).toBe('riskScorecardInput')
    expect(props.checkWatchList).toHaveBeenCalled()
    expect(props.checkWatchList.mock.calls[0][0]).toBe(123)
  })
  it('onClick Delete should run correctly', () => {
    const component = shallow(<RiskScorecardMenu {...props} />)

    component.find('#rsMenu-DeleteRiskScorecardButton').prop('onClick')()

    expect(props.deleteRiskScorecard).toHaveBeenCalled()
    expect(props.deleteRiskScorecard.mock.calls[0][0]).toBe('riskScorecard')
    expect(props.closeDialog).toHaveBeenCalled()
    expect(props.closeDialog.mock.calls[0][0]).toBe('riskScorecardMenu')
  })
  it('should render correctly when delete is disabled', () => {
    canModify.mockReturnValue(false)
    const component = renderer.create(<RiskScorecardMenu {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('mapStateToProps should map correctly', () => {
    expect(connect.mock.calls.length).toBe(2)

    const state = {
      dialog: {
        riskScorecardMenu: {anchorEl: 'anchorEl'}
      },
      contracts: {
        riskScorecard: 'riskScorecard',
        contract: 'contract'
      }
    }

    const response = connect.mock.calls[1][0](state)

    expect(response.open).toBe(true)
    expect(response.anchorEl).toBe('anchorEl')
    expect(response.riskScorecard).toBe('riskScorecard')
    expect(response.contract).toBe('contract')
  })
  it('mapStateToProps should map correctly', () => {
    expect(connect.mock.calls.length).toBe(2)

    const state = {
      dialog: {
        riskScorecardMenu: null
      },
      contracts: {
        riskScorecard: 'riskScorecard',
        contract: 'contract'
      }
    }

    const response = connect.mock.calls[1][0](state)

    expect(response.open).toBe(false)
    expect(response.anchorEl).toBe(null)
  })
})

describe('RiskScorecardMenuLink', () => {
  let props
  beforeEach(() => {
    props = {
      cell: 'cell',
      row: {id: 123},
      openDialog: jest.fn(),
      selectRiskScorecardInputRow: jest.fn()
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<RiskScorecardMenuLink {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('onClick should run correctly', () => {
    const component = shallow(<RiskScorecardMenuLink {...props} />)
    const e = {nativeEvent: {preventDefault: jest.fn()}, currentTarget: 'currentTarget'}

    component.prop('onClick')(e)

    expect(e.nativeEvent.preventDefault).toHaveBeenCalled()
    expect(props.selectRiskScorecardInputRow).toHaveBeenCalled()
    expect(props.selectRiskScorecardInputRow.mock.calls[0][0]).toEqual({id: 123})
    expect(props.openDialog).toHaveBeenCalled()
    expect(props.openDialog.mock.calls[0][0]).toBe('riskScorecardMenu')
    expect(props.openDialog.mock.calls[0][1]).toEqual({anchorEl: 'currentTarget'})
  })
})
