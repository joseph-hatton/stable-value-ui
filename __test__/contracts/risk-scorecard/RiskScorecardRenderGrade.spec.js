import RiskScorecardRenderGrade from '../../../src/contracts/risk-scorecard/RiskScorecardRenderGrade'

describe('RiskScorecardRenderGrade', () => {
  it('should ready value correctly', () => {
    const result = RiskScorecardRenderGrade({grade: 'A'})
    expect(result).toBe('A')
  })
})
