import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'

import {RiskScorecardColumns} from '../../../src/contracts/risk-scorecard/RiskScorecardColumns'

jest.mock('../../../src/contracts/risk-scorecard/RiskScorecardRenderGrade', () => 'RiskScorecardRenderGrade')

jest.mock('../../../src/contracts/risk-scorecard/RiskScorecardMenu', () => ({
  RiskScorecardMenuLink: mockComponent('RiskScorecardMenuLink')
}))

jest.mock('../../../src/components/utilities/Formatters', () => ({
  dateFormat: jest.fn((item) => `dateFormat(${item})`)
}))

describe('RiskScorecardColumns', () => {
  it('RiskScorecardRenderGrade', () => {
    const fieldsToCheck = [
      'cashFlow',
      'demographics',
      'competingFunds',
      'planSponsor',
      'planSvFundBalances',
      'planDesign',
      'pooledFundBalances',
      'pooledFundDesign',
      'fundManagement',
      'wrappedPortfolio',
      'marketBookRatio',
      'overallUnadjusted',
      'overallAdjusted'
    ]

    const cols = RiskScorecardColumns('contractType')

    fieldsToCheck.forEach((field) => {
      const col = cols.find((i) => i.field === field)
      expect(col.renderer).toBe('RiskScorecardRenderGrade')
    })
  })
  it('RiskScorecardMenuLink', () => {
    const col = RiskScorecardColumns('contractType').find((i) => i.field === 'effectiveDate')
    const component = renderer.create(col.renderer('cell', 'row'))
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('contractType Pooled Plan', () => {
    const fieldsToCheck = [
      'planSponsor',
      'planSvFundBalances',
      'planDesign'
    ]

    const cols = RiskScorecardColumns('POOLED_PLAN')

    fieldsToCheck.forEach((field) => {
      const col = cols.find((i) => i.field === field)
      expect(col.customProps.hidden).toBe(true)
    })
  })
  it('contractType not Pooled Plan', () => {
    const fieldsToCheck = [
      'planSponsor',
      'planSvFundBalances',
      'planDesign'
    ]

    const cols = RiskScorecardColumns('NOT_POOLED_PLAN')

    fieldsToCheck.forEach((field) => {
      const col = cols.find((i) => i.field === field)
      expect(col.customProps.hidden).toBe(false)
    })
  })
  it('contractType Pooled Plan', () => {
    const fieldsToCheck = [
      'pooledFundBalances',
      'pooledFundDesign'
    ]

    const cols = RiskScorecardColumns('POOLED_PLAN')

    fieldsToCheck.forEach((field) => {
      const col = cols.find((i) => i.field === field)
      expect(col.customProps.hidden).toBe(false)
    })
  })
  it('contractType not Pooled Plan', () => {
    const fieldsToCheck = [
      'pooledFundBalances',
      'pooledFundDesign'
    ]

    const cols = RiskScorecardColumns('NOT_POOLED_PLAN')

    fieldsToCheck.forEach((field) => {
      const col = cols.find((i) => i.field === field)
      expect(col.customProps.hidden).toBe(true)
    })
  })
})
