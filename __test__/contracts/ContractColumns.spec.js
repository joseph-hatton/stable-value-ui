import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'

import {ContractColumns} from '../../src/contracts/ContractColumns'

jest.mock('../../src/components/grid', () => ({
  dateFormat: 'dateFormat'
}))

jest.mock('react-router', () => ({
  Link: mockComponent('Link')
}))

describe('ContractColumns', () => {
  it('contractNumber renderer should create a link to specifications', () => {
    const item = ContractColumns.find((i) => i.field === 'contractNumber')
    const comp = renderer.create(item.renderer('cell', {contractId: 'contractId'}))
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('effectiveDate renderer should be dateFormat', () => {
    const item = ContractColumns.find((i) => i.field === 'effectiveDate')
    expect(item.renderer).toBe('dateFormat')
  })
  it('there should be 7 columns', () => {
    expect(ContractColumns.length).toBe(7)
  })
})
