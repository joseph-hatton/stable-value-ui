import React from 'react'
import mockConnect from '../MockConnect'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import {connect} from 'react-redux'
import {shallow} from 'enzyme'
import _ from 'lodash'

import ContractContainer from '../../src/contracts/ContractContainer'

jest.mock('react-redux', () => mockConnect())
jest.mock('../../src/contracts/ContractActions', () => ({
  loadContract: 'loadContract',
  clearEditContractState: 'clearEditContractState',
  hasEditHistory: jest.fn().mockReturnValue(true),
  canNavigateAwayFromContracts: 'canNavigateAwayFromContracts',
  leaveWithoutSaving: 'leaveWithoutSaving',
  resetLeaveWithoutSaving: 'resetLeaveWithoutSaving',
  askToNotSaveContract: 'askToNotSaveContract'
}))
jest.mock('../../src/contacts/ContactActions', () => ({
  loadAllContacts: 'loadAllContacts'
}))
jest.mock('react-router', () => ({
  withRouter: (comp) => comp
}))
jest.mock('../../src/contracts/ContractToolbar', () => mockComponent('ContractToolbar'))
jest.mock('../../src/contracts/edithistory/FieldHistory', () => mockComponent('FieldHistory'))
jest.mock('../../src/components/common/LeftDrawerActions', () => ({
  setOrToggleListItem: 'setOrToggleListItem'
}))
jest.mock('../../src/components/redux-dialog/redux-dialog', () => ({
  openConfirmationAlert: 'openConfirmationAlert'
}))

describe('ContractContainer', () => {
  let props
  beforeEach(() => {
    props = {
      location: {
        pathname: 'pathname'
      },
      params: {},
      loadAllContacts: jest.fn(),
      contract: {},
      loadContract: jest.fn(),
      clearEditContractState: jest.fn(),
      setOrToggleListItem: jest.fn(),
      router: {
        setRouteLeaveHook: jest.fn()
      },
      askToNotSaveContract: jest.fn()
    }
  })
  it('should render correctly', () => {
    const comp = renderer.create(<ContractContainer {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('should not call loadContract when contractId is not set', () => {
    const comp = renderer.create(<ContractContainer {...props} />)
    expect(props.loadContract).not.toHaveBeenCalled()
  })
  it('should call loadContract when contractId is set', () => {
    props.params.contractId = 123
    const comp = renderer.create(<ContractContainer {...props} />)
    expect(props.loadContract).toHaveBeenCalledWith(123)
  })
  it('should call loadAllContacts when managers is empty', () => {
    props.managers = []
    const comp = renderer.create(<ContractContainer {...props} />)
    expect(props.loadAllContacts).toHaveBeenCalled()
  })
  it('should not call loadAllContacts when managers is populated', () => {
    props.managers = [{}]
    const comp = renderer.create(<ContractContainer {...props} />)
    expect(props.loadAllContacts).not.toHaveBeenCalled()
  })
  it('should clear edit contract state on unmount', () => {
    const comp = shallow(<ContractContainer {...props} />)
    expect(props.clearEditContractState).not.toHaveBeenCalled()
    comp.unmount()
    expect(props.clearEditContractState).toHaveBeenCalled()
  })
  it('should redirect the route when received new contractId', () => {
    props.router = []
    const comp = shallow(<ContractContainer {...props} />)
    comp.setProps({...props, contract: {contractId: 123}})
    expect(props.router.length).toBe(1)
    expect(props.router[0]).toBe('/contracts/123/specifications')
  })
  it('should not redirect the route when received old contractId', () => {
    props.router = []
    props.location.pathname = '/contracts/123/specifications'
    const comp = shallow(<ContractContainer {...props} />)
    comp.setProps({...props, contract: {contractId: 123}})
    expect(props.router.length).toBe(0)
  })
  it('setRouteLeaveHook should be called with the current route and the result of check', () => {
    const comp = shallow(<ContractContainer {...props} />)
    expect(props.router.setRouteLeaveHook).toHaveBeenCalled
  })
  it('mapStateToProps should map correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][0]({
      contacts: {managers: 'managers'},
      contracts: {allContracts: {results: 'results'}, contract: 'contract', currentContractFormId: 'currentContractFormId'}
    })).toEqual({
      contract: 'contract',
      currentContractFormId: 'currentContractFormId',
      contracts: 'results',
      managers: 'managers'
    })
  })
  it('actions should be set up correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][1]).toEqual({
      loadContract: 'loadContract',
      clearEditContractState: 'clearEditContractState',
      loadAllContacts: 'loadAllContacts',
      setOrToggleListItem: 'setOrToggleListItem',
      askToNotSaveContract: 'askToNotSaveContract'
    })
  })
})
