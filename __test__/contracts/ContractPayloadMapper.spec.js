import _ from 'lodash'
import moment from 'moment'
import {
  getCurrentAuditableContractValues,
  createContractSavePayload,
  mapContract,
  createNewContractSavePayload,
  mapDerivativeTypes,
  mergeAuditableFieldChanges
} from '../../src/contracts/ContractPayloadMapper'
import { SPECIFICATIONS, SCHEDULE_FUNDING } from '../../src/contracts/ContractForms'
import { dateFormat } from '../../src/components/utilities/Formatters'
import derivativeTypes from './derivativeTypes.json'

describe('ContractPayloadMapper Specs', () => {
  it('maps history to an empty object if there is no edit history', () => {
    const emptyAuditableContractValues = getCurrentAuditableContractValues([])

    expect(emptyAuditableContractValues).toEqual({})
  })

  it('maps the auditable field values with most recent effective date ', () => {
    const editHistory = [
      {
        'effectiveDate': '2012-11-15T00:00:00.000Z',
        'jurisdiction': 'IL',
        'assignmentState': 'IL'
      },
      {
        'effectiveDate': '2013-04-01T00:00:00.000Z',
        'jurisdiction': null,
        'assignmentState': 'MA'
      },
      {
        'effectiveDate': '2014-12-31T00:00:00.000Z',
        'jurisdiction': null,
        'assignmentState': null
      },
      {
        'effectiveDate': '2017-07-25T00:00:00.000Z',
        'jurisdiction': 'MO',
        'assignmentState': null
      }
    ]

    const expectedValues = {
      'jurisdiction': 'MO',
      'assignmentState': 'MA'
    }

    const actualValues = getCurrentAuditableContractValues(editHistory)

    expect(actualValues).toEqual(expectedValues)
  })

  it('maps the auditable field value with immediate effective date value, if there are multiple values with effective date in future and no values for past', () => {
    const editHistory = [
      {
        'effectiveDate': '2099-11-15T00:00:00.000Z',
        'jurisdiction': 'IL'
      },
      {
        'effectiveDate': '2099-10-15T00:00:00.000Z',
        'jurisdiction': 'PA'
      },
      {
        'effectiveDate': '2012-12-31T00:00:00.000Z',
        'jurisdiction': null,
        'assignmentState': 'MO'
      }
    ]

    const expectedValues = {
      'jurisdiction': 'PA',
      'assignmentState': 'MO'
    }

    const actualValues = getCurrentAuditableContractValues(editHistory)

    expect(actualValues).toEqual(expectedValues)
  })

  it('maps new effective date for a modified auditable field as a new edit history record', () => {
    const originalEditHistories = [{
      'effectiveDate': '2017-11-30T00:00:00.000Z',
      'jurisdiction': 'MO'
    }, {
      'premiumRate': 25,
      'effectiveDate': '2012-10-30T00:00:00.000Z'
    }]

    const forms = {
      [SPECIFICATIONS]: {
        values: {
          'jurisdiction': 'IL',
          'jurisdictionEffectiveDate': '2017-12-06T00:00:00.000Z',
          'premiumRate': 35,
          'premiumRateEffectiveDate': '2017-12-06T00:00:00.000Z'
        }
      }
    }

    const contract = {
      editHistories: originalEditHistories,
      status: 'ACTIVE'
    }

    console.log('Original EditHistories', originalEditHistories.length)

    const contractPayload = createContractSavePayload(contract, forms)
    const newEditHistories = contractPayload.editHistories

    expect(newEditHistories.length).toEqual(originalEditHistories.length + 1)

    const editHistoriesByEffectiveDate = _.keyBy(newEditHistories, edit => dateFormat(edit.effectiveDate))
    const NOV_30_2017 = '11/30/2017'
    const DEC_06_2017 = '12/06/2017'
    expect(editHistoriesByEffectiveDate[DEC_06_2017].jurisdiction).toEqual(forms[SPECIFICATIONS].values.jurisdiction)
    expect(editHistoriesByEffectiveDate[DEC_06_2017].premiumRate).toEqual(forms[SPECIFICATIONS].values.premiumRate)
    expect(editHistoriesByEffectiveDate[NOV_30_2017].jurisdiction).toEqual(originalEditHistories[0].jurisdiction)
  })

  it('maps modified auditable field with existing effective date to an existing edit history record', () => {
    const originalEditHistories = [{
      'premiumRate': 25,
      'effectiveDate': '2012-10-30T00:00:00.000Z'
    }, {
      'effectiveDate': '2017-11-30T00:00:00.000Z',
      'jurisdiction': 'MO'
    }]

    const forms = {
      [SPECIFICATIONS]: {
        values: {
          'creditingRateCalcMethod': 'All Out',
          'creditingRateCalcMethodEffectiveDate': '2017-11-30T00:00:00.000Z',
          'legalFormName': 'Form 007',
          'legalFormNameEffectiveDate': '2017-11-30T00:00:00.000Z'
        }
      },
      [SCHEDULE_FUNDING]: {}
    }

    const contract = {
      editHistories: originalEditHistories,
      status: 'ACTIVE'
    }

    const contractPayload = createContractSavePayload(contract, forms)
    const newEditHistories = contractPayload.editHistories

    expect(newEditHistories.length).toEqual(originalEditHistories.length)

    const editHistoriesByEffectiveDate = _.keyBy(newEditHistories, edit => dateFormat(edit.effectiveDate))
    const NOV_30_2017 = '11/30/2017'
    expect(editHistoriesByEffectiveDate[NOV_30_2017].creditingRateCalcMethod).toEqual(originalEditHistories[1].creditingRateCalcMethod)
    expect(editHistoriesByEffectiveDate[NOV_30_2017].legalFormName).toEqual(originalEditHistories[1].legalFormName)
  })

  it('it creates ContractSavePayload for pending contracts', () => {
    const forms = {
      [SPECIFICATIONS]: {
        values: {
          contractType: 'POOLED_PLAN',
          shortPlanName: 'New',
          effectiveDate: '2012-11-15T00:00:00.000Z',
          jurisdiction: 'IL'
        }
      },
      [SCHEDULE_FUNDING]: {
        values: {
          additionalDeposits: 'NEGOTIATED'
        }
      }
    }

    const contract = {
      status: 'PENDING',
      contractType: 'POOLED_PLAN',
      shortPlanName: 'Gold'
    }

    const expectedContractPayload = {
      status: 'PENDING',
      contractType: 'POOLED_PLAN',
      shortPlanName: 'New',
      effectiveDate: '2012-11-15T00:00:00.000Z',
      additionalDeposits: 'NEGOTIATED',
      editHistories: [
        {
          jurisdiction: 'IL'
        }
      ],
      derivativeTypes: []
    }

    const actualContractPayload = createContractSavePayload(contract, forms)

    expect(actualContractPayload).toEqual(expectedContractPayload)
  })

  it('maps contract payload with audit histories', () => {
    const editHistory = [
      {
        'effectiveDate': '2012-11-15T00:00:00.000Z',
        'jurisdiction': 'IL',
        'assignmentState': 'IL'
      },
      {
        'effectiveDate': '2013-04-01T00:00:00.000Z',
        'jurisdiction': null,
        'assignmentState': 'MA'
      },
      {
        'effectiveDate': '2014-12-31T00:00:00.000Z',
        'jurisdiction': null,
        'assignmentState': null
      },
      {
        'effectiveDate': '2017-07-25T00:00:00.000Z',
        'jurisdiction': 'MO',
        'assignmentState': null
      }
    ]

    const contract = {
      contractNumber: 'CASC-1222',
      editHistories: editHistory
    }

    const sortedEditHistory = _.sortBy(editHistory, ({effectiveDate}) => moment(effectiveDate).valueOf() * -1)

    const expectedContract = {
      contractNumber: 'CASC-1222',
      'jurisdiction': 'MO',
      'assignmentState': 'MA',
      'plan401ARate': 0,
      'plan401KRate': 0,
      'plan403BRate': 0,
      'plan457Rate': 0,
      'plan501CRate': 0,
      'plan529Rate': 0,
      'planTaftRate': 0,
      editHistories: sortedEditHistory
    }

    const actualContract = mapContract(contract)

    expect(actualContract).toEqual(expectedContract)
  })

  it('creates new contract payload', () => {
    const forms = {
      [SPECIFICATIONS]: {
        values: {
          contractType: 'POOLED_PLAN',
          effectiveDate: '2012-11-15T00:00:00.000Z',
          shortPlanName: 'Why'
        }
      }
    }

    const expectedPayload = {
      contractType: 'POOLED_PLAN',
      effectiveDate: '2012-11-15T00:00:00.000Z',
      shortPlanName: 'Why',
      status: 'PENDING',
      statedMaturityDate: '2999-01-01T00:00:00.000Z',
      issueCompany: 'RGA'
    }

    const actualPayload = createNewContractSavePayload(forms)

    expect(actualPayload).toEqual(expectedPayload)
  })

  it('maps derivatives correctly', () => {
    const scheduleFormValue = {
      CURRENCY_SWAPS: true,
      INTEREST_RATE_SWAPS: false,
      'INVERSE_FLOATERS/IOS/POS': true
    }

    const expectedContractDerivativeTypes = [{
      'derivativeTypeId': 'CURRENCY_SWAPS',
      'description': 'Currency Swaps'
    }, {
      'derivativeTypeId': 'INVERSE_FLOATERS/IOS/POS',
      'description': 'Inverse Floaters/IOs/POs'
    }]

    const actualContractDerivativeTypes = mapDerivativeTypes(scheduleFormValue, derivativeTypes)

    expect(actualContractDerivativeTypes).toEqual(expectedContractDerivativeTypes)
  })
  it('getCurrentAuditableContractValues should not remove manager rates set to 0', () => {
    const editHistory = [
      {
        'contractEditHistoryId': 1581,
        'effectiveDate': '2016-04-01T00:00:00.000Z',
        'managerFeeRate': 8
      },
      {
        'contractEditHistoryId': 1684,
        'effectiveDate': '2016-06-01T00:00:00.000Z',
        'managerFeeRate': 0
      }
    ]

    const result = getCurrentAuditableContractValues(editHistory)

    expect(result.managerFeeRate).toBe(0)
  })
  it('getCurrentAuditableContractValues should pick the latest', () => {
    const editHistory = [
      {
        'contractEditHistoryId': 1581,
        'effectiveDate': '2016-08-01T00:00:00.000Z',
        'managerFeeRate': 8
      },
      {
        'contractEditHistoryId': 1684,
        'effectiveDate': '2016-06-01T00:00:00.000Z',
        'managerFeeRate': 0
      }
    ]

    const result = getCurrentAuditableContractValues(editHistory)

    expect(result.managerFeeRate).toBe(8)
  })
  describe('mergeAuditableFieldChanges', () => {
    it('should deep clone edit histories', () => {
      const contract = {editHistories: [{advisorFeeRate: null, effectiveDate: '2018-02-23T00:00:00.000Z'}]}
      const edit = {advisorFeeRate: 1, advisorFeeRateEffectiveDate: '2018-02-23T00:00:00.000Z'}

      const result = mergeAuditableFieldChanges(contract, edit)

      expect(result).not.toEqual(contract.editHistories)
    })
  })
})
