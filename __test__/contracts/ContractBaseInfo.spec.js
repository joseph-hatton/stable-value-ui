import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../MockComponent'
import mockConnect from '../MockConnect'
import {connect} from 'react-redux'
import {planAnniversaryMonthChanged, planAnniversaryDayChanged} from '../../src/contracts/ContractActions'
import {change} from 'redux-form'

import ContractBaseInfo, {mapPlanAnniversary, mapStateToForm} from '../../src/contracts/ContractBaseInfo'

jest.mock('react-redux', () => mockConnect())

jest.mock('../../src/contracts/CreateContractForm', () => jest.fn((i) => i))
jest.mock('../../src/components/forms/MonthAsTextFormField', () => mockComponent('MonthAsTextFormField'))
jest.mock('../../src/components/forms/DaysOfMonthFormField', () => mockComponent('DaysOfMonthFormField'))
jest.mock('../../src/components/forms/BenefitResponseTypes', () => mockComponent('BenefitResponseTypes'))
jest.mock('../../src/components/forms/BenefitTiers', () => mockComponent('BenefitTiers'))
jest.mock('../../src/components/forms/CalendarTypes', () => mockComponent('CalendarTypes'))
jest.mock('../../src/components/forms/NotificationTypes', () => mockComponent('NotificationTypes'))
jest.mock('../../src/components/forms/BillFrequency', () => mockComponent('BillFrequency'))
jest.mock('../../src/components/forms/FeeDailyRateFormula', () => mockComponent('FeeDailyRateFormula'))
jest.mock('../../src/components/forms/MarketValueEvents', () => mockComponent('MarketValueEvent'))
jest.mock('../../src/components/forms/YesNo', () => mockComponent('YesNo'))
jest.mock('../../src/contracts/edithistory/AuditableContractField', () => mockComponent('AuditableContractField'))
jest.mock('../../src/contracts/RateField', () => mockComponent('RateField'))
jest.mock('../../src/components/forms/Validators', () => ({
  MaxLength: jest.fn(),
  isInteger: 'isInteger',
  toInteger: 'toInteger'
}))
jest.mock('../../src/contracts/ContractActions', () => ({
  planAnniversaryMonthChanged: jest.fn(),
  planAnniversaryDayChanged: jest.fn()
}))

jest.mock('redux-form', () => ({
  Field: mockComponent('Field'),
  change: jest.fn().mockReturnValue('change')
}))

jest.mock('material-ui/Card', () => ({
  Card: mockComponent('Card')
}))

describe('Contract Base Info', () => {
  let props
  beforeEach(() => {
    props = {
      cashBufferReplenishmentOnChange: jest.fn(),
      planAnniversaryMonthChanged: jest.fn().mockReturnValue('planAnniversaryMonthChanged'),
      planAnniversaryDayChanged: jest.fn().mockReturnValue('planAnniversaryDayChanged'),
      cashBufferReplenishment: true
    }
  })
  it('should render correctly', () => {
    const comp = renderer.create(<ContractBaseInfo {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('mapPlanAnniversary should map the plan anniversary string to month and day', () => {
    expect(mapPlanAnniversary('MARCH 21')).toEqual({
      planAnniversaryMonth: 'MARCH',
      planAnniversaryDay: 21
    })
  })
  it('mapPlanAnniversary should map the default plan anniversary string when none passed in', () => {
    expect(mapPlanAnniversary()).toEqual({
      planAnniversaryMonth: 'DECEMBER',
      planAnniversaryDay: 31
    })
  })
  it('mapStateToForm should map correctly if contract is set', () => {
    expect(mapStateToForm({contracts: {contract: {}}})).toEqual({
      initialValues: {
        feeDailyRateFormula: 'DAILY_FEE',
        planAnniversaryMonth: 'DECEMBER',
        planAnniversaryDay: 31
      }
    })
  })
  it('mapStateToForm should map correctly if contract is not set', () => {
    expect(mapStateToForm({contracts: {contract: null}})).toEqual({
      initialValues: {}
    })
  })
  describe('actions', () => {
    it('planAnniversaryMonthChanged should be set correctly', () => {
      expect(connect).toHaveBeenCalledTimes(1)
      expect(connect.mock.calls[0][1].planAnniversaryMonthChanged).toBe(planAnniversaryMonthChanged)
    })
    it('planAnniversaryDayChanged should be set correctly', () => {
      expect(connect).toHaveBeenCalledTimes(1)
      expect(connect.mock.calls[0][1].planAnniversaryDayChanged).toBe(planAnniversaryDayChanged)
    })
    it('cashBufferReplenishmentOnChange should dispatch min and max to 0', () => {
      expect(connect).toHaveBeenCalledTimes(1)
      const dispatch = jest.fn()
      connect.mock.calls[0][1].cashBufferReplenishmentOnChange()(dispatch)
      expect(dispatch).toHaveBeenCalledTimes(2)
      expect(dispatch).toHaveBeenCalledWith('change')
      expect(change).toHaveBeenCalledWith('baseInfo', 'replenishmentMinimum', 0)
      expect(change).toHaveBeenCalledWith('baseInfo', 'replenishmentMaximum', 0)
    })
  })
  it('mapStatesToProps should map correctly', () => {
    expect(connect.mock.calls[0][0]({
      contracts: {contract: 'contract'},
      form: {baseInfo: {values: {cashBufferReplenishment: 'cashBufferReplenishment'}}}})
    ).toEqual({
      contract: 'contract',
      cashBufferReplenishment: 'cashBufferReplenishment'
    })
  })
})
