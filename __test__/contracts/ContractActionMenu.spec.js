import React from 'react'
import mockConnect from '../MockConnect'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import {connect} from 'react-redux'
import {shallow} from 'enzyme'

import ContractActionMenu from '../../src/contracts/ContractActionMenu'

jest.mock('react-redux', () => mockConnect())
jest.mock('../../src/contracts/ContractActions', () => ({
  updateContractStatus: 'updateContractStatus'
}))
jest.mock('material-ui/IconMenu', () => mockComponent('IconMenu'))
jest.mock('material-ui/svg-icons/content/send', () => mockComponent('SendIcon'))
jest.mock('material-ui', () => ({
  MenuItem: mockComponent('MenuItem')
}))

describe('ContractActionMenu', () => {
  let props
  beforeEach(() => {
    props = {
      stateTransitions: [
        {label: 'label1', path: 'path1'},
        {label: 'label2', path: 'path2', disabled: true}
      ],
      updateContractStatus: jest.fn()
    }
  })
  it('should render correctly', () => {
    const comp = renderer.create(<ContractActionMenu {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('should render correctly when there are no stateTransitions', () => {
    props.stateTransitions = []
    const comp = renderer.create(<ContractActionMenu {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('on onclick of menu items should call update contract status on path', () => {
    const comp = shallow(<ContractActionMenu {...props} />)
    expect(props.updateContractStatus).not.toHaveBeenCalled()
    comp.find('[primaryText="label1"]').prop('onClick')()
    expect(props.updateContractStatus).toHaveBeenCalledWith('path1')
    comp.find('[primaryText="label2"]').prop('onClick')()
    expect(props.updateContractStatus).toHaveBeenCalledWith('path2')
  })
  describe('mapStateToProps', () => {
    beforeEach(() => {
      expect(connect).toHaveBeenCalledTimes(1)
    })
    it('should map stateTransitions when stateTransitions and contractLifeCycleStatus are set', () => {
      expect(connect.mock.calls[0][0]({contracts: {contractLifeCycleStatus: {stateTransitions: 'stateTransitions'}}})).toEqual({
        stateTransitions: 'stateTransitions'
      })
    })
    it('should map an empty array when stateTransitions isn\'t set', () => {
      expect(connect.mock.calls[0][0]({contracts: {contractLifeCycleStatus: {}}})).toEqual({
        stateTransitions: []
      })
    })
    it('should map an empty array when contractLifeCycleStatus isn\'t set', () => {
      expect(connect.mock.calls[0][0]({contracts: {}})).toEqual({
        stateTransitions: []
      })
    })
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][1]).toEqual({
      updateContractStatus: 'updateContractStatus'
    })
  })
})
