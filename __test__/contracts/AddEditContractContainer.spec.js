import React from 'react'
import mockConnect from '../MockConnect'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import {connect} from 'react-redux'

import AddEditContractContainer from '../../src/contracts/AddEditContractContainer'

jest.mock('react-redux', () => mockConnect())
jest.mock('../../src/contracts/ContractActions', () => ({
  loadContract: 'loadContract',
  initializeContract: 'initializeContract'
}))
jest.mock('../../src/contracts/ContractSpecifications', () => mockComponent('ContractSpecifications'))

describe('AddEditContractContainer', () => {
  let props
  beforeEach(() => {
    props = {
      contract: {},
      params: {contractId: 123},
      loadContract: jest.fn(),
      initializeContract: jest.fn()
    }
  })
  it('should render correctly', () => {
    const comp = renderer.create(<AddEditContractContainer {...props} />)
    expect(comp).toMatchSnapshot()
  })
  it('should load contract if contract id is set and contract isn\'t', () => {
    props.contract = undefined
    const comp = renderer.create(<AddEditContractContainer {...props} />)
    expect(props.loadContract).toHaveBeenCalled()
  })
  it('should init contract if contract id is not set', () => {
    props.params.contractId = undefined
    const comp = renderer.create(<AddEditContractContainer {...props} />)
    expect(props.initializeContract).toHaveBeenCalled()
  })
  it('mapStateToProps should map correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][0]({contracts: {contract: 'contract'}})).toEqual({
      contract: 'contract'
    })
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][1]).toEqual({
      loadContract: 'loadContract',
      initializeContract: 'initializeContract'
    })
  })
})
