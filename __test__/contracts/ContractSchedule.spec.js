import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../MockComponent'
import mockConnect from '../MockConnect'
import {shallow, mount} from 'enzyme'
import {connect} from 'react-redux'

import ContractSchedule, {mapDerivateTypes, mapStateToForm} from '../../src/contracts/ContractSchedule'

jest.mock('react-redux', () => mockConnect())
jest.mock('material-ui/Tabs', () => ({
  Tabs: mockComponent('Tabs'),
  Tab: mockComponent('Tab')
}))

jest.mock('material-ui/Card', () => ({
  Card: mockComponent('Card')
}))
jest.mock('../../src/components/common/LeftDrawerActions', () => ({
  goToTab: jest.fn().mockReturnValue(true)
}))

jest.mock('../../src/contracts/ContractForms', () => ({
  SCHEDULE_FUNDING: [],
  SCHEDULE_FUNDING_PROPS: ['id1', 'id2']
}))

jest.mock('../../src/contracts/CreateContractForm', () => () => mockComponent('ContractScheduleForm'))
jest.mock('../../src/contracts/schedule', () => [
  {label: 'label', ContractTab: (props) => (<span {...props} />)} // eslint-disable-line react/display-name
])

describe('ContractSchedule', () => {
  let props
  beforeEach(() => {
    props = {
      test: 'test',
      setOrToggleListItem: jest.fn(),
      goToTab: jest.fn()
    }
  })
  it('should render correctly', () => {
    const comp = renderer.create(<ContractSchedule {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('setOrToggleListItem should be called if schedule is falsy and set it to true on mount', () => {
    const comp = mount(<ContractSchedule {...props} />)
    expect(props.setOrToggleListItem).toHaveBeenCalledWith('schedule', true)
  })
  it('setOrToggleListItem should not be called if schedule is truthy on mount', () => {
    props.schedule = true
    const comp = mount(<ContractSchedule {...props} />)
    expect(props.setOrToggleListItem).not.toHaveBeenCalled()
  })
  it('setOrToggleListItem should be called if schedule is truthy and set it to false on unmount', () => {
    props.schedule = true
    const comp = mount(<ContractSchedule {...props} />)
    comp.unmount()
    expect(props.setOrToggleListItem).toHaveBeenCalledWith('schedule', false)
  })
  it('setOrToggleListItem should not be called if schedule is falsy on unmount', () => {
    const comp = mount(<ContractSchedule {...props} />)
    comp.unmount()
    expect(props.setOrToggleListItem).not.toHaveBeenCalledWith('schedule', false)
  })
  it('mapDerivateTypes should map correctly when item is in the list', () => {
    expect(mapDerivateTypes({derivativeTypes: [
      {derivativeTypeId: 'id1'},
      {derivativeTypeId: 'id2'}
    ]}, [
      {derivativeTypeId: 'id2'}
    ])).toEqual({
      id2: true
    })
  })
  it('mapDerivateTypes should map correctly when item is not in the list', () => {
    expect(mapDerivateTypes({derivativeTypes: [
      {derivativeTypeId: 'id1'},
      {derivativeTypeId: 'id2'}
    ]}, [
      {derivativeTypeId: 'id3'}
    ])).toEqual({
      id3: false
    })
  })
  it('mapStateToForm should map correctly', () => {
    expect(mapStateToForm({
      contracts: {contract: {id2: 'test', contractId: 123}},
      referenceData: {derivativeTypes: [{derivativeTypeId: 'id1'}]}
    })).toEqual({
      contractId: 123,
      initialValues: {
        id1: false,
        id2: 'test'
      }
    })
  })
  it('mapStateToForm should map correctly if contract isn\'t set', () => {
    expect(mapStateToForm({
      contracts: {},
      referenceData: {derivativeTypes: [{derivativeTypeId: 'id1'}]}
    })).toEqual({
      initialValues: {}
    })
  })
  it('mapStateToProps should map correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][0]({
      sideBar: {
        selectedTab: 'selectedTab',
        schedule: 'schedule'
      },
      contracts: {contract: {contractId: 123}}
    })).toEqual({
      selectedTab: 'selectedTab',
      schedule: 'schedule',
      contractId: 123
    })
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
  })
})
