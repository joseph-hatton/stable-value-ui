import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from '../MockComponent'
import mockConnect from '../MockConnect'
import {selectAndLoadContact} from '../../src/contacts/ContactActions'
import {connect} from 'react-redux'

import ContactsMenu, {ContactsMenuLink, parseName} from '../../src/companies/ContactsMenu'

jest.mock('react-redux', () => mockConnect())
jest.mock('material-ui', () => ({
  Popover: mockComponent('Popover'),
  Menu: mockComponent('Menu'),
  MenuItem: mockComponent('MenuItem')
}))
jest.mock('../../src/companies/CompaniesActions', () => ({
  selectContacts: 'selectContacts'
}))
jest.mock('../../src/components/redux-dialog/redux-dialog', () => ({
  openDialog: 'openDialog',
  closeDialog: 'closeDialog'
}))
jest.mock('../../src/contacts/ContactActions', () => ({
  selectAndLoadContact: 'selectAndLoadContact'
}))

describe('ContactsMenu', () => {
  let props
  beforeEach(() => {
    props = {
      selectedContacts: [{contactId: 'contactId', firstName: 'firstName', lastName: 'lastName', middleName: 'middleName'}],
      open: 'open',
      closeDialog: jest.fn(),
      openDialog: jest.fn(),
      selectAndLoadContact: jest.fn()
    }
  })
  it('should render correctly', () => {
    const comp = renderer.create(<ContactsMenu {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('should render correctly with multiple selectedContracts', () => {
    props.selectedContacts.push({contactId: 'contactId2', firstName: 'firstName2', lastName: 'lastName2', middleName: 'middleName2'})
    const comp = renderer.create(<ContactsMenu {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('onRequestClose should close the dialog contactsMenu', () => {
    const comp = shallow(<ContactsMenu {...props} />)
    comp.prop('onRequestClose')()
    expect(props.closeDialog).toHaveBeenCalledWith('contactsMenu')
  })
  it('onClick of list item should close dialog and redirect to the right contact', () => {
    const comp = shallow(<ContactsMenu {...props} />)
    comp.find('#ContactsMenu-MenuItem-0').prop('onClick')()
    expect(props.closeDialog).toHaveBeenCalledWith('contactsMenu')
    expect(props.selectAndLoadContact).toHaveBeenCalled()
    expect(props.openDialog).toHaveBeenCalledWith('addEditContact')
  })
  it('mapStateToProps should map correctly', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[1][0]({
      dialog: {contactsMenu: {anchorEl: 'anchorEl'}},
      companies: {selectedContacts: 'selectedContacts'}
    })).toEqual({
      open: true,
      anchorEl: 'anchorEl',
      selectedContacts: 'selectedContacts'
    })
  })
  it('mapStateToProps should map correctly when  is not set', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[1][0]({
      dialog: {},
      companies: {selectedContacts: 'selectedContacts'}
    })).toEqual({
      open: false,
      anchorEl: null,
      selectedContacts: 'selectedContacts'
    })
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[1][1]).toEqual({
      selectAndLoadContact: 'selectAndLoadContact',
      closeDialog: 'closeDialog',
      openDialog: 'openDialog'
    })
  })
})

describe('ContactsMenuLink', () => {
  let props
  beforeEach(() => {
    props = {
      cell: 'cell',
      row: {
        contacts: ['test']
      },
      selectContacts: jest.fn(),
      openDialog: jest.fn()
    }
  })
  it('should render correctly', () => {
    const comp = renderer.create(<ContactsMenuLink {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('on onClick shoud prevent default, select contracts and open dialog', () => {
    const comp = shallow(<ContactsMenuLink {...props} />)
    const e = {nativeEvent: {preventDefault: jest.fn()}, currentTarget: 'currentTarget'}
    comp.prop('onClick')(e)
    expect(e.nativeEvent.preventDefault).toHaveBeenCalled()
    expect(props.selectContacts).toHaveBeenCalledWith({contacts: ['test']})
    expect(props.openDialog).toHaveBeenCalledWith('contactsMenu', {anchorEl: 'currentTarget'})
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[0][1]).toEqual({
      openDialog: 'openDialog',
      selectContacts: 'selectContacts'
    })
  })
})
describe('parseName', () => {
  const lastName = 'lastName'
  const firstName = 'firstName'
  const middleName = 'middleName'
  it('should display only lastname if that is all that is set', () => {
    expect(parseName({lastName})).toBe(`${lastName}`)
  })
  it('should display lastname, firstname if that is all that is set', () => {
    expect(parseName({lastName, firstName})).toBe(`${lastName}, ${firstName}`)
  })
  it('should display lastname, firstname, middlename if that is what is set', () => {
    expect(parseName({lastName, firstName, middleName})).toBe(`${lastName}, ${firstName}, ${middleName}`)
  })
  it('should display lastname, middlename if that is all that is set', () => {
    expect(parseName({lastName, middleName})).toBe(`${lastName}, ${middleName}`)
  })
})
