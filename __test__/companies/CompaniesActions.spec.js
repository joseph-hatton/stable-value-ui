import http from '../../src/actions/http'
import {apiPath} from '../../src/config'
import {browserHistory} from 'react-router'

import * as companiesActions from '../../src/companies/CompaniesActions'

jest.mock('../../src/actions/http', () => ({
  put: jest.fn().mockReturnValue('put'),
  post: jest.fn().mockReturnValue('post'),
  delete: jest.fn().mockReturnValue('delete'),
  get: jest.fn().mockReturnValue('get'),
  STANDARD_ERROR_MESSAGE: 'STANDARD_ERROR_MESSAGE'
}))

jest.mock('react-router', () => ({
  browserHistory: {
    push: jest.fn()
  }
}))

describe('Companies Actions', () => {
  let dispatch, getState, state
  beforeEach(() => {
    dispatch = jest.fn()
    state = {form: {AddEditCompanyForm: {values: {test: 'test'}}}, companies: {selectedCompany: {companyId: 'companyId'}}}
    getState = jest.fn().mockReturnValue(state)
    http.put.mockClear()
    http.post.mockClear()
    http.delete.mockClear()
    http.get.mockClear()
  })
  it('loadAllCompanies should get the companies from the api', () => {
    expect(companiesActions.loadAllCompanies()).toEqual({
      type: 'LOAD_ALL_COMPANIES',
      payload: 'get'
    })
    expect(http.get).toHaveBeenCalledWith(`${apiPath}/companies`)
  })
  describe('saveCompany', () => {
    it('should dispatch saveCompany with put if editing', async () => {
      http.put.mockReturnValueOnce(Promise.resolve('put'))
      companiesActions.saveCompany()(dispatch, getState)
      expect(dispatch).toHaveBeenCalled()
      expect(dispatch.mock.calls[0][0].type).toBe('SAVE_COMPANY')
      expect(await dispatch.mock.calls[0][0].payload).toBe('put')
      expect(http.put).toHaveBeenCalledWith(`${apiPath}/companies/companyId`, {test: 'test'}, {successMessage: 'Company saved successfully'}, 'addEditCompany')
    })
    it('should dispatch saveCompany with post if creating', async () => {
      state.companies.selectedCompany.companyId = null
      http.post.mockReturnValueOnce(Promise.resolve('post'))
      companiesActions.saveCompany()(dispatch, getState)
      expect(dispatch).toHaveBeenCalled()
      expect(dispatch.mock.calls[0][0].type).toBe('SAVE_COMPANY')
      expect(await dispatch.mock.calls[0][0].payload).toBe('post')
      expect(http.post).toHaveBeenCalledWith(`${apiPath}/companies`, {test: 'test'}, {successMessage: 'Company saved successfully'}, 'addEditCompany')
    })
    it('should delete address2 if its not set', () => {
      state.form.AddEditCompanyForm.values.address2 = null
      http.put.mockReturnValueOnce(Promise.resolve())
      companiesActions.saveCompany()(dispatch, getState)
      expect(http.put).toHaveBeenCalled
      expect(http.put.mock.calls[0][1]).toEqual({test: 'test'})
    })
    it('should not delete address2 if its set', () => {
      state.form.AddEditCompanyForm.values.address2 = 'address2'
      http.put.mockReturnValueOnce(Promise.resolve())
      companiesActions.saveCompany()(dispatch, getState)
      expect(http.put).toHaveBeenCalled
      expect(http.put.mock.calls[0][1]).toEqual({test: 'test', address2: 'address2'})
    })
  })
  it('selectContacts should return correctly', () => {
    expect(companiesActions.selectContacts({contacts: 'contacts'})).toEqual({
      type: 'SELECT_CONTACTS',
      contacts: 'contacts'
    })
  })
  it('selectCompany should return correctly', () => {
    expect(companiesActions.selectCompany('company')).toEqual({
      type: 'SELECT_COMPANY',
      company: 'company'
    })
  })
})
