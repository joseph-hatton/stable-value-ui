import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import { CompaniesContainer, actions, mapStateToProps } from '../../src/companies/CompaniesContainer'

jest.mock('react-redux', () => {
  return {
    connect: (mapStateToProps, mapDispatchToProps) => (component) => <component/> // eslint-disable-line react/display-name
  }
})
jest.mock('../../src/companies/CompaniesComponent', () => {
  return mockComponent('CompaniesComponent')
})

describe('CompaniesContainer Specs', () => {
  it('Renders Correctly and loadCompanies is called when no companies are passed', () => {
    const loadAllCompanies = jest.fn()
    const props = {loadAllCompanies, clearEditCompanyState: jest.fn(), clearSaveCompanyResult: jest.fn()}

    const companiesContainer = renderer.create(
      <CompaniesContainer {...props} />
    ).toJSON()

    expect(companiesContainer).toMatchSnapshot()
    expect(loadAllCompanies).toHaveBeenCalled()
  })

  it('clears edit company state if any', () => {
    const clearEditCompanyState = jest.fn()
    const company = {name: 'At&t'}
    const props = {
      loadAllCompanies: jest.fn(),
      clearEditCompanyState,
      clearSaveCompanyResult: jest.fn(),
      companies: [company],
      editCompany: company
    }

    const companiesContainer = renderer.create(
      <CompaniesContainer {...props} />
    ).toJSON()

    expect(clearEditCompanyState).toHaveBeenCalled()
  })

  it('defines redux actions', () => {
    const clearEditCompany = {
      type: 'CLEAR_EDIT_COMPANY'
    }

    expect(Object.keys(actions).length).toEqual(2)

    expect(actions.clearEditCompanyState()).toEqual(clearEditCompany)
  })

  it('maps state to props', () => {
    const company = {name: 'At&t'}
    const companies = {
      allCompanies: {
        results: [
          company
        ]
      },
      editCompany: company
    }
    const state = {companies}

    const props = mapStateToProps(state)

    expect(props.companies).toEqual(companies.allCompanies.results)
    expect(props.editCompany).toEqual(companies.editCompany)
  })
})
