import React from 'react'
import mockComponent from '../MockComponent'
import mockConnect from '../MockConnect'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import {CreateGridConfig} from '../../src/components/grid'
import {connect} from 'react-redux'

import CompaniesComponent from '../../src/companies/CompaniesComponent'

jest.mock('react-redux', () => mockConnect())
jest.mock('material-ui/styles/colors', () => ({
  blue500: 'blue500',
  red500: 'red500'
}))
jest.mock('material-ui/FloatingActionButton', () => mockComponent('FloatingActionButton'))
jest.mock('material-ui/svg-icons/content/add', () => mockComponent('ContentAdd'))
jest.mock('material-ui/svg-icons/editor/mode-edit', () => mockComponent('EditIcon'))
jest.mock('material-ui/svg-icons/action/delete', () => mockComponent('DeleteIcon'))
jest.mock('../../src/components/common/MultiActionMenu', () => mockComponent('MultiActionMenu'))
jest.mock('../../src/companies/ContactsMenu', () => mockComponent('ContactsMenu'))
jest.mock('../../src/contacts/AddEditContactComponent', () => mockComponent('AddEditContactComponent'))
jest.mock('../../src/companies/AddEditCompanyComponent', () => mockComponent('AddEditCompanyComponent'))
jest.mock('../../src/companies/CompanyColumns', () => ({CompanyColumns: 'CompanyColumns'}))
jest.mock('../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn()
}))
jest.mock('material-ui/Menu', () => ({
  Menu: mockComponent('Menu'),
  MenuItem: mockComponent('MenuItem')
}))
jest.mock('../../src/companies/CompaniesActions', () => ({
  deleteCompany: 'deleteCompany',
  selectCompany: 'selectCompany'
}))
jest.mock('../../src/components/redux-dialog', () => ({
  closeDialog: 'closeDialog',
  openDialog: 'openDialog',
  openConfirmationAlert: 'openConfirmationAlert'
}))

describe('CompaniesComponent', () => {
  let props, companies
  beforeEach(() => {
    companies = [{name: 'At&t'}]
    props = {
      clearSaveCompanyResult: jest.fn(),
      companies,
      saveResult: {message: 'Ok'},
      selectCompany: jest.fn(),
      selectedCompany: 'selectedCompany',
      openDialog: jest.fn(),
      closeDialog: jest.fn(),
      deleteCompany: jest.fn(),
      openConfirmationAlert: jest.fn()
    }
  })
  it('Renders Spinner when there are no companies loaded and there is no error', () => {
    delete props.companies
    expect(renderer.create(<CompaniesComponent {...props} />).toJSON()).toMatchSnapshot()
  })

  it('Renders error when there is error', () => {
    expect(renderer.create(<CompaniesComponent {...props} />).toJSON()).toMatchSnapshot()
  })

  it('Renders grid when there is no error and companies are provided', () => {
    expect(renderer.create(<CompaniesComponent {...props} />).toJSON()).toMatchSnapshot()
    expect(CreateGridConfig).toHaveBeenCalledWith(companies, {'title': 'Companies'})
  })

  it('Renders Company Saved message along with the grid when there is no error, companies are provided and save result is provided', () => {
    expect(renderer.create(<CompaniesComponent {...props} />).toJSON()).toMatchSnapshot()
    expect(CreateGridConfig).toHaveBeenCalledWith(companies, {'title': 'Companies'})
  })
  it('on add button click should reset selected company and open dialog', () => {
    shallow(<CompaniesComponent {...props} />).find('#companies-floating-add-button').prop('onClick')()
    expect(props.selectCompany).toHaveBeenCalledWith({})
    expect(props.openDialog).toHaveBeenCalledWith('addEditCompany')
  })
  it('on view/edit button click should close the menu and open the view/edit popup', () => {
    shallow(<CompaniesComponent {...props} />).find('#companies-floating-view-edit-button').prop('onClick')()
    expect(props.closeDialog).toHaveBeenCalledWith('companyMenu')
    expect(props.openDialog).toHaveBeenCalledWith('addEditCompany')
  })
  it('on delete button click should close the menu and ask to delete the company; on ok should delete the company', () => {
    shallow(<CompaniesComponent {...props} />).find('#companies-floating-delete-button').prop('onClick')()
    expect(props.closeDialog).toHaveBeenCalledWith('companyMenu')
    expect(props.openConfirmationAlert).toHaveBeenCalledTimes(1)
    expect(props.openConfirmationAlert.mock.calls[0][0].message).toEqual('Are you sure you want to delete this Company?')
    props.openConfirmationAlert.mock.calls[0][0].onOk()
    expect(props.deleteCompany).toHaveBeenCalledWith('selectedCompany')
  })
  it('on row double click of grid should select the row and open the view edit screen', () => {
    shallow(<CompaniesComponent {...props} />).find('#Companies').prop('onRowDoubleClick')('row')
    expect(props.selectCompany).toHaveBeenCalledWith('row')
    expect(props.openDialog).toHaveBeenCalledWith('addEditCompany')
  })
  it('mapStateToProps should map correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][0]({companies: {selectedCompany: 'selectedCompany'}})).toEqual({
      selectedCompany: 'selectedCompany'
    })
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][1]).toEqual({
      closeDialog: 'closeDialog',
      openDialog: 'openDialog',
      openConfirmationAlert: 'openConfirmationAlert',
      deleteCompany: 'deleteCompany',
      selectCompany: 'selectCompany'
    })
  })
})
