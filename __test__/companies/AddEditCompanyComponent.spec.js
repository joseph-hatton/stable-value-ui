import React from 'react'
import mockComponent from '../MockComponent'
import mockConnect from '../MockConnect'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import StatesFormField from '../../src/components/forms/StatesFormField'

import { AddEditCompany } from '../../src/companies/AddEditCompanyComponent'

jest.mock('material-ui/Card', () => {
  return {
    Card: mockComponent('Card'),
    CardTitle: mockComponent('CardTitle')
  }
})

jest.mock('material-ui/Toolbar', () => {
  return {
    Toolbar: mockComponent('Card'),
    ToolbarGroup: mockComponent('CardTitle')
  }
})

jest.mock('material-ui/RaisedButton', () => {
  return mockComponent('RaisedButton')
})

jest.mock('redux-form', () => {
  return {
    Field: mockComponent('Field'),
    reduxForm: () => jest.fn()
  }
})

jest.mock('react-redux', () => mockConnect())
jest.mock('../../src/components/redux-dialog/redux-dialog', () => mockComponent('ReduxDialog'))
jest.mock('../../src/components/forms/StatesFormField', () => mockComponent('StatesFormField'))

jest.mock('../../src/components/common/Message', () => mockComponent('Message'))

describe('Add Edit Company Component', () => {
  it('Renders the form correctly', () => {
    const handleSubmit = jest.fn()
    const saveCompany = jest.fn()
    const clearSaveCompanyResult = jest.fn()

    const props = {handleSubmit, saveCompany, clearSaveCompanyResult}

    const component = renderer.create(<AddEditCompany {...props}/>).toJSON()

    expect(component).toMatchSnapshot()
  })

  it('Renders the spinner while saving', () => {
    const handleSubmit = jest.fn()
    const saveCompany = jest.fn()
    const clearSaveCompanyResult = jest.fn()

    const props = {handleSubmit, saveCompany, clearSaveCompanyResult, saving: true}

    const component = renderer.create(<AddEditCompany {...props}/>).toJSON()

    expect(component).toMatchSnapshot()
  })

  it('Renders the spinner while validating company name', () => {
    const handleSubmit = jest.fn()
    const saveCompany = jest.fn()
    const clearSaveCompanyResult = jest.fn()

    const props = {handleSubmit, saveCompany, clearSaveCompanyResult, saving: false, asyncValidating: 'name'}

    const component = renderer.create(<AddEditCompany {...props}/>).toJSON()

    expect(component).toMatchSnapshot()
  })

  it('Displays Error if error in saving', () => {
    const handleSubmit = jest.fn()
    const saveCompany = jest.fn()
    const clearSaveCompanyResult = jest.fn()

    const props = {
      handleSubmit,
      saveCompany,
      clearSaveCompanyResult,
      saving: false,
      asyncValidating: 'name',
      initialValues: {name: 'RGA'},
      saveError: {message: 'failed to save'}
    }

    const component = renderer.create(<AddEditCompany {...props}/>).toJSON()

    expect(component).toMatchSnapshot()
  })
  it('cancel button closes dialog', () => {
    const props = {
      handleSubmit: jest.fn(),
      saveCompany: jest.fn(),
      closeDialog: jest.fn(),
      clearSaveCompanyResult: jest.fn(),
      saving: false,
      asyncValidating: 'name',
      initialValues: {name: 'RGA'},
      saveError: {message: 'failed to save'}
    }

    shallow(<AddEditCompany {...props}/>).prop('actions')[1].props.onClick()
    expect(props.closeDialog).toHaveBeenCalledWith('addEditCompany')
  })
})
