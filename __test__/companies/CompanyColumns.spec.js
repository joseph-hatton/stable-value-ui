import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../MockComponent'
import store from '../../src/store/store'
import {shallow} from 'enzyme'
import {selectCompany} from '../../src/companies/CompaniesActions'

import {CompanyColumns} from '../../src/companies/CompanyColumns'

jest.mock('../../src/store/store', () => ({
  dispatch: jest.fn()
}))

jest.mock('../../src/components/common/MultiActionMenu', () => ({
  MultiActionMenuLink: mockComponent('MultiActionMenuLink')
}))

jest.mock('../../src/companies/CompaniesActions', () => ({
  selectCompany: jest.fn()
}))

jest.mock('../../src/companies/ContactsMenu', () => ({
  ContactsMenuLink: mockComponent('ContactsMenuLink')
}))

describe('Company Columns', () => {
  it('should match snapshot', () => {
    expect(CompanyColumns).toMatchSnapshot()
  })
  it('company name should render correctly', () => {
    selectCompany.mockReturnValue('selectCompany')
    const compBefore = CompanyColumns.find(({field}) => field === 'name').renderer('cell', 'row')
    expect(renderer.create(compBefore).toJSON()).toMatchSnapshot()
    shallow(compBefore).prop('selections')('row')
    expect(store.dispatch).toHaveBeenCalledWith('selectCompany')
    expect(selectCompany).toHaveBeenCalledWith('row')
  })
  it('contacts should render correctly', () => {
    const compBefore = CompanyColumns.find(({field}) => field === 'contacts').renderer([{}], 'row')
    expect(renderer.create(compBefore).toJSON()).toMatchSnapshot()
  })
  it('contacts should still render correctly when no contacts are passed in', () => {
    const compBefore = CompanyColumns.find(({field}) => field === 'contacts').renderer(null, 'row')
    expect(renderer.create(compBefore).toJSON()).toMatchSnapshot()
  })
})
