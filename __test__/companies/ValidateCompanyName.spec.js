import validateCompanyName from '../../src/companies/ValidateCompanyName'
import http from '../../src/actions/http'

jest.mock('../../src/actions/http', () => ({
  get: jest.fn()
}))

describe('validate company name', () => {
  it('should reject if name is not passed in', () => {
    expect(validateCompanyName({})).rejects.toEqual({
      name: 'Required'
    })
  })
  it('should reject if found companies with the same name and not the same companyId', async () => {
    http.get.mockReturnValue(Promise.resolve({results: [{name: 'name', companyId: 'companyIdDiff'}]}))
    expect(validateCompanyName({name: 'name', companyId: 'companyId'})).rejects.toEqual({
      name: 'Company name already exists'
    })
  })
  it('should resolve if found companies with the same name and the same companyId', async () => {
    http.get.mockReturnValue(Promise.resolve({results: [{name: 'name', companyId: 'companyIdD'}]}))
    expect(validateCompanyName({name: 'name', companyId: 'companyId'})).resolves
  })
})
