import React from 'react'
import mockComponent from '../../MockComponent'
import renderer from 'react-test-renderer'

import MonthlyBookValuesFilter from '../../../src/bookValues/monthly/MonthlyBookValuesFilter'
import {MonthlyBookValueColumns} from '../../../src/bookValues/monthly/MonthlyBookValueColumns'
import {Grid, CreateGridConfig} from '../../../src/components/grid'
import {loadAllMonthlyBookValues} from '../../../src/bookValues/BookValueActions'

import MonthlyBookValues from '../../../src/bookValues/monthly/MonthlyBookValues'

jest.mock('react-redux', () => ({
  connect: jest.fn(() => jest.fn(component => component))
}))
jest.mock('../../../src/bookValues/BookValueActions', () => ({
  loadAllMonthlyFees: jest.fn()
}))
jest.mock('../../../src/bookValues/monthly/MonthlyBookValuesFilter', () => mockComponent('MonthlyBookValuesFilter'))
jest.mock('../../../src/bookValues/monthly/MonthlyBookValueColumns', () => ({
  MonthlyBookValueColumns: () => []
}))
jest.mock('../../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn().mockReturnValue({})
}))

describe('Monthly Book Values', () => {
  let props
  beforeEach(() => {
    props = {
      loadedBookValues: [],
      params: {contractId: 123},
      loadAllMonthlyBookValues: jest.fn()
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<MonthlyBookValues {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should call loadAllMonthlyBookValues', () => {
    const component = renderer.create(<MonthlyBookValues {...props} />)
    expect(props.loadAllMonthlyBookValues.mock.calls.length).toBe(1)
  })
})
