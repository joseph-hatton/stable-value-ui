import {currencyFormat} from '../../../src/components/grid'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'

import {MonthlyBookValueColumns} from '../../../src/bookValues/monthly/MonthlyBookValueColumns'

jest.mock('../../../src/components/grid', () => ({
  currencyFormat: 'currencyFormat',
  dateFormat: 'dateFormat'
}))

jest.mock('react-router', () => ({
  Link: mockComponent('Link')
}))

describe('MonthlyBookValueColumns', () => {
  let cols
  beforeAll(() => {
    cols = MonthlyBookValueColumns()
  })
  it('should call currencyFormat', () => {
    const currencyFormatCols = [
      'beginningBookValue',
      'deposits',
      'withdrawals',
      'adjustments',
      'interest',
      'endingBookValue'
    ]

    currencyFormatCols.forEach((col) => {
      const element = cols.find((i) => i.field === col)
      expect(element.renderer).toBe('currencyFormat')
    })
  })
  it('should setup link for contract number', () => {
    const element = cols.find((i) => i.field === 'contractNumber')

    const resultComponent = renderer.create(element.renderer('cell', {shortPlanName: 'spn', contractId: 123}))

    expect(resultComponent.toJSON()).toMatchSnapshot()
  })
})
