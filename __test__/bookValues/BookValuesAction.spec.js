import http from '../../src/actions/http'
import moment from 'moment'

import {apiPath} from '../../src/config'
import {
  loadAllMonthlyBookValues,
  selectDailyBookValues,
  filterMonthly,
  filterDailyBookValues,
  setContractId,
  clearContractId,
  setDateFilter,
  setFilterOnDay
} from '../../src/bookValues/BookValueActions'

jest.mock('../../src/actions/http', () => {
  return {
    get: jest.fn()
  }
})

describe('BookValuesActions', () => {
  let dispatch, getState, state
  beforeEach(() => {
    state = {bookValues: {currentContractId: 321, filterOnDay: false, dateFilter: moment()}}
    http.get.mockClear()
    dispatch = jest.fn()
    getState = jest.fn(() => state)
  })
  it('loadAllMonthlyBookValues should return action', () => {
    const res = loadAllMonthlyBookValues()

    expect(res.type).toBe('LOAD_ALL_BOOK_VALUES')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/balances/monthly`)
  })
  it('selectDailyBookValues should return action with passed in contractId', () => {
    selectDailyBookValues(123)(dispatch, getState)

    const res = dispatch.mock.calls[0][0]

    expect(res.type).toBe('SELECT_BOOK_VALUES_BY_CONTRACT_ID')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/balances/daily`)
    expect(http.get.mock.calls[0][1]).toEqual({contractId: 123})
  })
  it('selectDailyBookValues should return action with contractId in state', () => {
    selectDailyBookValues()(dispatch, getState)

    const res = dispatch.mock.calls[0][0]

    expect(res.type).toBe('SELECT_BOOK_VALUES_BY_CONTRACT_ID')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/balances/daily`)
    expect(http.get.mock.calls[0][1]).toEqual({contractId: 321})
  })
  it('filterMonthly should return action without filterOnDay', () => {
    filterMonthly()(dispatch, getState)

    const res = dispatch.mock.calls[0][0]

    expect(res.type).toBe('FILTER_ALL_BOOK_VALUES')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/balances/monthly`)
    expect(http.get.mock.calls[0][1]).toEqual({dateFilter: state.bookValues.dateFilter.format()})
  })
  it('filterMonthly should return action with filterOnDay', () => {
    state.bookValues.filterOnDay = true
    filterMonthly()(dispatch, getState)

    const res = dispatch.mock.calls[0][0]

    expect(res.type).toBe('FILTER_ALL_BOOK_VALUES')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/balances/monthly`)
    expect(http.get.mock.calls[0][1]).toEqual({dateFilter: state.bookValues.dateFilter.format(), filterOnDay: true})
  })
  it('filterDailyBookValues should return action', () => {
    filterDailyBookValues()(dispatch, getState)

    const res = dispatch.mock.calls[0][0]

    expect(res.type).toBe('FILTER_BOOK_VALUES_BY_CONTRACT_ID')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/balances/daily`)
    expect(http.get.mock.calls[0][1]).toEqual({contractId: 321, dateFilter: state.bookValues.dateFilter.format()})
  })
  it('setContractId should return action', () => {
    const res = setContractId(123)

    expect(res.type).toBe('SET_CONTRACT_ID')
    expect(res.payload).toBe(123)
  })
  it('clearContractId should return action', () => {
    const res = clearContractId()

    expect(res.type).toBe('SET_CONTRACT_ID')
    expect(res.payload).toBe(null)
  })
  it('setDateFilter should return action', () => {
    const res = setDateFilter('date')

    expect(res.type).toBe('SET_DATE_FILTER')
    expect(res.payload).toBe('date')
  })
  it('setFilterOnDay should return action', () => {
    const res = setFilterOnDay('day')

    expect(res.type).toBe('SET_FILTER_ON_DAY')
    expect(res.payload).toBe('day')
  })
})
