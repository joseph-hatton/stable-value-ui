import {currencyFormat, dateFormat, percentFormat} from '../../../src/components/grid'
import {FormattedNumber} from 'react-intl'
import mockComponent from '../../MockComponent'
import renderer from 'react-test-renderer'

import {DailyBookValuesColumns} from '../../../src/bookValues/daily/DailyBookValuesColumns'

jest.mock('../../../src/components/grid', () => ({
  currencyFormat: 'currencyFormat',
  dateFormat: 'dateFormat',
  percentFormat: 'percentFormat'
}))

jest.mock('react-intl', () => ({
  FormattedNumber: 'FormattedNumber'
}))

describe('DailyBookValuesColumns', () => {
  let cols
  beforeAll(() => {
    cols = DailyBookValuesColumns()
  })
  it('should call currencyFormat', () => {
    const currencyFormatCols = [
      'beginningBookValue',
      'deposits',
      'withdrawals',
      'adjustments',
      'interest',
      'endingBookValue'
    ]

    currencyFormatCols.forEach((col) => {
      const element = cols.find((i) => i.field === col)
      expect(element.renderer).toBe('currencyFormat')
    })
  })
  it('should call dateFormat', () => {
    const currencyFormatCols = [
      'effectiveDate',
      'rateEffectiveDate'
    ]

    currencyFormatCols.forEach((col) => {
      const element = cols.find((i) => i.field === col)
      expect(element.renderer).toBe('dateFormat')
    })
  })
  it('should call rate renderer with 3 decimals', () => {
    const currencyFormatCols = [
      'rate'
    ]

    currencyFormatCols.forEach((col) => {
      const element = cols.find((i) => i.field === col)
      const component = renderer.create(element.renderer(1.1234, {creditingRateRoundDecimals: 3}))
      expect(component.toJSON()).toMatchSnapshot()
    })
  })

  it('should call rate renderer with default 2 decimals', () => {
    const currencyFormatCols = [
      'rate'
    ]

    currencyFormatCols.forEach((col) => {
      const element = cols.find((i) => i.field === col)
      const component = renderer.create(element.renderer(1.1234, {}))
      expect(component.toJSON()).toMatchSnapshot()
    })
  })
})
