import React from 'react'
import mockComponent from '../../MockComponent'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'

import DailyBookValuesFilter from '../../../src/bookValues/daily/DailyBookValuesFilter'
import {DailyBookValuesColumns} from '../../../src/bookValues/daily/DailyBookValuesColumns'
import {Grid, CreateGridConfig} from '../../../src/components/grid'
import {selectDailyBookValues, setContractId, filterDailyBookValues} from '../../../src/bookValues/BookValueActions'

import DailyBookValues from '../../../src/bookValues/daily/DailyBookValues'

jest.mock('react-redux', () => ({
  connect: jest.fn(() => jest.fn(component => component))
}))
jest.mock('../../../src/bookValues/BookValueActions', () => ({
  selectDailyBookValues: jest.fn(),
  setContractId: jest.fn(),
  filterDailyBookValues: jest.fn()
}))
jest.mock('../../../src/bookValues/daily/DailyBookValuesFilter', () => mockComponent('DailyBookValuesFilter'))
jest.mock('../../../src/bookValues/daily/DailyBookValuesColumns', () => ({
  DailyBookValuesColumns: () => []
}))
jest.mock('../../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn().mockReturnValue({})
}))

describe('Daily Book Values', () => {
  let props
  beforeEach(() => {
    props = {
      loadedBookValues: [],
      params: {contractId: 123},
      selectDailyBookValues: jest.fn(),
      setContractId: jest.fn(),
      filterDailyBookValues: jest.fn()
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<DailyBookValues {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should call selectDailyBookValues', () => {
    props.filtered = false

    const component = renderer.create(<DailyBookValues {...props} />)

    expect(props.selectDailyBookValues.mock.calls.length).toBe(1)
    expect(props.filterDailyBookValues.mock.calls.length).toBe(0)
  })
  it('should call selectDailyBookValues', () => {
    props.filtered = true

    const component = renderer.create(<DailyBookValues {...props} />)

    expect(props.selectDailyBookValues.mock.calls.length).toBe(0)
    expect(props.filterDailyBookValues.mock.calls.length).toBe(1)
  })
})
