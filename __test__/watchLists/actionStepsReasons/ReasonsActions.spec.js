import {apiPath} from '../../../src/config'
import http from '../../../src/actions/http'

import {
  deleteReason,
  selectReason,
  saveReason
} from '../../../src/watchLists/actionStepsReasons/ReasonsActions'

jest.mock('../../../src/actions/http', () => ({
  delete: jest.fn(),
  put: jest.fn(),
  post: jest.fn()
}))

describe('ReasonsActions', () => {
  let dispatch, getState, state
  beforeEach(() => {
    dispatch = jest.fn()
    state = {}
    getState = jest.fn().mockReturnValue(state)
    http.delete.mockClear()
    http.put.mockClear()
    http.post.mockClear()
  })
  it('deleteReason should dispatch', () => {
    deleteReason({reasonId: 123})(dispatch)
    const action = dispatch.mock.calls[0][0]
    expect(action.type).toBe('DELETE_REASON')
    expect(http.delete).toHaveBeenCalledTimes(1)
    expect(http.delete.mock.calls[0][0]).toBe(`${apiPath}/reasons/123`)
  })
  it('selectReason should dispatch', () => {
    selectReason('reason')(dispatch)
    const action = dispatch.mock.calls[0][0]
    expect(action.type).toBe('SELECT_REASON')
    expect(action.payload).toBe('reason')
  })
  it('saveReason should dispatch update', () => {
    state.form = {ReasonAddEditForm: {values: {val: 'val'}}}
    state.watchLists = {selectedReason: {reasonId: 123}}
    saveReason()(dispatch, getState)
    const action = dispatch.mock.calls[0][0]
    expect(action.type).toBe('SAVE_REASON')
    expect(http.put.mock.calls.length).toBe(1)
    expect(http.put.mock.calls[0][0]).toBe(`${apiPath}/reasons/123`)
    expect(http.put.mock.calls[0][1]).toEqual({val: 'val', standardSw: true, reasonId: 123})
    expect(http.put.mock.calls[0][2]).toEqual({successMessage: 'Reason Saved Successfully!'})
    expect(http.put.mock.calls[0][3]).toBe('reasonAddEdit')
  })
  it('saveReason should dispatch save new', () => {
    state.form = {ReasonAddEditForm: {values: {val: 'val'}}}
    state.watchLists = {selectedReason: {}}
    saveReason()(dispatch, getState)
    const action = dispatch.mock.calls[0][0]
    expect(action.type).toBe('SAVE_REASON')
    expect(http.post.mock.calls.length).toBe(1)
    expect(http.post.mock.calls[0][0]).toBe(`${apiPath}/reasons`)
    expect(http.post.mock.calls[0][1]).toEqual({val: 'val', standardSw: true})
    expect(http.post.mock.calls[0][2]).toEqual({successMessage: 'Reason Saved Successfully!'})
    expect(http.post.mock.calls[0][3]).toBe('reasonAddEdit')
  })
})
