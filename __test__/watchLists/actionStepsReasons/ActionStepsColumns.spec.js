import mockComponent from '../../MockComponent'
import renderer from 'react-test-renderer'

import {ActionStepsColumns} from '../../../src/watchLists/actionStepsReasons/ActionStepsColumns'

jest.mock('../../../src/components/common/MultiActionMenu', () => ({
  MultiActionMenuLink: mockComponent('MultiActionMenuLink')
}))

describe('Action Steps Columns', () => {
  it('should render Menu Link correctly', () => {
    const col = ActionStepsColumns().find((i) => i.field === 'description')
    const component = renderer.create(col.renderer('cell', 'row'))
    expect(component.toJSON()).toMatchSnapshot()
  })
})
