import React, { Component } from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from '../../MockComponent'
import {hasEntitlement} from '../../../src/entitlements'

import ActionStepsReasons from '../../../src/watchLists/actionStepsReasons/ActionStepsReasons'

jest.mock('react-redux', () => ({
  connect: () => (comp) => comp
}))

jest.mock('../../../src/watchLists/actionStepsReasons/ActionStepsActions', () => ({
  selectActionStep: 'selectActionStep',
  deleteActionStep: 'selectActionStep'
}))

jest.mock('../../../src/watchLists/actionStepsReasons/ActionStepsColumns', () => ({
  ActionStepsColumns: () => 'ActionStepsColumns'
}))

jest.mock('../../../src/watchLists/actionStepsReasons/ReasonsActions', () => ({
  selectReason: 'selectReason',
  deleteReason: 'deleteReason'
}))

jest.mock('../../../src/watchLists/actionStepsReasons/ReasonsColumns', () => ({
  ReasonsColumns: () => 'ReasonsColumns'
}))

jest.mock('../../../src/components/redux-dialog', () => ({
  openDialog: jest.fn()
}))

jest.mock('../../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn()
}))
jest.mock('../../../src/components/fab-dial', () => ({
  SpeedDial: mockComponent('SpeedDial'),
  SpeedDialItem: mockComponent('SpeedDialItem')
}))
jest.mock('material-ui/Card', () => ({
  Card: mockComponent('Card'),
  CardTitle: mockComponent('CardTitle')
}))
jest.mock('material-ui/Menu', () => ({
  Menu: mockComponent('Menu'),
  MenuItem: mockComponent('MenuItem')
}))

jest.mock('../../../src/components/common/MultiActionMenu', () => mockComponent('MultiActionMenu'))
jest.mock('../../../src/watchLists/actionStepsReasons/ActionStepsAddEdit', () => mockComponent('ActionStepsAddEdit'))
jest.mock('../../../src/watchLists/actionStepsReasons/ReasonAddEdit', () => mockComponent('ReasonAddEdit'))
jest.mock('material-ui/FloatingActionButton', () => mockComponent('FloatingActionButton'))
jest.mock('material-ui/svg-icons/content/add', () => mockComponent('ContentAdd'))
jest.mock('material-ui/svg-icons/editor/mode-edit', () => mockComponent('EditIcon'))
jest.mock('material-ui/svg-icons/action/delete', () => mockComponent('DeleteIcon'))
jest.mock('material-ui/svg-icons/navigation/close', () => mockComponent('NavigationClose'))
jest.mock('../../../src/entitlements', () => ({
  hasEntitlement: jest.fn().mockReturnValue(true)
}))

describe('ActionStepsReasons', () => {
  let props
  beforeEach(() => {
    props = {
      openDialog: jest.fn(),
      loadActionSteps: jest.fn(),
      selectActionStep: jest.fn(),
      selectReason: jest.fn(),
      reasons: [],
      currentTab: 'actionStepsReasons'
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<ActionStepsReasons {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
    expect(props.loadActionSteps.mock.calls.length).toBe(1)
  })
  it('actionsteps new onClick should respond appropriately', () => {
    const component = shallow(<ActionStepsReasons {...props} />)
    component.find('#adding-new-ActionSteps').prop('onClick')()

    expect(props.selectActionStep).toHaveBeenCalledTimes(1)
    expect(props.openDialog).toHaveBeenCalledTimes(1)
  })
  it('reasons new onClick should respond appropriately', () => {
    const component = shallow(<ActionStepsReasons {...props} />)

    component.find('#adding-new-Reasons').prop('onClick')()

    expect(props.selectReason).toHaveBeenCalledTimes(1)
    expect(props.openDialog).toHaveBeenCalledTimes(1)
  })
  it('should hide the button when the tab isn\'t actionSteps', () => {
    props.currentTab = 'none'
    const component = renderer.create(<ActionStepsReasons {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
    expect(props.loadActionSteps.mock.calls.length).toBe(1)
  })
  it('should hide the button when no entitlement', () => {
    hasEntitlement.mockReturnValue(false)
    const component = renderer.create(<ActionStepsReasons {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
})
