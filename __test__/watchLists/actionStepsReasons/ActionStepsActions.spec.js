import {apiPath} from '../../../src/config'
import http from '../../../src/actions/http'

import {
  deleteActionStep,
  selectActionStep,
  saveActionStep
} from '../../../src/watchLists/actionStepsReasons/ActionStepsActions'

jest.mock('../../../src/actions/http', () => ({
  delete: jest.fn(),
  put: jest.fn(),
  post: jest.fn()
}))

describe('ActionStepsActions', () => {
  let dispatch, getState, state
  beforeEach(() => {
    dispatch = jest.fn()
    state = {}
    getState = jest.fn().mockReturnValue(state)
    http.delete.mockClear()
    http.put.mockClear()
    http.post.mockClear()
  })
  it('deleteActionStep should dispatch', () => {
    deleteActionStep({actionStepId: 123})(dispatch)
    const action = dispatch.mock.calls[0][0]
    expect(action.type).toBe('DELETE_ACTION_STEP')
    expect(http.delete).toHaveBeenCalledTimes(1)
    expect(http.delete.mock.calls[0][0]).toBe(`${apiPath}/action-steps/123`)
  })
  it('selectActionStep should dispatch', () => {
    selectActionStep('actionStep')(dispatch)
    const action = dispatch.mock.calls[0][0]
    expect(action.type).toBe('SELECT_ACTION_STEP')
    expect(action.payload).toBe('actionStep')
  })
  it('saveActionStep should dispatch update', () => {
    state.form = {ActionStepAddEditForm: {values: {val: 'val'}}}
    state.watchLists = {selectedActionStep: {actionStepId: 123}}
    saveActionStep()(dispatch, getState)
    const action = dispatch.mock.calls[0][0]
    expect(action.type).toBe('SAVE_ACTION_STEP')
    expect(http.put.mock.calls.length).toBe(1)
    expect(http.put.mock.calls[0][0]).toBe(`${apiPath}/action-steps/123`)
    expect(http.put.mock.calls[0][1]).toEqual({val: 'val', standardSw: true, actionStepId: 123})
    expect(http.put.mock.calls[0][2]).toEqual({successMessage: 'Action Step Saved Successfully!'})
    expect(http.put.mock.calls[0][3]).toBe('actionStepAddEdit')
  })
  it('saveActionStep should dispatch save new', () => {
    state.form = {ActionStepAddEditForm: {values: {val: 'val'}}}
    state.watchLists = {selectedActionStep: {}}
    saveActionStep()(dispatch, getState)
    const action = dispatch.mock.calls[0][0]
    expect(action.type).toBe('SAVE_ACTION_STEP')
    expect(http.post.mock.calls.length).toBe(1)
    expect(http.post.mock.calls[0][0]).toBe(`${apiPath}/action-steps`)
    expect(http.post.mock.calls[0][1]).toEqual({val: 'val', standardSw: true})
    expect(http.post.mock.calls[0][2]).toEqual({successMessage: 'Action Step Saved Successfully!'})
    expect(http.post.mock.calls[0][3]).toBe('actionStepAddEdit')
  })
})
