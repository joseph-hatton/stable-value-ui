import mockComponent from '../../MockComponent'
import renderer from 'react-test-renderer'

import {ReasonsColumns} from '../../../src/watchLists/actionStepsReasons/ReasonsColumns'

jest.mock('../../../src/components/common/MultiActionMenu', () => ({
  MultiActionMenuLink: mockComponent('MultiActionMenuLink')
}))

describe('Reasons Columns', () => {
  it('should render Menu Link correctly', () => {
    const col = ReasonsColumns().find((i) => i.field === 'description')
    const component = renderer.create(col.renderer('cell', 'row'))
    expect(component.toJSON()).toMatchSnapshot()
  })
})
