import React, { Component } from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from '../../MockComponent'
import {hasEntitlement} from '../../../src/entitlements'

import ReasonAddEdit from '../../../src/watchLists/actionStepsReasons/ReasonAddEdit'

jest.mock('react-redux', () => ({
  connect: () => (comp) => comp
}))

jest.mock('redux-form', () => ({
  reduxForm: () => (comp) => comp,
  Field: mockComponent('Field'),
  reset: jest.fn()
}))

jest.mock('../../../src/components/redux-dialog', () => ({
  closeDialog: jest.fn()
}))

jest.mock('material-ui', () => ({
  RaisedButton: mockComponent('RaisedButton')
}))

jest.mock('../../../src/watchLists/actionStepsReasons/ReasonsActions', () => ({
  saveReason: jest.fn()
}))

jest.mock('../../../src/entitlements', () => ({
  hasEntitlement: jest.fn().mockReturnValue(true)
}))

jest.mock('../../../src/components/redux-dialog/redux-dialog', () => mockComponent('ReduxDialog'))

describe('Reason Add Edit', () => {
  let props
  beforeEach(() => {
    props = {
      closeDialog: jest.fn(),
      saveReason: jest.fn(),
      handleSubmit: jest.fn(),
      initialValues: {},
      reset: jest.fn()
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<ReasonAddEdit {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should render title correctly when reasonId is set', () => {
    props.reasonId = 123
    const component = renderer.create(<ReasonAddEdit {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('cancel onClick should call closeDialog', () => {
    const component = shallow(<ReasonAddEdit {...props} />)

    component.prop('actions')[1].props.onClick()

    expect(props.closeDialog.mock.calls.length).toBe(1)
    expect(props.reset.mock.calls.length).toBe(1)
  })
  it('should disable save correctly', () => {
    hasEntitlement.mockReturnValue(false)
    const component = renderer.create(<ReasonAddEdit {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
})
