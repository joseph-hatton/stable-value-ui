import React, { Component } from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from '../../MockComponent'
import {hasEntitlement} from '../../../src/entitlements'

import ActionStepsAddEdit from '../../../src/watchLists/actionStepsReasons/ActionStepsAddEdit'

jest.mock('react-redux', () => ({
  connect: () => (comp) => comp
}))

jest.mock('redux-form', () => ({
  reduxForm: () => (comp) => comp,
  Field: mockComponent('Field'),
  reset: jest.fn()
}))

jest.mock('../../../src/components/redux-dialog', () => ({
  closeDialog: jest.fn()
}))

jest.mock('material-ui', () => ({
  RaisedButton: mockComponent('RaisedButton')
}))

jest.mock('../../../src/watchLists/actionStepsReasons/ActionStepsActions', () => ({
  saveActionStep: jest.fn()
}))

jest.mock('../../../src/entitlements', () => ({
  hasEntitlement: jest.fn().mockReturnValue(true)
}))

jest.mock('../../../src/components/redux-dialog/redux-dialog', () => mockComponent('ReduxDialog'))

describe('Action Step Add Edit', () => {
  let props
  beforeEach(() => {
    props = {
      closeDialog: jest.fn(),
      saveActionStep: jest.fn(),
      handleSubmit: jest.fn(),
      reset: jest.fn(),
      initialValues: {}
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<ActionStepsAddEdit {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should render title correctly when actionStepuId is set', () => {
    props.actionStepId = 123
    const component = renderer.create(<ActionStepsAddEdit {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('cancel onClick should call closeDialog', () => {
    const component = shallow(<ActionStepsAddEdit {...props} />)

    component.prop('actions')[1].props.onClick()

    expect(props.closeDialog.mock.calls.length).toBe(1)
    expect(props.reset.mock.calls.length).toBe(1)
  })
  it('hasEntitlement should disable submit when false', () => {
    hasEntitlement.mockReturnValue(false)
    const component = renderer.create(<ActionStepsAddEdit {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
})
