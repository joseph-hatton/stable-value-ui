import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from '../../MockComponent'

import WatchListMenu, {WatchListMenuLink} from '../../../src/watchLists/watchLists/WatchListMenu'

jest.mock('react-redux', () => ({
  connect: jest.fn(() => (comp) => comp)
}))
jest.mock('react-router', () => ({
  Link: mockComponent('Link')
}))
jest.mock('material-ui', () => ({
  Popover: mockComponent('Popover'),
  Menu: mockComponent('Menu'),
  MenuItem: mockComponent('MenuItem')
}))

describe('WatchListMenu', () => {
  let props
  beforeEach(() => {
    props = {
      watchList: {watchListId: 123},
      open: true,
      closeDialog: jest.fn(),
      openDialog: jest.fn(),
      deleteWatchList: jest.fn()
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<WatchListMenu {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should call closeDialog', () => {
    const component = shallow(<WatchListMenu {...props} />)

    component.prop('onRequestClose')()

    expect(props.closeDialog.mock.calls.length).toBe(1)
  })
  it('should call deleteWatchList and close Dialog', () => {
    const component = shallow(<WatchListMenu {...props} />)

    component.find('#watchListMenu-deleteButton').prop('onClick')()

    expect(props.deleteWatchList.mock.calls.length).toBe(1)
    expect(props.closeDialog.mock.calls.length).toBe(1)
  })
  describe('WatchListMenuLink', () => {
    let linkProps
    beforeEach(() => {
      linkProps = {
        cell: 'cell',
        row: {},
        openDialog: jest.fn(),
        selectWatchList: jest.fn()
      }
    })
    it('should render correctly', () => {
      const component = renderer.create(<WatchListMenuLink {...linkProps} />)
      expect(component.toJSON()).toMatchSnapshot()
    })
    it('should call prevDefault, select WL, Open Dialog', () => {
      const component = shallow(<WatchListMenuLink {...linkProps} />)
      const e = {nativeEvent: {preventDefault: jest.fn()}}

      component.prop('onClick')(e)

      expect(e.nativeEvent.preventDefault.mock.calls.length).toBe(1)
      expect(linkProps.selectWatchList.mock.calls.length).toBe(1)
      expect(linkProps.openDialog.mock.calls.length).toBe(1)
    })
  })
})
