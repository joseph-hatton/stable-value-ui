import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'
import {shallow} from 'enzyme'
import {connect} from 'react-redux'

import WatchLists from '../../../src/watchLists/watchLists/WatchLists'

jest.mock('react-redux', () => ({
  connect: jest.fn(() => (comp) => comp)
}))

jest.mock('react-router', () => ({
  Link: mockComponent('Link')
}))

jest.mock('../../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn()
}))

jest.mock('../../../src/watchLists/watchLists/WatchListColumns', () => ({
  WatchListColumns: jest.fn()
}))

jest.mock('../../../src/components/redux-dialog/redux-dialog', () => ({
  closeDialog: 'closeDialog',
  openDialog: 'openDialog'
}))

jest.mock('../../../src/watchLists/watchLists/WatchListActions', () => ({
  loadAll: jest.fn()
}))

jest.mock('material-ui/FloatingActionButton', () => mockComponent('FloatingActionButton'))

jest.mock('../../../src/watchLists/watchLists/WatchListMenu', () => mockComponent('WatchListMenu'))

jest.mock('../../../src/watchLists/watchLists/WatchListAddEdit', () => mockComponent('WatchListAddEdit'))

jest.mock('material-ui/svg-icons/content/add', () => mockComponent('ContentAdd'))

describe('WatchLists', () => {
  let props
  beforeEach(() => {
    props = {
      loadAll: jest.fn(),
      selectWatchList: jest.fn(),
      openDialog: jest.fn(),
      closeDialog: jest.fn(),
      currentTab: 'watchLists'
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<WatchLists {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should call load all', () => {
    const component = renderer.create(<WatchLists {...props} />)
    expect(props.loadAll.mock.calls.length).toBe(1)
  })
  it('should hide the button when the tab isn\'t watchLists', () => {
    props.currentTab = 'none'
    const component = renderer.create(<WatchLists {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
    expect(props.loadAll.mock.calls.length).toBe(1)
  })
  it('should reset selected watchlist on add', () => {
    const component = shallow(<WatchLists {...props} />)
    component.find('#addNewWatchList-fab').prop('onClick')()
    expect(props.selectWatchList.mock.calls.length).toBe(1)
  })
})
