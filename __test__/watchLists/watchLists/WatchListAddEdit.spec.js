import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'
import {chipify} from '../../../src/watchLists/chipify'
import {connect} from 'react-redux'
import {change} from 'redux-form'

import WatchListAddEdit, {menuItems, displayArray} from '../../../src/watchLists/watchLists/WatchListAddEdit'

jest.mock('react-redux', () => ({
  connect: jest.fn(() => {
    return (component) => component
  })
}))

jest.mock('material-ui/Toolbar', () => ({
  Toolbar: mockComponent('Toolbar'),
  ToolbarGroup: mockComponent('ToolbarGroup')
}))

jest.mock('../../../src/components/redux-dialog', () => mockComponent('ReduxDialog'))
jest.mock('../../../src/components/redux-dialog/redux-dialog', () => ({
  closeDialog: 'closeDialog'
}))

jest.mock('material-ui/RaisedButton', () => mockComponent('RaisedButton'))

jest.mock('material-ui/MenuItem', () => mockComponent('MenuItem'))

jest.mock('../../../src/contracts/ContractPicker', () => mockComponent('ContractPicker'))

jest.mock('material-ui/Card', () => ({
  Card: mockComponent('Card')
}))

jest.mock('../../../src/watchLists/chipify', () => ({
  chipify: jest.fn()
}))

jest.mock('../../../src/watchLists/watchLists/WatchListActions.js', () => ({
  loadReasons: jest.fn(),
  loadActionSteps: jest.fn(),
  saveWatchList: jest.fn(),
  loadWatchList: jest.fn()
}))

jest.mock('react-router', () => ({
  Link: mockComponent('react-router')
}))

jest.mock('redux-form-material-ui', () => ({
  DatePicker: mockComponent('DatePicker'),
  SelectField: mockComponent('SelectField'),
  TextField: mockComponent('TextField')
}))

jest.mock('redux-form', () => ({
  reduxForm: () => (comp) => comp,
  Field: mockComponent('Field'),
  change: jest.fn()
}))

describe('WatchListAddEdit', () => {
  beforeEach(() => {
    chipify.mockClear()
  })
  it('should render correctly', () => {
    const props = {
      loadReasons: jest.fn(),
      loadActionSteps: jest.fn(),
      saveWatchList: jest.fn(),
      loadWatchList: jest.fn(),
      params: {watchListId: 123},
      reasons: [],
      actionSteps: [],
      handleSubmit: jest.fn(),
      deleteReason: jest.fn(),
      deleteActionStep: jest.fn()
    }
    const component = renderer.create(<WatchListAddEdit {...props}/>)

    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should load reasons, action steps and watchlist', () => {
    const props = {
      loadReasons: jest.fn(),
      loadActionSteps: jest.fn(),
      saveWatchList: jest.fn(),
      params: {watchListId: 123},
      reasons: [],
      actionSteps: [],
      handleSubmit: jest.fn(),
      deleteReason: jest.fn(),
      deleteActionStep: jest.fn()
    }
    const component = renderer.create(<WatchListAddEdit {...props}/>)

    expect(props.loadReasons.mock.calls.length).toBe(1)
    expect(props.loadActionSteps.mock.calls.length).toBe(1)
  })
  it('should load reasons and action steps', () => {
    const props = {
      loadReasons: jest.fn(),
      loadActionSteps: jest.fn(),
      saveWatchList: jest.fn(),
      loadWatchList: jest.fn(),
      params: {},
      reasons: [],
      actionSteps: [],
      handleSubmit: jest.fn(),
      deleteReason: jest.fn(),
      deleteActionStep: jest.fn()
    }
    const component = renderer.create(<WatchListAddEdit {...props}/>)

    expect(props.loadReasons.mock.calls.length).toBe(1)
    expect(props.loadActionSteps.mock.calls.length).toBe(1)
    expect(props.loadWatchList.mock.calls.length).toBe(0)
  })

  it('menuItems should render correctly', () => {
    const items = [
      {description: 'test1'},
      {description: 'test2'},
      {description: 'test3'}
    ]
    const component = renderer.create(<div> {menuItems(items, 'description')} </div>)

    expect(component.toJSON()).toMatchSnapshot()
  })

  it('displayArray should call chipify', () => {
    const items = []
    displayArray(items)

    expect(chipify.mock.calls.length).toBe(1)
  })

  describe('actions', () => {
    let actions, dispatch, getState, state
    beforeEach(() => {
      const props = {
        loadReasons: jest.fn(),
        loadActionSteps: jest.fn(),
        saveWatchList: jest.fn(),
        loadWatchList: jest.fn(),
        params: {},
        reasons: [],
        actionSteps: [],
        handleSubmit: jest.fn(),
        deleteReason: jest.fn(),
        deleteActionStep: jest.fn()
      }
      renderer.create(<WatchListAddEdit {...props}/>)
      actions = connect.mock.calls[0][1]
      dispatch = jest.fn()
      state = {form: {WatchListAddEditForm: {values: {}}}}
      getState = jest.fn().mockReturnValue(state)
      change.mockClear()
    })

    it('deleteReason', () => {
      const reasons = [{reasonId: 123}, {reasonId: 321}]
      state.form.WatchListAddEditForm.values = {reasons}
      actions.deleteReason(null, {reasonId: 123})(dispatch, getState)

      expect(dispatch.mock.calls.length).toBe(1)
      expect(change.mock.calls.length).toBe(1)
      expect(change.mock.calls[0][2].length).toBe(1)
      expect(change.mock.calls[0][2][0]).toBe(reasons[1])
    })

    it('deleteActionStep', () => {
      const actionSteps = [{actionStepId: 123}, {actionStepId: 321}]
      state.form.WatchListAddEditForm.values = {actionSteps}
      actions.deleteActionStep(null, {actionStepId: 123})(dispatch, getState)

      expect(dispatch.mock.calls.length).toBe(1)
      expect(change.mock.calls.length).toBe(1)
      expect(change.mock.calls[0][2].length).toBe(1)
      expect(change.mock.calls[0][2][0]).toBe(actionSteps[1])
    })
  })
})
