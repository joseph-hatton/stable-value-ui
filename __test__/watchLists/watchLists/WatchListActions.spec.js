import http from '../../../src/actions/http'
import {apiPath} from '../../../src/config'

import {
  loadAll,
  loadReasons,
  loadActionSteps,
  saveWatchList,
  loadWatchList,
  deleteWatchList,
  selectWatchList
} from '../../../src/watchLists/watchLists/WatchListActions'

jest.mock('../../../src/actions/http', () => ({
  get: jest.fn().mockReturnValue('get'),
  put: jest.fn().mockReturnValue('put'),
  post: jest.fn().mockReturnValue('post'),
  delete: jest.fn().mockReturnValue('delete')
}))

describe('WatchListActions', () => {
  let dispatch, getState, state
  beforeEach(() => {
    http.get.mockClear()
    http.put.mockClear()
    http.post.mockClear()
    http.delete.mockClear()
    state = {}
    dispatch = jest.fn()
    getState = jest.fn().mockReturnValue(state)
  })
  it('load all should return action', () => {
    const result = loadAll()

    expect(result.type).toBe('LOAD_ALL_WATCH_LISTS')
    expect(result.payload).toBe('get')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/watch-lists`)
  })

  it('load reasons should return action', () => {
    const result = loadReasons()

    expect(result.type).toBe('LOAD_REASONS')
    expect(result.payload).toBe('get')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/reasons`)
  })

  it('load action steps should return action', () => {
    const result = loadActionSteps()

    expect(result.type).toBe('LOAD_ACTION_STEPS')
    expect(result.payload).toBe('get')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/action-steps`)
  })

  it('save watch list should save if watchlist id isn\'t set', () => {
    state.form = {WatchListAddEditForm: {values: 'values'}}
    state.watchLists = {currentWatchList: {}}

    saveWatchList()(dispatch, getState)

    const result = dispatch.mock.calls[0][0]

    expect(result.type).toBe('LOAD_WATCH_LIST')
    expect(result.payload).toBe('post')
    expect(http.post.mock.calls[0][0]).toBe(`${apiPath}/watch-lists`)
    expect(http.post.mock.calls[0][1]).toBe('values')
  })

  it('save watch list should update if watchlist id set', () => {
    state.form = {WatchListAddEditForm: {values: 'values'}}
    state.watchLists = {currentWatchList: {watchListId: 123}}

    saveWatchList()(dispatch, getState)

    const result = dispatch.mock.calls[0][0]

    expect(result.type).toBe('LOAD_WATCH_LIST')
    expect(result.payload).toBe('put')
    expect(http.put.mock.calls[0][0]).toBe(`${apiPath}/watch-lists/123`)
    expect(http.put.mock.calls[0][1]).toBe('values')
  })

  it('load watch list should return action', () => {
    const result = loadWatchList(123)

    expect(result.type).toBe('LOAD_WATCH_LIST')
    expect(result.payload).toBe('get')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/watch-lists/123`)
  })

  it('delete watch list should return action', () => {
    const result = deleteWatchList({watchListId: 123})

    expect(result.type).toBe('DELETE_WATCH_LIST')
    expect(result.payload).toBe('delete')
    expect(http.delete.mock.calls[0][0]).toBe(`${apiPath}/watch-lists/123`)
  })

  it('select watch list should return action', () => {
    const result = selectWatchList('watchList')

    expect(result.type).toBe('SELECT_WATCH_LIST')
    expect(result.payload).toBe('watchList')
  })
})
