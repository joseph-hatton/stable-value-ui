import React from 'react'
import renderer from 'react-test-renderer'
import {dateFormat} from '../../../src/components/grid'
import mockComponent from '../../MockComponent'

import {WatchListColumns} from '../../../src/watchLists/watchLists/WatchListColumns'

jest.mock('../../../src/components/grid', () => ({
  currencyFormat: 'currencyFormat',
  dateFormat: 'dateFormat'
}))

jest.mock('../../../src/watchLists/chipify', () => ({
  chipify: 'chipify'
}))

jest.mock('../../../src/watchLists/watchLists/WatchListMenu', () => ({
  WatchListMenuLink: mockComponent('WatchListMenuLink')
}))

describe('WatchListColumns', () => {
  let cols
  beforeAll(() => {
    cols = WatchListColumns()
  })
  it('should call dateFormat', () => {
    const currencyFormatCols = [
      'effectiveDate',
      'modifiedDate'
    ]

    currencyFormatCols.forEach((col) => {
      const element = cols.find((i) => i.field === col)
      expect(element.renderer).toBe('dateFormat')
    })
  })
  it('should render WatchListMenuLink', () => {
    const element = cols.find((i) => i.field === 'contractNumber')
    const component = renderer.create(element.renderer('cell', 'row'))
    expect(component.toJSON()).toMatchSnapshot()
  })
})
