import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from '../MockComponent'

import { chipify } from '../../src/watchLists/chipify'

jest.mock('material-ui/Chip', () => mockComponent('Chip'))

const cell = [
  {description: 'test1'},
  {description: 'test2'}
]

describe('chipify', () => {
  it('should render correctly', () => {
    const component = renderer.create(chipify(cell))
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should render correctly when inline and onDelete is set', () => {
    const component = renderer.create(chipify(cell, null, true, () => {}))
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should call onDelete', () => {
    const onDelete = jest.fn()
    const component = shallow(chipify(cell, null, false, onDelete))

    component.prop('children')[0].props.onRequestDelete()

    expect(onDelete.mock.calls.length).toBe(1)
  })
})
