import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../MockComponent'
import {shallow} from 'enzyme'
import {changeCurrentTab} from '../../src/watchLists/watchLists/WatchListActions'
import {connect} from 'react-redux'

import WatchListContainer from '../../src/watchLists/WatchListContainer'

jest.mock('react-redux', () => ({
  connect: jest.fn(() => (comp) => comp)
}))

jest.mock('../../src/watchLists/watchLists/WatchListActions', () => ({
  changeCurrentTab: jest.fn()
}))

jest.mock('material-ui/Tabs', () => ({
  Tabs: mockComponent('Tabs'),
  Tab: mockComponent('Tab')
}))

jest.mock('../../src/watchLists/watchLists/WatchLists', () => mockComponent('WatchLists'))
jest.mock('../../src/watchLists/actionStepsReasons/ActionStepsReasons', () => mockComponent('ActionStepsReasons'))

describe('WatchListContainer', () => {
  let props
  beforeEach(() => {
    props = {
      changeTab: jest.fn()
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<WatchListContainer {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should render children only if there are any', () => {
    props.children = <span>Testing</span>
    const component = renderer.create(<WatchListContainer {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should change tab on active for wl', () => {
    const component = shallow(<WatchListContainer {...props} />)

    component.children('#WatchListTab').prop('onActive')()

    expect(props.changeTab.mock.calls.length).toBe(1)
    expect(props.changeTab.mock.calls[0][0]).toBe('watchLists')
  })
  it('should change tab on active for r', () => {
    const component = shallow(<WatchListContainer {...props} />)

    component.children('#ActionStepsReasonTab').prop('onActive')()

    expect(props.changeTab.mock.calls.length).toBe(1)
    expect(props.changeTab.mock.calls[0][0]).toBe('actionStepsReasons')
  })
  it('actions change tab should dispatch', () => {
    const component = renderer.create(<WatchListContainer {...props} />)
    const dispatch = jest.fn()

    connect.mock.calls[0][1].changeTab('tab')(dispatch)

    expect(dispatch.mock.calls.length).toBe(1)
    expect(changeCurrentTab.mock.calls.length).toBe(1)
    expect(changeCurrentTab.mock.calls[0][0]).toBe('tab')
  })
})
