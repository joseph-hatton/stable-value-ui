import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'
import {shallow} from 'enzyme'

import WatchListIcon from '../../../src/watchLists/watchListAlert/WatchListIcon'

jest.mock('react-redux', () => ({
  connect: () => (comp) => comp
}))

jest.mock('../../../src/watchLists/chipify', () => ({
  chipify: mockComponent('chipify')
}))

jest.mock('material-ui/Popover', () => mockComponent('Popover'))
jest.mock('material-ui/svg-icons/alert/warning', () => mockComponent('WarningIcon'))

describe('WatchListIcon', () => {
  let props
  beforeEach(() => {
    props = {
      setContractWatchlistItem: jest.fn(),
      all: [{}],
      currentContractWatchListItem: {
        shortPlanName: 'shortPlanName',
        reasons: [],
        actionSteps: [],
        comments: 'comments',
        notInUse: false
      },
      contractId: 123,
      loadAll: jest.fn()
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<WatchListIcon {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should call setContractWatchlistItem', () => {
    const component = renderer.create(<WatchListIcon {...props} />)
    expect(props.setContractWatchlistItem.mock.calls.length).toBe(1)
    expect(props.setContractWatchlistItem.mock.calls[0][0]).toBe(123)
  })
  it('should render correctly with multiple reasons and action steps', () => {
    props.currentContractWatchListItem.reasons = [{}, {}]
    props.currentContractWatchListItem.actionSteps = [{}, {}]
    const component = renderer.create(<WatchListIcon {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should render correctly when all is set to 0', () => {
    props.all = []
    const component = renderer.create(<WatchListIcon {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should render correctly when not in use is true', () => {
    props.currentContractWatchListItem.notInUse = true
    const component = renderer.create(<WatchListIcon {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should set the state to be initially open to false', () => {
    props.currentContractWatchListItem.notInUse = true
    const component = shallow(<WatchListIcon {...props} />)
    expect(component.state().open).toBe(false)
  })
  it('should open on click of icon', () => {
    const component = shallow(<WatchListIcon {...props} />)
    const e = {preventDefault: jest.fn(), currentTarget: 'currentTarget'}

    component.children('#watchList-Icon').simulate('click', e)

    expect(component.state().open).toBe(true)
  })
  it('should close request close', () => {
    const component = shallow(<WatchListIcon {...props} />)
    const e = {preventDefault: jest.fn(), currentTarget: 'currentTarget'}

    component.children('#watchList-Icon').simulate('click', e)

    expect(component.state().open).toBe(true)

    component.children('#WatchListIcon-Popover').prop('onRequestClose')()

    expect(component.state().open).toBe(false)
  })
})
