import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'
import {shallow} from 'enzyme'

import WatchListAlert from '../../../src/watchLists/watchListAlert/WatchListAlert'

jest.mock('react-redux', () => ({
  connect: () => (comp) => comp
}))

jest.mock('../../../src/components/redux-dialog/redux-dialog', () => mockComponent('ReduxDialog'))
jest.mock('../../../src/watchLists/chipify', () => ({
  chipify: mockComponent('chipify')
}))

describe('Watch List Alert', () => {
  let props
  beforeEach(() => {
    props = {
      all: [],
      loadAll: jest.fn(),
      closeDialog: jest.fn(),
      currentContractWatchListItem: {
        shortPlanName: 'shortPlanName',
        reasons: [],
        actionSteps: [],
        comments: 'comments'
      }
    }
  })
  it('should render correctly', () => {
    const component = renderer.create(<WatchListAlert {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should render correctly when multiple reasons and action steps', () => {
    props.currentContractWatchListItem.reasons = [{}, {}]
    props.currentContractWatchListItem.actionSteps = [{}, {}]
    const component = renderer.create(<WatchListAlert {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should call load all if all\'s length is 0', () => {
    const component = renderer.create(<WatchListAlert {...props} />)
    expect(props.loadAll.mock.calls.length).toBe(1)
  })
  it('should not call load all if all\'s length is not 0', () => {
    props.all = [{}]
    const component = renderer.create(<WatchListAlert {...props} />)
    expect(props.loadAll.mock.calls.length).toBe(0)
  })
  it('should close dialog on cancel', () => {
    const component = shallow(<WatchListAlert {...props} />)
    expect(props.closeDialog.mock.calls.length).toBe(0)
    component.prop('actions')[0].props['onClick']()
    expect(props.closeDialog.mock.calls.length).toBe(1)
  })
})
