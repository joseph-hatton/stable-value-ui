import {loadAll} from '../../../src/watchLists/watchLists/WatchListActions'
import {openDialog} from '../../../src/components/redux-dialog'

import {
  setContractWatchlistItem,
  checkWatchList
} from '../../../src/watchLists/watchListAlert/WatchListAlertActions'

jest.mock('../../../src/watchLists/watchLists/WatchListActions', () => ({
  loadAll: jest.fn()
}))

jest.mock('../../../src/components/redux-dialog', () => ({
  openDialog: jest.fn()
}))

describe('Watch List Alert Actions', () => {
  let dispatch, getState, state
  beforeEach(() => {
    dispatch = jest.fn().mockReturnValue(Promise.resolve())
    state = {watchLists: {all: []}}
    getState = jest.fn().mockReturnValue(state)
  })
  describe('setContractWatchlistItem', () => {
    it('should dispatch loadAll', async () => {
      await setContractWatchlistItem()(dispatch, getState)

      expect(dispatch.mock.calls.length).toBe(2)
      expect(loadAll.mock.calls.length).toBe(1)
    })
    it('should dispatch setting current WL item', async () => {
      state.watchLists.all = [{contractId: 123}]

      await setContractWatchlistItem(123)(dispatch, getState)

      expect(dispatch.mock.calls[1][0].type).toBe('SET_CURRENT_CONTRACT_WATCH_LIST_ITEM')
      expect(dispatch.mock.calls[1][0].payload).toEqual({contractId: 123})
    })
    it('should call cb if defined', async () => {
      state.watchLists.all = [{contractId: 123}]
      const cb = jest.fn()

      await setContractWatchlistItem(123, cb)(dispatch, getState)

      expect(cb.mock.calls.length).toBe(1)
    })
    it('should dispatch setting current WL item to default', async () => {
      state.watchLists.all = [{contractId: 321}]

      await setContractWatchlistItem(123)(dispatch, getState)

      expect(dispatch.mock.calls[1][0].type).toBe('SET_CURRENT_CONTRACT_WATCH_LIST_ITEM')
      expect(dispatch.mock.calls[1][0].payload).toEqual({reasons: [], actionSteps: [], notInUse: true})
    })
  })
  it('checkWatchList should dispatch openDialog', async () => {
    state.watchLists.all = [{contractId: 123}]

    await checkWatchList(123)(dispatch, getState)

    expect(dispatch.mock.calls.length).toBe(3)
    expect(openDialog.mock.calls.length).toBe(1)
  })
})
