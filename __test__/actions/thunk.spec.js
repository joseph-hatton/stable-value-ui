import thunk from '../../src/actions/thunk'

const dispatch = jest.fn()
const getState = jest.fn()
const func = jest.fn().mockReturnValue('action called')

describe('thunk Specs', () => {
  beforeEach(() => {
    dispatch.mockClear()
    getState.mockClear()
  })

  it('captures id and args', () => {
    const action = thunk('some id', func, 'arg1', 'arg2')

    expect(action.id).toEqual('some id')
    expect(action.args).toEqual(['arg1', 'arg2'])
  })

  it('calls action', () => {
    const action = thunk('some id', func, 'arg1', 'arg2')

    const result = action(dispatch, getState)

    expect(result).toEqual('action called')
  })
})
