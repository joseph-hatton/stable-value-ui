import getCurrentUser, { initializeDispatch } from '../../src/actions/getCurrentUser'
import {get} from 'superagent'

let dispatch

jest.mock('superagent', () => ({ get: jest.fn() }))

beforeEach(() => {
  get.mockClear()
  dispatch = jest.fn()
})

describe('getCurrentUser Specs', () => {
  describe('get returns user', () => {
    beforeEach(() => {
      dispatch = jest.fn()
      get.mockReturnValue({
        redirects: jest.fn()
          .mockReturnValue(Promise.resolve({body: {name: 'a user', session: {expires: new Date().getTime()}}}))
      })
    })

    it('gets and dispatches user', async () => {
      const user = await getCurrentUser(dispatch)

      expect(dispatch).toHaveBeenCalledTimes(1)
      expect(dispatch).toHaveBeenCalledWith({type: 'SET_CURRENT_USER', user})
    })

    it('user is not expired', async () => {
      initializeDispatch(dispatch)
      const result1 = await getCurrentUser(dispatch)

      expect(dispatch).toHaveBeenCalledTimes(1)
    })
  })

  it('throws error', async () => {
    get.mockReturnValue({
      redirects: jest.fn()
        .mockReturnValue(Promise.reject(new Error('async error')))
    })
    initializeDispatch(dispatch)
    try {
      await getCurrentUser(dispatch)
    } catch (err) {
      expect(err.message).toBe('async error')
    }
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch).toHaveBeenCalledWith({type: 'TIME_OUT_USER'})
  })
})
