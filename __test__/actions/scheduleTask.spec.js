import scheduleTask from '../../src/actions/scheduleTask'

jest.useFakeTimers()
const task = jest.fn()

describe('scheduleTask Specs', () => {
  it('sets timeout', () => {
    scheduleTask(new Date().getTime() + 5000, task)

    expect(setTimeout).toHaveBeenCalledTimes(1)
    expect(setTimeout.mock.calls[0][0]).toEqual(task)
    expect(setTimeout.mock.calls[0][1]).toBeGreaterThanOrEqual(4500)
    expect(setTimeout.mock.calls[0][1]).toBeLessThanOrEqual(5500)
  })
})
