import loadReferenceData from '../../../src/actions/startup/loadReferenceData'

import {get} from '../../../src/actions/http'

const dispatch = jest.fn()

jest.mock('../../../src/actions/http', () => ({
  get: jest.fn()
}))

describe('loadReferenceData Specs', () => {
  beforeEach(() => {
    dispatch.mockClear()

    get.mockReturnValueOnce({firstCall: 'firstCallResults'})
      .mockReturnValueOnce({results: 'secondCallResults'})
  })

  it('calls dispatch with type', () => {
    loadReferenceData(dispatch)

    expect(dispatch).toHaveBeenCalled()
    expect(dispatch.mock.calls[0][0].type).toBe('LOAD_REFERENCE_DATA')
  })

  it('calls dispatch with payload', async () => {
    loadReferenceData(dispatch)

    expect(dispatch).toHaveBeenCalled()
    const result = await dispatch.mock.calls[0][0].payload

    expect(result).toEqual({derivativeTypes: 'secondCallResults', firstCall: 'firstCallResults'})
  })
})
