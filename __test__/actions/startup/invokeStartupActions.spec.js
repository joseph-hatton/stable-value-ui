import invokeStartupActions from '../../../src/actions/startup/invokeStartupActions'
import {updateCurrentUser} from '../../../src/actions/getCurrentUser'
import loadReferenceData from '../../../src/actions/startup/loadReferenceData'
import isCycleRunning from '../../../src/actions/startup/isCycleRunning'

jest.mock('../../../src/actions/getCurrentUser', () => ({ updateCurrentUser: jest.fn() }))
jest.mock('../../../src/actions/startup/loadReferenceData', () => jest.fn())
jest.mock('../../../src/actions/startup/isCycleRunning', () => jest.fn())

const {dispatch = 'DISPATCH', getState = 'GETSTATE'} = {}

describe('invokeStartupActions Specs', () => {
  it('calls actions', async () => {
    await invokeStartupActions(dispatch, getState)

    expect(updateCurrentUser).toHaveBeenCalledWith(dispatch, getState)
    expect(loadReferenceData).toHaveBeenCalledWith(dispatch, getState)
    expect(isCycleRunning).toHaveBeenCalledWith(dispatch, getState)
  })
})
