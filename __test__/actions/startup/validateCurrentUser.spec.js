import validateCurrentUser, {cancel} from '../../../src/actions/startup/validateCurrentUser'

jest.useFakeTimers()

let dispatch, getState

describe('validateCurrentUser Specs', () => {
  beforeEach(() => {
    setInterval.mockClear()
    dispatch = jest.fn()
    getState = jest.fn()
  })

  it('clears interval', () => {
    validateCurrentUser(dispatch, getState)

    expect(setInterval).toHaveBeenCalledTimes(1)

    cancel()

    expect(clearInterval).toHaveBeenCalledTimes(1)
  })

  it('calls set interval with default', () => {
    validateCurrentUser(dispatch, getState)

    expect(setInterval).toHaveBeenCalledTimes(1)
    expect(setInterval).toHaveBeenLastCalledWith(expect.any(Function), 5000)
  })

  it('calls set interval with override', () => {
    validateCurrentUser(dispatch, getState, 6000)

    expect(setInterval).toHaveBeenCalledTimes(1)
    expect(setInterval).toHaveBeenLastCalledWith(expect.any(Function), 6000)
  })

  const mockState = (timedOut, expires) => {
    getState.mockReturnValue({currentUser: {session: {timedOut, expires}}})
    validateCurrentUser(dispatch, getState)

    expect(setInterval).toHaveBeenCalledTimes(1)
    setInterval.mock.calls[0][0]()
  }

  it('get state called', () => {
    mockState(false, new Date().getTime() + 5000)

    expect(getState).toHaveBeenCalled()
  })

  it('not timed out and not expired', () => {
    mockState(false, new Date().getTime() + 5000)

    expect(dispatch).not.toHaveBeenCalled()
  })

  it('timed and not expired', () => {
    mockState(true, new Date().getTime() - 5000)

    expect(dispatch).not.toHaveBeenCalled()
  })

  it('not timed and expired', () => {
    mockState(false, new Date().getTime() - 5000)
    validateCurrentUser(dispatch, getState)

    expect(dispatch).toHaveBeenCalled()
    expect(dispatch).toHaveBeenLastCalledWith({type: 'TIME_OUT_USER'})
  })
})
