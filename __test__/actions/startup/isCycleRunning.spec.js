import isCycleRunning from '../../../src/actions/startup/isCycleRunning'

import {getNoSpin} from '../../../src/actions/http'
import scheduleTask from '../../../src/actions/scheduleTask'
import {loadDailyCycleMessages, loadMonthlyCycleMessages} from '../../../src/cycles/CyclesActions'
import {dispatch as storeDispatch} from '../../../src/store/store'
import {openSnackbar} from '../../../src/components/redux-dialog'

jest.mock('../../../src/store/store', () => ({ dispatch: jest.fn() }))
jest.mock('../../../src/actions/http', () => ({
  getNoSpin: jest.fn()
}))
jest.mock('../../../src/actions/scheduleTask', () => jest.fn())
jest.mock('../../../src/cycles/CyclesActions', () => ({
  loadDailyCycleMessages: jest.fn(),
  loadMonthlyCycleMessages: jest.fn()
}))
jest.mock('../../../src/components/redux-dialog', () => ({openSnackbar: jest.fn()}))

let dispatch, getState

describe('isCycleRunning Specs', () => {
  beforeEach(() => {
    loadDailyCycleMessages.mockClear()
    loadMonthlyCycleMessages.mockClear()
    getNoSpin.mockClear()
    storeDispatch.mockClear()
    scheduleTask.mockClear()
    openSnackbar.mockClear()
    openSnackbar.mockReturnValue('openSnackbarCalled')
    dispatch = jest.fn()
  })

  describe('not running', () => {
    beforeEach(async () => {
      getState = jest.fn().mockReturnValue({cycles: {isRunning: false}})
      getNoSpin.mockReturnValue(Promise.resolve({isRunning: false}))

      await isCycleRunning(dispatch, getState)
    })

    it('dispatches isRunning', () => {
      expect(dispatch).toHaveBeenCalledTimes(1)
      expect(dispatch).toHaveBeenLastCalledWith({type: 'IS_CYCLE_RUNNING', isRunning: false})
    })

    it('does not dispatch load daily cycle messages', () => {
      expect(loadDailyCycleMessages).not.toHaveBeenCalled()
    })

    it('does not dispatch load monthly cycle messages', () => {
      expect(loadMonthlyCycleMessages).not.toHaveBeenCalled()
    })

    it('does not call store.dispatch', () => {
      expect(storeDispatch).not.toHaveBeenCalled()
    })

    it('schedules task for roughly 10 seconds later', () => {
      expect(scheduleTask).toHaveBeenCalledTimes(1)
      expect(scheduleTask.mock.calls[0][0] - new Date()).toBeGreaterThanOrEqual(9500)
      expect(scheduleTask.mock.calls[0][0] - new Date()).toBeLessThanOrEqual(10500)
    })
  })

  describe('now running', () => {
    beforeEach(async () => {
      getState = jest.fn().mockReturnValue({cycles: {isRunning: false}})
      getNoSpin.mockReturnValue(Promise.resolve({isRunning: true}))
      loadDailyCycleMessages.mockReturnValue('loadDailyCalled')
      loadMonthlyCycleMessages.mockReturnValue('loadMonthlyCalled')
      await isCycleRunning(dispatch, getState)
    })

    it('dispatches isRunning', () => {
      expect(dispatch).toHaveBeenCalledTimes(1)
      expect(dispatch).toHaveBeenLastCalledWith({type: 'IS_CYCLE_RUNNING', isRunning: true})
    })

    it('dispatches load daily cycle messages', () => {
      expect(loadDailyCycleMessages).toHaveBeenCalled()
    })

    it('dispatches load monthly cycle messages', () => {
      expect(loadMonthlyCycleMessages).toHaveBeenCalled()
    })

    it('calls store.dispatch', () => {
      expect(storeDispatch).toHaveBeenCalledTimes(2)
      expect(storeDispatch.mock.calls[0]).toEqual(['loadDailyCalled'])
      expect(storeDispatch.mock.calls[1]).toEqual(['loadMonthlyCalled'])
    })

    it('schedules task for roughly 10 seconds later', () => {
      expect(scheduleTask).toHaveBeenCalledTimes(1)
      expect(scheduleTask.mock.calls[0][0] - new Date()).toBeGreaterThanOrEqual(9500)
      expect(scheduleTask.mock.calls[0][0] - new Date()).toBeLessThanOrEqual(10500)
    })
  })

  describe('was running', () => {
    beforeEach(async () => {
      getState = jest.fn().mockReturnValue({cycles: {isRunning: true}})
      getNoSpin.mockReturnValue(Promise.resolve({isRunning: false}))
      loadDailyCycleMessages.mockReturnValue('loadDailyCalled')
      loadMonthlyCycleMessages.mockReturnValue('loadMonthlyCalled')
      await isCycleRunning(dispatch, getState)
    })

    it('dispatches isRunning', () => {
      expect(dispatch).toHaveBeenCalledTimes(1)
      expect(dispatch).toHaveBeenLastCalledWith({type: 'IS_CYCLE_RUNNING', isRunning: false})
    })

    it('dispatches load daily cycle messages', () => {
      expect(loadDailyCycleMessages).toHaveBeenCalled()
    })

    it('dispatches load monthly cycle messages', () => {
      expect(loadMonthlyCycleMessages).toHaveBeenCalled()
    })

    it('calls store.dispatch', () => {
      expect(storeDispatch).toHaveBeenCalledTimes(3)
      expect(storeDispatch.mock.calls[0]).toEqual(['loadDailyCalled'])
      expect(storeDispatch.mock.calls[1]).toEqual(['loadMonthlyCalled'])
      expect(storeDispatch.mock.calls[2]).toEqual(['openSnackbarCalled'])
    })

    it('openSnackbar called', () => {
      expect(openSnackbar).toHaveBeenCalledTimes(1)
      expect(openSnackbar).toHaveBeenLastCalledWith({type: 'success', message: 'Cycle completed.', autoHideDuration: 60000})
    })

    it('schedules task for roughly 10 seconds later', () => {
      expect(scheduleTask).toHaveBeenCalledTimes(1)
      expect(scheduleTask.mock.calls[0][0] - new Date()).toBeGreaterThanOrEqual(9500)
      expect(scheduleTask.mock.calls[0][0] - new Date()).toBeLessThanOrEqual(10500)
    })
  })

  describe('error getting cycle', () => {
    beforeEach(async () => {
      getNoSpin.mockReturnValue(Promise.reject(new Error('Async error')))

      await isCycleRunning(dispatch, getState)
    })

    it('does not dispatch isRunning', () => {
      expect(dispatch).not.toHaveBeenCalled()
    })

    it('does not dispatch load daily cycle messages', () => {
      expect(loadDailyCycleMessages).not.toHaveBeenCalled()
    })

    it('does not dispatch load monthly cycle messages', () => {
      expect(loadMonthlyCycleMessages).not.toHaveBeenCalled()
    })

    it('does not call store.dispatch', () => {
      expect(storeDispatch).not.toHaveBeenCalled()
    })

    it('schedules task for roughly 10 seconds later', () => {
      expect(scheduleTask).toHaveBeenCalledTimes(1)
      expect(scheduleTask.mock.calls[0][0] - new Date()).toBeGreaterThanOrEqual(9500)
      expect(scheduleTask.mock.calls[0][0] - new Date()).toBeLessThanOrEqual(10500)
    })
  })
})
