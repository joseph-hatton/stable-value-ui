import React from 'react'
import mockComponent from '../MockComponent'
import renderLogoutView from '../../src/actions/renderLogoutView'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import { render } from 'react-dom'
import LogOutIFrame from '../../src/components/navigation/LogOutIFrame'

jest.mock('react-dom', () => {
  return {
    render: jest.fn()
  }
})
jest.mock('../../src/components/navigation/LogOutIFrame', () => mockComponent('LogOutIFrame'))

document.getElementById = jest.fn().mockReturnValue('main')

describe('renderLogoutView Specs', () => {
  it('renders correctly', () => {
    renderLogoutView({url: 'some url', height: 200, width: 300})

    expect(render).toHaveBeenCalled()
    expect(render.mock.calls[0][0].props).toEqual({height: 200, url: 'some url', width: 300})
  })

  it('renders to main element', () => {
    renderLogoutView({url: 'some url', height: 200, width: 300})

    expect(render).toHaveBeenCalled()
    expect(render.mock.calls[0][1]).toBe('main')
  })
})
