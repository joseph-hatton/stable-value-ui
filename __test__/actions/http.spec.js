import http from '../../src/actions/http'

import {dispatch} from '../../src/store/store'
import {get, put} from 'superagent'
import invokeHttpRequest from '../../src/actions/invokeHttpRequest'

jest.useFakeTimers()

jest.mock('superagent', () => ({get: jest.fn(), put: jest.fn()}))
jest.mock('../../src/actions/getCurrentUser', () => jest.fn().mockReturnValue(Promise.resolve({session: {token: 'ABC123'}, customHeaders: {header1: 'some custom headers'}})))
jest.mock('../../src/store/store', () => ({ dispatch: jest.fn() }))
jest.mock('../../src/components/redux-dialog', () => ({
  spin: jest.fn().mockReturnValue('spinStart'),
  stop: jest.fn().mockReturnValue('spinStop'),
  openSnackbar: jest.fn().mockReturnValue('openedSnackbar'),
  closeDialog: jest.fn()
}))
jest.mock('../../src/actions/invokeHttpRequest', () => jest.fn())

let mockRequest

beforeEach(() => {
  setTimeout.mockClear()
  dispatch.mockClear()
  invokeHttpRequest.mockClear()
  get.mockClear()
  put.mockClear()
  mockRequest = {
    accept: jest.fn(),
    query: jest.fn(),
    set: jest.fn(),
    type: jest.fn(),
    send: jest.fn()
  }
  mockRequest.accept.mockReturnValue(mockRequest)
  mockRequest.query.mockReturnValue(mockRequest)
  mockRequest.set.mockReturnValue(mockRequest)
  mockRequest.type.mockReturnValue(mockRequest)
  mockRequest.send.mockReturnValue(mockRequest)
})

describe('http Specs', () => {
  describe('httpErrorMessage', () => {
    it('structure exists', () => {
      const result = http.httpErrorMessage({response: {body: {message: 'some error'}}})
      expect(result).toBe('some error')
    })

    it('structure does not exist', () => {
      const result = http.httpErrorMessage({response: {message: 'bad structure'}})
      expect(result).toBeUndefined()
    })
  })

  describe('get', () => {
    let result
    describe('valid response', () => {
      beforeEach(async () => {
        get.mockReturnValue(mockRequest)
        invokeHttpRequest.mockReturnValue({body: 'expected body response'})

        result = await http.get('some url', {queryA: 'hi'})
      })
      it('request called with', () => {
        expect(get).toHaveBeenLastCalledWith('some url')
        expect(mockRequest.accept).toHaveBeenLastCalledWith('json')
        expect(mockRequest.query).toHaveBeenLastCalledWith({queryA: 'hi'})
      })
      it('spinner shown', () => {
        expect(dispatch).toHaveBeenCalledTimes(1)
        expect(dispatch).toHaveBeenLastCalledWith('spinStart')
      })
      it('expected result', () => {
        expect(dispatch).toHaveBeenCalledTimes(1)
        expect(result).toBe('expected body response')
      })
      it('spinner stopped', () => {
        expect(setTimeout).toHaveBeenCalledTimes(1)
        expect(setTimeout.mock.calls[0][1]).toBe(500)
        setTimeout.mock.calls[0][0]()
        expect(dispatch).toHaveBeenLastCalledWith('spinStop')
      })
    })
    // it('errors', async () => {
    //   invokeHttpRequest.mockClear()
    //   invokeHttpRequest.mockReturnValue(Promise.reject(new Error('async error')))

    //   result = await http.get('some url', {queryA: 'hi'})

    //   expect(setTimeout).toHaveBeenCalledTimes(1)
    //   expect(setTimeout.mock.calls[0][1]).toBe(500)
    //   setTimeout.mock.calls[0][0]()
    //   expect(dispatch).toHaveBeenLastCalledWith('spinStop')
    // })
  })

  describe('getNoSpin', () => {
    let result
    describe('valid response', () => {
      beforeEach(async () => {
        get.mockReturnValue(mockRequest)
        invokeHttpRequest.mockReturnValue({body: 'expected body response'})

        result = await http.getNoSpin('some url')
      })
      it('request called with', () => {
        expect(get).toHaveBeenLastCalledWith('some url')
      })
      it('spinner not shown', () => {
        expect(dispatch).toHaveBeenCalledTimes(0)
      })
      it('expected result', () => {
        expect(result).toBe('expected body response')
      })
    })
  })

  describe('put', () => {
    let result
    describe('valid response', () => {
      beforeEach(async () => {
        put.mockReturnValue(mockRequest)
        invokeHttpRequest.mockReturnValue({body: 'expected body response'})

        result = await http.put('some url', {some: 'body'}, 'msgconfig', 'dialog')
      })
      it('request called with', () => {
        expect(put).toHaveBeenLastCalledWith('some url')
      })
      it('spinner shown', () => {
        expect(dispatch).toHaveBeenCalledTimes(2)
        expect(dispatch.mock.calls[0][0]).toBe('spinStart')
      })
      it('expected result', () => {
        expect(dispatch).toHaveBeenCalledTimes(2)
        expect(result).toBe('expected body response')
      })
      it('spinner stopped', () => {
        expect(setTimeout).toHaveBeenCalledTimes(1)
        expect(setTimeout.mock.calls[0][1]).toBe(500)
        setTimeout.mock.calls[0][0]()
        expect(dispatch).toHaveBeenLastCalledWith('spinStop')
      })
    })
  })
})
