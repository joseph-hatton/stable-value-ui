import invokeHttpRequest from '../../src/actions/invokeHttpRequest'

jest.mock('../../src/actions/getCurrentUser', () => jest.fn().mockReturnValue(Promise.resolve({session: {token: 'ABC123'}, customHeaders: {header1: 'some custom headers'}})))

let request

beforeEach(() => {
  request = {set: jest.fn()}
})

describe('invokeHttpRequest Specs', () => {
  it('sets authorization and custom headers', async () => {
    const result = await invokeHttpRequest(request)

    expect(request.set).toHaveBeenCalledTimes(1)
    expect(request.set).toHaveBeenCalledWith({Authorization: 'Bearer ABC123', header1: 'some custom headers'})
  })
})
