import logOut from '../../src/actions/logOut'
import thunk from '../../src/actions/thunk'
import renderLogoutView from '../../src/actions/renderLogoutView'
import agent from 'superagent'

jest.mock('superagent', () => ({ del: jest.fn() }))

jest.mock('../../src/actions/renderLogoutView', () => jest.fn())

let dispatch, getState

beforeEach(() => {
  agent.del.mockClear()
  agent.del.mockReturnValue(Promise.resolve({body: {logoutUrl: 'some logout url'}}))
  renderLogoutView.mockClear()
  dispatch = jest.fn()
  getState = jest.fn().mockReturnValue({window: {height: 200, width: 300}})
})

describe('logOut Specs', () => {
  it('renders logout view', async () => {
    await logOut(dispatch, getState)

    expect(agent.del).toHaveBeenCalledTimes(1)
    expect(renderLogoutView).toHaveBeenCalledTimes(1)
    expect(renderLogoutView.mock.calls[0][0].url).toBe('some logout url')
  })
})
