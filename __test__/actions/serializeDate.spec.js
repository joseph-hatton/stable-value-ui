import {serializeDates} from '../../src/actions/serializeDate'

const payload = {
  'transactionId': 4123,
  'contractId': 539,
  'transactionType': 'FEE_RECEIPT',
  'effectiveDate': new Date('2015-07-29T07:53:19.455Z'),
  'detail': null,
  'amount': 17492.73,
  'comments': null,
  'locked': true,
  'createdDate': new Date('2015-07-29T07:53:19.455Z'),
  'createdId': 'crogers',
  'modifiedDate': new Date('2015-08-03T14:54:58.656Z'),
  'modifiedId': null,
  'version': 1,
  'adjustmentDate': null,
  'adjustmentTransactionId': null,
  'contractNumber': 'AGLNT-0712-01',
  'shortPlanName': 'Agilent',
  'balanceId': 1354631,
  'beginningBookValue': 33196295.167494506
}

describe('Serialize Dates', () => {
  it('serializes dates of all props', () => {
    const result = serializeDates(payload)

    expect(result).toEqual({
      ...payload,
      'createdDate': '2015-07-29T00:00:00Z',
      'effectiveDate': '2015-07-29T00:00:00Z',
      'modifiedDate': '2015-08-03T00:00:00Z'
    })
  })

  it('serializes dates of all props but the ones ignored', () => {
    const result = serializeDates(payload, field => field === 'modifiedDate')

    expect(result).toEqual({
      ...payload,
      'createdDate': '2015-07-29T00:00:00Z',
      'effectiveDate': '2015-07-29T00:00:00Z'
    })
  })

  it('serializes dates of props and deep objects, but the ones ignored', () => {
    const SKIP_DATE_SERIALIZATION = ['createdDate', 'modifiedDate']

    const result = serializeDates(contract, field => SKIP_DATE_SERIALIZATION.includes(field))

    expect(result).toEqual({
      'receipt': {
        'transactionId': 1666,
        'contractId': 530,
        'transactionType': 'FEE_RECEIPT',
        'effectiveDate': '2014-01-14T00:00:00Z',
        'amount': 22073.95,
        'createdDate': new Date('2014-04-21T10:43:06.000Z'),
        'createdId': 'system',
        'modifiedDate': new Date('2014-04-21T10:43:06.000Z')
      },
      'contractId': 530,
      'contractNumber': 'ADMCO-1112-01',
      'contractType': 'SINGLE_PLAN',
      'effectiveDate': '2012-11-15T00:00:00Z',
      'statedMaturityDate': '2999-01-01T00:00:00Z',
      'editHistories': [
        {
          'contractEditHistoryId': 2916,
          'effectiveDate': '2017-11-01T00:00:00Z',
          'createdDate': new Date('2017-11-02T08:03:53.426Z'),
          'createdId': 's0042526',
          'modifiedDate': new Date('2018-01-18T22:22:53.569Z'),
          'modifiedId': 's0045233',
          'transactions': [
            {
              'transactionId': 4123,
              'contractId': 539,
              'transactionType': 'FEE_RECEIPT',
              'effectiveDate': '2015-07-29T00:00:00Z',
              'amount': 17492.73,
              'createdDate': new Date('2015-07-29T07:53:19.455Z'),
              'createdId': 'crogers',
              'modifiedDate': new Date('2015-08-03T14:54:58.656Z'),
              'modifiedId': null
            }
          ]
        },
        {
          'contractEditHistoryId': 2691,
          'version': 1,
          'effectiveDate': '2017-07-25T00:00:00Z',
          'createdDate': new Date('2017-07-25T00:00:00Z'),
          'createdId': 's0044888',
          'modifiedDate': new Date('2018-01-18T22:22:53.650Z'),
          'modifiedId': 's0045233',
          'jurisdiction': null,
          'accountDurationLimit': 3.5
        }
      ]
    })
  })
})

const contract = {
  'receipt': {
    'transactionId': 1666,
    'contractId': 530,
    'transactionType': 'FEE_RECEIPT',
    'effectiveDate': new Date('2014-01-14T10:43:06.000Z'),
    'amount': 22073.95,
    'createdDate': new Date('2014-04-21T10:43:06.000Z'),
    'createdId': 'system',
    'modifiedDate': new Date('2014-04-21T10:43:06.000Z')
  },
  'contractId': 530,
  'contractNumber': 'ADMCO-1112-01',
  'contractType': 'SINGLE_PLAN',
  'effectiveDate': new Date('2012-11-15T22:22:53.569Z'),
  'statedMaturityDate': new Date('2999-01-01T22:22:53.569Z'),
  'editHistories': [{
    'contractEditHistoryId': 2916,
    'effectiveDate': new Date('2017-11-01T22:22:53.569Z'),
    'createdDate': new Date('2017-11-02T08:03:53.426Z'),
    'createdId': 's0042526',
    'modifiedDate': new Date('2018-01-18T22:22:53.569Z'),
    'modifiedId': 's0045233',
    'transactions': [
      {
        'transactionId': 4123,
        'contractId': 539,
        'transactionType': 'FEE_RECEIPT',
        'effectiveDate': new Date('2015-07-29T07:53:19.455Z'),
        'amount': 17492.73,
        'createdDate': new Date('2015-07-29T07:53:19.455Z'),
        'createdId': 'crogers',
        'modifiedDate': new Date('2015-08-03T14:54:58.656Z'),
        'modifiedId': null
      }
    ]
  }, {
    'contractEditHistoryId': 2691,
    'version': 1,
    'effectiveDate': new Date('2017-07-25T22:22:53.569Z'),
    'createdDate': new Date('2017-07-25T00:00:00Z'),
    'createdId': 's0044888',
    'modifiedDate': new Date('2018-01-18T22:22:53.650Z'),
    'modifiedId': 's0045233',
    'jurisdiction': null,
    'accountDurationLimit': 3.5
  }]
}
