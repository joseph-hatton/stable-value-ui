import React from 'react'
import mockComponent from '../MockComponent'
import mockConnect from '../MockConnect'
import mockReduxFrom from '../MockReduxForm'
import renderer from 'react-test-renderer'
import {connect, connectChildFn} from 'react-redux'
import {reduxForm, reduxFormChildFn, reset, resetAction} from 'redux-form'
import {RaisedButton} from 'material-ui'
import {Card} from 'material-ui/Card'
import createRefDataFormFieldComponent from '../../src/components/forms/createRefDataFormFieldComponent'

import {MarketValueFilters, actions} from '../../src/marketValue/MarketValueFilters'

jest.mock('react-redux', () => mockConnect())
jest.mock('redux-form', () => mockReduxFrom())

jest.mock('material-ui', () => ({
  RaisedButton: mockComponent('RaisedButton')
}))

jest.mock('material-ui/Card', () => ({Card: mockComponent('Card')}))

jest.mock('../../src/contacts/ManagersFormField', () => mockComponent('ManagersFormField'))
jest.mock('../../src/contracts/ContractPicker', () => mockComponent('ContractPicker'))
jest.mock('../../src/components/common/DateRangFilter', () => mockComponent('DateRangFilter'))
jest.mock('../../src/components/forms/createRefDataFormFieldComponent', () => () => mockComponent('CreditingRateStatus'))

describe('MarketValue Filters', () => {
  it('Renders MarketValues Filter Form', () => {
    const clearAll = 'clearAllFn'
    const component = renderer.create(<MarketValueFilters clearAll={clearAll}/>)

    expect(component.toJSON()).toMatchSnapshot()
    expect(connect).toHaveBeenCalledWith(null, actions)
    expect(reduxForm).toHaveBeenCalledWith({form: 'MarketValueFiltersForm'})
    expect(reduxFormChildFn).toHaveBeenCalledWith(MarketValueFilters)
    expect(connectChildFn).toHaveBeenCalledWith(MarketValueFilters)
  })

  it('clears the form using reset', () => {
    const disptach = jest.fn()

    actions.clearAll()(disptach)

    expect(reset).toHaveBeenCalledWith('MarketValueFiltersForm')
    expect(disptach).toHaveBeenCalledWith(resetAction)
  })
})
