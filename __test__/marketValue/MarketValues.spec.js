import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from '../MockComponent'
import mockConnect from '../MockConnect'
import {connect, connectChildFn} from 'react-redux'

import ConnectedMarketValues, {
  MarketValues,
  mapStateToProps,
  actions
} from '../../src/marketValue/MarketValues'

jest.mock('react-redux', () => mockConnect())

jest.mock('../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn(),
  GridContextMenu: mockComponent('GridContextMenu')
}))

jest.mock('react-intl', () => ({
  IntlProvider: mockComponent('IntlProvider')
}))
jest.mock('../../src/components/common/Expandable', () => mockComponent('Expandable'))
jest.mock('../../src/contracts/crediting-rate/CreditingRatesMenu', () => mockComponent('CreditingRateMenu'))
jest.mock('../../src/contracts/crediting-rate/ViewEditCreditingRate', () => mockComponent('ViewEditCreditingRates'))
jest.mock('../../src/contracts/crediting-rate/AddEditMarketValue', () => mockComponent('AddEditMarketValue'))
jest.mock('../../src/contracts/crediting-rate/CreditingRatesActions', () => ({
  setMarketValuesView: jest.fn()
}))
jest.mock('../../src/marketValue/MarketValueActions', () => ({
  marketValuesSelected: jest.fn()
}))
jest.mock('../../src/marketValue/MarketValueFilters', () => mockComponent('MarketValueFilters'))
jest.mock('../../src/marketValue/MarketValuesColumns', () => jest.fn())
jest.mock('../../src/marketValue/MarketValueBulkActionsToolbar', () => mockComponent('MarketValueBulkActionsToolbar'))
jest.mock('../../src/marketValue/MarketValueBulkActionStatus', () => mockComponent('MarketValueBulkActionStatus'))
jest.mock('../../src/components/common/FABAddButton', () => mockComponent('FABAddButton'))

let mockCanAddCreditingRate = true
jest.mock('../../src/contracts/crediting-rate/CreditingRatesRules', () => ({
  hasCreditingRateEntitlement: () => mockCanAddCreditingRate
}))

describe('Market Values Grid', () => {
  it('Renders Market Value Grid', () => {
    const filters = {status: 'APPROVED', fromEffectiveDate: null}
    const selectedCreditingRate = {}
    const setMarketValuesView = jest.fn()
    const marketValuesSelected = jest.fn()

    const component = renderer.create(<MarketValues filters={filters} selectedCreditingRate={selectedCreditingRate}
                                                    setMarketValuesView={setMarketValuesView}
                                                    marketValuesSelected={marketValuesSelected}
    />)

    expect(component.toJSON()).toMatchSnapshot()
    expect(connect).toHaveBeenCalledWith(mapStateToProps, actions)
    expect(connectChildFn).toHaveBeenCalledWith(MarketValues)
    expect(ConnectedMarketValues).toEqual(MarketValues)
    expect(setMarketValuesView).toHaveBeenCalled()
  })

  it('Renders Market Value Grid Without Market Value and Crediting Rate Dialogs ', () => {
    const filters = {status: 'PENDING', fromEffectiveDate: null}
    const selectedCreditingRate = null
    const setMarketValuesView = jest.fn()
    const marketValuesSelected = jest.fn()

    const component = renderer.create(<MarketValues filters={filters} selectedCreditingRate={selectedCreditingRate}
                                                    setMarketValuesView={setMarketValuesView}
                                                    marketValuesSelected={marketValuesSelected}
    />)

    expect(component.toJSON()).toMatchSnapshot()
    expect(connect).toHaveBeenCalledWith(mapStateToProps, actions)
    expect(connectChildFn).toHaveBeenCalledWith(MarketValues)
    expect(ConnectedMarketValues).toEqual(MarketValues)
    expect(setMarketValuesView).toHaveBeenCalled()
  })

  it('Renders Market Value Grid Without FAB to add Market Value', () => {
    const filters = {status: 'PENDING', fromEffectiveDate: null}
    const selectedCreditingRate = null
    const setMarketValuesView = jest.fn()
    mockCanAddCreditingRate = false
    const marketValuesSelected = jest.fn()
    const addEditMarketValue = jest.fn()

    const component = renderer.create(<MarketValues filters={filters} selectedCreditingRate={selectedCreditingRate}
                                                    setMarketValuesView={setMarketValuesView}
                                                    marketValuesSelected={marketValuesSelected}
                                                    addEditMarketValue={addEditMarketValue}
    />)

    expect(component.toJSON()).toMatchSnapshot()
    expect(connect).toHaveBeenCalledWith(mapStateToProps, actions)
    expect(connectChildFn).toHaveBeenCalledWith(MarketValues)
    expect(ConnectedMarketValues).toEqual(MarketValues)
    expect(setMarketValuesView).toHaveBeenCalled()

    component.getInstance()._addEditMarketValue()
    expect(addEditMarketValue).toHaveBeenCalledWith({}, false)

    component.getInstance().componentWillUnmount()
    expect(marketValuesSelected).toHaveBeenCalledWith([])

    expect(component.getInstance().getGridRef()).toBe(null)
  })

  it('Refreshes Market Values Grid when refresh prop is received as true', () => {
    const filters = {status: 'PENDING', fromEffectiveDate: null}
    const marketValuesRefreshed = jest.fn()
    const ref = {
      refresh: jest.fn()
    }

    const wrapper = shallow(<MarketValues filters={filters} marketValuesRefreshed={marketValuesRefreshed}/>)
    wrapper.instance().getGridRef = () => ref
    wrapper.update()

    wrapper.setProps({refreshMarketValues: true})

    expect(ref.refresh).toHaveBeenCalled()
    expect(marketValuesRefreshed).toHaveBeenCalled()
  })

  it('maps MarketValueFilters to props', () => {
    const state = {
      form: {
        MarketValueFiltersForm: {
          values: {status: 'APPROVED'}
        }
      },
      contracts: {
        selectedCreditingRate: {
          creditingRateId: 200,
          status: 'Approved'
        }
      }
    }

    const props = mapStateToProps(state)

    expect(props).toEqual({
      filters: {
        status: 'APPROVED'
      },
      selectedCreditingRate: {
        creditingRateId: 200,
        status: 'Approved'
      }
    })
  })

  it('maps MarketValueFilters to props gracefully', () => {
    const state = {
      form: {},
      contracts: {}
    }

    const props = mapStateToProps(state)

    expect(props).toEqual({filters: {}})
  })
})
