import reduce from '../../src/marketValue/MarketValueReducers'

const initState = {}

describe('Market Value Reducers', () => {
  it('sets selected market value to the state', () => {
    const selectedMarketValues = jest.fn()

    const newState = reduce(initState, {type: 'MARKET_VALUES_SELECTED', selectedMarketValues})

    expect(newState).toEqual({selectedMarketValues})
  })

  it('sets marketValuesBulkActionStatus for selected market values', () => {
    const marketValuesBulkActionStatus = jest.fn()
    const actionLabel = jest.fn()

    const newState = reduce(initState, {
      type: 'SET_MARKET_VALUE_BULK_ACTION_STATUS',
      payload: {
        marketValuesBulkActionStatus,
        actionLabel
      }
    })

    expect(newState).toEqual({
      marketValuesBulkActionStatus,
      actionLabel
    })
  })

  const marketValuesBulkActionStatus = {
    '555': {creditingRateId: 555, row: {contractId: 121, creditingRateId: 555, marketValue: 8798798798}},
    '500': {creditingRateId: 500, row: {contractId: 333, creditingRateId: 500, marketValue: 8664545454}}
  }

  it('updates the status of bulk action for a crediting rate to Executing', () => {
    const newState = reduce({marketValuesBulkActionStatus}, {
      type: 'MARKET_VALUE_BULK_ACTION_PENDING',
      payload: {
        creditingRateId: 500
      }
    })

    expect(newState.marketValuesBulkActionStatus[500].status).toEqual('Executing')
    expect(newState.marketValuesBulkActionStatus[555].status).toBeUndefined()
  })

  it('updates the status of bulk action for a crediting rate to Error', () => {
    const newState = reduce({marketValuesBulkActionStatus}, {
      type: 'MARKET_VALUE_BULK_ACTION_ERROR',
      payload: {
        creditingRateId: 500
      }
    })

    expect(newState.marketValuesBulkActionStatus[500].status).toEqual('Errored')
    expect(newState.marketValuesBulkActionStatus[555].status).toBeUndefined()
  })

  it('updates the status of bulk action for a crediting rate to Completed', () => {
    const newState = reduce({marketValuesBulkActionStatus}, {
      type: 'MARKET_VALUE_BULK_ACTION_SUCCESS',
      payload: {
        creditingRateId: 555
      }
    })

    expect(newState.marketValuesBulkActionStatus[555].status).toEqual('Completed')
    expect(newState.marketValuesBulkActionStatus[500].status).toBeUndefined()
  })
})
