import React from 'react'
import mockComponent from '../MockComponent'

import {CreditRatePercentFormat} from '../../src/contracts/crediting-rate/CreditingRatesColumns'
import {
  createReferenceDataTextRenderer,
  ReferenceDataTextRenderer
} from '../../src/components/renderers/ReferenceDataTextRenderer'
import ContractManagerText from '../../src/contacts/ContractManagerText'

import MarketValuesColumns, {percentFormatMKOne, OptionalPercentFormat, contractRoundingPercentFormat, currencyFormatMV} from '../../src/marketValue/MarketValuesColumns'
import {currencyFormat, percentFormat, dateFormat} from '../../src/components/grid'

import {CreditingRatesMenuLink} from '../../src/contracts/crediting-rate/CreditingRatesMenu'

jest.mock('../../src/components/grid', () => ({
  dateFormat: jest.fn(),
  currencyFormat: jest.fn(),
  percentFormat: jest.fn()
}))
jest.mock('../../src/contacts/ContractManagerText', () => mockComponent('ContractManagerText'))
jest.mock('../../src/contracts/crediting-rate/CreditingRatesColumns', () => ({
  CreditRatePercentFormat: jest.fn()
}))
jest.mock('../../src/contracts/crediting-rate/CreditingRatesMenu', () => ({
  CreditingRatesMenuLink: () => mockComponent('ContractManagerText')
}))

jest.mock('../../src/components/renderers/ReferenceDataTextRenderer', () => {
  const ReferenceDataTextRenderer = jest.fn()
  return {
    createReferenceDataTextRenderer: jest.fn(() => ReferenceDataTextRenderer),
    ReferenceDataTextRenderer
  }
})
const getColumnDef = (field) => MarketValuesColumns.find(column => column.field === field)

describe('Market Value Columns', () => {
  it('List all Crediting Rate Columns and Contract Columns ', () => {
    expect(MarketValuesColumns.map(column => column.field)).toEqual([
      'creditingRateId',
      'contractNumber',
      'shortPlanName',
      'managerId',
      'managerName',
      'referenceDate',
      'marketValue',
      'bookValue',
      'mvbvRatio',
      'annualEffectiveYield',
      'duration',
      'effectiveDate',
      'grossRate',
      'rate',
      'status'])
  })

  it('Uses CreditRatePercentFormat for annualEffectiveYield, rate, grossRate', () => {
    expect(getColumnDef('annualEffectiveYield').renderer).toEqual(OptionalPercentFormat)
   expect(getColumnDef('rate').renderer).toEqual(contractRoundingPercentFormat)
    expect(getColumnDef('grossRate').renderer).toEqual(contractRoundingPercentFormat)
  })

  it('Uses ReferenceDataTextRenderer to Render Status', () => {
    const columnDef = getColumnDef('status')

    expect(columnDef.renderer).toEqual(ReferenceDataTextRenderer)
  })

  it('displays duration with 2 digit decimal precision', () => {
    const columnDef = getColumnDef('duration')

    expect(columnDef.renderer(2.53455)).toEqual('2.53')
  })

 it('displays marketValue, bookValue in currency format', () => {
    expect(getColumnDef('marketValue').renderer).toEqual(currencyFormatMV)
    expect(getColumnDef('bookValue').renderer).toEqual(currencyFormatMV)
  })

  it('displays mvbvRatio in percent format', () => {
    expect(getColumnDef('mvbvRatio').renderer).toEqual(percentFormatMKOne)
  })

  it('displays referenceDate, effectiveDate in date format', () => {
    expect(getColumnDef('referenceDate').renderer).toEqual(dateFormat)
    expect(getColumnDef('effectiveDate').renderer).toEqual(dateFormat)
  })

  it('renders contract number column as a CreditingRatesMenuLink', () => {
    expect(getColumnDef('contractNumber').renderer('RGA007', {creditingRateId: 555})).toEqual(<CreditingRatesMenuLink
      creditingRate={{'creditingRateId': 555}}>RGA007</CreditingRatesMenuLink>)
  })
})
