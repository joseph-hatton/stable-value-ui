import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import { PreliminaryMonthlyRun, PreliminaryMonthlyRunForm } from '../../src/cycles/PreliminaryMonthlyRun'
import { openConfirmationAlert } from '../../src/components/redux-dialog/redux-dialog'

jest.mock('react-redux', () => {
  return {
    connect: (mapStateToProps, mapDispatchToProps) => (component) => <component/> // eslint-disable-line react/display-name
  }
})
jest.mock('redux-form', () => ({
  change: jest.fn(),
  reduxForm: () => (comp) => comp
}))
jest.mock('material-ui', () => ({ Card: mockComponent('Card'), RaisedButton: mockComponent('RaisedButton') }))
jest.mock('../../src/components/forms/YesNo', () => mockComponent('YesNo'))
jest.mock('../../src/components/redux-dialog', () => mockComponent('ReduxDialog'))
jest.mock('../../src/components/redux-dialog/redux-dialog', () => ({closeDialog: jest.fn()}))
jest.mock('../../src/cycles/CyclesActions', () => ({runPreliminaryMonthlyCycle: jest.fn()}))

const createProps = () => ({
  handleSubmit: jest.fn(),
  closeDialog: jest.fn(),
  runPreliminaryMonthlyCycle: jest.fn()
})

describe('PreliminaryMonthlyRun Specs', () => {
  it('renders correctly', () => {
    const props = createProps()
    const component = renderer.create(
      <PreliminaryMonthlyRun {...props} />
    )
    expect(component.toJSON()).toMatchSnapshot()
  })

  it('cancel button closes dialog', () => {
    const props = createProps()
    const component = shallow(
      <PreliminaryMonthlyRun {...props} />
    )
    component.prop('actions').find((i) => i.props.id === 'PreliminaryMonthlyRun-Cancel').props.onClick()
    expect(props.closeDialog).toHaveBeenCalled()
  })
})
