import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import { MonthlyCycles, actions, mapStateToProps } from '../../src/cycles/MonthlyCycles'
import { openConfirmationAlert } from '../../src/components/redux-dialog/redux-dialog'

jest.mock('react-redux', () => {
  return {
    connect: (mapStateToProps, mapDispatchToProps) => (component) => <component/> // eslint-disable-line react/display-name
  }
})
jest.mock('react-intl', () => ({ IntlProvider: mockComponent('IntlProvider') }))
jest.mock('material-ui/Card', () => ({ Card: mockComponent('Card') }))
jest.mock('material-ui/Toolbar', () => ({ Toolbar: mockComponent('Toolbar'), ToolbarGroup: mockComponent('ToolbarGroup') }))
jest.mock('material-ui/RaisedButton', () => mockComponent('RaisedButton'))
jest.mock('material-ui/svg-icons/action/cached', () => mockComponent('Cached'))
jest.mock('material-ui/svg-icons/av/play-arrow', () => mockComponent('PlayArrow'))
jest.mock('../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn().mockReturnValue({})
}))
jest.mock('../../src/cycles/PreliminaryMonthlyRun', () => mockComponent('PreliminaryMonthlyRun'))

const createProps = (runMonthly = true) => ({
  monthlyCycleMessages: jest.fn(),
  loadMonthlyCycleMessages: jest.fn(),
  isRunning: false,
  finalizeMonthlyCycle: jest.fn().mockReturnValue('finalizeMonthlyCycleFunction'),
  openDialog: jest.fn(),
  openConfirmationAlert: jest.fn(),
  canRunMonthlyCycle: runMonthly
})

describe('MonthlyCycles Specs', () => {
  it('renders correctly', () => {
    const props = createProps()
    const monthlyCycles = renderer.create(
      <MonthlyCycles {...props} />
    )
    expect(monthlyCycles.toJSON()).toMatchSnapshot()
    monthlyCycles.getInstance().componentDidMount()
    expect(props.loadMonthlyCycleMessages).toHaveBeenCalled()
  })

  it('refresh button loads daily cycle messages', () => {
    const props = createProps()
    const monthlyCycles = shallow(
      <MonthlyCycles {...props} />
    )
    monthlyCycles.find({label: 'Refresh'}).simulate('click')
    expect(props.loadMonthlyCycleMessages).toHaveBeenCalled()
  })

  it('run button opens preliminary monthly cycle prompt', () => {
    const props = createProps()
    const monthlyCycles = shallow(
      <MonthlyCycles {...props} />
    )
    monthlyCycles.find({label: 'Run'}).simulate('click')
    expect(props.openDialog).toHaveBeenCalledWith('runPreliminaryMonthlyCycle')
  })

  it('finalize button prompts for confirmation then fires finalize monthly cycle', () => {
    const props = createProps()
    const monthlyCycles = shallow(
      <MonthlyCycles {...props} />
    )
    monthlyCycles.find({label: 'Finalize Month'}).simulate('click')
    expect(props.openConfirmationAlert).toHaveBeenCalled()
    console.log(props.openConfirmationAlert.mock.calls)
    expect(props.openConfirmationAlert.mock.calls[0][0].message).toBe('Are you sure you want to finalize the month?')
    expect(props.openConfirmationAlert.mock.calls[0][0].onOk()).toBe('finalizeMonthlyCycleFunction')
  })

  it('mapStateToProps', () => {
    expect(mapStateToProps({cycles: {monthlyCycleMessages: 'monthlyCycleMessages', isRunning: true}, currentUser: {RUN_MONTHLY_CYCLE: true}}))
    .toEqual({monthlyCycleMessages: 'monthlyCycleMessages', isRunning: true, canRunMonthlyCycle: true})
  })
})
