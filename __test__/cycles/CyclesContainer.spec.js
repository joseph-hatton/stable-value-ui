import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import { CyclesContainer, mapStateToProps } from '../../src/cycles/CyclesContainer'

jest.mock('react-redux', () => {
  return {
    connect: (mapStateToProps, mapDispatchToProps) => (component) => <component/> // eslint-disable-line react/display-name
  }
})
jest.mock('material-ui/Tabs', () => {
  return {
    Tabs: mockComponent('Tabs'),
    Tab: mockComponent('Tab')
  }
})
jest.mock('material-ui/Card', () => ({ Card: mockComponent('Card') }))
jest.mock('../../src/cycles/CyclesTabs', () => {
  return [
    {
      label: 'labelB',
      CycleTab: mockComponent('CycleTabB')
    }
  ]
})

const testContainerWithProps = (props) => {
  const cyclesContainer = renderer.create(
    <CyclesContainer {...props} />
  ).toJSON()

  expect(cyclesContainer).toMatchSnapshot()
}

describe('CyclesContainer Specs', () => {
  it('renders with first active tab defaulted', () => {
    testContainerWithProps({activeCyclesTab: null, activateCyclesTab: 'activeCyclesTab1'})
  })

  it('renders with first active tab', () => {
    testContainerWithProps({activeCyclesTab: 'labelA', activateCyclesTab: 'activeCyclesTab2'})
  })

  it('renders with second active tab', () => {
    testContainerWithProps({activeCyclesTab: 'labelB', activateCyclesTab: 'activeCyclesTab3'})
  })

  it('mapStateToProps', () => {
    const expectedProps = {activeCyclesTab: 'yep'}
    expect(mapStateToProps({cycles: expectedProps})).toEqual(expectedProps)
  })
})
