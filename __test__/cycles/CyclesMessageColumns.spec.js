import {CycleMessageColumns} from '../../src/cycles/CycleMessageColumns'

describe('CycleMessageColumns', () => {
  let cols
  beforeAll(() => {
    cols = CycleMessageColumns
  })
  it('has id column', () => {
    expect(cols[0]).toEqual({
      field: 'id',
      customProps: {
        hidden: true,
        isKey: true
      }
    })
  })

  it('has type column', () => {
    expect(cols[1]).toEqual({
      field: 'messageTypeName',
      sort: true,
      label: 'Type',
      customProps: {
        textAlign: 'left',
        textAlignHeader: 'left',
        width: '100'
      }
    })
  })

  it('has contract number column', () => {
    expect(cols[2]).toEqual({
      field: 'contractNumber',
      sort: true,
      label: 'Contract #',
      customProps: {
        textAlign: 'left',
        width: '150'
      }
    })
  })

  it('has message column', () => {
    expect(cols[3]).toEqual({
      field: 'message',
      sort: true,
      label: 'Message',
      customProps: {
        textAlign: 'left',
        textAlignHeader: 'left'
      }
    })
  })
})
