import {apiPath} from '../../src/config'
import http from '../../src/actions/http'

import {
  activateCyclesTab,
  loadDailyCycleMessages,
  loadMonthlyCycleMessages,
  runDailyCycle,
  runPreliminaryMonthlyCycle,
  finalizeMonthlyCycle
} from '../../src/cycles/CyclesActions'

jest.mock('../../src/actions/http', () => ({
  get: jest.fn(),
  post: jest.fn()
}))

jest.mock('../../src/components/redux-dialog/redux-dialog', () => ({
  closeDialog: jest.fn().mockImplementation(dialogId => dialogId),
  openSnackbar: jest.fn()
}))

describe('CyclesActions', () => {
  let dispatch, getState, state

  beforeEach(() => {
    dispatch = jest.fn()
    state = {}
    getState = jest.fn().mockReturnValue(state)
    http.post.mockClear()
    http.get.mockClear()
  })

  it('activateCyclesTab', () => {
    const result = activateCyclesTab('some label')
    expect(result.type).toBe('ACTIVATE_CYCLES_TAB')
    expect(result.label).toBe('some label')
  })

  it('loadDailyCycleMessages', () => {
    loadDailyCycleMessages()(dispatch)
    const action = dispatch.mock.calls[0][0]

    expect(action.type).toBe('LOAD_DAILY_CYCLE_MESSAGES')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/cycles/daily`)
  })

  it('loadMonthlyCycleMessages', () => {
    loadMonthlyCycleMessages()(dispatch)
    const action = dispatch.mock.calls[0][0]

    expect(action.type).toBe('LOAD_MONTHLY_CYCLE_MESSAGES')
    expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/cycles/monthly`)
  })

  it('runDailyCycle', () => {
    runDailyCycle()(dispatch)
    const action = dispatch.mock.calls[0][0]

    expect(action.type).toBe('RUN_DAILY_CYCLE')
    expect(http.post.mock.calls[0][0]).toBe(`${apiPath}/cycles/daily`)
    expect(dispatch.mock.calls[1][0]).toEqual({type: 'IS_CYCLE_STARTED', isStarted: true})
  })

  it('runMonthlyCycle', () => {
    dispatch.mockImplementation(() => Promise.resolve('do something'))
    state.form = {
      PreliminaryMonthlyRun: {
        values: {
          invoices: true,
          statements: true
        }
      }
    }
    const thePromise = runPreliminaryMonthlyCycle()(dispatch, getState)
    const action = dispatch.mock.calls[0][0]
    expect(action.type).toBe('RUN_MONTHLY_CYCLE')
    expect(http.post.mock.calls[0][1]).toEqual({
      invoices: true,
      statements: true
    })
    expect(http.post.mock.calls[0][0]).toBe(`${apiPath}/cycles/monthly`)
    return thePromise.then((test) => {
      expect(dispatch.mock.calls[1][0]).toEqual({type: 'IS_CYCLE_STARTED', isStarted: true})
      expect(dispatch.mock.calls[2][0]).toEqual('runPreliminaryMonthlyCycle')
    })
  })

  it('finalizeMonthlyCycle', () => {
    finalizeMonthlyCycle()(dispatch)
    const action = dispatch.mock.calls[0][0]

    expect(action.type).toBe('FINALIZE_MONTHLY_CYCLE')
    expect(http.post.mock.calls[0][0]).toBe(`${apiPath}/cycles/monthly/finalize`)
    expect(dispatch.mock.calls[1][0]).toEqual({type: 'IS_CYCLE_STARTED', isStarted: true})
  })
})
