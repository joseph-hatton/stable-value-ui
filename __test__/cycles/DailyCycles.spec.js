import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import { DailyCycles, actions, mapStateToProps } from '../../src/cycles/DailyCycles'

jest.mock('react-redux', () => {
  return {
    connect: (mapStateToProps, mapDispatchToProps) => (component) => <component/> // eslint-disable-line react/display-name
  }
})
jest.mock('react-intl', () => ({ IntlProvider: mockComponent('IntlProvider') }))
jest.mock('material-ui/Card', () => ({ Card: mockComponent('Card') }))
jest.mock('material-ui/Toolbar', () => ({ Toolbar: mockComponent('Toolbar'), ToolbarGroup: mockComponent('ToolbarGroup') }))
jest.mock('material-ui/RaisedButton', () => mockComponent('RaisedButton'))
jest.mock('material-ui/svg-icons/action/cached', () => mockComponent('Cached'))
jest.mock('material-ui/svg-icons/av/play-arrow', () => mockComponent('PlayArrow'))
jest.mock('../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn().mockReturnValue({})
}))

const createProps = (canRun = true) => ({
  dailyCycleMessages: jest.fn(),
  loadDailyCycleMessages: jest.fn(),
  runDailyCycle: jest.fn(),
  canRunDailyCycle: canRun
})

describe('DailyCycles Specs', () => {
  it('renders correctly', () => {
    const props = createProps()
    const dailyCycles = renderer.create(
      <DailyCycles {...props} />
    )
    expect(dailyCycles.toJSON()).toMatchSnapshot()
    dailyCycles.getInstance().componentDidMount()
    expect(props.loadDailyCycleMessages).toHaveBeenCalled()
  })

  it('run button is disabled without permissions', () => {
    const props = createProps(false)
    const dailyCycles = renderer.create(
      <DailyCycles {...props} />
    )
    expect(dailyCycles.toJSON()).toMatchSnapshot()
    dailyCycles.getInstance().componentDidMount()
    expect(props.loadDailyCycleMessages).toHaveBeenCalled()
  })

  it('refresh button loads daily cycle messages', () => {
    const props = createProps()
    const dailyCycles = shallow(
      <DailyCycles {...props} />
    )
    dailyCycles.find({label: 'Refresh'}).simulate('click')
    expect(props.loadDailyCycleMessages).toHaveBeenCalled()
  })

  it('run button runs daily cycle', () => {
    const props = createProps()
    const dailyCycles = shallow(
      <DailyCycles {...props} />
    )
    dailyCycles.find({label: 'Run'}).simulate('click')
    expect(props.runDailyCycle).toHaveBeenCalled()
  })

  it('mapStateToProps', () => {
    expect(mapStateToProps({cycles: {dailyCycleMessages: 'dailyCycleMessages', isRunning: true}, currentUser: {RUN_DAILY_CYCLE: true}}))
    .toEqual({dailyCycleMessages: 'dailyCycleMessages', isRunning: true, canRunDailyCycle: true})
  })
})
