import React from 'react'
// import renderer from 'react'
import CyclesTabs from '../../src/cycles/CyclesTabs'
jest.mock('../../src/cycles/DailyCycles', () => 'DailyCycles')
jest.mock('../../src/cycles/MonthlyCycles', () => 'MonthlyCycles')

describe('CyclesTabs Specs', () => {
  it('has two cycles tabs', () => {
    expect(CyclesTabs.length).toBe(2)
  })

  it('has daily tab', () => {
    const cycle = CyclesTabs[0]
    expect(cycle.label).toBe('Daily')
    expect(cycle.CycleTab).toBe('DailyCycles')
  })

  it('has monthly tab', () => {
    const cycle = CyclesTabs[1]
    expect(cycle.label).toBe('Monthly')
    expect(cycle.CycleTab).toBe('MonthlyCycles')
  })
})
