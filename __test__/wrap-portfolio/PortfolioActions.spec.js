import http from '../../src/actions/http'
import {apiPath} from '../../src/config'
import { browserHistory } from 'react-router'

import {
    loadAllPortfolios,
    savePortfolio, // pending
    loadPortfolio,
    selectPortfolioInputRow,
    deletePortfolio,
    clearModelInputEditForm,
    loadPortfolioByContract,
    openPortfolio// pending
} from '../../src/wrap-portfolio/PortfolioActions'

jest.mock('../../src/components/redux-dialog/redux-dialog', () => ({
    openDialog: jest.fn().mockImplementation(dialogName => dialogName),
    openSnackbar: jest.fn().mockImplementation(payload => payload),
    openConfirmationAlert: jest.fn().mockImplementation(payload => payload)
}))

jest.mock('../../src/actions/http', () => ({
    get: jest.fn().mockReturnValue('get'),
    put: jest.fn().mockReturnValue('put'),
    post: jest.fn().mockReturnValue('post'),
    delete: jest.fn().mockReturnValue('delete')
}))

jest.mock('react-router', () => ({
    browserHistory: {
        push: jest.fn()
    }
}))

const createGetState = (state) => jest.fn().mockImplementation(() => state)

describe('Portfolio Actions', () => {
    let dispatch, getState, state
    beforeEach(() => {
         state = {form: {}}
         dispatch = jest.fn()
         getState = jest.fn().mockReturnValue(state)
    })
    it('load all should return action', () => {
        const result = loadAllPortfolios()
        expect(result.type).toBe('LOAD_ALL_PORTFOLIOS')
        expect(result.payload).toBe('get')
        expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/portfolios`)
    })

    it('loadPortfolio should return', () => {
        const portfolio = {portfolioId: 1}
        loadPortfolio(1)(dispatch, getState)
        const result = dispatch.mock.calls[0][0]
        expect(result.type).toBe('LOAD_PORTFOLIO')
        expect(result.payload).toBe('get')
        expect(http.get.mock.calls[1][0]).toBe(`${apiPath}/portfolios/1`)
    })

    it('selectPortfolio should return editPortfolio', () => {
        const portfolio = {portfolioId: 1}
        const result = selectPortfolioInputRow(portfolio)
        expect(result.type).toBe('SELECT_PORTFOLIO_INPUT')
        expect(result.payload).toBe(portfolio)
    })

    it('deletePortfolio should return Delete', () => {
        const portfolio = {portfolioId: 1}
        deletePortfolio(portfolio)(dispatch, getState)
        const result = dispatch.mock.calls[0][0]
        expect(result.type).toBe('DELETE_PORTFOLIO')
        expect(result.payload).toBe('delete')
    })

    it('clearModelInputEditForm should return payload Undefined', () => {
        const portfolio = {portfolioId: 1}
        clearModelInputEditForm(portfolio)(dispatch)
        const result = dispatch.mock.calls[0][0]
        const result1 = dispatch.mock.calls[1][0]
        expect(result1.type).toBe('CLEAR_EDIT_PORTFOLIO')
        expect(result1.payload).toBe(undefined)
        expect(dispatch.mock.calls[0][0].type).toBe('@@redux-form/DESTROY')
    })
    it('openPortfolio  should return payload portfolio', () => {
        const portfolio = {portfolioId: 1}
        openPortfolio(portfolio)(dispatch)
        const result = dispatch.mock.calls[0][0]
        expect(result.type).toBe('SELECT_PORTFOLIO')
        expect(result.payload).toBe(portfolio)
        expect(dispatch.mock.calls[0][0].type).toBe('SELECT_PORTFOLIO')
    })

  it('SAVE_PORTFOLIO should return SAVE_PORTFOLIO', async () => {
      const dispatch = jest.fn()
      const editPortfolio = null
      const portfolio = {
          version: 2
      }
      const getState = createGetState({
          portfolios: {
              id: 1, version: 2
          },
          form: {
              AddEditPortfolioForm: {
                  values: {version: 2}
              }
          }
      })

      http.post.mockReturnValue(Promise.resolve(editPortfolio))
      savePortfolio()(dispatch, getState)
      const result = dispatch.mock.calls[0][0]
      expect(result.type).toBe('SAVE_PORTFOLIO')
      expect(await result.payload).toBe(editPortfolio)
      expect(browserHistory.push).toHaveBeenCalled()
  })

    it('loadPortfolioByContract should return LOAD_PORTFOLIO_BY_CONTRACT', async () => {
        const contractId = 123
        const contract = {contractId: 123}
        http.get.mockReturnValue(Promise.resolve(contract))
        loadPortfolioByContract(contractId)(dispatch, getState)

        const result = dispatch.mock.calls[0][0]
        expect(result.type).toBe('LOAD_PORTFOLIO_BY_CONTRACT')
        expect(await result.payload).toEqual({'contractId': 123, 'portfolios': {'contractId': 123}})
    })
})
