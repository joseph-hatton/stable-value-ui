import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from '../MockComponent'
import ChipPortfolioArray from '../../src/wrap-portfolio/PorfoliosChip'

jest.mock('material-ui/Chip', () => mockComponent('Chip'))

const data = {data:
    {portfolioOneName: 'ABC',
        portfolioOneRate: 1,
        portfolioTwoName: 'ABC2',
        portfolioTwoRate: 2,
        portfolioThreeName: 'ABC3',
        portfolioThreeRate: 3,
        portfolioFourName: 'ABC4',
        portfolioFourRate: 4,
        portfolioFiveName: 'AB5C',
        portfolioFiveRate: 5,
        portfolioSixName: 'ABC6',
        portfolioSixRate: 6,
        portfolioSevenName: 'ABC7',
        portfolioSevenRate: 7,
        portfolioEightName: 'ABC8',
        portfolioEightRate: 8
    }}
const dataBad = {data:
    {
    }}

describe('ChipPortfolioArray', () => {
    it('should render correctly', () => {
        const component = renderer.create(<ChipPortfolioArray {...data} />)
        expect(component.toJSON()).toMatchSnapshot()
    })

    it('should not render correctly bad data', () => {
        const component = renderer.create(<ChipPortfolioArray {...dataBad} />)
        expect(component.toJSON()).toMatchSnapshot()
    })
})
