import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from '../MockComponent'
import { connect } from 'react-redux'
import {hasEntitlement} from '../../src/entitlements'
import { ViewEditPortfolioMenu, portfolioMenu, mapStateToProps } from '../../src/wrap-portfolio/ViewEditPortfolioMenu'

jest.mock('../../src/entitlements', () => ({hasEntitlement: jest.fn().mockReturnValue(true)}))
jest.mock('material-ui/styles/colors', () => ({blue500: 'blue', red500: 'blue'}))
jest.mock('material-ui/svg-icons/editor/mode-edit', () => mockComponent('EditIcon'))
jest.mock('material-ui/svg-icons/action/delete', () => mockComponent('DeleteIcon'))
jest.mock('react-redux', () => ({ connect: jest.fn(() => (comp) => comp) }))
jest.mock('../../src/wrap-portfolio/PortfolioActions', () => ({
    selectPortfolioInputRow: jest.fn(),
    deletePortfolio: jest.fn
}))

jest.mock('../../src/components/redux-dialog/redux-dialog', () => ({
    openDialog: jest.fn(),
    closeDialog: jest.fn()
}))

jest.mock('material-ui', () => ({
    Popover: mockComponent('Popover'),
    Menu: mockComponent('Menu'),
    MenuItem: mockComponent('MenuItem')
}))

describe('ViewEditPortfolioMenu', () => {
    let props
    beforeEach(() => {
        props = {
            open: true,
            closeDialog: jest.fn(),
            openDialog: jest.fn(),
            selectPortfolioInputRow: jest.fn(),
            deletePortfolio: jest.fn(),
            editPortfolio: {portfolioId: 1},
            anchorEl: 'anchorEl',
            cell: 'cell',
            row: {portfolioId: 123}
        }
    })
    it('should render correctly', () => {
        const component = renderer.create(<ViewEditPortfolioMenu {...props} />)
        expect(component.toJSON()).toMatchSnapshot()
    })

    it('should render correctly on click', () => {
        const component = shallow(<ViewEditPortfolioMenu {...props} />)
        const e = {nativeEvent: {preventDefault: jest.fn()}, currentTarget: 'currentTarget'}
        component.prop('onClick')(e)
        expect(props.selectPortfolioInputRow).toHaveBeenCalled()
        expect(props.openDialog).toHaveBeenCalled()
        expect(props.openDialog.mock.calls[0][0]).toBe('portfolioMenu')
    })

    it('should render correctly Menu', () => {
        const component = renderer.create(<portfolioMenu {...props} />)
        expect(component.toJSON()).toMatchSnapshot()
    })

    it('mapStateToProps should map correctly', () => {
        const state = {
            dialog: {
                portfolioMenu: {anchorEl: 'anchorEl'}
            },
            portfolios: {
                editPortfolio: {portfolioId: 1}
            }
        }
        const props = mapStateToProps(state)
        expect(props.open).toBe(true)
        expect(props.anchorEl).toBe('anchorEl')
        expect(props.editPortfolio.portfolioId).toBe(1)
    })

    it('mapStateToProps should NOT map correctly', () => {
        const state = {
            dialog: {
                portfolioInput: false
            },
            portfolios: {
                editPortfolio: undefined
            }
        }
        const props = mapStateToProps(state)
        expect(props.open).toBe(false)
        expect(props.anchorEl).toBe(null)
        expect(props.editPortfolio).toBe(undefined)
    })
})
