import React from 'react'
import _ from 'lodash'
import { Link } from 'react-router'
import {dateFormat} from '../../src/components/utilities/Formatters'
import moment from 'moment'
import { PortfolioAllocationColumns } from '../../src/wrap-portfolio/PortfolioAllocationColumns'
import ChipPortfolioArray from '../../src/wrap-portfolio/PorfoliosChip'

jest.mock('../../src/components/renderers/ReferenceDataTextRenderer', () => {
  return {createReferenceDataTextRenderer: jest.fn().mockImplementation(refDataKey => refDataKey)}
})

const expectedColumns = [
  'portfolioAllocationId',
  'effectiveDate',
  'contractNumber',
  'shortPlanName',
  'manager',
  'portfolios'
]

const getRenderer = (field) => PortfolioAllocationColumns.find(column => column.field === field).renderer

describe('PortfolioAllocations Columns', () => {
  it('Defines all Portfolio columns', () => {
    expect(PortfolioAllocationColumns.length).toEqual(6)
    expect(_.map(PortfolioAllocationColumns, 'field')).toEqual(expectedColumns)
  })
  it('formats effectiveDate to correct format', () => {
    const date = moment('2018-03-01')
    const effectiveDateRenderer = getRenderer('effectiveDate')
    expect(effectiveDateRenderer(date)).toMatchSnapshot()
  })

  it('renders contractNumber', () => {
    const RenderContractNumber = getRenderer('contractNumber')
    const contractNumber = 'RGA007'
    const portfolioAllocation = {portfolioAllocationId: 1, portfolioName: 'ABC-123', managerName: 'DEF-345'}
  })
})
