import React from 'react'
import mockComponent from '../MockComponent'
import {shallow} from 'enzyme'
import renderer from 'react-test-renderer'
import { PortfolioAllocationFilter, mapStateToProps } from '../../src/wrap-portfolio/PortfolioAllocationFilter'
import moment from 'moment'
import ContractPicker from '../../src/contracts/ContractPicker'
import ManagersFormField from '../../src/contacts/ManagersFormField'

jest.mock('react-redux', () => {
    const mockConnectChildFunction = jest.fn().mockImplementation(component => <component/>)
    return {
        connect: jest.fn().mockImplementation(() => mockConnectChildFunction)
    }
})
jest.mock('material-ui', () => ({RaisedButton: mockComponent('RaisedButton'), IconButton: mockComponent('IconButton')}))
jest.mock('redux-form', () => ({Field: mockComponent('Field'), reduxForm: () => (comp) => comp, change: mockComponent('change')}))
jest.mock('redux-form-material-ui', () => ({DatePicker: mockComponent('DatePicker'), TextField: mockComponent('TextField')}))
jest.mock('../../src/contracts/ContractPicker', () => mockComponent('ContractPicker'))
jest.mock('../../src/contacts/ManagersFormField', () => mockComponent('ManagersFormField'))
jest.mock('material-ui/svg-icons/content/clear', () => mockComponent('Clear'))

describe('Add Edit Portfolio Allocation Filter', () => {
    it('Renders correctly with props', () => {
        const clearAll = jest.fn()
        const clearDate = jest.fn()
        const from = null
        const to = null
        const props = {clearAll, clearDate, from, to}
        const component = renderer.create(<PortfolioAllocationFilter {...props}/>).toJSON()
        expect(component).toMatchSnapshot()
    })
})

describe('mapStateToProps', () => {
    it('mapping correctly', () => {
        const fromDate = moment().toDate()
        const toDate = moment().subtract('month', 5).toDate()
        const form = {form: {
            PortfolioAllocationFiltersForm: {values: {
                from: fromDate,
                to: toDate
            }}}}

        const result = mapStateToProps(form)
        expect(result.from).toBe(fromDate)
    })
})

describe('cick actions ', () => {
    it('click clearAll ', () => {
        const clearAll = jest.fn()
        const clearDate = jest.fn()
        const from = null
        const to = null
        const props = {clearAll, clearDate, from, to}
        const component = shallow(<PortfolioAllocationFilter {...props}/>)
        expect(props.clearAll).not.toHaveBeenCalled()
        component.find({id: 'clearButtonid'}).prop('onClick')()
        expect(props.clearAll).toHaveBeenCalled()
    })
})
