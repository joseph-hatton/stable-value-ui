import React from 'react'
import mockComponent from '../MockComponent'
import {shallow} from 'enzyme'
import renderer from 'react-test-renderer'
import { hasEntitlement } from '../../src/entitlements'

import {
    AddEditPortfolioAllocationComponent,
    actions,
    mapStatesToProps,
    map,
    AddEditPortfolioAllocationForm
} from '../../src/wrap-portfolio/AddEditPortfolioAllocationComponent'

// jest.mock('react-redux', () => ({connect: jest.fn(() => jest.fn(component => component))}))
jest.mock('react-redux', () => {
    const mockConnectChildFunction = jest.fn().mockImplementation(component => <component/>)
    return {
        connect: jest.fn().mockImplementation(() => mockConnectChildFunction)
    }
})
jest.mock('material-ui/Card', () => ({Card: mockComponent('Card')}))
jest.mock('material-ui/RaisedButton', () => mockComponent('RaisedButton'))
jest.mock('redux-form', () => ({Field: mockComponent('Field'), reduxForm: () => (comp) => comp, change: mockComponent('change')}))
jest.mock('material-ui/Toolbar', () => ({ Toolbar: mockComponent('Toolbar'), ToolbarGroup: mockComponent('ToolbarGroup') }))
jest.mock('../../src/entitlements', () => ({hasEntitlement: jest.fn().mockReturnValue(true)}))
jest.mock('redux-form-material-ui', () => ({DatePicker: mockComponent('DatePicker'), TextField: mockComponent('TextField')}))
jest.mock('react-router', () => ({Link: mockComponent('react-router')}))
jest.mock('../../src/wrap-portfolio/PortfolioByContractPicker', () => mockComponent('PortfolioByContractPicker'))
jest.mock('../../src/contracts/ContractPicker', () => mockComponent('ContractPicker'))
jest.mock('../../src/styles/styles', () => ({borderTop: 'solid 1px #e0e0e0', backgroundColor: '#f8f9fa', marginRight: '50px', padding: '25px'}))
jest.mock('../../src/components/redux-dialog/redux-dialog', () => mockComponent('ReduxDialog'))

describe('Add Edit Portfolio Form', () => {
    it('Renders correctly', () => {
        const saveAllocation = jest.fn()
        const handleSubmit = jest.fn()
        const addPortfolioAllocations = jest.fn()
        const resetAllocations = jest.fn()
        const clearModelInputEditForm = jest.fn()
        const closeDialog = jest.fn()
        const loadPortfolioByContract = jest.fn()
        const initialValues = {portfolioAllocationId: 1}
     handleSubmit.mockReturnValueOnce('saveAllocation')
        const props = {handleSubmit, closeDialog, saveAllocation, initialValues, addPortfolioAllocations, clearModelInputEditForm, resetAllocations, loadPortfolioByContract}
        const component = renderer.create(<AddEditPortfolioAllocationComponent {...props}/>).toJSON()
        expect(component).toMatchSnapshot()
        expect(handleSubmit).toHaveBeenCalledWith(AddEditPortfolioAllocationComponent)
    })
    it('should render title correctly when portfolioAllocationId is undefined', () => {
        const saveAllocation = jest.fn()
        const handleSubmit = jest.fn()
        const addPortfolioAllocations = jest.fn()
        const resetAllocations = jest.fn()
        const loadPortfolioByContract = jest.fn()
        const closeDialog = jest.fn()
        const clearModelInputEditForm = jest.fn()
        const initialValues = {portfolioAllocationId: undefined}
        handleSubmit.mockReturnValueOnce('saveAllocation')
        const props = {handleSubmit, closeDialog, clearModelInputEditForm, saveAllocation, initialValues, addPortfolioAllocations, resetAllocations, loadPortfolioByContract}
        const component = renderer.create(<AddEditPortfolioAllocationComponent {...props}/>)
        expect(component.toJSON()).toMatchSnapshot()
    })
    it('should disable save correctly', () => {
        const saveAllocation = jest.fn()
        const handleSubmit = jest.fn()
        const addPortfolioAllocations = jest.fn()
        const resetAllocations = jest.fn()
        const loadPortfolioByContract = jest.fn()
        const closeDialog = jest.fn()
        const clearModelInputEditForm = jest.fn()
        const initialValues = {portfolioAllocationId: undefined}
        handleSubmit.mockReturnValueOnce('saveAllocation')
        const props = {handleSubmit, closeDialog, clearModelInputEditForm, saveAllocation, initialValues, addPortfolioAllocations, resetAllocations, loadPortfolioByContract}
        hasEntitlement.mockReturnValue(false)
        const component = renderer.create(<AddEditPortfolioAllocationComponent {...props}/>)
        expect(component.toJSON()).toMatchSnapshot()
    })
    it('should render correctly when the contract is defined', () => {
        const saveAllocation = jest.fn()
      const closeDialog = jest.fn()
      const clearModelInputEditForm = jest.fn()
        const handleSubmit = jest.fn()
        const addPortfolioAllocations = jest.fn()
        const resetAllocations = jest.fn()
        const loadPortfolioByContract = jest.fn()
        const initialValues = {portfolioAllocationId: 1, contractId: 123}
        const props = {handleSubmit, closeDialog, clearModelInputEditForm, saveAllocation, initialValues, addPortfolioAllocations, resetAllocations, loadPortfolioByContract}
        const component = renderer.create(<AddEditPortfolioAllocationComponent {...props}/>)
        expect(component.toJSON()).toMatchSnapshot()
    })
    it('should change doSomething when Contract change', () => {
        const saveAllocation = jest.fn()
        const handleSubmit = jest.fn()
        const doSomething = jest.fn()
      const closeDialog = jest.fn()
      const clearModelInputEditForm = jest.fn()
        const resetAllocations = jest.fn()
        const addPortfolioAllocations = jest.fn()
        const loadPortfolioByContract = jest.fn()
        const event = jest.fn()
        const contractId = 538
        const initialValues = {contractId: 538,
contractNumber: 'MARSH-0812-01',
createdDate: '2014-08-15T07:34:11.242Z',
createdId: 's0041073',
            effectiveDate: '2014-06-30T00:00:00.000Z',
manager: 'Invesco Advisers, Inc',
modifiedDate: '2018-02-05T20:29:47.450Z',
            modifiedId: 's0043661',
portfolioAllocationId: 1,
portfolioEightId: null,
portfolioEightName: null,
portfolioEightRate: null,
            portfolioFiveId: null,
portfolioFiveName: null,
portfolioFiveRate: null,
            portfolioFourId: null,
portfolioFourName: null,
portfolioFourRate: null,
            portfolioManagerId: '559',
portfolioOneId: 1,
portfolioOneName: 'Short Term Bond Fund',
            portfolioOneRate: 45,
portfolioSevenId: null,
portfolioSevenName: null,
portfolioSevenRate: null,
            portfolioSixId: null,
portfolioSixName: null,
portfolioSixRate: null,
            portfolioThreeId: 3,
portfolioThreeName: 'Invesco Intermediate Gov/Credit',
            portfolioThreeRate: 36,
portfolioTwoId: 2,
portfolioTwoName: 'Invesco Core Fund',
            portfolioTwoRate: 19,
shortPlanName: 'Marsh',
version: 0
        }
        const props = {handleSubmit, closeDialog, clearModelInputEditForm, contractId, saveAllocation, initialValues, addPortfolioAllocations, resetAllocations, loadPortfolioByContract}
        const component = shallow(<AddEditPortfolioAllocationComponent {...props}/>)
        component.find({id: 'ContractPickerPortfolio'}).prop('onChange')()
        expect(resetAllocations).toHaveBeenCalled()
    })
})

describe('mapStateToProps - map actions', () => {
    const form = {AddEditPortfolioAllocationForm: {
        values: {contractId: 538,
contractNumber: 'MARSH-0812-01',
createdDate: '2014-08-15T07:34:11.242Z',
createdId: 's0041073',
            effectiveDate: '2014-06-30T00:00:00.000Z',
manager: 'Invesco Advisers, Inc',
modifiedDate: '2018-02-05T20:29:47.450Z',
            modifiedId: 's0043661',
portfolioAllocationId: 1,
portfolioEightId: null,
portfolioEightName: null,
portfolioEightRate: null,
            portfolioFiveId: null,
portfolioFiveName: null,
portfolioFiveRate: null,
            portfolioFourId: null,
portfolioFourName: null,
portfolioFourRate: null,
            portfolioManagerId: '559',
portfolioOneId: 1,
portfolioOneName: 'Short Term Bond Fund',
            portfolioOneRate: 45,
portfolioSevenId: null,
portfolioSevenName: null,
portfolioSevenRate: null,
            portfolioSixId: null,
portfolioSixName: null,
portfolioSixRate: null,
            portfolioThreeId: 3,
portfolioThreeName: 'Invesco Intermediate Gov/Credit',
            portfolioThreeRate: 36,
portfolioTwoId: 2,
portfolioTwoName: 'Invesco Core Fund',
            portfolioTwoRate: 19,
shortPlanName: 'Marsh',
version: 0
        }
    }
    }

    it('maps addEditPortfolio NEW PORTFOLIO', () => {
        const props = mapStatesToProps({form, portfolioAllocations: {editAllocation: AddEditPortfolioAllocationForm.values}})
        expect(props.portfolioThreeRate).toBe(36)
    })
})
