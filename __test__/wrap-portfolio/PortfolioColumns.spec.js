import React from 'react'
import _ from 'lodash'
import {createReferenceDataTextRenderer} from '../../src/components/renderers/ReferenceDataTextRenderer'
import { PortfolioColumns } from '../../src/wrap-portfolio/PortfolioColumns'
import {ViewEditPortfolioMenu} from '../../src/wrap-portfolio/ViewEditPortfolioMenu'
import {DeletePortfolioButton} from '../../src/wrap-portfolio/DeletePortfolioButton'
jest.mock('../../src/components/renderers/ReferenceDataTextRenderer', () => {
    return {createReferenceDataTextRenderer: jest.fn().mockImplementation(refDataKey => refDataKey)}
})

const expectedColumns = [
    'portfolioId',
    'portfolioName',
    'managerName',
    'active'
]

const getRenderer = (field) => PortfolioColumns.find(column => column.field === field).renderer

describe('Portfolio Columns', () => {
    it('Defines all Portfolio columns', () => {
        expect(PortfolioColumns.length).toEqual(4)
        expect(_.map(PortfolioColumns, 'field')).toEqual(expectedColumns)
    })
})
