import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import { loadAllPortfolios, openPortfolio } from '../../src/wrap-portfolio/PortfolioActions'
import {hasEntitlement} from '../../src/entitlements'
import {shallow} from 'enzyme'
import { PortfolioComponent, actions, mapStateToProps } from '../../src/wrap-portfolio/PortfolioComponent'

jest.mock('../../src/wrap-portfolio/PortfolioActions', () => ({
  loadAllPortfolios: jest.fn(),
    openPortfolio: jest.fn()
}))
jest.mock('../../src/wrap-portfolio/PortfolioColumns', () => ({
  PortfolioColumns: () => []
}))
jest.mock('../../src/wrap-portfolio/AddEditPortfolioComponent', () => mockComponent('AddEditPortfolioComponent'))
jest.mock('../../src/wrap-portfolio/ViewEditPortfolioMenu', () => mockComponent('ViewEditPortfolioMenu'))
jest.mock('material-ui/FloatingActionButton', () => mockComponent('FloatingActionButton'))
jest.mock('material-ui/svg-icons/content/add', () => mockComponent('ContentAdd'))
jest.mock('material-ui/Card')
jest.mock('../../src/components/grid/material-ui-grid/Grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn()
}))

jest.mock('react-redux', () => ({
  connect: jest.fn(() => jest.fn(component => component))
}))

jest.mock('../../src/entitlements', () => ({
  hasEntitlement: jest.fn().mockReturnValue(true)
}))

describe('PortfolioComponent Specs', () => {
  let props
  beforeEach(() => {
    props = {
      loadAllPortfolios,
      openDialog: jest.fn(),
      clearEditPortfolioState: jest.fn(),
      closeDialog: jest.fn(),
      clearModelInputEditForm: jest.fn(),
        openPortfolio
    }
  })
  it('Renders Spinner when there are no portfolios loaded and there is no error', () => {
    const component = renderer.create(<PortfolioComponent {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
    expect(loadAllPortfolios).toHaveBeenCalled()
  })
  it('should render correctly', () => {
    props.portfolios = [{portfolioId: 1}, {portfolioId: 2}, {portfolioId: 3}]
    const component = renderer.create(<PortfolioComponent {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('should hide fab correctly', () => {
    props.portfolios = [{}]
    hasEntitlement.mockReturnValue(false)
    const component = renderer.create(<PortfolioComponent {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })

    it('should show fab correctly', () => {
        props.portfolios = [{portfolioId: 1}, {portfolioId: 2}, {portfolioId: 3}]
        hasEntitlement.mockReturnValue(true)
        const component = renderer.create(<PortfolioComponent {...props} />)
        expect(component.toJSON()).toMatchSnapshot()
    })
})

describe('PortfolioComponent', () => {
    it('should call clearEditPortfolioState if edit portfolio is there', () => {
       const props = {
            loadAllPortfolios,
            openDialog: jest.fn(),
            clearEditPortfolioState: jest.fn(),
            closeDialog: jest.fn(),
            clearModelInputEditForm: jest.fn(),
            editPortfolio: {portfolioId: 1},
            openPortfolio,
           portfolios: [{portfolioId: 1}, {portfolioId: 2}, {portfolioId: 3}]

        }
        const component = renderer.create(<PortfolioComponent {...props} />)
        expect(component.toJSON()).toMatchSnapshot()
        expect(props.clearEditPortfolioState).toHaveBeenCalled()
    })

    it('should call closeDialog on onClose AddEditPortfolioComponent', () => {
        const props = {
            loadAllPortfolios,
            openDialog: jest.fn(),
            clearEditPortfolioState: jest.fn(),
            closeDialog: jest.fn(),
            clearModelInputEditForm: jest.fn(),
            editPortfolio: {portfolioId: 1},
            openPortfolio,
            portfolios: [{portfolioId: 1}, {portfolioId: 2}, {portfolioId: 3}]
        }
        const component = shallow(<PortfolioComponent {...props}/>)
        expect(props.clearEditPortfolioState).not.toHaveBeenCalled()
        expect(props.closeDialog).not.toHaveBeenCalled()
        component.find({id: 'addEditPortfolioId'}).prop('onClose')()
        expect(props.clearEditPortfolioState).toHaveBeenCalled()
        expect(props.closeDialog).toHaveBeenCalled()
    })

    it('should call openPortfolio on onRowDoubleClick', () => {
        const props = {
            loadAllPortfolios,
            openDialog: jest.fn(),
            clearEditPortfolioState: jest.fn(),
            closeDialog: jest.fn(),
            clearModelInputEditForm: jest.fn(),
            editPortfolio: {portfolioId: 1},
            openPortfolio: jest.fn(),
            portfolios: [{portfolioId: 1}, {portfolioId: 2}, {portfolioId: 3}]
        }
        const component = shallow(<PortfolioComponent {...props}/>)
        expect(props.openPortfolio).not.toHaveBeenCalled()
        component.find({id: 'portfolioGrid'}).prop('onRowDoubleClick')()
        expect(props.openPortfolio).toHaveBeenCalled()
    })
})

describe('Portfolio Component maps ', () => {
    it('maps to Props', () => {
        const state = {
            portfolios: {editPortfolio: {portfolioId: 1}, portfolios: [{portfolioId: 1}, {portfolioId: 2}, {portfolioId: 3}]}
        }
        const props = mapStateToProps(state)
        expect(props).toEqual({'editPortfolio': {'portfolioId': 1}, 'portfolios': [{'portfolioId': 1}, {'portfolioId': 2}, {'portfolioId': 3}]}
        )
    })
})
