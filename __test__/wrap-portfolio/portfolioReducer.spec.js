import _ from 'lodash'
import reduce, { addOrUpdatePortfolio, deletePortfolio } from '../../src/wrap-portfolio/portfolioReducer'

const initialState = {
    portfolios: [],
    editPortfolio: undefined,
    portfolioId: undefined,
    initialValues: undefined
}

describe('Portfolio reducer', () => {
    it('maps default empty state', () => {
        const newState = reduce(null, {type: 'UNKNOWN'})
        expect(newState).toEqual(null)
    })
    it('maps LOAD_ALL_PORTFOLIOS_FULFILLED Portfolio to state', () => {
        const portfolios = [{portfolioId: 123}, {portfolioId: 123}]
        const newState = reduce(null, {
            type: 'LOAD_ALL_PORTFOLIOS_FULFILLED',
            payload: {results: portfolios}
        })
        expect(newState).toEqual({portfolios})
    })
    it('maps SAVE_PORTFOLIO_FULFILLED Portfolio to state Save', () => {
        const portfolios = {portfolios: [{portfolioId: 123}, {portfolioId: 1}]}
        const newState = reduce(portfolios, {
            type: 'SAVE_PORTFOLIO_FULFILLED',
            payload: {portfolioId: 2}
        })
        expect(newState.editPortfolio).toEqual(undefined)
        expect(newState.portfolios.length).toBe(3)
    })
    it('maps SAVE_PORTFOLIO_FULFILLED Portfolio to state UPDATE', () => {
        const portfolios = {portfolios: [{portfolioId: 123}, {portfolioId: 1}]}
        const newState = reduce(portfolios, {
            type: 'SAVE_PORTFOLIO_FULFILLED',
            payload: {portfolioId: 123}
        })
        expect(newState.editPortfolio).toEqual(undefined)
        expect(newState.portfolios.length).toBe(2)
    })

    it('maps LOAD_PORTFOLIO_FULFILLED Portfolio to state', () => {
        const newState = reduce({}, {
            type: 'LOAD_PORTFOLIO_FULFILLED',
            payload: {portfolioID: 123}
        })
        expect(newState.editPortfolio).toEqual({portfolioID: 123})
    })

    it('maps CLEAR_EDIT_PORTFOLIO Portfolio to state', () => {
        const newState = reduce({}, {
            type: 'CLEAR_EDIT_PORTFOLIO',
            payload: {portfolioID: 123}
        })
        expect(newState.editPortfolio).toEqual(undefined)
    })

    it('maps SELECT_PORTFOLIO_INPUT Portfolio to state', () => {
        const newState = reduce({}, {
            type: 'SELECT_PORTFOLIO_INPUT',
            payload: {portfolioId: 123}
        })
        expect(newState.editPortfolio).toEqual({portfolioId: 123})
        expect(newState.portfolioId).toEqual(123)
    })

    it('maps SELECT_PORTFOLIO Portfolio to state', () => {
        const newState = reduce({}, {
            type: 'SELECT_PORTFOLIO',
            payload: {portfolioId: 123}
        })
        expect(newState.editPortfolio).toEqual({portfolioId: 123})
    })

    it('maps DELETE_PORTFOLIO_FULFILLED Portfolio to state', () => {
        const portfolios = {portfolios: [{portfolioId: 123}, {portfolioId: 1}]}
        const newState = reduce(portfolios, {
            type: 'DELETE_PORTFOLIO_FULFILLED',
            payload: {portfolioId: 123, id: 123}
        })
        expect(newState.editPortfolio).toEqual(undefined)
    })

    it('maps DELETE_PORTFOLIO_FULFILLED Portfolio doesnt exist  to state', () => {
        const portfolios = {portfolios: [{portfolioId: 123}, {portfolioId: 1}]}
        const newState = reduce(portfolios, {
            type: 'DELETE_PORTFOLIO_FULFILLED',
            payload: {portfolioId: 785, id: 7745}
        })
        expect(newState.editPortfolio).toEqual(undefined)
    })
})
