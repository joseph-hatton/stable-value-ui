import reduce from '../../src/wrap-portfolio/portfolioAllocationReducer'

const initialState = {
    allocations: [],
    portfoliosByContract: {
        results: []
    }
}

describe('Portfolio Allocation reducer', () => {
    it('maps default empty state', () => {
        const newState = reduce(null, {type: 'UNKNOWN'})
        expect(newState).toEqual(null)
    })
    it('maps results mot empty state', () => {
        const newState = reduce(null,
            {type: 'LOAD_ALL_PORTFOLIO_ALLOCATIONS_FULFILLED',
            payload: {results: [{portfolioAllocationId: 1}, {portfolioAllocationId: 3}]}})
        expect(newState.allocations).toEqual([{portfolioAllocationId: 1}, {portfolioAllocationId: 3}])
    })

    it('maps results not empty state SAVE_PORTFOLIO_ALLOCATION', () => {
        const newState = reduce({allocations: [{portfolioAllocationId: 1}]},
            {type: 'SAVE_PORTFOLIO_ALLOCATION_FULFILLED',
                    allocations: [{portfolioAllocationId: 1}],
                    payload: {portfolioAllocationId: 1}
            }
        )
        expect(newState.editAllocation).toEqual(undefined)
    })

    it('maps results not empty state LOAD_PORTFOLIO_ALLOCATION_FULFILLED', () => {
        const newState = reduce(null,
            {type: 'LOAD_PORTFOLIO_ALLOCATION_FULFILLED',
                payload: {portfolios: [{portfolioAllocationId: 1}, {portfolioAllocationId: 3}],
                    allocation: {portfolioAllocationId: 1}}
            }
        )
        expect(newState.portfoliosByContract).toEqual([{portfolioAllocationId: 1}, {portfolioAllocationId: 3}])
    })

    it('maps results CLEAR_EDIT_ALLOCATION', () => {
        const newState = reduce(null,
            {type: 'CLEAR_EDIT_ALLOCATION',
                payload: {portfolios: [{portfolioAllocationId: 1}, {portfolioAllocationId: 3}],
                    allocation: {portfolioAllocationId: 1}}
            }
        )
        expect(newState.editAllocation).toEqual(undefined)
    })
    it('maps results LOAD_PORTFOLIO_BY_CONTRACT_FULFILLED', () => {
        const newState = reduce(null,
            {type: 'LOAD_PORTFOLIO_BY_CONTRACT_FULFILLED',
                payload: {portfolios: [{portfolioAllocationId: 1}, {portfolioAllocationId: 3}],
                    allocation: {portfolioAllocationId: 1}}
            }
        )
        expect(newState.portfoliosByContract).toEqual([{portfolioAllocationId: 1}, {portfolioAllocationId: 3}])
    })
})
