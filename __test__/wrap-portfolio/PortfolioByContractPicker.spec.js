import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import { PortfolioByContractPicker, mapStateToProps } from '../../src/wrap-portfolio/PortfolioByContractPicker'
import MaterialSelectField from '../../src/components/forms/MaterialSelectField'
jest.mock('../../src/wrap-portfolio/PortfolioActions', () => ({
    loadPortfolioByContract: jest.fn()
}))
jest.mock('../../src/components/forms/MaterialSelectField', () => {
    return mockComponent('MaterialSelectField')
})

describe('PortfolioByContractPicker Test', () => {
    it('Renders correctly', () => {
        const loadPortfolioByContract = jest.fn()
        const options = []
        const name = 'AAAA'
        const props = {loadPortfolioByContract, options, name, contractId: 12}
        const PortfolioByContractPickerRender = renderer.create(
            <PortfolioByContractPicker {...props} />
        ).toJSON()
        expect(PortfolioByContractPickerRender).toMatchSnapshot()
        expect(loadPortfolioByContract).toHaveBeenCalled()
    })

    it('Not Renders correctly', () => {
        const loadPortfolioByContract = jest.fn()
        const options = [{portfolioId: 123, portfolioName: 'test'}, {}]
        const name = 'Test'
        const props = {loadPortfolioByContract, options, name}
        const PortfolioByContractPickerRender = renderer.create(
            <PortfolioByContractPicker {...props} />
        ).toJSON()
        expect(PortfolioByContractPickerRender).toMatchSnapshot()
        expect(loadPortfolioByContract).not.toHaveBeenCalled()
    })
})

describe('mapStateToProps', () => {
    it('mapping correctly', () => {
        const results = [ {text: 'test1', value: 1},
            {text: 'test2', value: 2},
            {text: 'test2', value: 2}]
        const state = {
            portfolioAllocations: {
                portfoliosByContract: {results: [ {portfolioName: 'test1', portfolioId: 1},
                    {portfolioName: 'test2', portfolioId: 2},
                    {portfolioName: 'test2', portfolioId: 2}]
            }}
        }
        const props = mapStateToProps(state)
        expect(props).toBeDefined()
        expect(props.options).toEqual(results)
    })
})
