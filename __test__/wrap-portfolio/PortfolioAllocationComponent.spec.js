import React from 'react'
import { PropTypes as T } from 'prop-types'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import {loadAllPortfolioAllocations, filterAllocationsByContract, filterAllocationsByManager} from '../../src/wrap-portfolio/PortfolioAllocationActions'
import {PortfolioAllocationColumns} from '../../src/wrap-portfolio/PortfolioAllocationColumns'
import PortfolioAllocationFilter from '../../src/wrap-portfolio/PortfolioAllocationFilter'
import {Card} from 'material-ui'
import moment from 'moment'
import {CreateGridConfig} from '../../src/components/grid/material-ui-grid/Grid'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import {hasEntitlement} from '../../src/entitlements'
import {serializeDates} from '../../src/actions/serializeDate'

import { PortfolioAllocationComponent, formatValues, mapStateToProps } from '../../src/wrap-portfolio/PortfolioAllocationComponent'

jest.mock('material-ui/svg-icons/content/add', () => mockComponent('ContentAdd'))
jest.mock('material-ui', () => ({
    Card: mockComponent('Card')
}))
jest.mock('react-redux', () => ({
    connect: jest.fn(() => jest.fn(component => component))
}))
jest.mock('../../src/components/common/Expandable', () => mockComponent('Expandable'))
jest.mock('material-ui/FloatingActionButton', () => mockComponent('FloatingActionButton'))
jest.mock('material-ui/Card')
jest.mock('../../src/components/grid/material-ui-grid/Grid', () => ({
    Grid: mockComponent('Grid'),
    CreateGridConfig: jest.fn()
}))
jest.mock('../../src/wrap-portfolio/PortfolioAllocationActions', () => ({
    loadAllPortfolioAllocations: jest.fn(),
    filterAllocationsByContract: jest.fn(),
    filterAllocationsByManager: jest.fn()
}))
jest.mock('../../src/wrap-portfolio/PortfolioAllocationFilter', () => mockComponent('PortfolioAllocationFilter'))

jest.mock('../../src/entitlements', () => ({
    hasEntitlement: jest.fn().mockReturnValue(true)
}))

jest.mock('react-router', () => ({
    Link: mockComponent('Link')
}))

describe('PortfolioAllocationComponent.js Specs', () => {
    let props
    beforeEach(() => {
        props = {
            loadAllPortfolioAllocations,
            clearEditAllocationState: jest.fn(),
            clearModelInputEditForm: jest.fn(),
            openDialog: jest.fn(),
            filterAllocationsByContract: jest.fn(),
            filterAllocationsByManager: jest.fn(),
            searchPortfolioAllocations: jest.fn()
        }
    })
    it('Renders Spinner when there are no portfolio loaded and there is no error', () => {
        const portfoliosAllocationComp = renderer.create(
            <PortfolioAllocationComponent {...props} />
        ).toJSON()
        expect(portfoliosAllocationComp).toMatchSnapshot()
        expect(loadAllPortfolioAllocations).toHaveBeenCalled()
    })

    it('clear if editAllocation', () => {
        props.editAllocation = {id: 123, portfolioId: 2}

        const portfoliosAllocationComp = renderer.create(
            <PortfolioAllocationComponent {...props} />
        ).toJSON()
        expect(portfoliosAllocationComp).toMatchSnapshot()
        expect(props.clearEditAllocationState).toHaveBeenCalled()
    })
    it('Filtering Allocations ', () => {
        const from = moment('2018-11-07').toDate()
        const to = moment('2018-11-10').toDate()
        const allocations = [{id: 1, effectiveDate: '2018-11-09T00:00:00.000Z'},
            {id: 2, effectiveDate: '2018-11-08T00:00:00.000Z', contractId: 123, portfolioManagerId: 3},
            {id: 3, effectiveDate: '2018-11-10T00:00:00.000Z', contractId: 123, portfolioManagerId: 3},
            {id: 4, effectiveDate: '2018-11-11T00:00:00.000Z', contractId: 123, portfolioManagerId: 3}]

        filterAllocationsByContract.mockReturnValueOnce(allocations)
        filterAllocationsByManager.mockReturnValueOnce(allocations)
        props.searchPortfolioAllocations.mockReturnValue(allocations)
        const filters = formatValues({from, to})

       expect(allocations.length).toBe(4)

        const comp = shallow(<PortfolioAllocationComponent {...props}/>).instance()
        const allocationFiltered = comp.filterAllocations(filters, allocations)
        expect(allocationFiltered.length).toBe(2)
    })

    it('Filtering Allocations null to  ', () => {
        const from = moment('2018-11-07').toDate()
        const to = null

        const filters = formatValues({from, to})

        const allocations = [{id: 1, effectiveDate: '2018-11-09T00:00:00.000Z'},
            {id: 2, effectiveDate: '2018-11-12T00:00:00.000Z'},
            {id: 3, effectiveDate: '2018-11-13T00:00:00.000Z'},
            {id: 4, effectiveDate: '2018-11-14T00:00:00.000Z'}]

        filterAllocationsByContract.mockReturnValueOnce(allocations)
        filterAllocationsByManager.mockReturnValueOnce(allocations)
        props.searchPortfolioAllocations.mockReturnValue(allocations)

        expect(allocations.length).toBe(4)
        const comp = shallow(<PortfolioAllocationComponent {...props}/>).instance()
        const allocationFiltered = comp.filterAllocations(filters, allocations)
        expect(allocationFiltered.length).toBe(4)
    })
})

describe('PortfolioAllocationComponent ', () => {
    it('maps Undefined filters to Props', () => {
        const state = {
            portfolioAllocations: {allocations: [{id: 1}, {id: 2}], editAllocation: {id: 2}},
            form: {values: {id: 2}}
        }
        const props = mapStateToProps(state)
        expect(props).toEqual({'allocations': [{'id': 1}, {'id': 2}], 'editAllocation': {'id': 2}, 'filters': undefined})
    })

    it('Removes empty filter values', () => {
        const filters = formatValues({from: null})

        expect(filters).toEqual({})
    })
// pending
    it('Formats from and to filter dates in YYYY-MM-DDT00:00:00.000Z format', () => {
        const from = moment('2017-11-20').toDate()
        const to = moment('2018-01-20').toDate()
        const filters = formatValues({from, to})

        expect(filters).toEqual({'from': '2017-11-20T00:00:00Z', 'to': '2018-01-20T00:00:00Z'}) // pending
    })
    it('Filtering Allocations null from   ', () => {
        const from = null
        const to = moment('2018-11-09').toDate()
        const filters = formatValues({from, to})
        const allocations = [{id: 1, effectiveDate: '2018-11-09T00:00:00.000Z'},
            {id: 2, effectiveDate: '2018-11-08T00:00:00.000Z'},
            {id: 3, effectiveDate: '2018-11-10T00:00:00.000Z'},
            {id: 4, effectiveDate: '2018-11-11T00:00:00.000Z'}]

      //  expect(allocations.length).toBe(4)
       // const allocationFiltered = filterAllocations(filters,allocations)
       // expect(allocationFiltered.length).toBe(1)
    })
})
