import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import { AddEditPortfolioContainer, mapStateToProps } from '../../src/wrap-portfolio/AddEditPortfolioContainer'

jest.mock('../../src/wrap-portfolio/PortfolioAllocationActions', () => ({
    savePortfolio: jest.fn(),
    loadPortfolio: jest.fn()
}))

jest.mock('../../src/wrap-portfolio/AddEditPortfolioComponent', () => {
    return mockComponent('AddEditPortfolioComponent')
})

describe('AddEditPortfolioContainer Specs', () => {
    it('Renders Correctly and load  is called when is loadPortfolio is passed', () => {
        const params = {portfolioId: 1}
        const loadPortfolio = jest.fn()
        const props = {params, loadPortfolio}

        const AddEditPortfolioContainerRender = renderer.create(
            <AddEditPortfolioContainer {...props} />
        ).toJSON()

        expect(AddEditPortfolioContainerRender).toMatchSnapshot()
        expect(loadPortfolio).toHaveBeenCalled()
    })

    it('Renders Correctly and load  is called when no loadPortfolio is passed', () => {
        const params = {portfolioId: null}
        const loadPortfolio = jest.fn()
        const router = {}
        const props = {params, loadPortfolio, router}
        const AddEditPortfolioContainerRender = renderer.create(
            <AddEditPortfolioContainer {...props} />
        ).toJSON()

        expect(AddEditPortfolioContainerRender).toMatchSnapshot()
        expect(loadPortfolio).not.toHaveBeenCalled()
    })
})

describe('mapStateToProps', () => {
    it('Renders Correctly and load  is called when no allocation are passed', () => {
        const state = {
            portfolios: {
                editPortfolio: {
                    portfolioId: 123,
                    managerName: 'TEST',
                    portfolioManagerId: 34
                }
            }
        }
        const props = mapStateToProps(state)
        expect(props.editPortfolio).toBeDefined()
        expect(props.editPortfolio.portfolioId).toBe(123)
    })
})
