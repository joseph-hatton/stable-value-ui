import http from '../../src/actions/http'
import {apiPath} from '../../src/config'
import { browserHistory } from 'react-router'

import {
    loadAllPortfolioAllocations,
    saveAllocation, // pending
    loadAllocation, // pending
    resetAllocations
} from '../../src/wrap-portfolio/PortfolioAllocationActions'

jest.mock('../../src/components/redux-dialog/redux-dialog', () => ({
    openDialog: jest.fn().mockImplementation(dialogName => dialogName),
    openSnackbar: jest.fn().mockImplementation(payload => payload),
    openConfirmationAlert: jest.fn().mockImplementation(payload => payload)
}))

jest.mock('../../src/actions/http', () => ({
    get: jest.fn().mockReturnValue('get'),
    put: jest.fn().mockReturnValue('put'),
    post: jest.fn().mockReturnValue('post'),
    delete: jest.fn().mockReturnValue('delete')
}))

jest.mock('react-router', () => ({
    browserHistory: {
        push: jest.fn()
    }
}))

const createGetState = (state) => jest.fn().mockImplementation(() => state)

describe('Portfolio Allocation Actions', () => {
    let dispatch, getState, state
    beforeEach(() => {
         state = {form: {}}
         dispatch = jest.fn()
         getState = jest.fn().mockReturnValue(state)
    })
    it('loadAllPortfolioAllocations', () => {
        const result = loadAllPortfolioAllocations()
        expect(result.type).toBe('LOAD_ALL_PORTFOLIO_ALLOCATIONS')
        expect(result.payload).toBe('get')
        expect(http.get.mock.calls[0][0]).toBe(`${apiPath}/portfolio-allocations`)
    })

    it('resetAllocations', () => {
        const allocationId = 1
        const resetFields = [{id: 'id', name: 'name', rate: 'rate'}]
        resetAllocations(resetFields)(dispatch)
        expect(dispatch).toHaveBeenCalled()
    })

    it('saveAllocation should return SAVE_PORTFOLIO_ALLOCATION', async () => {
        const dispatch = jest.fn()
        const editAllocation = {portfolioAllocationId: 123}
        const portfolioAllocation = {
            version: 2
        }
        const getState = createGetState({
            portfolioAllocations: {
                id: 1, version: 2
            },
            form: {
                AddEditPortfolioAllocationForm: {
                    values: {version: 2}
                }
            }
        })

        http.post.mockReturnValue(Promise.resolve(editAllocation))
        saveAllocation()(dispatch, getState)
        const result = dispatch.mock.calls[0][0]
        expect(result.type).toBe('SAVE_PORTFOLIO_ALLOCATION')
        expect(await result.payload).toBe(editAllocation)
        expect(browserHistory.push).toHaveBeenCalled()
    })

    it('loadAllocation should return LOAD_PORTFOLIO_ALLOCATION', async () => {
        const dispatch = jest.fn()
        const editAllocation = {portfolioAllocationId: 123}
        const portfolioAllocation = {
            portfolioAllocationId: 123
        }
        const getState = createGetState({
            portfolioAllocations: {
                portfolioAllocationId: 123
            },
            form: {
                AddEditPortfolioAllocationForm: {
                    values: {portfolioAllocationId: 123}
                }
            }
        })

        http.get.mockReturnValue(Promise.resolve(editAllocation))
        loadAllocation(123)(dispatch, getState)
        const result = dispatch.mock.calls[0][0]
        expect(result.type).toBe('LOAD_PORTFOLIO_ALLOCATION')
        expect(await result.payload).toEqual({'allocation': {'portfolioAllocationId': 123}, 'portfolios': {'portfolioAllocationId': 123}})
        expect(browserHistory.push).toHaveBeenCalled()
    })
})
