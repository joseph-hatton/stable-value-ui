import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import { savePortfolio } from '../../src/wrap-portfolio/PortfolioActions'
import { hasEntitlement } from '../../src/entitlements'
import {shallow} from 'enzyme'
import {reduxForm} from 'redux-form'

import {
    AddEditPortfolioComponent,
    actions,
    AddEditPortfolioForm,
    mapStateToProps
} from '../../src/wrap-portfolio/AddEditPortfolioComponent'
jest.mock('react-redux', () => ({connect: jest.fn(() => jest.fn(component => component))}))
jest.mock('material-ui', () => ({Card: mockComponent('Card')}))
jest.mock('material-ui/RaisedButton', () => mockComponent('RaisedButton'))
jest.mock('redux-form', () => ({Field: mockComponent('Field'), reduxForm: () => (comp) => comp}))
jest.mock('material-ui/Toolbar', () => ({ Toolbar: mockComponent('Toolbar'), ToolbarGroup: mockComponent('ToolbarGroup') }))
jest.mock('../../src/components/redux-dialog/redux-dialog', () => mockComponent('ReduxDialog'))
jest.mock('../../src/contacts/ManagersFormField', () => mockComponent('ManagersFormField'))
jest.mock('../../src/wrap-portfolio/PortfolioActions', () => ({savePortfolio: jest.fn()}))
jest.mock('../../src/styles/styles', () => ({
borderTop: 'solid 1px #e0e0e0', backgroundColor: '#f8f9fa', marginRight: '50px', padding: '25px'}))
jest.mock('../../src/contacts/ManagersFormField', () => mockComponent('ManagersFormField'))
jest.mock('../../src/entitlements', () => ({hasEntitlement: jest.fn().mockReturnValue(true)}))

describe('Add Edit Portfolio Form', () => {
    it('Renders correctly', () => {
        const handleSubmit = jest.fn()
        const onClose = jest.fn()
        const initialValues = null
        handleSubmit.mockReturnValueOnce('savePortfolio')
        const props = {handleSubmit, onClose, savePortfolio, initialValues}
        const component = renderer.create(<AddEditPortfolioComponent {...props}/>).toJSON()
        expect(component).toMatchSnapshot()
        expect(handleSubmit).toHaveBeenCalledWith(savePortfolio)
    })

    it('cancel onClick should call closeDialog', () => {
        const handleSubmit = jest.fn()
        const onClose = jest.fn()
        const initialValues = null
        handleSubmit.mockReturnValueOnce('savePortfolio')
        const props = {handleSubmit, onClose, savePortfolio, initialValues}
        const component = shallow(<AddEditPortfolioComponent {...props}/>)
        component.prop('actions')[1].props.onClick()
        expect(props.onClose.mock.calls.length).toBe(1)
    })

    it('should disable save correctly', () => {
        const handleSubmit = jest.fn()
        const onClose = jest.fn()
        const initialValues = null
        const props = {handleSubmit, onClose, savePortfolio, initialValues}
        hasEntitlement.mockReturnValue(false)
        const component = renderer.create(<AddEditPortfolioComponent {...props}/>)
        expect(component.toJSON()).toMatchSnapshot()
    })

    it('should render title correctly when portfolioId is set', () => {
        const handleSubmit = jest.fn()
        const onClose = jest.fn()
        const initialValues = {portfolioId: 123}
        handleSubmit.mockReturnValueOnce('savePortfolio')
        const props = {handleSubmit, onClose, savePortfolio, initialValues}
        const component = renderer.create(<AddEditPortfolioComponent {...props}/>)
        expect(component.toJSON()).toMatchSnapshot()
    })
})

describe('mapStateToProps', () => {
    const newPortfolio = {
        portfolioId: 2,
        managerId: 3
    }

    const validateEditPortfolioValues = (editPortfolioValues) => {
        expect(editPortfolioValues.portfolioId).toBeDefined()
        expect(editPortfolioValues.managerId).toBeDefined()
    }

    it('maps addEditPortfolio NEW PORTFOLIO', () => {
        const state = {
            portfolios: {editPortfolio: newPortfolio},
            form: {AddEditPortfolioForm: {}}
        }
        const props = mapStateToProps(state)
        expect(props.editPortfolio).toEqual(newPortfolio)
        expect(props.editPortfolio.portfolioId).toBe(2)
        validateEditPortfolioValues(props.initialValues)
    })
})
