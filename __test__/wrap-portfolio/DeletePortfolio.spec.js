import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../MockComponent'
import {shallow} from 'enzyme'
import { openConfirmationAlert, openSnackbar } from '../../src/components/redux-dialog/redux-dialog'
import { deletePortfolio } from '../../src/wrap-portfolio/PortfolioActions'
import { DeletePortfolioButton, DeletePortfolio } from '../../src/wrap-portfolio/DeletePortfolioButton'

jest.mock('../../src/wrap-portfolio/PortfolioActions', () => ({deletePortfolio: jest.fn}))
jest.mock('../../src/components/redux-dialog/redux-dialog', () => ({
    openDialog: jest.fn().mockImplementation(dialogName => dialogName),
    openSnackbar: jest.fn().mockImplementation(payload => payload),
    openConfirmationAlert: jest.fn().mockImplementation(payload => {
        return payload
    })
}))

jest.mock('react-redux', () => ({
    connect: () => (comp) => comp
}))

jest.mock('material-ui/svg-icons/action/delete', () => mockComponent('DeleteIcon'))

describe('DeletePortfolioButton', () => {
    it('should render correctly', () => {
        const e = {nativeEvent: {preventDefault: jest.fn()}, currentTarget: 'currentTarget'}
        const props = {
            row: {portfolioId: 1, active: 'YES'},
            openConfirmationAlert,
            openSnackbar,
            deletePortfolio
        }
        const component = renderer.create(<DeletePortfolio {...props} />)
        expect(component.toJSON()).toMatchSnapshot()
    })
    it('OnClick button when is active', () => {
        const props = {
            row: {portfolioId: 1, active: 'NO'},
            openConfirmationAlert,
            openSnackbar,
            deletePortfolio
        }
        openConfirmationAlert.mockClear()
        const e = {preventDefault: jest.fn(), currentTarget: 'currentTarget'}
        const component = shallow(<DeletePortfolio {...props} />)
        component.find({id: 'deletePortfolioId'}).prop('onClick')(e)
        expect(props.openConfirmationAlert).toHaveBeenCalled()
        expect(props.openConfirmationAlert.mock.calls[0][0].message).toBe('Are you sure you want to delete this Portfolio?')
    })

    it('OnClick button when is not active', () => {
        const props = {
            row: {portfolioId: 1, active: 'YES'},
            openConfirmationAlert,
            openSnackbar,
            deletePortfolio
        }
        openConfirmationAlert.mockClear()
        const e = {preventDefault: jest.fn(), currentTarget: 'currentTarget'}
        const component = shallow(<DeletePortfolio {...props} />)
        component.find({id: 'deletePortfolioId'}).prop('onClick')(e)
        expect(props.openConfirmationAlert).toHaveBeenCalled()
        expect(props.openConfirmationAlert.mock.calls[0][0].message).toBe('You can not delete this Portfolio')
    })
})
