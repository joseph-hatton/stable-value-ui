import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import { AddEditPortfolioAllocationContainer, mapStateToProps } from '../../src/wrap-portfolio/AddEditPortfolioAllocationContainer'

jest.mock('../../src/wrap-portfolio/PortfolioAllocationActions', () => ({
    saveAllocation: jest.fn(),
    loadAllocation: jest.fn(),
    resetAllocations: jest.fn()
}))

jest.mock('../../src/wrap-portfolio/AddEditPortfolioAllocationComponent', () => {
    return mockComponent('AddEditPortfolioAllocationComponent')
})

describe('AddEditPortfolioAllocationContainer Specs', () => {
    it('Renders Correctly and load  is called when is allocation are passed', () => {
        const params = {portfolioAllocationId: 1}
        const loadAllocation = jest.fn()
        const props = {params, loadAllocation}

        const AddEditPortfolioAllocationContainerRender = renderer.create(
            <AddEditPortfolioAllocationContainer {...props} />
        ).toJSON()

        expect(AddEditPortfolioAllocationContainerRender).toMatchSnapshot()
        expect(loadAllocation).toHaveBeenCalled()
    })

    it('Renders Correctly and load  is called when no allocation are passed', () => {
        const params = {portfolioAllocationId: null}
          const loadAllocation = jest.fn()
        const router = {}
        const loadPortfolioByContract = jest.fn()
        const resetAllocations = jest.fn()
        const props = {params, loadAllocation, loadPortfolioByContract, resetAllocations, router}

        const AddEditPortfolioAllocationContainerRender = renderer.create(
            <AddEditPortfolioAllocationContainer {...props} />
        ).toJSON()

        expect(AddEditPortfolioAllocationContainerRender).toMatchSnapshot()
        expect(loadAllocation).not.toHaveBeenCalled()
    })
})

describe('mapStateToProps - map actions', () => {
    it('Renders Correctly and load  is called when no allocation are passed', () => {
        const state = {
            portfolioAllocations: {
                editAllocation: {
                    contractId: 123,
                    portfolioAllocationId: '2',
                    portfolioEightRate: 34
                }
            },
            contractId: 123
        }
        const props = mapStateToProps(state)
        expect(props.initialValues).toBeDefined()
        expect(props.contractId).toBe(123)
    })
})
