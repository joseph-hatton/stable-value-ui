import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../MockComponent'
import { WrapPortfolioContainer } from '../../src/wrap-portfolio/WrapPortfolioContainer'
import { browserHistory } from 'react-router'
import { goToTab, setOrToggleListIt } from '../../src/components/common/LeftDrawerActions'
jest.mock('../../src/components/common/LeftDrawerActions', () => ({
  goToTab: jest.fn(),
  setOrToggleListIt: jest.fn()
}))
jest.mock('material-ui/Card', () => ({
  Card: mockComponent('Card')
}))

jest.mock('react-router', () => ({
    browserHistory: {
        push: jest.fn()
    }
}))

jest.mock('material-ui/Tabs', () => ({
  Tabs: mockComponent('Tabs'),
  Tab: mockComponent('Tab')
}))

describe('WrapPortfolioContainer Specs', () => {
    it('Renders Correctly ', () => {
        const props = {
          selectedTab: ' ',
          goToTab,
          setOrToggleListIt,
          wrapPortfolio: true,
         browserHistory
        }
        const WrapPortfolioContainerRender = renderer.create(
            <WrapPortfolioContainer {...props}/>
        ).toJSON()
        expect(WrapPortfolioContainerRender).toMatchSnapshot()
    })
})
