import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockConnect from '../../MockConnect'
import mockComponent from '../../MockComponent'
import {change} from 'redux-form'

import DateRangFilter, {ClearDateButton, actions, mapStateToProps} from '../../../src/components/common/DateRangFilter'

jest.mock('react-redux', () => mockConnect())
jest.mock('redux-form', () => ({change: jest.fn(), Field: mockComponent('Field')}))
jest.mock('material-ui/IconButton', () => ({IconButton: mockComponent('IconButton')}))
jest.mock('material-ui/svg-icons/content/clear', () => mockComponent('Clear'))

describe('DateRangFilter', () => {
  let props
  beforeEach(() => {
    props = {
      fromFieldName: 'fromFieldName',
      toFieldName: 'toFieldName',
      forms: {
        formId: {
          values: {
            fromFieldName: 'fromFieldName',
            toFieldName: 'toFieldName'
          }
        }
      },
      formId: 'formId'
    }
  })
  it('should render correctly', () => {
    expect(renderer.create(<DateRangFilter />).toJSON()).toMatchSnapshot()
  })
  it('should show from and to clear buttons if set', () => {
    expect(renderer.create(<DateRangFilter {...props}/>).toJSON()).toMatchSnapshot()
  })
  it('actions.clearDate should dispatch change', () => {
    const dispatch = jest.fn()
    change.mockReturnValue('change')
    actions.clearDate('formId', 'dateProp')(dispatch)
    expect(dispatch).toHaveBeenCalledWith('change')
    expect(change).toHaveBeenCalledWith('formId', 'dateProp', null)
  })
  it('mapStateToProps should map correctly', () => {
    expect(mapStateToProps({form: 'form'})).toEqual({forms: 'form'})
  })
})
describe('ClearDateButton', () => {
  let props
  beforeEach(() => {
    props = {
      formId: 'formId',
      clearDate: jest.fn(),
      dateProp: 'dateProp'
    }
  })
  it('should render correctly', () => {
    expect(renderer.create(<ClearDateButton {...props}/>).toJSON()).toMatchSnapshot()
  })
  it('on click of Clear Date Button should clear the date', () => {
    shallow(<ClearDateButton {...props}/>).prop('onClick')()
    expect(props.clearDate).toHaveBeenCalledWith('formId', 'dateProp')
  })
})
