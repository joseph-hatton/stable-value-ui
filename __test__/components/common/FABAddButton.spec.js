import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockConnect from '../../MockConnect'
import mockComponent from '../../MockComponent'

import FABAddButton from '../../../src/components/common/FABAddButton'

jest.mock('react-redux', () => mockConnect())
jest.mock('material-ui/svg-icons/content/add', () => mockComponent('ContentAdd'))
jest.mock('material-ui/FloatingActionButton', () => mockComponent('FloatingActionButton'))
jest.mock('../../../src/components/redux-dialog/redux-dialog', () => ({openDialog: 'openDialog'}))
jest.mock('material-ui/styles/colors', () => ({red500: 'red500'}))

describe('FABAddButton', () => {
  let props
  beforeEach(() => {
    props = {
      disabled: 'disabled',
      openDialog: jest.fn()
    }
  })
  it('should render correctly', () => {
    expect(renderer.create(<FABAddButton {...props} />)).toMatchSnapshot()
  })
  // it('onClick should call onClick when not disabled and onclick is a function', () => {
  //   props.onClick = jest.fn()
  //   shallow(<FABAddButton {...props} />).prop('onClick')()
  //   expect(props.onClick).toHaveBeenCalled()
  // })
})
