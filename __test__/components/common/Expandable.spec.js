import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from '../../MockComponent'
import localStorage from 'localStorage'

import Expandable from '../../../src/components/common/Expandable'

jest.mock('material-ui/Card', () => ({
  Card: mockComponent('Card'),
  CardHeader: mockComponent('CardHeader'),
  CardText: mockComponent('CardText')
}))

jest.mock('material-ui/svg-icons/content/clear', () => mockComponent('Clear'))

describe('Expandable', () => {
  let props
  beforeEach(() => {
    props = {
      title: 'title',
      icon: 'icon',
      id: 'id'
    }
  })
  it('should render correctly', () => {
    expect(renderer.create(<Expandable {...props} />)).toMatchSnapshot()
  })
  it('should render correctly when style and contentStyle are passed in', () => {
    props.style = 'style'
    props.contentStyle = {contentStyle: 'contentStyle'}
    expect(renderer.create(<Expandable {...props} />)).toMatchSnapshot()
  })
  it('handle expand change should store the passed in value and save it to state and the local storage', () => {
    const instance = shallow(<Expandable {...props} />).instance()
    instance.handleExpandChange('expanded')
    expect(instance.state.expanded).toEqual('expanded')
    expect(localStorage.getItem('id-expanded')).toEqual('expanded')
  })
  it('toggle should store the opposite of the passed in value and save it to state and the local storage', () => {
    const instance = shallow(<Expandable {...props} />).instance()
    instance.setState({expanded: true})
    instance.toggle()
    expect(instance.state.expanded).toBeFalsy()
    expect(localStorage.getItem('id-expanded')).toEqual('false')
  })
})
