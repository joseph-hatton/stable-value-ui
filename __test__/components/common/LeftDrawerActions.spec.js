import {setOrToggleListItem, goToTab} from '../../../src/components/common/LeftDrawerActions'

describe('LeftDrawerActions', () => {
  let dispatch
  beforeEach(() => {
    dispatch = jest.fn()
  })
  it('setOrToggleListItem should dispatch the correct action', () => {
    expect(setOrToggleListItem('text', 'value')).toEqual({
      type: 'SET_SUBLIST_VALUE',
      value: 'value',
      text: 'text'
    })
  })
  it('goToTab should dispatch the correct action', () => {
    expect(goToTab('tab')).toEqual({
      type: 'GO_TO_TAB',
      tab: 'tab'
    })
  })
})
