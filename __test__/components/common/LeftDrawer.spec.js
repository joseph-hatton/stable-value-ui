import React from 'react'
import mockComponent from '../../MockComponent'
import mockConnect from '../../MockConnect'
import LeftDrawer, { mapStatesToProps, createSubMenuItem } from '../../../src/components/common/LeftDrawer'
import menus from '../../../src/components/menus'
import renderer from 'react-test-renderer'
import ContractsInfoIcon from 'material-ui/svg-icons/action/info'
import {openConfirmationAlert} from '../../../src/components/redux-dialog'

jest.mock('material-ui/Drawer', () => {
  return mockComponent('Drawer')
})

jest.mock('material-ui/List', () => {
  return {
    List: mockComponent('List'),
    ListItem: mockComponent('ListItem')
  }
})

jest.mock('react-redux', () => mockConnect())

jest.mock('../../../src/components/redux-dialog', () => ({
  ConfirmationAlert: mockComponent('ConfirmationAlert')
}))

describe('Left Drawer', () => {
  let props
  beforeEach(() => {
    props = {
      username: 'username',
      roles: [{text: 'role1'}, {text: 'role2'}],
      onListItemClick: jest.fn(),
      onNestedListToggle: jest.fn(),
      sideBar: {text1: 'open'},
      menus: [
        {
          text: 'text1',
          leftIcon: 'icon1',
          initiallyOpen: 'initiallyOpen1'
        },
        {
          text: 'text2',
          leftIcon: 'icon2',
          initiallyOpen: 'initiallyOpen2',
          showSubMenus: () => true,
          subMenus: [
            {
              text: 'text3',
              leftIcon: 'icon3',
              initiallyOpen: 'initiallyOpen3',
              showSubMenu: () => true,
              showSubMenus: () => true,
              subMenus: [
                {
                  text: 'text4',
                  leftIcon: 'icon4',
                  initiallyOpen: 'initiallyOpen4',
                  showSubMenu: () => true
                }
              ]
            }
          ]
        }
      ]
    }
  })
  it('Renders correctly', () => {
    const component = renderer.create(<LeftDrawer {...props}/>).toJSON()
    expect(component).toMatchSnapshot()
  })
  it('Renders correctly when menus is empty', () => {
    props.menus = []
    const component = renderer.create(<LeftDrawer {...props}/>).toJSON()
    expect(component).toMatchSnapshot()
  })
  it('maps state to props', () => {
    const props = mapStatesToProps({sideBar: 'sideBar', test: 'test'})
    expect(props.appState).toEqual({sideBar: 'sideBar', test: 'test'})
    expect(props.sideBar).toEqual('sideBar')
  })
})
