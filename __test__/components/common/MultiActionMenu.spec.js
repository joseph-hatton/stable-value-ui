import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from '../../MockComponent'
import mockConnect from '../../MockConnect'
import {connect} from 'react-redux'

import MultiActionMenu, {MultiActionMenuLink} from '../../../src/components/common/MultiActionMenu'

jest.mock('react-redux', () => mockConnect())
jest.mock('material-ui', () => ({
  Popover: mockComponent('Popover'),
  Menu: mockComponent('Menu'),
  MenuItem: mockComponent('MenuItem')
}))
jest.mock('../../../src/components/redux-dialog/redux-dialog', () => ({
  openDialog: 'openDialog',
  closeDialog: 'closeDialog'
}))
jest.mock('react-router', () => ({
  browserHistory: {
    push: jest.fn()
  }
}))

describe('MultiActionMenu', () => {
  let props
  beforeEach(() => {
    props = {
      closeDialog: jest.fn(),
      openDialog: jest.fn(),
      dialog: {},
      dialogName: 'dialogName'
    }
  })
  it('should render correctly', () => {
    const comp = renderer.create(<MultiActionMenu {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('onRequestClose should close the dialog', () => {
    const comp = shallow(<MultiActionMenu {...props} />)
    comp.prop('onRequestClose')()
    expect(props.closeDialog).toHaveBeenCalledWith('dialogName')
  })
  it('mapStateToProps should map correctly', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[1][0]({
      dialog: 'dialog'
    })).toEqual({
      dialog: 'dialog'
    })
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[1][1]).toEqual({
      closeDialog: 'closeDialog',
      openDialog: 'openDialog'
    })
  })
})

describe('MultiActionMenuLink', () => {
  let props
  beforeEach(() => {
    props = {
      cell: 'cell',
      row: 'row',
      selections: jest.fn(),
      openDialog: jest.fn(),
      dialogName: 'dialogName'
    }
  })
  it('should render correctly', () => {
    const comp = renderer.create(<MultiActionMenuLink {...props} />)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('on onClick shoud prevent default, select contracts and open dialog', () => {
    const comp = shallow(<MultiActionMenuLink {...props} />)
    const e = {preventDefault: jest.fn(), currentTarget: 'currentTarget'}
    comp.prop('onClick')(e)
    expect(e.preventDefault).toHaveBeenCalled()
    expect(props.selections).toHaveBeenCalledWith('row')
    expect(props.openDialog).toHaveBeenCalledWith('dialogName', {anchorEl: 'currentTarget'})
  })
  it('actions should be setup correctly', () => {
    expect(connect).toHaveBeenCalledTimes(2)
    expect(connect.mock.calls[0][1]).toEqual({
      openDialog: 'openDialog'
    })
  })
})
