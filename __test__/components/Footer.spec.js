import React from 'react'
import Footer from '../../src/components/Footer'
import renderer from 'react-test-renderer'

describe('Footer Specs', () => {
  it('Footer Renders Correctly', () => {
    const footer = renderer.create(
      <Footer />
    ).toJSON()
    expect(footer).toMatchSnapshot()
  })
})
