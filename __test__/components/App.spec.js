import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import localStorage from 'localStorage'

import App, {mapGroupsToRoles} from '../../src/components/App'

jest.mock('react-redux', () => ({
  connect: jest.fn(() => jest.fn(component => component))
}))

jest.mock('material-ui/utils/withWidth', () => () => {
  return (component) => component
})

jest.mock('react-breadcrumbs', () => {
  return mockComponent('BreadCrumbs')
})

jest.mock('../../src/components/common/LeftDrawer', () => {
  return mockComponent('LeftDrawer')
})

jest.mock('../../src/components/common/Header', () => {
  return mockComponent('Header')
})

jest.mock('../../src/components/redux-dialog', () => {
  return {
    Snackbar: mockComponent('Snackbar'),
    ConfirmationAlert: mockComponent('ConfirmationAlert'),
    MaterialSpinner: mockComponent('MaterialSpinner')
  }
})

const createProps = (timedOut = false) => ({
  openConfirmationAlert: jest.fn(),
  timedOut
})

describe('App Specs', () => {
  it('App Renders Correctly', () => {
    const app = renderer.create(
      <App/>
    ).toJSON()
    expect(app).toMatchSnapshot()
  })

  it('timedOut prop calls open confirmation alert', () => {
    const props = createProps()
    const app = renderer.create(
      <App {...props}/>
    )
    location.reload = jest.fn()
    app.getInstance().componentWillReceiveProps({timedOut: true})
    expect(props.openConfirmationAlert).toHaveBeenCalled()
    expect(props.openConfirmationAlert.mock.calls[0][0].message).toEqual('Your session has terminated.  Please log back in.')
    expect(props.openConfirmationAlert.mock.calls[0][0].disableCancel).toBeTruthy()
    expect(location.reload).not.toHaveBeenCalled()
    props.openConfirmationAlert.mock.calls[0][0].onOk()
    expect(location.reload).toHaveBeenCalled()
  })

  it('Default State of the drawer is opened', () => {
    const wrapper = shallow(<App/>)
    expect(wrapper.state().navDrawerOpen).toEqual(true)
  })

  it('Default State of the drawer is closed if user has closed the drawer', () => {
    localStorage.setItem('left-nav-drawer-state', false)

    const wrapper = shallow(<App/>)
    expect(wrapper.state().navDrawerOpen).toEqual(false)
  })

  it('Drawer state is toggled', () => {
    localStorage.setItem('left-nav-drawer-state', false)
    const wrapper = shallow(<App/>)
    wrapper.instance().handleChangeRequestNavDrawer()

    expect(wrapper.state().navDrawerOpen).toEqual(true)
    expect(localStorage.getItem('left-nav-drawer-state')).toEqual('true')
  })
  it('mapGroupsToRoles should map to user friendly  text', () => {
    expect(mapGroupsToRoles([
      'app_stable_value_ops_admin',
      'app_stable_value_it_support',
      'app_stable_value_compliance',
      'app_stable_value_risk'
    ])).toEqual([
      {role: 'app_stable_value_ops_admin', text: 'Admin'},
      {role: 'app_stable_value_it_support', text: 'Support'},
      {role: 'app_stable_value_compliance', text: 'Compliance'},
      {role: 'app_stable_value_risk', text: 'Risk'}
    ])
  })
})
