import MonthAsTextFormField from '../../../src/components/forms/MonthAsTextFormField'
import createRefDataFormFieldComponent from '../../../src/components/forms/createRefDataFormFieldComponent'

jest.mock('../../../src/components/forms/createRefDataFormFieldComponent', () => jest.fn().mockReturnValue('component returned'))

describe('MonthAsTextFormField Specs', () => {
  it('creates ref data component', () => {
    expect(MonthAsTextFormField).toEqual('component returned')
    expect(createRefDataFormFieldComponent).toHaveBeenCalledWith('monthTypes', {floatingLabelText: 'Month', type: 'text'})
  })
})
