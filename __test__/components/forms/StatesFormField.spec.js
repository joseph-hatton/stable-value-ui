import StatesFormField from '../../../src/components/forms/StatesFormField'
import createRefDataFormFieldComponent from '../../../src/components/forms/createRefDataFormFieldComponent'

jest.mock('../../../src/components/forms/createRefDataFormFieldComponent', () => jest.fn().mockReturnValue('component returned'))

describe('StatesFormField Specs', () => {
  it('creates ref data component', () => {
    expect(StatesFormField).toEqual('component returned')
    expect(createRefDataFormFieldComponent).toHaveBeenCalledWith('states', {
      floatingLabelText: '* State',
      type: 'text'
    })
  })
})
