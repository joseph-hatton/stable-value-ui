import React from 'react'
import mockComponent from '../../MockComponent'
import CheckboxList from '../../../src/components/forms/CheckboxList'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'

jest.mock('redux-form', () => {
  return {
    Field: mockComponent('Field'),
    reduxForm: () => jest.fn()
  }
})
jest.mock('redux-form-material-ui', () => ({ Checkbox: mockComponent('Checkbox') }))

describe('CheckboxList Specs', () => {
  it('CheckboxList Renders Correctly', () => {
    const props = {
      title: 'some title',
      name: 'key',
      label: 'text',
      list: [{key: 'keyA', text: 'textA'}, {key: 'keyB', text: 'textB'}]
    }
    const component = renderer.create(
      <CheckboxList {...props}/>
    ).toJSON()
    expect(component).toMatchSnapshot()
  })

  it('CheckboxList Renders Correctly with more than 3 in list', () => {
    const props = {
      title: 'some title',
      name: 'key',
      label: 'text',
      list: [{key: 'keyA', text: 'textA'}, {key: 'keyB', text: 'textB'}, {key: 'keyC', text: 'textC'}, {key: 'keyD', text: 'textD'}]
    }
    const component = renderer.create(
      <CheckboxList {...props}/>
    ).toJSON()
    expect(component).toMatchSnapshot()
  })
})
