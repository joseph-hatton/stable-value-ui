import ContractTypesFormField from '../../../src/components/forms/ContractTypesFormField'
import createRefDataFormFieldComponent from '../../../src/components/forms/createRefDataFormFieldComponent'

jest.mock('../../../src/components/forms/createRefDataFormFieldComponent', () => jest.fn().mockReturnValue('component returned'))

describe('ContractTypesFormField Specs', () => {
  it('creates ref data component', () => {
    expect(ContractTypesFormField).toEqual('component returned')
    expect(createRefDataFormFieldComponent).toHaveBeenCalledWith('contractTypes', {floatingLabelText: '* Contract Type', type: 'text'})
  })
})
