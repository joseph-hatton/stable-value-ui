import React from 'react'
import mockComponent from '../../MockComponent'
import MaterialTextField from '../../../src/components/forms/MaterialTextField'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'

jest.mock('redux-form', () => {
  return {
    Field: mockComponent('Field'),
    reduxForm: () => jest.fn()
  }
})
jest.mock('redux-form-material-ui', () => ({ TextField: mockComponent('TextField') }))

describe('MaterialTextField Specs', () => {
  it('MaterialTextField Renders Correctly', () => {
    const props = {
      nullable: true
    }
    const component = renderer.create(
      <MaterialTextField {...props}/>
    ).toJSON()
    expect(component).toMatchSnapshot()
  })
})
