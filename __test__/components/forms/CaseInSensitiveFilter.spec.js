import CaseInSensitiveFilter from '../../../src/components/forms/CaseInSensitiveFilter'

describe('CaseInSensitiveFilter Specs', () => {
  it('matches different case', () => {
    expect(CaseInSensitiveFilter('one', 'OnE')).toEqual(true)
  })

  it('no match', () => {
    expect(CaseInSensitiveFilter('no', 'OnE')).toEqual(false)
  })

  it('matches partial different case', () => {
    expect(CaseInSensitiveFilter('ne', 'OnE')).toEqual(true)
  })

  it('matches spaces', () => {
    expect(CaseInSensitiveFilter(' ', 'a dog')).toEqual(true)
  })
})
