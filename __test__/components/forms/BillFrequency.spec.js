import BillFrequency from '../../../src/components/forms/BillFrequency'
import createRefDataFormFieldComponent from '../../../src/components/forms/createRefDataFormFieldComponent'

jest.mock('../../../src/components/forms/createRefDataFormFieldComponent', () => jest.fn().mockReturnValue('component returned'))

describe('BillFrequency Specs', () => {
  it('creates ref data component', () => {
    expect(BillFrequency).toEqual('component returned')
    expect(createRefDataFormFieldComponent).toHaveBeenCalledWith('billingFrequencyTypes', {floatingLabelText: 'Bill Frequency Type', type: 'text'})
  })
})
