import {exists, Required, MaxLength255, MaxLength, isInteger, toInteger, phoneNumber, IsValidZipCode, Email, NormalizePhone, toNumber, toNumberAcceptsNegative, FormatCurrency, FormatRate} from '../../../src/components/forms/Validators'

const existingString = 'something'
const errorRequired = 'Required'
const errorPhoneDigits = 'Must be 10 digit'
const errorZipCode = 'Must be a valid Zip code'

describe('Validators Specs', () => {
  describe('exists', () => {
    it('finds existing', () => expect(exists(existingString)).toEqual(true))
    it('finds space', () => expect(exists(' ')).toEqual(true))
    it('doesnt find undefined', () => expect(exists()).toEqual(false))
    it('doesnt find null', () => expect(exists(null)).toEqual(false))
    it('doesnt find empty string', () => expect(exists('')).toEqual(false))
  })

  describe('Required', () => {
    it('no error message with existing', () => expect(Required(existingString)).toBeUndefined())
    it('no error message with space', () => expect(Required(' ')).toBeUndefined())
    it('required error message for undefined', () => expect(Required()).toEqual(errorRequired))
    it('required error message for null', () => expect(Required(null)).toEqual(errorRequired))
    it('required error message for empty string', () => expect(Required('')).toEqual(errorRequired))
  })

  describe('MaxLength255', () => {
    it('no error message with empty string', () => expect(MaxLength255('')).toBeUndefined())
    it('no error message with short string', () => expect(MaxLength255(existingString)).toBeUndefined())
    it('no error message with string of 255', () => expect(MaxLength255('a'.repeat(255))).toBeUndefined())
    it('error message with string over 255', () => expect(MaxLength255('a'.repeat(256))).toBe('Cannot exceed 255 Characters'))
  })

  describe('MaxLength', () => {
    it('no error message with empty string', () => expect(MaxLength(2)('')).toBeUndefined())
    it('no error message with short string', () => expect(MaxLength(2)('a')).toBeUndefined())
    it('no error message with string of 2', () => expect(MaxLength(2)('aa')).toBeUndefined())
    it('no error message with string of 5', () => expect(MaxLength(5)('aaaa')).toBeUndefined())
    it('error message with string over 2', () => expect(MaxLength(2)('aaa')).toBe('Cannot exceed 2 Characters'))
    it('error message with string over 5', () => expect(MaxLength(5)('a'.repeat(8))).toBe('Cannot exceed 5 Characters'))
  })

  describe('isInteger', () => {
    it('no error message with integer number', () => expect(isInteger(1)).toBeUndefined())
    it('no error message with integer string', () => expect(isInteger('1')).toBeUndefined())
    it('no error message with undefined', () => expect(isInteger()).toBeUndefined())
    it('error message for string', () => expect(isInteger(existingString)).toEqual('Must be an Integer'))
  })

  describe('toInteger', () => {
    it('no error message with integer number', () => expect(toInteger(1)).toBe(1))
    it('no error message with integer string', () => expect(toInteger('12')).toBe(12))
    it('no error message with undefined', () => expect(toInteger()).toBeNull())
    it('error message for string', () => expect(toInteger(existingString)).toBeNull())
  })

  describe('phoneNumber', () => {
    it('no error message with 10 digit phone number in display format', () => expect(phoneNumber('(123) 456-7890')).toBeUndefined())
    it('error message with empty string', () => expect(phoneNumber('')).toBeUndefined())
    it('error message with some other string', () => expect(phoneNumber(existingString)).toBe(errorPhoneDigits))
    it('error message with 10 digit phone number with dashes', () => expect(phoneNumber('123-456-7890')).toBe(errorPhoneDigits))
  })

  describe('IsValidZipCode', () => {
    it('no error message with zip code in short format', () => expect(IsValidZipCode('12345')).toBeUndefined())
    it('no error message with zip code in long format', () => expect(IsValidZipCode('12345-6789')).toBeUndefined())
    it('error message with empty string', () => expect(IsValidZipCode('')).toBeUndefined())
    it('error message with some other string', () => expect(IsValidZipCode(existingString)).toBe(errorZipCode))
  })

  describe('Email', () => {
    it('no error message with valid email', () => expect(Email('something@something.com')).toBeUndefined())
    it('no error message with valid email with ampersand', () => expect(Email('some&thing@something.com')).toBeUndefined())
    it('error message with empty string', () => expect(Email('')).toBeUndefined())
    it('error message with some other string', () => expect(Email(existingString)).toBe('Invalid email'))
  })

  describe('NormalizePhone', () => {
    it('returns value if falsy', () => expect(NormalizePhone()).toBeUndefined())
    it('strips all non numbers', () => expect(NormalizePhone('abc123def456')).toBe('(123) 456'))
    it('normalizes with only 3 numbers', () => expect(NormalizePhone('abc123def')).toBe('(123) '))
    it('normalizes with only 7 numbers', () => expect(NormalizePhone('abc123def4567')).toBe('(123) 4567'))
    it('normalizes with all 10 numbers', () => expect(NormalizePhone('abc123def4567890')).toBe('(123) 456-7890'))
  })

  describe('toNumber', () => {
    it('returns null if falsy', () => expect(toNumber()).toBeNull())
    it('positive whole number', () => expect(toNumber('23')).toBe(23))
    it('positive number', () => expect(toNumber('23.2')).toBe(23.2))
    it('negative number becomes positive', () => expect(toNumber('-23.2')).toBe(23.2))
  })

  describe('toNumberAcceptsNegative', () => {
    it('returns null if falsy', () => expect(toNumberAcceptsNegative()).toBeNull())
    it('positive whole number', () => expect(toNumberAcceptsNegative('23')).toBe(23))
    it('positive number', () => expect(toNumberAcceptsNegative('23.2')).toBe(23.2))
    it('negative number', () => expect(toNumberAcceptsNegative('-23.2')).toBe(-23.2))
  })

  describe('FormatCurrency', () => {
    it('returns empty string if falsy', () => expect(FormatCurrency()).toBe(''))
    it('returns string representation of number', () => expect(FormatCurrency(1.23)).toBe('1.23'))
    it('number in thousands', () => expect(FormatCurrency(12345.67)).toBe('12,345.67'))
  })

  describe('FormatRate', () => {
    it('returns empty string if falsy', () => expect(FormatRate('0.0')()).toBe(''))
    it('returns string representation of number', () => expect(FormatRate('0.0')(1.23)).toBe('1.2'))
    it('number in thousands', () => expect(FormatRate('0.0')(12345.67)).toBe('12345.7'))
  })
})
