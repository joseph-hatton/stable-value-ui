import MarketValueEvents from '../../../src/components/forms/MarketValueEvents'
import createRefDataFormFieldComponent from '../../../src/components/forms/createRefDataFormFieldComponent'

jest.mock('../../../src/components/forms/createRefDataFormFieldComponent', () => jest.fn().mockReturnValue('component returned'))

describe('MarketValueEvents Specs', () => {
  it('creates ref data component', () => {
    expect(MarketValueEvents).toEqual('component returned')
    expect(createRefDataFormFieldComponent).toHaveBeenCalledWith('marketValueEvents', {floatingLabelText: 'Market Value Event', type: 'text'})
  })
})
