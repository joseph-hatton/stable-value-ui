import BenefitTiers from '../../../src/components/forms/BenefitTiers'
import createRefDataFormFieldComponent from '../../../src/components/forms/createRefDataFormFieldComponent'

jest.mock('../../../src/components/forms/createRefDataFormFieldComponent', () => jest.fn().mockReturnValue('component returned'))

describe('BenefitTiers Specs', () => {
  it('creates ref data component', () => {
    expect(BenefitTiers).toEqual('component returned')
    expect(createRefDataFormFieldComponent).toHaveBeenCalledWith('tiers', {floatingLabelText: 'Tier', type: 'text'})
  })
})
