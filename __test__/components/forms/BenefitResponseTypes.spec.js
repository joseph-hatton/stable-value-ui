import BenefitResponseTypes from '../../../src/components/forms/BenefitResponseTypes'
import createRefDataFormFieldComponent from '../../../src/components/forms/createRefDataFormFieldComponent'

jest.mock('../../../src/components/forms/createRefDataFormFieldComponent', () => jest.fn().mockReturnValue('component returned'))

describe('BenefitResponseTypes Specs', () => {
  it('creates ref data component', () => {
    expect(BenefitResponseTypes).toEqual('component returned')
    expect(createRefDataFormFieldComponent).toHaveBeenCalledWith('benefitResponseTypes', {floatingLabelText: 'Benefit Response Type', type: 'text'})
  })
})
