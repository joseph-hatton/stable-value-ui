import React from 'react'
import mockComponent from '../../MockComponent'
import MonthFormField from '../../../src/components/forms/MonthFormField'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'

jest.mock('redux-form', () => {
  return {
    Field: mockComponent('Field'),
    reduxForm: () => jest.fn()
  }
})
jest.mock('material-ui/MenuItem', () => mockComponent('MenuItem'))
jest.mock('redux-form-material-ui', () => ({ SelectField: mockComponent('SelectField') }))

describe('MonthFormField Specs', () => {
  it('MonthFormField Renders Correctly', () => {
    const props = {
      name: 'day field',
      floatingLabelText: '2018-01',
      onChange: jest.fn()
    }
    const component = renderer.create(
      <MonthFormField {...props}/>
    ).toJSON()
    expect(component).toMatchSnapshot()
  })
})
