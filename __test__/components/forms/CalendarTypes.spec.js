import CalendarTypes from '../../../src/components/forms/CalendarTypes'
import createRefDataFormFieldComponent from '../../../src/components/forms/createRefDataFormFieldComponent'

jest.mock('../../../src/components/forms/createRefDataFormFieldComponent', () => jest.fn().mockReturnValue('component returned'))

describe('CalendarTypes Specs', () => {
  it('creates ref data component', () => {
    expect(CalendarTypes).toEqual('component returned')
    expect(createRefDataFormFieldComponent).toHaveBeenCalledWith('calendarTypes', {floatingLabelText: 'Calendar Type', type: 'text'})
  })
})
