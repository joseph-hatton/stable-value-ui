import PayerFormField from '../../../src/components/forms/PayerFormField'
import createRefDataFormFieldComponent from '../../../src/components/forms/createRefDataFormFieldComponent'

jest.mock('../../../src/components/forms/createRefDataFormFieldComponent', () => jest.fn().mockReturnValue('component returned'))

describe('PayerFormField Specs', () => {
  it('creates ref data component', () => {
    expect(PayerFormField).toEqual('component returned')
    expect(createRefDataFormFieldComponent).toHaveBeenCalledWith('payerTypes', {floatingLabelText: '* Payer', type: 'text'})
  })
})
