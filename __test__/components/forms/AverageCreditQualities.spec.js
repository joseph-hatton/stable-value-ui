import AverageCreditQualities from '../../../src/components/forms/AverageCreditQualities'
import createRefDataFormFieldComponent from '../../../src/components/forms/createRefDataFormFieldComponent'

jest.mock('../../../src/components/forms/createRefDataFormFieldComponent', () => jest.fn().mockReturnValue('component returned'))

describe('AverageCreditQualities Specs', () => {
  it('creates ref data component', () => {
    expect(AverageCreditQualities).toEqual('component returned')
    expect(createRefDataFormFieldComponent).toHaveBeenCalledWith('averageCreditQualities', {floatingLabelText: 'Average Credit Quality', type: 'text'})
  })
})
