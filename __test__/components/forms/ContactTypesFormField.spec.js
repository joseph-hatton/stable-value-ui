import ContactTypesFormField from '../../../src/components/forms/ContactTypesFormField'
import createRefDataFormFieldComponent from '../../../src/components/forms/createRefDataFormFieldComponent'

jest.mock('../../../src/components/forms/createRefDataFormFieldComponent', () => jest.fn().mockReturnValue('component returned'))

describe('ContactTypesFormField Specs', () => {
  it('creates ref data component', () => {
    expect(ContactTypesFormField).toEqual('component returned')
    expect(createRefDataFormFieldComponent).toHaveBeenCalledWith('contactTypes', {floatingLabelText: '* Contact Type', type: 'text'})
  })
})
