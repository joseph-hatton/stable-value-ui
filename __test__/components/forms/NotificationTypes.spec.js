import NotificationTypes from '../../../src/components/forms/NotificationTypes'
import createRefDataFormFieldComponent from '../../../src/components/forms/createRefDataFormFieldComponent'

jest.mock('../../../src/components/forms/createRefDataFormFieldComponent', () => jest.fn().mockReturnValue('component returned'))

describe('NotificationTypes Specs', () => {
  it('creates ref data component', () => {
    expect(NotificationTypes).toEqual('component returned')
    expect(createRefDataFormFieldComponent).toHaveBeenCalledWith('notificationTypes', {floatingLabelText: 'Notification Type', type: 'text'})
  })
})
