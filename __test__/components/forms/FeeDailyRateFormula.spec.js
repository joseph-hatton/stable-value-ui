import FeeDailyRateFormula from '../../../src/components/forms/FeeDailyRateFormula'
import createRefDataFormFieldComponent from '../../../src/components/forms/createRefDataFormFieldComponent'

jest.mock('../../../src/components/forms/createRefDataFormFieldComponent', () => jest.fn().mockReturnValue('component returned'))

describe('FeeDailyRateFormula Specs', () => {
  it('creates ref data component', () => {
    expect(FeeDailyRateFormula).toEqual('component returned')
    expect(createRefDataFormFieldComponent).toHaveBeenCalledWith('feeDailyRateFormulae', {floatingLabelText: '* Fee Daily Rate Formula', type: 'text'})
  })
})
