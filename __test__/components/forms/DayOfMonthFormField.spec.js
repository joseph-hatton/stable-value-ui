import DaysOfMonthFormField from '../../../src/components/forms/DaysOfMonthFormField'
import createRefDataFormFieldComponent from '../../../src/components/forms/createRefDataFormFieldComponent'

jest.mock('../../../src/components/forms/createRefDataFormFieldComponent', () => jest.fn().mockReturnValue('component returned'))

describe('DaysOfMonthFormField Specs', () => {
  it('creates ref data component', () => {
    expect(DaysOfMonthFormField).toEqual('component returned')
    expect(createRefDataFormFieldComponent).toHaveBeenCalledWith('daysOfMonth', {floatingLabelText: 'Day', type: 'text'})
  })
})
