import React from 'react'
import mockComponent from '../../MockComponent'
import DayFormField from '../../../src/components/forms/DayFormField'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'

jest.mock('redux-form', () => {
  return {
    Field: mockComponent('Field'),
    reduxForm: () => jest.fn()
  }
})
jest.mock('material-ui/MenuItem', () => mockComponent('MenuItem'))
jest.mock('redux-form-material-ui', () => ({ SelectField: mockComponent('SelectField') }))

describe('DayFormField Specs', () => {
  it('DayFormField Renders Correctly', () => {
    const props = {
      year: 2018,
      month: 1,
      name: 'day field',
      floatingLabelText: '2018-01',
      onChange: jest.fn()
    }
    const component = renderer.create(
      <DayFormField {...props}/>
    ).toJSON()
    expect(component).toMatchSnapshot()
  })
})
