import YesNoContingent from '../../../src/components/forms/YesNoContingent'
import createRefDataFormFieldComponent from '../../../src/components/forms/createRefDataFormFieldComponent'

jest.mock('../../../src/components/forms/createRefDataFormFieldComponent', () => jest.fn().mockReturnValue('component returned'))

describe('YesNoContingent Specs', () => {
  it('creates ref data component', () => {
    expect(YesNoContingent).toEqual('component returned')
    expect(createRefDataFormFieldComponent).toHaveBeenCalledWith('yesNoTypes', {type: 'text'})
  })
})
