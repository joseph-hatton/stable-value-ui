import React from 'react'
import mockComponent from '../../MockComponent'
import BooleanSelectField from '../../../src/components/forms/BooleanSelectField'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'

jest.mock('redux-form', () => {
  return {
    Field: mockComponent('Field'),
    reduxForm: () => jest.fn()
  }
})
jest.mock('material-ui/MenuItem', () => mockComponent('MenuItem'))
jest.mock('redux-form-material-ui', () => ({ SelectField: mockComponent('SelectField') }))

describe('BooleanSelectField Specs', () => {
  it('BooleanSelectField Renders Correctly', () => {
    const component = renderer.create(
      <BooleanSelectField/>
    ).toJSON()
    expect(component).toMatchSnapshot()
  })
})
