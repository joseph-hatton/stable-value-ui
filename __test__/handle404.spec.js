import React from 'react'
import mockComponent from './MockComponent'
import renderer from 'react-test-renderer'

import Handle404 from '../src/handle404'

jest.mock('material-ui/styles/colors', () => 'red')

describe('handle404', () => {
  it('should render correctly', () => {
    const component = renderer.create(<Handle404 />)
    expect(component.toJSON()).toMatchSnapshot()
  })
})
