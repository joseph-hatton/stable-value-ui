import React from 'react'
import moment from 'moment'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import Clear from 'material-ui/svg-icons/content/clear'
import {shallow} from 'enzyme'
import {reset, change} from 'redux-form'
import ContractPicker from '../../src/contracts/ContractPicker'
import {
  TransactionTypes,
  TransactionDetailsTypes,
  AmountComparators
} from '../../src/transactions/TransactionReferenceDataFields'

import {
  TransactionFilters, actions, ClearDateButton, mapStateToProps
} from '../../src/transactions/TransactionFilters'

jest.mock('react-redux', () => {
  const mockConnectChildFunction = jest.fn().mockImplementation(component => <component/>)
  return {
    connect: jest.fn().mockImplementation(() => mockConnectChildFunction)
  }
})
jest.mock('../../src/contracts/ContractPicker', () => mockComponent('ContractPicker'))
jest.mock('redux-form', () => {
  return {
    Field: mockComponent('Field'),
    reduxForm: jest.fn().mockImplementation(() => jest.fn()),
    reset: jest.fn(),
    change: jest.fn()
  }
})
jest.mock('material-ui/svg-icons/content/clear', () => mockComponent('Clear'))
jest.mock('material-ui', () => ({
  RaisedButton: mockComponent('RaisedButton'),
  IconButton: mockComponent('IconButton')
}))
jest.mock('material-ui/Card', () => ({Card: mockComponent('Card')}))
jest.mock('../../src/transactions/TransactionReferenceDataFields', () => ({
  TransactionTypes: mockComponent('TransactionTypes'),
  TransactionDetailsTypes: mockComponent('TransactionDetailsTypes'),
  AmountComparators: mockComponent('AmountComparators')
}))

describe('Transaction Filters Form', () => {
  it('renders Filters form', () => {
    const component = renderer.create(<TransactionFilters clearAll={actions.clearAll} clearDate={actions.clearDate}
                                                          active="amount"/>)

    expect(component.toJSON()).toMatchSnapshot()
  })

  it('renders clear buttons if from and to dates are selected in Filters form', () => {
    const from = moment().toDate()
    const to = moment().subtract('month', 5).toDate()

    const component = renderer.create(<TransactionFilters clearAll={actions.clearAll} clearDate={actions.clearDate}
                                                          from={from} to={to}/>)

    expect(component.toJSON()).toMatchSnapshot()
  })

  it('calls clear Date action when clear icon is clicked ', () => {
    const clearDate = jest.fn()

    const wrapper = shallow(<ClearDateButton clearDate={clearDate} dateProp="from"/>)

    wrapper.find({id: 'from-date-clear'}).prop('onClick')('from')

    expect(clearDate).toHaveBeenCalledWith('from')
  })
})

describe('Filter Actions', () => {
  it('Clear All Clears the form', () => {
    const dispatch = jest.fn()

    actions.clearAll()(dispatch)

    expect(reset).toHaveBeenCalledWith('TransactionFiltersForm')
    expect(dispatch).toHaveBeenCalled()
  })

  it('Clear Date Clears the date property', () => {
    const dispatch = jest.fn()

    actions.clearDate('from')(dispatch)

    expect(change).toHaveBeenCalledWith('TransactionFiltersForm', 'from', null)
    expect(dispatch).toHaveBeenCalled()
  })
})

describe('mapStateToProps', () => {
  it('maps from and to Date from form values to props', () => {
    const to = moment().toDate()
    const from = moment().subtract('day', 5).toDate()

    const props = mapStateToProps({
      form: {
        TransactionFiltersForm: {
          values: {from, to}
        }
      }
    })

    expect(props).toEqual({from, to})
  })
})
