import React from 'react'
import mockComponent from '../MockComponent'
import _ from 'lodash'
import {connect} from 'react-redux'
import {feeAmountChanged} from '../../src/transactions/TransactionActions'

import FeeDistributionColumns, {AmountCell} from '../../src/transactions/FeeDistributionColumns'

jest.mock('../../src/components/grid', () => {
  return {
    EditableCell: mockComponent('EditableCell')
  }
})

jest.mock('../../src/transactions/TransactionActions', () => {
  return {
    feeAmountChanged: jest.fn()
  }
})

jest.mock('react-redux', () => ({
  connect: jest.fn((mapStateToProps, actions) => {
    return (component) => component
  })
}))

const expectedColumns = [
  'invoiceNumber',
  'beginningBalance',
  'amount',
  'adjustment',
  'endingBalance'
]

const getRenderer = (field) => FeeDistributionColumns.find(column => column.field === field).renderer

describe('Fee Distribution Columns', () => {
  it('Defines all Fee Distribution columns', () => {
    expect(FeeDistributionColumns.length).toEqual(5)
    expect(_.map(FeeDistributionColumns, 'field')).toEqual(expectedColumns)
  })

  it('renders "Next" if invoice number empty', () => {
    const invoiceNumberRenderer = getRenderer('invoiceNumber')

    expect(invoiceNumberRenderer(null)).toEqual('Next')
  })

  it('renders beginningBalance and endingBalance Dollar Amount Renderer', () => {
    expect(getRenderer('beginningBalance')(20003.566)).toEqual('20,003.57')
    expect(getRenderer('endingBalance')(20003.566)).toEqual('20,003.57')
  })

  it('renders amount and adjustment with AmountCell', () => {
    expect(getRenderer('amount')(20003.566, {transactionType: 'FEE_RECEIPT'})).toEqual(<AmountCell editable={true}
                                                                                                   field="amount"
                                                                                                   row={{'transactionType': 'FEE_RECEIPT'}}
                                                                                                   value={20003.566}/>)
    expect(getRenderer('adjustment')(20003.566, {transactionType: 'FEE_RECEIPT'})).toEqual(<AmountCell editable={false}
                                                                                                       field="adjustment"
                                                                                                       row={{'transactionType': 'FEE_RECEIPT'}}
                                                                                                       value={20003.566}/>)

    expect(connect).toHaveBeenCalledWith(null, {feeAmountChanged})
  })
})
