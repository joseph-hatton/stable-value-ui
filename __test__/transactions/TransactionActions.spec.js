import moment from 'moment'
import {openDialog, openConfirmationAlert} from '../../src/components/redux-dialog/redux-dialog'
import http, {httpErrorMessage} from '../../src/actions/http'

import {
  isFeeTransaction,
  newTransaction,
  createSelectTransactionPayload,
  selectTransaction,
  openTransaction,
  deleteTransaction,
  clearTransaction,
  saveTransaction,
  getBalanceForEffectiveDate,
  loadFeeDistributions,
  createNewFeeDistribution,
  feeReceiptAmountChanged,
  feeAmountChanged,
  handleFeeTransaction,
  saveWithdrawalTransaction
} from '../../src/transactions/TransactionActions'

jest.mock('../../src/components/redux-dialog/redux-dialog', () => ({
  openDialog: jest.fn().mockImplementation(dialogName => dialogName),
  openSnackbar: jest.fn().mockImplementation(payload => payload),
  openConfirmationAlert: jest.fn().mockImplementation(payload => {
    return payload
  })
}))

const mockBalances = () => {
  return Promise.resolve({
    results: [{
      beginningBookValue: 500
    }]
  })
}

const mockTransaction = () => {
  return Promise.resolve({
    'feeDistributions': [{
      'feeDistributionId': 898,
      'invoiceId': 2097,
      'invoiceNumber': 70,
      'amount': 36383.35,
      'createdDate': '2014-04-21T10:43:06.000Z',
      'createdId': 'system',
      'modifiedDate': '2014-04-21T10:43:06.000Z',
      'modifiedId': 'system',
      'version': 1,
      'beginningBalance': 36383.35,
      'adjustment': 0,
      'endingBalance': 0
    }],
    'transactionId': 1716,
    'contractId': 527,
    'transactionType': 'FEE_RECEIPT',
    'effectiveDate': '2013-05-01T00:00:00.000Z',
    'detail': null,
    'amount': 36383.35,
    'comments': 'jasper data migration',
    'locked': true,
    'createdDate': '2014-04-21T10:43:06.000Z',
    'createdId': 'system',
    'modifiedDate': '2014-04-21T10:43:06.000Z',
    'modifiedId': 'system',
    'version': 1,
    'adjustmentDate': null,
    'adjustmentTransactionId': null,
    'contractNumber': 'FLUOR-0113-01',
    'shortPlanName': 'Fluor',
    'balanceId': 257091,
    'beginningBookValue': 71496720.98
  })
}

const mockOpenInvoices = () => {
  return Promise.resolve({
    'total': 1,
    'pageNumber': null,
    'pageSize': null,
    'results': [{
      'invoiceId': 6073,
      'contractId': 527,
      'invoiceNumber': 1284,
      'invoiceDate': '2018-01-02T00:00:00.000Z',
      'dueDate': '2018-02-06T00:00:00.000Z',
      'beginningBalance': 43769.39,
      'endingBalance': 38904.02,
      'feesAccrued': 38904.02,
      'feesReceived': 0,
      'adjustments': 0,
      'outstandingBalance': 38904.02,
      'beginDate': '2017-10-01T00:00:00.000Z',
      'endDate': '2017-12-31T00:00:00.000Z'
    }]
  })
}

jest.mock('../../src/actions/http', () => {
  const httpMethod = (url, body, messageConfig) => Promise.resolve(({url, body, messageConfig}))
  return {
    delete: jest.fn().mockImplementation(httpMethod),
    post: jest.fn().mockImplementation((url, body, messageConfig) => {
      return Promise.resolve({
        ...body,
        transactionId: 100
      })
    }),
    put: jest.fn().mockImplementation((url, body, messageConfig) => {
      return Promise.resolve({...body})
    }),
    get: jest.fn().mockImplementation((url, query) => {
      if (url.includes('/balances')) {
        return mockBalances()
      }
      if (url.includes('/transactions')) {
        return mockTransaction()
      }
      if (url.includes('/open-invoices')) {
        return mockOpenInvoices()
      }
      if (url.includes('/prior-withdrawals')) {
        const {contractId, amount} = query
        if (contractId === 500 && amount === 100000) {
          return Promise.resolve({confirmationRequired: false})
        }
        if (contractId === 500 && amount === 200000) {
          return Promise.resolve({
            confirmationRequired: true,
            message: 'Total Withdrawal % is equal to or more than the Corridor Option Limits-Annual. Are you sure you want to create this transaction?'
          })
        }
      }
    }),
    httpErrorMessage: jest.fn()
  }
})

jest.mock('redux-form', () => ({
  change: jest.fn().mockImplementation((formName, field, value) => ({
    type: 'FIELD_CHANGED',
    formName,
    field,
    value
  }))
}))

describe('Fee Transaction', () => {
  it('Returns true if transaction is FEE_RECEIPT or FEE_ADJUSTMENT', () => {
    expect(isFeeTransaction({transactionType: 'FEE_RECEIPT'})).toBe(true)
    expect(isFeeTransaction({transactionType: 'FEE_ADJUSTMENT'})).toBe(true)
  })

  it('Returns false if transaction is non FEE', () => {
    expect(isFeeTransaction({transactionType: 'DEPOSIT'})).toBe(false)
    expect(isFeeTransaction({transactionType: 'WITHDRAWAL'})).toBe(false)
    expect(isFeeTransaction({transactionType: 'ADJUSTMENT'})).toBe(false)
    expect(isFeeTransaction({transactionType: 'TERMINATION'})).toBe(false)
  })
})

const createGetState = (state) => jest.fn().mockImplementation(() => state)

const referenceData = {
  transactionTypes: [
    {id: 'DEPOSIT', text: 'Deposit'},
    {id: 'ADJUSTMENT', text: 'Adjustment'},
    {id: 'TERMINATION', text: 'Termination'},
    {id: 'WITHDRAWAL', text: 'Withdrawal'}
  ]
}

afterEach(() => {
  openDialog.mockClear()
  openConfirmationAlert.mockClear()
  http.post.mockClear()
  http.put.mockClear()
  http.delete.mockClear()
})

describe('Transaction Actions', () => {
  it('dispatches to New Transaction and opens Add Edit Transaction Dialog', () => {
    const dispatch = jest.fn()
    const getState = createGetState({referenceData})

    newTransaction('DEPOSIT')(dispatch, getState)

    expect(dispatch).toHaveBeenCalledTimes(2)

    const newTransactionDispatchCall = dispatch.mock.calls[0]
    expect(newTransactionDispatchCall[0]).toEqual({
      type: 'NEW_TRANSACTION',
      transactionType: 'DEPOSIT',
      transactionTypeText: 'Deposit'
    })

    const openDialogDispatchCall = dispatch.mock.calls[1]
    expect(openDialogDispatchCall[0]).toEqual('AddEditTransaction')
    expect(openDialog).toHaveBeenCalledWith('AddEditTransaction')
  })

  it('createSelectTransactionPayload creates transaction payload', () => {
    const transaction = {
      transactionId: 499,
      transactionType: 'TERMINATION',
      amount: 9999999999
    }
    const payload = createSelectTransactionPayload(transaction, {referenceData})

    expect(payload).toEqual({
      transactionId: 499,
      transactionType: 'TERMINATION',
      amount: 9999999999,
      transactionTypeText: 'Termination'
    })
  })

  it('selectTransaction opens AddEditTransaction Dialog -- old', () => {
    const dispatch = jest.fn()

    selectTransaction(dispatch, createGetState({transactions: {addEditTransaction: {transactionType: 'TERMINATION'}}}))

    expect(dispatch).toHaveBeenCalledWith('AddEditTransaction')
    expect(openDialog).toHaveBeenCalledWith('AddEditTransaction')
  })

  it('selectTransaction opens AddEditTransaction Dialog', () => {
    const dispatch = jest.fn()

    selectTransaction(dispatch)

    expect(dispatch).toHaveBeenCalledWith('AddEditTransaction')
    expect(openDialog).toHaveBeenCalledWith('AddEditTransaction')
  })

  it('opens transaction dialog for double click', () => {
    const dispatch = jest.fn()
    const transaction = {transactionType: 'DEPOSIT', transactionId: 101, amount: 500}
    const getState = createGetState({referenceData: {transactionTypes: [{id: 'DEPOSIT', text: 'Deposit'}]}})

    openTransaction(transaction)(dispatch, getState)

    expect(dispatch).toHaveBeenCalledWith({
      type: 'SELECT_TRANSACTION',
      payload: {
        ...transaction,
        transactionTypeText: 'Deposit'
      }
    })

    expect(dispatch).toHaveBeenCalledWith('AddEditTransaction')
    expect(openDialog).toHaveBeenCalledWith('AddEditTransaction')
  })

  it('Delete Transaction opens confirmation alert', () => {
    const dispatch = jest.fn()
    const getState = createGetState({referenceData, transactions: {addEditTransaction: {transactionId: 401}}})

    deleteTransaction(dispatch, getState)

    expect(dispatch).toHaveBeenCalled()
    expect(openConfirmationAlert).toHaveBeenCalled()

    const dispatchPayload = dispatch.mock.calls[0][0]
    const openConfirmationAlertPayload = openConfirmationAlert.mock.calls[0][0]

    expect(dispatchPayload).toEqual(openConfirmationAlertPayload) // Mock version of openConfirmationAlert returns input param
    expect(openConfirmationAlertPayload.message).toBe('Are you sure you want to delete this Transaction?')

    // Test Cancel Dialog
    dispatch.mockClear()
    openConfirmationAlertPayload.onCancel()

    expect(dispatch).toHaveBeenCalledWith(clearTransaction())

    // Test Ok calls delete transaction
    dispatch.mockClear()
    openConfirmationAlertPayload.onOk()

    expect(dispatch).toHaveBeenCalled()
    const dispatchPayload2 = dispatch.mock.calls[0][0]
    expect(dispatchPayload2.type).toEqual('DELETE_TRANSACTION')
    return dispatchPayload2.payload.then(result => {
      expect(result).toEqual(
        {
          url: 'https://stable-value-api.rgare.net/api/v1/transactions/401',
          body: null,
          messageConfig: {
            errorMessage: httpErrorMessage,
            successMessage: 'Transaction Deleted Successfully'
          }
        }
      )
    })
  })

  it('Saves New Transaction', () => {
    const dispatch = jest.fn()
    const newTransaction = {
      contractId: 200,
      effectiveDate: moment().toDate(),
      detail: 'EMPLOYER_ACTIVITY',
      amount: 9999,
      comments: 'Test Transaction'
    }
    const getState = createGetState({
      transactions: {
        addEditTransaction: {
          transactionType: 'WITHDRAWAL'
        }
      },
      form: {
        AddEditTransactionForm: {
          values: newTransaction
        }
      }
    })

    saveTransaction()(dispatch, getState)

    expect(dispatch).toHaveBeenCalled()

    const dispatchPayload = dispatch.mock.calls[0][0]

    expect(dispatchPayload.type).toBe('SAVE_TRANSACTION')

    return dispatchPayload.payload.then(result => {
      expect(result).toEqual({...newTransaction, transactionId: 100, amount: -9999, transactionType: 'WITHDRAWAL'})

      const expectedMessageConfig = {
        errorMessage: httpErrorMessage, successMessage: 'Transaction Saved Successfully'
      }
      expect(http.post)
        .toHaveBeenCalledWith('https://stable-value-api.rgare.net/api/v1/transactions',
          {
            ...newTransaction,
            amount: -9999,
            transactionType: 'WITHDRAWAL'
          },
          expectedMessageConfig,
          'AddEditTransaction')
    })
  })

  it('Updates existing Transaction', () => {
    const dispatch = jest.fn()
    const depositTransaction = {
      transactionId: 303,
      contractId: 202,
      effectiveDate: moment().toDate(),
      detail: 'ADDITIONAL_DEPOSIT',
      amount: 1000,
      comments: 'Test Transaction',
      transactionType: 'DEPOSIT'
    }
    const getState = createGetState({
      transactions: {
        addEditTransaction: {
          transactionType: 'DEPOSIT'
        }
      },
      form: {
        AddEditTransactionForm: {
          values: depositTransaction
        }
      }
    })

    saveTransaction()(dispatch, getState)

    expect(dispatch).toHaveBeenCalled()

    const dispatchPayload = dispatch.mock.calls[0][0]

    expect(dispatchPayload.type).toBe('SAVE_TRANSACTION')

    return dispatchPayload.payload.then(result => {
      expect(result).toEqual(depositTransaction)

      const expectedMessageConfig = {
        errorMessage: httpErrorMessage, successMessage: 'Transaction Saved Successfully'
      }
      expect(http.put)
        .toHaveBeenCalledWith('https://stable-value-api.rgare.net/api/v1/transactions/303',
          depositTransaction,
          expectedMessageConfig,
          'AddEditTransaction')
    })
  })

  it('Gets balance for given effective date for Termination Transaction', () => {
    const now = moment().toDate()
    const getState = createGetState({
      transactions: {
        addEditTransaction: {transactionType: 'TERMINATION'}
      },
      form: {
        AddEditTransactionForm: {
          values: {
            contractId: 404,
            effectiveDate: now
          }
        }
      }
    })
    const dispatch = jest.fn()

    return getBalanceForEffectiveDate()(dispatch, getState).then(() => {
      const expectedEffectiveDateParam = moment(now).startOf('day').format()
      expect(http.get).toHaveBeenCalledWith(`https://stable-value-api.rgare.net/api/v1/contracts/404/balances?effectiveDate=${expectedEffectiveDateParam}`)
      expect(dispatch).toHaveBeenCalledWith({
        field: 'amount',
        formName: 'AddEditTransactionForm',
        type: 'FIELD_CHANGED',
        value: -500
      })
    })
  })
})

describe('Fee Transactions', () => {
  it('Loads Existing Fee transaction along with Open Invoice', () => {
    const getState = createGetState({
      form: {AddEditTransactionForm: {values: {transactionId: 101, contractId: 501}}},
      transactions: {feeDistributions: []}
    })

    const dispatch = jest.fn()

    return loadFeeDistributions()(dispatch, getState).then(() => {
      expect(dispatch).toHaveBeenCalledWith({
        'payload': [{
          'adjustment': 0,
          'amount': 36383.35,
          'beginningBalance': 36383.35,
          'createdDate': '2014-04-21T10:43:06.000Z',
          'createdId': 'system',
          'endingBalance': 0,
          'feeDistributionId': 898,
          'invoiceId': 2097,
          'invoiceNumber': 70,
          'modifiedDate': '2014-04-21T10:43:06.000Z',
          'modifiedId': 'system',
          'version': 1
        }, {
          'adjustment': 0,
          'amount': 0,
          'beginningBalance': 38904.02,
          'endingBalance': 38904.02,
          'invoiceId': 6073,
          'invoiceNumber': 1284
        }],
'type': 'SET_FEE_DISTRIBUTIONS'
      })
    })
  })

  it('Creates New Fee Distributions for Open Invoice', () => {
    const getState = createGetState({
      form: {AddEditTransactionForm: {values: {contractId: 502}}}
    })

    const dispatch = jest.fn()

    return createNewFeeDistribution()(dispatch, getState).then(() => {
      expect(dispatch).toHaveBeenCalledWith({
        'payload': [{
          'adjustment': 0,
          'amount': 0,
          'beginningBalance': 38904.02,
          'endingBalance': 38904.02,
          'invoiceId': 6073,
          'invoiceNumber': 1284
        }, {
          'isFutureInvoice': true,
          'adjustment': 0,
          'amount': 0,
          'beginningBalance': 0,
          'endingBalance': 0,
          'invoiceNumber': null
        }],
        'type': 'SET_FEE_DISTRIBUTIONS'
      })
    })
  })

  const openInvoice = {
    'adjustment': 0,
    'amount': 0,
    'beginningBalance': 38904.02,
    'endingBalance': 38904.02,
    'invoiceId': 6073,
    'invoiceNumber': 1284
  }

  const nextInvoice = {'adjustment': 0, 'amount': 0, 'beginningBalance': 0, 'endingBalance': 0, 'invoiceNumber': null}

  it('Distributes the exact Fee Received to Open Invoice', () => {
    const feeDistributions = [openInvoice, nextInvoice]
    const getState = createGetState({transactions: {feeDistributions}})

    const dispatch = jest.fn()

    feeReceiptAmountChanged(38904.02)(dispatch, getState)

    expect(dispatch).toHaveBeenCalledWith({
      'payload': [{
        'adjustment': 0,
        'amount': 38904.02,
        'beginningBalance': 38904.02,
        'endingBalance': 0,
        'invoiceId': 6073,
        'invoiceNumber': 1284
      }, nextInvoice],
      'type': 'SET_FEE_DISTRIBUTIONS'
    })
  })

  it('Distributes the partial Fee Received to open Invoice', () => {
    const feeDistributions = [openInvoice, nextInvoice]
    const getState = createGetState({transactions: {feeDistributions}})

    const dispatch = jest.fn()

    feeReceiptAmountChanged(30000)(dispatch, getState)

    expect(dispatch).toHaveBeenCalledWith({
      'payload': [{
        'adjustment': 0,
        'amount': 30000,
        'beginningBalance': 38904.02,
        'endingBalance': 8904.019999999997,
        'invoiceId': 6073,
        'invoiceNumber': 1284
      }, nextInvoice],
      'type': 'SET_FEE_DISTRIBUTIONS'
    })
  })

  it('Distributes the extra Fee Received to Next Invoice', () => {
    const feeDistributions = [openInvoice, nextInvoice]
    const getState = createGetState({transactions: {feeDistributions}})

    const dispatch = jest.fn()

    feeReceiptAmountChanged(40000)(dispatch, getState)

    expect(dispatch).toHaveBeenCalledWith({
      'payload': [{
        'adjustment': 0,
        'amount': 38904.02,
        'beginningBalance': 38904.02,
        'endingBalance': 0,
        'invoiceId': 6073,
        'invoiceNumber': 1284
      }, {
        'adjustment': 0,
        'amount': 1095.9800,
        'beginningBalance': 0,
        'endingBalance': -1095.98,
        'invoiceNumber': null
      }],
'type': 'SET_FEE_DISTRIBUTIONS'
    })

    expect(dispatch).toHaveBeenCalledWith({
      type: 'FIELD_CHANGED',
      formName: 'AddEditTransactionForm',
      field: 'feeDistributionTotal',
      value: 40000
    })
  })

  it('Applies the Fee Receipt Amount directly to the Open Invoice', () => {
    const feeDistributions = [{...openInvoice, transactionType: 'FEE_RECEIPT'}, nextInvoice]
    const getState = createGetState({transactions: {feeDistributions}})

    const dispatch = jest.fn()

    feeAmountChanged('amount', 505.75, feeDistributions[0])(dispatch, getState)

    expect(dispatch).toHaveBeenCalledWith({
      'payload': [{
        'adjustment': 0,
        'amount': 505.75,
        'beginningBalance': 38904.02,
        'endingBalance': 38398.27,
        'invoiceId': 6073,
        'invoiceNumber': 1284,
        'transactionType': 'FEE_RECEIPT'
      }, nextInvoice],
      'type': 'SET_FEE_DISTRIBUTIONS'
    })
    expect(dispatch).toHaveBeenCalledWith({
      type: 'FIELD_CHANGED',
      formName: 'AddEditTransactionForm',
      field: 'feeDistributionTotal',
      value: 505.75
    })
  })

  it('Applies the Fee Adjustment Amount to Open Invoice', () => {
    const feeDistributions = [{...openInvoice, transactionType: 'FEE_ADJUSTMENT'}, nextInvoice]
    const getState = createGetState({transactions: {feeDistributions}})

    const dispatch = jest.fn()

    feeAmountChanged('amount', 505.75, openInvoice)(dispatch, getState)

    expect(dispatch).toHaveBeenCalledWith({
      'payload': [{
        'adjustment': 0,
        'amount': 505.75,
        'beginningBalance': 38904.02,
        'endingBalance': 39409.77,
        'invoiceId': 6073,
        'invoiceNumber': 1284,
        'transactionType': 'FEE_ADJUSTMENT'
      }, nextInvoice],
      'type': 'SET_FEE_DISTRIBUTIONS'
    })

    expect(dispatch).toHaveBeenCalledTimes(1)
  })

  it('Applies the Fee Adjustment Amount to Next Invoice', () => {
    const fd = {...nextInvoice, transactionType: 'FEE_ADJUSTMENT'}

    const getState = createGetState({transactions: {feeDistributions: [fd]}})

    const dispatch = jest.fn()

    feeAmountChanged('adjustment', 505.75, fd)(dispatch, getState)

    expect(dispatch).toHaveBeenCalledWith({
      'payload': [{
        ...fd,
        'adjustment': 505.75,
        'endingBalance': 505.75
      }],
      'type': 'SET_FEE_DISTRIBUTIONS'
    })
  })

  it('it discards fee distributions with 0 amount from Fee Receipt Transaction Payload', () => {
    const updatedFeeDistribution = {
      'adjustment': 0,
      'amount': 30000,
      'beginningBalance': 38904.02,
      'endingBalance': 8904.019999999997,
      'invoiceId': 6073,
      'invoiceNumber': 1284
    }
    const payload = {transactionType: 'FEE_RECEIPT'}

    handleFeeTransaction(payload,
      [updatedFeeDistribution, nextInvoice])

    expect(payload.feeDistributions).toEqual([updatedFeeDistribution])
  })

  it('it discards fee distributions with 0 adjustments from Fee Adjustment Transaction Payload', () => {
    const updatedFeeDistribution = {
      'adjustment': 505.75,
      'amount': 0,
      'beginningBalance': 38904.02,
      'endingBalance': 39409.77,
      'invoiceId': 6073,
      'invoiceNumber': 1284
    }
    const payload = {transactionType: 'FEE_ADJUSTMENT'}

    handleFeeTransaction(payload,
      [updatedFeeDistribution, nextInvoice])

    expect(payload.feeDistributions).toEqual([updatedFeeDistribution])
  })

  describe('Withdrawal transactions', () => {
    it('Saves transaction without Confirmation, if the withdrawal is within limits', () => {
      const newWithdrawal = {amount: 100000, contractId: 500}
      const getState = createGetState({form: {AddEditTransactionForm: {values: newWithdrawal}}})
      const dispatch = jest.fn()

      return saveWithdrawalTransaction()(dispatch, getState).then(() => {
        expect(openConfirmationAlert).toHaveBeenCalledTimes(0)
      })
    })

    it('User is prompted for confirmation if the contract has corrOptionLimitsAnnual limit and withdrawal is off limits', () => {
      const newWithdrawal = {amount: 200000, contractId: 500}
      const getState = createGetState({form: {AddEditTransactionForm: {values: newWithdrawal}}})
      const dispatch = jest.fn()

      return saveWithdrawalTransaction()(dispatch, getState).then(() => {
        expect(openConfirmationAlert).toHaveBeenCalled()
        const openConfirmationAlertPayload = openConfirmationAlert.mock.calls[0][0]
        expect(dispatch).toHaveBeenCalledWith({
          message: 'Total Withdrawal % is equal to or more than the Corridor Option Limits-Annual. Are you sure you want to create this transaction?',
          onOk: openConfirmationAlertPayload.onOk
        })
      })
    })
  })
})
