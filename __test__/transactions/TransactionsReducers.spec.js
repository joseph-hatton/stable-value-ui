import reduce from '../../src/transactions/TransactionsReducers'
import moment from 'moment'

const initState = {addEditTransaction: null}

describe('transactions reducer', () => {
  it('maps default empty state', () => {
    const newState = reduce(null, {type: 'UNKNOWN'})

    expect(newState).toEqual(null)
  })

  it('maps New Transaction to state', () => {
    const transactionType = 'TERMINATION'
    const transactionTypeText = 'Termination'

    const newState = reduce(null, {
      type: 'NEW_TRANSACTION',
      transactionType,
      transactionTypeText
    })

    expect(newState).toEqual({
      addEditTransaction: {
        transactionType,
        transactionTypeText
      }
    })
  })

  it('maps selected Transaction to state', () => {
    const transaction = {
      transactionType: 'TERMINATION',
      transactionTypeText: 'Termination',
      effectiveDate: moment().toDate()
    }

    const newState = reduce(null, {
      type: 'SELECT_TRANSACTION',
      payload: transaction
    })

    expect(newState).toEqual({
      addEditTransaction: transaction
    })
  })

  it('clears selected Transaction from state', () => {
    const transaction = {
      transactionType: 'TERMINATION',
      transactionTypeText: 'Termination',
      effectiveDate: moment().toDate()
    }

    const newState = reduce(initState, {
      type: 'CLEAR_TRANSACTION'
    })

    expect(newState).toEqual({addEditTransaction: null, feeDistributions: [], refresh: false})
  })

  it('sets the Fee Distribution', () => {
    const openInvoice = {
      'adjustment': 0,
      'amount': 0,
      'beginningBalance': 38904.02,
      'endingBalance': 38904.02,
      'invoiceId': 6073,
      'invoiceNumber': 1284
    }

    const nextInvoice = {
      'isFutureInvoice': true,
      'adjustment': 0,
      'amount': 0,
      'beginningBalance': 0,
      'endingBalance': 0,
      'invoiceNumber': null
    }

    const newState = reduce({addEditTransaction: {transactionType: 'FEE_ADJUSTMENT'}}, {
      type: 'SET_FEE_DISTRIBUTIONS',
      payload: [openInvoice, nextInvoice]
    })

    expect(newState).toEqual({
      addEditTransaction: {transactionType: 'FEE_ADJUSTMENT'},
      feeDistributions: [
        {...openInvoice, transactionType: 'FEE_ADJUSTMENT'},
        {...nextInvoice, transactionType: 'FEE_ADJUSTMENT'}
      ]
    })
  })

  it('sets the transaction summary when transaction grid data is loaded', () => {
    const transactionSummary = {
      'deposits': 11989953522.15,
      'withdrawals': -2065362433.97,
      'adjustments': -10617471.04,
      'feeReceipts': 65367157.15,
      'feeAdjustments': -1651.49,
      'terminations': -349202530.73,
      'amountBvPercent': 5407.87,
      'withdrawalBvPercent': 1788.98,
      'depositBvPercent': 3179.13
    }

    const newState = reduce(initState, {
      type: 'GRID_DATA_UPDATED',
      id: 'Transactions',
      response: {transactionSummary}
    })

    expect(newState).toEqual({addEditTransaction: null, transactionSummary})
  })

  it('ignores the data load for other grids', () => {
    const newState = reduce(initState, {
      type: 'GRID_DATA_UPDATED',
      id: 'Companies',
      response: {companies: jest.fn()}
    })

    expect(newState).toEqual({addEditTransaction: null})
  })

  it('clears the selected transaction after delete is complete', () => {
    const newState = reduce({addEditTransaction: {transactionId: 1001}}, {
      type: 'DELETE_TRANSACTION_FULFILLED'
    })

    expect(newState).toEqual({
      addEditTransaction: null,
      feeDistributions: [],
      refresh: true
    })
  })

  it('clears refresh status after transaction grid is refreshed.', () => {
    const newState = reduce({refresh: true}, {
      type: 'TRANSACTION_REFRESHED'
    })

    expect(newState.refresh).toBe(false)
  })

  it('set refresh flag true after save transaction is fulfilled', () => {
    const newState = reduce({}, {
      type: 'SAVE_TRANSACTION_FULFILLED'
    })

    expect(newState.refresh).toBe(true)
  })
})
