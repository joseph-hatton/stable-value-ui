import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import moment from 'moment'
import {blue500, red500} from 'material-ui/styles/colors'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import ContentAdd from 'material-ui/svg-icons/content/add'
import NavigationClose from 'material-ui/svg-icons/navigation/close'
import DepositIcon from 'material-ui/svg-icons/action/system-update-alt'
import {Card} from 'material-ui'
import {IntlProvider} from 'react-intl'
import {shallow} from 'enzyme'
import TransactionFilters from '../../src/transactions/TransactionFilters'
import AddEditTransaction from '../../src/transactions/AddEditTransaction'
import canEdit from '../../src/transactions/canEdit'
import {hasEntitlement} from '../../src/entitlements'

import {Transactions, mapStateToProps, formatValues} from '../../src/transactions/Transactions'

jest.mock('react-redux', () => {
  const mockConnectChildFunction = jest.fn().mockImplementation(component => <component/>)
  return {
    connect: jest.fn().mockImplementation(() => mockConnectChildFunction)
  }
})
jest.mock('material-ui/styles/colors', () => ({blue500: 'blue', red500: 'blue'}))
jest.mock('material-ui', () => ({
  Card: mockComponent('Card')
}))
jest.mock('material-ui/svg-icons/editor/mode-edit', () => mockComponent('EditIcon'))
jest.mock('material-ui/svg-icons/action/delete', () => mockComponent('DeleteIcon'))
jest.mock('material-ui/svg-icons/content/add', () => mockComponent('ContentAdd'))
jest.mock('material-ui/svg-icons/content/filter-list', () => mockComponent('Filter'))
jest.mock('material-ui/svg-icons/navigation/close', () => mockComponent('NavigationClose'))
jest.mock('material-ui/svg-icons/action/system-update-alt', () => mockComponent('DepositIcon'))
jest.mock('react-intl', () => ({
  IntlProvider: mockComponent('IntlProvider')
}))

jest.mock('../../src/components/grid', () => ({
  Grid: mockComponent('Grid'),
  CreateGridConfig: jest.fn(),
  GridContextMenu: mockComponent('GridContextMenu'),
  DESCENDING: 'desc'
}))
jest.mock('../../src/components/fab-dial', () => ({
  SpeedDial: mockComponent('SpeedDial'),
  SpeedDialItem: mockComponent('SpeedDialItem')
}))
jest.mock('../../src/styles/styles', () => ({
  fab: {
    position: 'fixed',
    right: 50,
    bottom: 50
  }
}))
jest.mock('../../src/transactions/TransactionFilters', () => mockComponent('TransactionFilters'))
jest.mock('../../src/transactions/TransactionSummary', () => mockComponent('TransactionSummary'))
jest.mock('../../src/components/common/Expandable', () => mockComponent('Expandable'))
jest.mock('../../src/transactions/TransactionColumns', () => [])
jest.mock('../../src/transactions/TransactionActions', () => ({
  newTransaction: jest.fn(),
  selectTransaction: jest.fn(),
  deleteTransaction: jest.fn(),
  transactionsRefreshed: jest.fn(),
  openTransaction: jest.fn()
}))
jest.mock('../../src/transactions/AddEditTransaction', () => mockComponent('AddEditTransaction'))
jest.mock('../../src/transactions/canEdit', () => jest.fn())
jest.mock('../../src/entitlements', () => ({
  hasEntitlement: jest.fn()
}))

describe('Transactions Grid', () => {
  beforeEach(() => {
    canEdit.mockReturnValue(true)
    hasEntitlement.mockReturnValue(true)
  })

  it('should hide fab when not allowed', () => {
    hasEntitlement.mockReturnValue(false)
    const newTransaction = jest.fn()

    const component = renderer.create(<Transactions newTransaction={newTransaction}/>)

    expect(component.toJSON()).toMatchSnapshot()
  })

  it('Renders Transaction Grid without AddEditTransaction dialog if no Transaction is Selected', () => {
    const newTransaction = jest.fn()

    const component = renderer.create(<Transactions newTransaction={newTransaction}/>)

    expect(component.toJSON()).toMatchSnapshot()
  })

  it('Renders Transaction Grid without AddEditTransaction dialog if Fee Transaction is Selected', () => {
    const newTransaction = jest.fn()
    const addEditTransaction = {
      transactionType: 'FEE_RECEIPT'
    }

    const component = renderer.create(<Transactions newTransaction={newTransaction}
                                                    addEditTransaction={addEditTransaction}/>)

    expect(component.toJSON()).toMatchSnapshot()
  })

  it('Renders Transaction Grid with AddEditTransaction dialog if non-Fee Transaction is Selected', () => {
    const newTransaction = jest.fn()
    const addEditTransaction = {
      transactionType: 'DEPOSIT'
    }

    const component = renderer.create(<Transactions newTransaction={newTransaction}
                                                    addEditTransaction={addEditTransaction}/>)

    expect(component.toJSON()).toMatchSnapshot()
  })

  it('Refreshes Transaction Grid when refresh props is received as true', () => {
    const newTransaction = jest.fn()
    const transactionsRefreshed = jest.fn()
    const ref = {
      refresh: jest.fn()
    }

    const wrapper = shallow(<Transactions newTransaction={newTransaction}
                                          transactionsRefreshed={transactionsRefreshed}/>)
    wrapper.instance().getGridRef = () => ref
    wrapper.update()

    wrapper.setProps({refresh: true})

    expect(ref.refresh).toHaveBeenCalled()
    expect(transactionsRefreshed).toHaveBeenCalled()
  })

  const testNewTransactionMenu = (label, id) => {
    const newTransaction = jest.fn()

    const wrapper = shallow(<Transactions newTransaction={newTransaction}/>)
    wrapper.find({label}).prop('onClick')()

    expect(newTransaction).toHaveBeenCalledWith(id)
  }

  it('Creates new Deposit Transaction when user clicks on Deposit Transaction Menu', () => {
    testNewTransactionMenu('Deposit', 'DEPOSIT')
  })

  it('Creates new Withdrawal Transaction when user clicks on Withdrawal Transaction Menu', () => {
    testNewTransactionMenu('Withdrawal', 'WITHDRAWAL')
  })

  it('Creates new Book Value Adjustment Transaction when user clicks on Book Value Adjustment Transaction Menu', () => {
    testNewTransactionMenu('Book Value Adjustment', 'ADJUSTMENT')
  })

  it('Creates new Termination Transaction when user clicks on Termination Transaction Menu', () => {
    testNewTransactionMenu('Termination', 'TERMINATION')
  })

  it('Creates new Fee Receipt Transaction when user clicks on Fee Receipt Transaction Menu', () => {
    testNewTransactionMenu('Fee Receipt', 'FEE_RECEIPT')
  })

  it('Creates new Fee Adjustment Transaction when user clicks on Fee Adjustment Transaction Menu', () => {
    testNewTransactionMenu('Fee Adjustment', 'FEE_ADJUSTMENT')
  })
})

describe('mapStateToProps', () => {
  it('maps selected filters and selected transaction to Props', () => {
    const state = {
      form: {
        TransactionFiltersForm: {
          values: {
            contractId: 404,
            transactionType: 'DEPOSIT'
          }
        }
      },
      transactions: {
        addEditTransaction: {
          transactionId: 402,
          transactionType: 'FEE'
        }
      }
    }

    const props = mapStateToProps(state)

    expect(props.filters).toEqual(state.form.TransactionFiltersForm.values)
    expect(props.addEditTransaction).toEqual(state.transactions.addEditTransaction)
  })

  it('maps empty filters to Props', () => {
    const state = {
      form: {},
      transactions: {}
    }

    const props = mapStateToProps(state)

    expect(props).toEqual({filters: {}})
  })
})

describe('Format Filter Values', () => {
  it('Removes empty filter values', () => {
    const filters = formatValues({from: null})

    expect(filters).toEqual({})
  })

  it('Formats from and to filter dates in YYYY-MM-DDT00:00:00.000Z format', () => {
    const from = moment('2017-11-20').toDate()
    const to = moment('2018-01-20').toDate()

    const filters = formatValues({from, to})

    expect(filters).toEqual({'from': '2017-11-20T00:00:00Z', 'to': '2018-01-20T00:00:00Z'})
  })

  it('Sets the amount on filter if comparator is defined', () => {
    const filters = formatValues({amountComparator: '>', amount: '45000'})

    expect(filters).toEqual({
      amountComparator: '>',
      amount: 45000.00
    })
  })

  it('Removes the amount and amountComparator if the amount is invalid', () => {
    const filters = formatValues({amountComparator: '>', amount: '45_000'})

    expect(filters).toEqual({})
  })

  it('Removes the amount if amountComparator is not set', () => {
    const filters = formatValues({contractId: 404, amount: '45000'})

    expect(filters).toEqual({contractId: 404})
  })
})
