import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'
import {connect} from 'react-redux'
import moment from 'moment'
import {reduxForm} from 'redux-form'
import ReduxDialog from '../../src/components/redux-dialog/redux-dialog'
import ContractPicker from '../../src/contracts/ContractPicker'
import NumberField from '../../src/contracts/NumberField'
import FeeDistributionGrid from '../../src/transactions/FeeDistributionGrid'
import {isFeeTransaction, saveTransaction, saveWithdrawalTransaction} from '../../src/transactions/TransactionActions'
import canEdit from '../../src/transactions/canEdit'

import {
  AddEditTransaction,
  validateEffectiveDate,
  mapStateToProps,
  actions,
  AddEditTransactionForm,
  validateTransaction
} from '../../src/transactions/AddEditTransaction'

jest.mock('react-redux', () => {
  const mockConnectChildFunction = jest.fn().mockImplementation(component => <component/>)
  return {
    connect: jest.fn().mockImplementation(() => mockConnectChildFunction)
  }
})

jest.mock('redux-form', () => {
  return {
    Field: mockComponent('Field'),
    reduxForm: jest.fn().mockImplementation(() => jest.fn())
  }
})

jest.mock('material-ui/styles/colors', () => ({orange500: 'orange'}))

jest.mock('material-ui', () => ({
  RaisedButton: mockComponent('RaisedButton')
}))

jest.mock('../../src/watchLists/watchListAlert/WatchListAlert', () => mockComponent('WatchListAlert'))
jest.mock('../../src/components/redux-dialog/redux-dialog', () => mockComponent('ReduxDialog'))
jest.mock('../../src/components/redux-dialog/ConfirmationAlert', () => mockComponent('ConfirmationAlert'))
jest.mock('../../src/contracts/ContractPicker', () => mockComponent('ContractPicker'))
jest.mock('../../src/contracts/NumberField', () => mockComponent('NumberField'))
jest.mock('../../src/transactions/FeeDistributionGrid', () => mockComponent('FeeDistributionGrid'))
jest.mock('../../src/transactions/TransactionReferenceDataFields', () => ({
  TransactionDetailsTypesMap: {
    DEPOSIT: mockComponent('DEPOSIT_DETAILS'),
    WITHDRAWAL: mockComponent('WITHDRAWAL_DETAILS'),
    ADJUSTMENT: mockComponent('ADJUSTMENT_DETAILS'),
    TERMINATION: mockComponent('WITHDRAWAL_DETAILS')
  }
}))
jest.mock('../../src/transactions/TransactionActions', () => ({
  isFeeReceipt: (transactionType) => transactionType === 'FEE_RECEIPT',
  isFeeAdjustment: (transactionType) => transactionType === 'FEE_ADJUSTMENT',
  saveTransaction: jest.fn(),
  saveWithdrawalTransaction: jest.fn(),
  clearTransaction: jest.fn(),
  getBalanceForEffectiveDate: jest.fn(),
  isFeeTransaction: jest.fn().mockImplementation((addEditTransaction) => addEditTransaction.transactionType === 'FEE_RECEIPT' || addEditTransaction.transactionType === 'FEE_ADJUSTMENT')
}))
jest.mock('../../src/styles/styles', () => ({
  textAreaFont: {
    fontSize: '14px'
  }
}))
jest.mock('../../src/contracts/ContractActions', () => ({loadContract: jest.fn()}))
jest.mock('../../src/transactions/canEdit', () => jest.fn())

afterEach(() => {
  jest.restoreAllMocks()
})

describe('Add Edit Transactions Form', () => {
  beforeEach(() => {
    canEdit.mockReturnValue(true)
  })

  it('Renders correctly when permissions are denied', () => {
    const newTransaction = {
      transactionType: 'DEPOSIT',
      transactionTypeText: 'Deposit'
    }
    canEdit.mockReturnValue(false)

    const loadContract = jest.fn()
    const handleSubmit = jest.fn()
    handleSubmit.mockReturnValueOnce('saveTransaction')
    const checkWatchList = jest.mock()

    const component = renderer.create(<AddEditTransaction saveTransaction={saveTransaction}
                                                          clearTransaction="clearTransactionFunction"
                                                          getBalanceForEffectiveDate="getBalanceForEffectiveDateFunction"
                                                          handleSubmit={handleSubmit} loadContract={loadContract}
                                                          checkWatchList={checkWatchList}
                                                          addEditTransaction={newTransaction}/>)
    expect(component.toJSON()).toMatchSnapshot()
  })

  it('Renders New Deposit Transaction', () => {
    const newTransaction = {
      transactionType: 'DEPOSIT',
      transactionTypeText: 'Deposit' // Dialog Displays Transaction Type Text
    }

    const loadContract = jest.fn()
    const handleSubmit = jest.fn()
    handleSubmit.mockReturnValueOnce('saveTransaction')
    const checkWatchList = jest.mock()

    const component = renderer.create(<AddEditTransaction saveTransaction={saveTransaction}
                                                          clearTransaction="clearTransactionFunction"
                                                          getBalanceForEffectiveDate="getBalanceForEffectiveDateFunction"
                                                          handleSubmit={handleSubmit} loadContract={loadContract}
                                                          checkWatchList={checkWatchList}
                                                          addEditTransaction={newTransaction}/>)
    expect(component.toJSON()).toMatchSnapshot()
    expect(handleSubmit).toHaveBeenCalledWith(saveTransaction)
  })

  it('Renders Adjustment date for Adjustment Transaction', () => {
    const newTransaction = {
      transactionType: 'ADJUSTMENT',
      transactionTypeText: 'Adjustment' // Dialog Displays Transaction Type Text
    }

    const loadContract = jest.fn()
    const handleSubmit = jest.fn()
    handleSubmit.mockReturnValueOnce('saveTransaction')
    const checkWatchList = jest.mock()

    const component = renderer.create(<AddEditTransaction saveTransaction={saveTransaction}
                                                          clearTransaction="clearTransactionFunction"
                                                          getBalanceForEffectiveDate="getBalanceForEffectiveDateFunction"
                                                          handleSubmit={handleSubmit} loadContract={loadContract}
                                                          checkWatchList={checkWatchList}
                                                          addEditTransaction={newTransaction}/>)
    expect(component.toJSON()).toMatchSnapshot()
    expect(handleSubmit).toHaveBeenCalledWith(saveTransaction)
  })

  it('Renders Existing Deposit Transaction', () => {
    const existingTransaction = {
      transactionId: 100,
      transactionType: 'DEPOSIT',
      transactionTypeText: 'Deposit' // Dialog Displays Transaction Type Text
    }

    const loadContract = jest.fn()
    const handleSubmit = jest.fn()
    handleSubmit.mockReturnValueOnce('saveTransaction')
    const checkWatchList = jest.mock()

    const component = renderer.create(<AddEditTransaction saveTransaction={saveTransaction}
                                                          clearTransaction="clearTransactionFunction"
                                                          getBalanceForEffectiveDate="getBalanceForEffectiveDateFunction"
                                                          handleSubmit={handleSubmit} loadContract={loadContract}
                                                          checkWatchList={checkWatchList}
                                                          addEditTransaction={existingTransaction}/>)
    expect(component.toJSON()).toMatchSnapshot()
    expect(handleSubmit).toHaveBeenCalledWith(saveTransaction)
  })

  it('Disables Modifications for Termination Transaction', () => {
    const existingTransaction = {
      transactionId: 100,
      transactionType: 'TERMINATION',
      transactionTypeText: 'Termination' // Dialog Displays Transaction Type Text
    }

    const loadContract = jest.fn()
    const handleSubmit = jest.fn()
    handleSubmit.mockReturnValueOnce('saveTransaction')
    const checkWatchList = jest.mock()

    const component = renderer.create(<AddEditTransaction saveTransaction={saveTransaction}
                                                          clearTransaction="clearTransactionFunction"
                                                          getBalanceForEffectiveDate="getBalanceForEffectiveDateFunction"
                                                          handleSubmit={handleSubmit} loadContract={loadContract}
                                                          checkWatchList={checkWatchList}
                                                          addEditTransaction={existingTransaction}/>)
    expect(component.toJSON()).toMatchSnapshot()
    expect(handleSubmit).toHaveBeenCalledWith(saveTransaction)
  })

  it('Renders Fee Transaction', () => {
    const existingTransaction = {
      transactionId: 100,
      transactionType: 'FEE_RECEIPT',
      transactionTypeText: 'Fee Receipt' // Dialog Displays Transaction Type Text
    }

    const contract = {contractId: 200}
    const loadContract = jest.fn()
    const handleSubmit = jest.fn()
    handleSubmit.mockReturnValueOnce('saveTransaction')
    const checkWatchList = jest.mock()

    const component = renderer.create(<AddEditTransaction saveTransaction={saveTransaction}
                                                          clearTransaction="clearTransactionFunction"
                                                          getBalanceForEffectiveDate="getBalanceForEffectiveDateFunction"
                                                          handleSubmit={handleSubmit} loadContract={loadContract}
                                                          checkWatchList={checkWatchList}
                                                          addEditTransaction={existingTransaction} contract={contract}/>
    )

    expect(component.toJSON()).toMatchSnapshot()
    expect(handleSubmit).toHaveBeenCalledWith(saveTransaction)
  })

  it('Renders Fee Adjustment Transaction', () => {
    const existingTransaction = {
      transactionId: 100,
      transactionType: 'FEE_ADJUSTMENT',
      transactionTypeText: 'Fee Adjustment' // Dialog Displays Transaction Type Text
    }

    const contract = {contractId: 200}
    const loadContract = jest.fn()
    const handleSubmit = jest.fn()
    handleSubmit.mockReturnValueOnce('saveTransaction')
    const checkWatchList = jest.mock()

    const component = renderer.create(<AddEditTransaction saveTransaction={saveTransaction}
                                                          clearTransaction="clearTransactionFunction"
                                                          getBalanceForEffectiveDate="getBalanceForEffectiveDateFunction"
                                                          handleSubmit={handleSubmit} loadContract={loadContract}
                                                          checkWatchList={checkWatchList}
                                                          addEditTransaction={existingTransaction} contract={contract}/>
    )

    expect(component.toJSON()).toMatchSnapshot()
    expect(handleSubmit).toHaveBeenCalledWith(saveTransaction)
  })

  it('Disables Modifications for Existing Deposit Transaction effective last month', () => {
    const existingTransaction = {
      effectiveDate: moment().subtract('month', 1).toDate(),
      transactionId: 100,
      transactionType: 'DEPOSIT',
      transactionTypeText: 'Deposit' // Dialog Displays Transaction Type Text
    }

    const loadContract = jest.fn()
    const handleSubmit = jest.fn()
    handleSubmit.mockReturnValueOnce('saveTransaction')
    const checkWatchList = jest.mock()

    const component = renderer.create(<AddEditTransaction saveTransaction={saveTransaction}
                                                          clearTransaction="clearTransactionFunction"
                                                          getBalanceForEffectiveDate="getBalanceForEffectiveDateFunction"
                                                          handleSubmit={handleSubmit} loadContract={loadContract}
                                                          checkWatchList={checkWatchList}
                                                          addEditTransaction={existingTransaction}/>)
    expect(component.toJSON()).toMatchSnapshot()
    expect(handleSubmit).toHaveBeenCalledWith(saveTransaction)
  })

  const contractId = 550
  let loadContract
  beforeEach(() => {
    loadContract = jest.fn()
  })
})

const renderAddEditTransactionWithFeeTransaction = (newTransaction, feeReceiptAmountChanged) => {
  const handleSubmit = jest.fn()
  handleSubmit.mockReturnValueOnce('saveTransaction')
  const checkWatchList = jest.mock()

  return renderer.create(<AddEditTransaction contractId={560}
                                             saveTransaction={saveTransaction}
                                             clearTransaction={jest.fn()}
                                             getBalanceForEffectiveDate={jest.fn()}
                                             handleSubmit={handleSubmit} loadContract={jest.fn()}
                                             addEditTransaction={newTransaction}
                                             checkWatchList={checkWatchList}
                                             feeReceiptAmountChanged={feeReceiptAmountChanged}
  />)
}

describe('Fee Transactions', () => {
  it('calls feeReceiptAmountChanged when Fee Receipt Amount changed', () => {
    const feeReceiptAmountChanged = jest.fn()

    const newTransaction = {
      transactionType: 'FEE_RECEIPT',
      transactionTypeText: 'Fee Receipt'
    }

    const AddEditTransactionComponent
      = renderAddEditTransactionWithFeeTransaction(newTransaction, feeReceiptAmountChanged)

    AddEditTransactionComponent.getInstance().transactionAmountChanged({target: {value: 500}})

    expect(feeReceiptAmountChanged).toHaveBeenCalledWith(500)
  })

  it('returns without processing invalid amount', () => {
    const feeAdjustmentAmountChanged = jest.fn()
    const feeReceiptAmountChanged = jest.fn()

    const newTransaction = {
      transactionType: 'FEE_RECEIPT',
      transactionTypeText: 'Fee Receipt'
    }

    const AddEditTransactionComponent
      = renderAddEditTransactionWithFeeTransaction(newTransaction, feeReceiptAmountChanged, feeAdjustmentAmountChanged)

    AddEditTransactionComponent.getInstance().transactionAmountChanged({target: {value: '29*&&.00'}})

    expect(feeReceiptAmountChanged).toHaveBeenCalledTimes(0)
    expect(feeAdjustmentAmountChanged).toHaveBeenCalledTimes(0)
  })
})

describe('Add Edit Transaction Utilities', () => {
  it('validates Transaction Effective date of today as valid', () => {
    const today = moment().toDate()

    expect(validateEffectiveDate(today)).toBeUndefined()
  })

  it('returns validation error message for Transaction Effective date of last month', () => {
    const today = moment().subtract('month', 1).toDate()

    expect(validateEffectiveDate(today)).toEqual('Effective date must be in open month or future.')
  })

  it('returns connected Add Edit Transaction Form', () => {
    expect(connect).toHaveBeenCalledWith(mapStateToProps, actions)
  })

  it('creates redux form', () => {
    expect(reduxForm).toHaveBeenCalledWith({form: 'AddEditTransactionForm', validate: validateTransaction})
  })

  it('validates FeeReceipt Amount to equal to feeDistributionTotal', () => {
    const error = validateTransaction({amount: 100.456755}, {
      addEditTransaction: {
        transactionType: 'FEE_RECEIPT'
      },
      feeDistributionTotal: 100.4567000001
    })

    expect(error).toEqual({})
  })

  it('defaults amounts to 0 before validating FeeReceipt Amount to be equal to feeDistributionTotal', () => {
    const error = validateTransaction({}, {
      addEditTransaction: {
        transactionType: 'FEE_RECEIPT'
      }
    })

    expect(error).toEqual({})
  })

  it('doesnt validate non fee receipt transactions', () => {
    const error = validateTransaction({}, {
      addEditTransaction: {
        transactionType: 'DEPOSIT'
      }
    })

    expect(error).toEqual({})
  })

  it('returns error when FeeReceipt Amount is Not Equal to feeDistributionTotal', () => {
    const error = validateTransaction({amount: 100.456755}, {
      addEditTransaction: {
        transactionType: 'FEE_RECEIPT'
      },
      feeDistributionTotal: 103.458
    })

    expect(error).toEqual({'amount': 'Amount must be equal to total Fee Received'})
  })

  it('validates transaction detail type for withdrawal', () => {
    const error = validateTransaction({amount: 100.456755, detail: 'PUT_OPTION'}, {
      addEditTransaction: {
        transactionType: 'WITHDRAWAL'
      },
      contract: {
        contractType: 'POOLED_PLAN'
      }
    })

    expect(error).toEqual({})
  })

  it('validates transaction detail type of PUT_OPTION for withdrawal transaction and single plan contracts to be invalid', () => {
    const error = validateTransaction({amount: 100.456755, detail: 'PUT_OPTION'}, {
      addEditTransaction: {
        transactionType: 'WITHDRAWAL'
      },
      contract: {
        contractType: 'SINGLE_PLAN'
      }
    })

    expect(error).toEqual({detail: 'Transaction detail value not valid for transaction and contract type'})
  })
})

describe('mapStateToProps', () => {
  const newDepositTransaction = {
    transactionType: 'DEPOSIT',
    transactionTypeText: 'Deposit'
  }

  const validateInitialValues = (initialValues) => {
    expect(initialValues.feeDistributionTotal).toEqual(0)
    expect(initialValues.effectiveDate).toBeUndefined()
  }

  it('maps addEditTransaction with no contract selected', () => {
    const state = {
      transactions: {addEditTransaction: newDepositTransaction},
      form: {TransactionFiltersForm: {}},
      contracts: {}
    }

    const props = mapStateToProps(state)

    expect(props.addEditTransaction).toEqual(newDepositTransaction)
    expect(props.contractId).toBeUndefined()
    validateInitialValues(props.initialValues)
  })

  it('maps props gracefully when edit transaction is not selected', () => {
    const state = {
      transactions: {addEditTransaction: undefined},
      form: {TransactionFiltersForm: {}},
      contracts: {}
    }

    const props = mapStateToProps(state)

    expect(props.addEditTransaction).toEqual({})
    validateInitialValues(props.initialValues)
    expect(props.contractId).toBeUndefined()
  })

  it('maps existing Transaction to props', () => {
    const existingTransaction = {
      transactionId: 303,
      transactionType: 'DEPOSIT',
      transactionTypeText: 'Deposit'
    }
    const state = {
      transactions: {addEditTransaction: existingTransaction},
      form: {TransactionFiltersForm: {}},
      contracts: {}
    }

    const props = mapStateToProps(state)

    expect(props.addEditTransaction).toEqual(existingTransaction)
    expect(props.initialValues).toEqual(existingTransaction)
    expect(props.contractId).toBeUndefined()
  })

  it('defaults transaction amount to 0 for fee adjustments', () => {
    const newTransaction = {
      transactionType: 'FEE_ADJUSTMENT',
      transactionTypeText: 'Fee Adjustment'
    }
    const state = {
      transactions: {addEditTransaction: newTransaction},
      form: {TransactionFiltersForm: {}},
      contracts: {}
    }

    const props = mapStateToProps(state)

    expect(props.addEditTransaction).toEqual(newTransaction)
    validateInitialValues(props.initialValues)
    expect(props.contractId).toBeUndefined()
    expect(props.initialValues.amount).toEqual(0)
  })

  it('doesnt default effective date for Termination Transaction', () => {
    const newTransaction = {
      transactionType: 'TERMINATION',
      transactionTypeText: 'Termination'
    }
    const state = {
      transactions: {addEditTransaction: newTransaction},
      form: {TransactionFiltersForm: {}},
      contracts: {}
    }

    const props = mapStateToProps(state)

    expect(props.addEditTransaction).toEqual(newTransaction)
    expect(props.initialValues.feeDistributionTotal).toEqual(0)
    expect(props.initialValues.contractId).toBeUndefined()
    expect(props.initialValues.effecitveDate).toBeUndefined()

    expect(props.contractId).toBeUndefined()
    expect(props.initialValues.amount).toBeUndefined()
  })

  it('maps selected contract', () => {
    const selectedContractId = 600
    const contract1 = {contractId: 599, contractNumber: 'RGA00599'}
    const contract2 = {contractId: 600, contractNumber: 'RGA00600'}

    const state = {
      transactions: {addEditTransaction: newDepositTransaction},
      form: {AddEditTransactionForm: {values: {contractId: selectedContractId}}},
      contracts: {
        allContracts: {
          results: [contract1, contract2]
        }
      }
    }

    const props = mapStateToProps(state)

    expect(props.addEditTransaction).toEqual(newDepositTransaction)
    validateInitialValues(props.initialValues)
    expect(props.contract).toEqual(contract2)
  })
})
