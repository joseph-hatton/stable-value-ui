import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'

import {FeeDistributionGrid, mapStateToProps} from '../../src/transactions/FeeDistributionGrid'

jest.mock('react-redux', () => {
  const mockConnectChildFunction = jest.fn().mockImplementation(component => <component/>)
  return {
    connect: jest.fn().mockImplementation(() => mockConnectChildFunction)
  }
})
jest.mock('../../src/components/grid', () => ({
  Grid: mockComponent('IntlProvider'),
  CreateGridConfig: jest.fn().mockImplementation((data, config) => config),
  GridContextMenu: mockComponent('GridContextMenu')
}))

const renderFeeDistributionGrid = (feeDistributions = [],
                                   transactionId = 100,
                                   contractId = 500,
                                   loadFeeDistributions = jest.fn(),
                                   createNewFeeDistribution = jest.fn()) => {
  return {
    component: renderer.create(<FeeDistributionGrid feeDistributions={feeDistributions}
                                                    transactionId={transactionId} contractId={contractId}
                                                    loadFeeDistributions={loadFeeDistributions}
                                                    createNewFeeDistribution={createNewFeeDistribution}/>),
    props: {
      feeDistributions,
      transactionId,
      contractId,
      loadFeeDistributions,
      createNewFeeDistribution
    }
  }
}

describe('Fee Distribution Grid', () => {
  it('renders the Grid', () => {
    expect(renderFeeDistributionGrid().component.toJSON()).toMatchSnapshot()
  })

  it('loads existing fee distributions if transaction is provided, on mount', () => {
    const {component, props: {loadFeeDistributions, createNewFeeDistribution}} = renderFeeDistributionGrid()

    component.getInstance().componentDidMount()

    expect(loadFeeDistributions).toHaveBeenCalled()
    expect(createNewFeeDistribution).toHaveBeenCalledTimes(0)
  })

  it('creates new fee distributions if transaction is provided, on mount', () => {
    const {component, props: {loadFeeDistributions, createNewFeeDistribution}} = renderFeeDistributionGrid(undefined, null)

    component.getInstance().componentDidMount()

    expect(createNewFeeDistribution).toHaveBeenCalled()
    expect(loadFeeDistributions).toHaveBeenCalledTimes(0)
  })

  it('loads existing fee distributions if transaction is provided, when new transaction is opened', () => {
    const {component, props: {loadFeeDistributions, createNewFeeDistribution}} = renderFeeDistributionGrid()

    component.getInstance().componentWillReceiveProps()

    expect(loadFeeDistributions).toHaveBeenCalled()
    expect(createNewFeeDistribution).toHaveBeenCalledTimes(0)
  })

  it('loads existing fee distributions if transaction is provided, when new transaction is opened', () => {
    const {component, props: {loadFeeDistributions, createNewFeeDistribution}} = renderFeeDistributionGrid(undefined, null)

    component.getInstance().componentWillReceiveProps({contractId: 401})

    expect(loadFeeDistributions).toHaveBeenCalledTimes(0)
    expect(createNewFeeDistribution).toHaveBeenCalled()
  })

  it('maps state to props', () => {
    const feeDistributions = jest.fn()

    const props = mapStateToProps({
      transactions: {
        feeDistributions: feeDistributions
      },
      form: {
        AddEditTransactionForm: {
          values: {
            transactionId: 405,
            contractId: 303
          }
        }
      }
    })

    expect(props).toEqual({
      feeDistributions,
      transactionId: 405,
      contractId: 303
    })
  })
})
