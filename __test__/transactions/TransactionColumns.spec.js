import React from 'react'
import _ from 'lodash'
import moment from 'moment'
import {FormattedNumber} from 'react-intl'
import {dateFormat} from '../../src/components/utilities/Formatters'
import {createReferenceDataTextRenderer} from '../../src/components/renderers/ReferenceDataTextRenderer'
import {GridContextMenuLink} from '../../src/components/grid'
import {createSelectTransactionPayload} from '../../src/transactions/TransactionActions'

import TransactionColumns from '../../src/transactions/TransactionColumns'

jest.mock('../../src/components/renderers/ReferenceDataTextRenderer', () => {
  return {createReferenceDataTextRenderer: jest.fn().mockImplementation(refDataKey => refDataKey)}
})

const expectedColumns = [
  'contractNumber',
  'shortPlanName',
  'effectiveDate',
  'transactionType',
  'detail',
  'amount',
  'beginningBookValue',
  'amountByBookValue',
  'comments'
]

const getRenderer = (field) => TransactionColumns.find(column => column.field === field).renderer

describe('Transaction Columns', () => {
  it('Defines all Transaction columns', () => {
    expect(TransactionColumns.length).toEqual(9)
    expect(_.map(TransactionColumns, 'field')).toEqual(expectedColumns)
  })

  it('formats effectiveDate to correct format', () => {
    const today = moment()

    const effectiveDateRenderer = getRenderer('effectiveDate')

    expect(effectiveDateRenderer(today)).toEqual(dateFormat(today))
  })

  it('defines transactionTypes renderer for transactionTypes column', () => {
    expect(createReferenceDataTextRenderer).toHaveBeenCalledWith('transactionTypes')
    expect(getRenderer('transactionType')).toEqual('transactionTypes')
  })

  it('defines transactionDetailedTypes renderer for transaction Details column', () => {
    expect(createReferenceDataTextRenderer).toHaveBeenCalledWith('transactionDetailedTypes')
    expect(getRenderer('detail')).toEqual('transactionDetailedTypes')
  })

  it('renders amountByBookValue using Formatted Number Component', () => {
    const amountByBookValueRenderer = getRenderer('amountByBookValue')

    expect(amountByBookValueRenderer(null, {amount: 100, beginningBookValue: 150}))
      .toEqual(<FormattedNumber minimumFractionDigits={2} style="percent" value={0.6666666666666666}/>)
  })

  it('renders empty amountByBookValue if beginningBookValue is not present', () => {
    const amountByBookValueRenderer = getRenderer('amountByBookValue')

    expect(amountByBookValueRenderer(null, {amount: 100})).toEqual('')
  })

  it('renders empty amountByBookValue for fee transaction', () => {
    expect(getRenderer('amountByBookValue')(45454542.545454, {transactionType: 'FEE_RECEIPT'})).toEqual('')
    expect(getRenderer('amountByBookValue')(45454542.545454, {transactionType: 'FEE_ADJUSTMENT'})).toEqual('')
  })

  it('renders contractNumber correctly', () => {
    const contractNumberRenderer = getRenderer('contractNumber')

    const contractNumber = 'RGA007'
    const transaction = {amount: 100, beginningBookValue: 150}

    expect(contractNumberRenderer(contractNumber, transaction))
      .toEqual(<GridContextMenuLink menuName="TransactionsGridMenu" cell={contractNumber} row={transaction}
                                    rowSelectionActionType="SELECT_TRANSACTION"
                                    createPayload={createSelectTransactionPayload}/>)
  })

  it('renders transaction amount in Dollar Amount format', () => {
    expect(getRenderer('amount')(2523242.66565)).toEqual('2,523,242.67')
  })

  it('renders transaction beginningBookValue in Dollar Amount format for deposit', () => {
    expect(getRenderer('beginningBookValue')(45454542.545454, {transactionType: 'DEPOSIT'})).toEqual('45,454,542.55')
  })

  it('renders transaction beginningBookValue in Dollar Amount format for Fee Transaction', () => {
    expect(getRenderer('beginningBookValue')(45454542.545454, {transactionType: 'FEE_RECEIPT'})).toEqual('')
    expect(getRenderer('beginningBookValue')(45454542.545454, {transactionType: 'FEE_ADJUSTMENT'})).toEqual('')
  })
})
