import createRefDataFormFieldComponent from '../../src/components/forms/createRefDataFormFieldComponent'

import {
  TransactionTypes,
  TransactionDetailsTypes,
  AmountComparators,
  DepositTransactionDetails,
  WithdrawalTransactionDetails,
  AdjustmentTransactionDetails,
  TerminationTransactionDetails,
  TransactionDetailsTypesMap
} from '../../src/transactions/TransactionReferenceDataFields'

jest.mock('../../src/components/forms/createRefDataFormFieldComponent',
  () => jest.fn().mockImplementation((transactionType) => transactionType))

describe('TransactionReferenceDataFields', () => {
  const assetOption = (Option, name, config, mapper, nullable) => {
    expect(createRefDataFormFieldComponent).toHaveBeenCalledWith(name, config, mapper, nullable)
    expect(Option).toEqual(name) // Mock Implementation returns the name
  }

  it('defines TransactionTypes', () => {
    assetOption(TransactionTypes, 'transactionTypes', {
      floatingLabelText: 'Transaction Type',
      type: 'text'
    }, undefined, true)
  })

  it('defines AmountComparators', () => {
    assetOption(AmountComparators, 'amountComparators', {
      floatingLabelText: 'Amount Comparators',
      type: 'text'
    }, undefined, true)
  })

  it('defines TransactionDetailsTypes', () => {
    assetOption(TransactionDetailsTypes, 'transactionDetailedTypes', {
      floatingLabelText: 'Transaction Details Type',
      type: 'text'
    }, undefined, true)
  })

  it('defines DepositTransactionDetails', () => {
    assetOption(DepositTransactionDetails, 'depositTransactionDetails', {
      floatingLabelText: '* Transaction Detail',
      type: 'text'
    }, undefined, false)
  })

  it('defines WithdrawalTransactionDetails', () => {
    assetOption(WithdrawalTransactionDetails, 'withdrawalTransactionDetails', {
      floatingLabelText: '* Transaction Detail',
      type: 'text'
    }, undefined, false)
  })

  it('defines AdjustmentTransactionDetails', () => {
    assetOption(AdjustmentTransactionDetails, 'adjustmentTransactionDetails', {
      floatingLabelText: '* Transaction Detail',
      type: 'text'
    }, undefined, false)
  })

  it('defines TerminationTransactionDetails', () => {
    assetOption(TerminationTransactionDetails, 'terminationTransactionDetails', {
      floatingLabelText: '* Transaction Detail',
      type: 'text'
    }, undefined, false)
  })

  it('maps TransactionDetailsType', () => {
    expect(TransactionDetailsTypesMap.DEPOSIT).toEqual(DepositTransactionDetails)
    expect(TransactionDetailsTypesMap.WITHDRAWAL).toEqual(WithdrawalTransactionDetails)
    expect(TransactionDetailsTypesMap.TERMINATION).toEqual(TerminationTransactionDetails)
    expect(TransactionDetailsTypesMap.ADJUSTMENT).toEqual(AdjustmentTransactionDetails)
  })
})
