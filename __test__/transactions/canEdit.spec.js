import {hasEntitlement} from '../../src/entitlements'
import store from '../../src/store/store'

import canEdit from '../../src/transactions/canEdit'

jest.mock('../../src/entitlements', () => ({
  hasEntitlement: jest.fn()
}))

jest.mock('../../src/store/store', () => ({
  getState: jest.fn()
}))

describe('canEdit', () => {
  let state
  beforeEach(() => {
    state = {
      transactions: {
        addEditTransaction: {
          locked: false
        }
      }
    }
    store.getState.mockReturnValue(state)
    hasEntitlement.mockReturnValue(true)
  })
  it('should evaluate correctly', () => {
    expect(canEdit()).toBeTruthy()
  })
  it('should evaluate correctly', () => {
    hasEntitlement.mockReturnValue(false)
    expect(canEdit()).toBeFalsy()
  })
  it('should evaluate correctly', () => {
    state.transactions.addEditTransaction = null
    expect(canEdit()).toBeFalsy()
  })
  it('should evaluate correctly', () => {
    state.transactions.addEditTransaction.locked = true
    expect(canEdit()).toBeFalsy()
  })
})
