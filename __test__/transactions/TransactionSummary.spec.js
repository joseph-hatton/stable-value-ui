import React from 'react'
import mockComponent from '../MockComponent'
import renderer from 'react-test-renderer'

import {TransactionSummary, mapStateToProps} from '../../src/transactions/TransactionSummary'

jest.mock('material-ui/Card', () => ({Card: mockComponent('Card')}))
jest.mock('material-ui/styles/colors', () => ({white: 'white', red500: 'blue'}))
jest.mock('react-redux', () => {
  const mockConnectChildFunction = jest.fn().mockImplementation(component => <component/>)
  return {
    connect: jest.fn().mockImplementation(() => mockConnectChildFunction)
  }
})

const transactionSummary = {
  'deposits': 11989953522.15,
  'withdrawals': -2065362433.97,
  'adjustments': -10617471.04,
  'feeReceipts': 65367157.15,
  'feeAdjustments': -1651.49,
  'terminations': -349202530.73,
  'amountBvPercent': 5407.87,
  'withdrawalBvPercent': 1788.98,
  'depositBvPercent': 3179.13
}

describe('Transaction Summary', () => {
  it('Renders Summary', () => {
    const component = renderer.create(<TransactionSummary transactionSummary={transactionSummary}/>)

    expect(component.toJSON()).toMatchSnapshot()
  })

  it('maps State to Props', () => {
    const state = {
      transactions: {
        transactionSummary
      }
    }

    const props = mapStateToProps(state)

    expect(props).toEqual({transactionSummary})
  })

  it('maps State to Props if summary is not loaded', () => {
    const state = {
      transactions: {}
    }

    const props = mapStateToProps(state)

    expect(props).toEqual({transactionSummary: {}})
  })
})
