import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import {dispatch, getState} from '../src/store/store'
import {canNavigateAwayFromContracts} from '../src/contracts/ContractActions'
import {openConfirmationAlert} from '../src/components/redux-dialog'
import {browserHistory} from 'react-router'

import stopAndAsk from '../src/stopAndAsk'

const mockCanNavigateAwayFromContracts = jest.fn()

jest.mock('../src/contracts/ContractActions', () => ({
  canNavigateAwayFromContracts: jest.fn(() => mockCanNavigateAwayFromContracts)
}))

jest.mock('react-router', () => ({
  browserHistory: {
    push: jest.fn()
  }
}))

jest.mock('../src/store/store', () => ({
  dispatch: jest.fn(),
  getState: jest.fn()
}))

jest.mock('../src/components/redux-dialog', () => ({
  openConfirmationAlert: jest.fn().mockReturnValue('openConfirmationAlert')
}))

describe('stopAndAsk', () => {
  it('should render correctly', () => {
    const comp = renderer.create(stopAndAsk('name')())
    expect(comp.toJSON()).toMatchSnapshot()
  })
  describe('onClick', () => {
    let event
    beforeEach(() => {
      event = {
        target: {
          parentNode: {pathname: 'pathname'}
        },
        persist: jest.fn(),
        preventDefault: jest.fn(),
        stopPropagation: jest.fn()
      }
      browserHistory.push.mockClear()
      dispatch.mockClear()
      getState.mockClear()
      mockCanNavigateAwayFromContracts.mockClear()
      openConfirmationAlert.mockClear()
    })
    it('should stop the event propagation and check if can navigate from contracts', () => {
      const comp = shallow(stopAndAsk('name')())
      mockCanNavigateAwayFromContracts.mockReturnValue(true)

      comp.prop('onClick')(event)

      expect(event.persist).toHaveBeenCalled()
      expect(event.preventDefault).toHaveBeenCalled()
      expect(event.stopPropagation).toHaveBeenCalled()
      expect(canNavigateAwayFromContracts).toHaveBeenCalled()
      expect(mockCanNavigateAwayFromContracts).toHaveBeenCalledWith(dispatch, getState)
    })
    it('should redirect when can navigate from contracts returns true', () => {
      const comp = shallow(stopAndAsk('name')())
      mockCanNavigateAwayFromContracts.mockReturnValue(true)

      comp.prop('onClick')(event)

      expect(browserHistory.push).toHaveBeenCalledWith('pathname')
    })
    it('should open confirmation dialog when can navigate from contracts returns false', () => {
      const comp = shallow(stopAndAsk('name')())
      mockCanNavigateAwayFromContracts.mockReturnValue(false)

      comp.prop('onClick')(event)

      expect(dispatch).toHaveBeenCalledWith('openConfirmationAlert')
      expect(openConfirmationAlert).toHaveBeenCalled()
      expect(openConfirmationAlert.mock.calls[0][0].message).toBe('Your changes are not saved. Leaving this contract will erase your changes.')
      expect(openConfirmationAlert.mock.calls[0][1]).toBe('navigateAwayFromContract')
    })
    it('onOk should redirect', () => {
      const comp = shallow(stopAndAsk('name')())
      mockCanNavigateAwayFromContracts.mockReturnValue(false)

      comp.prop('onClick')(event)
      openConfirmationAlert.mock.calls[0][0].onOk()

      expect(browserHistory.push).toHaveBeenCalledWith('pathname')
    })
  })
})
