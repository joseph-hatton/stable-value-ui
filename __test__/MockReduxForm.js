import mockComponent from './MockComponent'

export default function () {
  const reduxFormChildFn = jest.fn(component => component)
  const resetAction = jest.fn()
  const changeAction = jest.fn()
  return {
    Field: mockComponent('Field'),
    reduxForm: jest.fn(() => reduxFormChildFn),
    reduxFormChildFn,
    reset: jest.fn(() => resetAction),
    resetAction,
    change: jest.fn(() => changeAction),
    changeAction
  }
}
