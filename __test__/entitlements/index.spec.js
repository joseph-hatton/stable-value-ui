import * as ALL_ENTITLEMENTS from '../../src/entitlements/entitlements'
import determineEntitlements, {hasEntitlement} from '../../src/entitlements/index'

jest.mock('../../src/store/store.js', () => ({
  getState: () => ({
    currentUser: {
      ADD_CONTRACT: true,
      REJECT_CONTRACT: false
    }
  })
}))

describe('Entitlements', () => {
  it('Creates entitlement map for ADMIN', () => {
    const entitlements = determineEntitlements(ALL_ENTITLEMENTS.ADMIN)

    expect(entitlements).toEqual({
      ADD_CONTRACT: false,
      EDIT_PENDING_CONTRACT: true,
      EDIT_ACTIVE_CONTRACT: true,
      EDIT_INACTIVE_CONTRACT: false,
      INACTIVATE_CONTRACT: false,
      REACTIVATE_CONTRACT: false,
      DELETE_CONTRACT: false,
      DELETE_CONTRACT_HISTORY: true,
      SUBMIT_CONTRACT: true,
      REJECT_CONTRACT: false,
      ADD_ACTION_STEP: false,
      CREDITING_RATE: true,
      DELETE_ACTION_STEP: false,
      EDIT_ACTION_STEP: false,
      ADD_MARKET_VALUE: true,
      MODIFY_PORTFOLIO: false,
      MODIFY_REASON: false,
      MODIFY_RISK_SCORECARD: false,
      RUN_DAILY_CYCLE: true,
      RUN_MONTHLY_CYCLE: true,
      MODIFY_TRANSACTION: true
    })
  })

  it('Creates entitlement map for RISK', () => {
    const entitlements = determineEntitlements(ALL_ENTITLEMENTS.RISK)

    expect(entitlements).toEqual({
      ADD_CONTRACT: false,
      EDIT_PENDING_CONTRACT: true,
      EDIT_ACTIVE_CONTRACT: true,
      EDIT_INACTIVE_CONTRACT: false,
      INACTIVATE_CONTRACT: false,
      REACTIVATE_CONTRACT: false,
      DELETE_CONTRACT: false,
      DELETE_CONTRACT_HISTORY: false,
      SUBMIT_CONTRACT: false,
      REJECT_CONTRACT: false,
      ADD_ACTION_STEP: true,
      CREDITING_RATE: false,
      DELETE_ACTION_STEP: true,
      EDIT_ACTION_STEP: true,
      ADD_MARKET_VALUE: false,
      MODIFY_PORTFOLIO: true,
      MODIFY_REASON: true,
      MODIFY_RISK_SCORECARD: true,
      RUN_DAILY_CYCLE: false,
      RUN_MONTHLY_CYCLE: false,
      MODIFY_TRANSACTION: false
    })
  })

  it('Creates entitlement map for COMPLIANCE', () => {
    const entitlements = determineEntitlements(ALL_ENTITLEMENTS.COMPLIANCE)

    expect(entitlements).toEqual({
      ADD_CONTRACT: true,
      EDIT_PENDING_CONTRACT: true,
      EDIT_ACTIVE_CONTRACT: true,
      EDIT_INACTIVE_CONTRACT: true,
      INACTIVATE_CONTRACT: true,
      REACTIVATE_CONTRACT: true,
      DELETE_CONTRACT: true,
      DELETE_CONTRACT_HISTORY: false,
      SUBMIT_CONTRACT: false,
      REJECT_CONTRACT: true,
      ADD_ACTION_STEP: false,
      CREDITING_RATE: false,
      DELETE_ACTION_STEP: false,
      EDIT_ACTION_STEP: false,
      ADD_MARKET_VALUE: false,
      MODIFY_PORTFOLIO: false,
      MODIFY_REASON: false,
      MODIFY_RISK_SCORECARD: false,
      RUN_DAILY_CYCLE: false,
      RUN_MONTHLY_CYCLE: false,
      MODIFY_TRANSACTION: false
    })

    console.log(entitlements)
  })

  it('Creates entitlement map for SUPPORT', () => {
    const entitlements = determineEntitlements(ALL_ENTITLEMENTS.SUPPORT)

    expect(entitlements).toEqual({
      ADD_CONTRACT: true,
      EDIT_PENDING_CONTRACT: true,
      EDIT_ACTIVE_CONTRACT: true,
      EDIT_INACTIVE_CONTRACT: true,
      INACTIVATE_CONTRACT: true,
      REACTIVATE_CONTRACT: true,
      DELETE_CONTRACT: true,
      DELETE_CONTRACT_HISTORY: true,
      SUBMIT_CONTRACT: true,
      REJECT_CONTRACT: true,
      ADD_ACTION_STEP: true,
      CREDITING_RATE: true,
      DELETE_ACTION_STEP: true,
      EDIT_ACTION_STEP: true,
      ADD_MARKET_VALUE: true,
      MODIFY_PORTFOLIO: true,
      MODIFY_REASON: true,
      MODIFY_RISK_SCORECARD: true,
      RUN_DAILY_CYCLE: false,
      RUN_MONTHLY_CYCLE: false,
      MODIFY_TRANSACTION: true
    })
  })

  it('validates access to give entitlement', () => {
    expect(hasEntitlement('ADD_CONTRACT')).toBe(true)
    expect(hasEntitlement('REJECT_CONTRACT')).toBe(false)
  })
})
