import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme'
import mockComponent from './MockComponent'

import Routes from '../src/routes'

jest.mock('react-router', () => ({
  Route: mockComponent('Route'),
  IndexRoute: mockComponent('IndexRoute')
}))

jest.mock('../src/components/common/HomePage', () => 'Home')
jest.mock('../src/components/App', () => 'App')
jest.mock('../src/companies/CompaniesContainer', () => 'CompaniesContainer')
jest.mock('../src/contracts/ContractsContainer', () => 'ContractsContainer')
jest.mock('../src/contacts/ContactsContainer', () => 'ContactsContainer')
jest.mock('../src/contracts/AddEditContractContainer', () => 'AddEditContractContainer')
jest.mock('../src/contracts/ContractContainer', () => 'ContractContainer')
jest.mock('../src/contracts/ContractBaseInfo', () => 'ContractBaseInfo')
jest.mock('../src/contracts/ContractSchedule', () => 'ContractSchedule')
jest.mock('../src/contracts/ContractUnderwriting', () => 'ContractUnderwriting')
jest.mock('../src/contracts/ContractContacts', () => 'ContractContacts')
jest.mock('../src/contracts/risk-scorecard/RiskScorecard', () => 'RiskScorecard')
jest.mock('../src/contracts/crediting-rate/CreditingRates', () => 'CreditingRates')
jest.mock('../src/wrap-portfolio/WrapPortfolioContainer', () => 'WrapPortfolioContainer')
jest.mock('../src/wrap-portfolio/PortfolioAllocationComponent', () => 'PortfolioAllocationComponent')
jest.mock('../src/wrap-portfolio/PortfolioComponent', () => 'Portfolio')
jest.mock('../src/wrap-portfolio/AddEditPortfolioAllocationContainer', () => 'AddEditPortfolioAllocationContainer')
jest.mock('../src/transactions/Transactions', () => 'Transactions')
jest.mock('../src/fees/monthly/MonthlyFees', () => 'MonthlyFees')
jest.mock('../src/fees/daily/DailyFees', () => 'DailyFees')
jest.mock('../src/bookValues/monthly/MonthlyBookValues', () => 'MonthlyBookValues')
jest.mock('../src/bookValues/daily/DailyBookValues', () => 'DailyBookValues')
jest.mock('../src/watchLists/WatchListContainer', () => 'WatchListContainer')
jest.mock('../src/cycles/CyclesContainer', () => 'Cycles')
jest.mock('../src/handle404', () => 'Handle404')
jest.mock('../src/marketValue/MarketValues', () => 'MarketValues')

jest.mock('../src/components/common/LeftDrawerActions', () => ({
  onEnterTabbed: (tab) => `onEnterTabbed('${tab}')`
}))

describe('routes', () => {
  it('should render correctly', () => {
    expect(renderer.create(Routes).toJSON()).toMatchSnapshot()
  })
  it('Schedule onEnter should redirect to funding', () => {
    const comp = shallow(Routes)
    const state = {params: {contractId: 'contractId'}}
    expect(comp.find('#schedule-route').prop('onEnter')(state, (str) => `replace('${str}')`))
      .toEqual('replace(\'/contracts/contractId/schedule/funding\')')
  })
  it('Underwriting onEnter should redirect to model inputs', () => {
    const comp = shallow(Routes)
    const state = {params: {contractId: 'contractId'}}
    expect(comp.find('#underwriting-route').prop('onEnter')(state, (str) => `replace('${str}')`))
      .toEqual('replace(\'/contracts/contractId/underwriting/model-inputs\')')
  })
  it('Wrap Portfolio onEnter should redirect to allocations', () => {
    const comp = shallow(Routes)
    expect(comp.find('#wrap-portfolio-route').prop('onEnter')(null, (str) => `replace('${str}')`))
      .toEqual('replace(\'/wrap-portfolio/allocations\')')
  })
  it('Fees prettify params', () => {
    const comp = shallow(Routes)
    expect(comp.find('#fees-for-contract-route').prop('prettifyParam')())
      .toEqual('By Contract')
  })
  it('Book Values prettify params', () => {
    const comp = shallow(Routes)
    expect(comp.find('#book-values-for-contract-route').prop('prettifyParam')())
      .toEqual('By Contract')
  })
})
