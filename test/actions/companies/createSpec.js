// import proxyquire from '../../proxyquire'
//
// const companyToSave = {
//   name: 'company A'
// }
//
// describe('companies', () => {
//   let fn, post, dispatch, getState, redirectCallback
//
//   beforeEach(() => {
//     post = sinon.stub().resolves(['a', 'b'])
//     dispatch = sinon.spy()
//     getState = sinon.spy()
//     redirectCallback = sinon.spy()
//
//     fn = proxyquire('src/actions/companies/create', {
//       '../http': {
//         post: post
//       }
//     }).default
//   })
//
//   describe('create', () => {
//     it('sends the passed in company', () =>
//       fn(companyToSave, redirectCallback)(dispatch, getState)
//       .then(() => post.should.have.been.calledWithExactly('https://stable-value-api-dev.rgare.net/api/v1/companies2', companyToSave))
//     )
//
//     it('creates a company', () => {
//       const companyCall = fn(companyToSave, redirectCallback)(dispatch, getState)
//
//       dispatch.should.have.been.calledWithExactly({ type: 'CREATE_COMPANY', status: 'start' })
//       dispatch.should.have.been.calledWithExactly({ type: 'SPINNER_MODAL', active: true })
//       return companyCall.then(() => {
//         // dispatch.should.have.been.calledWithExactly({ type: 'CREATE_COMPANY', status: 'done', result: ['a', 'b'] })
//         dispatch.should.have.been.calledWithExactly({ type: 'SPINNER_MODAL', active: false })
//         redirectCallback.should.have.been.called
//       })
//     })
//
//     it('throws error if failed', () => {
//       const error = new Error('It broke')
//       post.rejects(error)
//
//       const companyCall = fn(companyToSave, redirectCallback)(dispatch, getState)
//
//       dispatch.should.have.been.calledWithExactly({ type: 'CREATE_COMPANY', status: 'start' })
//       dispatch.should.have.been.calledWithExactly({ type: 'SPINNER_MODAL', active: true })
//       return companyCall.then(() => {
//         dispatch.should.have.been.calledWithExactly({ type: 'CREATE_COMPANY', status: 'error', error })
//         dispatch.should.have.been.calledWithExactly({ type: 'SPINNER_MODAL', active: false })
//         redirectCallback.should.have.not.been.called
//       })
//     })
//   })
// })
