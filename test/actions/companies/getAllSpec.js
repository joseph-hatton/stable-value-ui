// import proxyquire from '../../proxyquire'
//
// describe('companies', () => {
//   let fn, get, dispatch, getState
//
//   beforeEach(() => {
//     get = sinon.stub().resolves(['a', 'b'])
//     dispatch = sinon.spy()
//     getState = sinon.spy()
//
//     fn = proxyquire('src/actions/companies2/getAll', {
//       '../http': {
//         get: get
//       }
//     }).default
//   })
//
//   describe('get all', () => {
//     it('resolves all companies', () => {
//       const companyCall = fn()(dispatch, getState)
//
//       dispatch.should.have.been.calledWithExactly({ type: 'GET_COMPANIES', status: 'start' })
//       return companyCall.then(() => {
//         dispatch.should.have.been.calledWithExactly({ type: 'GET_COMPANIES', status: 'done', result: ['a', 'b'] })
//       })
//     })
//
//     it('throws error if failed', () => {
//       const error = new Error('It broke')
//       get.rejects(error)
//
//       const companyCall = fn()(dispatch, getState)
//
//       dispatch.should.have.been.calledWithExactly({ type: 'GET_COMPANIES', status: 'start' })
//       return companyCall.then(() => {
//         dispatch.should.have.been.calledWithExactly({ type: 'GET_COMPANIES', status: 'error', error })
//       })
//     })
//   })
// })
