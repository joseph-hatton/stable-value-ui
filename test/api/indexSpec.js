/* global sinon */
import proxyquire from '../proxyquire'

describe('api index', () => {
  let request, routerUse, response

  beforeEach(() => {
    request = {
      method: 'somemethod',
      originalUrl: 'someOriginalUrl'
    }
    routerUse = sinon.spy()
    response = {
      json: sinon.spy(),
      status: sinon.stub()
    }

    proxyquire('api', {
      'express': {
        'Router': () => {
          return { 'use': routerUse }
        }
      }
    })
  })

  describe('temp', () => {
    it('renders message', () => {
      const fn = routerUse.getCall(0).args[1]

      fn(request, response)

      response.json.should.have.been.calledWithExactly(
        { firstName: 'Mark', lastName: 'Otto', userid: '@mdo' },
        { firstName: 'Jacob', lastName: 'Thornton', userid: '@fat' },
        { firstName: 'Mark', lastName: 'Otto', userid: '@mdo' },
        { firstName: 'Mark', lastName: 'Otto', userid: '@mdo' })
    })
  })
})
