import React from 'react'
import {shallow} from 'enzyme'
import Dialog from '../../../src/components/utilities/Dialog'

describe('Dialog', () => {
  let component, props

  beforeEach(() => {
    props = {
    }
    component = shallow(
      <Dialog>
        <div className='test-content'/>
      </Dialog>
    )
  })

  it('renders backdrop and children', () => {
    component.hasClass('custom-modal-dialog').should.be.true
    component.find('.custom-backdrop').should.exist
    component.find('.dialog .test-content').should.exist
  })
})
