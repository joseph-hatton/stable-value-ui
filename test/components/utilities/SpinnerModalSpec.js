import React from 'react'
import {shallow} from 'enzyme'
import SpinnerModal from '../../../src/components/utilities/SpinnerModal'

describe('SpinnerModal', () => {
  let component, props, Spinner

  beforeEach(() => {
    props = {}
    component = shallow(<SpinnerModal {...props} />)
  })

  describe('render', () => {
    it('shows a back backdrop', () => {
      component.find('.modal-backdrop').should.exist
    })

    it('shows a spinner', () => {
      component.find('.modal-backdrop').should.have.descendants('Spinner')
    })

    it('shows an overlayed spinner', () => {
      component.find('Spinner').props().should.eql({overlay: true})
    })
  })
})
