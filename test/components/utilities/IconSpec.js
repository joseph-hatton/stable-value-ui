import React from 'react'
import {shallow} from 'enzyme'
import Icon from '../../../src/components/utilities/Icon'

describe('Icon', () => {
  let component, props

  beforeEach(() => {
    props = {name: 'foo'}
    component = shallow(<Icon {...props}/>)
  })

  it('renders font awesome classes', () =>
    component.find('i').props().should.eql({className: `fa fa-${props.name}`, onClick: undefined, 'aria-hidden': true})
  )

  it('renders size if included', () => {
    component.setProps({size: 'lg'})

    component.find('i.fa-lg').should.exist
  })

  it('forwards onClick', () => {
    const onClick = sinon.stub()
    component.setProps({onClick})
    component.find('i').simulate('click')

    onClick.should.have.been.called
  })
})
