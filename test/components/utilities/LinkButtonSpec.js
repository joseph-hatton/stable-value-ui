import React from 'react'
import {shallow} from 'enzyme'
import LinkButton from '../../../src/components/utilities/LinkButton'

describe('LinkButton', () => {
  let component, props

  beforeEach(() => {
    props = {
      className: 'foo',
      onClick: sinon.stub()
    }
    component = shallow(<LinkButton {...props}/>)
  })

  const clickLink = () => {
    const event = {preventDefault: sinon.stub()}
    component.find('a').prop('onClick')(event)
    return event
  }

  it('prevents default behavior on click', () => {
    const event = clickLink()

    event.preventDefault.should.have.been.calledWithExactly()
    props.onClick.should.have.been.calledWithExactly(event)
  })

  it('prevents default behavior on click even if no onClick fn is specified', () => {
    component.setProps({onClick: undefined})
    const event = clickLink()

    event.preventDefault.should.have.been.calledWithExactly()
  })

  it('includes classname', () =>
    component.find('a').prop('className').should.equal(props.className)
  )

  it('renders disabled', () => {
    component.setProps({disabled: true})

    component.find('a').should.not.exist
    component.find('span.disabled-link').should.exist
  })
})
