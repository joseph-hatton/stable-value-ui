import React from 'react'
import {shallow} from 'enzyme'
import Spinner from '../../../src/components/utilities/Spinner'

describe('Spinner', () => {
  let component, props

  beforeEach(() => {
    props = {
      inline: false,
      overlay: false
    }
    // component = shallow(<Spinner {...props} />)
  })

  const renderComponent = () => {
    component = shallow(<Spinner {...props} />)
  }

  describe('render', () => {
    describe('with default props', () => {
      beforeEach(() => {
        renderComponent()
      })

      it('default spinner', () => {
        component.find('div').should.have.className('spinner')
      })

      it('has Icon', () => {
        component.find('.spinner').should.have.descendants('Icon')
      })

      it('sends Icon expected props', () => {
        component.find('Icon').props().should.eql({name: 'spinner', className: 'fa-spin'})
      })
    })

    describe('with custom props', () => {
      it('inline spinner', () => {
        props.inline = true
        renderComponent()
        component.find('div').should.have.className('spinner-inline')
      })

      it('overlay spinner', () => {
        props.overlay = true
        renderComponent()
        component.find('div').should.have.className('spinner-overlay')
      })
    })
  })
})
