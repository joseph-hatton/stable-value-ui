import fn from '../../src/reducers/companyReducers'
import reducerTester from './reducerTester'

const initialState = { allCompanies: { results: undefined }, editCompany: undefined }
const doneResult = ['a', 'b']

describe('company reducers', () => {
  let state, reducerParams

  beforeEach(() => {
    reducerParams = {fn, state, result: doneResult}
  })

  it('get default state with unknown action type', () => reducerTester.testDefault(reducerParams, '', initialState))
})
