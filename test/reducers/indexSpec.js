import proxyquire from '../proxyquire'

describe('reducers', () => {
  let fn, combineReducers, contractReducers, companyReducers, contactReducers, currentUserReducer, dialogReducer,
    referenceDataReducer

  const combineReducersResult = 'expected result'
  const gridReducer = {Reducers: {a: true}}
  const formReducer = {reducer: {b: true}}

  beforeEach(() => {
    combineReducers = sinon.stub().returns(combineReducersResult)
    contractReducers = sinon.spy()
    dialogReducer = sinon.spy()
    companyReducers = sinon.spy()
    contactReducers = sinon.spy()
    currentUserReducer = sinon.spy()
    referenceDataReducer = sinon.spy()

    fn = proxyquire('src/reducers/index', {
      'redux': {combineReducers},
      './contractReducers': contractReducers,
      './companyReducers': companyReducers,
      './contactReducers': contactReducers,
      './currentUserReducer': currentUserReducer,
      '../components/redux-dialog': { dialogReducer },
      'react-redux-grid': gridReducer,
      'redux-form': formReducer,
      './referenceDataReducer': referenceDataReducer
    }).default
  })

  it('combines all expected reducers', () =>
    combineReducers.should.have.been.calledWithExactly({
      ...gridReducer.Reducers,
      currentUser: currentUserReducer,
      dialog: dialogReducer,
      companies: companyReducers,
      contacts: contactReducers,
      contracts: contractReducers,
      form: formReducer.reducer,
      referenceData: referenceDataReducer
    }))

  it('returns the result of combine reducers', () =>
    fn.should.eql(combineReducersResult)
  )
})
