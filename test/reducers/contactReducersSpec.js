import contactReducer from '../../src/reducers/contactReducers'

describe('contact reducers', () => {
  const initState = {
    allContacts: [],
    editContact: {
      receiveInvoice: false,
      receiveScheduleA: false,
      receiveStatement: false
    },
    managers: []
  }

  it('get default state with unknown action type', () => {
    contactReducer(undefined, {}).should.eql(initState)
  })

  describe('QUEUE_ASSIGN_CONTRACTS', () => {
    let state, action
    beforeEach(() => {
      state = {
        bufferedActions: {
          unassignContracts: [],
          assignContracts: []
        },
        selectedContact: {
          contracts: []
        },
        originalCopySelectedContact: {
          contracts: []
        }
      }
      action = {
        type: 'QUEUE_ASSIGN_CONTRACTS',
        payload: []
      }
    })
    it('should do nothing if the payload is in the original list and in the current list', () => {
      const contract = {contractId: 123}
      state.selectedContact.contracts = [contract]
      state.originalCopySelectedContact.contracts = [contract]
      action.payload = [contract]

      expect(contactReducer(state, action)).toEqual({
        bufferedActions: {
          unassignContracts: [],
          assignContracts: []
        },
        selectedContact: {
          contracts: [contract]
        },
        originalCopySelectedContact: {
          contracts: [contract]
        }
      })
    })
    it('should remove the contract from unassign buffer and add to current list if the payload is in the original list, not in the current list and in the unassign queue', () => {
      const contract = {contractId: 123}
      state.originalCopySelectedContact.contracts = [contract]
      state.bufferedActions.unassignContracts = [contract]
      action.payload = [contract]

      expect(contactReducer(state, action)).toEqual({
        bufferedActions: {
          unassignContracts: [],
          assignContracts: []
        },
        selectedContact: {
          contracts: [contract]
        },
        originalCopySelectedContact: {
          contracts: [contract]
        }
      })
    })
    it('should do nothing if the payload is not in the original list and in the current list', () => {
      const contract = {contractId: 123}
      state.selectedContact.contracts = [contract]
      action.payload = [contract]

      expect(contactReducer(state, action)).toEqual({
        bufferedActions: {
          unassignContracts: [],
          assignContracts: []
        },
        selectedContact: {
          contracts: [contract]
        },
        originalCopySelectedContact: {
          contracts: []
        }
      })
    })
    it('should add the contract to the action queue and to the current list if the payload is not in the original list and not in the current list', () => {
      const contract = {contractId: 123}
      action.payload = [contract]

      expect(contactReducer(state, action)).toEqual({
        bufferedActions: {
          unassignContracts: [],
          assignContracts: [contract]
        },
        selectedContact: {
          contracts: [contract]
        },
        originalCopySelectedContact: {
          contracts: []
        }
      })
    })
  })
})
