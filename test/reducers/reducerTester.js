const testDefault = ({fn, state, error, result}, type, expectedEndState) => {
  const reducedState = fn(state, {type: ''})
  reducedState.should.eql(expectedEndState)
}

const testStart = ({fn, state, error, result}, type, expectedEndState) => {
  const reducedState = fn(state, {type: type, status: 'start'})
  reducedState.should.eql(expectedEndState)
}

const testError = ({fn, state, error, result}, type, expectedEndState) => {
  const reducedState = fn(state, {type: type, status: 'error', error: error})
  reducedState.should.eql(expectedEndState)
}

const testDone = ({fn, state, error, result}, type, expectedEndState) => {
  const reducedState = fn(state, {type: type, status: 'done', result: result})
  reducedState.should.eql(expectedEndState)
}

export default {
  testDefault,
  testStart,
  testError,
  testDone
}
