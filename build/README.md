The default.conf file extends the default nginx configuration.  See file history for changes.  The original change was:

```
    add_header Cache-Control "public, must-revalidate, proxy-revalidate";
    try_files $uri /usr/share/nginx/html/index.html;
```