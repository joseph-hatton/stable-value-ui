status=$(curl --write-out "%{http_code}\n" --silent --output /dev/null --head localhost/healthcheck)
apiStatus=$(curl --write-out "%{http_code}\n" --silent --output /dev/null --head api:3000/api/v1/health/ping)

if [ $status -eq 200 ] && [ $apiStatus -eq 200 ]
then
  echo "Healthcheck passed"
  exit 0
else
  echo "Healthcheck failed"
  exit 1
fi