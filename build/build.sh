#!/bin/bash
set -e
rm -rf build/docker-build || echo "docker-build not found (this is normal)"
cd build
mkdir docker-build
cd docker-build
cp -r ../../public public
cp ../Dockerfile .
cp ../default.conf .
cp ../run.sh .
cp ../healthcheck.sh .
if [ -n "$TAG" ]; then
    docker build -t $TAG .
else
    docker build .
fi
cd ..
rm -rf docker-build
