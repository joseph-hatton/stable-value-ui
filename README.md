# Stable Value UI

React UI fronting the microservice stable-value-api

---

## Deployed URLs

* DEV
    * [ui](https://stable-value-dev.rgare.net)
    * [api](https://stable-value-api-dev.rgare.net)
* TEST
    * [ui](https://stable-value-test.rgare.net)
    * [api](https://stable-value-api-test.rgare.net)
* UAT
    * [ui](https://stable-value-uat.rgare.net)
    * [api](https://stable-value-api-uat.rgare.net)
* PROD

--- 

## Running Locally

```
# Some secrets and passwords are required to run against the live dev api (see devServerConfig.js):
export DEV_CLIENT_SECRET=**** # the client secret for the environment you're attempting to hit (defaults to stable value api deployed to DEV).  This can be found in vault (see below)
export DEV_PASSWORD=***  # your active directory password (to be able to receive an auth token to hit the live dev api)

npm run install

npm run dev

```

---

## VCS

* [TFS Stable Value API](http://tfs2015.rgare.net:8080/tfs/UWSolutions/uws-gfs/_git/stable-value-api)
* [TFS Stable Value UI](http://tfs2015.rgare.net:8080/tfs/UWSolutions/uws-gfs/_git/stable-value-ui)
* [TFS Deployment Project](http://tfs2015.rgare.net:8080/tfs/UWSolutions/uws-gfs/_git/common-deployment)

---

## CI

[Stable Value Jenkins Builds](https://almjenkins.rgare.net/job/ActuarialSolutions/job/Stable%20Value/)

* [build api](https://almjenkins.rgare.net/job/ActuarialSolutions/job/Stable%20Value/job/build%20api/)
* [build ui](https://almjenkins.rgare.net/job/ActuarialSolutions/job/Stable%20Value/job/build%20ui/)
* [deploy to dev](https://almjenkins.rgare.net/job/ActuarialSolutions/job/Stable%20Value/job/deploy%20to%20dev/)
* [test the dev api](https://almjenkins.rgare.net/job/ActuarialSolutions/job/Stable%20Value/job/test%20the%20dev%20api/)
* [deploy to test](https://almjenkins.rgare.net/job/ActuarialSolutions/job/Stable%20Value/job/deploy%20to%20test/)

---

## Code Quality

* [SonarQube](https://stlpsnrqube01.rgare.net/dashboard?id=StableValueUI)

---

## Hosts

* **dev**, **test**, **uat**
    * stldstbvldock01.rgare.net
    * stldstbvldock02.rgare.net
    * stldstbvldock03.rgare.net
* **prod**
    * stlpstbvldock01.rgare.net
    * stlpstbvldock02.rgare.net
    * stlpstbvldock03.rgare.net
    
---

## Secret/Password Management - Vault

RGA has a vault secret store to manage application configuration and secrets.

All secrets for NON-PROD fast and StableValue is all put into StableValue_dev (since they all share the same deployment scripts)
