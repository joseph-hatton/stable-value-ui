import React from 'react'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import {Tabs, Tab} from 'material-ui/Tabs'
import WatchLists from './watchLists/WatchLists'
import ActionStepsReasons from './actionStepsReasons/ActionStepsReasons'
import {changeCurrentTab} from './watchLists/WatchListActions'

const style = {
  tab: {backgroundColor: '#0062cc'}
}

class WatchListContainer extends React.Component {
  render () {
    const {changeTab, children} = this.props
    return ((children) || (<Tabs primary={true}>
      <Tab id="WatchListTab" onActive={() => changeTab('watchLists')} style={style.tab} label="Watch Lists"><WatchLists /></Tab>
      <Tab id="ActionStepsReasonTab" onActive={() => changeTab('actionStepsReasons')} style={style.tab} label="Action Steps & Reasons"><ActionStepsReasons /></Tab>
    </Tabs>))
  }
}

WatchListContainer.propTypes = {
  children: T.element,
  changeTab: T.func.isRequired
}

const actions = {
  changeTab: (tab) => (dispatch) => {
    dispatch(changeCurrentTab(tab))
  }
}

export default connect(() => {}, actions)(WatchListContainer)
