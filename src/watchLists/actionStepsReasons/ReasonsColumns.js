import React from 'react'
import store from '../../store/store'
import {MultiActionMenuLink} from '../../components/common/MultiActionMenu'
import {selectReason} from './ReasonsActions'

export const ReasonsColumns = () => ([
  {
    field: 'description',
    sort: true,
    label: 'Description',
    renderer: (cell, row) => { // eslint-disable-line react/display-name
      return (
        <MultiActionMenuLink dialogName='reasonMenu' row={row} cell={cell} selections={(row) => {
          store.dispatch(selectReason(row))
        }} />
      )
    },
    customProps: {
      textAlign: 'left',
      width: '100%'
    }
  }
])
