import React from 'react'
import {RaisedButton} from 'material-ui'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import ReduxDialog from '../../components/redux-dialog/redux-dialog'
import {closeDialog} from '../../components/redux-dialog'
import {reduxForm, Field, reset} from 'redux-form'
import {TextField} from 'redux-form-material-ui'
import {saveActionStep} from './ActionStepsActions'
import {hasEntitlement} from '../../entitlements'

const formId = 'ActionStepAddEditForm'
let title
class ActionStepAddEdit extends React.Component {
  render () {
    const {saveActionStep, closeDialog, handleSubmit, actionStepId, reset} = this.props
    title = `${actionStepId ? 'Edit' : 'Add'} Action Step`
    const actions = [
      <RaisedButton
        key="Save"
        label="Save"
        primary={true}
        style={{marginRight: 20}}
        type="submit"
        form={formId}
        disabled={!(hasEntitlement(actionStepId ? 'EDIT_ACTION_STEP' : 'ADD_ACTION_STEP'))}
      />,
      <RaisedButton
        key="Cancel"
        label="Cancel"
        onClick={() => {
          closeDialog('actionStepAddEdit')
          reset(formId)
        }}
      />
    ]
    return (<ReduxDialog
        dialogName="actionStepAddEdit"
        title={title}
        actions={actions}
        modal={true}
        autoScrollBodyContent={true}><form id={formId} onSubmit={this.props.handleSubmit(saveActionStep)}>
      <Field style={{width: '100%'}} floatingLabelText='Description' component={TextField} name='description' />
    </form></ReduxDialog>)
  }
}

ActionStepAddEdit.propTypes = {
  closeDialog: T.func.isRequired,
  handleSubmit: T.func.isRequired,
  saveActionStep: T.func.isRequired,
  reset: T.func.isRequired,
  initialValues: T.object,
  actionStepId: T.number
}

const ActionStepAddEditForm = reduxForm({
  form: formId,
  enableReinitialize: true,
  onSubmitSuccess: (res, dispatch) => dispatch(reset(formId))
})(ActionStepAddEdit)

const actions = {
  closeDialog,
  saveActionStep,
  reset
}

const mapStateToProps = ({watchLists: {selectedActionStep}}) => ({
  actionStepId: selectedActionStep.actionStepId,
  initialValues: selectedActionStep
})

export default connect(mapStateToProps, actions)(ActionStepAddEditForm)
