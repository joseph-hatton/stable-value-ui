import http from '../../actions/http'
import {apiPath} from '../../config'

export const deleteReason = ({reasonId}) => (dispatch) => {
  dispatch({
    type: 'DELETE_REASON',
    payload: http.delete(`${apiPath}/reasons/${reasonId}`, null, {successMessage: 'Reason Deleted Successfully!'})
  })
}

export const selectReason = (reason) => (dispatch) => {
  dispatch({
    type: 'SELECT_REASON',
    payload: reason
  })
}

export const saveReason = () => (dispatch, getState) => {
  const {form: {ReasonAddEditForm: {values}}, watchLists: {selectedReason}} = getState()
  const reasonId = selectedReason.reasonId
  dispatch({
    type: 'SAVE_REASON',
    payload: http[reasonId ? 'put' : 'post'](`${apiPath}/reasons${reasonId ? `/${reasonId}` : ''}`, Object.assign({standardSw: true}, selectedReason, values), {
      successMessage: 'Reason Saved Successfully!'
    }, 'reasonAddEdit')
  })
}
