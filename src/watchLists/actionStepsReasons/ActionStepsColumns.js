import React from 'react'
import store from '../../store/store'
import {MultiActionMenuLink} from '../../components/common/MultiActionMenu'
import {selectActionStep} from './ActionStepsActions'

export const ActionStepsColumns = () => ([
  {
    field: 'description',
    sort: true,
    label: 'Description',
    renderer: (cell, row) => { // eslint-disable-line react/display-name
      return (
        <MultiActionMenuLink dialogName='actionStepMenu' row={row} cell={cell} selections={(row) => {
          store.dispatch(selectActionStep(row))
        }} />
      )
    },
    customProps: {
      textAlign: 'left',
      width: '100%'
    }
  }
])
