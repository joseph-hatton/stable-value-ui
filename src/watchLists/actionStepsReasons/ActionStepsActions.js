import http from '../../actions/http'
import {apiPath} from '../../config'

export const deleteActionStep = ({actionStepId}) => (dispatch) => {
  dispatch({
    type: 'DELETE_ACTION_STEP',
    payload: http.delete(`${apiPath}/action-steps/${actionStepId}`, null, {successMessage: 'Action Step Deleted Successfully!'})
  })
}

export const selectActionStep = (actionStep) => (dispatch) => {
  dispatch({
    type: 'SELECT_ACTION_STEP',
    payload: actionStep
  })
}

export const saveActionStep = () => (dispatch, getState) => {
  const {form: {ActionStepAddEditForm: {values}}, watchLists: {selectedActionStep}} = getState()
  const actionStepId = selectedActionStep.actionStepId
  dispatch({
    type: 'SAVE_ACTION_STEP',
    payload: http[actionStepId ? 'put' : 'post'](`${apiPath}/action-steps${actionStepId ? `/${actionStepId}` : ''}`, Object.assign({standardSw: true}, selectedActionStep, values), {
      successMessage: 'Action Step Saved Successfully!'
    }, 'actionStepAddEdit')
  })
}
