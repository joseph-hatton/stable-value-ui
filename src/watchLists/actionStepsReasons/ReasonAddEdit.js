import React from 'react'
import {RaisedButton} from 'material-ui'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import ReduxDialog from '../../components/redux-dialog/redux-dialog'
import {closeDialog} from '../../components/redux-dialog'
import {reduxForm, Field, reset} from 'redux-form'
import {TextField} from 'redux-form-material-ui'
import {saveReason} from './ReasonsActions'
import {hasEntitlement} from '../../entitlements'

const formId = 'ReasonAddEditForm'
let title
class ReasonAddEdit extends React.Component {
  render () {
    const {saveReason, closeDialog, handleSubmit, reasonId, reset} = this.props
    title = `${reasonId ? 'Edit' : 'Add'} Reason`
    const actions = [
      <RaisedButton
        key="Save"
        label="Save"
        primary={true}
        style={{marginRight: 20}}
        type="submit"
        form={formId}
        disabled={!hasEntitlement('MODIFY_REASON')}
      />,
      <RaisedButton
        id="AssignContact-Cancel"
        key="Cancel"
        label="Cancel"
        onClick={() => {
          closeDialog('reasonAddEdit')
          reset(formId)
        }}
      />
    ]
    return (<ReduxDialog
        dialogName="reasonAddEdit"
        title={title}
        actions={actions}
        modal={true}
        autoScrollBodyContent={true}><form id={formId} onSubmit={this.props.handleSubmit(saveReason)}>
      <Field style={{width: '100%'}} floatingLabelText='Description' component={TextField} name='description' />
    </form></ReduxDialog>)
  }
}

ReasonAddEdit.propTypes = {
  closeDialog: T.func.isRequired,
  handleSubmit: T.func.isRequired,
  saveReason: T.func.isRequired,
  reset: T.func.isRequired,
  initialValues: T.object,
  reasonId: T.number
}

const ReasonAddEditForm = reduxForm({
  form: formId,
  enableReinitialize: true,
  onSubmitSuccess: (res, dispatch) => dispatch(reset(formId))
})(ReasonAddEdit)

const actions = {
  closeDialog,
  saveReason,
  reset
}

const mapStateToProps = ({watchLists: {selectedReason}}) => ({
  reasonId: selectedReason.reasonId,
  initialValues: selectedReason
})

export default connect(mapStateToProps, actions)(ReasonAddEditForm)
