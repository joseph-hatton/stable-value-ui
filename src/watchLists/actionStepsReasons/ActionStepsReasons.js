import React from 'react'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import {Card, CardTitle} from 'material-ui/Card'
import {loadActionSteps} from '../watchLists/WatchListActions'
import {Grid, CreateGridConfig} from '../../components/grid'
import {ActionStepsColumns} from './ActionStepsColumns'
import {ReasonsColumns} from './ReasonsColumns'
import ActionStepsAddEdit from './ActionStepsAddEdit'
import ReasonAddEdit from './ReasonAddEdit'
import {selectActionStep, deleteActionStep} from './ActionStepsActions'
import {openDialog, closeDialog, openConfirmationAlert} from '../../components/redux-dialog'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import NavigationClose from 'material-ui/svg-icons/navigation/close'
import {blue500, red500} from 'material-ui/styles/colors'
import {hasEntitlement} from '../../entitlements'
import MultiActionMenu from '../../components/common/MultiActionMenu'
import {Menu, MenuItem} from 'material-ui/Menu'
import {selectReason, deleteReason} from './ReasonsActions'
import {SpeedDial, SpeedDialItem} from '../../components/fab-dial'
import styles from '../../styles/styles'

const config = {
  showHeaderToolbar: false,
  paginate: false,
  height: 675
}

class ActionStepsReasons extends React.Component {
  componentDidMount () {
    this.props.loadActionSteps()
  }

  openActionStep = (actionStep) => {
    const {openDialog, selectActionStep} = this.props
    selectActionStep(actionStep)
    openDialog('actionStepAddEdit')
  }

  openReason = (reason) => {
    const {openDialog, selectReason} = this.props
    selectReason(reason)
    openDialog('reasonAddEdit')
  }

  render () {
    const {actionSteps, reasons, deleteActionStep, deleteReason, selectActionStep, selectReason, selectedActionStep, selectedReason, openDialog, openConfirmationAlert, closeDialog, currentTab} = this.props
    const options = CreateGridConfig(actionSteps, config)
    return (<div style={{display: 'grid', gridColumnGap: 15, gridTemplateColumns: 'Calc(50% - 7.5px) Calc(50% - 7.5px)', padding: 15}}>
      <Card>
        <CardTitle style={{fontSize: 20}}>Action Steps</CardTitle>
        <Grid sortBy="description" sortDirection='asc' onRowDoubleClick={this.openActionStep} columns={ActionStepsColumns()} options={options} data={actionSteps}></Grid>
        <ActionStepsAddEdit />
        <MultiActionMenu dialogName='actionStepMenu'>
          <Menu>
            <MenuItem id='action-steps-floating-view-edit-button' primaryText="View/Edit Action Step" leftIcon={<EditIcon color={blue500}/>} onClick={() => {
              closeDialog('actionStepMenu')
              openDialog('actionStepAddEdit')
            }} />
            <MenuItem id='action-steps-floating-delete-button' primaryText="Delete Action Step" leftIcon={<DeleteIcon color={red500}/>} onClick={() => {
              closeDialog('actionStepMenu')
              openConfirmationAlert({
                message: 'Are you sure you want to delete this Action Step?',
                onOk: () => deleteActionStep(selectedActionStep)
              })
            }} />
          </Menu>
        </MultiActionMenu>
      </Card>
      <Card>
        <CardTitle style={{fontSize: 20}}>Reasons</CardTitle>
        <Grid sortBy="description" sortDirection='asc' onRowDoubleClick={this.openReason} columns={ReasonsColumns()} options={options} data={reasons}></Grid>
        <ReasonAddEdit/>
        <MultiActionMenu dialogName='reasonMenu'>
          <Menu>
            <MenuItem id='reason-floating-view-edit-button' primaryText="View/Edit Reason" leftIcon={<EditIcon color={blue500}/>} onClick={() => {
              closeDialog('reasonMenu')
              openDialog('reasonAddEdit')
            }} />
            <MenuItem id='reason-floating-delete-button' primaryText="Delete Reason" leftIcon={<DeleteIcon color={red500}/>} onClick={() => {
              closeDialog('reasonMenu')
              openConfirmationAlert({
                message: 'Are you sure you want to delete this Reason?',
                onOk: () => deleteReason(selectedReason)
              })
            }} />
          </Menu>
        </MultiActionMenu>
      </Card>
      {currentTab === 'actionStepsReasons' && hasEntitlement('ADD_ACTION_STEP') &&
        <SpeedDial style={styles.fab} fabContentOpen={<ContentAdd/>} fabContentClose={<NavigationClose/>}>
          <SpeedDialItem id='adding-new-Reasons' label="Reason" onClick={() => { selectReason({}); openDialog('reasonAddEdit') }}/>
          <SpeedDialItem id='adding-new-ActionSteps' label="Action Step" onClick={() => { selectActionStep({}); openDialog('actionStepAddEdit') }}/>
        </SpeedDial>
      }
    </div>)
  }
}

ActionStepsReasons.propTypes = {
  actionSteps: T.array,
  reasons: T.array,
  selectedActionStep: T.object,
  loadActionSteps: T.func.isRequired,
  selectActionStep: T.func.isRequired,
  selectedReason: T.object,
  openDialog: T.func.isRequired,
  closeDialog: T.func.isRequired,
  deleteActionStep: T.func.isRequired,
  deleteReason: T.func.isRequired,
  selectReason: T.func.isRequired,
  openConfirmationAlert: T.func.isRequired,
  currentTab: T.string
}

const actions = {
  loadActionSteps,
  selectActionStep,
  openDialog,
  closeDialog,
  deleteActionStep,
  deleteReason,
  selectReason,
  openConfirmationAlert
}

const mapStateToProps = ({watchLists: {actionSteps, selectedActionStep, selectedReason, reasons, currentTab}}) => ({
  actionSteps,
  currentTab,
  reasons,
  selectedActionStep,
  selectedReason
})

export default connect(mapStateToProps, actions)(ActionStepsReasons)
