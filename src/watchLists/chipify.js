import React from 'react'
import Chip from 'material-ui/Chip'

const styles = {
  chip: {
    margin: 4,
    maxWidth: '98%'
  },
  allChips: {
    fontSize: '12px'
  },
  inlineChip: {
    margin: 4,
    maxWidth: '98%',
    display: 'inline-flex'
  },
  wrapper: {
    maxWidth: '100%',
    textOverflow: 'ellipsis',
    overflow: 'hidden'
  },
  inlineWrapper: {
    padding: '15px'
  }
}

export const chipify = (cell, row, inline, onDelete) => {
  let res = []
  cell.forEach((item, index) => {
    res.push(<Chip key={index} onRequestDelete={onDelete ? (e) => onDelete(e, item) : null}
    labelStyle={Object.assign(styles.allChips, (inline ? {} : styles.wrapper))}
    title={item.description}
    style={inline ? styles.inlineChip : styles.chip}>
      {item.description}
    </Chip>)
  })
  return (<div style={inline ? styles.inlineWrapper : styles.wrapper}>{res}</div>)
}
