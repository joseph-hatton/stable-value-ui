import React from 'react'
import {RaisedButton} from 'material-ui'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import ReduxDialog from '../../components/redux-dialog/redux-dialog'
import {loadAll} from '../watchLists/WatchListActions'
import {chipify} from '../chipify'
import {closeDialog} from '../../components/redux-dialog'

export const dialogName = 'watchListAlert'

class WatchListAlert extends React.Component {
  componentWillMount () {
    if (this.props.all.length === 0) {
      this.props.loadAll()
    }
  }
  render () {
    const {currentContractWatchListItem, closeDialog} = this.props
    const {shortPlanName, reasons, actionSteps, comments} = currentContractWatchListItem
    const actions = [
      <RaisedButton
        key="Ok"
        label="Ok"
        primary={true}
        onClick={() => {
          closeDialog(dialogName)
        }}
      />
    ]
    return (<ReduxDialog
        dialogName={dialogName}
        title={`Warning: ${shortPlanName} is on a Watch List`}
        actions={actions}
        modal={true}
        autoScrollBodyContent={true}>
      <br/>
      <p><strong>{comments}</strong></p>
      Reason{reasons.length <= 1 || 's'}: {chipify(reasons, null, true)}
      Action Step{actionSteps.length <= 1 || 's'}: {chipify(actionSteps, null, true)}
    </ReduxDialog>)
  }
}

WatchListAlert.propTypes = {
  all: T.array.isRequired,
  currentContractWatchListItem: T.object.isRequired,
  loadAll: T.func.isRequired,
  closeDialog: T.func.isRequired
}

const actions = {
  loadAll,
  closeDialog
}

const mapStateToProps = ({watchLists: {currentContractWatchListItem, all}}) => ({
  currentContractWatchListItem,
  all
})

export default connect(mapStateToProps, actions)(WatchListAlert)
