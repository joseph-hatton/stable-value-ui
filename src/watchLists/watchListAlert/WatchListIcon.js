import React from 'react'
import {RaisedButton} from 'material-ui'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import {loadAll} from '../watchLists/WatchListActions'
import {chipify} from '../chipify'
import {closeDialog} from '../../components/redux-dialog'
import Popover from 'material-ui/Popover'
import WarningIcon from 'material-ui/svg-icons/alert/warning'
import {setContractWatchlistItem} from './WatchListAlertActions'

const style = {
  padding: '15px',
  maxWidth: '400px'
}

class WatchListAlert extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      open: false
    }
  }

  componentWillMount () {
    this.props.setContractWatchlistItem(this.props.contractId)
  }

  handleClick = (e) => {
    e.preventDefault()
    this.setState({
      anchorEl: e.currentTarget,
      open: true
    })
  }

  handleRequestClose = () => {
    this.setState({
      open: false
    })
  }

  render () {
    const {currentContractWatchListItem, all} = this.props
    const {shortPlanName, reasons, actionSteps, comments, notInUse} = currentContractWatchListItem
    return (<span>
      {all.length === 0 || notInUse || (<WarningIcon id="watchList-Icon" style={{cursor: 'pointer', color: '#FF6E40'}} title={`Click to See More.`} onClick={this.handleClick} />)}
      <Popover
          id="WatchListIcon-Popover"
          className="popoverFix"
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={this.handleRequestClose}
          style={style}>
        <div>
          <p>Warning: {shortPlanName} is on a Watch List.</p>
          <p><strong>{comments}</strong></p>
          Reason{reasons.length <= 1 || 's'}: {chipify(reasons, null)}
          Action Step{actionSteps.length <= 1 || 's'}: {chipify(actionSteps, null)}
        </div>
      </Popover>
    </span>)
  }
}

WatchListAlert.propTypes = {
  contractId: T.number.isRequired,
  all: T.array.isRequired,
  setContractWatchlistItem: T.func.isRequired,
  loadAll: T.func.isRequired,
  currentContractWatchListItem: T.object.isRequired
}

const actions = {
  setContractWatchlistItem,
  loadAll
}

const mapStateToProps = ({watchLists: {currentContractWatchListItem, all}}) => ({
  currentContractWatchListItem,
  all
})

export default connect(mapStateToProps, actions)(WatchListAlert)
