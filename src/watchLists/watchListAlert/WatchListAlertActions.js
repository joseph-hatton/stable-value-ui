import {openDialog} from '../../components/redux-dialog'
import {dialogName} from './WatchListAlert'
import {loadAll} from '../watchLists/WatchListActions'

export const setContractWatchlistItem = (contractId, cb) => async (dispatch, getState) => {
  await dispatch(loadAll())
  const {watchLists: {all}} = getState()
  const item = all.find((i) => i.contractId === contractId)
  if (item) {
    dispatch({
      type: 'SET_CURRENT_CONTRACT_WATCH_LIST_ITEM',
      payload: item
    })
    cb && cb()
  } else {
    dispatch({
      type: 'SET_CURRENT_CONTRACT_WATCH_LIST_ITEM',
      payload: {reasons: [], actionSteps: [], notInUse: true}
    })
  }
}

export const checkWatchList = (contractId) => async (dispatch, getState) => {
  await setContractWatchlistItem(contractId, () => {
    dispatch(openDialog(dialogName))
  })(dispatch, getState)
}
