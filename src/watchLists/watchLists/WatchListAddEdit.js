import React from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router'
import {reduxForm, Field, change} from 'redux-form'
import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar'
import {Card} from 'material-ui/Card'
import {PropTypes as T} from 'prop-types'
import {DatePicker, SelectField, TextField} from 'redux-form-material-ui'
import MenuItem from 'material-ui/MenuItem'
import { Required } from '../../components/forms/Validators'
import { dateFormat, toUtc } from '../../components/utilities/Formatters'
import ContractPicker from '../../contracts/ContractPicker'
import {loadReasons, loadActionSteps, saveWatchList, loadWatchList} from './WatchListActions'
import IconButton from 'material-ui/IconButton'
import AddIcon from 'material-ui/svg-icons/content/add'
import {chipify} from '../chipify'
import _ from 'lodash'
import RaisedButton from 'material-ui/RaisedButton'
import ReduxDialog from '../../components/redux-dialog'
import { closeDialog } from '../../components/redux-dialog/redux-dialog'

const formId = 'WatchListAddEditForm'

const cardStyle = {
  marginTop: 15
}

export const menuItems = (items, key) => {
  return items.map((i) => (
    <MenuItem key={i[key]} value={i} primaryText={i.description} />
  ))
}

export const displayArray = (items, key, onDelete) => {
  return chipify(_.orderBy(items, ['description']) || [], {}, true, onDelete)
}

class WatchListAddEdit extends React.Component {
  componentWillMount () {
    this.props.loadReasons()
    this.props.loadActionSteps()
  }

  render () {
    const {closeDialog} = this.props
    const actions = [
      <RaisedButton key='save' form={formId} style={{marginRight: 15}} primary={true} label="Save" onClick={this.props.saveWatchList} />,
      <RaisedButton key='cancel' label="Cancel" onClick={() => {
        closeDialog('addEditWatchList')
      }} />
    ]
    return (<ReduxDialog contentStyle={{maxWidth: 1000}} actions={actions} title='Add/Edit Watch List' dialogName='addEditWatchList' modal={true} autoScrollBodyContent={true}>
      <form id={formId} onSubmit={this.props.handleSubmit(WatchListAddEdit)}>
        <div className="row">
          <div className="col-md-6">
            <ContractPicker name="contractId" floatingLabelText="* Contract" validate={Required} />
          </div>
          <div className="col-md-6">
            <Field fullWidth name="effectiveDate" format={toUtc} formatDate={dateFormat} component={DatePicker} firstDayOfWeek={0} floatingLabelText="* Effective Date" validate={Required} autoOk />
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <Field style={{width: '100%'}} component={TextField} multiLine={true} floatingLabelText="Comments" name="comments" />
          </div>
        </div>
        <Card style={cardStyle} className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <Field multiple style={{width: '100%'}} component={SelectField} floatingLabelText="Add Reasons" name="reasons" >
                {menuItems(this.props.reasons, 'reasonId')}
              </Field>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <h2>Active Reasons</h2>
              {displayArray(this.props.reasonsSelected, 'reasonId', this.props.deleteReason)}
            </div>
          </div>
        </Card>
        <Card style={cardStyle} className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <Field multiple style={{width: '100%'}} component={SelectField} floatingLabelText="Add Action Steps" name="actionSteps" >
                {menuItems(this.props.actionSteps, 'actionStepId')}
              </Field>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <h2>Active Action Steps</h2>
              {displayArray(this.props.actionStepsSelected, 'actionStepId', this.props.deleteActionStep)}
            </div>
          </div>
        </Card>
      </form>
    </ReduxDialog>)
  }
}

WatchListAddEdit.propTypes = {
  handleSubmit: T.func.isRequired,
  loadReasons: T.func.isRequired,
  loadActionSteps: T.func.isRequired,
  deleteReason: T.func.isRequired,
  deleteActionStep: T.func.isRequired,
  saveWatchList: T.func.isRequired,
  loadWatchList: T.func.isRequired,
  closeDialog: T.func.isRequired,
  reasons: T.array,
  currentWatchList: T.array,
  actionSteps: T.array,
  reasonsSelected: T.array,
  actionStepsSelected: T.array,
  params: T.object
}

const WatchListAddEditForm = reduxForm({
  form: formId,
  enableReinitialize: true
})(WatchListAddEdit)

const actions = {
  loadReasons,
  loadActionSteps,
  saveWatchList,
  loadWatchList,
  closeDialog,
  deleteReason: (e, item) => (dispatch, getState) => {
    const {form: {WatchListAddEditForm: {values: {reasons}}}} = getState()
    dispatch(change(formId, 'reasons', reasons.filter((i) => i.reasonId !== item.reasonId)))
  },
  deleteActionStep: (e, item) => (dispatch, getState) => {
    const {form: {WatchListAddEditForm: {values: {actionSteps}}}} = getState()
    dispatch(change(formId, 'actionSteps', actionSteps.filter((i) => i.actionStepId !== item.actionStepId)))
  }
}

const mapStateToProps = ({form, watchLists: {reasons, actionSteps, currentWatchList}}) => ({
  reasons,
  actionSteps,
  reasonsSelected: _.get(form, `${formId}.values.reasons`),
  actionStepsSelected: _.get(form, `${formId}.values.actionSteps`),
  initialValues: currentWatchList,
  currentWatchList
})

export default connect(mapStateToProps, actions)(WatchListAddEditForm)
