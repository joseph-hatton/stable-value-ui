import React from 'react'
import {connect} from 'react-redux'
import {PropTypes as T} from 'prop-types'
import {Grid, CreateGridConfig} from '../../components/grid'
import {WatchListColumns} from './WatchListColumns'
import {loadAll, selectWatchList} from './WatchListActions'
import { Link, browserHistory } from 'react-router'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import { red500 } from 'material-ui/styles/colors'
import WatchListMenu from './WatchListMenu'
import {closeDialog, openDialog} from '../../components/redux-dialog/redux-dialog'
import WatchListAddEdit from './WatchListAddEdit'

const config = {
  showHeaderToolbar: false,
  paginate: true,
  defaultSortName: 'effectiveDate',
  defaultSortOrder: 'asc',
  height: window.innerHeight - 200
}

class WatchLists extends React.Component {
  componentDidMount () {
    this.props.loadAll()
  }

  render () {
    const {watchLists, currentTab, selectWatchList, openDialog} = this.props
    const options = CreateGridConfig(watchLists, config)
    return (<div>
      <Grid onRowDoubleClick={(row) => {
        selectWatchList(row)
        openDialog('addEditWatchList')
      }} id="WatchLists" options={options} data={watchLists} columns={WatchListColumns()}/>
      {currentTab === 'watchLists' && <FloatingActionButton id="addNewWatchList-fab" onClick={() => {
          selectWatchList({})
          openDialog('addEditWatchList')
        }} backgroundColor={red500} mini={false} className={'floating-add-button'}>
          <ContentAdd/>
        </FloatingActionButton>}
      <WatchListMenu />
      <WatchListAddEdit/>
    </div>)
  }
}

WatchLists.propTypes = {
  watchLists: T.array,
  loadAll: T.func.isRequired,
  selectWatchList: T.func.isRequired,
  closeDialog: T.func.isRequired,
  openDialog: T.func.isRequired,
  children: T.element,
  currentTab: T.string
}

const actions = {
  loadAll,
  selectWatchList,
  openDialog,
  closeDialog
}

const mapStateToProps = ({watchLists: {all, currentTab}}) => ({
  watchLists: all,
  currentTab
})

export default connect(mapStateToProps, actions)(WatchLists)
