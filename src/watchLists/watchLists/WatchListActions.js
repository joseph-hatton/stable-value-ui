import { apiPath } from '../../config'
import http from '../../actions/http'
import _ from 'lodash'

export const loadAll = () => ({
  type: 'LOAD_ALL_WATCH_LISTS',
  payload: http.get(`${apiPath}/watch-lists`)
})

export const loadReasons = () => ({
  type: 'LOAD_REASONS',
  payload: http.get(`${apiPath}/reasons`)
})

export const loadActionSteps = () => ({
  type: 'LOAD_ACTION_STEPS',
  payload: http.get(`${apiPath}/action-steps`)
})

export const saveWatchList = () => (dispatch, getState) => {
  const {form, watchLists: {currentWatchList: {watchListId}}} = getState()
  const formContent = _.get(form, 'WatchListAddEditForm.values')
  dispatch({
    type: 'LOAD_WATCH_LIST',
    payload: http[watchListId ? 'put' : 'post'](`${apiPath}/watch-lists${watchListId ? `/${watchListId}` : ''}`, formContent, {
      successMessage: `Watch List ${watchListId ? 'Created' : 'Updated'} Successfully`
    }, 'addEditWatchList')
  })
}

export const loadWatchList = (watchListId) => ({
  type: 'LOAD_WATCH_LIST',
  payload: http.get(`${apiPath}/watch-lists/${watchListId}`)
})

export const deleteWatchList = ({watchListId}) => ({
  type: 'DELETE_WATCH_LIST',
  payload: http.delete(`${apiPath}/watch-lists/${watchListId}`, null, { successMessage: 'Watch List Deleted Successfully' })
})

export const selectWatchList = (watchList) => ({
  type: 'SELECT_WATCH_LIST',
  payload: watchList
})

export const changeCurrentTab = (tab) => ({
  type: 'CHANGE_CURRENT_TAB',
  payload: tab
})
