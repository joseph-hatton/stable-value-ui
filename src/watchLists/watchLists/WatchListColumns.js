import React from 'react'
import {dateFormat} from '../../components/grid'
import Badge from 'material-ui/Badge'
import Chip from 'material-ui/Chip'
import _ from 'lodash'
import {chipify} from '../chipify'
import {Link} from 'react-router'
import {WatchListMenuLink} from './WatchListMenu'

const commonWidth = 100 / 7

const commonProps = {
  customProps: {
    width: `${commonWidth}%`,
    textAlign: 'center'
  }
}

export const WatchListColumns = () => ([
  {
    field: 'contractNumber',
    sort: true,
    label: 'Contract',
    renderer: (cell, row) => ( // eslint-disable-line react/display-name
      <WatchListMenuLink cell={cell} row={row} />
    ),
    customProps: {
      ...(commonProps.customProps),
      width: '12%',
      isKey: true,
      textAlignHeader: 'left',
      textAlign: 'left'
    }
  },
  {
    field: 'shortPlanName',
    sort: true,
    label: 'Short Plan Name',
    customProps: {
      ...(commonProps.customProps),
      width: '10%',
      textAlign: 'left'
    }
  },
  {
    field: 'effectiveDate',
    sort: true,
    label: 'Date',
    renderer: dateFormat,
    customProps: {
      ...(commonProps.customProps),
      width: '9%',
      textAlign: 'center'
    }
  },
  {
    field: 'reasons',
    sort: false,
    label: 'Reasons',
    renderer: (cell, row) => _.map(cell, 'description').join(', '),
    customProps: {
      ...(commonProps.customProps),
      width: '20%',
      textAlign: 'left'
    }
  },
  {
    field: 'actionSteps',
    sort: false,
    label: 'Action Steps',
    renderer: (cell, row) => _.map(cell, 'description').join(', '),
    customProps: {
      ...(commonProps.customProps),
      width: '20%',
      textAlign: 'left'
    }
  },
  {
    field: 'comments',
    sort: false,
    label: 'Comments',
    customProps: {
      ...(commonProps.customProps),
      width: '20%',
      textAlign: 'left'
    }
  },
  {
    field: 'modifiedDate',
    sort: true,
    label: 'Last Modified',
    renderer: dateFormat,
    customProps: {
      ...(commonProps.customProps),
      width: '9%',
      textAlign: 'center'
    }
  }
])
