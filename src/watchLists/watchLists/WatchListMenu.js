import React from 'react'
import { PropTypes as T } from 'prop-types'
import { Link } from 'react-router'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import { blue500, red500 } from 'material-ui/styles/colors'
import { connect } from 'react-redux'
import { Popover, Menu, MenuItem } from 'material-ui'
import { openDialog, closeDialog } from '../../components/redux-dialog/redux-dialog'
import { deleteWatchList, selectWatchList } from './WatchListActions'

const MenuLink = ({cell, openDialog, row, selectWatchList}) => {
  return (
    <a href="" onClick={(e) => {
      e.nativeEvent.preventDefault()
      selectWatchList(row)
      openDialog('watchListMenu', {
        anchorEl: e.currentTarget
      })
    }}> {cell} </a>
  )
}

MenuLink.propTypes = {
  cell: T.node,
  row: T.object.isRequired,
  openDialog: T.func.isRequired,
  selectWatchList: T.func.isRequired
}

export const WatchListMenuLink = connect(null, {openDialog, selectWatchList})(MenuLink)

const Divider = () => (<hr style={{margin: 0}}/>)

const WatchListMenu = ({open, anchorEl, closeDialog, openDialog, watchList, deleteWatchList}) => (
  <Popover
    id='WatchListMenu-Popover'
    open={open}
    anchorEl={anchorEl}
    anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
    targetOrigin={{horizontal: 'left', vertical: 'top'}}
    onRequestClose={() => {
      closeDialog('watchListMenu')
    }}
  >
    <Menu>
      <MenuItem primaryText="View/Edit Watch List" leftIcon={<EditIcon color={blue500}/>} onClick={() => {
        closeDialog('watchListMenu')
        openDialog('addEditWatchList')
      }}/>
      <Divider/>
      <MenuItem id="watchListMenu-deleteButton" primaryText="Delete Watch List" onClick={() => {
        deleteWatchList(watchList)
        closeDialog('watchListMenu')
      }} leftIcon={<DeleteIcon color={red500}/>}/>
    </Menu>
  </Popover>
)

WatchListMenu.propTypes = {
  open: T.bool.isRequired,
  anchorEl: T.object,
  closeDialog: T.func.isRequired,
  openDialog: T.func.isRequired,
  deleteWatchList: T.func.isRequired,
  watchList: T.object
}

export const mapStateToProps = ({dialog: {watchListMenu}, watchLists: {selectedWatchList}}) => {
  return {
    open: !!watchListMenu,
    anchorEl: watchListMenu ? watchListMenu.anchorEl : null,
    watchList: selectedWatchList
  }
}

const actions = {
  closeDialog,
  openDialog,
  deleteWatchList
}

export default connect(mapStateToProps, actions)(WatchListMenu)
