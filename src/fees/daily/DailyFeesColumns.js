import React from 'react'
import {currencyFormat, dateFormat} from '../../components/grid'

const commonWidth = 100 / 8

const commonProps = {
  customProps: {
    width: `${commonWidth}%`,
    textAlign: 'center'
  }
}

const invoiceWidthVariant = 6

export const DailyFeesColumns = () => ([
  {
    field: 'effectiveDate',
    sort: true,
    label: 'As of Date',
    renderer: dateFormat,
    customProps: {
      ...(commonProps.customProps),
      isKey: true
    }
  },
  {
    field: 'beginningFee',
    sort: true,
    label: 'Beginning Balance',
    renderer: currencyFormat,
    ...commonProps
  },
  {
    field: 'accruedFee',
    sort: true,
    label: 'Daily Fee',
    renderer: currencyFormat,
    customProps: {
      ...commonProps.customProps,
      width: `${commonWidth - (invoiceWidthVariant / 2)}%`
    }
  },
  {
    field: 'accruedFeeMtd',
    sort: true,
    label: 'MTD Fee',
    renderer: currencyFormat,
    customProps: {
      ...commonProps.customProps,
      width: `${commonWidth - (invoiceWidthVariant / 2)}%`
    }
  },
  {
    field: 'feeReceived',
    sort: true,
    label: 'Fee Received',
    renderer: currencyFormat,
    ...commonProps
  },
  {
    field: 'feeAdjustments',
    sort: true,
    label: 'Fee Earned Adj.',
    renderer: currencyFormat,
    ...commonProps
  },
  {
    field: 'endingFee',
    sort: true,
    label: 'Ending Fee Balance',
    renderer: currencyFormat,
    ...commonProps
  },
  {
    field: 'invoiceNumbers',
    sort: true,
    label: 'Paid Invoice Numbers',
    renderer: (cell, row) => cell.join(', '),
    customProps: {
      ...commonProps.customProps,
      width: `${commonWidth + invoiceWidthVariant}%`
    }
  }
])
