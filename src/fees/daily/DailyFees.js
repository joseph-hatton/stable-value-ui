import React from 'react'
import {connect} from 'react-redux'
import {Grid, CreateGridConfig} from '../../components/grid'
import {PropTypes as T} from 'prop-types'
import {DailyFeesColumns} from './DailyFeesColumns'
import {selectDailyFees, setContractId, filterDailyFees} from '../FeeActions'
import DailyFeesFilter from './DailyFeesFilter'

const config = {
  showHeaderToolbar: false,
  paginate: true,
  defaultSortName: 'effectiveDate',
  defaultSortOrder: 'asc',
  height: window.innerHeight - 200
}

class DailyFees extends React.Component {
  componentDidMount () {
    this.props.setContractId(this.props.params.contractId)
    if (this.props.filtered) {
      this.props.filterDailyFees()
    } else {
      this.props.selectDailyFees(this.props.params.contractId)
    }
  }

  render () {
    const {loadedFees} = this.props
    const options = CreateGridConfig(loadedFees, config)
    return (
      <div>
        <DailyFeesFilter contractId={this.props.params.contractId}/>
        <Grid id="DailyFees" options={options} data={loadedFees}
          columns={DailyFeesColumns()}/>
      </div>
    )
  }
}

DailyFees.propTypes = {
  params: T.object,
  loadedFees: T.array,
  selectDailyFees: T.func.isRequired,
  setContractId: T.func.isRequired,
  filterDailyFees: T.func.isRequired,
  filtered: T.bool
}

const actions = {
  selectDailyFees,
  setContractId,
  filterDailyFees
}

const mapStateToProps = ({fees: {loadedFee, filtered}}) => ({
  loadedFees: loadedFee,
  filtered
})

export default connect(mapStateToProps, actions)(DailyFees)
