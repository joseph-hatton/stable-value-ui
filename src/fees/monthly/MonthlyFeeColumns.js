import React from 'react'
import {currencyFormat} from '../../components/grid'
import { Link } from 'react-router'

const commonProps = {
  customProps: {
    width: `${100 / 6}%`
  }
}

export const MonthlyFeeColumns = () => ([
  {
    field: 'contractNumber',
    sort: true,
    label: 'Contract #',
    renderer: (cell, row) => ( // eslint-disable-line react/display-name
      <Link title={row.shortPlanName} to={`/fees/${row.contractId}`}>{cell}</Link>
    ),
    customProps: {
      ...(commonProps.customProps),
      isKey: true,
      textAlignHeader: 'left'
    }
  },
  {
    field: 'shortPlanName',
    sort: true,
    label: 'Plan Name',
      customProps: {
        ...(commonProps.customProps),
        textAlignHeader: 'left'
      }
  },
  {
    field: 'accruedFeeMtd',
    sort: true,
    label: 'Accrued Fee Mtd',
    renderer: currencyFormat,
    customProps: {
      ...commonProps.customProps,
      textAlign: 'right'
    }
  },
  {
    field: 'feeReceived',
    sort: true,
    label: 'Fee Received Amount',
    renderer: currencyFormat,
    customProps: {
      ...commonProps.customProps,
      textAlign: 'right'
    }
  },
  {
    field: 'feeAdjustments',
    sort: true,
    label: 'Fee Earned Adjustments',
    renderer: currencyFormat,
    customProps: {
      ...commonProps.customProps,
      textAlign: 'right'
    }
  },
  {
    field: 'endingFee',
    sort: true,
    label: 'Fee Balance Amount',
    renderer: currencyFormat,
    customProps: {
      ...commonProps.customProps,
      textAlign: 'right'
    }
  }
])
