import React from 'react'
import {connect} from 'react-redux'
import {browserHistory} from 'react-router'
import {Grid, CreateGridConfig} from '../../components/grid'
import {PropTypes as T} from 'prop-types'
import { Card } from 'material-ui/Card'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import {MonthlyFeeColumns} from './MonthlyFeeColumns'
import MonthlyFeesFilter from './MonthlyFeesFilter'
import {loadAllMonthlyFees} from '../FeeActions'

const config = {
  showHeaderToolbar: false,
  paginate: true,
  defaultSortName: 'contractNumber',
  defaultSortOrder: 'asc',
  height: window.innerHeight - 200
}

const open = ({contractId}) => {
  browserHistory.push(`/fees/${contractId}`)
}

class MonthlyFees extends React.Component {
  componentDidMount () {
    if (!this.props.filtered) {
      this.props.loadAllMonthlyFees()
    }
  }

  render () {
    const {MonthlyFees} = this.props
    const options = CreateGridConfig(MonthlyFees, config)
    return (
      (this.props.children) || (<div>
        <MonthlyFeesFilter/>
        <Grid onRowDoubleClick={open} id="MonthlyFees" options={options} data={MonthlyFees} columns={MonthlyFeeColumns()}/>
      </div>)
    )
  }
}

MonthlyFees.propTypes = {
  MonthlyFees: T.array,
  children: T.element,
  filtered: T.bool,
  loadAllMonthlyFees: T.func.isRequired
}

const actions = {
  loadAllMonthlyFees
}

const mapStateToProps = ({fees: {allFees, filtered}}) => ({
  MonthlyFees: allFees,
  filtered
})

export default connect(mapStateToProps, actions)(MonthlyFees)
