import React from 'react'
import _ from 'lodash'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import {reduxForm, reset, change, Field} from 'redux-form'
import RaisedButton from 'material-ui/RaisedButton'
import IconButton from 'material-ui/IconButton'
import Clear from 'material-ui/svg-icons/content/clear'
import {TextField} from 'redux-form-material-ui'
import {dateFormat} from '../../components/utilities/Formatters'
import {Card} from 'material-ui/Card'
import {filterMonthly, loadAllMonthlyFees, setDateFilter, setFilterOnDay} from '../FeeActions'
import moment from 'moment'
import YearFormField from '../../components/forms/YearFormField'
import MonthFormField from '../../components/forms/MonthFormField'
import DayFormField from '../../components/forms/DayFormField'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'

const formId = 'MonthlyFeesFilterForm'

const showDateFilter = ({clearFilter, yearFilter, monthFilter, yearChange, monthChange, dayChange}) => {
  return (
    <Card>
      <Toolbar style={{borderTop: 'solid 1px #e0e0e0', backgroundColor: '#f8f9fa', height: '75px'}}>
        <ToolbarGroup>
          <YearFormField style={{marginRight: '5px'}} floatingLabelText="Filter by Year" name="yearFilter" onChange={yearChange} />
          <MonthFormField style={{marginRight: '5px'}} floatingLabelText="Filter by Month" name="monthFilter" onChange={monthChange} />
          <DayFormField year={yearFilter} month={monthFilter - 1} floatingLabelText="Filter by Day" name="dayFilter" onChange={dayChange} />
        </ToolbarGroup>
        <ToolbarGroup>
          <RaisedButton style={{margin: '20px'}} label="Clear Filter" onClick={clearFilter} />
        </ToolbarGroup>
      </Toolbar>
    </Card>
  )
}

showDateFilter.propTypes = {
  clearFilter: T.func.isRequired,
  dateFilter: T.obj,
  yearChange: T.func.isRequired,
  monthChange: T.func.isRequired,
  dayChange: T.func.isRequired,
  yearFilter: T.number.isRequired,
  monthFilter: T.number.isRequired
}

const MonthlyFeesFilter = (props) => {
  return (
      <form id={formId}>
        { showDateFilter(props) }
      </form>
  )
}

const MonthlyFeesFilterForm = reduxForm({
  form: formId
})(MonthlyFeesFilter)

const actions = {
  clearFilter: () => (dispatch, getState) => {
    const {fees: {dateFilter}} = getState()
    dateFilter.year(moment().year())
    dateFilter.month(moment().month())
    dispatch(reset('MonthlyFeesFilterForm'))
    dispatch(setDateFilter(dateFilter))
    dispatch(setFilterOnDay(false))
    dispatch(loadAllMonthlyFees())
  },
  yearChange: (e, year) => (dispatch, getState) => {
    const {fees: {dateFilter}} = getState()
    dateFilter.year(year)
    dispatch(change('MonthlyFeesFilterForm', 'yearFilter', year))
    dispatch(setDateFilter(dateFilter))
    dispatch(filterMonthly())
  },
  monthChange: (e, month) => (dispatch, getState) => {
    const {fees: {dateFilter}} = getState()
    dateFilter.month(month - 1)
    dispatch(change('MonthlyFeesFilterForm', 'monthFilter', month))
    dispatch(setDateFilter(dateFilter))
    dispatch(filterMonthly())
  },
  dayChange: (e, day) => (dispatch, getState) => {
    const {fees: {dateFilter}} = getState()
    dateFilter.date(day)
    dispatch(change('MonthlyFeesFilterForm', 'dayFilter', day))
    dispatch(setDateFilter(dateFilter))
    dispatch(setFilterOnDay(true))
    dispatch(filterMonthly())
  }
}

const mapStateToProps = ({form, fees: {dateFilter}}) => {
  return {
    yearFilter: _.get(form, 'MonthlyFeesFilterForm.values.yearFilter'),
    monthFilter: _.get(form, 'MonthlyFeesFilterForm.values.monthFilter'),
    initialValues: {
      yearFilter: moment().year(),
      monthFilter: moment().month() + 1
    }
  }
}

export default connect(mapStateToProps, actions)(MonthlyFeesFilterForm)
