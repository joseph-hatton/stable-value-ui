import { apiPath } from '../config'
import http from '../actions/http'
import moment from 'moment'

export const loadAllMonthlyFees = () => ({
  type: 'LOAD_ALL_FEES',
  payload: http.get(`${apiPath}/balances/monthly`)
})

export const selectDailyFees = (contractId) => (dispatch, getState) => {
  const {fees: {currentContractId}} = getState()
  contractId = contractId || currentContractId
  dispatch({
    type: 'SELECT_FEES_BY_CONTRACT_ID',
    payload: Promise.all([http.get(`${apiPath}/balances/daily`, {contractId}),
      http.get(`${apiPath}/contracts/${contractId}`)])
  })
}

export const filterMonthly = () => (dispatch, getState) => {
  const {fees: {dateFilter, filterOnDay}} = getState()
  dateFilter.hour(12)

  const queries = {dateFilter: dateFilter.format()}

  if (filterOnDay) {
    queries.filterOnDay = true
  }

  dispatch({
    type: 'FILTER_ALL_FEES',
    payload: http.get(`${apiPath}/balances/monthly`, queries)
  })
}

export const filterDailyFees = () => (dispatch, getState) => {
  const {fees: {currentContractId, dateFilter}} = getState()

  const queries = {
    contractId: currentContractId,
    dateFilter: dateFilter.format()
  }

  dispatch({
    type: 'FILTER_FEES_BY_CONTRACT_ID',
    payload: http.get(`${apiPath}/balances/daily`, queries)
  })
}

export const setContractId = (contractId) => ({
  type: 'SET_CONTRACT_ID',
  payload: contractId
})

export const clearContractId = () => ({
  type: 'SET_CONTRACT_ID',
  payload: null
})

export const setDateFilter = (date) => ({
  type: 'SET_DATE_FILTER',
  payload: date
})

export const setFilterOnDay = (filterOnDay) => ({
  type: 'SET_FILTER_ON_DAY',
  payload: filterOnDay
})
