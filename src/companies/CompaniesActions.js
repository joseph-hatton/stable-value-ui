import { browserHistory } from 'react-router'
import { apiPath } from '../config'
import http, {STANDARD_ERROR_MESSAGE} from '../actions/http'
import _ from 'lodash'

const COMPANIES_HOME = '/companies'

export const loadAllCompanies = () => {
  return {
    type: 'LOAD_ALL_COMPANIES',
    payload: http.get(`${apiPath}/companies`)
  }
}

export const saveCompany = () => (dispatch, getState) => {
  const {form: {AddEditCompanyForm}, companies: {selectedCompany}} = getState()
  const method = selectedCompany.companyId ? 'put' : 'post'
  const url = selectedCompany.companyId ? `${apiPath}/companies/${selectedCompany.companyId}` : `${apiPath}/companies`
  if (!AddEditCompanyForm.values.address2) { // Remove NULL fields to Support API validation
    delete AddEditCompanyForm.values.address2
  }
  if (selectedCompany.companyId) {
    delete AddEditCompanyForm.values.receivesEdiSw
  }
  dispatch({
    type: 'SAVE_COMPANY',
    payload: http[method](url, _.omit(AddEditCompanyForm.values, ['contacts']), {successMessage: 'Company saved successfully'}, 'addEditCompany')
  })
}

export const loadCompany = (companyId) => (dispatch) => {
  dispatch({
    type: 'LOAD_COMPANY',
    payload: http.get(`${apiPath}/companies/${companyId}`)
  })
}

const errorMessage = (error) => _.get(error, 'response.body.message', '').includes('ORA-02292: integrity constraint') ?
  'Cannot delete a company that has contacts associated with it.' : STANDARD_ERROR_MESSAGE

export const deleteCompany = ({companyId}) => (dispatch) => {
  dispatch({
    type: 'DELETE_COMPANY',
    payload: http.delete(`${apiPath}/companies/${companyId}`, null, {successMessage: 'Company deleted successfully', errorMessage})
  })
}
export const selectContacts = ({contacts}) => ({
  type: 'SELECT_CONTACTS',
  contacts
})
export const selectCompany = (company) => ({
  type: 'SELECT_COMPANY',
  company
})
