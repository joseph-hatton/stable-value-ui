import React from 'react'
import { PropTypes as T } from 'prop-types'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import { blue500 } from 'material-ui/styles/colors'
import { connect } from 'react-redux'
import { Popover, Menu, MenuItem } from 'material-ui'
import { openDialog, closeDialog } from '../components/redux-dialog/redux-dialog'
import { selectContacts } from './CompaniesActions'
import { selectAndLoadContact } from '../contacts/ContactActions'

const MenuLink = ({cell, openDialog, row, selectContacts}) => {
  return (
    <a href="" onClick={(e) => {
      e.nativeEvent.preventDefault()
      if (row.contacts.length > 0) {
        selectContacts(row)
        openDialog('contactsMenu', {
          anchorEl: e.currentTarget
        })
      }
    }}> {cell} </a>
  )
}

MenuLink.propTypes = {
  cell: T.node,
  row: T.object.isRequired,
  openDialog: T.func.isRequired,
  selectContacts: T.func.isRequired
}

export const ContactsMenuLink = connect(null, {openDialog, selectContacts})(MenuLink)

export const parseName = ({lastName, firstName, middleName}) => (
  lastName + (firstName ? `, ${firstName}` : '') + (middleName ? `, ${middleName}` : '')
)

const Divider = () => (<hr style={{margin: 0}}/>)

const ContactsMenu = ({open, anchorEl, closeDialog, openDialog, selectedContacts, selectAndLoadContact}) => (
  <Popover
    open={open}
    anchorEl={anchorEl}
    anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
    targetOrigin={{horizontal: 'left', vertical: 'top'}}
    onRequestClose={() => {
      closeDialog('contactsMenu')
    }}
  >
    <Menu maxHeight='475px'>
      {selectedContacts.map((contact, index, arr) => {
        const {contactId, firstName, middleName, lastName} = contact
        return (<span key={index}>
          <MenuItem id={`ContactsMenu-MenuItem-${index}`} primaryText={`${parseName({lastName, firstName, middleName})}`} onClick={() => {
            closeDialog('contactsMenu')
            selectAndLoadContact(contact)
            openDialog('addEditContact')
          }} leftIcon={<EditIcon color={blue500}/>}/>
          {arr.length - 1 !== index && <Divider />}
        </span>)
      })}
    </Menu>
  </Popover>
)

ContactsMenu.propTypes = {
  open: T.bool.isRequired,
  anchorEl: T.object,
  closeDialog: T.func.isRequired,
  openDialog: T.func.isRequired,
  selectAndLoadContact: T.func.isRequired,
  selectedContacts: T.array.isRequired
}

export const mapStateToProps = ({dialog: {contactsMenu}, companies: {selectedContacts}}) => ({
  open: !!contactsMenu,
  anchorEl: contactsMenu ? contactsMenu.anchorEl : null,
  selectedContacts
})

const actions = {
  closeDialog,
  openDialog,
  selectAndLoadContact
}

export default connect(mapStateToProps, actions)(ContactsMenu)
