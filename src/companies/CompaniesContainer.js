import React from 'react'
import _ from 'lodash'
import { connect } from 'react-redux'
import { PropTypes as T } from 'prop-types'
import CompaniesComponent from './CompaniesComponent'
import { loadAllCompanies } from './CompaniesActions'

export class CompaniesContainer extends React.Component {
  componentDidMount () {
    const {editCompany, clearEditCompanyState, loadAllCompanies, companies} = this.props
    if (editCompany) {
      clearEditCompanyState()
    }

    if (_.isEmpty(companies)) {
      loadAllCompanies()
    }
  }

  render () {
    return (<CompaniesComponent {...this.props} />)
  }
}

CompaniesContainer.propTypes = {
  loadAllCompanies: T.func.isRequired,
  companies: T.array,
  editCompany: T.object,
  saveResult: T.object,
  clearEditCompanyState: T.func.isRequired
}

export const actions = {
  loadAllCompanies,
  clearEditCompanyState: () => ({
    type: 'CLEAR_EDIT_COMPANY'
  })
}

export const mapStateToProps = ({companies}) => {
  return {
    companies: companies.allCompanies.results,
    editCompany: companies.editCompany
  }
}

export default connect(mapStateToProps, actions)(CompaniesContainer)
