import React from 'react'
import {PropTypes as T} from 'prop-types'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import {blue500, red500} from 'material-ui/styles/colors'
import {Menu, MenuItem} from 'material-ui/Menu'
import {CompanyColumns} from './CompanyColumns'
import {Grid, CreateGridConfig} from '../components/grid'
import {closeDialog, openDialog, openConfirmationAlert} from '../components/redux-dialog'
import ContactsMenu from './ContactsMenu'
import AddEditContactComponent from '../contacts/AddEditContactComponent'
import AddEditCompanyComponent from './AddEditCompanyComponent'
import MultiActionMenu from '../components/common/MultiActionMenu'
import {connect} from 'react-redux'
import {deleteCompany, selectCompany} from './CompaniesActions'

class Companies extends React.Component {
  open = (row) => {
    const {openDialog, selectCompany} = this.props
    selectCompany(row)
    openDialog('addEditCompany')
  }

  render () {
    const {companies, closeDialog, openDialog, selectCompany, openConfirmationAlert, deleteCompany, selectedCompany} = this.props
    const options = CreateGridConfig(companies, {title: 'Companies'})
    return (<div>
      <Grid onRowDoubleClick={this.open} id="Companies" data={companies} options={options} columns={CompanyColumns}/>
      <ContactsMenu />
      <AddEditContactComponent />
      <AddEditCompanyComponent />
      <FloatingActionButton id='companies-floating-add-button' backgroundColor={red500} mini={false} className={'floating-add-button'} onClick={() => {
        selectCompany({})
        openDialog('addEditCompany')
      }}>
        <ContentAdd/>
      </FloatingActionButton>
      <MultiActionMenu dialogName='companyMenu'>
        <Menu>
          <MenuItem id='companies-floating-view-edit-button' primaryText="View/Edit Company" leftIcon={<EditIcon color={blue500}/>} onClick={() => {
            closeDialog('companyMenu')
            openDialog('addEditCompany')
          }} />
          <MenuItem id='companies-floating-delete-button' primaryText="Delete Company" leftIcon={<DeleteIcon color={red500}/>} onClick={() => {
            closeDialog('companyMenu')
            openConfirmationAlert({
              message: 'Are you sure you want to delete this Company?',
              onOk: () => deleteCompany(selectedCompany)
            })
          }} />
        </Menu>
      </MultiActionMenu>
    </div>)
  }
}

Companies.propTypes = {
  companies: T.array,
  selectedCompany: T.object,
  closeDialog: T.func.isRequired,
  openDialog: T.func.isRequired,
  deleteCompany: T.func.isRequired,
  selectCompany: T.func.isRequired,
  openConfirmationAlert: T.func.isRequired
}

const mapStateToProps = ({companies: {selectedCompany}}) => ({
  selectedCompany
})

const actions = {
  closeDialog,
  openDialog,
  openConfirmationAlert,
  deleteCompany,
  selectCompany
}

export default connect(mapStateToProps, actions)(Companies)
