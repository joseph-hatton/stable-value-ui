import React from 'react'
import { PropTypes as T } from 'prop-types'
import { Link } from 'react-router'
import { Card } from 'material-ui/Card'
import RaisedButton from 'material-ui/RaisedButton'
import { Field, reduxForm } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import { saveCompany } from './CompaniesActions'
import { Required, IsValidZipCode } from '../components/forms/Validators'
import validateCompanyName from './ValidateCompanyName'
import StatesFormField from '../components/forms/StatesFormField'
import ReduxDialog from '../components/redux-dialog/redux-dialog'
import {closeDialog} from '../components/redux-dialog'
import { connect } from 'react-redux'

export class AddEditCompany extends React.Component {
  render () {
    const {submitting, handleSubmit, saveCompany, closeDialog} = this.props
    const actions = [
      <RaisedButton key='save' form='addEditCompanyForm' style={{marginRight: 15}} label="Save" type="submit" primary={true} disabled={submitting}/>,
      <RaisedButton key='cancel' label="Cancel" onClick={() => {
        closeDialog('addEditCompany')
      }}/>
    ]
    return (
      <ReduxDialog dialogName='addEditCompany' title='Add/Edit Company' modal={true} autoScrollBodyContent={true} actions={actions}>
        <form id='addEditCompanyForm' onSubmit={handleSubmit(saveCompany)}>
          <div className="row">
            <div className="col-lg-12">
              <Field fullWidth name="name" component={TextField} type="text" floatingLabelText="* Company Name"
                      validate={Required}/>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-6">
              <Field fullWidth name="address1" component={TextField} type="text" floatingLabelText="Address"
                      validate={[Required]} style={{marginRight: '100px'}}/>
            </div>
            <div className="col-lg-6">
              <Field fullWidth name="address2" component={TextField} type="text" floatingLabelText="Address2"/>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-4">
              <Field fullWidth name="city" component={TextField} type="text" floatingLabelText="* City"
                      validate={Required} style={{marginRight: '100px'}}/>
            </div>
            <div className="col-lg-4">
              <StatesFormField fullWidth name="state" validate={Required}/>
            </div>
            <div className="col-lg-4">
              <Field fullWidth name="zipCode" component={TextField} type="text" floatingLabelText="* Zip Code"
                      validate={[Required, IsValidZipCode]}/>
            </div>
          </div>
        </form>
      </ReduxDialog>
    )
  }
}

AddEditCompany.propTypes = {
  saveCompany: T.func.isRequired,
  loadCompany: T.func.isRequired,
  submitting: T.bool,
  initialValues: T.object,
  handleSubmit: T.func.isRequired,
  closeDialog: T.func.isRequired
}

const AddEditCompanyForm = reduxForm({
  form: 'AddEditCompanyForm',
  enableReinitialize: true,
  asyncValidate: validateCompanyName,
  asyncBlurFields: ['name']
})(AddEditCompany)

const mapStateToProps = ({companies: {selectedCompany}}) => ({
  initialValues: selectedCompany
})

const actions = {
  saveCompany,
  closeDialog
}

export default connect(mapStateToProps, actions)(AddEditCompanyForm)
