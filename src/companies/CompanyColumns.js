import React from 'react'
import { Link } from 'react-router'
import store from '../store/store'
import { selectCompany } from './CompaniesActions'
import { ContactsMenuLink } from './ContactsMenu'
import { MultiActionMenuLink } from '../components/common/MultiActionMenu'

export const CompanyColumns = [
  {
    field: 'companyId',
    customProps: {
      hidden: true,
      isKey: true
    }
  },
  {
    field: 'name',
    sort: true,
    label: 'Company Name',
    customProps: {
      textAlignHeader: 'left',
      width: '30%'
    },
    renderer: (cell, row) => { // eslint-disable-line react/display-name
      return (
        <MultiActionMenuLink dialogName='companyMenu' row={row} cell={cell} selections={(row) => {
          store.dispatch(selectCompany(row))
        }} />
      )
    }
  },
  {
    field: 'address1',
    sort: true,
    label: 'Address',
    customProps: {
      textAlignHeader: 'left',
      width: '20%'
    }
  },
  {
    field: 'city',
    sort: true,
    label: 'City',
    customProps: {
      width: '15%',
      textAlign: 'left'
    }
  },
  {
    field: 'state',
    sort: true,
    label: 'State',
    customProps: {
      width: '10%',
      textAlign: 'center'
    }
  },
  {
    field: 'contacts',
    sort: true,
    label: 'Contacts',
    renderer: (cell, row) => <ContactsMenuLink cell={(cell || []).length} row={row} />, // eslint-disable-line react/display-name
    customProps: {
      width: '10%',
      textAlign: 'center'
    }
  }
]
