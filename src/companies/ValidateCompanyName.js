import _ from 'lodash'
import { apiPath } from '../config'
import http from '../actions/http'

const validateCompanyName = ({name, companyId}) => {
  if (name) {
    return http.get(`${apiPath}/companies?name=${name}`).then(({results}) => {
      let duplicate
      if (!_.isEmpty(results)
          && (duplicate = _.find(results, company => name.toLowerCase() === company.name.toLowerCase()))
            && duplicate.companyId !== companyId) {
        throw {name: 'Company name already exists'} // eslint-disable-line no-throw-literal
      }
    })
  } else {
    return Promise.reject({name: 'Required'}) // eslint-disable-line prefer-promise-reject-errors
  }
}

export default validateCompanyName
