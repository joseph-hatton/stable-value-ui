import React from 'react'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import {white, red500} from 'material-ui/styles/colors'
import {Card} from 'material-ui/Card'
import numeral from 'numeral'
import DollarAmountFormatter from './DollarAmountFormatter'

const amountStyle = {
  textAlign: 'right',
  paddingRight: 25
}
const SummaryAmount = ({label, amount, formatter, labelStyle = {}}) => (
  <div className="col-lg-4">
    <div className="row">
      <div className="col-lg-6" style={labelStyle}>
        {label}
      </div>
      <div className="col-lg-5" style={amountStyle}>
        {formatter ? formatter(amount) : amount}
      </div>
      <div className="col-lg-1">
      </div>
    </div>
  </div>
)

SummaryAmount.propTypes = {
  label: T.string.isRequired,
  amount: T.number,
  formatter: T.func,
  labelStyle: T.object
}

const titleStyle = {
  backgroundColor: red500,
  color: white,
  textAlign: 'center',
  verticalAlign: 'center',
  lineHeight: '160px',
  fontSize: 20
}

const topRowStyle = {paddingTop: 10, paddingBottom: 3}
const middleRowStyle = {paddingTop: 3, paddingBottom: 3}
const bottomRowStyle = {paddingTop: 3, paddingBottom: 10}
const totalLabelStyle = {fontWeight: 'bold'}
const cardStyle = {paddingLeft: 15}

const percentFormatter = (value) => numeral(value).format('(0,0.00)')

export const TransactionSummary = ({transactionSummary}) => {
  const {
    deposits, withdrawals, adjustments, feeReceipts, feeAdjustments, terminations, amountBvPercent, withdrawalBvPercent, depositBvPercent
  } = transactionSummary

  const balance = deposits + withdrawals + adjustments + terminations
  const feeTotal = feeReceipts + feeAdjustments
  return (
    <Card style={cardStyle}>
      <div className="row">
        <div className="col-lg-2" style={titleStyle}>
          Selected Values:
        </div>
        <div className="col-lg-10">
          <div className="row" style={topRowStyle}>
            <SummaryAmount label="Deposits" amount={deposits} formatter={DollarAmountFormatter}/>
            <SummaryAmount label="Deposit BV %" amount={depositBvPercent} formatter={percentFormatter}/>
            <SummaryAmount label="Fee Receipts" amount={feeReceipts} formatter={DollarAmountFormatter}/>
          </div>
          <div className="row" style={middleRowStyle}>
            <SummaryAmount label="Withdrawals" amount={withdrawals} formatter={DollarAmountFormatter}/>
            <SummaryAmount label="Withdrawal BV %" amount={withdrawalBvPercent} formatter={percentFormatter}/>
            <SummaryAmount label="Fee Adjustments" amount={feeAdjustments} formatter={DollarAmountFormatter}/>
          </div>
          <div className="row" style={middleRowStyle}>
            <SummaryAmount label="BV Adjustments" amount={adjustments} formatter={DollarAmountFormatter}/>
          </div>
          <div className="row" style={middleRowStyle}>
            <SummaryAmount label="Terminations" amount={terminations} formatter={DollarAmountFormatter}/>
          </div>
          <div className="row" style={bottomRowStyle}>
            <SummaryAmount label="Total" labelStyle={totalLabelStyle} amount={balance}
                           formatter={DollarAmountFormatter}/>
            <div className="col-lg-4">
            </div>
            <SummaryAmount label="Total" labelStyle={totalLabelStyle} amount={feeTotal}
                           formatter={DollarAmountFormatter}/>
          </div>
        </div>
      </div>
    </Card>
  )
}

TransactionSummary.propTypes = {
  transactionSummary: T.object
}

export const mapStateToProps = ({transactions: {transactionSummary = {}}}) => ({transactionSummary})

export default connect(mapStateToProps)(TransactionSummary)
