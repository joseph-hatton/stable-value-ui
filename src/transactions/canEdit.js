import {hasEntitlement} from '../entitlements'
import store from '../store/store'

const canEdit = () => {
  const {transactions: {addEditTransaction}} = store.getState()
  return hasEntitlement('MODIFY_TRANSACTION') && addEditTransaction && !addEditTransaction.locked
}

export default canEdit
