import createRefDataFormFieldComponent from '../components/forms/createRefDataFormFieldComponent'

export const TransactionTypes = createRefDataFormFieldComponent('transactionTypes', {
  floatingLabelText: 'Transaction Type',
  type: 'text'
}, undefined, true)

export const TransactionDetailsTypes = createRefDataFormFieldComponent('transactionDetailedTypes', {
  floatingLabelText: 'Transaction Details Type',
  type: 'text'
}, undefined, true)

export const AmountComparators = createRefDataFormFieldComponent('amountComparators', {
  floatingLabelText: 'Amount Comparators',
  type: 'text'
}, undefined, true)

export const DepositTransactionDetails = createRefDataFormFieldComponent('depositTransactionDetails', {
  floatingLabelText: '* Transaction Detail',
  type: 'text'
}, undefined, false)

export const WithdrawalTransactionDetails = createRefDataFormFieldComponent('withdrawalTransactionDetails', {
  floatingLabelText: '* Transaction Detail',
  type: 'text'
}, undefined, false)

export const AdjustmentTransactionDetails = createRefDataFormFieldComponent('adjustmentTransactionDetails', {
  floatingLabelText: '* Transaction Detail',
  type: 'text'
}, undefined, false)

export const TerminationTransactionDetails = createRefDataFormFieldComponent('terminationTransactionDetails', {
  floatingLabelText: '* Transaction Detail',
  type: 'text'
}, undefined, false)

export const TransactionDetailsTypesMap = {
  DEPOSIT: DepositTransactionDetails,
  WITHDRAWAL: WithdrawalTransactionDetails,
  ADJUSTMENT: AdjustmentTransactionDetails,
  TERMINATION: TerminationTransactionDetails
}
