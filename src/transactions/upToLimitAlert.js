import React from 'react'
import Dialog from 'material-ui/Dialog'
import {RaisedButton} from 'material-ui'

export default class UpToLimitAlert extends React.Component {
  state = {
    open: true
  }

  handleOpen = () => {
    this.setState({open: true})
  }

  handleClose = () => {
    this.setState({open: false})
  }

  render () {
    const actions = [
      <RaisedButton
        key="OK"
        label="OK"
        primary={true}
        onClick={this.handleClose}
      />
    ]

    return (
      <div>
        <Dialog
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          <strong> Contract has Deposit Limit - Please Review </strong>
        </Dialog>
      </div>
    )
  }
}
