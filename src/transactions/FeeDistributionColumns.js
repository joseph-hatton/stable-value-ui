import React from 'react'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import numeral from 'numeral'
import {EditableCell} from '../components/grid'
import {feeAmountChanged} from './TransactionActions'

export const DollarAmountRenderer = (value) => numeral(value).format('(0,0.00)')

export const AmountCell = ({field, value, row, feeAmountChanged, editable}) => (
  <EditableCell
    field={field}
    rowId={`edit-amount-${row.invoiceNumber || 'Next'}`}
    renderer={DollarAmountRenderer} value={value} row={row}
    onChange={feeAmountChanged}
    editable={editable}
  />
)

AmountCell.propTypes = {
  field: T.string.isRequired,
  value: T.number.isRequired,
  row: T.object.isRequired,
  feeAmountChanged: T.func.isRequired,
  editable: T.bool.isRequired
}

const TransactionAmountCell = connect(null, {feeAmountChanged})(AmountCell)

export const FeeDistributionColumns = [
  {
    field: 'invoiceNumber',
    label: 'Invoice #',
    renderer: (cell) => cell || 'Next'
  },
  {
    field: 'beginningBalance',
    label: 'Beginning Balance',
    renderer: DollarAmountRenderer
  },
  {
    field: 'amount',
    label: 'Fee Received',
    renderer: (value, row) => <TransactionAmountCell field="amount" // eslint-disable-line react/display-name
                                                     value={value}
                                                     row={row}
                                                     editable={row.transactionType === 'FEE_RECEIPT'}/>
  },
  {
    field: 'adjustment',
    label: 'Fee Earned Adjustment',
    renderer: (value, row) => <TransactionAmountCell field="adjustment" // eslint-disable-line react/display-name
                                                     value={value}
                                                     row={row}
                                                     editable={row.transactionType === 'FEE_ADJUSTMENT'}/>
  },
  {
    field: 'endingBalance',
    label: 'Ending Balance',
    renderer: DollarAmountRenderer
  }
]

export default FeeDistributionColumns
