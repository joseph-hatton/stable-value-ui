import React from 'react'
import numeral from 'numeral'
import {createReferenceDataTextRenderer} from '../components/renderers/ReferenceDataTextRenderer'
import {dateFormat, percentFormat} from '../components/utilities/Formatters'
import {exists} from '../components/forms/Validators'
import {GridContextMenuLink} from '../components/grid'
import {createSelectTransactionPayload, isFeeTransaction} from './TransactionActions'

export const DollarAmountRenderer = (value) => exists(value) ? numeral(value).format('(0,0.00)') : ''

export const TransactionColumns = [
  {
    field: 'contractNumber',
    sort: true,
    label: 'Contract Number',
    customProps: {
      textAlignHeader: 'left',
      width: '10%'
    },
    renderer: (cell, row) => { // eslint-disable-line react/display-name
      return (
        <GridContextMenuLink menuName="TransactionsGridMenu" cell={cell} row={row}
                             rowSelectionActionType="SELECT_TRANSACTION"
                             createPayload={createSelectTransactionPayload}/>)
    }
  },
  {
    field: 'shortPlanName',
    sort: true,
    label: 'Short Plan Name',
    customProps: {
      width: '10%',
      textAlignHeader: 'left'
    }
  },
  {
    field: 'effectiveDate',
    sort: true,
    label: 'Effective Date',
    renderer: (cell) => { // eslint-disable-line react/display-name
      return dateFormat(cell)
    },
    customProps: {
      textAlign: 'center',
      width: '10%'
    }
  },
  {
    field: 'transactionType',
    sort: true,
    label: 'Transaction Type',
    renderer: createReferenceDataTextRenderer('transactionTypes'),
    customProps: {
      textAlign: 'left',
      width: '10%'
    }
  },
  {
    field: 'detail',
    sort: true,
    label: 'Transaction Detail',
    renderer: createReferenceDataTextRenderer('transactionDetailedTypes'),
    customProps: {
      textAlign: 'left',
      width: '10%'
    }
  },
  {
    field: 'amount',
    sort: true,
    label: 'Amount',
    renderer: DollarAmountRenderer,
    customProps: {
      textAlign: 'right',
      width: '10%'
    }
  },
  {
    field: 'beginningBookValue',
    sort: false,
    label: 'Beginning BV',
    renderer: (value, transaction) => isFeeTransaction(transaction) ? '' : DollarAmountRenderer(value),
    customProps: {
      textAlign: 'right',
      width: '10%'
    }
  },
  {
    field: 'amountByBookValue',
    sort: false,
    label: 'Amount/BV %',
    customProps: {
      textAlign: 'right',
      width: '10%'
    },
    renderer: (cell, transaction) => { // eslint-disable-line react/display-name
      const {amount, beginningBookValue} = transaction
      return isFeeTransaction(transaction) ? '' : (beginningBookValue ? percentFormat(amount / beginningBookValue) : '')
    }
  },
  {
    field: 'comments',
    sort: false,
    label: 'Comments',
    customProps: {
      textAlignHeader: 'left',
      width: '10%'
    }
  }
]

export default TransactionColumns
