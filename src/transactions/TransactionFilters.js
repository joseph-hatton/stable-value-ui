import React from 'react'
import _ from 'lodash'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import {reduxForm, reset, change, Field} from 'redux-form'
import {RaisedButton, IconButton} from 'material-ui'
import Clear from 'material-ui/svg-icons/content/clear'
import {DatePicker, TextField} from 'redux-form-material-ui'
import {Card} from 'material-ui/Card'
import {dateFormat, normalizeNumberField, formatNumberField} from '../components/utilities/Formatters'
import ContractPicker from '../contracts/ContractPicker'
import {
  TransactionTypes,
  TransactionDetailsTypes,
  AmountComparators
} from './TransactionReferenceDataFields'

const formId = 'TransactionFiltersForm'

const CLEAR_ALL_BUTTON_STYLE = {
  alignItems: 'flex-end',
  display: 'flex',
  justifyContent: 'center',
  marginBottom: 20
}

export const ClearDateButton = ({clearDate, dateProp}) => (
  <IconButton id={`${dateProp}-date-clear`} style={{marginTop: 25}} onClick={() => clearDate(dateProp)}>
    <Clear/>
  </IconButton>)

ClearDateButton.propTypes = {
  clearDate: T.func.isRequired,
  dateProp: T.string.isRequired
}

const cardStyle = {paddingLeft: 20, paddingRight: 20}

export const noPendingContracts = ({status}) => ['PENDING_TERMINATION', 'ACTIVE', 'TERMINATED'].indexOf(status) >= 0

export const labelWithStatus = ({contractNumber, shortPlanName, status, statusText}) =>
  `${contractNumber} - ${shortPlanName} (${statusText || status})`

export const TransactionFilters = ({clearAll, clearDate, from, to, active}) => {
  return (
    <Card style={cardStyle}>
      <form id={formId}>
        <div className="row">
          <div className="col-lg-4">
            <ContractPicker makeLabel={labelWithStatus} nullable={true} name="contractId" type="text"
                            filterContract={noPendingContracts}
                            floatingLabelText="Contract"/>
          </div>
          <div className="col-lg-4">
            <TransactionTypes name="transactionType"/>
          </div>
          <div className="col-lg-4">
            <TransactionDetailsTypes name="detail"/>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-2">
            <Field autoOk fullWidth name="from" component={DatePicker} firstDayOfWeek={0} floatingLabelText="From"
                   formatDate={dateFormat}/>
          </div>
          <div className="col-lg-1">
            {from && <ClearDateButton dateProp="from" clearDate={clearDate}/>}
          </div>
          <div className="col-lg-2">
            <Field autoOk fullWidth name="to" component={DatePicker} firstDayOfWeek={0} floatingLabelText="To" formatDate={dateFormat}/>
          </div>
          <div className="col-lg-1">
            {to && <ClearDateButton dateProp="to" clearDate={clearDate}/>}
          </div>
          <div className="col-lg-2">
            <AmountComparators name="amountComparator" floatingLabelText="Amount is"/>
          </div>
          <div className="col-lg-2">
            <Field fullWidth component={TextField} name="amount" floatingLabelText="Amount"
                   format={formatNumberField(active === 'amount')} normalize={normalizeNumberField}/>
          </div>
          <div className="col-lg-2" style={CLEAR_ALL_BUTTON_STYLE}>
            <RaisedButton label="Clear All" onClick={clearAll}/>
          </div>
        </div>
      </form>
    </Card>
  )
}

TransactionFilters.propTypes = {
  clearAll: T.func.isRequired,
  clearDate: T.func.isRequired,
  from: T.string,
  to: T.string,
  active: T.string
}

const TransactionFiltersForm = reduxForm({
  form: formId
})(TransactionFilters)

export const actions = {
  clearAll: () => (dispatch) => {
    dispatch(reset('TransactionFiltersForm'))
  },
  clearDate: (dateProp) => (dispatch) => {
    dispatch(change('TransactionFiltersForm', dateProp, null))
  }
}

export const mapStateToProps = ({form}) => {
  return {
    from: _.get(form, 'TransactionFiltersForm.values.from'),
    to: _.get(form, 'TransactionFiltersForm.values.to'),
    active: _.get(form, 'TransactionFiltersForm.active')
  }
}

export default connect(mapStateToProps, actions)(TransactionFiltersForm)
