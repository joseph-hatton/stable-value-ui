import React from 'react'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import {Grid, CreateGridConfig} from '../components/grid'
import FeedDistributionColumns from './FeeDistributionColumns'
import {loadFeeDistributions, createNewFeeDistribution} from './TransactionActions'

const options = CreateGridConfig(null, {
  showHeaderToolbar: false,
  paginate: false,
  height: 150
})

export class FeeDistributionGrid extends React.Component {
  componentDidMount () {
    if (this.props.transactionId) {
      this.props.loadFeeDistributions()
    } else if (this.props.contractId) {
      this.props.createNewFeeDistribution()
    }
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.transactionId) {
      this.props.loadFeeDistributions()
    } else if (nextProps.contractId && nextProps.contractId !== this.props.contractId) {
      this.props.createNewFeeDistribution()
    }
  }

  render () {
    const {feeDistributions} = this.props
    return (
      <div style={{marginTop: 20, marginBottom: 20}}>
        <Grid id="FeeDistributionGrid" options={options} columns={FeedDistributionColumns} data={feeDistributions}/>
      </div>
    )
  }
}

FeeDistributionGrid.propTypes = {
  feeDistributions: T.array,
  transactionId: T.number,
  contractId: T.number,
  loadFeeDistributions: T.func.isRequired,
  createNewFeeDistribution: T.func.isRequired
}

export const mapStateToProps = (state) => {
  const {
    transactions: {
      feeDistributions
    },
    form: {
      AddEditTransactionForm: {
        values: {
          transactionId,
          contractId
        }
      }
    }
  } = state

  return {
    feeDistributions,
    transactionId,
    contractId
  }
}

const actions = {loadFeeDistributions, createNewFeeDistribution}

export default connect(mapStateToProps, actions)(FeeDistributionGrid)
