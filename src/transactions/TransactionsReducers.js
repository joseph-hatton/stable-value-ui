const INITIAL_STATE = {
  feeDistributions: []
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'NEW_TRANSACTION': {
      const {transactionType, transactionTypeText} = action
      return {
        ...state,
        addEditTransaction: {
          transactionType,
          transactionTypeText
        }
      }
    }
    case 'SELECT_TRANSACTION': {
      return {
        ...state,
        addEditTransaction: action.payload
      }
    }
    case 'SET_FEE_DISTRIBUTIONS': {
      const {addEditTransaction: {transactionType}} = state
      return {
        ...state,
        feeDistributions: action.payload.map(feeDistribution => ({
          ...feeDistribution,
          transactionType
        }))
      }
    }
    case 'CLEAR_TRANSACTION': {
      return {
        ...state,
        addEditTransaction: null,
        feeDistributions: [],
        refresh: action.refresh || false
      }
    }
    case 'TRANSACTION_REFRESHED': {
      return {
        ...state,
        refresh: false
      }
    }
    case 'GRID_DATA_UPDATED': {
      const {id, response: {transactionSummary}} = action
      if (id === 'Transactions') {
        return {
          ...state,
          transactionSummary
        }
      } else {
        return state
      }
    }
    case 'DELETE_TRANSACTION_FULFILLED': {
      return {
        ...state,
        addEditTransaction: null,
        feeDistributions: [],
        refresh: true
      }
    }
    case 'SAVE_TRANSACTION_FULFILLED': {
      return {
        ...state,
        refresh: true
      }
    }
    default:
      return state
  }
}
