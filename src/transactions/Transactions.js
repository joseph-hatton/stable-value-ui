import React from 'react'
import _ from 'lodash'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import {blue500, red500} from 'material-ui/styles/colors'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import ContentAdd from 'material-ui/svg-icons/content/add'
import NavigationClose from 'material-ui/svg-icons/navigation/close'
import DepositIcon from 'material-ui/svg-icons/action/system-update-alt'
import Filter from 'material-ui/svg-icons/content/filter-list'
import Summary from 'material-ui/svg-icons/editor/functions'
import {apiPath} from '../config'
import {IntlProvider} from 'react-intl'
import Expandable from '../components/common/Expandable'
import {Grid, CreateGridConfig, GridContextMenu, DESCENDING} from '../components/grid'
import TransactionColumns from './TransactionColumns'
import {SpeedDial, SpeedDialItem} from '../components/fab-dial'
import styles from '../styles/styles'
import TransactionFilters from './TransactionFilters'
import {
  newTransaction,
  selectTransaction,
  deleteTransaction,
  transactionsRefreshed,
  openTransaction
} from './TransactionActions'
import AddEditTransaction from './AddEditTransaction'
import TransactionSummary from './TransactionSummary'
import store from '../store/store'
import {serializeDates} from '../actions/serializeDate'
import {hasEntitlement} from '../entitlements'
import canEdit from './canEdit'

const TRANSACTIONS_URL = `${apiPath}/transactions`

export const formatValues = (filters = {}) => {
  const newFilters = _.omitBy(filters, filter => !filter)
  const {amount, amountComparator} = filters

  if (amountComparator && amount !== null && amount !== undefined && !isNaN(amount)) {
    newFilters.amount = parseFloat(amount)
  } else {
    delete newFilters.amount
    delete newFilters.amountComparator
  }

  return serializeDates(newFilters)
}

const TransactionMenus = [
  {
    label: 'View/Edit Transaction',
    icon: <EditIcon color={blue500}/>,
    action: selectTransaction
  },
  {
    label: 'Delete Transaction',
    icon: <DeleteIcon color={red500}/>,
    action: deleteTransaction,
    disabled: () => !canEdit()
  }
]

export class Transactions extends React.Component {
  getGridRef () {
    return this.refs['TransactionsGrid']
  }

  componentWillReceiveProps ({refresh}) {
    if (refresh) {
      this.getGridRef().refresh()
      this.props.transactionsRefreshed()
    }
  }

  render () {
    const filters = formatValues(this.props.filters)
    const {newTransaction, addEditTransaction, openTransaction} = this.props

    const options = CreateGridConfig(null, {
      showHeaderToolbar: false,
      paginationType: 'remote',
      baseApiUrl: TRANSACTIONS_URL
    })

    return (
      <div>
        <Expandable id="TransactionFilters" title={<span>Transaction Filters</span>} icon={<Filter/>}>
          <TransactionFilters/>
        </Expandable>
        <Expandable id="TransactionSummary" title={<span>Transactions Summary</span>} icon={<Summary/>}>
          <TransactionSummary/>
        </Expandable>
        <IntlProvider>
          <Grid ref="TransactionsGrid" id="Transactions" options={options} columns={TransactionColumns}
                filters={filters} store={store} onRowDoubleClick={openTransaction} sortBy="effectiveDate"
                sortDirection={DESCENDING}/>
        </IntlProvider>
        <GridContextMenu menuName="TransactionsGridMenu" menus={TransactionMenus}/>
        {hasEntitlement('MODIFY_TRANSACTION') &&
        <SpeedDial style={styles.fab} fabContentOpen={<ContentAdd/>} fabContentClose={<NavigationClose/>}>
          <SpeedDialItem label="Deposit" onClick={() => newTransaction('DEPOSIT')}/>
          <SpeedDialItem label="Withdrawal" onClick={() => newTransaction('WITHDRAWAL')}/>
          <SpeedDialItem label="Book Value Adjustment" onClick={() => newTransaction('ADJUSTMENT')}/>
          <SpeedDialItem label="Fee Receipt" onClick={() => newTransaction('FEE_RECEIPT')}/>
          <SpeedDialItem label="Fee Adjustment" onClick={() => newTransaction('FEE_ADJUSTMENT')}/>
          <SpeedDialItem label="Termination" onClick={() => newTransaction('TERMINATION')}/>
        </SpeedDial>}
        {addEditTransaction && <AddEditTransaction/>}
      </div>
    )
  }
}

Transactions.propTypes = {
  filters: T.object,
  newTransaction: T.func.isRequired,
  transactionsRefreshed: T.func.isRequired,
  openTransaction: T.func.isRequired,
  addEditTransaction: T.object,
  refresh: T.bool
}

const NO_FILTERS = {}
export const mapStateToProps = ({form: {TransactionFiltersForm}, transactions: {addEditTransaction, refresh}}) => {
  return {
    filters: TransactionFiltersForm ? TransactionFiltersForm.values : NO_FILTERS,
    addEditTransaction,
    refresh
  }
}

const actions = {
  newTransaction,
  transactionsRefreshed,
  openTransaction
}

export default connect(mapStateToProps, actions)(Transactions)
