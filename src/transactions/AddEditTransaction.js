import React from 'react'
import _ from 'lodash'
import {PropTypes as T} from 'prop-types'
import moment from 'moment'
import {connect} from 'react-redux'
import RaisedButton from 'material-ui/RaisedButton'
import {orange500} from 'material-ui/styles/colors'
import {Field, reduxForm} from 'redux-form'
import {DatePicker, TextField} from 'redux-form-material-ui'
import ConfirmationAlert from '../components/redux-dialog/ConfirmationAlert'
import {exists, Required, MaxLength, isNegativeNumber} from '../components/forms/Validators'
import {toUtc, dateFormat} from '../components/utilities/Formatters'
import ReduxDialog, { openDialog, openConfirmationAlert } from '../components/redux-dialog/redux-dialog'
import UpToLimitAlert from './upToLimitAlert'
import {
  saveTransaction,
  clearTransaction,
  getBalanceForEffectiveDate,
  isFeeTransaction,
  isFeeAdjustment,
  feeReceiptAmountChanged,
  isFeeReceipt,
  saveWithdrawalTransaction
} from './TransactionActions'
import ContractPicker from '../contracts/ContractPicker'
import {TransactionDetailsTypesMap} from './TransactionReferenceDataFields'
import NumberField from '../contracts/NumberField'
import AppStyles from '../styles/styles'
import FeeDistributionGrid from './FeeDistributionGrid'
import WatchListAlert from '../watchLists/watchListAlert/WatchListAlert'
import {checkWatchList} from '../watchLists/watchListAlert/WatchListAlertActions'
import canEdit from './canEdit'
import {loadAllContracts} from '../contracts/ContractActions'

const formId = 'AddEditTransactionForm'

const makeDialogTitle = ({transactionId, transactionTypeText}) => {
  const action = exists(transactionId) ? 'Edit' : 'Add'
  return `${action} ${transactionTypeText} Transaction`
}

const isEffectiveDateBeforeThisMonth = (effectiveDate) => {
  const effectiveDateMoment = moment(effectiveDate)
  const today = moment()
  return effectiveDate && effectiveDateMoment.isBefore(today, 'month')
}

export const validateEffectiveDate = (effectiveDate) => {
  if (isEffectiveDateBeforeThisMonth(effectiveDate)) {
    return 'Effective date must be in open month or future.'
  }
}

export const ActiveContracts = ({status}) => status === 'ACTIVE'

const isTerminationTx = (transactionType) => transactionType === 'TERMINATION'

export class AddEditTransaction extends React.Component {
  transactionAmountChanged (event) {
    if (!exists(event.target.value) || isNaN(event.target.value)) {
      return
    }

    const {addEditTransaction: {transactionType}, amount} = this.props
    const newAmount = parseFloat(event.target.value)
    if (transactionType === 'FEE_RECEIPT' && amount !== newAmount) {
      this.props.feeReceiptAmountChanged(newAmount)
    }
  }

  componentDidMount () {
    const {addEditTransaction: {contractId, transactionType}, checkWatchList} = this.props
    if (['FEE_RECEIPT', 'BOOK_VALUE_ADJUSTMENT'].indexOf(transactionType) === -1 && contractId) {
      checkWatchList(contractId)
    }
  }

  render () {
    const {
      handleSubmit,
      addEditTransaction,
      saveTransaction,
      clearTransaction,
      contract,
      checkWatchList,
      saveWithdrawalTransaction,
      getBalanceForEffectiveDate,
      startOfCurrentOpenMonth,
      loadAllContracts
    } = this.props

    const {transactionType, transactionId, effectiveDate, contractId} = addEditTransaction
    const isWithdrawal = transactionType === 'WITHDRAWAL'
    const isAdjustment = transactionType === 'ADJUSTMENT'
    const isTermination = isTerminationTx(transactionType)
    const acceptNegativeNumber = isAdjustment || isWithdrawal
    const existingTransaction = !!transactionId
    const shouldCheckForWatchList = transactionType === 'FEE_RECEIPT' || transactionType === 'BOOK_VALUE_ADJUSTMENT'
    const saveDisabled = !canEdit() || (isFeeTransaction(addEditTransaction) && existingTransaction)

    const TransactionDetailsTypes = TransactionDetailsTypesMap[transactionType]

    const getBalanceForEffectiveDateDeferred = () => setTimeout(getBalanceForEffectiveDate)

    let amountValidator = isWithdrawal ? [Required, isNegativeNumber] : Required

    let filterContractProp
    if (transactionType === 'FEE_RECEIPT') {
      filterContractProp = {filterContract: ({status}) => ['ACTIVE', 'PENDING_TERMINATION'].includes(status)}
    } else if (contract && transactionId && ['PENDING_TERMINATION', 'TERMINATED'].includes(contract.status)) {
      filterContractProp = {}
    } else {
      filterContractProp = {filterContract: ActiveContracts}
    }

    const actions = [
      <RaisedButton
        key="Save"
        label="Save"
        primary={true}
        style={{marginRight: 20}}
        type="submit"
        form={formId}
        disabled={saveDisabled}
      />
    ]
    return (
      <ReduxDialog
        dialogName="AddEditTransaction"
        title={makeDialogTitle(addEditTransaction)}
        actions={actions}
        modal={true}
        autoScrollBodyContent={true}
        onCancel={clearTransaction}
        cancelButton
        contentStyle={{ width: '70%', maxWidth: 'none' }}
      >
        <form id={formId} onSubmit={handleSubmit(isWithdrawal ? saveWithdrawalTransaction : saveTransaction)}>
          {
            contract && contract.additionalDeposits === 'UP_TO_LIMIT' && transactionType === 'DEPOSIT'
            && <UpToLimitAlert/>
          }
          <div className="row">
            <div className="col-lg-7">
              <ContractPicker validate={Required} name="contractId"
                              floatingLabelText="* Contract"
                              {...filterContractProp}
                              disabled={isTermination && transactionId}
                              onChange={(e, val) => {
                                getBalanceForEffectiveDateDeferred()
                                !shouldCheckForWatchList && checkWatchList(val)
                              }}/>
            </div>
            <div className="col-lg-5">
              <Field validate={Required} fullWidth
                     name="effectiveDate"
                     formatDate={dateFormat}
                     format={toUtc} component={DatePicker}
                     minDate={moment(startOfCurrentOpenMonth).startOf('month').toDate()}
                     floatingLabelText="* Effective Date" onChange={getBalanceForEffectiveDateDeferred} autoOk/>
            </div>
          </div>
          <div className="row">
            {
              !isFeeTransaction(addEditTransaction) && (
                <div className="col-lg-7">
                  <TransactionDetailsTypes validate={Required} name="detail"/>
                </div>
              )
            }
            <div className="col-lg-5">
              <NumberField
                disabled={isTermination || isFeeAdjustment(transactionType)}
                acceptNegativeNumber={acceptNegativeNumber}
                validate={amountValidator}
                fullWidth
                name="amount"
                floatingLabelText='* Amount'
                blur={::this.transactionAmountChanged}
              />
            </div>
          </div>
          {
            isTermination && (
              <div className="row">
                <div className="col-lg-7">
                  <Field validate={Required} fullWidth
                         name="statedMaturityDate"
                         formatDate={dateFormat}
                         format={toUtc} component={DatePicker}
                         floatingLabelText="* Maturity Date" autoOk/>
                </div>
              </div>
            )
          }
          {
            isAdjustment && (
              <div className="row">
                <div className="col-lg-7">
                  <Field validate={Required} fullWidth
                         name="adjustmentDate"
                         formatDate={dateFormat}
                         format={toUtc} component={DatePicker}
                         floatingLabelText="* Adjustment Date" autoOk/>
                </div>
              </div>
            )
          }
          {
            isFeeTransaction(addEditTransaction) && (
              <div className="row">
                <div className="col-lg-12">
                  <FeeDistributionGrid/>
                  <span style={{display: 'none'}}> <Field name="feeDistributionTotal" component={TextField}/> </span>
                </div>
              </div>
            )
          }
          <div className="row">
            <div className="col-lg-12">
              <Field fullWidth textareaStyle={AppStyles.textAreaFont} name="comments" component={TextField}
                     validate={MaxLength(255)} floatingLabelText="Comments (Max: 255 Chars)"
                     multiLine rowsMax={3}/>
            </div>
          </div>
        </form>
        <WatchListAlert/>
        <ConfirmationAlert dialogName="withdrawalLimitWarning"/>
      </ReduxDialog>
    )
  }
}

AddEditTransaction.propTypes = {
  handleSubmit: T.func,
  addEditTransaction: T.object,
  saveTransaction: T.func.isRequired,
  saveWithdrawalTransaction: T.func.isRequired,
  clearTransaction: T.func.isRequired,
  getBalanceForEffectiveDate: T.func.isRequired,
  feeReceiptAmountChanged: T.func.isRequired,
  checkWatchList: T.func.isRequired,
  initialValues: T.object,
  feeDistributionTotal: T.number,
  contract: T.object,
  amount: T.number,
  startOfCurrentOpenMonth: T.date,
  openConfirmationAlert: T.func.isRequired,
  loadAllContracts: T.func.isRequired
}

export const validateTransaction = (values, props) => {
  const {addEditTransaction: {transactionType}, contract = {}} = props
  const errors = {}

  if (isFeeReceipt(transactionType)) {
    const amount = _.toNumber(values.amount || 0)
    const feeDistributionTotal = props.feeDistributionTotal || 0

    if (amount.toFixed(2) !== feeDistributionTotal.toFixed(2)) {
      errors.amount = 'Amount must be equal to total Fee Received'
    }
  }

  const {contractId, effectiveDate, amount} = values
  if (isTerminationTx(transactionType) && contractId && effectiveDate && !amount) {
    errors.amount = 'No balance information found for contract and effective date'
  }

  if (transactionType === 'WITHDRAWAL' && values.detail === 'PUT_OPTION' && contract.contractType !== 'POOLED_PLAN') {
    errors.detail = 'Transaction detail value not valid for transaction and contract type'
  }

  return errors
}

export const AddEditTransactionForm = reduxForm({
  form: formId,
  validate: validateTransaction
})(AddEditTransaction)

export const mapStateToProps = (state) => {
  const {
    transactions: {addEditTransaction = {}},
    form: {AddEditTransactionForm = {}},
    contracts: {allContracts = {}},
    monthlyCycle
  } = state

  const {values: addEditTransactionFormValues} = AddEditTransactionForm
  const {contractId: selectedContractId, feeDistributionTotal, amount} = addEditTransactionFormValues || {}
  const {transactionId, transactionType} = addEditTransaction
  const newTransaction = !transactionId
  const contractId = selectedContractId

  const initialValues = {
    feeDistributionTotal: 0
  }

  if (isFeeAdjustment(transactionType)) {
    initialValues.amount = 0
  }

  const props = {
    amount,
    feeDistributionTotal,
    addEditTransaction,
    initialValues: newTransaction ? initialValues : addEditTransaction,
    contractId,
    startOfCurrentOpenMonth: _.get(monthlyCycle, 'current.startDate')
  }

  if (contractId && !_.isEmpty(allContracts.results)) {
    props.contract = allContracts.results.find(contract => contract.contractId === contractId)
  }

  return props
}

export const actions = {
  saveTransaction,
  saveWithdrawalTransaction,
  clearTransaction,
  getBalanceForEffectiveDate,
  feeReceiptAmountChanged,
  checkWatchList,
  openConfirmationAlert,
  loadAllContracts
}

export default connect(mapStateToProps, actions)(AddEditTransactionForm)
