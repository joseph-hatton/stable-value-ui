import _ from 'lodash'
import moment from 'moment'
import {change} from 'redux-form'
import {openDialog, openConfirmationAlert} from '../components/redux-dialog/redux-dialog'
import {apiPath} from '../config'
import http from '../actions/http'
import {exists} from '../components/forms/Validators'
import {serializeDate} from '../actions/serializeDate'
import {loadAllContracts} from '../contracts/ContractActions'

export const isFeeTransaction = ({transactionType}) => isFeeReceipt(transactionType) || isFeeAdjustment(transactionType)

export const isFeeReceipt = transactionType => transactionType === 'FEE_RECEIPT'

export const isFeeAdjustment = transactionType => transactionType === 'FEE_ADJUSTMENT'

const getTransactionTypeText = (transactionType, {referenceData: {transactionTypes}}) => {
  return _.keyBy(transactionTypes, 'id')[transactionType].text
}

export const newTransaction = (transactionType) => (dispatch, getState) => {
  dispatch({
    type: 'NEW_TRANSACTION',
    transactionType,
    transactionTypeText: getTransactionTypeText(transactionType, getState())
  })

  dispatch(openDialog('AddEditTransaction'))
}

export const createSelectTransactionPayload = (row, state) => ({
  ...row,
  transactionTypeText: getTransactionTypeText(row.transactionType, state)
})

export const openTransaction = (transaction) => (dispatch, getState) => {
  const state = getState()
  const transactionPayload = createSelectTransactionPayload(transaction, state)
  dispatch({
    type: 'SELECT_TRANSACTION',
    payload: transactionPayload
  })
  selectTransaction(dispatch)
}

/**
 * This action is already connected by ContextMenuLink Component.
 */
export const selectTransaction = (dispatch) => {
  dispatch(openDialog('AddEditTransaction'))
}

const TRANSACTIONS_URL = `${apiPath}/transactions`

const TRANSACTION_DELETED_MESSAGE = {
  successMessage: 'Transaction Deleted Successfully',
  errorMessage: http.httpErrorMessage
}

export const deleteTransaction = (dispatch, getState) => {
  dispatch(openConfirmationAlert({
      message: 'Are you sure you want to delete this Transaction?',
      onOk: () => {
        const {
          transactions: {addEditTransaction: {transactionId}}
        } = getState()

        dispatch({
          type: 'DELETE_TRANSACTION',
          payload: http.delete(`${TRANSACTIONS_URL}/${transactionId}`, null, TRANSACTION_DELETED_MESSAGE).then(result => {
            dispatch(loadAllContracts())
            return result
          })
        })
      },
      onCancel: () => {
        dispatch(clearTransaction())
      }
    })
  )
}

const TRANSACTION_SAVED_MESSAGE = {
  successMessage: 'Transaction Saved Successfully',
  errorMessage: http.httpErrorMessage
}

export const handleFeeTransaction = (payload, feeDistributions) => {
  const {transactionType} = payload

  let amountProp = 'amount'
  if (isFeeAdjustment(transactionType)) {
    amountProp = 'adjustment'
    payload.amount = 0
    /**
     * Important Note - API Requires the FEE_ADJUSTMENT Transaction to be sent as FEE_RECEIPT
     */
    payload.transactionType = 'FEE_RECEIPT'
  }

  payload.feeDistributions = feeDistributions.filter(feeDistribution => feeDistribution[amountProp] !== 0)
}

export const saveTransaction = () => (dispatch, getState) => {
  const {
    form: {AddEditTransactionForm: {values: transaction}},
    transactions: {addEditTransaction: {transactionType}, feeDistributions}
  } = getState()

  const {transactionId} = transaction
  const newTransaction = !exists(transactionId)
  const url = newTransaction ? TRANSACTIONS_URL : `${TRANSACTIONS_URL}/${transactionId}`
  const method = newTransaction ? 'post' : 'put'

  const payload = {
    transactionType,
    ...transaction
  }

  if (transactionType === 'WITHDRAWAL' && payload.amount > 0) {
    payload.amount = payload.amount * -1
  }

  if (isFeeTransaction(payload)) {
    handleFeeTransaction(payload, feeDistributions)
  }

  dispatch({
    type: 'SAVE_TRANSACTION',
    payload: http[method](url, payload, TRANSACTION_SAVED_MESSAGE, 'AddEditTransaction').then(result => {
      dispatch(clearTransaction())
      dispatch(loadAllContracts())
      return result
    })
  })
}

const PRIOR_WITHDRAWALS_URL = `${apiPath}/prior-withdrawals`

export const saveWithdrawalTransaction = () => (dispatch, getState) => {
  const {
    form: {AddEditTransactionForm: {values: {amount, contractId, effectiveDate, transactionId}}}
  } = getState()

  return http.get(PRIOR_WITHDRAWALS_URL, {contractId, amount, effectiveDate: serializeDate(effectiveDate), transactionId})
    .then(({confirmationRequired, message}) => {
      if (confirmationRequired) {
        dispatch(openConfirmationAlert({
          message,
          onOk: () => dispatch(saveTransaction())
        }, 'withdrawalLimitWarning'))
      } else {
        dispatch(saveTransaction())
      }
    })
}

export const clearTransaction = () => ({
  type: 'CLEAR_TRANSACTION'
})

export const transactionsRefreshed = () => ({
  type: 'TRANSACTION_REFRESHED'
})

const balanceApi = (contractId, date) => `${apiPath}/contracts/${contractId}/balances?effectiveDate=${date}`

export const getBalanceForEffectiveDate = () => (dispatch, getState) => {
  const {
    transactions: {
      addEditTransaction: {transactionType}
    },
    form: {
      AddEditTransactionForm: {
        values: {
          contractId,
          effectiveDate
        }
      }
    }
  } = getState()

  if (transactionType === 'TERMINATION' && contractId && effectiveDate) {
    const effectiveDateQueryParam = moment(effectiveDate).startOf('day').format()
    return http
      .get(balanceApi(contractId, effectiveDateQueryParam))
      .then(({results}) => {
        if (!_.isEmpty(results) && results[0] && exists(results[0].beginningBookValue)) {
          const balance = results[0].beginningBookValue
          dispatch(change('AddEditTransactionForm', 'amount', balance * -1))
        } else {
          dispatch(change('AddEditTransactionForm', 'amount', 0))
        }
      })
  }
}

const openInvoicesUrl = (contractId) => `${apiPath}/contracts/${contractId}/open-invoices`

const orderFeeDistributions = (feeDistributions) =>
  _.orderBy(feeDistributions, ({invoiceNumber}) => !invoiceNumber ? Number.MAX_VALUE : invoiceNumber, 'asc')

const toFeeDistribution = ({invoiceId, invoiceNumber, outstandingBalance, feesReceived, adjustments}) => ({
  invoiceId,
  invoiceNumber,
  amount: feesReceived || 0,
  adjustment: adjustments || 0,
  beginningBalance: outstandingBalance,
  endingBalance: outstandingBalance
})

export const mergeFeeDistributions = (existingFeeDistributions, invoices) => {
  // const {feeDistributions: existingFeeDistributions} = transaction

  const paidInvoices = _(existingFeeDistributions)
    .map('invoiceNumber')
    .compact().value()

  const openInvoices = invoices
    .filter(({invoiceNumber}) => paidInvoices.indexOf(invoiceNumber) === -1)
    .map(toFeeDistribution)

  return orderFeeDistributions(existingFeeDistributions.concat(openInvoices))
}

export const loadFeeDistributions = () => (dispatch, getState) => {
  const {
    form: {AddEditTransactionForm: {values: {transactionId, contractId, parentTransactionId}}},
    transactions: {feeDistributions}
  } = getState()

  if (_.isEmpty(feeDistributions)) {
    const loadTransaction = http.get(`${TRANSACTIONS_URL}/${transactionId}`)
    const loadOpenInvoices = http.get(openInvoicesUrl(contractId))
    const loadTransactionDetails = [loadTransaction, loadOpenInvoices]

    // Note: For FEE ADJUSTMENT transactions the fee distributions are associated with parent fee receipt transaction
    // Stable Value API creates a FEE RECEIPT transaction for each FEE ADJUSTMENT
    if (parentTransactionId) {
      loadTransactionDetails.push(http.get(`${TRANSACTIONS_URL}/${parentTransactionId}`))
    }

    return Promise.all(loadTransactionDetails).then(responses => {
      const transaction = responses[0]
      const invoices = responses[1].results
      const parentTransaction = responses[2]

      let existingFeeDistributions = transaction.feeDistributions || []

      if (parentTransaction && !_.isEmpty(parentTransaction.feeDistributions)) {
        existingFeeDistributions = existingFeeDistributions.concat(parentTransaction.feeDistributions)
      }

      const feeDistributions = mergeFeeDistributions(existingFeeDistributions, invoices)

      if (!_.isEmpty(feeDistributions)) {
        dispatch({
          type: 'SET_FEE_DISTRIBUTIONS',
          payload: feeDistributions
        })
        updateFeeDistributionTotal(dispatch, feeDistributions)
      }
    })
  }
}

export const createNewFeeDistribution = () => (dispatch, getState) => {
  const {
    form: {AddEditTransactionForm: {values: {contractId}}}
  } = getState()

  return http.get(openInvoicesUrl(contractId)).then(({results: invoices}) => {
    const feeDistributions = (invoices || [])
      .map(toFeeDistribution)
      .concat({ // Dummy Invoice for over payment
        isFutureInvoice: true,
        invoiceNumber: null,
        amount: 0,
        adjustment: 0,
        endingBalance: 0,
        beginningBalance: 0
      })

    dispatch({
      type: 'SET_FEE_DISTRIBUTIONS',
      payload: orderFeeDistributions(feeDistributions)
    })
  })
}

export const feeReceiptAmountChanged = (totalFeeReceived) => (dispatch, getState) => {
  const {transactions: {feeDistributions}} = getState()

  let balance = totalFeeReceived
  const adjustedFeeDistributions = feeDistributions.map(feeDistribution => {
    const {beginningBalance = 0, invoiceNumber} = feeDistribution
    if (balance > 0) {
      let balanceApplied
      if (invoiceNumber) { // Apply remaining balance towards beginning balance
        balanceApplied = balance > beginningBalance ? beginningBalance : balance
      } else { // Apply remaining balance to Next (Dummy) Invoice
        balanceApplied = balance
      }
      balance = _.round(balance - balanceApplied, 4)
      return {
        ...feeDistribution,
        amount: balanceApplied,
        endingBalance: beginningBalance - balanceApplied
      }
    } else {
      return {
        ...feeDistribution,
        amount: 0,
        endingBalance: feeDistribution.beginningBalance
      }
    }
  })

  dispatch({
    type: 'SET_FEE_DISTRIBUTIONS',
    payload: adjustedFeeDistributions
  })

  updateFeeDistributionTotal(dispatch, adjustedFeeDistributions)
}

const isSameFeeDistribution = (modifiedFeeDistribution, feeDistribution) =>
  (modifiedFeeDistribution.invoiceNumber === feeDistribution.invoiceNumber) ||
  (modifiedFeeDistribution.isFutureInvoice && feeDistribution.isFutureInvoice)

const applyAmount = ({beginningBalance}, amount) => _.round(beginningBalance + amount, 4)

export const feeAmountChanged = (feeProperty, amount, modifiedFeeDistribution) => (dispatch, getState) => {
  const {transactions: {feeDistributions}} = getState()
  const {transactionType} = modifiedFeeDistribution

  const modifiedFeeDistributions = feeDistributions
    .map(feeDistribution => isSameFeeDistribution(modifiedFeeDistribution, feeDistribution) ? {
      ...feeDistribution,
      [feeProperty]: amount,
      endingBalance: isFeeReceipt(transactionType)
        ? applyAmount(feeDistribution, -amount)
        : applyAmount(feeDistribution, amount)
    } : feeDistribution)

  dispatch({
    type: 'SET_FEE_DISTRIBUTIONS',
    payload: modifiedFeeDistributions
  })

  if (isFeeReceipt(transactionType)) {
    updateFeeDistributionTotal(dispatch, modifiedFeeDistributions)
  }
}

const updateFeeDistributionTotal = (dispatch, feeDistributions) => {
  const total = _.reduce(feeDistributions, (sum, {amount}) => sum + amount, 0)
  dispatch(change('AddEditTransactionForm', 'feeDistributionTotal', total))
}
