import React from 'react'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import { RaisedButton, Card } from 'material-ui'
import styles from '../styles/styles'
import { reduxForm } from 'redux-form'
import YesNo from '../components/forms/YesNo'
import ReduxDialog from '../components/redux-dialog'
import { closeDialog } from '../components/redux-dialog/redux-dialog'
import { runPreliminaryMonthlyCycle } from './CyclesActions'

const formId = 'PreliminaryMonthlyRun'
const dialogName = 'runPreliminaryMonthlyCycle'

export const PreliminaryMonthlyRun = (props) => {
  const {handleSubmit, closeDialog, runPreliminaryMonthlyCycle} = props

  const actions = [
    <RaisedButton
      key="Run"
      label="Run"
      primary={true}
      style={{marginRight: 20}}
      type="submit"
      form={formId}
    />,
    <RaisedButton
      id="PreliminaryMonthlyRun-Cancel"
      key="Cancel"
      label="Cancel"
      onClick={() => {
        closeDialog(dialogName)
      }}
    />
  ]
  const title = 'Preliminary Monthly Run'
  return (
    <ReduxDialog
      dialogName={dialogName}
      title={title}
      actions={actions}
      modal={true}
      autoScrollBodyContent={true}
    >
      <Card containerStyle={{padding: 20, marginTop: 10}}>
        <form id={formId} onSubmit={handleSubmit(runPreliminaryMonthlyCycle)}>
          <div className="row">
            <div className="col-lg-6" style={styles.toggleDivStyle}>
              <YesNo
                name="invoices"
                label="Preliminary Invoices"
              />
            </div>
            <div className="col-lg-6" style={styles.toggleDivStyle}>
              <YesNo
                name="statements"
                label="Preliminary BV Statements"
              />
            </div>
          </div>
        </form>
      </Card>
    </ReduxDialog>
  )
}

PreliminaryMonthlyRun.propTypes = {
  handleSubmit: T.func.isRequired,
  closeDialog: T.func.isRequired,
  runPreliminaryMonthlyCycle: T.func.isRequired
}

export const PreliminaryMonthlyRunForm = reduxForm({
  form: formId,
  enableReinitialize: true
})(PreliminaryMonthlyRun)

const actions = {
  closeDialog,
  runPreliminaryMonthlyCycle
}

export default connect(() => ({}), actions)(PreliminaryMonthlyRunForm)
