import React from 'react'
import { PropTypes as T } from 'prop-types'
import _ from 'lodash'
import { connect } from 'react-redux'
import { IntlProvider } from 'react-intl'
import { Card } from 'material-ui/Card'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import RaisedButton from 'material-ui/RaisedButton'
import { red500 } from 'material-ui/styles/colors'
import Cached from 'material-ui/svg-icons/action/cached'
import PlayArrow from 'material-ui/svg-icons/av/play-arrow'
import { loadDailyCycleMessages, runDailyCycle } from './CyclesActions'
import { Grid, CreateGridConfig } from '../components/grid'
import { CycleMessageColumns } from './CycleMessageColumns'

const config = {
  showHeaderToolbar: false,
  paginate: false,
  defaultSortName: 'effectiveDate',
  defaultSortOrder: 'desc'
}
const style = {margin: 8}

export class DailyCycles extends React.Component {
  componentDidMount () {
    const {loadDailyCycleMessages} = this.props
    loadDailyCycleMessages()
  }

  render () {
    const {dailyCycleMessages, loadDailyCycleMessages, runDailyCycle, isRunning = false, isStarted = false, canRunDailyCycle} = this.props
    console.log('canRunDailyCycle' + canRunDailyCycle)
    const options = CreateGridConfig(dailyCycleMessages, config)
    return (
      <div>
        <Card>
          <Toolbar style={{borderTop: 'solid 1px #e0e0e0', backgroundColor: '#f8f9fa'}}>
            <ToolbarGroup></ToolbarGroup>
            <ToolbarGroup style={{marginRight: '50px'}} lastChild={true}>
              <RaisedButton style={style} label="Refresh" icon={<Cached/>} onClick={() => { loadDailyCycleMessages() }} />
              <RaisedButton style={style} label="Run" icon={<PlayArrow/>} primary={true} disabled={isRunning || isStarted || !canRunDailyCycle} onClick={() => { runDailyCycle() }} />
            </ToolbarGroup>
          </Toolbar>
        </Card>
        <IntlProvider>
          <Grid id="DailyCycleMessages" options={options} data={dailyCycleMessages} columns={CycleMessageColumns}/>
        </IntlProvider>
      </div>
    )
  }
}

DailyCycles.propTypes = {
  loadDailyCycleMessages: T.func.isRequired,
  runDailyCycle: T.func.isRequired,
  dailyCycleMessages: T.array,
  isRunning: T.bool,
  isStarted: T.bool,
  canRunDailyCycle: T.bool
}

export const actions = {
  loadDailyCycleMessages,
  runDailyCycle
}

export const mapStateToProps = ({cycles: {dailyCycleMessages, isRunning, isStarted}, currentUser: {RUN_DAILY_CYCLE}}) => ({
  dailyCycleMessages,
  isRunning,
  isStarted,
  canRunDailyCycle: RUN_DAILY_CYCLE
})

export default connect(mapStateToProps, actions)(DailyCycles)
