import {destroy, change, initialize} from 'redux-form'
import _ from 'lodash'
import moment from 'moment'
import {apiPath} from '../config'
import http from '../actions/http'
import {closeDialog, openSnackbar} from '../components/redux-dialog/redux-dialog'

export const activateCyclesTab = (label) => ({
  type: 'ACTIVATE_CYCLES_TAB',
  label
})

const loadCycleMessages = (type, apiCyclesPathSuffix) => () => (dispatch) => {
  dispatch({
    type,
    payload: http.get(`${apiPath}/cycles/${apiCyclesPathSuffix}`)
  })
}

export const loadDailyCycleMessages = loadCycleMessages('LOAD_DAILY_CYCLE_MESSAGES', 'daily')
export const loadMonthlyCycleMessages = loadCycleMessages('LOAD_MONTHLY_CYCLE_MESSAGES', 'monthly')

export const runDailyCycle = () => (dispatch) => {
  const url = `${apiPath}/cycles/daily`

  dispatch({
    type: 'RUN_DAILY_CYCLE',
    payload: http.post(url, {}, 'Daily Cycle started')
  })
  dispatch({type: 'IS_CYCLE_STARTED', isStarted: true})
}

export const runPreliminaryMonthlyCycle = () => (dispatch, getState) => {
  const {form: {PreliminaryMonthlyRun}} = getState()
  const url = `${apiPath}/cycles/monthly`
  return dispatch({
    type: 'RUN_MONTHLY_CYCLE',
    payload: http.post(url, PreliminaryMonthlyRun.values || {}, { successMessage: 'Preliminary Monthly Cycle started' })
  }).then(response => {
    dispatch({type: 'IS_CYCLE_STARTED', isStarted: true})
    dispatch(closeDialog('runPreliminaryMonthlyCycle'))
    return response
  })
}

export const finalizeMonthlyCycle = () => (dispatch) => {
  const url = `${apiPath}/cycles/monthly/finalize`

  dispatch({
    type: 'FINALIZE_MONTHLY_CYCLE',
    payload: http.post(url, {}, 'Finalizing Month Cycle started')
  })
  dispatch({type: 'IS_CYCLE_STARTED', isStarted: true})
}
