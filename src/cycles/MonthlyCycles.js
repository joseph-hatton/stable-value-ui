import React from 'react'
import { IntlProvider } from 'react-intl'
import { connect } from 'react-redux'
import { PropTypes as T } from 'prop-types'
import _ from 'lodash'
import { Card } from 'material-ui/Card'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import RaisedButton from 'material-ui/RaisedButton'
import { red500 } from 'material-ui/styles/colors'
import Cached from 'material-ui/svg-icons/action/cached'
import ActionUpdate from 'material-ui/svg-icons/action/update'
import PlayArrow from 'material-ui/svg-icons/av/play-arrow'
import { Grid, CreateGridConfig } from '../components/grid'
import { openDialog, openConfirmationAlert } from '../components/redux-dialog/redux-dialog'
import { loadMonthlyCycleMessages, finalizeMonthlyCycle } from './CyclesActions'
import PreliminaryMonthlyRun from './PreliminaryMonthlyRun'
import { CycleMessageColumns } from './CycleMessageColumns'

const config = {
  showHeaderToolbar: false,
  paginate: false,
  defaultSortName: 'effectiveDate',
  defaultSortOrder: 'desc'
}
const style = {margin: 8}

export class MonthlyCycles extends React.Component {
  componentDidMount () {
    const {loadMonthlyCycleMessages} = this.props
    loadMonthlyCycleMessages()
  }

  promptForPreliminaryMonthlyCycle () {
    const { openDialog } = this.props
    openDialog('runPreliminaryMonthlyCycle')
  }

  promptForFinalizeMonthlyCycle () {
    const { openConfirmationAlert, finalizeMonthlyCycle } = this.props
    openConfirmationAlert({
        message: 'Are you sure you want to finalize the month?',
        onOk: () => (finalizeMonthlyCycle())
    })
  }

  render () {
    const {monthlyCycleMessages, loadMonthlyCycleMessages, isRunning = false, isStarted = false, canRunMonthlyCycle} = this.props
    const options = CreateGridConfig(monthlyCycleMessages, config)
    return (
      <div>
        <Card>
          <Toolbar style={{borderTop: 'solid 1px #e0e0e0', backgroundColor: '#f8f9fa'}}>
            <ToolbarGroup></ToolbarGroup>
            <ToolbarGroup style={{marginRight: '50px'}} lastChild={true}>
              <RaisedButton style={style} label="Refresh" icon={<Cached/>} onClick={() => { loadMonthlyCycleMessages() }} />
              <RaisedButton style={style} label="Run" icon={<PlayArrow/>} primary={true} disabled={isRunning || isStarted || !canRunMonthlyCycle} onClick={::this.promptForPreliminaryMonthlyCycle} />
              <RaisedButton style={style} label="Finalize Month" icon={<ActionUpdate/>} secondary={true} disabled={isRunning || isStarted || !canRunMonthlyCycle} onClick={::this.promptForFinalizeMonthlyCycle} />
            </ToolbarGroup>
          </Toolbar>
        </Card>
        <IntlProvider>
          <Grid id="MonthlyCycleMessages" options={options} data={monthlyCycleMessages} columns={CycleMessageColumns}/>
        </IntlProvider>
        <PreliminaryMonthlyRun/>
      </div>
    )
  }
}

MonthlyCycles.propTypes = {
  openDialog: T.func.isRequired,
  openConfirmationAlert: T.func.isRequired,
  loadMonthlyCycleMessages: T.func.isRequired,
  finalizeMonthlyCycle: T.func.isRequired,
  monthlyCycleMessages: T.array,
  isRunning: T.bool,
  isStarted: T.bool,
  canRunMonthlyCycle: T.bool
}

export const actions = {
  openDialog,
  openConfirmationAlert,
  loadMonthlyCycleMessages,
  finalizeMonthlyCycle
}

export const mapStateToProps = ({cycles: {monthlyCycleMessages, isRunning, isStarted}, currentUser: {RUN_MONTHLY_CYCLE}}) => ({
  monthlyCycleMessages,
  isRunning,
  isStarted,
  canRunMonthlyCycle: RUN_MONTHLY_CYCLE
})

export default connect(mapStateToProps, actions)(MonthlyCycles)
