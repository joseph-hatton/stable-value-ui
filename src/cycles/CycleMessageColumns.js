import React from 'react'
const commonProps = {
  customProps: {
    textAlign: 'left',
    width: '150'
  }
}

export const CycleMessageColumns = [
  {
    field: 'id',
    customProps: {
      hidden: true,
      isKey: true
    }
  },
  {
    field: 'messageTypeName',
    sort: true,
    label: 'Type',
    customProps: {
      textAlign: 'left',
      textAlignHeader: 'left',
      width: '100'
    }
  },
  {
    field: 'contractNumber',
    sort: true,
    label: 'Contract #',
    ...commonProps
  },
  {
    field: 'message',
    sort: true,
    label: 'Message',
    customProps: {
      textAlign: 'left',
      textAlignHeader: 'left'
    }
  }
]

const extraColumns = [
]
