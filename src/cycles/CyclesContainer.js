import React from 'react'
import { PropTypes as T } from 'prop-types'
import _ from 'lodash'
import { connect } from 'react-redux'
import { Tabs, Tab } from 'material-ui/Tabs'
import { Card } from 'material-ui/Card'
import { activateCyclesTab } from './CyclesActions'
import CyclesTabs from './CyclesTabs'

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400
  },

  tabContainer: {
    marginTop: 20
  },

  tabContentContainerStyle: {
    padding: 20
  },
  mainTab: {
    backgroundColor: '#0062cc'
  }
}

export const CyclesContainer = (props) => {
  const tabs = CyclesTabs
  const {activeCyclesTab, activateCyclesTab} = props
  const indexOfLastTab = _.findIndex(CyclesTabs, tab => tab.label === activeCyclesTab)
  return (
    <Card style={styles.tabContainer}>
      <Tabs initialSelectedIndex={indexOfLastTab > -1 ? indexOfLastTab : 0}>
        {
          tabs.map(({label, CycleTab, loadDynamically}) => (
            <Tab key={label} label={label} onActive={() => { activateCyclesTab(label) }} style={styles.mainTab}>
              {activeCyclesTab === label && <CycleTab {...props}/>}
            </Tab>
          ))
        }
      </Tabs>
    </Card>
  )
}

CyclesContainer.propTypes = {
  activeCyclesTab: T.string,
  activateCyclesTab: T.func.isRequired
}

export const mapStateToProps = ({cycles: {activeCyclesTab}}) => ({activeCyclesTab})

export default connect(mapStateToProps, {activateCyclesTab})(CyclesContainer)
