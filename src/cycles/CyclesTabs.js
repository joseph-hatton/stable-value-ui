import React from 'react'
import DailyCycles from './DailyCycles'
import MonthlyCycles from './MonthlyCycles'

const CyclesTabs = [
  {
    label: 'Daily',
    CycleTab: DailyCycles
  },
  {
    label: 'Monthly',
    CycleTab: MonthlyCycles
  }
]

export default CyclesTabs
