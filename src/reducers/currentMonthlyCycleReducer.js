const initialState = {
    current: {}
}

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case 'LOAD_MONTHLY_CYCLE_FULFILLED':
            return {
                ...state,
                current: action.payload
            }
        default:
            return state
    }
}
