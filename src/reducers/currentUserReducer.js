import determineEntitlements from '../entitlements'

const initialState = {
  id: 'unknown',
  name: 'unknown'
}

export default (state = initialState, action = {}) => {
  if (action.type === 'SET_CURRENT_USER') {
    return {
      ...state,
      ...action.user,
      ...(determineEntitlements(action.user.groups))
    }
  } else if (action.type === 'TIME_OUT_USER') {
    return {
      ...state,
      session: {
        ...state.session,
        timedOut: true
      }
    }
  } else {
    return state
  }
}
