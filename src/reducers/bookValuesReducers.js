import moment from 'moment'
import _ from 'lodash'
const initialState = {
  allBookValues: [],
  loadedBookValue: [],
  filtered: false,
  dateFilter: moment(),
  filterOnDay: false,
  contractNumber: 0,
  shortPlanName: ''
}

  export default (state = initialState, action) => {
    switch (action.type) {
      case 'LOAD_ALL_BOOK_VALUES_FULFILLED':
      return {
        ...state,
        filtered: false,
        allBookValues: action.payload.results
      }
      case 'FILTER_ALL_BOOK_VALUES_FULFILLED':
      return {
        ...state,
        filtered: true,
        allBookValues: action.payload.results
      }
      case 'SELECT_BOOK_VALUES_BY_CONTRACT_ID_FULFILLED':
      const {contractNumber, shortPlanName} = action.payload[1]
      return {
        ...state,
        filtered: false,
        loadedBookValue: action.payload[0].results,
        contractNumber,
        shortPlanName
      }
      case 'FILTER_BOOK_VALUES_BY_CONTRACT_ID_FULFILLED':
      return {
        ...state,
        filtered: true,
        loadedBookValue: action.payload.results
      }
      case 'SET_CONTRACT_ID':
      return {
        ...state,
        currentContractId: action.payload
      }
      case 'SET_DATE_FILTER':
      return {
        ...state,
        dateFilter: action.payload
      }
      case 'SET_FILTER_ON_DAY':
      return {
        ...state,
        filterOnDay: action.payload
      }
      default:
      return state
    }
  }
