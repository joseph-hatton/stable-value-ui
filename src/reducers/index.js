import {combineReducers} from 'redux'
import currentUserReducer from './currentUserReducer'
import contractReducers from './contractReducers'
import companyReducers from './companyReducers'
import contactReducers from './contactReducers'
import {dialogReducer} from '../components/redux-dialog'
import {Reducers as gridReducers} from 'react-redux-grid'
import {reducer as formReducer} from 'redux-form'
import referenceDataReducer from './referenceDataReducer'
import portfolioAllocationReducer from '../wrap-portfolio/portfolioAllocationReducer'
import portfolioReducer from '../wrap-portfolio/portfolioReducer'
import currentMonthlyCycleReducer from './currentMonthlyCycleReducer'
import feesReducers from './feesReducers'
import bookValuesReducers from './bookValuesReducers'
import transactionsReducers from '../transactions/TransactionsReducers'
import watchListReducers from './watchListReducers'
import cyclesReducers from './cyclesReducers'
import MarketValueReducers from '../marketValue/MarketValueReducers'
import sideBarReducers from './sideBarReducers'

export default combineReducers({
  ...gridReducers,
  currentUser: currentUserReducer,
  companies: companyReducers,
  contacts: contactReducers,
  contracts: contractReducers,
  portfolios: portfolioReducer,
  portfolioAllocations: portfolioAllocationReducer,
  form: formReducer,
  referenceData: referenceDataReducer,
  dialog: dialogReducer,
  monthlyCycle: currentMonthlyCycleReducer,
  fees: feesReducers,
  bookValues: bookValuesReducers,
  transactions: transactionsReducers,
  watchLists: watchListReducers,
  cycles: cyclesReducers,
  marketValues: MarketValueReducers,
  sideBar: sideBarReducers
})
