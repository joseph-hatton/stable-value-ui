import _ from 'lodash'
import {mapContract, removeEditHistory} from '../contracts/ContractPayloadMapper'
import mapContacts from './ContactMapper'
import validateContractLifeCycleStatus from '../contracts/ContractValidators'
import {FormatRate} from '../components/forms/Validators'

const defaultContract = {
  status: 'PENDING'
}

const initialState = {
  allContracts: {results: undefined},
  contract: undefined,
  currentContractFormId: undefined,
  derivativeTypeData: [],
  activeUnderwritingTab: 'Model Inputs',
  creditingRates: [],
  canLeaveWithoutSaving: false
}

export const insertUpdatedContract = (savedContract, contracts) => {
  const contractIndex = _.findIndex(contracts, ({contractId}) => contractId === savedContract.contractId)
  console.log('about to insert', {contracts, contractIndex})
  if (contractIndex === -1) {
    contracts.push(savedContract)
  } else {
    contracts.splice(contractIndex, 1, savedContract)
  }
  const sorted = _.sortBy(contracts, 'contractNumber')
  console.log('already inserted', {contracts, contractIndex, sorted})

  return sorted
}

const contractSaved = (state, payload) => {
  const savedContract = mapContract(payload)
  return {
    ...state,
    contract: savedContract,
    allContracts: {
      results: insertUpdatedContract(savedContract, state.allContracts.results)
    },
    contractLifeCycleStatus: validateContractLifeCycleStatus(savedContract, state.contractContacts)
  }
}

const addOrUpdateCreditingRate = (creditingRates, savedCreditingRate, marketValueView) => {
  if (marketValueView) {
    return creditingRates
  }
  const existingCreditingRate = creditingRates.find(creditingRate => creditingRate.creditingRateId === savedCreditingRate.creditingRateId)
  if (existingCreditingRate) {
    return [savedCreditingRate].concat(_.without(creditingRates, existingCreditingRate))
  } else {
    return [savedCreditingRate].concat(creditingRates)
  }
}

const mapStableValue = stableValue => ({
  ...stableValue,
  calculatedMvBv: _.isNumber(stableValue.svBookValue) && stableValue.svBookValue !== 0
    ? stableValue.svMarketValue / stableValue.svBookValue
    : 0
})

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_ALL_CONTRACTS_FULFILLED':
      return {
        ...state,
        allContracts: action.payload,
        error: undefined
      }
    case 'INITIALIZE_CONTRACT':
      return {
        ...state,
        contract: {
          ...defaultContract
        },
        contractContacts: [],
        contractLifeCycleStatus: undefined
      }
    case 'SET_CURRENT_CONTRACT_FORM':
      return {
        ...state,
        currentContractFormId: action.contractFormId
      }
    case 'SAVE_CONTRACT_FULFILLED':
      return contractSaved(state, action.payload)
    case 'UPDATE_CONTRACT_STATUS_FULFILLED':
      return contractSaved(state, action.payload)
    case 'LOAD_CONTRACT_FULFILLED':
      const contractResponse = action.payload[0]
      const contractContactsResponse = action.payload[1]
      const loadedContract = mapContract(contractResponse)
      const contractContacts = mapContacts(contractContactsResponse.results)
      return {
        ...state,
        contract: loadedContract,
        contractContacts,
        contractLifeCycleStatus: validateContractLifeCycleStatus(loadedContract, contractContacts)
      }
    case 'CLEAR_EDIT_CONTRACT': {
      return {
        ...state,
        lastContractId: state.contract ? state.contract.contractId : null,
        contract: undefined
      }
    }
    case 'LOAD_MODEL_INPUTS_FULFILLED': {
      return {
        ...state,
        modelInputs: _.orderBy(action.payload.results, 'effectiveDate', 'desc')
      }
    }
    case 'SAVE_MODEL_INPUTS_FULFILLED': {
      const savedModelInput = action.payload
      const selectedModelInput = state.modelInput
      let newModelInputs
      if (selectedModelInput) { // Edit
        newModelInputs = _.map(state.modelInputs,
          modelInput =>
            modelInput.underwritingId === selectedModelInput.underwritingId ? savedModelInput : modelInput)
      } else { // Create
        newModelInputs = _.concat(state.modelInputs, savedModelInput)
      }
      return {
        ...state,
        modelInputSaved: true,
        modelInputs: newModelInputs
      }
    }
    case 'SELECT_MODEL_INPUT': {
      return {
        ...state,
        modelInput: action.payload,
        modelInputSaved: undefined
      }
    }
    case 'DELETE_MODEL_INPUT_FULFILLED': {
      return {
        ...state,
        modelInputs: state.modelInputs.filter((i) => i.underwritingId !== action.payload.id)
      }
    }
    case 'LOAD_STABLE_VALUE_FUNDS_FULFILLED': {
      return {
        ...state,
        stableValueFunds: action.payload.results.map(mapStableValue)
      }
    }
    case 'SAVE_STABLE_VALUE_FUND_FULFILLED': {
      action.payload.calculatedMvBv = _.isNumber(action.payload.svBookValue) && action.payload.svBookValue !== 0
        ? action.payload.svMarketValue / action.payload.svBookValue
        : 0
      const ready = state.stableValueFunds
        .filter((i) => i.stableValueFundId !== action.payload.stableValueFundId)
        .concat([action.payload])
      return {
        ...state,
        stableValueFundSaved: true,
        stableValueFunds: ready
      }
    }
    case 'SELECT_STABLE_VALUE_FUND': {
      return {
        ...state,
        stableValueFund: action.payload,
        stableValueFundSaved: undefined
      }
    }
    case 'DELETE_STABLE_VALUE_FUND_FULFILLED': {
      return {
        ...state,
        stableValueFunds: state.stableValueFunds.filter((i) => i.stableValueFundId !== action.payload.id)
      }
    }
    case 'ACTIVATE_UNDERWRITING_TAB': {
      return {
        ...state,
        activeUnderwritingTab: action.label
      }
    }
    case 'LOAD_CONTRACT_CONTACTS_FULFILLED': {
      const contractContacts = mapContacts(action.payload.results)
      return {
        ...state,
        contractContacts,
        contractLifeCycleStatus: validateContractLifeCycleStatus(state.contract, contractContacts)
      }
    }
    case 'SELECT_CONTACT': {
      return {
        ...state,
        newContact: action.payload
      }
    }
    case 'SELECT_CONTRACT_CONTACT': {
      const {contact, contractContact} = action.payload
      return {
        ...state,
        selectedContractContact: contractContact,
        existingContact: contact
      }
    }
    case 'SELECT_AUDITABLE_CONTRACT_FIELD': {
      return {
        ...state,
        selectedAuditableField: action.selectedAuditableField
      }
    }
    case 'LOAD_DERIVATIVE_TYPES_FULFILLED': {
      return {
        ...state,
        derivativeTypeData: action.payload.results
      }
    }
    case 'DELETE_CONTRACT_HISTORY': {
      const {effectiveDate, field} = action
      const {contract} = state
      return {
        ...state,
        contract: {
          ...contract,
          editHistories: removeEditHistory(contract.editHistories, action.effectiveDate, field)
        }
      }
    }
    case 'LOAD_CONTRACT_CREDITING_RATES_FULFILLED': {
      return {
        ...state,
        creditingRates: action.payload
      }
    }
    case 'LOAD_CONTRACT_RISK_SCORECARDS_FULFILLED': {
      return {
        ...state,
        riskScorecards: action.payload
      }
    }
    case 'SELECT_RISK_SCORECARD_INPUT': {
      return {
        ...state,
        riskScorecardId: action.payload ? (action.payload.riskScorecardId ? action.payload.riskScorecardId : state.riskScorecardId) : null,
        riskScorecard: action.payload
      }
    }
    case 'DELETE_RISK_SCORECARD': {
      let {riskScorecards} = state
      riskScorecards = riskScorecards.filter((i) => i.riskScorecardId !== action.payload.id)
      return {
        ...state,
        riskScorecards: riskScorecards
      }
    }
    case 'SAVE_RISK_SCORECARD_INPUTS_FULFILLED': {
      let {riskScorecards} = state
      const riskScorecardIndex = _.findIndex(riskScorecards, (i) => i.riskScorecardId === action.payload.riskScorecardId)
      riskScorecards = riskScorecards.filter((i) => i.riskScorecardId !== action.payload.riskScorecardId)
      if (riskScorecardIndex === -1) {
        riskScorecards.unshift(action.payload)
      } else {
        riskScorecards.splice(riskScorecardIndex, 0, action.payload)
      }
      return {
        ...state,
        riskScorecards: riskScorecards
      }
    }
    case 'SELECT_CREDITING_RATE': {
      const {
        selectedCreditingRate,
        editCreditingRate,
        calculateCreditingRateAfterSavingEffectiveDate = false
      } = action
      return {
        ...state,
        selectedCreditingRate,
        editCreditingRate,
        calculateCreditingRateAfterSavingEffectiveDate
      }
    }
    case 'NET_CREDITING_RATE_ADJUSTED': {
      const {editCreditingRate} = state
      const {netCreditingRate, isNetCreditingRateLessThanMinimum} = action
      return {
        ...state,
        editCreditingRate: {
          ...editCreditingRate,
          netCreditingRate,
          isNetCreditingRateLessThanMinimum
        }
      }
    }
    case 'GET_BALANCE_FOR_REFERENCE_DATE_FULFILLED': {
      return {
        ...state,
        balance: _.first(action.payload.results)
      }
    }
    case 'CLEAR_ADD_EDIT_MARKET_VALUE': {
      return {
        ...state,
        balance: null,
        selectedCreditingRate: null,
        calculateCreditingRateAfterSavingEffectiveDate: false,
        editCreditingRate: null
      }
    }
    case 'SAVE_MARKET_VALUE_FULFILLED': {
      return {
        ...state,
        creditingRates: _.orderBy(addOrUpdateCreditingRate(state.creditingRates, action.payload, state.marketValueView), ['referenceDate'], ['desc']),
        refreshMarketValues: state.marketValueView
      }
    }
    case 'CALCULATE_CREDITING_RATE_FULFILLED': {
      return {
        ...state,
        creditingRates: _.orderBy(addOrUpdateCreditingRate(state.creditingRates, action.payload, state.marketValueView), ['referenceDate'], ['desc']),
        refreshMarketValues: state.marketValueView
      }
    }
    case 'DELETE_MARKET_VALUE_FULFILLED': {
      const {creditingRates} = state
      const {payload: {id}} = action
      return {
        ...state,
        creditingRates: _.filter(creditingRates, ({creditingRateId}) => creditingRateId !== id),
        refreshMarketValues: state.marketValueView
      }
    }
    case 'UPDATE_CREDITING_RATE_FULFILLED': {
      return {
        ...state,
        creditingRates: _.orderBy(addOrUpdateCreditingRate(state.creditingRates, action.payload, state.marketValueView), ['referenceDate'], ['desc']),
        balance: null,
        selectedCreditingRate: null,
        calculateCreditingRateAfterSavingEffectiveDate: false,
        editCreditingRate: null,
        refreshMarketValues: state.marketValueView
      }
    }
    case 'REFRESH_MARKET_VALUES': {
      return {
        ...state,
        refreshMarketValues: true
      }
    }
    case 'MARKET_VALUE_REFRESHED': {
      return {
        ...state,
        refreshMarketValues: false
      }
    }
    case 'SET_MARKET_VALUE_VIEW': {
      return {
        ...state,
        marketValueView: action.marketValueView
      }
    }
    case 'LEAVE_WITHOUT_SAVING': {
      return {
        ...state,
        canLeaveWithoutSaving: true
      }
    }
    case 'RESET_LEAVE_WITHOUT_SAVING': {
      return {
        ...state,
        canLeaveWithoutSaving: false
      }
    }
    default:
      return state
  }
}
