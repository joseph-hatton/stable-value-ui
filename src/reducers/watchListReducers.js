const initialState = {
  all: [],
  reasons: [],
  actionSteps: [],
  currentWatchList: {},
  selectedWatchList: {},
  selectedReason: {},
  selectedActionStep: {},
  currentTab: 'watchLists',
  currentContractWatchListItem: {reasons: [], actionSteps: []}
}

  export default (state = initialState, action) => {
    switch (action.type) {
      case 'LOAD_ALL_WATCH_LISTS_FULFILLED':
      return {
        ...state,
        all: action.payload.results
      }
      case 'LOAD_REASONS_FULFILLED':
      return {
        ...state,
        reasons: action.payload.results
      }
      case 'LOAD_ACTION_STEPS_FULFILLED':
      return {
        ...state,
        actionSteps: action.payload.results
      }
      case 'LOAD_WATCH_LIST_FULFILLED':
      return {
        ...state,
        currentWatchList: action.payload
      }
      case 'SELECT_WATCH_LIST':
      return {
        ...state,
        selectedWatchList: action.payload,
        currentWatchList: action.payload
      }
      case 'DELETE_WATCH_LIST_FULFILLED':
      return {
        ...state,
        all: state.all.filter((i) => i.watchListId !== (action.payload.id * 1))
      }
      case 'DELETE_REASON_FULFILLED':
      return {
        ...state,
        reasons: state.reasons.filter((i) => i.reasonId !== (action.payload.id * 1))
      }
      case 'SELECT_REASON':
      return {
        ...state,
        selectedReason: action.payload
      }
      case 'SAVE_REASON_FULFILLED':
      const {reasonId} = action.payload
      let newReasons = state.reasons
      .filter((i) => i.reasonId !== reasonId)
      .concat([action.payload])
      .sort((a, b) => a.reasonId - b.reasonId)
      return {
        ...state,
        reasons: newReasons
      }
      case 'DELETE_ACTION_STEP_FULFILLED':
      return {
        ...state,
        actionSteps: state.actionSteps.filter((i) => i.actionStepId !== (action.payload.id * 1))
      }
      case 'SELECT_ACTION_STEP':
      return {
        ...state,
        selectedActionStep: action.payload
      }
      case 'SAVE_ACTION_STEP_FULFILLED':
      const {actionStepId} = action.payload
      let newActionSteps = state.actionSteps
      .filter((i) => i.actionStepId !== actionStepId)
      .concat([action.payload])
      .sort((a, b) => a.actionStepId - b.actionStepId)
      return {
        ...state,
        actionSteps: newActionSteps
      }
      case 'CHANGE_CURRENT_TAB':
      return {
        ...state,
        currentTab: action.payload
      }
      case 'SET_CURRENT_CONTRACT_WATCH_LIST_ITEM':
      return {
        ...state,
        currentContractWatchListItem: action.payload
      }
      default:
      return state
    }
  }
