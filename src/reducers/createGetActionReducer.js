export default (getStartState, getEndState, getErrorState) =>
  (state, action) => {
    switch (action.status) {
      case 'start':
        return {
          ...state,
          ...getStartState(state, action)
        }
      case 'error': {
        return {
          ...state,
          error: action.error,
          ...(getErrorState ? getErrorState(state, action) : {})
        }
      }
      case 'done':
        return {
          ...state,
          ...getEndState(state, action)
        }
      default:
        return state
    }
  }
