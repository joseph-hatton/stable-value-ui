import _ from 'lodash'

const initialState = {
  allCompanies: {results: undefined},
  editCompany: undefined,
  selectedContacts: [],
  selectedCompany: {}
}

const addOrUpdateCompany = (companies, savedCompany) => {
  const existingCompany = companies.find(company => company.companyId === savedCompany.companyId)
  if (existingCompany) {
    return [savedCompany].concat(_.without(companies, existingCompany))
  } else {
    return [savedCompany].concat(companies)
  }
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_ALL_COMPANIES_FULFILLED':
      return {
        ...state,
        allCompanies: action.payload
      }
    case 'SAVE_COMPANY_FULFILLED':
      const savedCompany = action.payload
      const companies = state.allCompanies.results || []
      return {
        ...state,
        allCompanies: {
          results: addOrUpdateCompany(companies, savedCompany)
        },
        editCompany: undefined
      }
    case 'LOAD_COMPANY_FULFILLED':
      return {
        ...state,
        editCompany: action.payload
      }
    case 'CLEAR_EDIT_COMPANY': {
      return {
        ...state,
        editCompany: undefined
      }
    }
    case 'DELETE_COMPANY_FULFILLED': {
      return {
        ...state,
        allCompanies: {
          results: state.allCompanies.results.filter((c) => c.companyId !== action.payload.id)
        }
      }
    }
    case 'SELECT_CONTACTS': {
      return {
        ...state,
        selectedContacts: action.contacts
      }
    }
    case 'SELECT_COMPANY': {
      return {
        ...state,
        selectedCompany: action.company
      }
    }
    default:
      return state
  }
}
