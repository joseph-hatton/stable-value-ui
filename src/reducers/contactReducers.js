import _ from 'lodash'

const defaultContact = {
  receiveStatement: false,
  receiveInvoice: false,
  receiveScheduleA: false
}

const initialState = {
  allContacts: [],
  allContracts: [],
  editContact: {...defaultContact},
  managers: [],
  selectedContracts: [],
  selectedContact: {},
  originalCopySelectedContact: {},
  bufferedActions: {
    assignContracts: [],
    unassignContracts: []
  }
}

const mapManagers = (allContacts) => _(allContacts)
  .filter(contact => contact.contactType === 'INVESTMENT_MANAGER')
  .map(contact => ({value: contact.companyId, text: contact.companyName}))
  .uniqBy('value')
  .sortBy('text')
  .value()

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_ALL_CONTACTS_FULFILLED':
      const allContacts = action.payload.results
      const managers = mapManagers(allContacts)
      return {
        ...state,
        allContacts,
        managers
      }
    case 'LOAD_ALL_CONTACTS_WITH_CONTACT_COMPANIES_FULFILLED':
      return {
        ...state,
        allContracts: action.payload.results
      }
    case 'SAVE_CONTACT_FULFILLED':
      return {
        ...state,
        editContact: {
          ...defaultContact
        }
      }
    case 'LOAD_CONTACT_FULFILLED':
      return {
        ...state,
        editContact: action.payload
      }
    case 'CLEAR_EDIT_CONTACT': {
      return {
        ...state,
        editContact: {
          ...defaultContact
        }
      }
    }
    case 'ADD_CONTACT': {
      return {
        ...state,
        selectedContact: {
          ...defaultContact
        }
      }
    }
    case 'DELETE_CONTACT_FULFILLED': {
      const all = state.allContacts.filter((i) => i.contactId !== action.payload.id)
      return {
        ...state,
        allContacts: all
      }
    }
    case 'SELECT_CONTRACTS': {
      return {
        ...state,
        selectedContracts: _.orderBy(action.contracts, ['contractNumber'], ['asc'])
      }
    }
    case 'SELECTED_CONTACT': {
      _.forEach(action.contact.contracts, (contract) => {
        const matchedContract = state.allContracts.find(({contractId}) => contractId === contract.contractId) || {}
        contract.contactCompanyNames = matchedContract.contactCompanyNames
      })
      return {
        ...state,
        selectedContact: action.contact,
        originalCopySelectedContact: action.contact
      }
    }
    case 'SELECTED_CONTACT_FULFILLED': {
      return {
        ...state,
        selectedContact: action.payload
      }
    }
    case 'SELECT_CONTRACT_ROW': {
      return {
        ...state,
        selectedContracts: action.contracts
      }
    }
    case 'QUEUE_UNASSIGN_CONTRACTS': {
      let {bufferedActions: {unassignContracts, assignContracts}, selectedContact, originalCopySelectedContact} = state
      const {contracts: originalContracts} = originalCopySelectedContact
      let {contracts: currentContracts} = selectedContact

      _.forEach(action.payload, (contract) => {
        const {contractId} = contract
        if (_.some(originalContracts, {contractId})) {
          unassignContracts.push(contract)
        } else {
          assignContracts = _.differenceBy(assignContracts, [{contractId}], 'contractId')
        }
        currentContracts = _.differenceBy(currentContracts, [{contractId}], 'contractId')
      })

      return {
        ...state,
        bufferedActions: {
          unassignContracts,
          assignContracts
        },
        selectedContact: _.assign({}, selectedContact, {contracts: currentContracts})
      }
    }
    case 'QUEUE_ASSIGN_CONTRACTS': {
      let {bufferedActions: {unassignContracts, assignContracts}, selectedContact, originalCopySelectedContact} = state
      const {contracts: originalContracts} = originalCopySelectedContact
      let {contracts: currentContracts} = selectedContact

      _.forEach(action.payload, (contract) => {
        const {contractId} = contract
        if (_.some(originalContracts, {contractId})) {
          if (!_.some(currentContracts, {contractId}) && _.some(unassignContracts, {contractId})) { // B' & C
            unassignContracts = _.differenceBy(unassignContracts, [{contractId}], 'contractId')
            currentContracts = currentContracts.concat([_.find(originalContracts, {contractId})])
          }
        } else if (!_.some(currentContracts, {contractId})) {
          assignContracts.push(contract)
          currentContracts = currentContracts.concat([contract])
        }
      })

      return {
        ...state,
        bufferedActions: {
          unassignContracts,
          assignContracts
        },
        selectedContact: _.assign({}, selectedContact, {contracts: currentContracts})
      }
    }
    case 'RESET_ASSIGN_UNASSIGN': {
      const contact = state.allContacts.find((i) => i.contactId === state.selectedContact.contactId)
      return {
        ...state,
        selectedContact: _.assign({}, contact),
        bufferedActions: {
          unassignContracts: [],
          assignContracts: []
        }
      }
    }
    default:
      return state
  }
}
