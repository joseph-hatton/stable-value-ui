const initialState = {
  contracts: true,
  schedule: false,
  underwriting: false,
  wrapPortfolio: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SET_SUBLIST_VALUE':
    return {
      ...state,
      [action.text]: action.value
    }
    case 'GO_TO_TAB':
    return {
      ...state,
      selectedTab: action.tab
    }
    default:
    return state
  }
}
