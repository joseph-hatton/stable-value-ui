const mapContacts = contacts => contacts.map((contact) => ({
  name: `${contact.lastName}${contact.firstName ? `, ${contact.firstName}` : ''}`,
  ...contact
}))

export default mapContacts
