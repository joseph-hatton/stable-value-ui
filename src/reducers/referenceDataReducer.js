import _ from 'lodash'

const mapScalarRefData = (values) => values.map(value => ({id: value, text: value}))

const staticReferenceData = {
  payerTypes: [
    {id: 'ACCOUNT', text: 'Account'},
    {id: 'PLAN', text: 'Plan'}
  ],
  daysOfMonth: mapScalarRefData(_.range(1, 32)),
  tiers: mapScalarRefData(_.range(1, 5)),
  calendarTypes: [
    {id: 'CALENDAR', text: 'Calendar'},
    {id: 'BUSINESS', text: 'Business'}
  ],
  notificationTypes: [
    {id: 'BEFORE', text: 'Before'},
    {id: 'AFTER', text: 'After'}
  ],
  feeDailyRateFormulae: [
    {id: 'DAILY_FEE', text: '(1+Fee Rate)^(1/number days in year)-1'}
  ],
  marketValueEvents: [
    {id: 'STANDARD', text: 'Standard'},
    {id: 'NONSTANDARD', text: 'Non-Standard'}
  ],
  creditingRateRoundingDecimals: mapScalarRefData(_.range(2, 4)),
  yesNoTypes: [
    {id: 'YES', text: 'Yes'},
    {id: 'NO', text: 'No'},
    {id: 'CONTINGENT', text: 'Contingent'}
  ],
  amountComparators: [
    {id: 'eq', text: '='},
    {id: 'lt', text: '<'},
    {id: 'lte', text: '<='},
    {id: 'gt', text: '>'},
    {id: 'gte', text: '>='}
  ],
  depositTransactionDetails: [
    {id: 'INITIAL_DEPOSIT', text: 'Initial Deposit'},
    {id: 'ADDITIONAL_DEPOSIT', text: 'Additional Deposit'}
  ],
  withdrawalTransactionDetails: [
    {id: 'PARTICIPANT_ACTIVITY', text: 'Participant Activity'},
    {id: 'EMPLOYER_ACTIVITY', text: 'Employer Activity'},
    {id: 'MANAGER_ACTIVITY', text: 'Manager Activity'},
    {id: 'MARKET_VALUE_ADJUSTMENT', text: 'MV Adjustment'},
    {id: 'PUT_OPTION', text: 'Put Option'}
  ],
  terminationTransactionDetails: [
    {id: 'TERMINATE_MV_ZERO', text: 'MV = Zero'},
    {id: 'TERMINATE_BV_ZERO', text: 'BV = Zero'},
    {id: 'TERMINATE_BY_OWNER', text: 'By Owner'},
    {id: 'TERMINATE_EXTENDED_TERMINATION', text: 'Extended Termination'},
    {id: 'TERMINATE_BY_COMPANY', text: 'By Company'},
    {id: 'TERMINATE_OTHER', text: 'Other'}
  ]
}

const {depositTransactionDetails, withdrawalTransactionDetails} = staticReferenceData

staticReferenceData.adjustmentTransactionDetails = depositTransactionDetails.concat(withdrawalTransactionDetails)

export default (state = staticReferenceData, action) => {
  switch (action.type) {
    case 'LOAD_REFERENCE_DATA_FULFILLED':
      return {
        ...state,
        ...action.payload
      }
    default:
      return state
  }
}
