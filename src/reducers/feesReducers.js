import moment from 'moment'

const initialState = {
  allFees: [],
  loadedFee: [],
  filtered: false,
  dateFilter: moment(),
  filterOnDay: false,
  contractNumber: 0,
  shortPlanName: ''
}

  export default (state = initialState, action) => {
    switch (action.type) {
      case 'LOAD_ALL_FEES_FULFILLED':
      return {
        ...state,
        filtered: false,
        allFees: action.payload.results
      }
      case 'FILTER_ALL_FEES_FULFILLED':
      return {
        ...state,
        filtered: true,
        allFees: action.payload.results
      }
      case 'SELECT_FEES_BY_CONTRACT_ID_FULFILLED':
      const {contractNumber, shortPlanName} = action.payload[1]
      return {
        ...state,
        filtered: false,
        loadedFee: action.payload[0].results,
        contractNumber,
        shortPlanName
      }
      case 'FILTER_FEES_BY_CONTRACT_ID_FULFILLED':
      return {
        ...state,
        filtered: true,
        loadedFee: action.payload.results
      }
      case 'SET_CONTRACT_ID':
      return {
        ...state,
        currentContractId: action.payload
      }
      case 'SET_DATE_FILTER':
      return {
        ...state,
        dateFilter: action.payload
      }
      case 'SET_FILTER_ON_DAY':
      return {
        ...state,
        filterOnDay: action.payload
      }
      default:
      return state
    }
  }
