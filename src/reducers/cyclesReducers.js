import _ from 'lodash'

const initialState = {
  isRunning: false,
  isStarted: false,
  activeCyclesTab: 'Daily',
  dailyCycleMessages: [],
  monthlyCycleMessages: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'IS_CYCLE_RUNNING': {
      return {
        ...state,
        isRunning: action.isRunning,
        isStarted: (action.isRunning === true) ? false : state.isStarted
      }
    }
    case 'IS_CYCLE_STARTED': {
      return {
        ...state,
        isStarted: action.isStarted
      }
    }
    case 'ACTIVATE_CYCLES_TAB': {
      return {
        ...state,
        activeCyclesTab: action.label
      }
    }
    case 'LOAD_DAILY_CYCLE_MESSAGES_FULFILLED': {
      _.forEach(action.payload.results, (dateTime) => {
         var date = dateTime.messageDate + ' ' + dateTime.messageTime
         var newDateFormat = new Date(date + ' GMT')
         dateTime.message = dateTime.message.replace(date, newDateFormat.toLocaleString())
      })
      return {
        ...state,
        dailyCycleMessages: action.payload.results
      }
    }
    case 'LOAD_MONTHLY_CYCLE_MESSAGES_FULFILLED': {
      _.forEach(action.payload.results, (dateTime) => {
        var date = dateTime.messageDate + ' ' + dateTime.messageTime
        var newDateFormat = new Date(date + ' GMT')
        dateTime.message = dateTime.message.replace(date, newDateFormat.toLocaleString())
     })
      return {
        ...state,
        monthlyCycleMessages: action.payload.results
      }
    }
    default:
      return state
  }
}
