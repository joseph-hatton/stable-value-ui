const initialState = []

const mapStates = ({results}) => results.map(state => ({text: state.long, value: state.short}))

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_STATES_FULFILLED':
      return mapStates(action.payload)
    default:
      return state
  }
}
