import React from 'react'
import _ from 'lodash'
import {PropTypes as T} from 'prop-types'
import MaterialSelectField from '../components/forms/MaterialSelectField'
import {connect} from 'react-redux'
import {loadAllContracts, loadContract} from './ContractActions'

const OWN_PROPS = ['loadAllContracts', 'short', 'shortOptions', 'loadSelectedContract', 'selectedContractId']

const defaultLabel = ({contractNumber, shortPlanName, status}) => `${contractNumber} (${shortPlanName})  (${status})`

class ContractPicker extends React.Component {
  componentDidMount () {
    const {contracts, loadAllContracts, loadContract, loadSelectedContract, selectedContractId} = this.props
    if (_.isEmpty(contracts)) {
      setTimeout(() => loadAllContracts())
    }
    if (loadSelectedContract && selectedContractId) {
      loadContract(selectedContractId)
    }
  }

  loadContract (event, contractId) {
    const {loadContract, loadSelectedContract, onChange} = this.props
    if (loadSelectedContract) {
      loadContract(contractId)
    }

    if (onChange) {
      onChange(event, contractId)
    }
  }

  render () {
    const {contracts, makeLabel = defaultLabel, filterContract} = this.props
    const options = (filterContract ? contracts.filter(filterContract) : contracts).map(contract => ({
      text: makeLabel(contract),
      value: contract.contractId
    }))

    return (<MaterialSelectField {..._.omit(this.props, OWN_PROPS)} options={options} onChange={::this.loadContract}/>)
  }
}

ContractPicker.propTypes = {
  name: T.string.isRequired,
  selectedContractId: T.number,
  contracts: T.array,
  shortOptions: T.array,
  loadAllContracts: T.func.isRequired,
  loadContract: T.func.isRequired,
  onChange: T.func,
  loadSelectedContract: T.bool,
  makeLabel: T.func,
  filterContract: T.func,
  short: T.bool
}

ContractPicker.displayName = 'ContractPicker'

const mapStateToProps = ({contracts: {allContracts: {results}}}) => {
  return {
    contracts: results || []
  }
}

export default connect(mapStateToProps, {loadAllContracts, loadContract})(ContractPicker)
