import React from 'react'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import _ from 'lodash'
import { Card, AutoComplete, RaisedButton } from 'material-ui'
import { Grid, CreateGridConfig } from '../components/grid'
import CaseInSensitiveFilter from '../components/forms/CaseInSensitiveFilter'
import { loadContractContacts, assignContactToContract, selectContact, selectContactContract } from './ContractActions'
import { loadAllContacts } from '../contacts/ContactActions'
import { openSnackbar, openDialog } from '../components/redux-dialog/redux-dialog'
import AssignContact from './contacts/AssignContact'
import AssignContactMenu from './contacts/AssignContactMenu'
import ContractContactColumns from './contacts/AssignContactColumns'
import styles from '../styles/styles'
import AddEditContactComponent from '../contacts/AddEditContactComponent'

const localStyles = {
  container: {
    marginTop: 20,
    padding: 20
  },
  addContactButton: {
    alignItems: 'flex-end',
    display: 'flex',
    justifyContent: 'center',
    fontSize: 10,
    paddingBottom: 10
  }
}

class ContractContacts extends React.Component {
  componentDidMount () {
    const {loadContractContacts, loadAllContacts, contractId, contractContacts, allContacts} = this.props
    if (contractId && (_.isEmpty(contractContacts) || _.first(contractContacts).contractId !== contractId)) {
      loadContractContacts(contractId)
    }
    if (_.isEmpty(allContacts)) {
      loadAllContacts()
    }
  }

  validateAndAssignContact () {
    const {newContact, contractContacts, openSnackbar, openDialog} = this.props
    if (newContact) {
      const existingContact = _.find(contractContacts, {contactId: newContact.contactId})
      if (existingContact) {
        openSnackbar({
          type: 'error',
          message: 'Contact is already assigned to the contract.',
          autoHideDuration: 5000
        })
      } else {
        openDialog('assignContactToContract')
      }
    }
  }

  render () {
    const {contractContacts, allContacts, selectContact, selectContactContract, openDialog} = this.props
    const options = CreateGridConfig(contractContacts, {showHeaderToolbar: false, paginate: false})

    return (
      <Card style={{...localStyles.container, paddingTop: 5}}>
        <div className="row">
          <div className="col-lg-6" style={{paddingLeft: 25}}>
            <AutoComplete
              dataSource={allContacts}
              floatingLabelText="Search Contact"
              fullWidth={true}
              filter={CaseInSensitiveFilter}
              onNewRequest={selectContact}
            />
          </div>
          <div className="col-lg-2" style={localStyles.addContactButton}>
            <RaisedButton id="ContractContacts-AssignContactButton" label="Assign Contact"
                          {...styles.secondaryButtonStyle}
                          onClick={::this.validateAndAssignContact}/>
          </div>
        </div>
        <br/>
        <Grid id="ContractContacts" options={options} data={contractContacts} columns={ContractContactColumns}
          onRowDoubleClick={(contract) => { selectContactContract(contract); openDialog('assignContactToContract') }} />
        <AssignContact/>
        <AssignContactMenu/>
        <AddEditContactComponent />
      </Card>
    )
  }
}

ContractContacts.propTypes = {
  contractId: T.number,
  contractContacts: T.array,
  loadContractContacts: T.func.isRequired,
  loadAllContacts: T.func.isRequired,
  contractNumber: T.string,
  allContacts: T.array,
  openSnackbar: T.func.isRequired,
  openDialog: T.func.isRequired,
  selectContact: T.func.isRequired,
  selectContactContract: T.func.isRequired,
  newContact: T.object
}

const mapContactForSearch = (allContacts) => {
  return allContacts.map((contact) => {
    const {name, companyName, contactTypeName, contactId} = contact
    return {
      text: `${name} - ${contactTypeName} ( ${companyName} )`,
      value: contactId,
      contact
    }
  })
}

const mapStateToProps = (state) => {
  const {
    referenceData: {contactTypes},
    contracts: {
      contractContacts,
      contract,
      newContact
    },
    contacts: {allContacts}
  } = state
  const contactTypeMap = _.keyBy(contactTypes, 'id')
  return {
    contractId: contract && contract.contractId,
    contractContacts: (contractContacts || []).map(contractContact => ({
      ...contractContact,
      contactType: contactTypeMap[contractContact.contactType].text
    })),
    allContacts: mapContactForSearch(allContacts),
    newContact
  }
}

const actions = {
  loadContractContacts,
  loadAllContacts,
  openSnackbar,
  openDialog,
  selectContact,
  selectContactContract
}

export default connect(mapStateToProps, actions)(ContractContacts)
