import {destroy, change, initialize} from 'redux-form'
import _ from 'lodash'
import moment from 'moment'
import {apiPath} from '../config'
import http from '../actions/http'
import CONTRACT_FORMS, {SPECIFICATIONS, BASE_INFO, getEffectiveDateProp} from './ContractForms'
import {createNewContractSavePayload, createContractSavePayload} from './ContractPayloadMapper'
import {closeDialog, openSnackbar, openConfirmationAlert} from '../components/redux-dialog/redux-dialog'

const CREATE_CONTRACT_URL = `${apiPath}/contracts`

export const loadAllContracts = () => (dispatch, getState) => {
  dispatch({
    type: 'LOAD_ALL_CONTRACTS',
    payload: http.get(`${apiPath}/contracts`).then(response => {
      const {referenceData: {contractStatuses}} = getState()
      const statusById = _.keyBy(contractStatuses, 'id')
      return {
        results: response.results.map(contract => ({...contract, statusText: statusById[contract.status].text}))
      }
    })
  })
}

export const initializeContract = () => ({
  type: 'INITIALIZE_CONTRACT'
})

export const setCurrentContractForm = (contractFormId) => ({
  type: 'SET_CURRENT_CONTRACT_FORM',
  contractFormId
})

const CONTRACT_SAVED_MESSAGE = {
  successMessage: 'Contract Saved Successfully'
}

export const saveContract = () => (dispatch, getState) => {
  const {contracts: {contract}, form, referenceData: {derivativeTypes}} = getState()
  if (!contract.contractNumber) {
    const payload = createNewContractSavePayload(form)
    dispatch({
      type: 'SAVE_CONTRACT',
      payload: http.post(CREATE_CONTRACT_URL, payload, CONTRACT_SAVED_MESSAGE)
    })
  } else if (contract.status === 'PENDING' || contract.status === 'ACTIVE') {
    const payload = createContractSavePayload(contract, form, derivativeTypes)
    dispatch({
      type: 'SAVE_CONTRACT',
      payload: http.put(`${CREATE_CONTRACT_URL}/${contract.contractId}`, payload, CONTRACT_SAVED_MESSAGE)
    })
  } else {
    dispatch(openSnackbar({
      type: 'error',
      message: `Cannot modify contract in ${contract.status} Status.`,
      autoHideDuration: 5000
    }))
  }
}

export const updateFormStateWithNewEditHistory = (formId, field, dispatch, getState) => {
  const {contracts: {contract}} = getState()
  dispatch(change(formId, field, contract[field] || ' '))
  dispatch(change(formId, field, contract[field] || ''))
  dispatch(change(formId, getEffectiveDateProp(field), null))
}

export const removeEditHistory = (formId, field) => (dispatch, getState) => {
  const {contracts: {contract}} = getState()
  dispatch({
    type: 'SAVE_CONTRACT',
    payload: http
      .put(`${CREATE_CONTRACT_URL}/${contract.contractId}`, contract, CONTRACT_SAVED_MESSAGE)
      .then((result) => {
        setTimeout(() => {
          updateFormStateWithNewEditHistory(formId, field, dispatch, getState)
        })
        return result
      })
  })
}

export const loadContract = (contractId) => (dispatch) => {
  dispatch({
    type: 'LOAD_CONTRACT',
    payload: Promise.all([
      http.get(`${apiPath}/contracts/${contractId}`),
      http.get(`${apiPath}/contracts/${contractId}/contacts`)
    ])
  })
}

export const clearEditContractState = () => (disptach) => {
  CONTRACT_FORMS.forEach(contractFormId => {
    disptach(destroy(contractFormId))
  })
  disptach({
    type: 'CLEAR_EDIT_CONTRACT'
  })
}

export const jurisdictionSelected = (event, value) => (dispatch, getState) => {
  const {form: {specifications}} = getState()
  if (!specifications.values.assignmentState) {
    dispatch(change(SPECIFICATIONS, 'assignmentState', value))
  }
}

export const planAnniversaryMonthChanged = (event, value) => (dispatch, getState) => {
  const {form: {baseInfo}} = getState()

  dispatch(change(BASE_INFO, 'planAnniversary', `${value} ${baseInfo.values.planAnniversaryDay}`))
}

export const planAnniversaryDayChanged = (event, value) => (dispatch, getState) => {
  const {form: {baseInfo}} = getState()

  dispatch(change(BASE_INFO, 'planAnniversary', `${baseInfo.values.planAnniversaryMonth} ${value}`))
}

export const loadModelInputs = (contractId) => (dispatch) => {
  dispatch({
    type: 'LOAD_MODEL_INPUTS',
    payload: http.get(`${apiPath}/contracts/${contractId}/underwriting`)
  })
}

const makePostUrl = (contractId, contractResource) => `${apiPath}/contracts/${contractId}/${contractResource}`

const makePutUrl = (contractId, contractResource, id) => `${makePostUrl(contractId, contractResource)}/${id}`

const CONTRACT_MODEL_INPUT_SAVED_MESSAGE = {
  successMessage: 'Model Input Saved Successfully'
}

export const saveModelInput = () => (dispatch, getState) => {
  const {form: {AddEditModelInputForm}, contracts: {contract: {contractId}}} = getState()
  const underwritingId = AddEditModelInputForm.values.underwritingId

  const method = underwritingId ? 'put' : 'post'
  const url = method === 'put' ? makePutUrl(contractId, 'underwriting', underwritingId) : makePostUrl(contractId, 'underwriting')
  const payload = {
    ...AddEditModelInputForm.values,
    contractId
  }
  dispatch({
    type: 'SAVE_MODEL_INPUTS',
    payload: http[method](url, payload, CONTRACT_MODEL_INPUT_SAVED_MESSAGE)
  })
}

export const selectModelInputRow = (modelInput) => ({
  type: 'SELECT_MODEL_INPUT',
  payload: modelInput
})

const CONTRACT_MODEL_INPUT_DELETED_MESSAGE = {
  successMessage: 'Model Input Deleted Successfully'
}

export const deleteModelInputRow = ({underwritingId, contractId}) => (dispatch) => {
  dispatch({
    type: 'DELETE_MODEL_INPUT',
    payload: http.delete(makePutUrl(contractId, 'underwriting', underwritingId), null, CONTRACT_MODEL_INPUT_DELETED_MESSAGE)
  })
}

export const clearModelInputEditForm = () => (dispatch) => {
  dispatch(destroy('AddEditModelInputForm'))
  dispatch({
    type: 'SELECT_MODEL_INPUT',
    payload: undefined
  })
}

// RiskScoreCard

const CONTRACT_RISK_SCORECARD_INPUT_SAVED_MESSAGE = {
  successMessage: 'Risk Scorecard Input Saved Successfully'
}
const CONTRACT_RISK_SCORECARD_INPUT_UPDATED_MESSAGE = {
  successMessage: 'Risk Scorecard Input Updated Successfully'
}
const CONTRACT_RISK_SCORECARD_INPUT_DELETED_MESSAGE = {
  successMessage: 'Risk Scorecard Input Deleted Successfully'
}

export const prepRiskScorecard = (rs, contractType) => {
  if (contractType === 'POOLED_PLAN') {
    rs.values.planSponsor = {score: 0}
    rs.values.planSvFundBalances = {score: 0}
    rs.values.planDesign = {score: 0}
  } else {
    rs.values.pooledFundBalances = {score: 0}
    rs.values.pooledFundDesign = {score: 0}
  }
  // rs.values.forEach({adjust})
}

export const saveRiskScorecardInput = ({riskScorecardId}) => (dispatch, getState) => {
  const {form: {RiskScorecardAddEdit}, contracts: {contract: {contractId, contractType}, riskScorecard: {riskScorecardId}}} = getState()
  prepRiskScorecard(RiskScorecardAddEdit, contractType)

  const method = riskScorecardId ? 'put' : 'post'
  const url = method === 'put' ? makePutUrl(contractId, 'risk-scorecards', riskScorecardId) : makePostUrl(contractId, 'risk-scorecards')

  const payload = {
    ...RiskScorecardAddEdit.values,
    contractId
  }

  let message = riskScorecardId ? CONTRACT_RISK_SCORECARD_INPUT_UPDATED_MESSAGE : CONTRACT_RISK_SCORECARD_INPUT_SAVED_MESSAGE

  dispatch({
    type: 'SAVE_RISK_SCORECARD_INPUTS',
    payload: http[method](url, payload, message, 'riskScorecardInput')
      .then((payload) => { clearRiskScorecardInputEditForm()(dispatch); return payload })
  })
}

export const selectRiskScorecardInputRow = (riskScorecard) => ({
  type: 'SELECT_RISK_SCORECARD_INPUT',
  payload: riskScorecard
})

export const clearRiskScorecardInputEditForm = () => (dispatch) => {
  dispatch(destroy('RiskScorecardAddEdit'))
}
export const gradeRiskScorecard = (e) => (dispatch, getState) => {
  let name = e.target.name.split('.')[0]
  let value = e.target.value

  const {contracts: {contract: {contractId, contractType}}, form: {RiskScorecardAddEdit}} = getState()

  const {riskScorecardId} = RiskScorecardAddEdit.values

  prepRiskScorecard(RiskScorecardAddEdit, contractType)

  RiskScorecardAddEdit.values[name].score = value * 1

  return http.put(makePostUrl(contractId, 'risk-scorecards/calculate-adjusted'), {
    ...RiskScorecardAddEdit.values,
    contractType
  })
  .then((data) => {
    console.log('initing...')
    dispatch(initialize('RiskScorecardAddEdit', {...data, riskScorecardId}))
    dispatch({
      type: 'SELECT_RISK_SCORECARD_INPUT',
      payload: {...data, riskScorecardId}
    })
  })
}

export const deleteRiskScorecard = ({contractId, riskScorecardId}) => (dispatch) => {
  return http.delete(makePutUrl(contractId, 'risk-scorecards', riskScorecardId), null, CONTRACT_RISK_SCORECARD_INPUT_DELETED_MESSAGE)
    .then((data) => {
      dispatch({
        type: 'DELETE_RISK_SCORECARD',
        payload: data
      })
      dispatch({
        type: 'SELECT_RISK_SCORECARD_INPUT',
        payload: undefined
      })
    })
}

// RiskScoreCard

const makeStableValueFundPostUrl = (contractId) => `${apiPath}/contracts/${contractId}/stable-value-funds`

const makeStableValueFundPutUrl = (contractId, stableValueFundId) => `${makeStableValueFundPostUrl(contractId)}/${stableValueFundId}`

const CONTRACT_STABLE_VALUE_SAVED_MESSAGE = {
  successMessage: 'Contract Stable Value Successfully'
}

export const saveStableValueFund = () => (dispatch, getState) => {
  const {form: {AddEditStableValueFundForm}, contracts: {contract: {contractId}}} = getState()
  const stableValueFundId = AddEditStableValueFundForm.values.stableValueFundId

  const method = stableValueFundId ? 'put' : 'post'
  const url = method === 'put' ? makeStableValueFundPutUrl(contractId, stableValueFundId) : makeStableValueFundPostUrl(contractId)
  const payload = {
    ...AddEditStableValueFundForm.values,
    contractId
  }
  dispatch({
    type: 'SAVE_STABLE_VALUE_FUND',
    payload: http[method](url, payload, CONTRACT_STABLE_VALUE_SAVED_MESSAGE, 'stableValueFund')
  })
}

export const loadStableValueFunds = (contractId) => (dispatch) => {
  dispatch({
    type: 'LOAD_STABLE_VALUE_FUNDS',
    payload: http.get(`${apiPath}/contracts/${contractId}/stable-value-funds`)
  })
}

export const selectStableValueFund = (stableValueFundId) => (dispatch, getState) => {
  const {contracts: {stableValueFunds}} = getState()
  dispatch({
    type: 'SELECT_STABLE_VALUE_FUND',
    payload: _.find(stableValueFunds, (i) => i.stableValueFundId === stableValueFundId)
  })
}

export const selectStableValueFundRow = (stableValueFund) => ({
  type: 'SELECT_STABLE_VALUE_FUND',
  payload: stableValueFund
})

const STABLE_VALUE_FUND_DELETED_MESSAGE = {
  successMessage: 'Stable Value Fund Deleted Successfully'
}

export const deleteStableValueFundRow = ({contractId, stableValueFundId}) => (dispatch) => {
  dispatch({
    type: 'DELETE_STABLE_VALUE_FUND',
    payload: http.delete(makeStableValueFundPutUrl(contractId, stableValueFundId), null, STABLE_VALUE_FUND_DELETED_MESSAGE)
  })
}

export const clearStableValueFundEditForm = () => (dispatch) => {
  dispatch(destroy('AddEditStableValueFundForm'))
  dispatch({
    type: 'SELECT_STABLE_VALUE_FUND',
    payload: undefined
  })
}

export const activateUnderwritingTab = (label) => ({
  type: 'ACTIVATE_UNDERWRITING_TAB',
  label
})

export const loadContractContacts = (contractId) => (dispatch) => {
  dispatch({
    type: 'LOAD_CONTRACT_CONTACTS',
    payload: http.get(`${apiPath}/contracts/${contractId}/contacts`)
  })
}

export const selectContact = ({contact}) => ({
  type: 'SELECT_CONTACT',
  payload: contact
})

export const selectContactContract = (contractContact) => (dispatch, getState) => {
  const {contacts: {allContacts}} = getState()
  dispatch({
    type: 'SELECT_CONTRACT_CONTACT',
    payload: {
      contractContact,
      contact: contractContact ? allContacts.find(contact => contact.contactId === contractContact.contactId) : null
    }
  })
}

const CONTRACT_CONTACT_ASSIGNED_MESSAGE = {
  successMessage: 'Contact Assigned Successfully'
}

export const assignContactToContract = () => (dispatch, getState) => {
  const {form: {AssignContact}} = getState()
  const {contactContractId, contractId} = AssignContact.values

  const method = contactContractId ? 'put' : 'post'
  const url = (method === 'put') ? makePutUrl(contractId, 'contacts', contactContractId) : makePostUrl(contractId, 'contacts')
  return dispatch({
    type: 'ASSIGN_CONTRACT_CONTACT',
    payload: http[method](url, AssignContact.values, CONTRACT_CONTACT_ASSIGNED_MESSAGE)
  }).then(response => {
    dispatch(loadContractContacts(contractId))
    dispatch(closeDialog('assignContactToContract'))
    return response
  })
}

const CONTRACT_CONTACT_UNASSIGNED_MESSAGE = {
  successMessage: 'Contact Unassigned Successfully'
}

export const unAssignContactFromContract = (contractId, contactContractId) => (dispatch) => {
  const url = makePutUrl(contractId, 'contacts', contactContractId)
  return dispatch({
    type: 'UNASSIGN_CONTRACT_CONTACT',
    payload: http.delete(url, {}, CONTRACT_CONTACT_UNASSIGNED_MESSAGE)
  }).then(response => {
    dispatch(loadContractContacts(contractId))
    return response
  })
}

export const standardErrorMessageRetriever = (error) => {
  return _.get(error, 'response.body.message')
}

const CONTRACT_STATUS_UPDATE_MESSAGE = {
  successMessage: 'Contract Status updated successfully',
  errorMessage: standardErrorMessageRetriever
}

export const updateContractStatus = (path) => (dispatch, getState) => {
  const {contracts: {contract: {contractId}}} = getState()
  const url = `${apiPath}/contracts/${contractId}/${path}`
  dispatch({
    type: 'UPDATE_CONTRACT_STATUS',
    payload: http.put(url, {}, CONTRACT_STATUS_UPDATE_MESSAGE)
  })
}

export const loadDerivativeTypes = () => (dispatch) => {
  dispatch({
    type: 'LOAD_DERIVATIVE_TYPES',
    payload: http.get(`${apiPath}/derivative-types`)
  })
}

export const hasEditHistory = (contract) => contract && contract.status === 'ACTIVE'

export const toEditHistory = (field, contract, managers) => {
  const {editHistories} = contract
  const managersById = _.keyBy(managers, 'value')

  const lastDayOfLastMonth = moment().subtract(1, 'month').endOf('month')

  const history = _(editHistories)
    .filter(edit => edit[field] != null)
    .map(edit => {
      const value = edit[field]
      return {
        value: field === 'managerId' && managersById[value] ? managersById[value].text : value,
        effectiveDate: edit.effectiveDate,
        future: moment(edit.effectiveDate).utc().isAfter(lastDayOfLastMonth)
      }
    })
    .value()

  const activeValue = _(history)
    .sortBy(({effectiveDate}) => moment(effectiveDate).valueOf() * -1)
    .filter(edit => !edit.future)
    .first()

  if (activeValue) {
    activeValue.active = true
  }

  return history
}

export const selectAuditableField = (formId, field, label) => (dispatch, getState) => {
  const {contracts: {contract}, contacts: {managers}} = getState()
  return dispatch({
    type: 'SELECT_AUDITABLE_CONTRACT_FIELD',
    selectedAuditableField: {
      formId,
      field,
      label,
      editHistory: toEditHistory(field, contract, managers)
    }
  })
}

export const clearAuditableFieldChange = (formId, field) => (dispatch, getState) => {
  const {contracts: {contract}} = getState()
  const originalValue = contract[field]
  dispatch(change(formId, field, originalValue || originalValue === 0 ? originalValue : ''))
  dispatch(change(formId, `${field}EffectiveDate`, null))
}

export const deleteContractEditHistory = (effectiveDate, field) => ({
  type: 'DELETE_CONTRACT_HISTORY',
  effectiveDate,
  field
})

const numberOfDifferencesInEditHistory = (curr, oldEditHistories, outerIndex) =>
  _.reduce(curr, (accum, eh, innerIndex) => accum + (eh !== _.get(oldEditHistories, `[${outerIndex}][${innerIndex}]`) ? 1 : 0), 0)

export const areEditHistoriesEqual = (oldEditHistories = [], newEditHistories = []) => {
  return newEditHistories.reduce((prev, curr, outerIndex) => numberOfDifferencesInEditHistory(curr, oldEditHistories, outerIndex), 0) === 0
}

export const isClean = ({contract, forms, derivativeTypes}) => {
  let mappedFormData = createContractSavePayload(contract, forms, derivativeTypes)

  // Cleaing up for comparison
  mappedFormData = _.mapValues(mappedFormData, (item, key) => item === '' ? null : item)
  mappedFormData.derivativeTypes = _.sortBy(_.get(mappedFormData, 'derivativeTypes', []), 'derivativeTypeId')
  contract.derivativeTypes = _.sortBy(_.get(contract, 'derivativeTypes', []), 'derivativeTypeId')

  // This block was added to support PENDING contracts, prevents isEqual from comparing Edit Histories
  // That way we can check it ourselves
  const oldContract = _.cloneDeep(contract)
  const newContract = _.cloneDeep(mappedFormData)
  const oldEditHistories = _.cloneDeep(contract.editHistories)
  const newEditHistories = _.cloneDeep(mappedFormData.editHistories)
  oldContract.editHistories = undefined
  newContract.editHistories = undefined

  return areEditHistoriesEqual(oldEditHistories, newEditHistories)
    && _.isEqual(oldContract, newContract)
}

export const canNavigateAwayFromContracts = () => (dispatch, getState) => {
  const {contracts: {contract}, form: forms, referenceData: {derivativeTypes}} = getState()
  return !contract || isClean({contract, forms, derivativeTypes})
}

export const askToNotSaveContract = (router) => (dispatch, getState) => (nextLocation) => {
  const {contracts: {canLeaveWithoutSaving}} = getState()
  if (canLeaveWithoutSaving || canNavigateAwayFromContracts()(dispatch, getState)) {
    dispatch(resetLeaveWithoutSaving())
    return true
  }
  dispatch(openConfirmationAlert({
    onOk: () => { dispatch(leaveWithoutSaving()); router.push(nextLocation) },
    message: 'Your changes are not saved. Leaving this contract will erase your changes.'
  }, 'navigateAwayFromContract'))
  return false
}

export const leaveWithoutSaving = () => {
  return {
    type: 'LEAVE_WITHOUT_SAVING'
  }
}

export const resetLeaveWithoutSaving = () => {
  return {
    type: 'RESET_LEAVE_WITHOUT_SAVING'
  }
}
