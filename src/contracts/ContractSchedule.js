import React from 'react'
import _ from 'lodash'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { PropTypes as T } from 'prop-types'
import { Tabs, Tab } from 'material-ui/Tabs'
import { Card } from 'material-ui/Card'
import { SCHEDULE_FUNDING, SCHEDULE_FUNDING_PROPS } from './ContractForms'
import createContractForm from './CreateContractForm'
import { goToTab, setOrToggleListItem } from '../components/common/LeftDrawerActions'

import ContractScheduleTabs from './schedule'

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400
  },
  tabContainer: {
    marginTop: 20
  },
  tabContentContainerStyle: {
    padding: 20
  },
  mainTab: {
    backgroundColor: '#0062cc',
    fontWeight: 600
  }
}

class ContractSchedule extends React.Component {
  componentDidMount () {
    if (!this.props.schedule) {
      this.props.setOrToggleListItem('schedule', true)
    }
    const selectedTab = _.findIndex(ContractScheduleTabs, ['label', this.props.selectedTab]) === -1 ? 'Funding' : this.props.selectedTab
    this.props.goToTab(selectedTab)
  }
  componentWillUnmount () {
    if (this.props.schedule) {
      this.props.setOrToggleListItem('schedule', false)
    }
  }

  render () {
    return (
      <ContractScheduleForm>
        <Card style={styles.tabContainer}>
          <Tabs value={this.props.selectedTab} contentContainerStyle={styles.tabContentContainerStyle}>
            {
              ContractScheduleTabs.map(({label, url, ContractTab}) => (
                <Tab key={label}
                     onActive={() => browserHistory.push(`/contracts/${this.props.contractId}/schedule/${url}`)}
                     label={label} value={label} style={styles.mainTab}>
                  <ContractTab {...(_.omit(this.props, ['selectedTab', 'schedule', 'contractId']))}/>
                </Tab>
              ))
            }
          </Tabs>
        </Card>
      </ContractScheduleForm>
    )
  }
}

ContractSchedule.propTypes = {
  selectedTab: T.string,
  schedule: T.boolean,
  setOrToggleListItem: T.func,
  goToTab: T.func,
  contractId: T.number
}

export const mapDerivateTypes = (contract, derivativeTypes) => {
  const contractDerivativeTypesById = _.keyBy(contract.derivativeTypes, 'derivativeTypeId')
  const result = _(derivativeTypes)
    .map(({derivativeTypeId}) => ({[derivativeTypeId]: !!contractDerivativeTypesById[derivativeTypeId]}))
    .reduce(_.assign, {})
  return result
}

export const mapStateToForm = ({contracts: {contract}, referenceData: {derivativeTypes}}) => ({
  initialValues: contract && !_.isEmpty(derivativeTypes) ? {
    ...(_.pick(contract, SCHEDULE_FUNDING_PROPS)),
    ...(mapDerivateTypes(contract, derivativeTypes))
  } : {},
  contractId: contract && contract.contractId
})

const ContractScheduleForm = createContractForm(SCHEDULE_FUNDING, mapStateToForm)

const mapStateToProps = ({sideBar: {selectedTab, schedule}, contracts: {contract: {contractId}}}) => ({
  selectedTab,
  schedule,
  contractId
})

export default connect(mapStateToProps, {goToTab, setOrToggleListItem})(ContractSchedule)
