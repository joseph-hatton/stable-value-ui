import React from 'react'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import RaisedButton from 'material-ui/RaisedButton'
import {Field, reduxForm} from 'redux-form'
import {TextField} from 'redux-form-material-ui'
import {red500} from 'material-ui/styles/colors'
import ReduxDialog, {ConfirmationAlert} from '../../components/redux-dialog'
import {dateFormat} from '../../components/utilities/Formatters'
import Styles from '../../styles/styles'
import ContractPicker from '../ContractPicker'
import {
  updateCreditingRate,
  clearAdjustmentIfInvalid,
  clearAdjustmentCreditingForm,
  adjustCreditingRate,
  adjustCreditingRateWithGrossRate,
  clearGrossRateIfInvalid,
  approveCreditingRate,
  clearCreditingRate
} from './CreditingRatesActions'
import {FormatCurrency} from '../../components/forms/Validators'
import {canEditCreditingRate, canApproveCreditingRate, canClearCreditingRate} from './CreditingRatesRules'
import RateField from '../RateField'

const formId = 'CreditingRateForm'

const defaultLabelStyle = {textAlign: 'right'}
const valueStyle = {textAlign: 'left', color: 'black'}
const hiddenContractPicker = {display: 'none'}

const CreditingRateProp = ({label, labelStyle = {}, children}) => (
  <div className="row">
    <div className="col-lg-offset-1 col-lg-3" style={{...defaultLabelStyle, ...labelStyle}}>
      {label}
    </div>
    <div className="col-lg-7" style={valueStyle}>
      {children}
    </div>
  </div>
)

CreditingRateProp.propTypes = {
  label: T.string,
  children: T.any.isRequired,
  labelStyle: T.object
}

const Rate = ({value, visible, round}) => <span> {visible && (
      <span>{Number(value / 100).toFixed(round)} ( {value || '0'} Bps )</span>)}
    </span>

Rate.propTypes = {
  value: T.number,
  visible: T.bool,
  round: T.number
}

export const toNumberAcceptsNegative = (creditingRateRoundDecimals) => (text, previousValue) => {
  console.log('entering', {text})
  if (['-', '-.', '.', ''].indexOf(text) !== -1) {
    console.log('match', {text})
    return text
  } else if (!isNaN(text)) {
    console.log('a number', {text})
    return +(Math.round(Number(text) + `e+${creditingRateRoundDecimals}`) + `e-${creditingRateRoundDecimals}`)
  }
  return previousValue
}

const getContractWithStatus = (creditingRate, contract) => contract || {status: creditingRate.contractStatus}

class ViewEditCreditingRates extends React.Component {
  constructor () {
    super()
    this.state = {
      adjustmentFocus: false
    }
  }

  approve () {
    const {selectedCreditingRate: {contractId, creditingRateId}, approveCreditingRate} = this.props
    approveCreditingRate(contractId, creditingRateId)
  }

  render () {
    const {
      handleSubmit,
      adjustCreditingRate,
      updateCreditingRate,
      clearAdjustmentIfInvalid,
      clearAdjustmentCreditingForm,
      canEditGrossRate,
      selectedCreditingRate,
      creditingRateCalcMethodText,
      contract,
      netCreditingRate,
      isNetCreditingRateLessThanMinimum,
      marketValueView,
      adjustCreditingRateWithGrossRate,
      clearGrossRateIfInvalid
    } = this.props
    const {
      referenceDate,
      effectiveDate,
      premiumRate,
      managerFeeRate,
      advisorFeeRate,
        grossRate,
      bookValue,
      calculatedBy,
      contractId,
      creditingRateAdvisorFee,
      creditingRateManagerFee,
      netOfThirdPartyFees,
      creditingRateType
    } = selectedCreditingRate

    const {
      contractNumber,
      shortPlanName,
      creditingRateRoundDecimals,
      minimumNetCreditingRate
    } = contract || {}

    const contractWithStatus = getContractWithStatus(selectedCreditingRate, contract)

    const formatIfNotBlur = (value) => {
      const {adjustmentFocus} = this.state
      if (!adjustmentFocus && !isNaN(value)) {
        return Number(value).toFixed(creditingRateRoundDecimals)
      }
      return value
    }

    const actions = [
      <RaisedButton
        key="Save"
        label="Save"
        primary={true}
        style={{marginRight: 20}}
        type="submit"
        form={formId}
        disabled={!canEditCreditingRate({
          creditingRate: selectedCreditingRate,
          contract: contractWithStatus
        }) || isNetCreditingRateLessThanMinimum}
      />
    ]

    if (canApproveCreditingRate({creditingRate: selectedCreditingRate, contract: contractWithStatus})) {
      actions.push(<RaisedButton
        key="Approve"
        label="Approve"
        primary={true}
        style={{marginRight: 20}}
        type="button"
        onClick={::this.approve}
        />)
      }

      if (canClearCreditingRate({creditingRate: selectedCreditingRate, contract})) {
        actions.push(<RaisedButton
          key="ClearCreditingRate"
          style={{marginRight: 20}}
          label="Clear Crediting Rate"
          onClick={() => {
            const {selectedCreditingRate: {contractId, creditingRateId}, clearCreditingRate} = this.props
            clearCreditingRate(contractId, creditingRateId, 'ViewEditCreditingRatesDialog')
          }}
        />)
      }

    const reviewTitle = 'Review Crediting Rates'
    const title = (marketValueView && contract) ? `${reviewTitle} - ${contractNumber} (${shortPlanName})` : reviewTitle

    return (
      <ReduxDialog
        dialogName="ViewEditCreditingRatesDialog"
        title={title}
        actions={actions}
        modal={true}
        style={{zIndex: 1450}}
        autoScrollBodyContent={true}
        cancelButton onCancel={clearAdjustmentCreditingForm}>
        <form id={formId} onSubmit={handleSubmit(updateCreditingRate)}>
          <div className="creditingRatesView" style={{marginTop: 20}}>
            {
              marketValueView && <div style={hiddenContractPicker}>
                <ContractPicker name="contractId" type="text" floatingLabelText="Contract"
                                disabled
                                loadSelectedContract
                                selectedContractId={contractId}/>
              </div>
            }
            <CreditingRateProp label="Reference Date:">
              {dateFormat(referenceDate)}
            </CreditingRateProp>
            <CreditingRateProp label="Effective Date:">
              {dateFormat(effectiveDate)}
            </CreditingRateProp>
            <div style={{marginBottom: 20}}>
                {!canEditGrossRate &&
                    <CreditingRateProp label="Gross Rate:">
                      <div>
                          {grossRate}%
                      </div>
                    </CreditingRateProp>
                } {canEditGrossRate &&
              <CreditingRateProp label="Gross Rate:" labelStyle={Styles.toggleDivStyle}>
                <Field fullWidth name="grossRate" onChange={adjustCreditingRateWithGrossRate}
                       onBlur={clearGrossRateIfInvalid} round={creditingRateRoundDecimals}
                       format={(value) => value + '%'}
                       parse={(value) => value.replace('%', '')}
                       normalize={toNumberAcceptsNegative(creditingRateRoundDecimals)} component={TextField} disabled={!canEditGrossRate}/>
              </CreditingRateProp>
                }
                    </div>
            <CreditingRateProp label="Premium Rate:">
              <Rate value={premiumRate} visible={creditingRateType === 'NET'} round={creditingRateRoundDecimals}/>
            </CreditingRateProp>
            <CreditingRateProp label="Manager Fee:">
              <Rate value={managerFeeRate} visible={netOfThirdPartyFees && creditingRateManagerFee} round={creditingRateRoundDecimals}/>
            </CreditingRateProp>
            <CreditingRateProp label="Advisor Fee:">
              <Rate value={advisorFeeRate} visible={netOfThirdPartyFees && creditingRateAdvisorFee} round={creditingRateRoundDecimals}/>
            </CreditingRateProp>
            <div style={{marginBottom: 20}}>
              <CreditingRateProp label="Adjustment:" labelStyle={Styles.toggleDivStyle}>
                <Field fullWidth name="adjustment" component={TextField}
                  onChange={adjustCreditingRate}
                  onBlur={(e, v) => { this.setState({adjustmentFocus: false}); clearAdjustmentIfInvalid(e, v) }}
                  onFocus={() => this.setState({adjustmentFocus: true})}
                  format={formatIfNotBlur}
                />
              </CreditingRateProp>
            </div>
            <CreditingRateProp label="Net Crediting Rate:">
              <div>
                {netCreditingRate}%
                {
                  isNetCreditingRateLessThanMinimum &&
                  (
                    <div>
                      <span style={{color: red500}}>Net Crediting Rate cannot be less than the minimum ({minimumNetCreditingRate}).</span>
                    </div>
                  )
                }
              </div>
            </CreditingRateProp>
            <CreditingRateProp label="Book Value:">
              {bookValue ? '$' + FormatCurrency(bookValue) : ''}
            </CreditingRateProp>
            <CreditingRateProp label="Rounding Decimals:">
              {creditingRateRoundDecimals}
            </CreditingRateProp>
            <CreditingRateProp label="Calculation Method:">
              {creditingRateCalcMethodText || ''}
            </CreditingRateProp>
            <CreditingRateProp label="Calculated By:">
              {calculatedBy}
            </CreditingRateProp>
            <div style={{marginBottom: 20}}>
              <CreditingRateProp label="Comments:" labelStyle={Styles.toggleDivStyle}>
                <Field fullWidth name="creditingRateRateComments" component={TextField}
                       style={{fontSize: 14}}
                       multiLine rowsMax={3}/>
              </CreditingRateProp>
            </div>
          </div>
        </form>
        <ConfirmationAlert dialogName="ConfirmCreditingRateApprove"/>
      </ReduxDialog>
    )
  }
}

ViewEditCreditingRates.propTypes = {
  handleSubmit: T.func,
  initialValues: T.object,
  selectedCreditingRate: T.object,
  creditingRateCalcMethodText: T.string,
  contract: T.object,
  updateCreditingRate: T.func,
  clearAdjustmentIfInvalid: T.func,
  clearAdjustmentCreditingForm: T.func,
  adjustCreditingRate: T.func,
  adjustCreditingRateWithGrossRate: T.func,
  clearGrossRateIfInvalid: T.func,
  approveCreditingRate: T.func.isRequired,
  canEditGrossRate: T.bool,
  netCreditingRate: T.number,
  isNetCreditingRateLessThanMinimum: T.bool,
  marketValueView: T.bool,
  netOfThirdPartyFees: T.bool,
  creditingRateType: T.string,
  grossRate: T.number,
  blur: T.event,
  clearCreditingRate: T.func
}

const ViewEditCreditingRatesForm = reduxForm({
  form: formId
})(ViewEditCreditingRates)

const actions = {
  updateCreditingRate,
  clearAdjustmentIfInvalid,
  clearAdjustmentCreditingForm,
  adjustCreditingRate,
  adjustCreditingRateWithGrossRate,
  clearGrossRateIfInvalid,
  approveCreditingRate,
  clearCreditingRate
}

const mapStateToProps = (state) => {
  const {
    contracts: {
      marketValueView,
      contract,
      selectedCreditingRate,
      editCreditingRate = {}
    }
  } = state

  const {
    netCreditingRate,
    canEditGrossRate,
    creditingRateCalcMethodText
  } = editCreditingRate

  const {adjustment, grossRate, creditingRateRateComments, contractId} = selectedCreditingRate

  const {minimumNetCreditingRate} = contract || {}
  const isNetCreditingRateLessThanMinimum = netCreditingRate != null && minimumNetCreditingRate != null
  ? netCreditingRate < minimumNetCreditingRate : false
  return {
    initialValues: {adjustment: adjustment === '' || adjustment == null ? 0 : adjustment, grossRate, creditingRateRateComments, contractId},
    contract,
    selectedCreditingRate,
    netCreditingRate,
    canEditGrossRate,
    creditingRateCalcMethodText,
    isNetCreditingRateLessThanMinimum,
    marketValueView
  }
}

export default connect(mapStateToProps, actions)(ViewEditCreditingRatesForm)
