import moment from 'moment'

export default function (values, props) {
  const errors = {}

  let {creditingRates} = props

  const {creditingRateId: creditingRateIdBeingEdited, referenceDate: newReferenceDate} = values
  const creditingRateWithSameReferenceDateExists
  = creditingRates.some(({creditingRateId, referenceDate}) =>
  creditingRateId !== creditingRateIdBeingEdited && moment(referenceDate).utc().isSame(moment(newReferenceDate), 'day'))

  if (creditingRateWithSameReferenceDateExists) {
    errors.referenceDate = 'Reference date must be unique.'
  }

  return errors
}
