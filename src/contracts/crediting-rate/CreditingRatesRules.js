import {hasEntitlement} from '../../entitlements'
import store from '../../store/store'

const DELETABLE_STATUS = ['AWAITING', 'FORECAST']

export const hasCreditingRateEntitlement = () => {
  return hasEntitlement('CREDITING_RATE')
}

const isContractActive = ({status} = {}) => status === 'ACTIVE'

const isAwaitingOrForecast = (creditingRateStatus) => DELETABLE_STATUS.includes(creditingRateStatus)

const isCalculated = (creditingRateStatus) => ['PENDING', 'APPROVED'].includes(creditingRateStatus)

const isPending = (creditingRateStatus) => creditingRateStatus === 'PENDING'

export const canAdd = (contractStatus) => contractStatus === 'ACTIVE' && hasCreditingRateEntitlement()

export const canEdit = ({contract: {status: contractStatus}, creditingRate: {creditingRateId, status: creditingRateStatus}}) =>
  canAdd(contractStatus)
  && (creditingRateId ? (isAwaitingOrForecast(creditingRateStatus)) : true)

export const canDelete = canEdit

export const canViewCreditingRate = (status) => hasCreditingRateEntitlement() && (isCalculated(status))

export const canEditCreditingRate = ({creditingRate: {status: crStatus, modifiedId}, contract}) => {
  const {currentUser: {id}} = store.getState()
  return isContractActive(contract)
    && hasCreditingRateEntitlement()
    && isPending(crStatus)
    && modifiedId === id
}

export const canClearCreditingRate = ({creditingRate: {status: crStatus, modifiedId, locked}, contract}) =>
  isContractActive(contract)
  && hasCreditingRateEntitlement()
  && isPending(crStatus)
  && !locked

export const canApproveCreditingRate = ({creditingRate: {status: crStatus, modifiedId}, contract}) => {
  const {currentUser: {id}} = store.getState()
  return isContractActive(contract)
    && hasCreditingRateEntitlement()
    && isPending(crStatus)
    && modifiedId !== id
}

export const canCalculateCreditingRate = ({creditingRate: {status, marketValue}, contract}) =>
  isContractActive(contract)
  && hasCreditingRateEntitlement()
  && isAwaitingOrForecast(status)
  && !!marketValue

export const canCalculateCreditingRateWithoutEffectiveDate = (props) =>
  canCalculateCreditingRate(props)
  && !!props.creditingRate.effectiveDate
