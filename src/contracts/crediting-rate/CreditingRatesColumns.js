import React from 'react'
import {CreditingRatesMenuLink} from './CreditingRatesMenu'
import {dateFormat} from '../../components/grid'
import numeral from 'numeral'
import {FormattedNumber} from 'react-intl'

const commonProps = {
  customProps: {
    textAlign: 'center',
    width: 150
  }
}

export const VariableLengthPercentFormat = (value, numberOfDecimals) => {
  if (value === 0) {
    return `0.${'0'.repeat(numberOfDecimals)}%`
  } else {
    if (isNaN(value) || value === null) {
          return value
      } else {
          return `${parseFloat(value).toFixed(numberOfDecimals)}%`
      }
  }
}

export const OptionalPercentFormat = (value) => VariableLengthPercentFormat(value, 3)

export const CreditRatePercentFormat = (value, row) => VariableLengthPercentFormat(value, row.currentCreditingRateRoundDecimals)

export const currencyFormatMV = (number) => {
    if (number) {
        return numeral(number).format('(0,0.00)')
    } else {
        return ''
    }
}

export const percentFormatMK = (number) => {
    if (number) {
        return (<FormattedNumber value={number || 0} style='percent' minimumFractionDigits={2}/>)
    } else {
        return ''
    }
}

export const CreditingRatesColumns = [
  {
    field: 'creditingRateId',
    customProps: {
      hidden: true,
      isKey: true
    }
  },
  {
    field: 'creditingRateId',
    sort: false,
    label: '',
    ...commonProps,
    renderer: (creditingRateId, creditingRate) => (<CreditingRatesMenuLink creditingRate={creditingRate}/>), // eslint-disable-line react/display-name
    customProps: {
      textAlign: 'center',
      width: 50
    }
  },
  {
    field: 'referenceDate',
    sort: true,
    label: 'Reference Date',
    renderer: (referenceDate, row) => dateFormat(referenceDate), // eslint-disable-line react/display-name
    ...commonProps
  },
  {
    field: 'marketValue',
    sort: true,
    label: 'Market Value',
    renderer: currencyFormatMV,
      customProps: {
      textAlign: 'right',
      width: 150
    }
  },
  {
    field: 'bookValue',
    sort: true,
    label: 'Book Value',
    renderer: currencyFormatMV,
      customProps: {
          textAlign: 'right',
          width: 150
      }
  },
  {
    field: 'mvbvRatio',
    sort: true,
    label: 'MV/BV',
    renderer: percentFormatMK,
    customProps: {
      textAlign: 'right',
      width: 100
    }
  },
  {
    field: 'annualEffectiveYield',
    sort: true,
    label: 'AEY',
    renderer: OptionalPercentFormat,
      customProps: {
          textAlign: 'right',
          width: 150
      }
  },
  {
    field: 'duration',
    sort: true,
    label: 'Duration',
    renderer: (value) => value ? parseFloat(value).toFixed(2) : '',
    customProps: {
      textAlign: 'right',
      width: 100
    }
  },
  {
    field: 'rate',
    sort: true,
    label: 'Net Rate',
    renderer: CreditRatePercentFormat,
      customProps: {
          textAlign: 'right',
          width: 150
      }
  },
  {
    field: 'effectiveDate',
    sort: true,
    label: 'Effective Date',
    renderer: dateFormat,
    ...commonProps
  },
  {
    field: 'statusText',
    sort: true,
    label: 'Status',
    ...commonProps,
    customProps: {
      textAlign: 'left',
      width: 200
    }
  },
  {
    field: 'creditingRateRateComments',
    sort: true,
    label: 'Comments',
    customProps: {
      textAlign: 'left',
      width: 175
    }
  }
]
