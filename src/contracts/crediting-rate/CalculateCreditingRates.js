const parseNumber = (value) => {
  var returnValue
  if (value == null || value === '' || isNaN(value)) {
    returnValue = 0.0
  } else {
    returnValue = parseFloat(value)
  }
  return returnValue
}

const bps = (rate) => rate / 100

export const calculateNetCreditingRate = (grossRate = 0,
                                          adjustment = 0,
                                          premiumRate = 0,
                                          managerFeeRate = 0,
                                          advisorFeeRate = 0,
                                          creditingRateRoundingDecimals = 2) => {
  const netRate = grossRate - bps(premiumRate) - bps(managerFeeRate) - bps(advisorFeeRate) + adjustment
  return Math.round(netRate * Math.pow(10, creditingRateRoundingDecimals)) / Math.pow(10, creditingRateRoundingDecimals)
}
