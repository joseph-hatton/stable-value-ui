import _ from 'lodash'
import {destroy, change} from 'redux-form'
import {apiPath} from '../../config'
import http from '../../actions/http'
import {openDialog, openConfirmationAlert} from '../../components/redux-dialog'
import {openSnackbar} from '../../components/redux-dialog/redux-dialog'
import {dateFormat} from '../../components/utilities/Formatters'
import {calculateNetCreditingRate} from './CalculateCreditingRates'

const creditingRateApi = (contractId) => `${apiPath}/contracts/${contractId}/crediting-rates`

const balanceApi = (contractId, date) => `${apiPath}/contracts/${contractId}/balances?effectiveDate=${date}`

const mapCreditingRates = (getState, creditingRates) => {
  const {referenceData: {creditingRateStatuses}} = getState()
  const creditingRateStatusesById = _.keyBy(creditingRateStatuses, 'id')
  return creditingRates.map(creditingRate => {
    const {status, marketValue, bookValue, endingBookValue} = creditingRate
    return {
      ...creditingRate,
      statusText: creditingRateStatusesById[status]
        ? creditingRateStatusesById[status].text
        : status,
      bookValue: bookValue || endingBookValue,
      mvbvRatio: (marketValue && bookValue) ? (marketValue / bookValue) : null
    }
  })
}

export const loadContractCreditingRates = (contractId) => (dispatch, getState) => {
  dispatch({
    type: 'LOAD_CONTRACT_CREDITING_RATES',
    payload: http.get(creditingRateApi(contractId)).then(response => mapCreditingRates(getState, response.results))
  })
}

const STANDARD_CREDITING_RATE_CALC_METHODS = [
  'COMPOUND_AMORTIZATION_METHOD',
  'BOOK_VALUE_DOLLAR_DURATION',
  'MARKET_VALUE_DOLLAR_DURATION'
]

export const addEditMarketValue =
  (selectedCreditingRate, calculateCreditingRateAfterSavingEffectiveDate) => (dispatch) => {
    dispatch({
      type: 'SELECT_CREDITING_RATE',
      selectedCreditingRate,
      calculateCreditingRateAfterSavingEffectiveDate
    })
    setTimeout(() => dispatch(openDialog('AddEditMarketValueDialog')))
  }

export const viewEditCreditingRate = (selectedCreditingRate) => (dispatch, getState) => {
  const canEditGrossRate = selectedCreditingRate
    && STANDARD_CREDITING_RATE_CALC_METHODS.indexOf(selectedCreditingRate.creditingRateCalcMethod) === -1

  const {rate: netCreditingRate} = selectedCreditingRate

  const editCreditingRate = {
    netCreditingRate,
    canEditGrossRate
  }

  const {creditingRateCalcMethod} = selectedCreditingRate
  if (creditingRateCalcMethod) {
    const state = getState()
    const {referenceData: {creditingRateCalculationMethods}} = state
    const creditingRateCalculationMethodsById = _.keyBy(creditingRateCalculationMethods, 'id')
    editCreditingRate.creditingRateCalcMethodText = creditingRateCalculationMethodsById[creditingRateCalcMethod].text
  }

  dispatch({
    type: 'SELECT_CREDITING_RATE',
    selectedCreditingRate,
    editCreditingRate
  })

  setTimeout(() => dispatch(openDialog('ViewEditCreditingRatesDialog')))
}

export const adjustCreditingRate = (event, newAdjustment, oldAdjustment) => (dispatch, getState) => {
  if (!isNaN(newAdjustment)) {
    const {
      contracts: {
        selectedCreditingRate: {
          premiumRate,
          managerFeeRate,
          advisorFeeRate
        },
        contract: {creditingRateRoundDecimals, minimumNetCreditingRate = 0}
      }, form: {CreditingRateForm: {values: {grossRate}}}
    } = getState()
    const netCreditingRate = calculateNetCreditingRate(
      grossRate,
      Number(newAdjustment),
      premiumRate,
      managerFeeRate,
      advisorFeeRate,
      creditingRateRoundDecimals)
    dispatch({
      type: 'NET_CREDITING_RATE_ADJUSTED',
      netCreditingRate,
      isNetCreditingRateLessThanMinimum: netCreditingRate < minimumNetCreditingRate
    })
  }
}

export const adjustCreditingRateWithGrossRate = (event, newGrossRate, oldGrossRate) => (dispatch, getState) => {
  if (!isNaN(newGrossRate)) {
    const {
      contracts: {
        selectedCreditingRate: {
          premiumRate,
          managerFeeRate,
          advisorFeeRate
        },
        contract: {creditingRateRoundDecimals, minimumNetCreditingRate = 0}
      }, form: {CreditingRateForm: {values: {adjustment}}}
    } = getState()
    const netCreditingRate = calculateNetCreditingRate(
      Number(newGrossRate),
      adjustment,
      premiumRate,
      managerFeeRate,
      advisorFeeRate,
      creditingRateRoundDecimals)
    dispatch({
      type: 'NET_CREDITING_RATE_ADJUSTED',
      netCreditingRate,
      isNetCreditingRateLessThanMinimum: netCreditingRate < minimumNetCreditingRate
    })
  }
}

export const getBalanceForReferenceDate = (date) => (dispatch, getState) => {
  const {contracts: {contract: {contractId}}} = getState()
  dispatch({
    type: 'GET_BALANCE_FOR_REFERENCE_DATE',
    payload: http.get(balanceApi(contractId, date))
  })
}

export const clearAddEditMarketValue = () => (dispatch, getState) => {
  dispatch({
    type: 'CLEAR_ADD_EDIT_MARKET_VALUE'
  })
  const {contracts: {marketValueView}} = getState()
  if (marketValueView) {
    dispatch({
      type: 'CLEAR_EDIT_CONTRACT'
    })
  }
  dispatch(destroy('AddEditMarketValueForm'))
}

export const calculateAnnualEffectiveYield = (event, bondEffectiveField) => (dispatch) => {
  if (isNaN(bondEffectiveField)) {
    return
  }

  const annualEffectiveYield = ((Math.pow((1.0 + ((bondEffectiveField / 100.0) / 2.0)), 2)) - 1.0) * 100.0
  dispatch(change('AddEditMarketValueForm', 'annualEffectiveYield', _.toNumber(annualEffectiveYield.toFixed(6))))
}
const HTTP_ERROR = (error) => {
  return _.get(error, 'response.body.message')
}

const MARKET_VALUE_SAVED_MESSAGE = {
  successMessage: 'Market Value Saved Successfully',
  errorMessage: HTTP_ERROR
}

export const makeMarketValueUrl = (contractId, creditingRateId) => `${creditingRateApi(contractId)}/${creditingRateId}/market-value`

export const saveMarketValue = () => (dispatch, getState) => {
  const {
    contracts: {contract: {contractId}, balance, calculateCreditingRateAfterSavingEffectiveDate},
    form: {AddEditMarketValueForm}
  } = getState()
  const {values: {creditingRateId}} = AddEditMarketValueForm

  if (!creditingRateId && !balance) {
    dispatch(openSnackbar({
      type: 'error',
      message: `Book Value not found for the reference date ${dateFormat(AddEditMarketValueForm.values.referenceDate)}`,
      autoHideDuration: 5000
    }))
    return
  }
  const payload = {
    ...AddEditMarketValueForm.values,
    contractId
  }

  if (balance) {
    payload.bookValue = balance.endingBookValue
  }

  payload.bookValue = isNaN(payload.bookValue) ? payload.bookValue : Number(payload.bookValue)

  const method = creditingRateId ? 'put' : 'post'

  const url = method === 'post' ? creditingRateApi(contractId) : makeMarketValueUrl(contractId, creditingRateId)

  dispatch({
    type: 'SAVE_MARKET_VALUE',
    payload: http[method](url, payload, MARKET_VALUE_SAVED_MESSAGE, 'AddEditMarketValueDialog')
      .then(savedCreditingRate => {
        const mappedCreditingRate = mapCreditingRates(getState, [savedCreditingRate])[0]
        dispatch(clearAddEditMarketValue())

        if (calculateCreditingRateAfterSavingEffectiveDate) {
          dispatch(calculateCreditingRate(mappedCreditingRate))
        }

        return mappedCreditingRate
      })
  })
}

const MARKET_VALUE_DELETED_MESSAGE = {
  successMessage: 'Market Value Deleted Successfully'
}

export const deleteMarketValue = ({contractId, creditingRateId}) => (dispatch) => {
  dispatch(openConfirmationAlert({
      message: 'Are you sure you want to delete this Market Value?',
      onOk: () => {
        dispatch({
          type: 'DELETE_MARKET_VALUE',
          payload: http.delete(makeMarketValueUrl(contractId, creditingRateId), null, MARKET_VALUE_DELETED_MESSAGE)
        })
      }
    })
  )
}

const CREDITING_RATE_CALCULATED_MESSAGE = {
  successMessage: 'Crediting rate calculated Successfully',
  errorMessage: HTTP_ERROR
}

export const makeCalculateCreditingRateUrl = (contractId, creditingRateId) =>
  `${creditingRateApi(contractId)}/${creditingRateId}/calculate`

export const calculateCreditingRate = (creditingRate) => (dispatch, getState) => {
  const {contractId, creditingRateId, effectiveDate} = creditingRate
  const url = makeCalculateCreditingRateUrl(contractId, creditingRateId)

  dispatch({
    type: 'CALCULATE_CREDITING_RATE',
    payload: http.put(url, {effectiveDate}, CREDITING_RATE_CALCULATED_MESSAGE)
      .then(savedCreditingRate => {
        const mappedCreditingRate = mapCreditingRates(getState, [savedCreditingRate])[0]
        dispatch(viewEditCreditingRate(mappedCreditingRate))
        return mappedCreditingRate
      })
  })
}

const CREDITING_RATE_UPDATED_SUCCESSFULLY = {
  successMessage: 'Crediting rate updated successfully',
  errorMessage: HTTP_ERROR
}

export const makeUpdateCreditingRateUrl = (contractId, creditingRateId) => `${creditingRateApi(contractId)}/${creditingRateId}/crediting-rate`

export const updateCreditingRate = () => (dispatch, getState) => {
  const {
    contracts: {
      contract: {contractId, minimumNetCreditingRate},
      selectedCreditingRate: {creditingRateId},
      editCreditingRate: {netCreditingRate: rate}
    },
    form: {CreditingRateForm: {values: {adjustment, grossRate, creditingRateRateComments}}}
  } = getState()
  const payload = {
    grossRate,
    adjustment,
    rate,
    creditingRateRateComments
  }

  const url = makeUpdateCreditingRateUrl(contractId, creditingRateId)

  dispatch(({
    type: 'UPDATE_CREDITING_RATE',
    payload: http
      .put(url, payload, CREDITING_RATE_UPDATED_SUCCESSFULLY, 'ViewEditCreditingRatesDialog')
      .then(savedCreditingRate => mapCreditingRates(getState, [savedCreditingRate])[0])
  }))
}

const CREDITING_RATE_CLEARED_MESSAGE = {
  errorMessage: 'Crediting rate cleared successfully'
}

export const clearCreditingRate = (contractId, creditingRateId, dialogToClose = '') => (dispatch, getState) => {
  dispatch(openConfirmationAlert({
      message: 'Are you sure you want to clear this Crediting Rate?',
      onOk: () => {
        dispatch({
          type: 'UPDATE_CREDITING_RATE',
          payload: http.delete(makeUpdateCreditingRateUrl(contractId, creditingRateId), null, CREDITING_RATE_CLEARED_MESSAGE, dialogToClose)
            .then(savedCreditingRate => mapCreditingRates(getState, [savedCreditingRate])[0])
        })
      }
    })
  )
}

export const clearAdjustmentIfInvalid = (event, value) => (dispatch, getState) => {
  const {form, contracts: {contract: {creditingRateRoundDecimals}}} = getState()
  let adjustment = _.get(form, 'CreditingRateForm.values.adjustment')
  if (!isNaN(value)) {
    setTimeout(() => dispatch(change('CreditingRateForm', 'adjustment', Number(Number(value).toFixed(creditingRateRoundDecimals)))))
  } else {
    setTimeout(() => dispatch(change('CreditingRateForm', 'adjustment', 0)))
  }
}

export const clearGrossRateIfInvalid = (event, value) => (dispatch, getState) => {
  let grossRate = _.get(getState(), 'form.CreditingRateForm.values.grossRate')
  if (!isNaN(value)) {
    grossRate = Number(value)
  }
  setTimeout(() => dispatch(change('CreditingRateForm', 'grossRate', grossRate)))
}

export const clearAdjustmentCreditingForm = () => (dispatch) => {
  dispatch({
    type: 'SELECT_CREDITING_RATE',
    creditingRate: null
  })
  dispatch(destroy('CreditingRateForm'))
}

const CREDITING_RATE_APPROVED_SUCCESSFULLY = {
  successMessage: 'Crediting rate approved successfully',
  errorMessage: HTTP_ERROR
}

export const approveCreditingRate = (contractId, creditingRateId) => (dispatch, getState) => {
  const url = `${makeUpdateCreditingRateUrl(contractId, creditingRateId)}/approve`

  dispatch(openConfirmationAlert({
      message: 'Are you sure you want to Approve this Crediting Rate?',
      onOk: () => {
        dispatch({
          type: 'UPDATE_CREDITING_RATE',
          payload: http
            .put(url, {}, CREDITING_RATE_APPROVED_SUCCESSFULLY, 'ViewEditCreditingRatesDialog')
            .then(savedCreditingRate => mapCreditingRates(getState, [savedCreditingRate])[0])
        })
      }
    }, 'ConfirmCreditingRateApprove')
  )
}

export const setMarketValuesView = (marketValueView = true) => ({type: 'SET_MARKET_VALUE_VIEW', marketValueView})

export const openCreditingRate = (creditingRate) => (dispatch) => {
  const {status} = creditingRate
  if (status === 'PENDING' || status === 'APPROVED') {
    dispatch(viewEditCreditingRate(creditingRate))
  } else {
    dispatch(addEditMarketValue(creditingRate))
  }
}
