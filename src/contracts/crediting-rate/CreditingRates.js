import React from 'react'
import {PropTypes as T} from 'prop-types'
import _ from 'lodash'
import {IntlProvider} from 'react-intl'
import {connect} from 'react-redux'
import {Card} from 'material-ui/Card'
import {Grid, CreateGridConfig} from '../../components/grid'
import FABAddButton from '../../components/common/FABAddButton'
import {
  setMarketValuesView,
  loadContractCreditingRates,
  addEditMarketValue,
  viewEditCreditingRate
} from './CreditingRatesActions'
import {CreditingRatesColumns} from './CreditingRatesColumns'
import CreditingRateMenu from './CreditingRatesMenu'
import AddEditMarketValue from './AddEditMarketValue'
import ViewEditCreditingRate from './ViewEditCreditingRate'
import {canAdd} from './CreditingRatesRules'

class CreditingRates extends React.Component {
  componentDidMount () {
    const {contractId, setMarketValuesView, loadContractCreditingRates, creditingRates} = this.props
    setMarketValuesView(false)
    if (contractId && (_.isEmpty(creditingRates) || _.first(creditingRates).contractId !== contractId)) {
      loadContractCreditingRates(contractId)
    }
  }

  open (creditingRate) {
    const {status} = creditingRate
    const {viewEditCreditingRate, addEditMarketValue} = this.props
    if (status === 'PENDING' || status === 'APPROVED') {
      viewEditCreditingRate(creditingRate)
    } else {
      addEditMarketValue(creditingRate)
    }
  }

  render () {
    const {creditingRates, addEditMarketValue, selectedCreditingRate, status} = this.props
    const options = CreateGridConfig(creditingRates, {title: 'Crediting Rates'})
    return (
      <IntlProvider locale="en">
        <div>
          {
            !_.isEmpty(creditingRates)
            && (<Grid id="CreditingRates" options={options} data={creditingRates} columns={CreditingRatesColumns}
                      onRowDoubleClick={::this.open}/>)
          }
          <CreditingRateMenu/>
          {selectedCreditingRate && <AddEditMarketValue/>}
          {selectedCreditingRate && <ViewEditCreditingRate/>}
          {canAdd(status) && <FABAddButton disabled={false} onClick={() => {
            addEditMarketValue({}, false)
          }}/>}
        </div>
      </IntlProvider>
    )
  }
}

CreditingRates.propTypes = {
  contractId: T.number,
  creditingRates: T.array,
  selectedCreditingRate: T.object,
  status: T.string,
  loadContractCreditingRates: T.func.isRequired,
  setMarketValuesView: T.func.isRequired,
  addEditMarketValue: T.func.isRequired,
  viewEditCreditingRate: T.func.isRequired
}

const actions = {
  loadContractCreditingRates,
  setMarketValuesView,
  addEditMarketValue,
  viewEditCreditingRate
}

const mapStateToProps = ({contracts: {selectedCreditingRate, creditingRates, contract: {contractId, status, creditingRateRoundDecimals = 2}}}) => {
  const creditingRatesWithCurrentRoundDecimals = (creditingRates && creditingRates.length > 0)
    ? creditingRates.map(creditingRate => ({...creditingRate, currentCreditingRateRoundDecimals: creditingRateRoundDecimals}))
    : creditingRates
  return {
    contractId,
    status,
    creditingRates: creditingRatesWithCurrentRoundDecimals,
    selectedCreditingRate
  }
}

export default connect(mapStateToProps, actions)(CreditingRates)
