import React from 'react'
import {PropTypes as T} from 'prop-types'
import _ from 'lodash'
import OptionsIcon from 'material-ui/svg-icons/navigation/apps'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import CalculateIcon from 'material-ui/svg-icons/editor/functions'
import CreditIcon from 'material-ui/svg-icons/action/assignment'
import ClearIcon from 'material-ui/svg-icons/content/clear'
import ApproveIcon from 'material-ui/svg-icons/action/done'
import {blue500, red500, green500} from 'material-ui/styles/colors'
import {connect} from 'react-redux'
import {Popover, Menu, MenuItem} from 'material-ui'
import {openDialog, closeDialog} from '../../components/redux-dialog/redux-dialog'
import {
  addEditMarketValue,
  viewEditCreditingRate,
  deleteMarketValue,
  calculateCreditingRate,
  clearCreditingRate,
  approveCreditingRate
} from './CreditingRatesActions'
import {
  canDelete,
  canViewCreditingRate,
  canClearCreditingRate,
  canApproveCreditingRate,
  canCalculateCreditingRate
} from './CreditingRatesRules'

const creditingRateMenuDialog = 'creditingRateMenu'

const MenuLink = ({creditingRate, openDialog, children}) => (
  <a href="" onClick={(e) => {
    e.nativeEvent.preventDefault()
    openDialog(creditingRateMenuDialog, {
      anchorEl: e.currentTarget,
      creditingRate
    })
  }}>
    { _.isEmpty(children) ? <OptionsIcon color={blue500}/> : children}
  </a>
)

MenuLink.propTypes = {
  creditingRate: T.object,
  openDialog: T.func.isRequired,
  children: T.any
}

export const CreditingRatesMenuLink = connect(null, {openDialog})(MenuLink)

const Divider = () => (<hr style={{margin: 0}}/>)

export const validateAndCalculateCreditingRate = ({creditingRate, addEditMarketValue, calculateCreditingRate}) => {
  if (!creditingRate.effectiveDate) {
    addEditMarketValue(creditingRate, true)
  } else {
    calculateCreditingRate(creditingRate)
  }
}

const getContractWithStatus = (creditingRate, contract) => contract || {status: creditingRate.contractStatus}

export const CreditingRateMenus = [
  {
    primaryText: 'View/Edit Market Value',
    leftIcon: <EditIcon color={blue500}/>,
    onClick: ({creditingRate, addEditMarketValue}) => {
      addEditMarketValue(creditingRate, false)
    }
  },
  {
    primaryText: 'Delete Market Value',
    leftIcon: <DeleteIcon color={red500}/>,
    disabled: (creditingRate, contract) => !canDelete({creditingRate, contract}),
    onClick: ({creditingRate, deleteMarketValue}) => {
      deleteMarketValue(creditingRate)
    }
  },
  {
    primaryText: 'Calculate Crediting Rate',
    leftIcon: <CalculateIcon color={blue500}/>,
    disabled: (creditingRate, contract) => !canCalculateCreditingRate({creditingRate, contract}),
    onClick: validateAndCalculateCreditingRate
  },
  {
    primaryText: 'View Crediting Rate',
    leftIcon: <CreditIcon color={blue500}/>,
    disabled: ({status}) => !canViewCreditingRate(status),
    onClick: ({creditingRate, viewEditCreditingRate}) => {
      viewEditCreditingRate(creditingRate)
    }
  },
  {
    primaryText: 'Clear Crediting Rate',
    leftIcon: <ClearIcon color={blue500}/>,
    disabled: (creditingRate, contract) => !canClearCreditingRate({creditingRate, contract}),
    onClick: ({creditingRate: {contractId, creditingRateId}, clearCreditingRate}) => {
      clearCreditingRate(contractId, creditingRateId)
    }
  },
  {
    primaryText: 'Approve Crediting Rate',
    leftIcon: <ApproveIcon color={green500}/>,
    disabled: (creditingRate, contract) => !canApproveCreditingRate({creditingRate, contract}),
    onClick: ({creditingRate, viewEditCreditingRate}) => {
      viewEditCreditingRate(creditingRate)
    }
  }
]

const CreditingRateMenu = (props) => {
  const {open, anchorEl, closeDialog, creditingRate, contract} = props

  return (
    <Popover
      id='CreditingRateMenu-Popover'
      open={open}
      anchorEl={anchorEl}
      anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
      targetOrigin={{horizontal: 'left', vertical: 'top'}}
      onRequestClose={() => {
        closeDialog(creditingRateMenuDialog)
      }}
    >
      <Menu>
        {
          creditingRate && CreditingRateMenus.map(({primaryText, leftIcon, onClick, disabled}, index) => (
            <div key={index}>
              <MenuItem primaryText={primaryText} leftIcon={leftIcon} onClick={() => {
                if (onClick) {
                  closeDialog(creditingRateMenuDialog)
                  onClick(props)
                }
              }} disabled={_.isFunction(disabled) ? disabled(creditingRate, getContractWithStatus(creditingRate, contract)) : false}/>
              <Divider/>
            </div>
          ))
        }
      </Menu>
    </Popover>
  )
}

CreditingRateMenu.propTypes = {
  open: T.bool.isRequired,
  anchorEl: T.object,
  creditingRate: T.object,
  contract: T.object,
  closeDialog: T.func.isRequired,
  addEditMarketValue: T.func.isRequired,
  deleteMarketValue: T.func.isRequired,
  calculateCreditingRate: T.func.isRequired,
  clearCreditingRate: T.func.isRequired,
  approveCreditingRate: T.func.isRequired,
  viewEditCreditingRate: T.func.isRequired
}

export const mapStateToProps = ({dialog: {creditingRateMenu}, contracts: {contract}}) => {
  const {anchorEl, creditingRate} = creditingRateMenu || {}
  return {
    open: !!creditingRateMenu,
    anchorEl,
    creditingRate,
    contract
  }
}

const actions = {
  viewEditCreditingRate,
  addEditMarketValue,
  deleteMarketValue,
  closeDialog,
  calculateCreditingRate,
  clearCreditingRate,
  approveCreditingRate
}

export default connect(mapStateToProps, actions)(CreditingRateMenu)
