import React from 'react'
import {PropTypes as T} from 'prop-types'
import _ from 'lodash'
import {connect} from 'react-redux'
import RaisedButton from 'material-ui/RaisedButton'
import {Field, reduxForm, touch} from 'redux-form'
import {DatePicker} from 'redux-form-material-ui'
import ReduxDialog from '../../components/redux-dialog'
import NumberField from '../NumberField'
import ContractPicker from '../ContractPicker'
import {exists, Required, FormatCurrency} from '../../components/forms/Validators'
import {dateFormat, toUtc} from '../../components/utilities/Formatters'
import {
  getBalanceForReferenceDate,
  clearAddEditMarketValue,
  saveMarketValue,
  calculateAnnualEffectiveYield,
  loadContractCreditingRates
} from './CreditingRatesActions'
import {isEditable} from '../ContractForms'
import {canEdit} from './CreditingRatesRules'
import ValidateMarketValue from './ValidateMarketValue'

const formId = 'AddEditMarketValueForm'

const EDITABLE_STATUS = ['AWAITING', 'FORECAST']

const isDisabled = (status) => status && EDITABLE_STATUS.indexOf(status) === -1

const EFFECTIVE_DATE_REQUIRED_RATE_CALCULATION = 'Effective Date is required to calculate Crediting Rate.'

export const ActiveContracts = ({status}) => status === 'ACTIVE'

export const effectiveDateRequired = value => exists(value) ? undefined : EFFECTIVE_DATE_REQUIRED_RATE_CALCULATION

export class AddEditMarketValue extends React.Component {
  componentWillReceiveProps (nextProps) {
    const {calculateCreditingRateAfterSavingEffectiveDate, touchEffectiveDateToTriggerValidation} = nextProps
    if (calculateCreditingRateAfterSavingEffectiveDate) {
      touchEffectiveDateToTriggerValidation()
    }

    const {selectedContractId, creditingRates, loadContractCreditingRates, touchReferenceDateToTriggerValidation} = nextProps
    if (selectedContractId && (creditingRates.length === 0 || creditingRates[0].contractId !== selectedContractId)) {
      loadContractCreditingRates(selectedContractId)
    }
    touchReferenceDateToTriggerValidation()
  }

  render () {
    const {
      handleSubmit,
      initialValues,
      bookValue,
      createdDate,
      disabled,
      calculateCreditingRateAfterSavingEffectiveDate,
      getBalanceForReferenceDate,
      calculateAnnualEffectiveYield,
      clearAddEditMarketValue,
      saveMarketValue,
      marketValueView,
      contractEffectiveDate
    } = this.props
    const saveButtonLabel = calculateCreditingRateAfterSavingEffectiveDate ? 'Save and Calculate Rate' : 'Save'

    const actions = [
      <RaisedButton
        key="Save"
        label={saveButtonLabel}
        primary={true}
        style={{marginRight: 20}}
        type="submit"
        form={formId}
        disabled={disabled}
      />
    ]

    return (
      <ReduxDialog
        dialogName="AddEditMarketValueDialog"
        title="Market Value"
        actions={actions}
        modal={true}
        autoScrollBodyContent={true}
        cancelButton onCancel={clearAddEditMarketValue}>
        <form id={formId} onSubmit={handleSubmit(saveMarketValue)}>
          {
            marketValueView && <div className="row">
              <div className="col-lg-12">
                <ContractPicker name="contractId" type="text" floatingLabelText="Contract"
                                disabled={exists(initialValues.creditingRateId)}
                                filterContract={ActiveContracts}
                                loadSelectedContract
                                selectedContractId={initialValues.contractId}
                />
              </div>
            </div>
          }
          <div className="row">
            <div className="col-lg-6">
              <Field autoOk validate={Required} fullWidth name="referenceDate" format={toUtc} formatDate={dateFormat}
                     component={DatePicker} firstDayOfWeek={0} minDate={contractEffectiveDate}
                     floatingLabelText="* Reference Date" onChange={(event, value) => {
                if (!disabled) {
                  getBalanceForReferenceDate(value)
                }
              }}/>
            </div>
            <div className="col-lg-6">
              <NumberField validate={Required} fullWidth name="marketValue" floatingLabelText="* Market Value"/>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-6">
              <NumberField validate={Required} fullWidth name="annualEffectiveYield" floatingLabelText="* AEY"
                           numberFormat="0.000000"/>
            </div>
            <div className="col-lg-6">
              <NumberField fullWidth name="bondEffectiveYield" floatingLabelText="BEY"
                           onChange={calculateAnnualEffectiveYield} numberFormat="0.000"/>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-6">
              <NumberField validate={Required} fullWidth name="duration" floatingLabelText="* Duration"
                           numberFormat="0.00"/>
            </div>
            <div className="col-lg-6">
              <Field fullWidth name="effectiveDate" format={toUtc} formatDate={dateFormat} component={DatePicker} firstDayOfWeek={0}
                     floatingLabelText="Effective Date" minDate={contractEffectiveDate} autoOk
                     validate={calculateCreditingRateAfterSavingEffectiveDate ? effectiveDateRequired : _.noop}/>
            </div>
          </div>
          <br/>
          <br/>
          <div className="row">
            <div className="col-lg-3">
              Book Value:
            </div>
            <div className="col-lg-3" style={{textAlign: 'left', color: 'black'}}>
              {bookValue ? `$${FormatCurrency(bookValue)}` : ''}
            </div>
            <div className="col-lg-3">
              Created Date:
            </div>
            <div className="col-lg-3" style={{textAlign: 'left', color: 'black'}}>
              {createdDate ? `${dateFormat(createdDate)}` : ''}
            </div>
          </div>
        </form>
      </ReduxDialog>
    )
  }
}

AddEditMarketValue.propTypes = {
  handleSubmit: T.func,
  initialValues: T.object,
  bookValue: T.number,
  createdDate: T.string,
  disabled: T.bool,
  calculateCreditingRateAfterSavingEffectiveDate: T.bool,
  getBalanceForReferenceDate: T.func.isRequired,
  clearAddEditMarketValue: T.func.isRequired,
  saveMarketValue: T.func.isRequired,
  calculateAnnualEffectiveYield: T.func.isRequired,
  touchReferenceDateToTriggerValidation: T.func.isRequired,
  touchEffectiveDateToTriggerValidation: T.func.isRequired,
  loadContractCreditingRates: T.func.isRequired,
  creditingRates: T.array,
  marketValueView: T.bool,
  contractEffectiveDate: T.func,
  referenceDate: T.func,
  selectedContractId: T.number
}

export const AddEditMarketValueForm = reduxForm({form: formId, validate: ValidateMarketValue})(AddEditMarketValue)

export const mapStateToProps = (state) => {
  const {
    contracts: {
      contract,
      creditingRates,
      selectedCreditingRate,
      balance,
      calculateCreditingRateAfterSavingEffectiveDate,
      marketValueView
    },
    form
  } = state
  const {bookValue, createdDate, status} = selectedCreditingRate || {}
  const props = {
    initialValues: selectedCreditingRate || {},
    bookValue: balance ? balance.endingBookValue : bookValue,
    disabled: contract && (!canEdit({
      contract,
      creditingRate: selectedCreditingRate
    }) || (!isEditable(contract) || isDisabled(status))),
    createdDate,
    calculateCreditingRateAfterSavingEffectiveDate,
    creditingRates,
    marketValueView,
    selectedContractId: _.get(form, 'AddEditMarketValueForm.values.contractId')
  }
  if (contract && contract.effectiveDate) {
    const contractEffectiveDate = new Date(contract.effectiveDate)
    contractEffectiveDate.setDate(contractEffectiveDate.getDate() + 1)
    props.contractEffectiveDate = contractEffectiveDate
  }
  return props
}

export const actions = {
  getBalanceForReferenceDate,
  clearAddEditMarketValue,
  saveMarketValue,
  calculateAnnualEffectiveYield,
  loadContractCreditingRates,
  touchEffectiveDateToTriggerValidation: () => (disptach) => {
    disptach(touch('AddEditMarketValueForm', 'effectiveDate'))
  },
  touchReferenceDateToTriggerValidation: () => (disptach) => {
    disptach(touch('AddEditMarketValueForm', 'reference'))
  }
}

export default connect(mapStateToProps, actions)(AddEditMarketValueForm)
