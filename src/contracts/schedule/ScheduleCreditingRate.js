import React from 'react'
import {PropTypes as T} from 'prop-types'
import _ from 'lodash'
import {connect} from 'react-redux'
import {Field, change} from 'redux-form'
import {TextField, DatePicker} from 'redux-form-material-ui'
import {MaxLength} from '../../components/forms/Validators'
import RateField from '../RateField'
import {
  CreditingRateCalcMethods,
  CalculationMethodInputDefinitions,
  CreditingRateRoundingDecimals,
  CreditingRateInputBases,
  CreditingRateFrequencies,
  CreditingRateMonths,
  CreditingRateTypes
} from '../ContractRefDataFields'
import YesNo from '../../components/forms/YesNo'
import AppStyles from '../../styles/styles'
import {SCHEDULE_FUNDING} from '../ContractForms'
import { toUtc, dateFormat } from '../../components/utilities/Formatters'
import AuditableContractField from '../edithistory/AuditableContractField'

const condRequired = (cond) => {
  return cond ? '* ' : ''
}

export const toNumberAcceptsNegative = (text, previousValue) => {
  if (['-', '-.', '.'].indexOf(text) !== -1 || !isNaN(text)) {
    return text
  }
  return previousValue
}

export const CreditingRate = ({creditingRateFrequencyChanged, creditingRateFrequency, creditingRateTypeChanged, creditingRateType, netOfThirdPartyFees, netOfThirdPartyFeesChanged, creditingRateFirstReset}) => (
  <div>
    <div className="row">
      <div className="col-md-3">
        <AuditableContractField formId={SCHEDULE_FUNDING}>
          <CreditingRateCalcMethods name="creditingRateCalcMethod" floatingLabelText="* Calculation Method"/>
        </AuditableContractField>
      </div>
      <div className="col-lg-3">
        <CalculationMethodInputDefinitions name="calculationMethodDefinition"/>
      </div>
      <div className="col-lg-3">
        <RateField normalize={toNumberAcceptsNegative} fullWidth name="minimumNetCreditingRate" floatingLabelText="* Minimum Net %"
                   rateFormat="0.000"/>
      </div>
      <div className="col-lg-3">
        <CreditingRateRoundingDecimals name="creditingRateRoundDecimals" floatingLabelText="Rounding Decimals"/>
      </div>
    </div>
    <div className="row">
      <div className="col-lg-3">
        <CreditingRateInputBases name="creditingRateInputBasis"/>
      </div>
      <div className="col-lg-3">
        <CreditingRateFrequencies id="creditingRateFrequencyId" name="creditingRateFrequency" onChange={creditingRateFrequencyChanged}/>
      </div>
      <div className="col-lg-3">
        <CreditingRateMonths name="creditingRateMonth" disabled={creditingRateFrequency === 'DAILY'}/>
      </div>
      <div className="col-lg-3">
        <Field autoOk fullWidth name="creditingRateFirstReset" component={DatePicker} format={toUtc} firstDayOfWeek={0}
               disabled={creditingRateFrequency !== 'DAILY'}
               floatingLabelText={condRequired(creditingRateFrequency === 'DAILY') + 'First Reset Date'}
               formatDate={dateFormat}/>
      </div>
    </div>
    <div className="row">
      <div className="col-lg-3">
        <CreditingRateTypes name="creditingRateType" onChange={creditingRateTypeChanged}/>
      </div>
      <div className="col-lg-3" style={AppStyles.toggleDivStyle}>
        <YesNo label="Net of Third Party Fees" id="yesNoId"
               labelPosition="right" onChange={netOfThirdPartyFeesChanged}
               name="netOfThirdPartyFees" disabled={creditingRateType !== 'NET'}/>
      </div>
      <div className="col-lg-3" style={AppStyles.toggleDivStyle}>
        <YesNo label="Manager Fee" labelPosition="right" name="creditingRateManagerFee" disabled={!netOfThirdPartyFees}/>
      </div>
      <div className="col-lg-3" style={AppStyles.toggleDivStyle}>
        <YesNo label="Advisor Fee" labelPosition="right" name="creditingRateAdvisorFee" disabled={!netOfThirdPartyFees}/>
      </div>
    </div>
    <div className="row">
      <div className="col-lg-12">
        <Field fullWidth textareaStyle={AppStyles.textAreaFont} name="investmentGuidelinesComments"
               component={TextField}
               rows={2}
               floatingLabelText="Comments (Max: 255 Chars)"
               multiLine validate={MaxLength(255)}/>
      </div>
    </div>
  </div>
)

CreditingRate.propTypes = {
  creditingRateFrequency: T.string,
  creditingRateFrequencyChanged: T.func.isRequired,
  creditingRateType: T.string,
  creditingRateTypeChanged: T.func.isRequired,
  netOfThirdPartyFeesChanged: T.func.isRequired,
  netOfThirdPartyFees: T.bool,
  creditingRateFirstReset: T.string
}

export const actions = {
  creditingRateFrequencyChanged: (event, frequency) => (dispatch, getState) => {
    const {contracts: {contract = {}}} = getState()
    if (frequency === 'DAILY') {
      dispatch(change(SCHEDULE_FUNDING, 'creditingRateMonth', null))
    } else {
      dispatch(change(SCHEDULE_FUNDING, 'creditingRateFirstReset', contract.creditingRateFirstReset))
    }
  },
  creditingRateTypeChanged: (event, creditingRateType) => (dispatch, getState) => {
    const {contracts: {contract = {}}} = getState()
    if (creditingRateType === 'GROSS') {
      dispatch(change(SCHEDULE_FUNDING, 'netOfThirdPartyFees', null))
    } else {
      dispatch(change(SCHEDULE_FUNDING, 'netOfThirdPartyFees', contract.netOfThirdPartyFees))
    }
  },
  netOfThirdPartyFeesChanged: (event, sw) => (dispatch) => {
    if (!sw) {
      dispatch(change(SCHEDULE_FUNDING, 'managerFeePayor', null))
      dispatch(change(SCHEDULE_FUNDING, 'advisorFeePayor', null))
    }
  }
}

export const mapStateToProps = ({form}) => ({
  creditingRateFrequency: _.get(form, 'scheduleFunding.values.creditingRateFrequency'),
  creditingRateType: _.get(form, 'scheduleFunding.values.creditingRateType'),
  netOfThirdPartyFees: _.get(form, 'scheduleFunding.values.netOfThirdPartyFees')
})

export default connect(mapStateToProps, actions)(CreditingRate)
