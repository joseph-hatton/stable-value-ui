import React from 'react'

import { Field } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import RateField from '../RateField'
import CalendarTypes from '../../components/forms/CalendarTypes'
import { MaxLength } from '../../components/forms/Validators'
import AppStyles from '../../styles/styles'

const Termination = () => (
  <div>
    <div className="row">
      <div className="col-lg-12">
        <RateField name="extTerminationMatDtExt" floatingLabelText="Extended Termination Maturity Date Extension" rateFormat="0.00"/>
      </div>
    </div>
    <div className="row">
      <div className="col-lg-6">
        <RateField name="extTerminationConvDays" floatingLabelText="Extended Termination Conversion Days" rateFormat="0"/>
      </div>
      <div className="col-lg-6">
        <CalendarTypes name="calendarTypeTerminationDays"/>
      </div>
    </div>
    <div className="row">
      <div className="col-lg-6">
        <RateField name="daysToMakePaym" floatingLabelText="Days to Make Payment" rateFormat="0"/>
      </div>
      <div className="col-lg-6">
        <CalendarTypes name="calendarTypePaymentDays"/>
      </div>
    </div>
    <div className="row">
      <div className="col-lg-12">
        <Field textareaStyle={AppStyles.textAreaFont} name="terminationComments"
            component={TextField} floatingLabelText="Comments (Max: 255 Chars)" rows={2}
            multiLine rowsMax={3} validate={MaxLength(255)}/>
      </div>
    </div>
  </div>
)

export default Termination
