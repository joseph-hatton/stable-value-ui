import React from 'react'
import Funding from './Funding'
import ScheduleCreditingRate from './ScheduleCreditingRate'
import Withdrawal from './Withdrawal'
import InvestmentGuidelines from './InvestmentGuidelines'
import Termination from './Termination'

const ContractScheduleTabs = [
  {
    label: 'Funding',
    url: 'funding',
    ContractTab: Funding
  },
  {
    label: 'Crediting Rate',
    url: 'crediting-rate',
    ContractTab: ScheduleCreditingRate
  },
  {
    label: 'Withdrawal',
    url: 'withdrawal',
    ContractTab: Withdrawal
  },
  {
    label: 'Investment Guidelines',
    url: 'investment-guidelines',
    ContractTab: InvestmentGuidelines
  },
  {
    label: 'Termination',
    url: 'termination',
    ContractTab: Termination
  }
]

export default ContractScheduleTabs
