import React from 'react'
import NumberField from '../NumberField'
import { PropTypes as T } from 'prop-types'
import { Field, change } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import { MaxLength, Required } from '../../components/forms/Validators'
import AppStyles from '../../styles/styles'
import {AdditionalDeposits} from '../ContractRefDataFields'
import { connect } from 'react-redux'
import _ from 'lodash'

export const Funding = ({ additionalDeposits, additionalDepositsChange }) => (
  <div>
    <div className="row">
      <div className="col-lg-4">
        <AdditionalDeposits name="additionalDeposits" onChange={additionalDepositsChange} />
      </div>
    <div className="col-lg-4">
        {
            additionalDeposits === 'UP_TO_LIMIT'
            && (
    <NumberField validate={Required} fullWidth name="depositLimit" floatingLabelText="Deposit Limit"
                 />
            )}
    </div>
    </div>
    <div className="row">
      <div className="col-lg-4">
        <Field fullWidth textareaStyle={AppStyles.textAreaFont} name="fundingComments" component={TextField}
               floatingLabelText="Comments (Max: 255 Chars)"
               rows={2}
               multiLine rowsMax={3} validate={MaxLength(255)}/>
      </div>
    </div>
  </div>
)

export const actions = {
    additionalDepositsChange: (event, value) => (dispatch) => {
        if (value !== 'UP_TO_LIMIT') {
        dispatch(change('scheduleFunding', 'depositLimit', null))
        }
    }}

Funding.propTypes = {
    additionalDeposits: T.string,
    depositLimit: T.number,
    additionalDepositsChange: T.func
}

export const mapStateToProps = ({form}) => ({
    additionalDeposits: _.get(form, 'scheduleFunding.values.additionalDeposits'),
    depositLimit: _.get(form, 'scheduleFunding.values.depositLimit')
})

export default connect(mapStateToProps, actions)(Funding)
