import React from 'react'
import {PropTypes as T} from 'prop-types'
import _ from 'lodash'
import {connect} from 'react-redux'
import {Field, change} from 'redux-form'
import {TextField, Checkbox} from 'redux-form-material-ui'
import {MaxLength, toInteger} from '../../components/forms/Validators'
import RateField from '../RateField'
import YesNo from '../../components/forms/YesNo'
import AverageCreditQualities from '../../components/forms/AverageCreditQualities'
import AppStyles from '../../styles/styles'
import {SCHEDULE_FUNDING} from '../ContractForms'
import {loadDerivativeTypes} from '../ContractActions'
import AuditableContractField from '../edithistory/AuditableContractField'
import clearFormField from '../../components/forms/clearFormField'

export class InvestmentGuidelines extends React.Component {
  render () {
    const {derivativeTypesRefData, securitiesLendingAllowed, securitiesLendingAllowedChanged, repurchaseAgreementsAllowed, repurchaseAgreementsAllowedChanged} = this.props
    return (
      <div style={{marginTop: 20}}>
        <div className="row">
          <div className="col-lg-3" style={AppStyles.textAreaDivStyle}>
            <AverageCreditQualities name="averageCreditQuality" floatingLabelText="Average Credit Quality"/>
          </div>
          <div className="col-lg-3">
            <AuditableContractField formId={SCHEDULE_FUNDING}>
              <RateField fullWidth name="accountDurationLimit" floatingLabelText="Account Duration Limit"
                         rateFormat="0.00"/>
            </AuditableContractField>
          </div>
          <div className="col-lg-3">
            <RateField name="downgradePercentage" floatingLabelText="Downgrade/PINCH Bucket Pct (%)"
                       rateFormat="0.0"/>
          </div>
          <div className="col-lg-3">
            <RateField name="defaultBucketPercentage" floatingLabelText="Default Bucket Percentage (%)"
                       rateFormat="0.00"/>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-4" style={AppStyles.toggleDivStyle}>
            <YesNo id="securitiesLendingAllowedId" name="securitiesLendingAllowed" label="Securites Lending Allowed"
                   onChange={securitiesLendingAllowedChanged}/>
          </div>
          <div className="col-lg-4">
            <RateField name="securitiesLendingLimit" floatingLabelText="Securites Lending Limit (%)" rateFormat="0.00"
                       disabled={!securitiesLendingAllowed}/>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-4" style={AppStyles.toggleDivStyle}>
            <YesNo name="repurchaseAgreementsAllowed" label="Repurchase Agreements Allowed"
                   onChange={repurchaseAgreementsAllowedChanged}/>
          </div>
          <div className="col-lg-4">
            <RateField name="repurchaseAgreementsLimit" floatingLabelText="Repurchase Agreements Limit (%)"
                       rateFormat="0.00" disabled={!repurchaseAgreementsAllowed}/>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12" style={AppStyles.textAreaDivStyle}>
            <Field fullWidth textareaStyle={AppStyles.textAreaFont} name="investmentGuidelinesComments"
                   component={TextField}
                   floatingLabelText="Comments (Max: 255 Chars)" multiLine rows={2}
                   validate={MaxLength(255)}/>
          </div>
        </div>
      </div>
    )
  }
}

InvestmentGuidelines.propTypes = {
  contract: T.object,
  derivativeTypesRefData: T.array,
  derivativeTypes: T.array,
  loadDerivativeTypes: T.func.isRequired,
  checkDerivativeTypeChanged: T.func.isRequired,
  securitiesLendingAllowedChanged: T.func.isRequired,
  securitiesLendingAllowed: T.bool,
  repurchaseAgreementsAllowedChanged: T.func.isRequired,
  repurchaseAgreementsAllowed: T.bool
}

export const actions = {
  loadDerivativeTypes,
  checkDerivativeTypeChanged: (event, dtype) => (dispatch, getState) => {
    const {contracts: {contract = {}}} = getState()
    dispatch(change(SCHEDULE_FUNDING, 'TREASURY_FUTURES', 'checked'))
  },
  securitiesLendingAllowedChanged: (event, securitiesLendingAllowed) => (dispatch, getState) => {
    console.log(securitiesLendingAllowed)
    const {contracts: {contract = {}}} = getState()
    dispatch(change(SCHEDULE_FUNDING, 'securitiesLendingAllowed', securitiesLendingAllowed))
    if (!securitiesLendingAllowed) {
      setTimeout(() => clearFormField(SCHEDULE_FUNDING, 'securitiesLendingLimit'))
    }
  },
  repurchaseAgreementsAllowedChanged: (event, repurchaseAgreementsAllowed) => (dispatch, getState) => {
    const {contracts: {contract = {}}} = getState()
    dispatch(change(SCHEDULE_FUNDING, 'repurchaseAgreementsAllowed', repurchaseAgreementsAllowed))
    if (!repurchaseAgreementsAllowed) {
      setTimeout(() => clearFormField(SCHEDULE_FUNDING, 'repurchaseAgreementsLimit'))
    }
  }
}

export const mapStateToProps = ({form, referenceData: {derivativeTypes}}) => ({
  derivativeTypesRefData: derivativeTypes || [],
  securitiesLendingAllowed: form.scheduleFunding.values.securitiesLendingAllowed,
  repurchaseAgreementsAllowed: form.scheduleFunding.values.repurchaseAgreementsAllowed
})

export default connect(mapStateToProps, actions)(InvestmentGuidelines)
