import React from 'react'
import { PropTypes as T } from 'prop-types'
import _ from 'lodash'
import { connect } from 'react-redux'
import { Field, change } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import { MaxLength, toInteger } from '../../components/forms/Validators'
import RateField from '../RateField'
import {
  CorridorOptionCalculationMethods
} from '../ContractRefDataFields'
import YesNo from '../../components/forms/YesNo'
import AppStyles from '../../styles/styles'
import { SCHEDULE_FUNDING } from '../ContractForms'

export const Withdrawal = ({
                      corrOption,
                      corrOptionChanged,
                      corrOptionCalcMethod,
                      corrOptionCalcMethodChanged
                    }) => (
  <div style={{marginTop: 20}}>
    <div className="row">
      <div className="col-lg-4" style={AppStyles.toggleDivStyle}>
        <YesNo id="yesNoId" label="* Corridor Option"
               name="corrOption" onChange={corrOptionChanged}/>
      </div>
    </div>
    <div className="row">
      <div className="col-lg-4">
        <RateField fullWidth name="corrOptionLimitsAnnual" floatingLabelText="Corridor Option Limits-Annual %"
                   rateFormat="0.0"/>

      </div>
      <div className="col-lg-4">
        <RateField fullWidth name="corrOptionLimitsLifetime" floatingLabelText="Corridor Option Limits-Lifetime %"
                   rateFormat="0.0"/>

      </div>
    </div>
    <div className="row">
      <div className="col-lg-4">
        <CorridorOptionCalculationMethods name="corrOptionCalcMethod" />
      </div>
      <div className="col-lg-4">
          {
              corrOptionCalcMethod === 'ROLLING'
              && (
        <Field id="corrOptionRollingMonthsId" fullWidth name="corrOptionRollingMonths" component={TextField}
               floatingLabelText="* Corridor Option Limits-Rolling Months" parse={toInteger} onChange={corrOptionCalcMethodChanged}/>
              )}
      </div>
    </div>
    <div className="row">
      <div className="col-lg-8">
        <Field fullWidth textareaStyle={AppStyles.textAreaFont} name="withdrawalComments" component={TextField}
               floatingLabelText="Comments (Max: 255 Chars)"
               rows={2}
               multiLine rowsMax={5} validate={MaxLength(255)}/>
      </div>
    </div>
  </div>
)

Withdrawal.propTypes = {
  corrOption: T.bool,
  corrOptionChanged: T.func.isRequired,
  corrOptionCalcMethod: T.string,
  corrOptionCalcMethodChanged: T.func.isRequired
}

export const actions = {
  corrOptionChanged: (event, corrOption) => (dispatch, getState) => {
    const {contracts: {contract = {}}} = getState()
    if (!corrOption) {
      dispatch(change(SCHEDULE_FUNDING, 'corrOptionCalcMethod', null))
      dispatch(change(SCHEDULE_FUNDING, 'corrOptionRollingMonths', null))
    } else {
      dispatch(change(SCHEDULE_FUNDING, 'corrOptionCalcMethod', contract.corrOptionCalcMethod))
      dispatch(change(SCHEDULE_FUNDING, 'corrOptionRollingMonths', contract.corrOptionRollingMonths))
    }
  },
  corrOptionCalcMethodChanged: (event, corrOptionCalcMethod) => (dispatch, getState) => {
    const {contracts: {contract = {}}} = getState()
    if (corrOptionCalcMethod !== 'ROLLING') {
      dispatch(change(SCHEDULE_FUNDING, 'corrOptionRollingMonths', null))
    } else {
      dispatch(change(SCHEDULE_FUNDING, 'corrOptionRollingMonths', contract.corrOptionRollingMonths))
    }
  }
}

export const mapStateToProps = ({form}) => ({
  corrOption: _.get(form, 'scheduleFunding.values.corrOption'),
  corrOptionCalcMethod: _.get(form, 'scheduleFunding.values.corrOptionCalcMethod')
})

export default connect(mapStateToProps, actions)(Withdrawal)
