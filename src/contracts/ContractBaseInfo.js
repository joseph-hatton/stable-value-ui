import React from 'react'
import _ from 'lodash'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import { Field, change } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import { BASE_INFO } from './ContractForms'
import createContractForm from './CreateContractForm'
import { Card } from 'material-ui/Card'
import styles from '../styles/styles'
import MonthAsTextFormField from '../components/forms/MonthAsTextFormField'
import DaysOfMonth from '../components/forms/DaysOfMonthFormField'
import BenefitResponseTypes from '../components/forms/BenefitResponseTypes'
import BenefitTiers from '../components/forms/BenefitTiers'
import CalendarTypes from '../components/forms/CalendarTypes'
import NotificationTypes from '../components/forms/NotificationTypes'
import BillFrequency from '../components/forms/BillFrequency'
import FeeDailyRateFormula from '../components/forms/FeeDailyRateFormula'
import MarketValueEvent from '../components/forms/MarketValueEvents'
import YesNo from '../components/forms/YesNo'
import { MaxLength, isInteger, toInteger } from '../components/forms/Validators'
import { planAnniversaryMonthChanged, planAnniversaryDayChanged } from './ContractActions'
import AuditableContractField from './edithistory/AuditableContractField'
import RateField from './RateField'

const sectionStyle = {
  padding: '10px 20px 10px 20px',
  marginTop: '20px'
}

export const mapPlanAnniversary = (planAnniversary) => {
  const planAnniversaryMonthDate = {
    planAnniversaryMonth: 'DECEMBER',
    planAnniversaryDay: 31
  }
  if (planAnniversary) {
    const split = planAnniversary.split(' ')
    planAnniversaryMonthDate.planAnniversaryMonth = split[0]
    planAnniversaryMonthDate.planAnniversaryDay = parseInt(split[1])
  }
  return planAnniversaryMonthDate
}

export const mapStateToForm = ({contracts: {contract}}) => {
  return {
    initialValues: contract ? {
      feeDailyRateFormula: 'DAILY_FEE',
      ...contract,
      ...mapPlanAnniversary(contract.planAnniversary)
    } : {}
  }
}

const ContractBaseInfoForm = createContractForm(BASE_INFO, mapStateToForm)

const ContractBaseInfo = ({planAnniversaryMonthChanged, planAnniversaryDayChanged, cashBufferReplenishment, cashBufferReplenishmentOnChange}) => {
  return (
    <ContractBaseInfoForm>
      <Card style={sectionStyle}>
        <div className="row">
          <div className="col-lg-4">
            <div className="row">
              <div className="col-lg-8">
                <MonthAsTextFormField name="planAnniversaryMonth" floatingLabelText="Plan Anniversary"
                                      onChange={planAnniversaryMonthChanged}/>
              </div>
              <div className="col-lg-4">
                <DaysOfMonth name="planAnniversaryDay" floatingLabelText="Day" onChange={planAnniversaryDayChanged}/>
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            <AuditableContractField formId={BASE_INFO}>
              <Field fullWidth name="legalFormName" component={TextField} floatingLabelText="* Legal Form Name"/>
            </AuditableContractField>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-4">
            <div className="row">
              <div className="col-lg-8">
                <BenefitResponseTypes name="benefitResponseType" floatingLabelText="* Benefit Response Type"/>
              </div>
              <div className="col-lg-4">
                <BenefitTiers name="tierNumber"/>
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            <NotificationTypes name="benefitResponseNotification" floatingLabelText="Benefit Response Notification"/>
          </div>
          <div className="col-lg-4">
            <div className="row">
              <div className="col-lg-7">
                <Field name="benefitResponseDays" fullWidth component={TextField} validate={isInteger} parse={toInteger}
                      floatingLabelText="Benefit Response Days"/>
              </div>
              <div className="col-lg-5">
                <CalendarTypes name="calendarTypeBenefitDays"/>
              </div>
            </div>
          </div>
        </div>

        <br/>
        <div className="row">
          <div className="col-lg-4">
            <div className="row">
              <div className="col-lg-6">
                <BillFrequency name="feeBillFrequency" floatingLabelText="* Fee Bill Frequency"/>
              </div>
              <div className="col-lg-6">
                <MonthAsTextFormField name="feeMonth" floatingLabelText="* Fee Month"/>
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="row">
              <div className="col-lg-7">
                <Field name="minimumDaysToPay" fullWidth component={TextField} validate={isInteger} parse={toInteger}
                      floatingLabelText="* Minimum Days To Pay"/>
              </div>
              <div className="col-lg-5">
                <CalendarTypes name="calendarTypeMinimumDays"/>
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            <Field fullWidth name="minimumPremiumYears" component={TextField} validate={isInteger} parse={toInteger}
                  floatingLabelText="Minimum Premium Years"/>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-4">
            <FeeDailyRateFormula name="feeDailyRateFormula"/>
          </div>
        </div>
        <br/>
        <div className="row">
          <div className="col-lg-4">
            <MarketValueEvent name="marketValueEvent"/>
          </div>
          <div className="col-lg-4">
            <Field fullWidth textareaStyle={styles.textAreaFont} name="marketValueComments" component={TextField}
                  floatingLabelText="Market Value Comments (Max: 255 Chars)"
                  multiLine rowsMax={3} validate={MaxLength(255)}/>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-3" style={styles.toggleDivStyle}>
            <YesNo
              name="cashBufferReplenishment"
              label="Cash Buffer Replenishment"
              onChange={cashBufferReplenishmentOnChange}
            />
          </div>
          <div className="col-lg-2 col-lg-offset-1">
            {cashBufferReplenishment && <RateField fullWidth name="replenishmentMinimum" rateFormat="0.00" floatingLabelText="Minimum (%)"/>}
            {!cashBufferReplenishment && <RateField disabled={true} fullWidth value='0' rateFormat="0.00" floatingLabelText="Minimum (%)"/>}
          </div>
          <div className="col-lg-2">
            {cashBufferReplenishment && <RateField fullWidth name="replenishmentMaximum" rateFormat="0.00" floatingLabelText="Maximum (%)"/>}
            {!cashBufferReplenishment && <RateField disabled={true} fullWidth value={0} rateFormat="0.00" floatingLabelText="Maximum (%)"/>}
          </div>
          <div className="col-lg-3" style={styles.toggleDivStyle}>
            <YesNo
              name="piSweepAllowed"
              label="P & I Sweep Allowed"
            />
          </div>
        </div>
      </Card>
    </ContractBaseInfoForm>
  )
}

ContractBaseInfo.displayName = 'ContractBaseInfoForm'

ContractBaseInfo.propTypes = {
  planAnniversaryMonthChanged: T.func.isRequired,
  planAnniversaryDayChanged: T.func.isRequired,
  cashBufferReplenishmentOnChange: T.func.isRequired,
  cashBufferReplenishment: T.bool
}

const actions = {
  planAnniversaryMonthChanged,
  planAnniversaryDayChanged,
  cashBufferReplenishmentOnChange: (e, value) => (dispatch) => {
    dispatch(change(BASE_INFO, 'replenishmentMinimum', 0))
    dispatch(change(BASE_INFO, 'replenishmentMaximum', 0))
  }
}

const mapStatesToProps = ({form, contracts: {contract}}) => ({
  contract,
  cashBufferReplenishment: _.get(form, 'baseInfo.values.cashBufferReplenishment')
})

export default connect(mapStatesToProps, actions)(ContractBaseInfo)
