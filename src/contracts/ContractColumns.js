import React from 'react'
import { dateFormat } from '../components/grid'
import { Link } from 'react-router'

export const ContractColumns = [
  {
    field: 'id',
    customProps: {
      hidden: true,
      isKey: true
    }
  },
  {
    field: 'contractNumber',
    sort: true,
    label: 'Contract #',
    renderer: (cell, row) => { // eslint-disable-line react/display-name
      return (
        <Link to={`/contracts/${row.contractId}/specifications`}>
          {cell}
        </Link>
      )
    },
    customProps: {
      width: '15%'
    }
  },
  {
    field: 'shortPlanName',
    sort: true,
    label: 'Plan Name',
    customProps: {
      width: '25%'
    }
  },
  {
    field: 'effectiveDate',
    sort: true,
    label: 'Effective Date',
    renderer: dateFormat,
    customProps: {
      width: '10%'
    }
  },
  {
    field: 'investmentManager',
    sort: true,
    label: 'Investment Manager',
    customProps: {
      width: '30%'
    }
  },
  {
    field: 'contractType',
    sort: true,
    label: 'Contract Type',
    customProps: {
      width: '15%'
    }
  },
  {
      field: 'status',
      sort: true,
      label: 'Status',
      customProps: {
          width: '15%'
      }
  }
]
