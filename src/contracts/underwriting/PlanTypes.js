/* eslint react/prop-types: off */
import React from 'react'
import _ from 'lodash'
import { PropTypes as T } from 'prop-types'
import { change } from 'redux-form'
import { connect } from 'react-redux'
import { PLAN_TYPE } from '../ContractForms'
import createContractForm from '../CreateContractForm'
import style from '../../styles/styles'
import Divider from 'material-ui/Divider'
import RateField from '../RateField'

export const mapFormProps = ({contracts: {contract}}) => ({
  initialValues: _(contract)
  .pick(contract, PlanTypeProps.map(({name}) => name))
  .transform((accum, item, key) => { accum[key] = item == null ? 0 : item; return true }, {})
  .value()
})

export const validate = (values, props) => {
  if (!_.isEqual(values, props.initialValues) && calcTotal(values) !== 100) {
    const message = 'The total must add up to 100%'
    const obj = _.transform(PlanTypeProps, (res, val) => { res[val.name] = message }, {})
    return obj
  }
  return {}
}

const PlanTypesForm = createContractForm(PLAN_TYPE, mapFormProps, validate)

const PlanTypeProps = [
  {
    name: 'plan401ARate',
    label: '401(a)'
  },
  {
    name: 'plan401KRate',
    label: '401(k)'
  },
  {
    name: 'plan403BRate',
    label: '403(b)'
  },
  {
    name: 'plan457Rate',
    label: '457'
  },
  {
    name: 'plan501CRate',
    label: '501(c)(9)'
  },
  {
    name: 'plan529Rate',
    label: '529 College Savings'
  },
  {
    name: 'planTaftRate',
    label: 'Taft-Hartley'
  }
]

export const calcTotal = (values) => values ? PlanTypeProps.reduce((total, planType) => {
  const val = isNaN(values[planType.name]) ? 0 : values[planType.name]
  return total + Number(val)
}, 0) : 0

const PlanTypes = (props) => {
  const total = calcTotal(props.values)
  return (
    <PlanTypesForm>
      {PlanTypeProps.map(({name, label}, index) => (
        <div key={index} className="row">
          <div className="col-md-6 col-md-offset-3">
            <RateField name={name} floatingLabelText={label} rateFormat="0.00" />
          </div>
        </div>
      ))}
      <Divider/>
      <p></p>
      <div className="row">
        <div className={`col-md-6 col-md-offset-3 ${total !== 100 && 'fontError'}`}>
          <h1>Total:&nbsp;<strong>{total}%</strong></h1>
          <p></p>
        </div>
      </div>
    </PlanTypesForm>
  )
}

PlanTypes.displayName = 'PlanTypes'

PlanTypes.propTypes = {
  values: T.obj
}

PlanTypeProps.forEach(({name}) => {
  PlanTypes.propTypes[name] = T.number
})

const mapStateToProps = ({form}) => ({
  values: _.transform(_.get(form, 'planType.values'), (accum, item, key) => { accum[key] = item == null ? 0 : item }, {})
})

export default connect(mapStateToProps)(PlanTypes)
