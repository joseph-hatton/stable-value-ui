import React from 'react'
import {Checkbox} from 'redux-form-material-ui'
import { Field } from 'redux-form'
import RateField from '../RateField'
import { PropTypes as T } from 'prop-types'
import styles from '../../styles/styles'
import YesNoContingent from '../../components/forms/YesNoContingent'

const show = (c) => c.switch && c.equityWash && c.equityWashContingent

class EquityWash extends React.Component {
    render () {
        let {children} = this.props
        children = children.reduce((obj, item) => {
            obj[item.type] = item.props
            return obj
        }, {})

        return !show(children) ? (<span />) : (
            <div className="row">
                <div className="col-lg-4" style={styles.toggleDivStyle}>
                    <Field fullWidth component={Checkbox} {...children.switch} />
                </div>
                <div className="col-lg-4">
                    <YesNoContingent {...children.equityWash} floatingLabelText='Equity Wash'/>
                </div>
                <div className="col-lg-4">
                    <RateField rateFormat="0.00" {...children.equityWashContingent} floatingLabelText='Contingent Equity Wash (%)'/>
                </div>
            </div>
        )
    }
}

EquityWash.propTypes = {
    children: T.node.isRequired
}

export default EquityWash
