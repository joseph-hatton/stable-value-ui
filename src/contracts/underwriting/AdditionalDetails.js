import React from 'react'
import _ from 'lodash'
import { PropTypes as T } from 'prop-types'
import { Field, change } from 'redux-form'
import { connect } from 'react-redux'
import RateField from '../RateField'
import { TextField, Checkbox } from 'redux-form-material-ui'
import { Card, CardTitle, CardText } from 'material-ui/Card'
import Slider from 'material-ui/Slider'
import { Required, FormatCurrency, toNumber, MaxLength, isInteger, toInteger } from '../../components/forms/Validators'
import { ADDITIONAL_DETAIL } from '../ContractForms'
import createContractForm from '../CreateContractForm'
import YesNo from '../../components/forms/YesNo'
import styles from '../../styles/styles'
import {
  Sectors,
  SPRatings,
  FitchRatings,
  MoodyRatings,
  OverallRatings,
  AdviceServiceRemedy
} from '../ContractRefDataFields'
import YesNoContingent from '../../components/forms/YesNoContingent'
import AuditableContractField from '../edithistory/AuditableContractField'
import EquityWash from './EquityWash'

export const mapFormProps = ({contracts: {contract}}) => ({
  initialValues: contract
})

const fundActiveInactiveWidth = 120

const style = {
  fundActiveInactive: {
    width: fundActiveInactiveWidth
  },
  fundActiveInactiveSlider: {
    width: `Calc(100% - ${fundActiveInactiveWidth * 2}px)`
  }
}

const AdditionalDetailsForm = createContractForm(ADDITIONAL_DETAIL, mapFormProps)

const CONTINGENT = 'CONTINGENT'

const tradeLimits = (adviceServiceRemedy) => !adviceServiceRemedy || adviceServiceRemedy === 'MV_EVENT_MODEL_METHODOLOGY_CHANGE'

const AdditionalDetails = ({brokerageWindowChanged, moneyMarketChanged, otherChanged, brokerageWindowChecked, moneyMarketChecked, otherChecked, brokerageEquityWash, moneyMarketEquityWash, otherEquityWash, brokerageEquityWashChanged, moneyMarketEquityWashChanged, otherEquityWashChanged, adviceService, adviceServiceRemedy, adviceServiceChanged, fundActiveRateChanged, fundActiveRate}) => {
  return (
    <AdditionalDetailsForm>
      <Card>
        <div>
          <Card>
            <CardText>
              <div className="row">
                <CardTitle className="card-title col-lg-12">Demographics:</CardTitle>
              </div>
              <div className="row">
                <div className="col-lg-6">
                  <AuditableContractField formId={ADDITIONAL_DETAIL}>
                    <div fullWidth className="row" floatingLabelText="Fund Active %" name="fundActiveRate">
                      <div style={style.fundActiveInactive} className="text-center"><u><strong>Fund Active</strong></u><br/>{fundActiveRate}%</div>
                      <div style={style.fundActiveInactiveSlider}>
                        <Slider min={0} max={100} step={1} onChange={fundActiveRateChanged} value={fundActiveRate} />
                      </div>
                      <div style={style.fundActiveInactive} className="text-center"><u><strong>Fund Inactive</strong></u><br/>{100 - fundActiveRate}%</div>
                    </div>
                  </AuditableContractField>
                </div>
                <div className="col-lg-5 col-lg-offset-1">
                  <AuditableContractField formId={ADDITIONAL_DETAIL}>
                    <Field fullWidth name="fundSeniorRate" component={TextField} floatingLabelText="Age 55+ %"/>
                  </AuditableContractField>
                </div>
              </div>
            </CardText>
          </Card>
          <p></p>
          <Card>
            <CardText>
              <div className="row">
                <CardTitle className="card-title col-lg-12">Plan Sponsor:</CardTitle>
              </div>
              <div className="row">
                <div className="col-lg-3">
                  <AuditableContractField formId={ADDITIONAL_DETAIL}>
                    <SPRatings name="spRating" floatingLabelText="S&P Rating"/>
                  </AuditableContractField>
                </div>
                <div className="col-lg-3">
                  <AuditableContractField formId={ADDITIONAL_DETAIL}>
                    <FitchRatings name="fitchRating" floatingLabelText="Fitch Rating"/>
                  </AuditableContractField>
                </div>
                <div className="col-lg-3">
                  <AuditableContractField formId={ADDITIONAL_DETAIL}>
                    <MoodyRatings name="moodyRating" floatingLabelText="Moody Rating"/>
                  </AuditableContractField>
                </div>
                <div className="col-lg-3">
                  <AuditableContractField formId={ADDITIONAL_DETAIL}>
                    <OverallRatings name="overallRating" floatingLabelText="Overall Rating"/>
                  </AuditableContractField>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12">
                  <Sectors name="sector"/>
                </div>
              </div>
            </CardText>
          </Card>
          <p></p>
          <Card>
            <CardText>
              <div className="row">
                <CardTitle className="card-title col-lg-12">Competing Fund:</CardTitle>
              </div>
              <EquityWash>
                <switch name='brokerageWindowSw' onChange={brokerageWindowChanged} label='Brokerage Window' />
                <equityWash name='brokerageEquityWash' onChange={brokerageEquityWashChanged} disabled={!brokerageWindowChecked} />
                <equityWashContingent name='brokerageEquityWashRate' disabled={brokerageEquityWash !== CONTINGENT} />
              </EquityWash>
              <EquityWash>
                <switch name='moneyMarketSw' onChange={moneyMarketChanged} label='Money Market'/>
                <equityWash name='moneyMarketEquityWash' onChange={moneyMarketEquityWashChanged} disabled={!moneyMarketChecked}/>
                <equityWashContingent name='moneyMarketEquityWashRate' disabled={moneyMarketEquityWash !== CONTINGENT}/>
              </EquityWash>
              <EquityWash>
                <switch name='otherSw' onChange={otherChanged} label='Other'/>
                <equityWash name='otherEquityWash' onChange={otherEquityWashChanged} disabled={!otherChecked}/>
                <equityWashContingent name='otherEquityWashRate' disabled={otherEquityWash !== CONTINGENT}/>
              </EquityWash>
            </CardText>
          </Card>
          <p></p>
          <Card>
            <CardText>
              <div className="row">
                <CardTitle className="card-title col-lg-4">Plan Design:</CardTitle>
                <div className="col-lg-3" style={styles.toggleDivStyle}>
                  <YesNo name="adviceService" label="Advice Service" onChange={adviceServiceChanged}/>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4">
                  <Field fullWidth name="adviceServiceProvider" disabled={!adviceService} component={TextField} floatingLabelText="Advice Service Provider"/>
                </div>
                <div className="col-lg-4">
                  <AdviceServiceRemedy name="adviceServiceRemedy" disabled={!adviceService} floatingLabelText="Advice Service Remedy"/>
                </div>
                <div className="col-lg-4">
                  <RateField rateFormat="0.0" name="tradeLimits" disabled={tradeLimits(adviceServiceRemedy)} floatingLabelText="* Trade Limits (%)" validate={!tradeLimits(adviceServiceRemedy) && Required}/>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-6">
                  <AuditableContractField formId={ADDITIONAL_DETAIL}>
                    <Field fullWidth name="fundLevelUtilization" component={TextField} floatingLabelText="Fund Level Utilization"/>
                  </AuditableContractField>
                </div>
                <div className="col-lg-6">
                  <AuditableContractField formId={ADDITIONAL_DETAIL}>
                    <Field fullWidth name="planLevelUtilization" component={TextField} floatingLabelText="Plan Level Utilization"/>
                  </AuditableContractField>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12">
                  <Field fullWidth name="featuresToMonitor" component={TextField} floatingLabelText="Plan Design Features to Monitor (Max: 255 Chars)" multiLine rows={2} validate={MaxLength(255)}/>
                </div>
              </div>
            </CardText>
          </Card>
        </div>
      </Card>
    </AdditionalDetailsForm>
  )
}

AdditionalDetails.displayName = 'AdditionalDetails'

AdditionalDetails.propTypes = {
  contract: T.object,
  brokerageWindowChanged: T.func.isRequired,
  moneyMarketChanged: T.func.isRequired,
  otherChanged: T.func.isRequired,
  brokerageWindowChecked: T.boolean,
  moneyMarketChecked: T.boolean,
  otherChecked: T.boolean,
  brokerageEquityWash: T.string,
  moneyMarketEquityWash: T.string,
  otherEquityWash: T.string,
  brokerageEquityWashChanged: T.func.isRequired,
  moneyMarketEquityWashChanged: T.func.isRequired,
  otherEquityWashChanged: T.func.isRequired,
  adviceService: T.boolean,
  adviceServiceRemedy: T.string,
  adviceServiceChanged: T.func.isRequired,
  fundActiveRateChanged: T.func.isRequired,
  fundActiveRate: T.number
}

const actions = {
  brokerageWindowChanged: (event, brokerage) => (dispatch, getState) => {
    const {contracts: {contract = {}}} = getState()
    if (brokerage) {
      dispatch(change(ADDITIONAL_DETAIL, 'brokerageEquityWash', contract.brokerageEquityWash))
    } else {
      dispatch(change(ADDITIONAL_DETAIL, 'brokerageEquityWash', null))
    }
  },
  brokerageEquityWashChanged: (event, ew) => (dispatch, getState) => {
    if (ew !== CONTINGENT) {
      dispatch(change(ADDITIONAL_DETAIL, 'brokerageEquityWashRate', 0))
    }
  },
  moneyMarketChanged: (event, moneyMarket) => (dispatch, getState) => {
    const {contracts: {contract = {}}} = getState()
    if (moneyMarket) {
      dispatch(change(ADDITIONAL_DETAIL, 'moneyMarketEquityWash', contract.moneyMarketEquityWash))
    } else {
      dispatch(change(ADDITIONAL_DETAIL, 'moneyMarketEquityWash', null))
    }
  },
  moneyMarketEquityWashChanged: (event, ew) => (dispatch, getState) => {
    if (ew !== CONTINGENT) {
      dispatch(change(ADDITIONAL_DETAIL, 'moneyMarketEquityWashRate', 0))
    }
  },
  otherChanged: (event, other) => (dispatch, getState) => {
    const {contracts: {contract = {}}} = getState()
    if (other) {
      dispatch(change(ADDITIONAL_DETAIL, 'otherEquityWash', contract.otherEquityWash))
    } else {
      dispatch(change(ADDITIONAL_DETAIL, 'otherEquityWash', null))
    }
  },
  otherEquityWashChanged: (event, ew) => (dispatch, getState) => {
    if (ew !== CONTINGENT) {
      dispatch(change(ADDITIONAL_DETAIL, 'otherEquityWashRate', 0))
    }
  },
  adviceServiceChanged: (event, adviceService) => (dispatch, getState) => {
    if (!adviceService) {
      dispatch(change(ADDITIONAL_DETAIL, 'adviceServiceProvider', null))
      dispatch(change(ADDITIONAL_DETAIL, 'adviceServiceRemedy', null))
      dispatch(change(ADDITIONAL_DETAIL, 'tradeLimits', null))
    }
  },
  fundActiveRateChanged: (event, fundActiveRate) => (dispatch, getState) => {
    dispatch(change(ADDITIONAL_DETAIL, 'fundActiveRate', fundActiveRate))
  }
}

const mapStatesToProps = ({form}) => ({
  brokerageWindowChecked: _.get(form, 'additionalDetail.values.brokerageWindowSw'),
  moneyMarketChecked: _.get(form, 'additionalDetail.values.moneyMarketSw'),
  otherChecked: _.get(form, 'additionalDetail.values.otherSw'),
  brokerageEquityWash: _.get(form, 'additionalDetail.values.brokerageEquityWash'),
  moneyMarketEquityWash: _.get(form, 'additionalDetail.values.moneyMarketEquityWash'),
  otherEquityWash: _.get(form, 'additionalDetail.values.otherEquityWash'),
  adviceService: _.get(form, 'additionalDetail.values.adviceService'),
  adviceServiceRemedy: _.get(form, 'additionalDetail.values.adviceServiceRemedy'),
  fundActiveRate: _.get(form, 'additionalDetail.values.fundActiveRate')
})

export default connect(mapStatesToProps, actions)(AdditionalDetails)
