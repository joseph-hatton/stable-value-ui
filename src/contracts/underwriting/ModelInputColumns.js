import React from 'react'
import store from '../../store/store'
import {dateFormat} from '../../components/utilities/Formatters'
import {selectModelInputRow} from '../ContractActions'
import {currencyFormat} from '../../components/grid'
import {openDialog} from '../../components/redux-dialog/redux-dialog'
import {DeleteButtonModelInput} from './DeleteButtonModelInput'
import {MultiActionMenuLink} from '../../components/common/MultiActionMenu'

const commonProps = {
  customProps: {
    textAlign: 'center',
    width: 'Calc((100% - 148px) / (6 * 1.25))',
    paddingLeft: 0
  }
}

export const ModelInputColumns = [
  {
    field: 'underwritingId',
    customProps: {
      hidden: true,
      isKey: true
    }
  },
  {
    field: 'effectiveDate',
    sort: true,
    label: 'Date',
    renderer: (cell, row) => <MultiActionMenuLink dialogName='modelInputsMenu' cell={dateFormat(cell)} row={row} selections={(row) => { // eslint-disable-line react/display-name
      store.dispatch(selectModelInputRow(row))
    }} />,
    customProps: {
      ...(commonProps.customProps),
      width: 100
    }
  },
  {
    field: 'contributionRate',
    sort: true,
    label: 'Contribution %',
    renderer: currencyFormat,
    ...commonProps
  },
  {
    field: 'withdrawalRate',
    sort: true,
    label: 'Withdrawal %',
    renderer: currencyFormat,
    ...commonProps
  },
  {
    field: 'withdrawalVolatilityRate',
    sort: true,
    label: 'Volatility %',
    renderer: currencyFormat,
    ...commonProps
  },
  {
    field: 'extTerminationRate',
    sort: true,
    label: 'Extended Termination Trigger %',
    renderer: currencyFormat,
    customProps: {
      ...(commonProps.customProps),
      width: '22%'
    }
  },
  {
    field: 'bufferLevelRate',
    sort: true,
    label: 'Buffer Level %',
    renderer: currencyFormat,
    ...commonProps
  },
  {
    field: 'maximumWithdrawalRate',
    sort: true,
    label: 'Max Net Withdrawal %',
    renderer: currencyFormat,
    customProps: {
      ...(commonProps.customProps),
      width: '17%'
    }
  }
]
