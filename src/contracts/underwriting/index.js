import ModelInputs from './ModelInputs'
import StableValueFunds from './StableValueFunds'
import AdditionalDetails from './AdditionalDetails'
import PlanTypes from './PlanTypes'

const UnderwritingTabs = [
  {
    label: 'Model Inputs',
    url: 'model-inputs',
    ContractTab: ModelInputs,
    loadDynamically: true
  },
  {
    label: 'Stable Value Fund',
    url: 'stable-value-fund',
    ContractTab: StableValueFunds,
    loadDynamically: true
  },
  {
    label: 'Additional Details',
    url: 'additional-details',
    ContractTab: AdditionalDetails,
    loadDynamically: false
  },
  {
    label: 'Plan Types',
    url: 'plan-types',
    ContractTab: PlanTypes,
    loadDynamically: false
  }
]

export default UnderwritingTabs
