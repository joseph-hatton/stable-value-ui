import React from 'react'
import {PropTypes as T} from 'prop-types'
import _ from 'lodash'
import {connect} from 'react-redux'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import { blue500, red500 } from 'material-ui/styles/colors'
import {loadModelInputs, saveModelInput, clearModelInputEditForm, selectModelInputRow, deleteModelInputRow} from '../ContractActions'
import {Grid, CreateGridConfig} from '../../components/grid'
import {ModelInputColumns} from './ModelInputColumns'
import AddEditModelInput from './AddEditModelInput'
import {openDialog, closeDialog, openConfirmationAlert} from '../../components/redux-dialog/redux-dialog'
import {isEditable} from '../ContractForms'
import WatchListAlert from '../../watchLists/watchListAlert/WatchListAlert'
import {checkWatchList} from '../../watchLists/watchListAlert/WatchListAlertActions'
import MultiActionMenu from '../../components/common/MultiActionMenu'
import { Menu, MenuItem } from 'material-ui/Menu'

const config = {
  showHeaderToolbar: false,
  paginate: false,
  defaultSortName: 'effectiveDate',
  defaultSortOrder: 'desc'
}

class ModelInputs extends React.Component {
  componentDidMount () {
    const {contract, loadModelInputs, modelInputs} = this.props
    const {contractId} = contract
    if (contractId && (_.isEmpty(modelInputs) || _.first(modelInputs).contractId !== contractId)) {
      loadModelInputs(contractId)
    }
  }

  open = (row) => {
    const {openDialog, selectModelInputRow} = this.props
    selectModelInputRow(row)
    openDialog('modelInput')
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.modelInputSaved) {
      this.props.closeDialog('modelInput')
      this.props.clearModelInputEditForm()
    }
  }

  render () {
    const {contract, modelInput, modelInputs, openDialog, closeDialog, clearModelInputEditForm, checkWatchList, openConfirmationAlert, deleteModelInputRow} = this.props
    const options = CreateGridConfig(modelInputs, config)
    return (
      <div>
        {!_.isEmpty(modelInputs) && (
          <Grid id="ModelInputs" options={options} data={modelInputs} columns={ModelInputColumns}
                onRowDoubleClick={this.open}/>)}
        <AddEditModelInput id="ModelInputs-AddEditModelInput" onClose={() => {
          closeDialog('modelInput')
          clearModelInputEditForm()
        }}/>
        <FloatingActionButton id="ModelInputs-AddButton" backgroundColor={red500} mini={false} className="floating-add-button"
                              disabled={!isEditable(contract)} onClick={() => {
          openDialog('modelInput')
          checkWatchList(contract.contractId)
        }}>
          <ContentAdd/>
        </FloatingActionButton>
        <WatchListAlert/>
        <MultiActionMenu dialogName='modelInputsMenu'>
          <Menu>
            <MenuItem primaryText="View/Edit Model Input" leftIcon={<EditIcon color={blue500}/>} onClick={() => {
              closeDialog('modelInputsMenu')
              openDialog('modelInput')
            }} />
            <MenuItem primaryText="Delete Model Input" leftIcon={<DeleteIcon color={red500}/>} onClick={() => {
              closeDialog('modelInputsMenu')
              openConfirmationAlert({
                message: 'Are you sure you want to delete this Model Input?',
                onOk: () => deleteModelInputRow(modelInput)
              })
            }} />
          </Menu>
        </MultiActionMenu>
      </div>
    )
  }
}

ModelInputs.propTypes = {
  contract: T.object,
  modelInput: T.object,
  modelInputs: T.array,
  loadModelInputs: T.func.isRequired,
  openDialog: T.func.isRequired,
  closeDialog: T.func.isRequired,
  modelInputSaved: T.bool,
  clearModelInputEditForm: T.func.isRequired,
  checkWatchList: T.func.isRequired,
  openConfirmationAlert: T.func.isRequired,
  deleteModelInputRow: T.func.isRequired,
  selectModelInputRow: T.func.isRequired
}

const actions = {
  loadModelInputs,
  saveModelInput,
  openDialog,
  closeDialog,
  clearModelInputEditForm,
  checkWatchList,
  selectModelInputRow,
  openConfirmationAlert,
  deleteModelInputRow
}

const mapStateToProps = ({contracts: {modelInput, modelInputs, contract, modelInputSaved}}) => ({
  contract,
  modelInput,
  modelInputs,
  modelInputSaved
})

export default connect(mapStateToProps, actions)(ModelInputs)
