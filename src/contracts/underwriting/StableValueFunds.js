import React from 'react'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import { blue500, red500 } from 'material-ui/styles/colors'
import {
  loadStableValueFunds,
  saveStableValueFund,
  clearStableValueFundEditForm,
  selectStableValueFundRow,
  deleteStableValueFundRow
} from '../ContractActions'
import {Grid, CreateGridConfig} from '../../components/grid'
import {StableValueFundColumns} from './StableValueFundColumns'
import AddEditStableValueFund from './AddEditStableValueFund'
import {openDialog, closeDialog, openConfirmationAlert} from '../../components/redux-dialog/redux-dialog'
import {IntlProvider} from 'react-intl'
import {isEditable} from '../ContractForms'
import MultiActionMenu from '../../components/common/MultiActionMenu'
import { Menu, MenuItem } from 'material-ui/Menu'

const config = {
  showHeaderToolbar: false,
  paginate: false
}

class StableValueFunds extends React.Component {
  componentDidMount () {
    const {contract: {contractId}, loadStableValueFunds} = this.props
    if (contractId) {
      loadStableValueFunds(contractId)
    }
  }

  open = (row) => {
    const {openDialog, selectStableValueFundRow} = this.props
    selectStableValueFundRow(row)
    openDialog('stableValueFund')
  }

  render () {
    const {stableValueFunds, openDialog, closeDialog, clearStableValueFundEditForm, contract, openConfirmationAlert, deleteStableValueFundRow, stableValueFund} = this.props
    const options = CreateGridConfig(stableValueFunds, config)
    return (
      <div>
        <IntlProvider>
          <Grid sortBy="effectiveDate" sortDirection="desc" id="StableValueFunds" options={options} data={stableValueFunds} columns={StableValueFundColumns}
                onRowDoubleClick={this.open}/>
        </IntlProvider>
        <AddEditStableValueFund id="StableValueFunds-AddEdit" onClose={() => {
          closeDialog('stableValueFund')
          clearStableValueFundEditForm()
        }}/>
        <FloatingActionButton id="StableValueFunds-AddButton" backgroundColor={red500} mini={false} className="floating-add-button"
                              disabled={!isEditable(contract)} onClick={() => openDialog('stableValueFund')}>
          <ContentAdd/>
        </FloatingActionButton>
        <MultiActionMenu dialogName='stableValueFundMenu'>
          <Menu>
            <MenuItem primaryText="View/Edit Stable Value Fund" leftIcon={<EditIcon color={blue500}/>} onClick={() => {
              closeDialog('stableValueFundMenu')
              openDialog('stableValueFund')
            }} />
            <MenuItem primaryText="Delete Stable Value Fund" leftIcon={<DeleteIcon color={red500}/>} onClick={() => {
              closeDialog('stableValueFundMenu')
              openConfirmationAlert({
                message: 'Are you sure you want to delete this Stable Value Fund?',
                onOk: () => deleteStableValueFundRow(stableValueFund)
              })
            }} />
          </Menu>
        </MultiActionMenu>
      </div>
    )
  }
}

StableValueFunds.propTypes = {
  contract: T.object,
  stableValueFund: T.object,
  stableValueFunds: T.array,
  loadStableValueFunds: T.func.isRequired,
  openDialog: T.func.isRequired,
  closeDialog: T.func.isRequired,
  clearStableValueFundEditForm: T.func.isRequired,
  deleteStableValueFundRow: T.func.isRequired,
  openConfirmationAlert: T.func.isRequired,
  selectStableValueFundRow: T.func.isRequired
}

const actions = {
  loadStableValueFunds,
  saveStableValueFund,
  openDialog,
  closeDialog,
  clearStableValueFundEditForm,
  selectStableValueFundRow,
  deleteStableValueFundRow,
  openConfirmationAlert
}

const mapStateToProps = ({contracts: {stableValueFund, stableValueFunds, contract}}) => ({
  contract,
  stableValueFunds,
  stableValueFund
})

export default connect(mapStateToProps, actions)(StableValueFunds)
