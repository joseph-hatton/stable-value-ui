import React from 'react'
import { PropTypes as T } from 'prop-types'
import style from '../../styles/styles'
import Slider from 'material-ui/Slider'

const SliderWithLabel = (props) => (
    <div className="row">
        <div className={`col-lg-2 text-right ${props.textClassName}`} style={style.sliderLabelStyle}>
            <h1><strong>{props.label}</strong>&nbsp;&nbsp;&nbsp;&nbsp;{props.value} %</h1>
        </div>
        <div className="col-lg-10">
            <Slider {...props} min={0} step={1} />
        </div>
    </div>
)

SliderWithLabel.propTypes = {
    onChange: T.func.isRequired,
    label: T.string.isRequired,
    value: T.number.isRequired,
    max: T.number.isRequired,
    style: T.object,
    textClassName: T.string
}

export default SliderWithLabel
