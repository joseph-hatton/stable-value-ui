import React from 'react'
import {PropTypes as T} from 'prop-types'
import _ from 'lodash'
import moment from 'moment'
import {connect} from 'react-redux'
import RaisedButton from 'material-ui/RaisedButton'
import {Field, reduxForm} from 'redux-form'
import {TextField, DatePicker} from 'redux-form-material-ui'
import RateField from '../RateField'
import ReduxDialog from '../../components/redux-dialog'
import {MaxLength, isInteger, toInteger, Required} from '../../components/forms/Validators'
import AppStyles from '../../styles/styles'
import {toUtc, dateFormat} from '../../components/utilities/Formatters'
import {saveModelInput} from '../ContractActions'
import {orange500} from 'material-ui/styles/colors'
import {isEditable} from '../ContractForms'

export const formId = 'AddEditModelInputForm'

const AddEditModelInput = (props) => {
  const {onClose, initialValues, contract, handleSubmit, saveModelInput} = props
  const underwritingId = initialValues ? initialValues.underwritingId : undefined
  const title = underwritingId ? 'Edit Model Input' : 'Add Model Input'
  const disablePooledPlanFields = contract.contractType !== 'POOLED_PLAN'
  const validatePooledPlanFields = disablePooledPlanFields ? {} : {validate: Required}
  const validatePutLengthMonths = disablePooledPlanFields ? {validate: isInteger} : {validate: [Required, isInteger]}
  const actions = [
    <RaisedButton
      key="Save"
      label="Save"
      primary={true}
      style={{marginRight: 20}}
      type="submit"
      form="AddEditModelInputForm"
      disabled={!isEditable(contract)}
    />,
    <RaisedButton
      key="Cancel"
      label="Cancel"
      onClick={onClose}
    />
  ]
  return (
    <ReduxDialog
      dialogName="modelInput"
      title={title}
      actions={actions}
      modal={true}
      autoScrollBodyContent={true}
    >
      <form id={formId} onSubmit={handleSubmit(saveModelInput)}>
        <div className="row">
          <div className="col-lg-6">
            <Field validate={Required} fullWidth
                   name="effectiveDate" autoOk
                   formatDate={dateFormat}
                   format={toUtc} component={DatePicker} firstDayOfWeek={0}
                   floatingLabelText="* Date"/>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <RateField validate={Required} fullWidth name="contributionRate" floatingLabelText="* Contribution %"
                       rateFormat="0.00"/>
          </div>
          <div className="col-lg-6">
            <RateField validate={Required} fullWidth name="withdrawalRate" floatingLabelText="* Withdrawal %"
                       rateFormat="0.00"/>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <RateField validate={Required} fullWidth name="withdrawalVolatilityRate" floatingLabelText="* Volatility %"
                       rateFormat="0.00"/>
          </div>
          <div className="col-lg-6">
            <RateField validate={Required} fullWidth name="extTerminationRate" floatingLabelText="* Termination Rate %"
                       rateFormat="0.00"/>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <RateField validate={Required} fullWidth name="bufferLevelRate" floatingLabelText="* Buffer Level %"
                       rateFormat="0.00"/>
          </div>
          <div className="col-lg-6">
            <RateField validate={Required} fullWidth name="maximumWithdrawalRate"
                       floatingLabelText="* Max Net Withdrawal %"
                       rateFormat="0.00"/>
          </div>
        </div>
        <br/>
        {!disablePooledPlanFields && <div>
          <div className="row">
            <div className="col-lg-6">
              <RateField {...validatePooledPlanFields} disabled={disablePooledPlanFields} fullWidth
                         name="putQueueWithdrawalRate" floatingLabelText="Put Queue Withdrawal %"
                         rateFormat="0.00"/>
            </div>
            <div className="col-lg-6">
              <Field {...validatePutLengthMonths} disabled={disablePooledPlanFields} name="putLengthMonths"
                     fullWidth component={TextField} parse={toInteger}
                     floatingLabelText="Put Length Months"/>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-6">
              <RateField {...validatePooledPlanFields} disabled={disablePooledPlanFields} fullWidth
                         name="maximumPutQueueRate" floatingLabelText="Max Put Queue %" rateFormat="0.00"/>
            </div>
          </div>
        </div>
        }
        <div className="row">
          <div className="col-lg-12">
            <Field fullWidth textareaStyle={AppStyles.textAreaFont} name="underwritingComments" component={TextField}
                   validate={MaxLength(255)} floatingLabelText="Mode Inputs Comments (Max: 255 Chars)"
                   multiLine rowsMax={3}/>

          </div>
        </div>
      </form>
    </ReduxDialog>
  )
}

AddEditModelInput.propTypes = {
  onClose: T.func.isRequired,
  handleSubmit: T.func,
  saveModelInput: T.func.isRequired,
  initialValues: T.object,
  contract: T.object
}

const validateEffectiveDate = (values, props) => {
  const effectiveDate = moment(values.effectiveDate)
  if (!_.isEmpty(props.modelInputs)) {
    const modelWithSameEffectiveDate = _.find(props.modelInputs,
      modelInput =>
        modelInput.underwritingId !== values.underwritingId && effectiveDate.isSame(modelInput.effectiveDate, 'day'))
    return modelWithSameEffectiveDate ? {effectiveDate: 'Effective Date is already used'} : null
  }
}

const AddEditModelInputForm = reduxForm({
  form: formId,
  validate: validateEffectiveDate
})(AddEditModelInput)

const mapStateToProps = ({contracts: {modelInput, modelInputs, contract}}) => {
  const props = {
    contract,
    initialValues: modelInput,
    modelInputs
  }
  return props
}

export default connect(mapStateToProps, {saveModelInput})(AddEditModelInputForm)
