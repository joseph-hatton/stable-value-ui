import React from 'react'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import { red500 } from 'material-ui/styles/colors'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import { openConfirmationAlert, openSnackbar } from '../../components/redux-dialog'
import { deleteModelInputRow } from '../ContractActions'

export const DeleteModelInput = ({
                                  row: ModelInput,
                                  openConfirmationAlert,
                                  confirmationAlert,
                                  openSnackbar,
                                   deleteModelInputRow
                                }) => (
  <div>
    <a href="" id='DeleteModelInputId' onClick={(e) => {
      e.preventDefault()
        openConfirmationAlert({
          message: 'Are you sure you want to delete this Model Input?',
          onOk: () => deleteModelInputRow(ModelInput)
        })
      }
    } ><DeleteIcon color={red500}/></a>
  </div>
)

DeleteModelInput.propTypes = {
  row: T.object,
  confirmationAlert: T.func,
  openConfirmationAlert: T.func.isRequired,
  openSnackbar: T.func.isRequired,
  deleteModelInputRow: T.func.isRequired
}

const actions = {
  openConfirmationAlert,
  openSnackbar,
  deleteModelInputRow
}

export const DeleteButtonModelInput = connect(null, actions)(DeleteModelInput)
