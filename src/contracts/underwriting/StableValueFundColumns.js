import React from 'react'
import {selectStableValueFundRow, deleteStableValueFundRow} from '../ContractActions'
import {dateFormat, percentFormat, currencyFormat, formatDecimal} from '../../components/utilities/Formatters'
import {openDialog, openConfirmationAlert} from '../../components/redux-dialog/redux-dialog'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import {red500} from 'material-ui/styles/colors'
import store from '../../store/store'
import {MultiActionMenuLink} from '../../components/common/MultiActionMenu'

const commonProps = {
  customProps: {
    textAlign: 'right',
    width: 'Calc((100% - 148px) / 6)',
    paddingLeft: 0
  }
}

export const StableValueFundColumns = [
  {
    field: 'stableValueFundId',
    customProps: {
      hidden: true,
      isKey: true
    }
  },
  {
    field: 'effectiveDate',
    sort: true,
    label: 'Date',
    renderer: (cell, row) => <MultiActionMenuLink dialogName='stableValueFundMenu' cell={dateFormat(cell)} row={row} selections={(row) => { // eslint-disable-line react/display-name
      store.dispatch(selectStableValueFundRow(row))
    }} />,
    customProps: {
      ...(commonProps.customProps),
      textAlign: 'center',
      width: 100
    }
  },
  {
    field: 'svBookValue',
    sort: true,
    label: 'SV Book Value',
    renderer: currencyFormat,
    ...commonProps
  },
  {
    field: 'svMarketValue',
    sort: true,
    label: 'SV Market Value',
    renderer: currencyFormat,
    ...commonProps
  },
  {
    field: 'calculatedMvBv',
    sort: true,
    label: 'SV MV/BV Ratio',
    renderer: calculatedMvBv => { // eslint-disable-line react/display-name
      return percentFormat(calculatedMvBv)
    },
    ...commonProps
  },
  {
    field: 'svDuration',
    sort: true,
    label: 'SV Duration',
    renderer: (cell) => formatDecimal(cell),
    ...commonProps
  },
  {
    field: 'svCreditedRate',
    sort: true,
    label: 'SV Credited Rate',
    renderer: (cell) => percentFormat(cell / 100), // eslint-disable-line react/display-name
    ...commonProps
  },
  {
    field: 'svCashBuffer',
    sort: true,
    label: 'SV Cash Buffer',
    renderer: (cell) => percentFormat(cell / 100), // eslint-disable-line react/display-name
    ...commonProps
  }
]

const extraColumns = []
