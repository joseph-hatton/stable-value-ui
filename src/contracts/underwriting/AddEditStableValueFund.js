import React from 'react'
import { PropTypes as T } from 'prop-types'
import _ from 'lodash'
import moment from 'moment'
import { connect } from 'react-redux'
import RaisedButton from 'material-ui/RaisedButton'
import { Field, reduxForm } from 'redux-form'
import { TextField, DatePicker } from 'redux-form-material-ui'
import RateField from '../RateField'
import ReduxDialog from '../../components/redux-dialog'
import { Required } from '../../components/forms/Validators'
import { toUtc, dateFormat, percentFormat } from '../../components/utilities/Formatters'
import { saveStableValueFund } from '../ContractActions'
import { isEditable } from '../ContractForms'
const formId = 'AddEditStableValueFundForm'

const AddEditStableValueFund = (props) => {
  const {onClose, initialValues, handleSubmit, saveStableValueFund, contract, calculateMvBv} = props
  const underwritingId = initialValues ? initialValues.underwritingId : undefined
  const title = underwritingId ? 'Edit Stable Value Fund' : 'Add Stable Value Fund'
  const actions = [
    <RaisedButton
      key="Save"
      label="Save"
      primary={true}
      style={{marginRight: 20}}
      type="submit"
      form="AddEditStableValueFundForm"
      disabled={!isEditable(contract)}
    />,
    <RaisedButton
      key="Cancel"
      label="Cancel"
      onClick={onClose}
    />
  ]
  return (
    <ReduxDialog
      dialogName="stableValueFund"
      title={title}
      actions={actions}
      modal={true}
      autoScrollBodyContent={true}
    >
      <form id={formId} onSubmit={handleSubmit(saveStableValueFund)}>
        <div className="row">
          <div className="col-lg-6">
            <Field validate={Required} fullWidth
                   name="effectiveDate" formatDate={dateFormat}
                   format={toUtc} component={DatePicker} firstDayOfWeek={0}
                   floatingLabelText="* Date" autoOk/>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <RateField validate={Required} fullWidth name="svBookValue" floatingLabelText="* SV Book Value"
                       rateFormat="0,0.00" />
          </div>
          <div className="col-lg-6">
            <RateField validate={Required} fullWidth name="svMarketValue" floatingLabelText="* SV Market Value"
                       rateFormat="0,0.00"/>
          </div>
          <div className="col-lg-6">
            <Field fullWidth floatingLabelText=" SV MV/BV" component={TextField}
                   format={(value) => calculateMvBv + '%'}
            />
            </div>
          <div className="col-lg-6">
            <RateField validate={Required} fullWidth name="svDuration" floatingLabelText="* SV Duration"
                       rateFormat="0.00"/>
          </div>
          <div className="col-lg-6">
            <RateField validate={Required} fullWidth name="svCreditedRate" floatingLabelText="* SV Credited Rate"
                       rateFormat="0.00"/>
          </div>
          <div className="col-lg-6">
            <RateField validate={Required} fullWidth name="svCashBuffer" floatingLabelText="* SV Cash Buffer"
                       rateFormat="0.00"/>
          </div>
        </div>
        <br/>
      </form>
    </ReduxDialog>
  )
}

AddEditStableValueFund.propTypes = {
  onClose: T.func.isRequired,
  handleSubmit: T.func,
  saveStableValueFund: T.func.isRequired,
  initialValues: T.object,
  contract: T.object,
  calculateMvBv: T.number
}

const validateEffectiveDate = (values, props) => {
  const effectiveDate = moment(values.effectiveDate)
  if (!_.isEmpty(props.stableValueFunds)) {
    const modelWithSameEffectiveDate = _.find(props.stableValueFunds,
      stableValueFund =>
        stableValueFund.stableValueFundId !== values.stableValueFundId && effectiveDate.isSame(stableValueFund.effectiveDate, 'day'))
    return modelWithSameEffectiveDate ? {effectiveDate: 'Effective Date is already used'} : null
  }
}

const AddEditStableValueFundForm = reduxForm({
  form: formId,
  validate: validateEffectiveDate
})(AddEditStableValueFund)

const mapCalculateMVBV = (valuesMV) => {
  return _.isNumber(valuesMV.svMarketValue) && valuesMV.svBookValue !== 0 ?
    ((valuesMV.svMarketValue / valuesMV.svBookValue) * 100).toFixed(2) : '00.00'
}

const mapStateToProps = ({ contracts: {stableValueFund, stableValueFunds, contract}, form }) => {
  const props = {
    contract,
    initialValues: stableValueFund,
    stableValueFunds,
    calculateMvBv: form.AddEditStableValueFundForm && form.AddEditStableValueFundForm.values ?
      mapCalculateMVBV(form.AddEditStableValueFundForm.values) : '00.00'
  }

  return props
}

export default connect(mapStateToProps, {saveStableValueFund})(AddEditStableValueFundForm)
