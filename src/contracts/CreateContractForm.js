import React from 'react'
import { PropTypes as T } from 'prop-types'
import _ from 'lodash'
import { connect } from 'react-redux'
import { reduxForm as createReduxForm } from 'redux-form'
import { saveContract, setCurrentContractForm } from './ContractActions'

const createContractForm = (formId, mapFormProps, validate) => {
  class ContractForm extends React.Component {
    componentDidMount () {
      this.props.setCurrentContractForm(formId)
    }

    render () {
      const {handleSubmit, saveContract, contractNumber} = this.props
      console.log('this.props', {props: this.props})
      return (
        <div>
          {
            (contractNumber || formId === 'specifications') && (
              <form id={formId} onSubmit={handleSubmit(saveContract)}>
                {this.props.children}
              </form>
            )
          }
        </div>
      )
    }
  }

  ContractForm.propTypes = {
    children: T.element,
    handleSubmit: T.func.isRequired,
    setCurrentContractForm: T.func.isRequired,
    saveContract: T.func.isRequired,
    initialValues: T.object,
    contractNumber: T.string
  }

  const ReduxForm = createReduxForm({form: formId, destroyOnUnmount: false, enableReinitialize: false, validate})(ContractForm)

  const mapStateToProps = (state) => ({
    ...mapFormProps(state),
    contractNumber: _.get(state, 'contracts.contract.contractNumber')
  })

  return connect(mapStateToProps, {setCurrentContractForm, saveContract})(ReduxForm)
}

export default createContractForm
