import React from 'react'
import _ from 'lodash'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import {change, Field} from 'redux-form'
import numeral from 'numeral'
import {TextField} from 'redux-form-material-ui'

const DEFAULT_CURRENCY_FORMAT = '0,0.00'

const createFormatter = (format) => (value) => {
  if (_.isNumber(value)) {
    return numeral(value).format(format || DEFAULT_CURRENCY_FORMAT)
  }
  return value
}

const normalize = (acceptNegativeNumber = false) => (value, previousValue) => {
  const valueWithoutCommas = value.replace(/,/g, '')
  return (acceptNegativeNumber ? valueWithoutCommas !== '-' : true) && isNaN(valueWithoutCommas)
    ? (isNaN(previousValue) ? 0 : previousValue)
    : valueWithoutCommas
}

const onBlur = (normalizeRate, blurCallback) => (event, rate) => {
  normalizeRate(event, rate, blurCallback)
}

const NUMBER_FIELD_PROPS = ['numberFormat', 'normalizeRate', 'acceptNegativeNumber', 'blur']

const NumberField = (props) => (
  <Field fullWidth {..._.omit(props, NUMBER_FIELD_PROPS)} component={TextField}
         format={createFormatter(props.numberFormat)}
         normalize={normalize(props.acceptNegativeNumber)}
         onBlur={onBlur(props.normalizeRate, props.blur)}
  />
)

NumberField.propTypes = {
  normalizeRate: T.func.isRequired,
  numberFormat: T.string,
  blur: T.func,
  acceptNegativeNumber: T.bool
}

export const toNumber = (text) => text ? numeral(text).value() : null

const actions = {
  normalizeRate: (event, rate, childBlur) => (dispatch) => {
    const {currentTarget} = event
    const {form: {id}, name} = currentTarget
    const newRate = toNumber(rate)
    setTimeout(() => dispatch(change(id, name, newRate)))
    event.target.value = newRate
    childBlur && childBlur(event)
  }
}

export default connect(null, actions)(NumberField)
