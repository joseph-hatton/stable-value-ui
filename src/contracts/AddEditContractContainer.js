import React from 'react'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import { loadContract, initializeContract } from './ContractActions'
import ContractSpecifications from './ContractSpecifications'

class AddEditContractContainer extends React.Component {
  componentDidMount () {
    const {contract, params, loadContract, initializeContract} = this.props

    if (params.contractId && !contract) {
      loadContract(params.contractId)
    }

    if (!params.contractId) {
      initializeContract()
    }
  }

  render () {
    return (<ContractSpecifications/>)
  }
}

AddEditContractContainer.propTypes = {
  contract: T.object,
  params: T.object,
  loadContract: T.func.isRequired,
  initializeContract: T.func.isRequired
}

export const mapStateToProps = ({contracts: {contract}}) => ({
  contract
})

const actions = {loadContract, initializeContract}

export default connect(mapStateToProps, actions)(AddEditContractContainer)
