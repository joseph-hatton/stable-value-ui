import _ from 'lodash'

const REQUIRED_CONTACT_TYPES = ['PLAN_SPONSOR', 'INVESTMENT_MANAGER']

const NOTICE_STATUSES = ['receiveInvoice', 'receiveScheduleA', 'receiveStatement']

const requiredAll = (values) => _.every(values)

const TRUE = () => true

const validators = {
  contacts: (contract, contractContacts) => {
    const contactTypes = _(contractContacts).map('contactType').uniq().value()

    const hasRequiredTypes = _.difference(REQUIRED_CONTACT_TYPES, contactTypes).length === 0
    const hasContactsToReceiveAllNotices = _(NOTICE_STATUSES)
      .map(notice => _(contractContacts).map(notice).some())
      .every()

    return hasRequiredTypes && hasContactsToReceiveAllNotices
  },
  specifications: ({
                     premiumRate, effectiveDate, shortPlanName, jurisdiction, assignmentState, managerId, owner,
                     initialCreditingRate
                   }) => {
    return requiredAll([
      premiumRate,
      effectiveDate,
      shortPlanName,
      jurisdiction,
      assignmentState,
      managerId,
      owner,
      initialCreditingRate
    ])
  },
  underwriting: TRUE,
  'risk-scorecard': TRUE,
  info: ({legalFormName, benefitResponseType, feeBillFrequency, minimumDaysToPay, feeMonth}) => {
    return requiredAll([
      legalFormName,
      benefitResponseType,
      feeBillFrequency,
      minimumDaysToPay,
      feeMonth
    ])
  },
  schedule: ({
               creditingRateCalcMethod, calculationMethodDefinition, creditingRateRoundDecimals,
               creditingRateInputBasis, creditingRateFrequency, creditingRateType,
               creditingRateMonth, creditingRateFirstReset,
               corrOption, corrOptionCalcMethod, corrOptionRollingMonths
             }) => {
    const hasRequiredFields = requiredAll([creditingRateCalcMethod,
      calculationMethodDefinition,
      creditingRateRoundDecimals,
      creditingRateInputBasis,
      creditingRateFrequency,
      creditingRateType])

    return hasRequiredFields
      && (creditingRateFrequency === 'DAILY' ? requiredAll([creditingRateFirstReset]) : requiredAll([creditingRateMonth]))
      && (corrOption ? requiredAll([corrOptionCalcMethod]) : true)
      && (corrOptionCalcMethod === 'ROLLING' ? requiredAll([corrOptionRollingMonths]) : true)
  }
}

const isReadyToSubmit = (contract, contractContacts) => {
  const validationResult = _(validators)
    .map((validatorFunc, contractSection) => ({
      [contractSection]: validatorFunc(contract, contractContacts)
    })).reduce(_.assign, {})

  if (contract.status !== 'PENDING') {
    validationResult.isReadyToSubmit = false
  } else {
    validationResult.isPending = true
    validationResult.isReadyToSubmit = _(validationResult).values().every()
  }
  return validationResult
}

const contractStateTransitionMap = {
  PENDING: (contract, lifeCycleStatus) => {
    return [
      {label: 'Submit for Approval', path: 'submit', disabled: !lifeCycleStatus.readyToSubmit.isReadyToSubmit},
      {label: 'Inactivate', path: 'inactivate'}
    ]
  },
  SUBMITTED: () => {
    return [
      {label: 'Reject', path: 'reject'},
      {label: 'Release', path: 'approve'}
    ]
  },
  ACTIVE: () => {
    return [
      {label: 'Terminate', path: 'terminate'}
    ]
  },
  TERMINATED: () => {
    return []
  },
  INACTIVE: () => {
    return [
      {label: 'Reactivate', path: 'reactivate'}
    ]
  },
  PENDING_TERMINATION: () => {
    return [
      {label: 'Terminate', path: ''},
      {label: 'Reactivate', path: ''}
    ]
  }
}

const validateContractLifeCycleStatus = (contract, contractContacts) => {
  if (contract && contract.status) {
    const lifeCycleStatus = {
      readyToSubmit: isReadyToSubmit(contract, contractContacts)
    }
    lifeCycleStatus.stateTransitions = contractStateTransitionMap[contract.status](contract, lifeCycleStatus)
    return lifeCycleStatus
  }
  return {}
}

export default validateContractLifeCycleStatus
