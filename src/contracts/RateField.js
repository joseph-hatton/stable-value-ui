import React from 'react'
import _ from 'lodash'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import { change, Field } from 'redux-form'
import numeral from 'numeral'
import { TextField } from 'redux-form-material-ui'

const DEFAULT_FORMAT = '0.0'

const formatNumber = (format) => (value) => {
    if (_.isNumber(value)) {
        return numeral(value).format(format)
    }
    return value
}

const normalize = (norm) => (value, previousValue) => {
  if (norm) {
    return norm(value, previousValue)
  } else {
    const valueWithoutCommas = value.replace(/,/g, '')
    return isNaN(valueWithoutCommas) ? (isNaN(previousValue) ? 0 : previousValue) : valueWithoutCommas
  }
}

const onBlur = (normalizeRate, blurCallback) => (event, rate) => {
  normalizeRate(event, rate, blurCallback)
}

const RateField = (props) => (
  <Field fullWidth {...props} component={TextField}
         format={formatNumber(props.rateFormat || DEFAULT_FORMAT)}
         normalize={normalize(props.normalize)}
         onBlur={onBlur(props.normalizeRate, props.blur)}
  />
)

RateField.propTypes = {
  normalizeRate: T.func.isRequired,
  rateFormat: T.string,
  blur: T.func,
  normalize: T.func
}

export const toNumber = (text) => text ? numeral(text).value() : null

const actions = {
  normalizeRate: (event, rate, childBlur) => (dispatch) => {
    const {currentTarget} = event
    const {form: {id}, name} = currentTarget
    const newRate = toNumber(rate)
    setTimeout(() => dispatch(change(id, name, newRate)))
    event.target.value = newRate
    childBlur && childBlur(event)
  }
}

export default connect(null, actions)(RateField)
