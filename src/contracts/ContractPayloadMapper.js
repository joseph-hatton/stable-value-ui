import _ from 'lodash'
import moment from 'moment'
import {
  SPECIFICATIONS,
  CONTRACT_FORM_PROPS,
  CONTRACT_AUDITABLE_PROPS,
  SCHEDULE_FUNDING,
  getEffectiveDateProp,
  isEffectiveDateProp,
  getAuditableProp
} from './ContractForms'
import { dateFormat } from '../components/utilities/Formatters'

const newContractDefaults = {
  status: 'PENDING',
  statedMaturityDate: '2999-01-01T00:00:00.000Z',
  issueCompany: 'RGA'
}

export const createNewContractSavePayload = (forms) => {
  const contractSpecifications = forms[SPECIFICATIONS].values
  return {
    ...newContractDefaults,
    ..._.pick(contractSpecifications, CONTRACT_FORM_PROPS[SPECIFICATIONS].props)
  }
}

export const mapDerivativeTypes = (scheduleFormValues, derivativeTypes) => {
  return _(derivativeTypes)
    .map(derivativeType => scheduleFormValues[derivativeType.derivativeTypeId] ? derivativeType : undefined)
    .compact()
    .value()
}

export const mergeAuditableFieldChanges = (contract, edit) => {
  const {editHistories, contractId} = contract
  const editHistoriesByEffectiveDate = _(editHistories).keyBy(edit => dateFormat(edit.effectiveDate)).cloneDeep()
  _.map(edit, (value, prop) => {
    if (isEffectiveDateProp(prop) && value != null) { // Changed Auditable Date must have a effective date
      const effectiveDateKey = dateFormat(value)
      const auditableProp = getAuditableProp(prop)
      let editForEffectiveDate = editHistoriesByEffectiveDate[effectiveDateKey]
      if (!editForEffectiveDate) {
        editForEffectiveDate = {
          effectiveDate: value,
          contractId
        }
        editHistoriesByEffectiveDate[effectiveDateKey] = editForEffectiveDate
      }
      editForEffectiveDate[auditableProp] = edit[auditableProp]
    }
  })
  return _.values(editHistoriesByEffectiveDate)
}

const withEffectiveDates = auditableProps =>
  _(auditableProps)
    .map(prop => [prop, getEffectiveDateProp(prop)])
    .flatten()
    .value()

export const createContractSavePayload = (contract, forms, derivativeTypes) => {
  const allFormProps = _(forms)
    .keys()
    .map(formId => {
        const allProps = CONTRACT_FORM_PROPS[formId] || []
        const formValues = forms[formId].values
        return {
          contractProps: _.pick(formValues, allProps.props),
          auditableProps: _.pick(formValues, withEffectiveDates(allProps.auditableProps))
        }
      }
    )
    .value()
  const allContractProps = _(allFormProps).map('contractProps').reduce(_.assign, {})

  const allAuditableProps = _.map(allFormProps, 'auditableProps')
  const edit = (allAuditableProps).reduce(_.assign, {})
  const editHistories = contract.status === 'PENDING' ? [edit] : mergeAuditableFieldChanges(contract, edit)
  const contractDerivativeTypes = forms[SCHEDULE_FUNDING]
    ? mapDerivativeTypes(forms[SCHEDULE_FUNDING].values, derivativeTypes) : contract.derivativeTypes
  let payload = {
    ...contract,
    ...allContractProps,
    editHistories,
    derivativeTypes: contractDerivativeTypes
  }
  payload = _.mapValues(payload, (item, key) => (item instanceof moment || item instanceof Date) ? moment(item).utc().format('YYYY-MM-DDTHH:mm:ss.SSS') + 'Z' : item)
  return payload
}

export const getCurrentAuditableContractValues = (editHistory) => {
  if (_.isEmpty(editHistory)) {
    return {}
  }

  const today = moment().utc()

  return _(CONTRACT_AUDITABLE_PROPS)
    .map(contractField => {
      const contractFieldHistory = _(editHistory).map(edit => (_.has(edit, contractField) ? {
        effectiveDate: moment(edit.effectiveDate),
        value: edit[contractField]
      } : null))
        .flatten()
        .compact()
        .filter(({value, effectiveDate}) => value != null)
        .sortBy(({effectiveDate}) => effectiveDate.valueOf() * -1)
        .value()

      if (_.isEmpty(contractFieldHistory)) {
        return null
      }

      const activeOrPastEffectiveValues
        = _.filter(contractFieldHistory, ({effectiveDate}) => effectiveDate.utc().isBefore(today))

      if (!_.isEmpty(activeOrPastEffectiveValues)) {
        return {[contractField]: activeOrPastEffectiveValues[0].value}
      } else {
        return {[contractField]: _.last(contractFieldHistory).value}
      }
    })
    .flatten()
    .reduce(_.assign, {})
}

export const removeEditHistory = (editHistories, effectiveDate, field) => {
  return editHistories.map(edit => {
    const copy = {...edit}
    if (moment(edit.effectiveDate).isSame(effectiveDate, 'day')) {
      copy[field] = null
    }
    return copy
  })
}

export const mapPlanTypeValues = ({plan401ARate, plan401KRate, plan403BRate, plan457Rate, plan501CRate, plan529Rate, planTaftRate}) => ({
  plan401ARate: plan401ARate || 0,
  plan401KRate: plan401KRate || 0,
  plan403BRate: plan403BRate || 0,
  plan457Rate: plan457Rate || 0,
  plan501CRate: plan501CRate || 0,
  plan529Rate: plan529Rate || 0,
  planTaftRate: planTaftRate || 0
})

export const mapContract = (contractPayload) => {
  const editHistories = _.sortBy(contractPayload.editHistories || [], ({effectiveDate}) => moment(effectiveDate).valueOf() * -1)
  const auditableContractFields = getCurrentAuditableContractValues(editHistories)
  const contract = {
    ...contractPayload,
    ...mapPlanTypeValues(contractPayload),
    ...auditableContractFields,
    editHistories
  }
  return contract
}
