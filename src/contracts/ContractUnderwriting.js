import React from 'react'
import { PropTypes as T } from 'prop-types'
import _ from 'lodash'
import { connect } from 'react-redux'
import { Tabs, Tab } from 'material-ui/Tabs'
import { Card } from 'material-ui/Card'
import { goToTab, setOrToggleListItem } from '../components/common/LeftDrawerActions'
import UnderwritingTabs from './underwriting'
import { browserHistory } from 'react-router'

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 900
  },

  tabContainer: {
    marginTop: 20
  },

  tabContentContainerStyle: {
    padding: 20
  },
  tabItemContainerStyle: {
    position: 'sticky',
    top: 113,
    zIndex: 2
  },
  mainTab: {
    backgroundColor: '#0062cc',
    fontWeight: 600
  }
}

class Underwriting extends React.Component {
  componentDidMount () {
    if (!this.props.underwriting) {
      this.props.setOrToggleListItem('underwriting', true)
    }
    const selectedTab = _.findIndex(UnderwritingTabs, ['label', this.props.selectedTab]) === -1 ? 'Model Inputs' : this.props.selectedTab
    this.props.goToTab(selectedTab)
  }
  componentWillUnmount () {
    if (this.props.underwriting) {
      this.props.setOrToggleListItem('underwriting', false)
    }
  }

  render () {
    const tabs = _.partition(UnderwritingTabs, tab => tab.loadDynamically)
    const dynamicTabs = tabs[0]
    const staticTabs = tabs[1]
    const indexOfLastTab = _.findIndex(UnderwritingTabs, tab => tab.label === this.props.selectedTab)
    return (
      <Card style={styles.tabContainer}>
        <Tabs className="UnderWritingTab" value={this.props.selectedTab}
              tabItemContainerStyle={styles.tabItemContainerStyle}
              contentContainerStyle={styles.tabContentContainerStyle}
              initialSelectedIndex={indexOfLastTab > -1 ? indexOfLastTab : 0}>
          {
            dynamicTabs.map(({label, ContractTab, url, loadDynamically}) => (
              <Tab key={label}
                   onActive={() => browserHistory.push(`/contracts/${this.props.contractId}/underwriting/${url}`)}
                   label={label} value={label}
                   style={styles.mainTab}>
                {this.props.selectedTab === label &&
                <ContractTab {...(_.omit(this.props, ['selectedTab', 'underwriting', 'contractId']))}/>}
              </Tab>
            ))
          }
          {
            staticTabs.map(({label, url, ContractTab}) => (
              <Tab key={label} label={label} value={label}
                   onActive={() => browserHistory.push(`/contracts/${this.props.contractId}/underwriting/${url}`)}
                   style={styles.mainTab}>
                <ContractTab {...(_.omit(this.props, ['selectedTab', 'underwriting', 'contractId']))}/>
              </Tab>
            ))
          }
        </Tabs>
      </Card>
    )
  }
}

Underwriting.propTypes = {
  selectedTab: T.string,
  goToTab: T.func.isRequired,
  underwriting: T.boolean,
  setOrToggleListItem: T.func.isRequired,
  contractId: T.number
}

const mapStateToProps = ({sideBar: { selectedTab, underwriting }, contracts: {contract: {contractId}}}) => ({
  selectedTab,
  underwriting,
  contractId
})

export default connect(mapStateToProps, {goToTab, setOrToggleListItem})(Underwriting)
