import React from 'react'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import {
  RaisedButton,
  Card,
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui'
import IconButton from 'material-ui/IconButton'
import DeleteIcon from 'material-ui/svg-icons/content/clear'
import { red500, green900 } from 'material-ui/styles/colors'
import ReduxDialog, {
  closeDialog,
  openConfirmationAlert,
  ConfirmationAlert
} from '../../components/redux-dialog'
import { dateFormat } from '../../components/utilities/Formatters'
import { deleteContractEditHistory, removeEditHistory } from '../ContractActions'

const headerStyle = {fontSize: 15, color: '#818182'}
const rowStyle = {fontSize: 15}
const activeEditStyle = {color: green900, fontSize: 15, fontWeight: 'bold'}

const CONFIRM_DELETE_DIALOG = 'deleteEditHistoryConfirmation'
const FIELD_HISTORY_DIALOG = 'contractFieldHistory'

class FieldHistory extends React.Component {
  deleteEditHistory (effectiveDate, field) {
    const {formId, deleteContractEditHistory, closeDialog, openConfirmationAlert, removeEditHistory} = this.props

    openConfirmationAlert({
      message: 'Do you really want to delete this record?',
      onOk: () => {
        deleteContractEditHistory(effectiveDate, field)
        closeDialog(FIELD_HISTORY_DIALOG)
        removeEditHistory(formId, field)
      }
    }, CONFIRM_DELETE_DIALOG)
  }

  render () {
    const {field, label, editHistory, closeDialog} = this.props

    const actions = [
      <RaisedButton
        key="Close"
        label="Close"
        onClick={() => {
          closeDialog(FIELD_HISTORY_DIALOG)
        }}
      />
    ]
    const title = `${label} Edit History`
    return (
      <ReduxDialog
        dialogName={FIELD_HISTORY_DIALOG}
        title={title}
        actions={actions}
        modal={false}
        autoScrollBodyContent={true}
      >
        <Card containerStyle={{padding: 20, marginTop: 10}}>
          <Table selectable={false}>
            <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
              <TableRow>
                <TableHeaderColumn style={headerStyle}>Value</TableHeaderColumn>
                <TableHeaderColumn style={headerStyle}>Effective Date</TableHeaderColumn>
                <TableHeaderColumn style={headerStyle}>Delete</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {
                editHistory.map(({value, effectiveDate, future, active}, index) => (
                  <TableRow key={index}>
                    <TableRowColumn style={rowStyle}>{value}</TableRowColumn>
                    <TableRowColumn style={rowStyle}>{dateFormat(effectiveDate)}</TableRowColumn>
                    <TableRowColumn>
                      {
                        future
                          ? (
                            <IconButton onClick={() => {
                              this.deleteEditHistory(effectiveDate, field)
                            }}>
                              <DeleteIcon color={red500}/>
                            </IconButton>
                          )
                          : (active ? (<span style={activeEditStyle}> Active </span>) : (<span></span>))
                      }
                    </TableRowColumn>
                  </TableRow>
                ))
              }
            </TableBody>
          </Table>
        </Card>
        <ConfirmationAlert dialogName={CONFIRM_DELETE_DIALOG}/>
      </ReduxDialog>
    )
  }
}

FieldHistory.propTypes = {
  formId: T.string,
  label: T.string,
  field: T.string,
  editHistory: T.array,
  closeDialog: T.func.isRequired,
  deleteContractEditHistory: T.func.isRequired,
  openConfirmationAlert: T.func.isRequired,
  removeEditHistory: T.func.isRequired
}

const mapStateToProps = ({contracts: {selectedAuditableField = {}}}) => {
  const {formId, field, label, editHistory = []} = selectedAuditableField
  return {
    formId,
    field,
    label,
    editHistory
  }
}

const actions = {
  closeDialog,
  deleteContractEditHistory,
  openConfirmationAlert,
  removeEditHistory
}

export default connect(mapStateToProps, actions)(FieldHistory)
