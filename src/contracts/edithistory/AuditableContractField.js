import React from 'react'
import { PropTypes as T } from 'prop-types'
import _ from 'lodash'
import { connect } from 'react-redux'
import { Field } from 'redux-form'
import { DatePicker } from 'redux-form-material-ui'
import IconButton from 'material-ui/IconButton'
import HistoryIcon from 'material-ui/svg-icons/action/date-range'
import ClearIcon from 'material-ui/svg-icons/content/reply'
import { red500, blue500 } from 'material-ui/styles/colors'
import { hasEditHistory, selectAuditableField, clearAuditableFieldChange } from '../ContractActions'
import { openDialog, openSnackbar } from '../../components/redux-dialog'
import { Required } from '../../components/forms/Validators'
import AppStyles from '../../styles/styles'
import { getEffectiveDateProp } from '../ContractForms'
import { dateFormat } from '../../components/utilities/Formatters'

const iconContainerStyle = {
  alignItems: 'flex-end',
  display: 'flex',
  justifyContent: 'center',
  marginBottom: 15,
  padding: '0 15px',
  width: 50
}

const ERROR = 'AuditableContractField must have a Redux Form Field or derived type as a child with name and floatingLabelText'

const ifExists = value => (value !== undefined && value !== null && value !== '') && value

class AuditableContractField extends React.Component {
  showEditHistory (formId, fieldName, label) {
    const {openDialog, selectAuditableField} = this.props
    selectAuditableField(formId, fieldName, label)
    openDialog('contractFieldHistory')
  }

  isFieldChanged (field) {
    const {contract, formId, forms} = this.props
    if (!forms[formId]) {
      return false
    }
    // To compare undefined and null
    return ifExists(contract[field]) !== ifExists(forms[formId].values[field])
  }

  isSaved (field) {
    const {contract: {editHistories}, formId, forms} = this.props
    const newEffectiveDate = forms[formId].values[getEffectiveDateProp(field)]
    const newValue = forms[formId].values[field]
    const result = newEffectiveDate &&
      _.find(editHistories, (edit) => {
        const {effectiveDate} = edit
        return dateFormat(effectiveDate) === dateFormat(newEffectiveDate) && newValue === edit[field]
      })
    return result
  }

  render () {
    const {contract, children, formId, clearAuditableFieldChange, openSnackbar, startDate} = this.props
    const {props: {name, floatingLabelText}} = children
    if (!name || !floatingLabelText) {
      throw new Error(ERROR)
    }
    const effectiveDateFieldName = getEffectiveDateProp(name)
    const audited = hasEditHistory(contract)
    const changed = audited && this.isFieldChanged(name)
    const startDateObj = new Date(startDate)
    startDateObj.setDate(1)
    return (
      <div>
        {
          audited
            ? (
              <div className={changed ? 'auditable-field-changed' : ''}>
                <div className="row">
                  <div style={{width: 'Calc(100% - 50px)', paddingLeft: 15}}>
                    {children}
                  </div>
                  <div style={iconContainerStyle}>
                    <IconButton tooltip="Show Edit History" tooltipPosition="top-center"
                                tooltipStyles={AppStyles.tooltipStyles}
                                onClick={() => {
                                  this.showEditHistory(formId, name, floatingLabelText)
                                }}>
                      <HistoryIcon color={blue500}/>
                    </IconButton>
                  </div>
                </div>
                <div className="row" style={{display: changed ? '' : 'none'}}>
                  <div className="col-lg-10">
                    <Field autoOk fullWidth name={effectiveDateFieldName} component={DatePicker} firstDayOfWeek={0} formatDate={dateFormat}
                           floatingLabelText="* Effective Date" validate={changed && Required} minDate={startDateObj}/>
                  </div>
                  <div className="col-lg-2" style={iconContainerStyle}>
                    <IconButton tooltip="Undo Change" tooltipPosition="top-center"
                                tooltipStyles={AppStyles.tooltipStyles}
                                onClick={() => {
                                  if (!this.isSaved(name)) {
                                    clearAuditableFieldChange(formId, name)
                                  } else {
                                    openSnackbar({
                                      type: 'error',
                                      message: `Cannot undo saved value for ${floatingLabelText}`,
                                      autoHideDuration: 5000
                                    })
                                  }
                                }}>
                      <ClearIcon color={red500}/>
                    </IconButton>
                  </div>
                </div>
              </div>
            )
            : (
              <div className="row">
                <div className="col-lg-12">
                  {children}
                </div>
              </div>
            )
        }
      </div>
    )
  }
}

AuditableContractField.propTypes = {
  contract: T.object,
  children: T.element,
  formId: T.string.isRequired,
  forms: T.object,
  openDialog: T.func.isRequired,
  openSnackbar: T.func.isRequired,
  selectAuditableField: T.func.isRequired,
  clearAuditableFieldChange: T.func.isRequired,
  startDate: T.string
}

const mapStateToProps = ({contracts: {contract = {}}, form = {}, monthlyCycle: {current: {startDate}}}) => ({
  contract,
  forms: form,
  startDate
})

const actions = {
  openDialog,
  selectAuditableField,
  clearAuditableFieldChange,
  openSnackbar
}

export default connect(mapStateToProps, actions)(AuditableContractField)
