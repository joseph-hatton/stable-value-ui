import _ from 'lodash'

export const SPECIFICATIONS = 'specifications'
export const BASE_INFO = 'baseInfo'
export const SCHEDULE_FUNDING = 'scheduleFunding'
export const ADDITIONAL_DETAIL = 'additionalDetail'
export const PLAN_TYPE = 'planType'

const EDITABLE_STATUSES = ['ACTIVE', 'PENDING']

export function isEditable ({status}) {
  return EDITABLE_STATUSES.indexOf(status) > -1
}

const CONTRACT_FORMS = [SPECIFICATIONS, BASE_INFO, SCHEDULE_FUNDING, ADDITIONAL_DETAIL, PLAN_TYPE]

export const CONTRACT_FORM_PROPS = {
  [SPECIFICATIONS]: {
    props: [
      'contractType',
      'shortPlanName',
      'effectiveDate',
      'initialCoveredBookValue',
      'initialCoveredMarketValue',
      'initialCreditingRate',
      'cashBufferTargetLevel',
      'owner',
      'plans',
      'stableFundValue',
      'custodyAccounts',
      'premiumPayor',
      'managerFeePayor',
      'advisorFeePayor'
    ],
    auditableProps: [
      'jurisdiction',
      'assignmentState',
      'premiumRate',
      'managerFeeRate',
      'advisorFeeRate',
      'managerId'
    ]
  },
  [BASE_INFO]: {
    props: [
      'planAnniversary',
      'legalFormName',
      'benefitResponseType',
      'tierNumber',
      'benefitResponseDays',
      'calendarTypeBenefitDays',
      'benefitResponseNotification',
      'feeBillFrequency',
      'feeMonth',
      'minimumDaysToPay',
      'calendarTypeMinimumDays',
      'minimumPremiumYears',
      'feeDailyRateFormula',
      'marketValueEvent',
      'marketValueComments',
      'cashBufferReplenishment',
      'replenishmentMinimum',
      'replenishmentMaximum',
      'piSweepAllowed'
    ],
    auditableProps: [
      'legalFormName'
    ]
  },
  [ADDITIONAL_DETAIL]: {
    props: [
      'sector',
      'brokerageWindowSw',
      'brokerageEquityWash',
      'brokerageEquityWashRate',
      'moneyMarketSw',
      'moneyMarketEquityWash',
      'moneyMarketEquityWashRate',
      'otherSw',
      'otherEquityWash',
      'otherEquityWashRate',
      'adviceService',
      'adviceServiceProvider',
      'adviceServiceRemedy',
      'tradeLimits',
      'featuresToMonitor'
    ],
    auditableProps: [
      'fundActiveRate',
      'fundSeniorRate',
      'spRating',
      'fitchRating',
      'moodyRating',
      'overallRating',
      'fundLevelUtilization',
      'planLevelUtilization'
    ]
  },
  [PLAN_TYPE]: {
    props: [
      'total',
      'plan401ARate',
      'plan401KRate',
      'plan403BRate',
      'plan457Rate',
      'plan501CARate',
      'plan529Rate',
      'planTaftRate'
    ],
    auditableProps: []
  },
  [SCHEDULE_FUNDING]: {
    props: [
      'averageCreditQuality',
      'additionalDeposits',
      'depositLimit',
      'fundingComments',
      'calculationMethodDefinition',
      'minimumNetCreditingRate',
      'creditingRateRoundDecimals',
      'creditingRateInputBasis',
      'creditingRateFrequency',
      'creditingRateMonth',
      'creditingRateFirstReset',
      'creditingRateType',
      'netOfThirdPartyFees',
      'creditingRateComments',
      'corrOption',
      'corrOptionLimitsAnnual',
      'corrOptionLimitsLifetime',
      'corrOptionCalcMethod',
      'corrOptionRollingMonths',
      'withdrawalComments',
      'securitiesLendingAllowed',
      'repurchaseAgreementsAllowed',
      'downgradePercentage',
      'derivativeTypes',
      'extTerminationMatDtExt',
      'extTerminationConvDays',
      'calendarTypeTerminationDays',
      'daysToMakePaym',
      'calendarTypePaymentDays',
      'terminationComments',
      'securitiesLendingLimit',
      'repurchaseAgreementsLimit',
      'investmentGuidelinesComments',
      'creditingRateManagerFee',
      'creditingRateAdvisorFee',
      'defaultBucketPercentage'
    ],
    auditableProps: [
      'creditingRateCalcMethod',
      'accountDurationLimit'
    ]
  }
}

const listAllProps = (form) => {
  const allProps = CONTRACT_FORM_PROPS[form]
  return allProps.props.concat(allProps.auditableProps)
}

export const SCHEDULE_FUNDING_PROPS = listAllProps(SCHEDULE_FUNDING)

export const CONTRACT_AUDITABLE_PROPS = _(CONTRACT_FORM_PROPS)
  .values()
  .map('auditableProps')
  .flatten()
  .value()

export const CONTRACT_AUDITABLE_PROP_TO_EFFECTIVE_DATE =
  _(CONTRACT_AUDITABLE_PROPS)
    .map(prop => ({[prop]: `${prop}EffectiveDate`}))
    .reduce(_.assign, {})

export const EFFECTIVE_DATE_TO_CONTRACT_AUDITABLE_PROP = _.invert(CONTRACT_AUDITABLE_PROP_TO_EFFECTIVE_DATE)

export const getEffectiveDateProp = (prop) => CONTRACT_AUDITABLE_PROP_TO_EFFECTIVE_DATE[prop]

export const isEffectiveDateProp = (prop) => _.has(EFFECTIVE_DATE_TO_CONTRACT_AUDITABLE_PROP, prop)

export const getAuditableProp = (effectiveDateProp) => EFFECTIVE_DATE_TO_CONTRACT_AUDITABLE_PROP[effectiveDateProp]

export default CONTRACT_FORMS
