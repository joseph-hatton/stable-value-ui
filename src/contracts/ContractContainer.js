import React from 'react'
import _ from 'lodash'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { loadContract, clearEditContractState, hasEditHistory, askToNotSaveContract } from './ContractActions'
import { loadAllContacts } from '../contacts/ContactActions'
import ContractToolbar from './ContractToolbar'
import FieldHistory from './edithistory/FieldHistory'
import { setOrToggleListItem } from '../components/common/LeftDrawerActions'

class ContractContainer extends React.Component {
  componentDidMount () {
    const {params, loadContract, loadAllContacts, managers, setOrToggleListItem, router, route, askToNotSaveContract} = this.props

    router.setRouteLeaveHook(route, askToNotSaveContract(router))

    if (params.contractId) {
      loadContract(params.contractId)
    }

    if (_.isEmpty(managers)) {
      loadAllContacts()
    }

    setOrToggleListItem('contracts', true)
  }

  componentWillUnmount () {
    this.props.clearEditContractState()
  }

  componentWillReceiveProps (nextProps) {
    const {location: {pathname}, router} = this.props
    const contractId = nextProps.contract ? nextProps.contract.contractId : null

    if (contractId && pathname.indexOf(contractId.toString()) === -1) {
      router.push(`/contracts/${contractId}/specifications`)
    }
  }

  isContractHomeView (path) {
    return path.endsWith('specifications') || path.endsWith('contracts')
  }

  render () {
    const {contract, contracts, location: {pathname}} = this.props
    return (
      <div>
        {contract && (<ContractToolbar/>)}
        {(contract || this.isContractHomeView(pathname)) && this.props.children}
        {hasEditHistory(contract) && <FieldHistory/>}
      </div>
    )
  }
}

ContractContainer.displayName = 'ContractContainer'

ContractContainer.propTypes = {
  params: T.object,
  loadContract: T.func.isRequired,
  clearEditContractState: T.func.isRequired,
  children: T.element,
  contract: T.object,
  currentContractFormId: T.string,
  location: T.object,
  router: T.object,
  contracts: T.array,
  loadAllContacts: T.func.isRequired,
  setOrToggleListItem: T.func.isRequired,
  askToNotSaveContract: T.func.isRequired,
  canLeaveWithoutSaving: T.boolean,
  route: T.string,
  managers: T.array
}

const actions = {
  loadContract,
  clearEditContractState,
  loadAllContacts,
  setOrToggleListItem,
  askToNotSaveContract
}

export const mapStateToProps = ({contacts: {managers}, contracts: {canLeaveWithoutSaving, allContracts: {results}, contract, currentContractFormId}}) => {
  return {contract, currentContractFormId, contracts: results, managers, canLeaveWithoutSaving}
}

export default withRouter(connect(mapStateToProps, actions)(ContractContainer))
