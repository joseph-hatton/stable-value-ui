import React from 'react'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import { Card } from 'material-ui/Card'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import RaisedButton from 'material-ui/RaisedButton'
import { Link } from 'react-router'
import ContractActionMenu from './ContractActionMenu'
import {isEditable} from './ContractForms'
import {ReferenceDataTextRenderer} from '../components/renderers/ReferenceDataTextRenderer'

import { clearEditContractState } from './ContractActions'

import WatchListIcon from '../watchLists/watchListAlert/WatchListIcon'

const labelMargin = {marginRight: '5px'}

const formatContractHeader = (contract) => {
  if (contract && contract.contractNumber) {
    const {status, shortPlanName} = contract
    return (
      <span>
        <span style={labelMargin}>
          <ReferenceDataTextRenderer refDataField="contractStatuses" refDataValue={status}/>
        </span>
        <strong style={labelMargin}>{contract.contractNumber}</strong>
        <span>{shortPlanName ? `(${shortPlanName})` : ''}</span>
        <span style={{position: 'relative', top: '6px', left: '5px'}}><WatchListIcon contractId={contract.contractId} /></span>
      </span>)
  } else {
    return (<strong>New Contract</strong>)
  }
}

const style = {margin: 8}

const ContractToolbar = ({clearEditContractState, contract, currentContractFormId, isReadyToSubmit}) => (
  <Card style={{zIndex: 2, position: 'sticky', top: 57}}>
    <Toolbar style={{borderTop: 'solid 1px #e0e0e0', backgroundColor: '#f8f9fa'}}>
      <ToolbarGroup>
        {formatContractHeader(contract)}
      </ToolbarGroup>
      <ToolbarGroup style={{marginRight: '50px'}} lastChild={true}>
        <RaisedButton style={style} label="Save" type="submit" form={currentContractFormId} primary={true}
                      disabled={!isEditable(contract)}/>
        <Link to={`/contracts`} onClick={() => {
          clearEditContractState()
        }}>
          <RaisedButton style={style} label="Cancel"/>
        </Link>
        <ContractActionMenu/>
      </ToolbarGroup>
    </Toolbar>
  </Card>
)

ContractToolbar.displayName = 'ContractToolbar'

ContractToolbar.propTypes = {
  clearEditContractState: T.func.isRequired,
  contract: T.object,
  currentContractFormId: T.string,
  isReadyToSubmit: T.bool
}

const actions = {
  clearEditContractState
}

export const mapStateToProps = ({contracts: {contract, currentContractFormId, contractLifeCycleStatus = {}}}) => {
  const isReadyToSubmit = contractLifeCycleStatus.readyToSubmit && contractLifeCycleStatus.readyToSubmit.isReadyToSubmit
  return {contract, currentContractFormId, isReadyToSubmit}
}

export default connect(mapStateToProps, actions)(ContractToolbar)
