import React from 'react'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import { MenuItem } from 'material-ui'
import { red500 } from 'material-ui/styles/colors'
import IconMenu from 'material-ui/IconMenu'
import IconButton from 'material-ui/IconButton'
import SendIcon from 'material-ui/svg-icons/content/send'
import { updateContractStatus } from './ContractActions'

class ContractActionMenu extends React.Component {
  render () {
    const {stateTransitions, updateContractStatus} = this.props
    return (
      <div>
        {
          stateTransitions.length > 0 &&
          (<IconMenu anchorOrigin={{vertical: 'bottom', horizontal: 'left'}}
              iconStyle={{color: red500}}
              iconButtonElement={
                <IconButton touch={true}>
                  <SendIcon/>
                </IconButton>
              } >
            {
              stateTransitions.map(({label, disabled, path}) => (
                <MenuItem key={label} primaryText={label} disabled={disabled} onClick={() => {
                  updateContractStatus(path)
                }}/>
              ))
            }
          </IconMenu>
          )

        }
      </div>

    )
  }
}

ContractActionMenu.displayName = 'ContractActionMenu'

ContractActionMenu.propTypes = {
  contract: T.object,
  stateTransitions: T.array,
  updateContractStatus: T.func.isRequired
}

export const mapStateToProps = ({contracts: {contractLifeCycleStatus = {}}}) => {
  return {
    stateTransitions: contractLifeCycleStatus.stateTransitions || []
  }
}

export default connect(mapStateToProps, {updateContractStatus})(ContractActionMenu)
