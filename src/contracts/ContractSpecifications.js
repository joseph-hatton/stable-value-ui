import React from 'react'
import { PropTypes as T } from 'prop-types'
import { Field } from 'redux-form'
import { connect } from 'react-redux'
import { TextField, DatePicker } from 'redux-form-material-ui'
import { Card } from 'material-ui/Card'
import ContractTypes from '../components/forms/ContractTypesFormField'
import StatesFormField from '../components/forms/StatesFormField'
import PayerFormField from '../components/forms/PayerFormField'
import ManagersFormField from '../contacts/ManagersFormField'
import RateField from './RateField'
import NumberField from './NumberField'
import { Required } from '../components/forms/Validators'
import { toUtc, dateFormat } from '../components/utilities/Formatters'
import { SPECIFICATIONS } from './ContractForms'
import createContractForm from './CreateContractForm'
import { jurisdictionSelected } from './ContractActions'
import AuditableContractField from './edithistory/AuditableContractField'

export const mapFormProps = ({contracts: {contract}}) => ({
  initialValues: contract && {
    ...contract,
    effectiveDate: contract.effectiveDate ? toUtc(contract.effectiveDate) : null,
    statedMaturityDate: toUtc(contract.statedMaturityDate),
    issueCompany: contract.issueCompany || 'RGA'
  }
})

const ContractSpecificationsForm = createContractForm(SPECIFICATIONS, mapFormProps)

const textAreaDivStyle = {
  alignItems: 'flex-end',
  display: 'flex',
  justifyContent: 'center',
  fontSize: '10px'
}
const textAreaFont = {
  fontSize: '14px'
}

const sectionStyle = {
  padding: '10px 20px 10px 20px',
  marginTop: '20px'
}

const disableIfNotPending = (contract) => contract && contract.status !== 'PENDING'

const ContractSpecifications = ({jurisdictionSelected, contract}) => {
  return (
    <ContractSpecificationsForm>
      <Card style={sectionStyle}>

        <div>
          <div className="row">
            <div className="col-lg-4">
              <ContractTypes name="contractType" validate={Required} disabled={disableIfNotPending(contract)}/>
            </div>
            <div className="col-lg-4">
              <Field autoOk fullWidth name="effectiveDate" component={DatePicker} firstDayOfWeek={0} floatingLabelText="* Effective Date"
                     disabled={disableIfNotPending(contract)} formatDate={dateFormat}/>
            </div>
            <div className="col-lg-4">
              <Field fullWidth name="shortPlanName" component={TextField}
                     floatingLabelText="* Short Plan Name" validate={Required}/>
            </div>
          </div>
        </div>
        {
          contract && contract.contractNumber && (
            <div>
              <div className="row">
                <div className="col-lg-4">
                  <AuditableContractField formId={SPECIFICATIONS}>
                    <StatesFormField name="jurisdiction" floatingLabelText="* Jurisdiction"
                                     onChange={jurisdictionSelected}/>
                  </AuditableContractField>
                </div>
                <div className="col-lg-4">
                  <AuditableContractField formId={SPECIFICATIONS}>
                    <StatesFormField name="assignmentState" floatingLabelText="* Assignment State"/>
                  </AuditableContractField>
                </div>
                <div className="col-lg-4">
                  <Field autoOk fullWidth name="statedMaturityDate" component={DatePicker} firstDayOfWeek={0} floatingLabelText="* Stated Maturity Date"
                        formatDate={dateFormat} disabled/>
                </div>
              </div>
              <br/>
              <div className="row">
                <div className="col-lg-4">
                  <AuditableContractField formId={SPECIFICATIONS}>
                    <RateField fullWidth name="premiumRate" floatingLabelText="* Premium Rate (Bps)"/>
                  </AuditableContractField>
                </div>
                <div className="col-lg-4">
                  <AuditableContractField formId={SPECIFICATIONS}>
                    <RateField fullWidth name="managerFeeRate" floatingLabelText="Manager Rate (Bps)"/>
                  </AuditableContractField>
                </div>
                <div className="col-lg-4">
                  <AuditableContractField formId={SPECIFICATIONS}>
                    <RateField fullWidth name="advisorFeeRate" floatingLabelText="Advisor Fee Rate (Bps)"/>
                  </AuditableContractField>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4">
                  <NumberField fullWidth name="initialCoveredBookValue"
                                 floatingLabelText="Initial Covered Book Value"/>
                </div>
                <div className="col-lg-4">
                  <NumberField fullWidth name="initialCoveredMarketValue"
                                 floatingLabelText="Initial Covered Market Value"
                  />
                </div>
                <div className="col-lg-4">
                  <RateField fullWidth name="initialCreditingRate" floatingLabelText="* Initial Crediting Rate (%)"
                             rateFormat="0.000"/>
                </div>
                <div className="col-lg-4">
                  <RateField fullWidth name="cashBufferTargetLevel" floatingLabelText="Cash Buffer Target Level (%)"
                             rateFormat="0.00"/>
                </div>
              </div>
              <br/>
              <div className="row">
                <div className="col-lg-4">
                  <AuditableContractField formId={SPECIFICATIONS}>
                    <ManagersFormField name="managerId" floatingLabelText="* Manager"/>
                  </AuditableContractField>
                </div>
                <div className="col-lg-4">
                  <Field fullWidth name="custodyAccounts" component={TextField}
                         floatingLabelText="Custody Account(s)"/>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4">
                  <PayerFormField name="premiumPayor" floatingLabelText="Premium Payor (BPS)"/>
                </div>
                <div className="col-lg-4">
                  <PayerFormField name="managerFeePayor" floatingLabelText="Manager Fee Payor (BPS)"/>
                </div>
                <div className="col-lg-4">
                  <PayerFormField name="advisorFeePayor" floatingLabelText="Advisor Fee Payor (BPS)"/>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4" style={textAreaDivStyle}>
                  <Field textareaStyle={textAreaFont} fullWidth name="owner" component={TextField}
                         floatingLabelText="* Owner"
                         multiLine rowsMax={3}/>
                </div>
                <div className="col-lg-4" style={textAreaDivStyle}>
                  <Field textareaStyle={textAreaFont} className="text-area-font" fullWidth name="plans"
                         component={TextField}
                         floatingLabelText="Plan(s)"
                         multiLine rowsMax={3}/>
                </div>
                <div className="col-lg-4" style={textAreaDivStyle}>
                  <Field fullWidth name="stableFundValue" component={TextField} floatingLabelText="Stable Value Fund"
                         multiLine rowsMax={3}/>
                </div>
              </div>
            </div>
          )
        }

      </Card>
    </ContractSpecificationsForm>
  )
}

ContractSpecifications.displayName = 'ContractSpecifications'

ContractSpecifications.propTypes = {
  contract: T.object,
  jurisdictionSelected: T.func.isRequired
}

const mapStatesToProps = ({contracts: {contract}}) => ({contract})

export default connect(mapStatesToProps, {jurisdictionSelected})(ContractSpecifications)
