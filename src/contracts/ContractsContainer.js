import React from 'react'
import { connect } from 'react-redux'
import { PropTypes as T } from 'prop-types'
import ContractsComponent from './ContractsComponent'
import _ from 'lodash'
import { loadAllContracts, clearEditContractState } from './ContractActions'

class ContractsContainer extends React.Component {
  componentDidMount () {
    const {contract, contracts, saveResult, loadAllContracts, clearEditContractState} = this.props
    if (contract) {
      clearEditContractState()
    }
    if (_.isEmpty(contracts) || saveResult) {
      loadAllContracts()
    }
  }

  render () {
    return (<ContractsComponent {...this.props} />)
  }
}

ContractsContainer.propTypes = {
  loadAllContracts: T.func.isRequired,
  contracts: T.array,
  contract: T.object,
  saveResult: T.object,
  clearEditContractState: T.func.isRequired
}

const actions = {
  loadAllContracts, clearEditContractState
}

const createGridFriendlyContracts = (all) => (all.results && all.results.length > 0)
  ? all.results
  : all.results

export const mapStateToProps = ({contracts}) => {
  return {
    contracts: createGridFriendlyContracts(contracts.allContracts),
    error: contracts.error,
    saveResult: contracts.saveResult,
    contract: contracts.contract

  }
}

export default connect(mapStateToProps, actions)(ContractsContainer)
