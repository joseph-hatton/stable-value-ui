import { apiPath } from '../../config'
import http from '../../actions/http'

export const loadContractRiskScorecards = (contractId) => (dispatch) => {
  dispatch({
    type: 'LOAD_CONTRACT_RISK_SCORECARDS',
    payload: http.get(`${apiPath}/contracts/${contractId}/risk-scorecards`)
    .then(response => response.results)
  })
}
