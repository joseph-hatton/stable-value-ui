import {hasEntitlement} from '../../entitlements'

const canModify = ({status}) => hasEntitlement('MODIFY_RISK_SCORECARD') && (['ACTIVE', 'PENDING'].indexOf(status) > -1)

export default canModify
