import React from 'react'
import { PropTypes as T } from 'prop-types'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import { blue500, red500 } from 'material-ui/styles/colors'
import { connect } from 'react-redux'
import { Popover, Menu, MenuItem } from 'material-ui'
import { openDialog, closeDialog } from '../../components/redux-dialog/redux-dialog'
import { selectRiskScorecardInputRow, deleteRiskScorecard } from '../ContractActions'
import canModify from './canModify'
import { checkWatchList } from '../../watchLists/watchListAlert/WatchListAlertActions'

const MenuLink = ({cell, openDialog, row, selectRiskScorecardInputRow}) => {
  return (
    <a href="" onClick={(e) => {
      e.nativeEvent.preventDefault()
      selectRiskScorecardInputRow(row)
      openDialog('riskScorecardMenu', {
        anchorEl: e.currentTarget
      })
    }}> {cell} </a>
  )
}

MenuLink.propTypes = {
  cell: T.node,
  row: T.object.isRequired,
  openDialog: T.func.isRequired,
  selectRiskScorecardInputRow: T.func.isRequired
}

export const RiskScorecardMenuLink = connect(null, {openDialog, selectRiskScorecardInputRow})(MenuLink)

const Divider = () => (<hr style={{margin: 0}}/>)

const RiskScorecardMenu = ({open, anchorEl, closeDialog, selectRiskScorecardInputRow, openDialog, riskScorecard, deleteRiskScorecard, contract, checkWatchList}) => (
  <Popover
    id='RiskScorecardMenu-Popover'
    open={open}
    anchorEl={anchorEl}
    anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
    targetOrigin={{horizontal: 'left', vertical: 'top'}}
    onRequestClose={() => {
      closeDialog('riskScorecardMenu')
    }}
  >
    <Menu>
      <MenuItem id="rsMenu-viewEditRiskScorecardButton" primaryText="View/Edit Risk Scorecard" onClick={() => {
        closeDialog('riskScorecardMenu')
        openDialog('riskScorecardInput')
        checkWatchList(contract.contractId)
      }} leftIcon={<EditIcon color={blue500}/>}/>
      <Divider/>
      <MenuItem id="rsMenu-DeleteRiskScorecardButton" primaryText="Delete Risk Scorecard" onClick={() => {
        deleteRiskScorecard(riskScorecard)
        closeDialog('riskScorecardMenu')
      }} leftIcon={<DeleteIcon color={red500}/>} disabled={!canModify(contract)}/>
    </Menu>
  </Popover>
)

RiskScorecardMenu.propTypes = {
  contract: T.object.isRequired,
  open: T.bool.isRequired,
  anchorEl: T.object,
  closeDialog: T.func.isRequired,
  openDialog: T.func.isRequired,
  selectRiskScorecardInputRow: T.func.isRequired,
  deleteRiskScorecard: T.func.isRequired,
  checkWatchList: T.func.isRequired,
  riskScorecard: T.object
}

export const mapStateToProps = ({dialog: {riskScorecardMenu}, contracts: { riskScorecard, contract }}) => {
  return {
    open: !!riskScorecardMenu,
    anchorEl: riskScorecardMenu ? riskScorecardMenu.anchorEl : null,
    riskScorecard,
    contract
  }
}

const actions = {
  closeDialog,
  selectRiskScorecardInputRow,
  openDialog,
  deleteRiskScorecard,
  checkWatchList
}

export default connect(mapStateToProps, actions)(RiskScorecardMenu)
