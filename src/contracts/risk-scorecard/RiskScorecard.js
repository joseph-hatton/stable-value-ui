import _ from 'lodash'
import React from 'react'
import {destroy} from 'redux-form'
import {connect} from 'react-redux'
import {PropTypes as T} from 'prop-types'
import {red500} from 'material-ui/styles/colors'
import ContentAdd from 'material-ui/svg-icons/content/add'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import {checkWatchList} from '../../watchLists/watchListAlert/WatchListAlertActions'
import {openDialog, closeDialog} from '../../components/redux-dialog/redux-dialog'
import WatchListAlert from '../../watchLists/watchListAlert/WatchListAlert'
import {Grid, CreateGridConfig} from '../../components/grid'
import {isEditable} from '../ContractForms'
import canModify from './canModify'
import {
  clearRiskScorecardInputEditForm,
  selectRiskScorecardInputRow
} from '../ContractActions'

import RiskScorecardMenu from './RiskScorecardMenu'
import newRiskScorecard from './newRiskScorecard.json'
import RiskScorecardAddEdit from './RiskScorecardAddEdit'
import {RiskScorecardColumns} from './RiskScorecardColumns'
import {loadContractRiskScorecards} from './RiskScorecardActions'

const config = {
  showHeaderToolbar: false,
  paginate: false
}

class RiskScorecards extends React.Component {
  componentDidMount () {
    const {contractId, loadContractRiskScorecards, riskScorecards} = this.props
    if (contractId && (_.isEmpty(riskScorecards) || _.first(riskScorecards).contractId !== contractId)) {
      loadContractRiskScorecards(contractId)
    }
  }

  open (riskScorecard) {
    const {selectRiskScorecardInputRow, openDialog} = this.props
    selectRiskScorecardInputRow(riskScorecard)
    openDialog('riskScorecardInput')
  }

  render () {
    const {riskScorecards, openDialog, closeDialog, destroy, status, checkWatchList, contractId} = this.props
    const options = CreateGridConfig(riskScorecards, config)
    return (
      <div className="center-grid">
        <Grid onRowDoubleClick={::this.open} id="RiskScorecard" options={options} data={riskScorecards}
              columns={RiskScorecardColumns(this.props.contractType)} sortBy="effectiveDate" sortDirection={'desc'}/>
        <RiskScorecardAddEdit id="RiskScorecard-RiskScorecardAddEdit" onClose={() => {
          this.props.clearRiskScorecardInputEditForm()
          destroy('RiskScorecardAddEdit')
          closeDialog('riskScorecardInput')
        }}/>
        {canModify({status}) && <FloatingActionButton id="RiskScorecard-FAB" backgroundColor={red500} mini={false} className="floating-add-button"
        onClick={() => {
          this.props.selectRiskScorecardInputRow(newRiskScorecard)
          openDialog('riskScorecardInput')
          checkWatchList(contractId)
        }}>
          <ContentAdd/>
        </FloatingActionButton>}
        <RiskScorecardMenu/>
        <WatchListAlert />
      </div>
    )
  }
}

RiskScorecards.propTypes = {
  contractId: T.number.isRequired,
  status: T.bool.isRequired,
  contractType: T.string.isRequired,
  riskScorecards: T.array,
  loadContractRiskScorecards: T.func.isRequired,
  clearRiskScorecardInputEditForm: T.func.isRequired,
  selectRiskScorecardInputRow: T.func.isRequired,
  openDialog: T.func.isRequired,
  closeDialog: T.func.isRequired,
  destroy: T.func.isRequired,
  checkWatchList: T.func.isRequired
}

const actions = {
  loadContractRiskScorecards,
  openDialog,
  closeDialog,
  clearRiskScorecardInputEditForm,
  selectRiskScorecardInputRow,
  destroy,
  checkWatchList
}

const mapStateToProps = ({contracts: {riskScorecards, contract: {contractId, contractType, status}}}) => ({
  contractId,
  contractType,
  riskScorecards,
  status
})

export default connect(mapStateToProps, actions)(RiskScorecards)
