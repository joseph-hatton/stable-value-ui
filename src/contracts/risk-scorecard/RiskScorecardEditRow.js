import React from 'react'
import _ from 'lodash'
import { connect } from 'react-redux'
import { Field, FormSection } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import RateField from '../RateField'
import { PropTypes as T } from 'prop-types'
import { Required } from '../../components/forms/Validators'
import styles from '../../styles/styles'
import { gradeRiskScorecard } from '../ContractActions'

const center = {
  textAlign: 'center',
  marginTop: 12,
  color: 'rgba(0,0,0,1)'
}
const centerField = {
  textAlign: 'center'
}

const LessThan100 = (value) => {
  if (isNaN(value)) {
    console.log('error...', {value})
    return 'Must be a number'
  } else if (Number(value) > 100) {
    return 'Must be below 100'
  }
}

class RiskScorecardEditRow extends React.Component {
  render () {
    const {errors, hidden, name, label, riskScorecard, gradeRiskScorecard} = this.props
    if (!hidden) {
      return (
      <FormSection hidden={hidden} name={name}>
        <div className="row">
          <div className="col-lg-3 text-right" style={styles.scorecardGridLabel}>
            <h1>{label}</h1>
          </div>
          <div className="col-lg-1 text-center">
            <RateField blur={(e) => !errors[name] && gradeRiskScorecard(e)} rateFormat="0.0"
                validate={LessThan100}
                className='center' fullWidth name="score"/>
          </div>
          <div className="col-lg-2" style={center}>
            {_.get(riskScorecard, `${name}.categoryWeight`, 0).toFixed(1)}
          </div>
          <div className="col-lg-2" style={center}>
            {_.get(riskScorecard, `${name}.adjustedWeight`, 0).toFixed(1)}
          </div>
          <div className="col-lg-1" style={center}>
            {_.get(riskScorecard, `${name}.grade`)}
          </div>
          <div className="col-lg-3">
            <Field component={TextField} multiLine fullWidth name="comments"/>
          </div>
        </div>
      </FormSection>
      )
    } else {
      return <span></span>
    }
  }
}

RiskScorecardEditRow.propTypes = {
  label: T.string.isRequired,
  name: T.string.isRequired,
  hidden: T.bool,
  gradeRiskScorecard: T.func.isRequired,
  riskScorecard: T.object,
  errors: T.object
}

const mapStateToProps = ({contracts: {riskScorecard}, form}) => ({
  riskScorecard,
  errors: _.get(form, 'RiskScorecardAddEdit.syncErrors', {})
})

export default connect(mapStateToProps, { gradeRiskScorecard })(RiskScorecardEditRow)
