import React from 'react'
import _ from 'lodash'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import RaisedButton from 'material-ui/RaisedButton'
import { Field, reduxForm, FormSection } from 'redux-form'
import { TextField, DatePicker } from 'redux-form-material-ui'
import RateField from '../RateField'
import ReduxDialog from '../../components/redux-dialog'
import { Required } from '../../components/forms/Validators'
import styles from '../../styles/styles'
import { dateFormat, toUtc } from '../../components/utilities/Formatters'
import { saveRiskScorecardInput } from '../ContractActions'
import RiskScorecardEditRow from './RiskScorecardEditRow'
import canModify from './canModify'

const formId = 'RiskScorecardAddEdit'
const POOLED_PLAN = 'POOLED_PLAN'
const center = {
  textAlign: 'center',
  marginTop: 12,
  color: 'rgba(0,0,0,1)'
}

const RiskScorecardAddEditInput = (props) => {
  const {onClose, contractType, initialValues, handleSubmit, saveRiskScorecardInput, status} = props
  const riskScorecardId = initialValues ? initialValues.riskScorecardId : undefined
  const title = riskScorecardId ? 'Edit Risk Scorecard' : 'Add Risk Scorecard'

  const actions = [
    <RaisedButton
      disabled={!canModify({status})}
      key="Save"
      label="Save"
      primary={true}
      style={{marginRight: 20}}
      type="submit"
      form="RiskScorecardAddEdit"
    />,
    <RaisedButton
      key="Cancel"
      label="Cancel"
      onClick={onClose}
    />
  ]
  return (
    <ReduxDialog
      dialogName="riskScorecardInput"
      title={title}
      actions={actions}
      modal={true}
      autoScrollBodyContent={true}
      contentStyle={styles.wideModal}
    >
      <form id={formId} onSubmit={handleSubmit(saveRiskScorecardInput)}>
        <div className="row">
          <div className="col-lg-6">
            <Field autoOk fullWidth name="effectiveDate" format={toUtc} formatDate={dateFormat} component={DatePicker} firstDayOfWeek={0} floatingLabelText="* Date" validate={Required}/>
          </div>
        </div>
        <div className="row text-center">
          <div className="col-lg-1 col-lg-offset-3">Score</div>
          <div className="col-lg-2">Weight (%)</div>
          <div className="col-lg-2">Adj. Weight (%)</div>
          <div className="col-lg-1">Grade</div>
          <div className="col-lg-3">Comments</div>
        </div>
        <RiskScorecardEditRow name="cashFlow" label='Cash Flow/Liquidity:' />
        <RiskScorecardEditRow name="demographics" label='Demographics:' />
        <RiskScorecardEditRow name="competingFunds" label='Competing Funds:' />
        <RiskScorecardEditRow hidden={contractType === POOLED_PLAN} name="planSponsor" label='Plan Sponsor:' />
        <RiskScorecardEditRow hidden={contractType === POOLED_PLAN} name="planSvFundBalances" label='Plan & SV Funds:' />
        <RiskScorecardEditRow hidden={contractType === POOLED_PLAN} name="planDesign" label='Plan Design/Potential Changes:' />
        <RiskScorecardEditRow hidden={contractType !== POOLED_PLAN} name="pooledFundBalances" label='Pooled Fund Balances:' />
        <RiskScorecardEditRow hidden={contractType !== POOLED_PLAN} name="pooledFundDesign" label='Pooled Fund Design:' />
        <RiskScorecardEditRow name="fundManagement" label='Fund Management:' />
        <RiskScorecardEditRow name="wrappedPortfolio" label='Wrapped Portfolio:' />
        <RiskScorecardEditRow name="marketBookRatio" label='Market/Book Ratio:' />
        <p></p>
        <FormSection name="overallUnadjusted">
          <div className="row">
            <div className="col-lg-3 text-right" style={styles.scorecardGridLabel}>
                <h1>Overall Score/Unadjusted:</h1>
            </div>
            <div className="col-lg-1 text-center" style={center}>
              {props.overallUnadjustedScore.toFixed(1)}
                {/* <RateField fullWidth name="score" rateFormat="0.0" readOnly/> */}
            </div>
            <div className="col-lg-1 col-lg-offset-4" style={center}>
              {props.overallUnadjustedGrade}
                {/* <Field component={TextField} fullWidth name="grade" readOnly/> */}
            </div>
            <div className="col-lg-3">
                <Field component={TextField} multiLine fullWidth name="comments"/>
            </div>
          </div>
        </FormSection>
        <FormSection name="overallAdjusted">
          <div className="row">
            <div className="col-lg-3 text-right" style={styles.scorecardGridLabel}>
                <h1>Overall Score Adjusted:</h1>
            </div>
            <div className="col-lg-1 text-center" style={center}>
              {props.overallAdjustedScore.toFixed(1)}
                {/* <RateField fullWidth name="score" defaultValue="0" rateFormat="0.0" readOnly/> */}
            </div>
            <div className="col-lg-1 col-lg-offset-4" style={center}>
              {props.overallAdjustedGrade}
                {/* <Field component={TextField} fullWidth name="grade" readOnly/> */}
            </div>
            <div className="col-lg-3">
                <Field component={TextField} multiLine fullWidth name="comments"/>
            </div>
          </div>
        </FormSection>
    </form>
    </ReduxDialog>
  )
}

RiskScorecardAddEditInput.propTypes = {
  onClose: T.func.isRequired,
  status: T.bool.isRequired,
  handleSubmit: T.func,
  saveRiskScorecardInput: T.func.isRequired,
  initialValues: T.object,
  riskScorecard: T.object,
  contractType: T.string.isRequired,
  overallUnadjustedScore: T.number,
  overallUnadjustedGrade: T.string,
  overallAdjustedScore: T.number,
  overallAdjustedGrade: T.string
}

const RiskScorecardAddEdit = reduxForm({
  form: formId
})(RiskScorecardAddEditInput)

const actions = {
  saveRiskScorecardInput
}

const mapStateToProps = ({contracts: {riskScorecard, contract: {contractType, status}}}) => ({
  contractType,
  initialValues: riskScorecard,
  overallUnadjustedScore: _.get(riskScorecard, 'overallUnadjusted.score', 0),
  overallUnadjustedGrade: _.get(riskScorecard, 'overallUnadjusted.grade'),
  overallAdjustedScore: _.get(riskScorecard, 'overallAdjusted.score', 0),
  overallAdjustedGrade: _.get(riskScorecard, 'overallAdjusted.grade'),
  status
})

export default connect(mapStateToProps, actions)(RiskScorecardAddEdit)
