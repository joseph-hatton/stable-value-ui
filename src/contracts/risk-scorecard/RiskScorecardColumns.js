import React from 'react'
import RiskScorecardRenderGrade from './RiskScorecardRenderGrade'
import moment from 'moment'
import { RiskScorecardMenuLink } from './RiskScorecardMenu'
import { dateFormat } from '../../components/utilities/Formatters'

const commonProps = {
  customProps: {
    width: `150px`
  }
}

const POOLED_PLAN = 'POOLED_PLAN'

export const RiskScorecardColumns = (contractType) => (
  [
    {
      field: 'riskScorecardId',
      customProps: {
        hidden: true,
        isKey: true
      }
    },
    {
      field: 'effectiveDate',
      sort: true,
      label: 'Effective Date',
      renderer: (cell, row) => ( // eslint-disable-line react/display-name
        <RiskScorecardMenuLink row={row} cell={dateFormat(cell)}/>
      ),
      customProps: {
        width: `150px`
      }
    },
    {
      field: 'cashFlow',
      sort: true,
      label: 'Cash Flow / Liquidity',
      renderer: RiskScorecardRenderGrade,
      customProps: {
        width: `175px`
      }
    },
    {
      field: 'demographics',
      sort: true,
      label: 'Demographics',
      renderer: RiskScorecardRenderGrade,
      customProps: {
        width: `150px`
      }
    },
    {
      field: 'competingFunds',
      sort: true,
      label: 'Competing Funds',
      renderer: RiskScorecardRenderGrade,
      customProps: {
        width: `160px`
      }
    },
    {
      field: 'planSponsor',
      renderer: RiskScorecardRenderGrade,
      sort: true,
      label: 'Plan Sponsor',
      customProps: {
        hidden: contractType === POOLED_PLAN,
        width: '150px'
      }
    },
    {
      field: 'planSvFundBalances',
      sort: true,
      label: 'Plan & SV Fund Balances',
      renderer: RiskScorecardRenderGrade,
      customProps: {
        hidden: contractType === POOLED_PLAN,
        width: '210px'
      }
    },
    {
      field: 'planDesign',
      sort: true,
      label: 'Plan Design / Potential Changes',
      renderer: RiskScorecardRenderGrade,
      customProps: {
        hidden: contractType === POOLED_PLAN,
        width: '250px'
      }
    },
    {
      field: 'pooledFundBalances',
      sort: true,
      label: 'Pooled Fund Balances',
      renderer: RiskScorecardRenderGrade,
      customProps: {
        hidden: contractType !== POOLED_PLAN,
        width: '150px'
      }
    },
    {
      field: 'pooledFundDesign',
      sort: true,
      label: 'Pooled Fund Design',
      renderer: RiskScorecardRenderGrade,
      customProps: {
        hidden: contractType !== POOLED_PLAN,
        width: '150px'
      }
    },
    {
      field: 'fundManagement',
      sort: true,
      label: 'Fund Management',
      renderer: RiskScorecardRenderGrade,
      customProps: {
        width: `160px`
      }
    },
    {
      field: 'wrappedPortfolio',
      sort: true,
      label: 'Wrapped Portfolio',
      renderer: RiskScorecardRenderGrade,
      customProps: {
        width: `160px`
      }
    },
    {
      field: 'marketBookRatio',
      sort: true,
      label: 'Market / Book Ratio',
      renderer: RiskScorecardRenderGrade,
      customProps: {
        width: `170px`
      }
    },
    {
      field: 'overallUnadjusted',
      sort: true,
      label: 'Overall Score / Unadjusted',
      renderer: RiskScorecardRenderGrade,
      customProps: {
        width: `220px`
      }
    },
    {
      field: 'overallAdjusted',
      sort: true,
      label: 'Overall Score Adjusted',
      renderer: RiskScorecardRenderGrade,
      customProps: {
        width: `195px`
      }
    }
  ]
)
