import React from 'react'
import _ from 'lodash'
import {PropTypes as T} from 'prop-types'
import {Link, browserHistory} from 'react-router'
import {connect} from 'react-redux'
import {IntlProvider} from 'react-intl'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import {red500} from 'material-ui/styles/colors'
import {ContractColumns} from './ContractColumns'
import {Grid, CreateGridConfig} from '../components/grid'
import {hasEntitlement} from '../entitlements/index'

const mapManagers = (contracts, managers) => {
  return contracts ? contracts.map((contract) => {
    const manager = managers ? managers.find((m) => m.value === contract.managerId) : null
    contract.investmentManager = manager ? manager.text : ''
    return contract
  }) : contracts
}

const open = ({contractId}) => {
  browserHistory.push(`/contracts/${contractId}/specifications`)
}

export const Contracts = ({managers, contracts}) => {
  const options = CreateGridConfig(contracts, {title: 'Contracts'})
  const contractsWithInvestmentManagers = mapManagers(contracts, managers)
  return (
    <IntlProvider locale="en">
      <div>
        {
          contracts
          && (
            <div>
              <Grid onRowDoubleClick={open} id="Contracts" options={options} data={contractsWithInvestmentManagers} columns={ContractColumns}/>
              {
                hasEntitlement('ADD_CONTRACT') && <Link to={`/contracts/specifications`}>
                  <FloatingActionButton backgroundColor={red500} mini={false} className={'floating-add-button'}>
                    <ContentAdd/>
                  </FloatingActionButton>
                </Link>
              }
            </div>)
        }
      </div>
    </IntlProvider>
  )
}

Contracts.propTypes = {
  contracts: T.array,
  managers: T.array
}

const mapStateToProps = ({contacts: {managers}}) => ({
  managers
})

export default connect(mapStateToProps)(Contracts)
