import createRefDataFormFieldComponent from '../components/forms/createRefDataFormFieldComponent'

export const AdditionalDeposits = createRefDataFormFieldComponent('additionalDeposits', {
  floatingLabelText: 'Additional Deposit',
  type: 'text'
})

export const CreditingRateCalcMethods = createRefDataFormFieldComponent('creditingRateCalculationMethods', {
  floatingLabelText: '* Calculation Method',
  type: 'text'
})

export const CalculationMethodInputDefinitions = createRefDataFormFieldComponent('calculationMethodInputDefinitions', {
  floatingLabelText: '* Calculation Method Input Definition',
  type: 'text'
})

export const CreditingRateRoundingDecimals = createRefDataFormFieldComponent('creditingRateRoundingDecimals', {
  floatingLabelText: '* Rounding',
  type: 'text'
})

export const CreditingRateInputBases = createRefDataFormFieldComponent('creditingRateInputBases', {
  floatingLabelText: '* Input Basis',
  type: 'text'
})

export const CreditingRateFrequencies = createRefDataFormFieldComponent('creditingRateFrequencies', {
  floatingLabelText: '* Frequency',
  type: 'text'
})

export const CreditingRateMonths = createRefDataFormFieldComponent('creditingRateMonths', {
  floatingLabelText: '* First Month',
  type: 'text'
})

export const CreditingRateTypes = createRefDataFormFieldComponent('creditingRateTypes', {
  floatingLabelText: '* Crediting Rate',
  type: 'text'
})

export const SPRatings = createRefDataFormFieldComponent('planSponsorRatings', {
  floatingLabelText: '* S&P Rating',
  type: 'text'
})

export const FitchRatings = createRefDataFormFieldComponent('planSponsorRatings', {
  floatingLabelText: '* Fitch',
  type: 'text'
})

export const MoodyRatings = createRefDataFormFieldComponent('planSponsorMoodyRatings', {
  floatingLabelText: '* Corridor Option Limits-Calculation Method',
  type: 'text'
})

export const OverallRatings = createRefDataFormFieldComponent('planSponsorOverallRatings', {
  floatingLabelText: '* Overall Rating',
  type: 'text'
})

export const Sectors = createRefDataFormFieldComponent('sectors', {
  floatingLabelText: 'Sector',
  type: 'text'
})

export const CorridorOptionCalculationMethods = createRefDataFormFieldComponent('corridorOptionCalculationMethods', {
  floatingLabelText: '* Corridor Option Limits-Calculation Method',
  type: 'text'
})

export const AdviceServiceRemedy = createRefDataFormFieldComponent('adviceServiceRemedies', {
  floatingLabelText: 'Advice Service Remedy',
  type: 'text'
})
