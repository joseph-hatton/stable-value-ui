import React from 'react'
import { PropTypes as T } from 'prop-types'
import _ from 'lodash'
import { connect } from 'react-redux'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import { blue500, red500 } from 'material-ui/styles/colors'
import { ContactColumns as allContactColumns } from '../../contacts/ContactColumns'
import createDialogLinkCell from '../../components/redux-dialog/DialogLinkCell'
import { openConfirmationAlert } from '../../components/redux-dialog'
import { selectContactContract, unAssignContactFromContract } from '../ContractActions'
import { AssignContactMenuLink } from './AssignContactMenu'

const editColumns = [
  {
    field: 'id',
    customProps: {
      hidden: true,
      isKey: true
    }
  },
  {
    field: 'name',
    sort: true,
    label: 'Name',
    customProps: {
      textAlignHeader: 'left',
      width: '20%'
    },
    renderer: (cell, row) => { // eslint-disable-line react/display-name
      return (
        <AssignContactMenuLink row={row} cell={cell} />
      )
    }
  }
]

export default editColumns.concat(_.reject(allContactColumns, (i) => _.includes(['id', 'name', 'contracts'], i.field)))
