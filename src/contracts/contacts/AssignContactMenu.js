import React from 'react'
import { PropTypes as T } from 'prop-types'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import AssignIcon from 'material-ui/svg-icons/content/link'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import { blue500, red500 } from 'material-ui/styles/colors'
import { connect } from 'react-redux'
import { Popover, Menu, MenuItem } from 'material-ui'
import { openDialog, closeDialog, openConfirmationAlert } from '../../components/redux-dialog/redux-dialog'
import { unAssignContactFromContract, selectContactContract } from '../ContractActions'
import { selectAndLoadContact } from '../../contacts/ContactActions'

const MenuLink = ({cell, openDialog, row, selectContactContract, selectAndLoadContact}) => {
  return (
    <a href="" onClick={(e) => {
      e.nativeEvent.preventDefault()
      selectContactContract(row)
      selectAndLoadContact(row)
      openDialog('assignContactMenu', {
        anchorEl: e.currentTarget
      })
    }}> {cell} </a>
  )
}

MenuLink.propTypes = {
  cell: T.node,
  row: T.object.isRequired,
  openDialog: T.func.isRequired,
  selectContactContract: T.func.isRequired,
  selectAndLoadContact: T.func.isRequired
}

export const AssignContactMenuLink = connect(null, {openDialog, selectContactContract, selectAndLoadContact})(MenuLink)

const Divider = () => (<hr style={{margin: 0}}/>)

const AssignContactMenu = ({open, anchorEl, closeDialog, openDialog, selectedContractContact: {contractId, contactId, contactContractId, firstName, lastName}, openConfirmationAlert, unAssignContactFromContract}) => (
  <Popover
    open={open}
    anchorEl={anchorEl}
    anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
    targetOrigin={{horizontal: 'left', vertical: 'top'}}
    onRequestClose={() => {
      closeDialog('assignContactMenu')
    }}
  >
    <Menu>
      <MenuItem id="ContactMenu-viewEditContactContractButton" primaryText="View/Edit Contact Contract Relationship" onClick={() => {
        closeDialog('assignContactMenu')
        openDialog('assignContactToContract')
      }} leftIcon={<AssignIcon color={blue500}/>}/>
      <Divider />
      <MenuItem id="ContactMenu-viewEditContactButton" primaryText="View/Edit Contact Details" onClick={() => {
        closeDialog('assignContactMenu')
        openDialog('addEditContact')
      }} leftIcon={<EditIcon color={blue500}/>}/>
      <Divider/>
      <MenuItem id="ContactMenu-UnassignContactButton" primaryText="Unassign Contact" onClick={() => {
        closeDialog('assignContactMenu')
        openConfirmationAlert({
          message: `This will unassign ${[lastName, firstName].join(', ')} from the contract. Do you want to proceed?`,
          onOk: () => unAssignContactFromContract(contractId, contactContractId)
        })
      }} leftIcon={<DeleteIcon color={red500}/>}/>
    </Menu>
  </Popover>
)

AssignContactMenu.propTypes = {
  open: T.bool.isRequired,
  anchorEl: T.object,
  closeDialog: T.func.isRequired,
  openDialog: T.func.isRequired,
  openConfirmationAlert: T.func.isRequired,
  unAssignContactFromContract: T.func.isRequired,
  selectedContractContact: T.object
}

export const mapStateToProps = ({dialog: {assignContactMenu}, contracts: {selectedContractContact = {}}}) => ({
  open: !!assignContactMenu,
  anchorEl: assignContactMenu ? assignContactMenu.anchorEl : null,
  selectedContractContact
})

const actions = {
  closeDialog,
  openDialog,
  openConfirmationAlert,
  unAssignContactFromContract
}

export default connect(mapStateToProps, actions)(AssignContactMenu)
