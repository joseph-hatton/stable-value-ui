import React from 'react'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import { RaisedButton, Card } from 'material-ui'
import styles from '../../styles/styles'
import { reduxForm } from 'redux-form'
import YesNo from '../../components/forms/YesNo'
import ReduxDialog from '../../components/redux-dialog'
import { closeDialog } from '../../components/redux-dialog/redux-dialog'
import { assignContactToContract, selectContactContract } from '../ContractActions'

const formId = 'AssignContact'

const AssignContact = (props) => {
  const {handleSubmit, closeDialog, assignContactToContract, selectContactContract, contact: {name, contactTypeName}} = props

  const actions = [
    <RaisedButton
      key="Save"
      label="Save"
      primary={true}
      style={{marginRight: 20}}
      type="submit"
      form={formId}
    />,
    <RaisedButton
      id="AssignContact-Cancel"
      key="Cancel"
      label="Cancel"
      onClick={() => {
        closeDialog('assignContactToContract')
        selectContactContract()
      }}
    />
  ]
  const title = `Assign ${name} as ${contactTypeName}`
  return (
    <ReduxDialog
      dialogName="assignContactToContract"
      title={title}
      actions={actions}
      modal={true}
      autoScrollBodyContent={true}
    >
      <div style={{marginTop: 15}}>
        <form className="AssignContactForm" id={formId} onSubmit={handleSubmit(assignContactToContract)}>
          <YesNo
            name="receiveStatement"
            label="Receive Statement"
          />
          <YesNo
            name="receiveInvoice"
            label="Receive Invoice"
          />
          <YesNo
            name="receiveScheduleA"
            label="Receive ScheduleA"
          />
        </form>
      </div>
    </ReduxDialog>
  )
}

AssignContact.propTypes = {
  handleSubmit: T.func.isRequired,
  closeDialog: T.func.isRequired,
  initialValues: T.object,
  contactTypeName: T.string,
  assignContactToContract: T.func.isRequired,
  selectContactContract: T.func.isRequired,
  contact: T.object
}

const AssignContactForm = reduxForm({
  form: formId,
  enableReinitialize: true
})(AssignContact)

const createContractContact = ({contactId, receiveInvoice, receiveStatement, receiveScheduleA}, contractId) => ({
  contactId,
  contractId,
  receiveInvoice,
  receiveStatement,
  receiveScheduleA
})

const mapStateToProps = ({contracts: {contract, selectedContractContact, newContact, existingContact}}) => {
  const contractId = contract && contract.contractId
  return {
    contractId,
    initialValues: selectedContractContact || (newContact ? createContractContact(newContact, contractId) : {}),
    contact: existingContact || newContact || {}
  }
}

const actions = {
  closeDialog,
  assignContactToContract,
  selectContactContract
}

export default connect(mapStateToProps, actions)(AssignContactForm)
