import React from 'react'
import { Route, IndexRoute } from 'react-router'
import _ from 'lodash'

import Home from './components/common/HomePage'
import App from './components/App'

import CompaniesContainer from './companies/CompaniesContainer'
import ContractsContainer from './contracts/ContractsContainer'
import ContactsContainer from './contacts/ContactsContainer'

import AddEditContractContainer from './contracts/AddEditContractContainer'
import ContractContainer from './contracts/ContractContainer'
import ContractBaseInfo from './contracts/ContractBaseInfo'
import ContractSchedule from './contracts/ContractSchedule'
import ContractUnderwriting from './contracts/ContractUnderwriting'
import ContractContacts from './contracts/ContractContacts'
import RiskScorecard from './contracts/risk-scorecard/RiskScorecard'
import CreditingRates from './contracts/crediting-rate/CreditingRates'
import WrapPortfolioContainer from './wrap-portfolio/WrapPortfolioContainer'
import PortfolioAllocationComponent from './wrap-portfolio/PortfolioAllocationComponent'
import Portfolio from './wrap-portfolio/PortfolioComponent'
import AddEditPortfolioAllocationContainer from './wrap-portfolio/AddEditPortfolioAllocationContainer'
import Transactions from './transactions/Transactions'
import MonthlyFees from './fees/monthly/MonthlyFees'
import DailyFees from './fees/daily/DailyFees'
import MonthlyBookValues from './bookValues/monthly/MonthlyBookValues'
import DailyBookValues from './bookValues/daily/DailyBookValues'
import WatchListContainer from './watchLists/WatchListContainer'
import Cycles from './cycles/CyclesContainer'
import Handle404 from './handle404'
import MarketValues from './marketValue/MarketValues'

import store from './store/store'
import {onEnterTabbed} from './components/common/LeftDrawerActions'

export default (
  <Route name="Stable Value" path="/" component={App}>
    <IndexRoute name="Stable Value" component={Home}></IndexRoute>
    <Route name="Companies" path="/companies" component={CompaniesContainer}></Route>
    <Route name="Contacts" path="/contacts" component={ContactsContainer}></Route>
    <Route name="Contracts" path="/contracts" component={ContractContainer}>
      <IndexRoute name="Contracts" component={ContractsContainer}></IndexRoute>
      <Route name="Edit Specifications" path=":contractId/specifications" component={AddEditContractContainer}></Route>
      <Route name="New Contract" path="specifications" component={AddEditContractContainer}></Route>
      <Route name="Base Info" path="info" component={ContractBaseInfo}></Route>
      <Route name="Edit Base Info" path=":contractId/info" component={ContractBaseInfo}></Route>
      <Route name="Schedule" path=":contractId/schedule">
        <IndexRoute name="Schedule" id='schedule-route' onEnter={(s, replace) => replace(`/contracts/${s.params.contractId}/schedule/funding`)}></IndexRoute>
        <Route name="Funding" path="funding" component={ContractSchedule} onEnter={onEnterTabbed('Funding')} />
        <Route name="Crediting Rate" path="crediting-rate" component={ContractSchedule} onEnter={onEnterTabbed('Crediting Rate')} />
        <Route name="Withdrawal" path="withdrawal" component={ContractSchedule} onEnter={onEnterTabbed('Withdrawal')} />
        <Route name="Investment Guidlines" path="investment-guidelines" component={ContractSchedule} onEnter={onEnterTabbed('Investment Guidelines')} />
        <Route name="Termination" path="termination" component={ContractSchedule} onEnter={onEnterTabbed('Termination')} />
      </Route>
      <Route name="Underwriting" path=":contractId/underwriting">
        <IndexRoute name="Underwriting" id='underwriting-route' onEnter={(s, replace) => replace(`/contracts/${s.params.contractId}/underwriting/model-inputs`)}></IndexRoute>
        <Route name="Model Inputs" path="model-inputs" component={ContractUnderwriting} onEnter={onEnterTabbed('Model Inputs')} />
        <Route name="Stable Value Fund" path="stable-value-fund" component={ContractUnderwriting} onEnter={onEnterTabbed('Stable Value Fund')} />
        <Route name="Additional Details" path="additional-details" component={ContractUnderwriting} onEnter={onEnterTabbed('Additional Details')} />
        <Route name="Plan Types" path="plan-types" component={ContractUnderwriting} onEnter={onEnterTabbed('Plan Types')} />
      </Route>
      <Route name="Contacts" path=":contractId/contacts" component={ContractContacts}></Route>
      <Route name="Risk Scorecard" path=":contractId/risk-scorecard" component={RiskScorecard}></Route>
      <Route name="Crediting Rates" path=":contractId/crediting-rates" component={CreditingRates}></Route>
    </Route>
    <Route name="Wrap Portfolio" path="/wrap-portfolio">
      <IndexRoute name="WrapPortfolio" id='wrap-portfolio-route' onEnter={(s, replace) => replace(`/wrap-portfolio/allocations`)}></IndexRoute>
      <Route name="Allocation" path="allocations" component={WrapPortfolioContainer} onEnter={onEnterTabbed('Allocations')} />
      <Route name="Portfolios" path="portfolios" component={WrapPortfolioContainer} onEnter={onEnterTabbed('Portfolios')} />
      <Route name="Add Allocation" path="allocation/add" component={AddEditPortfolioAllocationContainer}></Route>
      <Route name="Edit Allocation" staticName={true} path="allocation/edit/:portfolioAllocationId"
             component={AddEditPortfolioAllocationContainer}></Route>
    </Route>
    <Route name="Transactions" path="/transactions" component={Transactions}></Route>
    <Route name="Fees" path="/fees" component={MonthlyFees}>
      <IndexRoute name="Fees" component={MonthlyFees}></IndexRoute>
      <Route name="Fees for Contract" id='fees-for-contract-route' path=":contractId" prettifyParam={() => `By Contract`} component={DailyFees}></Route>
    </Route>
    <Route name="Book Values" path="/book-values" component={MonthlyBookValues}>
      <IndexRoute name="Book Values" component={MonthlyBookValues}></IndexRoute>
      <Route id='book-values-for-contract-route' name="Book Values for Contract" path=":contractId" prettifyParam={() => `By Contract`} component={DailyBookValues}></Route>
    </Route>
    <Route name="Market Values" path="/market-values" component={MarketValues} />
    <Route name="Watch Lists" path="/watch-lists" component={WatchListContainer}/>
    <Route name="Cycles" path="/cycles" component={Cycles}></Route>
    <Route name="Page Not Found" path='*' exact={true} component={Handle404} />
  </Route>
)
