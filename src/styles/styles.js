import {spacing, typography} from 'material-ui/styles'
import {grey600, white, red500} from 'material-ui/styles/colors'

const styles = {
  leftDrawer: {
    logo: {
      cursor: 'pointer',
      fontSize: 22,
      color: typography.textFullWhite,
      lineHeight: '56px',
      fontWeight: typography.fontWeightMedium,
      backgroundColor: red500,
      paddingLeft: 15,
      height: 56
    },
    menuItem: {
      color: white,
      fontSize: 14,
      padding: '16px 0'
    },
    avatar: {
      div: {
        padding: '15px',
        borderBottom: 'solid 1px #444'
      },
      username: {
        display: 'block',
        color: 'white',
        fontWeight: 400,
        textShadow: '1px 1px #444'
      },
      roles: {
        margin: '5px auto 0',
        display: 'block',
        color: '#ccc',
        fontSize: 13,
        fontWeight: 400,
        textShadow: '1px 1px #444'
      }
    }
  },
  navigation: {
    fontSize: 15,
    fontWeight: typography.fontWeightLight,
    color: grey600,
    paddingBottom: 15,
    display: 'block'
  },
  title: {
    fontSize: 24,
    fontWeight: typography.fontWeightLight,
    marginBottom: 20
  },
  paper: {
    padding: 30
  },
  clear: {
    clear: 'both'
  },
  textAreaDivStyle: {
    alignItems: 'flex-end',
    display: 'flex',
    justifyContent: 'center',
    fontSize: '10px'
  },
  textAreaFont: {
    fontSize: '14px'
  },
  titleStyle: {
    padding: '0px',
    fontSize: '20px'
  },
  sectionStyle: {
    padding: '10px 20px 10px 20px',
    marginTop: '20px'
  },
  toggleDivStyle: {
    alignItems: 'flex-end',
    display: 'flex',
    justifyContent: 'center'
  },
  secondaryButtonStyle: {
    labelColor: white,
    backgroundColor: red500
  },
  tooltipStyles: {
    fontSize: 14
  },
  wideModal: {
    maxWidth: '1100px'
  },
  smallModal: {
    maxWidth: '600px'
  },
  scorecardGridLabel: {
    marginTop: '15px'
  },
  sliderLabelStyle: {
    paddingTop: '16px'
  },
  topMargin: {
    marginTop: '15px'
  },
  fab: {
    position: 'fixed',
    right: 50,
    bottom: 50,
    zIndex: 501
  }
}

export default styles
