import React from 'react'
import {PropTypes as T} from 'prop-types'
import _ from 'lodash'
import {connect} from 'react-redux'
import {Card} from 'material-ui/Card'
import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar'
import RaisedButton from 'material-ui/RaisedButton'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import CalculateIcon from 'material-ui/svg-icons/editor/functions'
import ClearIcon from 'material-ui/svg-icons/content/clear'
import ApproveIcon from 'material-ui/svg-icons/action/done'
import {blue500, red500, white, green500} from 'material-ui/styles/colors'

import {
  canCalculateCreditingRateWithoutEffectiveDate,
  canDelete as canDeleteMarketValue,
  canClearCreditingRate,
  canApproveCreditingRate
} from '../contracts/crediting-rate/CreditingRatesRules'

import {
  deleteSelectedMarketValues,
  calculateCreditingRates,
  clearCreditingRates,
  approveCreditingRates
} from './MarketValueActions'

const style = {margin: 8}
const toolbarStyle = {backgroundColor: white}

const disableMarketValueBulkAction = (selectedRows, predicate) => {
  return _.isEmpty(selectedRows)
    || selectedRows.some(selectedRow => !predicate(selectedRow))
}

const MarketValueBulkActionsToolbar = ({selectedMarketValues}) => {
  const selectedRows = selectedMarketValues.map(marketValue => ({
    creditingRate: marketValue,
    contract: {status: marketValue.contractStatus}
  }))

  return (
    <Card>
      <Toolbar style={toolbarStyle}>
        <ToolbarGroup>
          <RaisedButton style={style} label="Calculate Crediting Rate(s)"
                        icon={<CalculateIcon color={blue500}/>}
                        disabled={disableMarketValueBulkAction(selectedRows, canCalculateCreditingRateWithoutEffectiveDate)}
                        onClick={calculateCreditingRates}
          />
          <RaisedButton style={style} label="Clear Crediting Rate(s)" icon={<ClearIcon color={blue500}/>}
                        disabled={disableMarketValueBulkAction(selectedRows, canClearCreditingRate)}
                        onClick={clearCreditingRates}
          />
          <RaisedButton style={style} label="Approve Crediting Rate(s)" icon={<ApproveIcon color={green500}/>}
                        disabled={disableMarketValueBulkAction(selectedRows, canApproveCreditingRate)}
                        onClick={approveCreditingRates}
          />
          <RaisedButton style={style} label="Delete Market Value(s)" icon={<DeleteIcon color={red500}/>}
                        disabled={disableMarketValueBulkAction(selectedRows, canDeleteMarketValue)}
                        onClick={deleteSelectedMarketValues}/>
        </ToolbarGroup>
      </Toolbar>
    </Card>
  )
}

MarketValueBulkActionsToolbar.propTypes = {
  selectedMarketValues: T.array,
  executeMarketValueBulkAction: T.func.isRequired
}

const mapStateToProps = ({marketValues: {selectedMarketValues = []}}) => ({selectedMarketValues})

export default connect(mapStateToProps)(MarketValueBulkActionsToolbar)
