import React from 'react'
import _ from 'lodash'
import {connect} from 'react-redux'
import {apiPath} from '../config'
import {Grid, CreateGridConfig} from '../components/grid'
import {PropTypes as T} from 'prop-types'
import Expandable from '../components/common/Expandable'
import Filter from 'material-ui/svg-icons/content/filter-list'
import MarketValueFilters from './MarketValueFilters'
import MarketValuesColumns from './MarketValuesColumns'
import CreditingRateMenu from '../contracts/crediting-rate/CreditingRatesMenu'
import MarketValueBulkActionsToolbar from './MarketValueBulkActionsToolbar'
import {hasCreditingRateEntitlement} from '../contracts/crediting-rate/CreditingRatesRules'
import FABAddButton from '../components/common/FABAddButton'
import {serializeDates} from '../actions/serializeDate'
import {IntlProvider} from 'react-intl'
import {
  setMarketValuesView,
  addEditMarketValue,
  openCreditingRate
} from '../contracts/crediting-rate/CreditingRatesActions'
import {
  marketValuesRefreshed,
  marketValuesSelected
} from './MarketValueActions'
import AddEditMarketValue from '../contracts/crediting-rate/AddEditMarketValue'
import ViewEditCreditingRate from '../contracts/crediting-rate/ViewEditCreditingRate'
import MarketValueBulkActionStatus from './MarketValueBulkActionStatus'

const config = {
  selectable: true,
  multiSelectable: true,
  enableSelectAll: true,
  showHeaderToolbar: false,
  paginationType: 'remote',
  baseApiUrl: `${apiPath}/crediting-rates`
}
const options = CreateGridConfig(null, config)

export class MarketValues extends React.Component {
  componentDidMount () {
    this.props.setMarketValuesView()
  }

  getGridRef () {
    return this.refs['MarketValuesGrid']
  }

  componentWillReceiveProps ({refreshMarketValues}) {
    if (refreshMarketValues) {
      this.getGridRef().refresh()
      this.props.marketValuesRefreshed()
    }
  }

  componentWillUnmount () {
    this.props.marketValuesSelected([])
  }

  _addEditMarketValue () {
    const {addEditMarketValue} = this.props
    addEditMarketValue({}, false)
  }

  render () {
    const {selectedCreditingRate, addEditMarketValue, openCreditingRate, marketValuesSelected} = this.props
    const filters = _.omitBy(this.props.filters, filter => !filter)

    return (
      <div>
        <Expandable id="MarketValueFilters" title={<span>Market Value Filters</span>} icon={<Filter/>}>
          <MarketValueFilters/>
        </Expandable>
        <MarketValueBulkActionsToolbar/>
        <IntlProvider locale="en">
          <Grid ref="MarketValuesGrid" id="MarketValues" options={options} columns={MarketValuesColumns}
                filters={serializeDates(filters)}
                onRowDoubleClick={openCreditingRate}
                onRowSelection={marketValuesSelected}
          />
        </IntlProvider>
        <CreditingRateMenu/>
        {selectedCreditingRate && <AddEditMarketValue/>}
        {selectedCreditingRate && <ViewEditCreditingRate/>}
        {
          hasCreditingRateEntitlement() && <FABAddButton onClick={::this._addEditMarketValue}/>
        }
        <MarketValueBulkActionStatus/>
      </div>
    )
  }
}

MarketValues.propTypes = {
  filters: T.object,
  selectedCreditingRate: T.object,
  refreshMarketValues: T.bool,
  addEditMarketValue: T.func.isRequired,
  setMarketValuesView: T.func.isRequired,
  marketValuesRefreshed: T.func.isRequired,
  marketValuesSelected: T.func.isRequired,
  openCreditingRate: T.func.isRequired
}

export const actions = {
  setMarketValuesView,
  addEditMarketValue,
  openCreditingRate,
  marketValuesSelected,
  marketValuesRefreshed
}

const NO_FILTERS = {}
export const mapStateToProps = ({form: {MarketValueFiltersForm}, contracts: {selectedCreditingRate, refreshMarketValues}}) => {
    return {
      selectedCreditingRate,
      filters: MarketValueFiltersForm ? MarketValueFiltersForm.values : NO_FILTERS,
      refreshMarketValues
    }
  }

export default connect(mapStateToProps, actions)(MarketValues)
