import _ from 'lodash'
import store from '../store/store'
import http from '../actions/http'
import {openConfirmationAlert, openDialog} from '../components/redux-dialog/redux-dialog'
import {
  makeMarketValueUrl,
  makeCalculateCreditingRateUrl,
  makeUpdateCreditingRateUrl
} from '../contracts/crediting-rate/CreditingRatesActions'

export const marketValuesRefreshed = () => ({
  type: 'MARKET_VALUE_REFRESHED'
})

export const marketValuesSelected = (selectedMarketValues = []) => ({
  type: 'MARKET_VALUES_SELECTED',
  selectedMarketValues
})

const setBulkActionStatus = (actionLabel, selectedMarketValues) => ({
  type: 'SET_MARKET_VALUE_BULK_ACTION_STATUS',
  payload: {
    marketValuesBulkActionStatus: _(selectedMarketValues)
      .map(marketValue => ({
        creditingRateId: marketValue.creditingRateId,
        row: marketValue
      }))
      .keyBy('creditingRateId')
      .value(),
    actionLabel
  }
})

const updateStatus = (row, actionType, message) => store.dispatch({
  type: actionType,
  payload: row,
  message
})

const executing = (row) => updateStatus(row, 'MARKET_VALUE_BULK_ACTION_EXECUTING')
const succeeded = (row) => updateStatus(row, 'MARKET_VALUE_BULK_ACTION_SUCCESS')
const errored = (row, message) => updateStatus(row, 'MARKET_VALUE_BULK_ACTION_ERROR', message)

const executeBulkAction = (index, httpMethod, makeUrl, makePayload, selectedMarketValues) => {
  if (index === selectedMarketValues.length) {
    store.dispatch({type: 'REFRESH_MARKET_VALUES'})
    return null
  } else {
    const row = selectedMarketValues[index]
    executing(row)
    http[httpMethod](makeUrl(row), makePayload ? makePayload(row) : null)
      .then(response => {
        console.log('res', {response})
        succeeded(row)
        executeBulkAction(++index, httpMethod, makeUrl, makePayload, selectedMarketValues)
      })
      .catch(error => {
        const message = _.get(error, 'response.body.message')
        const messages = _.get(message, 'messages')
        errored(row, messages ? messages.join(' ') : message)
      })
  }
}

export const executeMarketValueBulkAction = (actionLabel, httpMethod, makeUrl, makePayload) => {
  const {marketValues: {selectedMarketValues = []}} = store.getState()

  store.dispatch(openConfirmationAlert({
      message: `Are you sure you want to ${actionLabel}? ${selectedMarketValues.length} row(s) selected.`,
      onOk: () => {
        store.dispatch(setBulkActionStatus(actionLabel, selectedMarketValues))
        store.dispatch(openDialog('MarketValueBulkActionStatusGridDialog'))
        executeBulkAction(0, httpMethod, makeUrl, makePayload, _.orderBy(selectedMarketValues, 'creditingRateId'))
      }
    })
  )
}

const makeDeleteUrl = ({contractId, creditingRateId}) => makeMarketValueUrl(contractId, creditingRateId)
export const deleteSelectedMarketValues = () => {
  executeMarketValueBulkAction('Delete selected Market Value(s)', 'delete', makeDeleteUrl)
}

const makeCalculateUrl = ({contractId, creditingRateId}) => makeCalculateCreditingRateUrl(contractId, creditingRateId)
const makeCalculatePayload = ({effectiveDate}) => ({effectiveDate})
export const calculateCreditingRates = () => {
  executeMarketValueBulkAction('Calculate selected crediting Rate(s)', 'put', makeCalculateUrl, makeCalculatePayload)
}

const makeClearUrl = ({contractId, creditingRateId}) => makeUpdateCreditingRateUrl(contractId, creditingRateId)
export const clearCreditingRates = () => {
  executeMarketValueBulkAction('Clear selected Crediting Rate(s)', 'delete', makeClearUrl)
}
const makeApproveUrl = ({contractId, creditingRateId}) => `${makeUpdateCreditingRateUrl(contractId, creditingRateId)}/approve`
export const approveCreditingRates = () => {
  executeMarketValueBulkAction('Approve selected Crediting Rate(s)', 'put', makeApproveUrl, () => ({}))
}
