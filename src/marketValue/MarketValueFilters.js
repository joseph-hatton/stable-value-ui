import React from 'react'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import {reduxForm, reset} from 'redux-form'
import {RaisedButton} from 'material-ui'
import {Card} from 'material-ui/Card'
import DateRangeFilter from '../components/common/DateRangFilter'
import ManagersFormField from '../contacts/ManagersFormField'
import ContractPicker from '../contracts/ContractPicker'
import createRefDataFormFieldComponent from '../components/forms/createRefDataFormFieldComponent'

const formId = 'MarketValueFiltersForm'

const CLEAR_ALL_BUTTON_STYLE = {
  alignItems: 'flex-end',
  display: 'flex',
  justifyContent: 'center',
  marginBottom: 20
}

export const noPendingContracts = ({status}) => ['PENDING_TERMINATION', 'ACTIVE', 'TERMINATED'].indexOf(status) > 0

export const CreditingRateStatus = createRefDataFormFieldComponent('creditingRateStatuses', {
  floatingLabelText: 'Status',
  type: 'text'
}, undefined, true)

const cardStyle = {paddingLeft: 20, paddingRight: 20}

export const MarketValueFilters = ({clearAll}) => {
  return (
    <Card style={cardStyle}>
      <form id={formId}>
        <div className="row">
          <div className="col-lg-4">
            <ManagersFormField nullable={true} name="managerId" floatingLabelText="Manager"/>
          </div>
          <div className="col-lg-4">
            <ContractPicker nullable={true} name="contractId" type="text" floatingLabelText="Contract" filterContract={noPendingContracts}/>
          </div>
          <div className="col-lg-4">
            <CreditingRateStatus name="status"/>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-5">
            <DateRangeFilter formId={formId} fromFieldName="fromEffectiveDate" toFieldName="toEffectiveDate"
                             label="Effective Date"/>
          </div>
          <div className="col-lg-5">
            <DateRangeFilter formId={formId} fromFieldName="fromReferenceDate" toFieldName="toReferenceDate"
                             label="Reference Date"/>
          </div>
          <div className="col-lg-2" style={CLEAR_ALL_BUTTON_STYLE}>
            <RaisedButton label="Clear All" onClick={clearAll}/>
          </div>
        </div>
      </form>
    </Card>
  )
}

MarketValueFilters.propTypes = {
  clearAll: T.func.isRequired
}

const MarketValueFiltersForm = reduxForm({
  form: formId
})(MarketValueFilters)

export const actions = {
  clearAll: () => (dispatch) => {
    dispatch(reset(formId))
  }
}

export default connect(null, actions)(MarketValueFiltersForm)
