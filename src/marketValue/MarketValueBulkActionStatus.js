import React from 'react'
import _ from 'lodash'
import {connect} from 'react-redux'
import {PropTypes as T} from 'prop-types'
import {IntlProvider} from 'react-intl'
import {blue500, red500, green500} from 'material-ui/styles/colors'
import DoneIcon from 'material-ui/svg-icons/action/done'
import ErrorIcon from 'material-ui/svg-icons/alert/error'
import ReduxDialog from '../components/redux-dialog'
import {Grid, CreateGridConfig, dateFormat, GridStyles} from '../components/grid'
import {marketValuesSelected} from './MarketValueActions'

const progressIconMap = {
  Completed: <DoneIcon color={green500}/>,
  Errored: <ErrorIcon color={red500}/>
}

const MarketValueBulkActionStatusColumns = [
  {
    field: 'creditingRateId',
    customProps: {
      hidden: true,
      isKey: true
    }
  },
  {
    field: 'contractNumber',
    sort: true,
    label: 'Contract #',
    customProps: {
      textAlign: 'center'
    }
  },
  {
    field: 'effectiveDate',
    sort: true,
    label: 'Effective Date',
    renderer: dateFormat,
    customProps: {
      textAlign: 'center'
    }
  },
  {
    field: 'status',
    sort: true,
    label: 'Action Status',
    customProps: {
      textAlign: 'center'
    },
    renderer: status => progressIconMap[status]
  }
]

const WithMessageColumn = MarketValueBulkActionStatusColumns.concat([
  {
    field: 'message',
    sort: true,
    label: 'Message',
    customProps: {
      whiteSpace: 'normal',
      width: 300
    }
  }
])

const config = {
  paginate: false,
  showHeaderToolbar: false,
  height: 'none'
}

const options = CreateGridConfig(null, config)

const toRows = (marketValuesBulkActionStatus) => _(marketValuesBulkActionStatus)
  .values()
  .map(({creditingRateId, status, row: {effectiveDate, contractNumber}, message}) => ({
    contractNumber,
    creditingRateId,
    effectiveDate,
    status,
    message
  }))
  .orderBy('creditingRateId')
  .value()

export class MarketValueBulkActionStatus extends React.Component {
  render () {
    const {actionLabel, marketValuesBulkActionStatus, marketValuesSelected} = this.props
    const data = toRows(marketValuesBulkActionStatus)
    const hasMessage = _(data).some(({message}) => !!message)
    options.minHeight = data.length * GridStyles.rowHeight.height

    return (
      <div>
        <ReduxDialog
          dialogName="MarketValueBulkActionStatusGridDialog"
          title={actionLabel}
          actions={[]}
          modal={true}
          autoScrollBodyContent={true}
          cancelButton
          cancelLabel="Ok"
          onCancel={marketValuesSelected}>
          <div style={{marginTop: 20}}>
            <IntlProvider locale="en">
              <Grid id="MarketValueBulkActionStatusGrid" data={data} options={options}
                    columns={hasMessage ? WithMessageColumn : MarketValueBulkActionStatusColumns}/>
            </IntlProvider>
            <div style={{marginTop: 20, color: blue500}}>
              {data.length} row(s) selected.
            </div>
          </div>
        </ReduxDialog>
      </div>
    )
  }
}

MarketValueBulkActionStatus.propTypes = {
  marketValuesBulkActionStatus: T.object,
  actionLabel: T.string,
  marketValuesSelected: T.func.isRequired
}

export const mapStateToProps =
  ({marketValues: {marketValuesBulkActionStatus, actionLabel}}) => ({marketValuesBulkActionStatus, actionLabel})

export default connect(mapStateToProps, {marketValuesSelected})(MarketValueBulkActionStatus)
