import React from 'react'
import {CreditingRatesMenuLink} from '../contracts/crediting-rate/CreditingRatesMenu'
import {dateFormat, currencyFormat, percentFormat} from '../components/grid'
import {CreditRatePercentFormat} from '../contracts/crediting-rate/CreditingRatesColumns'
import {createReferenceDataTextRenderer} from '../components/renderers/ReferenceDataTextRenderer'
import ContractManagerText from '../contacts/ContractManagerText'
import {FormattedNumber} from 'react-intl'
import numeral from 'numeral'

const commonProps = {
  customProps: {
    textAlign: 'center',
    width: 150
  }
}

export const VariableLengthPercentFormat = (value, numberOfDecimals) => {
  if (value === 0) {
    return `0.${'0'.repeat(numberOfDecimals)}%`
  } else {
    if (isNaN(value) || value === null) {
      return value
    } else {
      return `${parseFloat(value).toFixed(numberOfDecimals)}%`
    }
  }
}

export const contractRoundingPercentFormat = (value, row) => VariableLengthPercentFormat(value, parseInt(row.currentContractRoundDecimal))

export const percentFormatMKOne = (number) => {
  if (number) {
    return (<FormattedNumber value={number || 0} style='percent' minimumFractionDigits={1}/>)
  } else {
    return ''
  }
}
export const OptionalPercentFormat = (value) => VariableLengthPercentFormat(value, 3)
export const currencyFormatMV = (number) => {
  if (number) {
    return numeral(number).format('(0,0.00)')
  } else {
    return ''
  }
}

const ContractNumberRenderer = (contractNumber, creditingRate) => (
  <CreditingRatesMenuLink creditingRate={creditingRate}>
    {contractNumber}
  </CreditingRatesMenuLink>
)

const MarketValueColumns = [
  {
    field: 'creditingRateId',
    customProps: {
      hidden: true,
      isKey: true
    }
  },
  {
    field: 'contractNumber',
    sort: true,
    label: 'Contract #',
    customProps: {
      textAlignHeader: 'left',
      textAlign: 'left',
      width: 150
    },
    renderer: ContractNumberRenderer
  },
  {
    field: 'shortPlanName',
    sort: true,
    label: 'Short Plan Name',
    customProps: {
      textAlign: 'left',
      width: 150
    }
  },
  {
    field: 'managerId',
    customProps: {
      hidden: true
    }
  },
  {
    field: 'managerName',
    sort: true,
    label: 'Manager',
    customProps: {
      textAlignHeader: 'left',
      textAlign: 'left',
      width: 200
    }
  },
  {
    field: 'referenceDate',
    sort: true,
    label: 'Reference Date',
    renderer: dateFormat, // eslint-disable-line react/display-name
    ...commonProps
  },
  {
    field: 'marketValue',
    sort: true,
    label: 'Market Value',
    renderer: currencyFormatMV,
    customProps: {
        textAlign: 'right',
        width: 150
    }
  },
  {
    field: 'bookValue',
    sort: true,
    label: 'Book Value',
    renderer: currencyFormatMV,
    customProps: {
        textAlign: 'right',
        width: 150
    }
  },
  {
    field: 'mvbvRatio',
    sort: true,
    label: 'MV/BV',
    renderer: percentFormatMKOne,
    customProps: {
      textAlign: 'right',
      width: 100
    }
  },
  {
    field: 'annualEffectiveYield',
    sort: true,
    label: 'AEY',
    renderer: OptionalPercentFormat,
    customProps: {
        textAlign: 'right',
        width: 150
    }
  },
  {
    field: 'duration',
    sort: true,
    label: 'Duration',
    renderer: (value) => value ? parseFloat(value).toFixed(2) : '',
    customProps: {
      textAlign: 'center',
      width: 100
    }
  },
  {
    field: 'effectiveDate',
    sort: true,
    label: 'Effective Date',
    renderer: dateFormat,
    ...commonProps
  },
  {
    field: 'grossRate',
    sort: true,
    label: 'Gross Crediting Rate',
    renderer: contractRoundingPercentFormat,
    ...commonProps
  },
  {
    field: 'rate',
    sort: true,
    label: 'Net Crediting Rate',
    renderer: contractRoundingPercentFormat,
    ...commonProps
  },
  {
    field: 'status',
    sort: true,
    label: 'Status',
    renderer: createReferenceDataTextRenderer('creditingRateStatuses'),
    ...commonProps,
    customProps: {
      textAlign: 'left',
      width: 200
    }
  }
]

export default MarketValueColumns
