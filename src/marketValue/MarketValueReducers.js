import _ from 'lodash'

const INITIAL_STATE = {}

const updateStatus = (state, action, status) => {
  const {creditingRateId} = action.payload
  const {message} = action

  return {
    ...state,
    marketValuesBulkActionStatus: _.transform(state.marketValuesBulkActionStatus, (result, value, key) => {
      if (key === String(creditingRateId)) {
        result[creditingRateId] = {...value, status, message}
      } else {
        result[key] = value
      }
    })
  }
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'MARKET_VALUES_SELECTED': {
      const {selectedMarketValues} = action
      return {
        ...state,
        selectedMarketValues
      }
    }
    case 'SET_MARKET_VALUE_BULK_ACTION_STATUS': {
      const {marketValuesBulkActionStatus, actionLabel} = action.payload
      return {
        ...state,
        marketValuesBulkActionStatus,
        actionLabel
      }
    }
    case 'MARKET_VALUE_BULK_ACTION_PENDING': {
      return updateStatus(state, action, 'Executing')
    }
    case 'MARKET_VALUE_BULK_ACTION_SUCCESS': {
      return updateStatus(state, action, 'Completed')
    }
    case 'MARKET_VALUE_BULK_ACTION_ERROR': {
      return updateStatus(state, action, 'Errored')
    }
    default:
      return state
  }
}
