import React from 'react'
import {currencyFormat} from '../../components/grid'
import { Link } from 'react-router'

const commonProps = {
  customProps: {
    width: `${100 / 6}%`
  }
}

export const MonthlyBookValueColumns = () => ([
  {
    field: 'contractNumber',
    sort: true,
    label: 'Contract #',
    renderer: (cell, row) => ( // eslint-disable-line react/display-name
      <Link title={row.shortPlanName} to={`/book-values/${row.contractId}`}>{cell}</Link>
    ),
    customProps: {
      ...(commonProps.customProps),
      isKey: true,
      textAlignHeader: 'left'
    }
  },
  {
    field: 'shortPlanName',
    sort: true,
    label: 'Plan Name',
      customProps: {
      width: `${100 / 6}%`,
      textAlignHeader: 'left'
  }
  },
  {
    field: 'beginningBookValue',
    sort: true,
    label: 'Beginning BV',
    renderer: currencyFormat,
    customProps: {
      ...commonProps.customProps,
      textAlign: 'right'
    }
  },
  {
    field: 'deposits',
    sort: true,
    label: 'Deposits',
    renderer: currencyFormat,
    customProps: {
      ...commonProps.customProps,
      textAlign: 'right'
    }
  },
  {
    field: 'withdrawals',
    sort: true,
    label: 'Withdrawals',
    renderer: currencyFormat,
    customProps: {
      ...commonProps.customProps,
      textAlign: 'right'
    }
  },
  {
    field: 'adjustments',
    sort: true,
    label: 'Adjustments',
    renderer: currencyFormat,
    customProps: {
      ...commonProps.customProps,
      textAlign: 'right'
    }
  },
  {
    field: 'interest',
    sort: true,
    label: 'Accrued Interest',
    renderer: currencyFormat,
    customProps: {
      ...commonProps.customProps,
      textAlign: 'right'
    }
  },
  {
    field: 'endingBookValue',
    sort: true,
    label: 'Ending BV',
    renderer: currencyFormat,
    customProps: {
      ...commonProps.customProps,
      textAlign: 'right'
    }
  }
])
