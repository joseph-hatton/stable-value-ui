import React from 'react'
import {connect} from 'react-redux'
import {browserHistory} from 'react-router'
import {Grid, CreateGridConfig} from '../../components/grid'
import {PropTypes as T} from 'prop-types'
import { Card } from 'material-ui/Card'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import {MonthlyBookValueColumns} from './MonthlyBookValueColumns'
import MonthlyBookValuesFilter from './MonthlyBookValuesFilter'
import {loadAllMonthlyBookValues} from '../BookValueActions'
import {IntlProvider} from 'react-intl'

const config = {
  showHeaderToolbar: false,
  paginate: true,
  defaultSortName: 'contractNumber',
  defaultSortOrder: 'asc',
  height: window.innerHeight - 200
}

const open = ({contractId}) => {
  browserHistory.push(`/book-values/${contractId}`)
}

class MonthlyBookValues extends React.Component {
  componentDidMount () {
    this.props.loadAllMonthlyBookValues()
  }

  render () {
    const {MonthlyBookValues} = this.props
    const options = CreateGridConfig(MonthlyBookValues, config)
    return (
      (this.props.children) || (<div>
        <MonthlyBookValuesFilter/>
        <IntlProvider>
          <Grid onRowDoubleClick={open} id="MonthlyBookValues" options={options} data={MonthlyBookValues} columns={MonthlyBookValueColumns()}/>
        </IntlProvider>
      </div>)
    )
  }
}

MonthlyBookValues.propTypes = {
  MonthlyBookValues: T.array,
  children: T.element,
  filtered: T.bool,
  loadAllMonthlyBookValues: T.func.isRequired
}

const actions = {
  loadAllMonthlyBookValues
}

const mapStateToProps = ({bookValues: {allBookValues, filtered}}) => ({
  MonthlyBookValues: allBookValues,
  filtered
})

export default connect(mapStateToProps, actions)(MonthlyBookValues)
