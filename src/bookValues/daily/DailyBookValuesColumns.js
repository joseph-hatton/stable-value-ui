import React from 'react'
import {FormattedNumber} from 'react-intl'
import {currencyFormat, dateFormat, percentFormat} from '../../components/grid'

const commonWidth = 100 / 8

const commonProps = {
  customProps: {
    width: `${commonWidth}%`,
    textAlign: 'center'
  }
}

const invoiceWidthVariant = 6

export const creditingRatePercent = (number, row) => {
  return (<FormattedNumber value={number || 0} style='percent' minimumFractionDigits={row.creditingRateRoundDecimals || 2}/>)
}

export const DailyBookValuesColumns = () => ([
  {
    field: 'effectiveDate',
    sort: true,
    label: 'As of Date',
    renderer: dateFormat,
    customProps: {
      ...(commonProps.customProps),
      isKey: true
    }
  },
  {
    field: 'beginningBookValue',
    sort: true,
    label: 'Beginning BV',
    renderer: currencyFormat,
    ...commonProps
  },
  {
    field: 'deposits',
    sort: true,
    label: 'Deposits',
    renderer: currencyFormat,
    customProps: {
      ...commonProps.customProps,
      width: `${commonWidth - (invoiceWidthVariant / 2)}%`
    }
  },
  {
    field: 'withdrawals',
    sort: true,
    label: 'Withdrawals',
    renderer: currencyFormat,
    customProps: {
      ...commonProps.customProps,
      width: `${commonWidth - (invoiceWidthVariant / 2)}%`
    }
  },
  {
    field: 'adjustments',
    sort: true,
    label: 'Adjustments',
    renderer: currencyFormat,
    ...commonProps
  },
  {
    field: 'interest',
    sort: true,
    label: 'Accrued Interest',
    renderer: currencyFormat,
    ...commonProps
  },
  {
    field: 'endingBookValue',
    sort: true,
    label: 'Ending BV',
    renderer: currencyFormat,
    ...commonProps
  },
  {
    field: 'rate',
    sort: true,
    label: 'Net CR',
    renderer: creditingRatePercent,
    ...commonProps
  },
  {
    field: 'rateEffectiveDate',
    sort: true,
    label: 'CR As of Date',
    renderer: dateFormat,
    ...commonProps
  }
])
