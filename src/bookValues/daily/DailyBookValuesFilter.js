import React from 'react'
import _ from 'lodash'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import {reduxForm, reset, change, Field} from 'redux-form'
import RaisedButton from 'material-ui/RaisedButton'
import IconButton from 'material-ui/IconButton'
import Clear from 'material-ui/svg-icons/content/clear'
import {TextField} from 'redux-form-material-ui'
import {dateFormat} from '../../components/utilities/Formatters'
import {Card} from 'material-ui/Card'
import {selectDailyBookValues, setDateFilter, setFilterOnDay, filterDailyBookValues} from '../BookValueActions'
import moment from 'moment'
import YearFormField from '../../components/forms/YearFormField'
import MonthFormField from '../../components/forms/MonthFormField'
import DayFormField from '../../components/forms/DayFormField'
import {white, red500} from 'material-ui/styles/colors'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'

const formId = 'DailyBookValuesFilterForm'

const showDateFilter = ({clearFilter, yearFilter, monthFilter, yearChange, monthChange, contractNumber, shortPlanName}) => {
  return (
    <Card>
      <Toolbar style={{borderTop: 'solid 1px #e0e0e0', backgroundColor: '#f8f9fa', height: '75px'}}>
        <ToolbarGroup style={{marginLeft: '5px'}} firstChild={true}>
          <strong>{contractNumber}</strong> ({shortPlanName})
        </ToolbarGroup>
        <ToolbarGroup style={{marginRight: '5px'}} lastChild={true}>
          <YearFormField style={{marginRight: '5px'}} floatingLabelText="Filter by Year" name="yearFilter" onChange={yearChange} />
          <MonthFormField floatingLabelText="Filter by Month" name="monthFilter" onChange={monthChange} />
          <RaisedButton style={{margin: '20px'}} label="Clear Filter" onClick={clearFilter} />
        </ToolbarGroup>
      </Toolbar>
    </Card>
  )
}

showDateFilter.propTypes = {
  clearFilter: T.func.isRequired,
  dateFilter: T.obj,
  yearChange: T.func.isRequired,
  monthChange: T.func.isRequired,
  yearFilter: T.number.isRequired,
  monthFilter: T.number.isRequired,
  contractNumber: T.string,
  shortPlanName: T.string
}

const DailyBookValuesFilter = (props) => {
  return (
      <form id={formId}>
        { showDateFilter(props) }
      </form>
  )
}

const DailyBookValuesFilterForm = reduxForm({
  form: formId
})(DailyBookValuesFilter)

const actions = {
  clearFilter: () => (dispatch, getState) => {
    const {bookValues: {dateFilter}} = getState()
    dateFilter.year(moment().year())
    dateFilter.month(moment().month())
    dispatch(change('DailyBookValuesFilterForm', 'monthFilter', dateFilter.month() + 1))
    dispatch(change('DailyBookValuesFilterForm', 'yearFilter', dateFilter.year()))
    dispatch(setDateFilter(dateFilter))
    dispatch(setFilterOnDay(false))
    dispatch(selectDailyBookValues())
  },
  yearChange: (e, year) => (dispatch, getState) => {
    const {bookValues: {dateFilter}} = getState()
    dateFilter.year(year)
    dispatch(change('DailyBookValuesFilterForm', 'yearFilter', year))
    dispatch(setDateFilter(dateFilter))
    dispatch(filterDailyBookValues())
  },
  monthChange: (e, month) => (dispatch, getState) => {
    const {bookValues: {dateFilter}} = getState()
    dateFilter.month(month - 1)
    dispatch(change('DailyBookValuesFilterForm', 'monthFilter', month))
    dispatch(setDateFilter(dateFilter))
    dispatch(filterDailyBookValues())
  }
}

const mapStateToProps = ({form, bookValues: {dateFilter, contractNumber, shortPlanName}}) => {
  return {
    yearFilter: _.get(form, 'DailyBookValuesFilterForm.values.yearFilter'),
    monthFilter: _.get(form, 'DailyBookValuesFilterForm.values.monthFilter'),
    contractNumber,
    shortPlanName,
    initialValues: {
        yearFilter: dateFilter.year(),
        monthFilter: dateFilter.month() + 1
    }
  }
}

export default connect(mapStateToProps, actions)(DailyBookValuesFilterForm)
