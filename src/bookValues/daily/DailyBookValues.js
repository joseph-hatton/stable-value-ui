import React from 'react'
import {connect} from 'react-redux'
import {Grid, CreateGridConfig} from '../../components/grid'
import {PropTypes as T} from 'prop-types'
import {DailyBookValuesColumns} from './DailyBookValuesColumns'
import {selectDailyBookValues, setContractId, filterDailyBookValues} from '../BookValueActions'
import DailyBookValuesFilter from './DailyBookValuesFilter'
import {IntlProvider} from 'react-intl'

const config = {
  showHeaderToolbar: false,
  paginate: true,
  defaultSortName: 'effectiveDate',
  defaultSortOrder: 'asc',
  height: window.innerHeight - 200
}

class DailyBookValues extends React.Component {
  componentDidMount () {
    this.props.setContractId(this.props.params.contractId)
    if (this.props.filtered) {
      this.props.filterDailyBookValues()
    } else {
      this.props.selectDailyBookValues(this.props.params.contractId)
    }
  }

  render () {
    const {loadedBookValues} = this.props
    const options = CreateGridConfig(loadedBookValues, config)
    return (
      <div>
        <DailyBookValuesFilter contractId={this.props.params.contractId}/>
        <IntlProvider>
          <Grid id="DailyBookValues" options={options} data={loadedBookValues}
            columns={DailyBookValuesColumns()}/>
        </IntlProvider>
      </div>
    )
  }
}

DailyBookValues.propTypes = {
  params: T.object,
  loadedBookValues: T.array,
  selectDailyBookValues: T.func.isRequired,
  setContractId: T.func.isRequired,
  filterDailyBookValues: T.func.isRequired,
  filtered: T.bool
}

const actions = {
  selectDailyBookValues,
  setContractId,
  filterDailyBookValues
}

const mapStateToProps = ({bookValues: {loadedBookValue, filtered}}) => ({
  loadedBookValues: loadedBookValue,
  filtered
})

export default connect(mapStateToProps, actions)(DailyBookValues)
