import React from 'react'
import store from '../src/store/store'
import {browserHistory} from 'react-router'
import {canNavigateAwayFromContracts} from './contracts/ContractActions'
import {openConfirmationAlert} from './components/redux-dialog'

const stopAndAsk = (name) => () => (<a href="" onClick={(event) => { // eslint-disable-line react/display-name
  const {dispatch, getState} = store
  event.persist()
  const url = event.target.parentNode.pathname
  event.preventDefault()
  event.stopPropagation()
  if (canNavigateAwayFromContracts()(dispatch, getState)) {
    browserHistory.push(url)
  } else {
    dispatch(openConfirmationAlert({
      onOk: () => {
        browserHistory.push(url)
      },
      message: 'Your changes are not saved. Leaving this contract will erase your changes.'
    }, 'navigateAwayFromContract'))
  }
}}>{name}</a>)

export default stopAndAsk
