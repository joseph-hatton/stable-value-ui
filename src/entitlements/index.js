import _ from 'lodash'
import {ENTITLEMENTS, API_ENTITLEMENTS} from './entitlements'
import store from '../store/store'

const determineEntitlements = (groups) => {
  return _.transform(ENTITLEMENTS, (result, value, key) => {
    result[key] = value.some(entitlement => groups.includes(entitlement))
  }, {})
}

export const hasEntitlement = (entitlement) => {
  return store.getState().currentUser[entitlement]
}

export default determineEntitlements
