import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'
import rootReducer from '../reducers'
import promiseMiddleware from 'redux-promise-middleware'

const hasCycleActionChanged = ({cycles: {isRunning = false}}, action) => isRunning !== action.isRunning

const logger = createLogger({
  predicate: (getState, action) => {
    const state = getState()
    const hasIt = hasCycleActionChanged(state, action)
    if (action.type === 'IS_CYCLE_RUNNING') {
      return hasCycleActionChanged(state, action)
    } else {
      return true
    }
  }
})

export default createStore(rootReducer, {}, applyMiddleware(thunk, logger, promiseMiddleware()))
