import React from 'react'
import { PropTypes as T } from 'prop-types'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import { blue500 } from 'material-ui/styles/colors'
import { connect } from 'react-redux'
import { Popover, Menu, MenuItem } from 'material-ui'
import { openDialog, closeDialog } from '../components/redux-dialog/redux-dialog'
import { selectContracts } from './ContactActions'
import { browserHistory } from 'react-router'

const MenuLink = ({cell, openDialog, row, selectContracts}) => {
  return (
    <a href="" onClick={(e) => {
      e.nativeEvent.preventDefault()
      if (row.contracts.length > 0) {
        selectContracts(row)
        openDialog('contractsMenu', {
          anchorEl: e.currentTarget
        })
      }
    }}> {cell} </a>
  )
}

MenuLink.propTypes = {
  cell: T.node,
  row: T.object.isRequired,
  openDialog: T.func.isRequired,
  selectContracts: T.func.isRequired
}

export const ContractsMenuLink = connect(null, {openDialog, selectContracts})(MenuLink)

const Divider = () => (<hr style={{margin: 0}}/>)

const ContractsMenu = ({open, anchorEl, closeDialog, openDialog, selectedContracts}) => (
  <Popover
    open={open}
    anchorEl={anchorEl}
    anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
    targetOrigin={{horizontal: 'left', vertical: 'top'}}
    onRequestClose={() => {
      closeDialog('contractsMenu')
    }}
  >
    <Menu maxHeight='475px'>
      {selectedContracts.map(({contractId, contractNumber}, index, arr) => (
        <span key={index}>
          <MenuItem id={`ContractsMenu-MenuItem-${index}`} primaryText={`${contractNumber}`} onClick={() => {
            closeDialog('contractsMenu')
            browserHistory.push(`/contracts/${contractId}/specifications`)
          }} leftIcon={<EditIcon color={blue500}/>}/>
          {arr.length - 1 !== index && <Divider />}
        </span>
      ))}
    </Menu>
  </Popover>
)

ContractsMenu.propTypes = {
  open: T.bool.isRequired,
  anchorEl: T.object,
  closeDialog: T.func.isRequired,
  openDialog: T.func.isRequired,
  selectedContracts: T.array.isRequired
}

export const mapStateToProps = ({dialog: {contractsMenu}, contacts: {selectedContracts}}) => ({
  open: !!contractsMenu,
  anchorEl: contractsMenu ? contractsMenu.anchorEl : null,
  selectedContracts
})

const actions = {
  closeDialog,
  openDialog
}

export default connect(mapStateToProps, actions)(ContractsMenu)
