import React from 'react'
import { connect } from 'react-redux'
import { PropTypes as T } from 'prop-types'
import _ from 'lodash'
import ContactsComponent from './ContactsComponent'
import {loadAllContacts} from './ContactActions'

export class ContactsContainer extends React.Component {
  componentDidMount () {
    const {Contacts, saveResult, loadAllContacts} = this.props
    if (_.isEmpty(Contacts) || saveResult) {
      loadAllContacts()
    }
  }

  componentWillUnmount () {
    const {saveResult, clearSaveContactResult} = this.props
    if (saveResult) {
      clearSaveContactResult()
    }
  }

  render () {
    return (<ContactsComponent {...this.props} />)
  }
}

ContactsContainer.propTypes = {
  loadAllContacts: T.func.isRequired,
  Contacts: T.array,
  editContact: T.object,
  saveResult: T.object,
  clearEditContactState: T.func.isRequired,
  clearSaveContactResult: T.func.isRequired
}

const actions = {
  loadAllContacts,
  clearSaveContactResult: () => ({
    type: 'CLEAR_CONTACT_SAVE_RESULT'
  }),
  clearEditContactState: () => ({
    type: 'CLEAR_EDIT_CONTACT'
  })
}

export const mapStateToProps = ({contacts}) => {
  return {
    contacts: contacts.allContacts,
    error: contacts.error,
    saveResult: contacts.saveResult,
    editContact: contacts.editContact
  }
}

export default connect(mapStateToProps, actions)(ContactsContainer)
