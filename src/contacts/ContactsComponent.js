import React from 'react'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import {red500} from 'material-ui/styles/colors'

import {ContactColumns} from './ContactColumns'
import {Grid, CreateGridConfig} from '../components/grid'
import ContractsMenu from './ContractsMenu'
import ContactMenu from './ContactMenu'
import AssignUnassignContracts from './AssignUnassignContracts'
import AssignContracts from './AssignContracts'
import AddEditContactComponent from './AddEditContactComponent'
import { selectContact, setAddContractState } from './ContactActions'
import { openDialog } from '../components/redux-dialog'

const fabStyle = {
  marginRight: 50,
  marginBottom: 50,
  position: 'fixed',
  right: 0,
  bottom: 0
}

class Contacts extends React.Component {
  render () {
    const {contacts, openDialog, selectContact, setAddContractState} = this.props
    const options = CreateGridConfig(contacts, {title: 'Contacts'})
    return (
      <div>
        {
          contacts
          && (
            <div>
              <Grid onRowDoubleClick={(row) => {
                selectContact(row)
                openDialog('addEditContact')
              }} id="Contacts" options={options} data={contacts} columns={ContactColumns}/>
              <ContactMenu />
              <ContractsMenu />
              <AssignUnassignContracts />
              <AssignContracts />
              <AddEditContactComponent />
              <FloatingActionButton backgroundColor={red500} mini={false} style={fabStyle} onClick={() => {
                setAddContractState()
                openDialog('addEditContact')
              }}>
                <ContentAdd/>
              </FloatingActionButton>
            </div>)
        }
      </div>
    )
  }
}

Contacts.propTypes = {
  contacts: T.array,
  openDialog: T.func.isRequired,
  setAddContractState: T.func.isRequired,
  selectContact: T.func.isRequired
}

const mapStateToProps = () => ({})

const actions = {
  openDialog,
  selectContact,
  setAddContractState
}

export default connect(mapStateToProps, actions)(Contacts)
