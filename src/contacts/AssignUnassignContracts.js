import React from 'react'
import _ from 'lodash'
import { connect } from 'react-redux'
import { PropTypes as T } from 'prop-types'
import RaisedButton from 'material-ui/RaisedButton'
import Card from 'material-ui/Card/Card'
import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar'
import ReduxDialog from '../components/redux-dialog/redux-dialog'
import {openDialog, closeDialog} from '../components/redux-dialog'
import { Grid, CreateGridConfig } from '../components/grid'
import { selectContractRow, queueUnassignContracts, resetAssignUnassign, saveBufferedAssignUnassignActions } from './ContactActions'
import { AssignUnassignContractsColumns } from './AssignUnassignContractsColumns'

const config = {
  selectable: true,
  enableSelectAll: true,
  multiSelectable: true,
  deselectOnClickaway: false,
  showHeaderToolbar: false,
  height: 400,
  paginate: false
}

class AssignUnassignContracts extends React.Component {
  render () {
    const {selectedContact: {firstName, lastName, contracts}, openDialog, closeDialog, selectContractRow, queueUnassignContracts, resetAssignUnassign, saveBufferedAssignUnassignActions} = this.props
    const options = CreateGridConfig(contracts, config)
    const actions = [
      <RaisedButton key="Save" label="Save" primary={true} style={{marginRight: 20}} onClick={saveBufferedAssignUnassignActions}/>,
      <RaisedButton key="Cancel" label="Cancel" onClick={() => { closeDialog('AssignUnassignContracts') }} />
    ]
    return (<ReduxDialog contentStyle={{maxWidth: 1200}} dialogName="AssignUnassignContracts" title={`Manage Contracts Linked to ${firstName ? firstName + ' ' : ''}${lastName}`} actions={actions}
    modal={true} autoScrollBodyContent={true}>
      <Card style={{margin: '20px 0'}}>
        <Toolbar>
          <ToolbarGroup firstChild={true}>
            <RaisedButton id="AssignUnassignDialog-AssignContractsButton" label="Assign Contracts" primary={true} onClick={() => openDialog('AssignContracts')}/>
            <RaisedButton style={{marginLeft: 0}} label="Unassign Contracts" onClick={queueUnassignContracts}/>
          </ToolbarGroup>
          <ToolbarGroup lastChild={true}>
            <RaisedButton label="Reset" onClick={resetAssignUnassign}/>
          </ToolbarGroup>
        </Toolbar>
      </Card>
      <Grid sortBy="contractNumber" sortDirection={'asc'} options={options} data={contracts} columns={AssignUnassignContractsColumns()} onRowSelection={selectContractRow} />
    </ReduxDialog>)
  }
}

AssignUnassignContracts.propTypes = {
  selectedContact: T.object,
  openDialog: T.func.isRequired,
  closeDialog: T.func.isRequired,
  selectContractRow: T.func.isRequired,
  resetAssignUnassign: T.func.isRequired,
  saveBufferedAssignUnassignActions: T.func.isRequired,
  queueUnassignContracts: T.func.isRequired
}

const mapStateToProps = ({contacts: {selectedContact}}) => ({
  selectedContact
})

const actions = {
  openDialog,
  closeDialog,
  selectContractRow,
  queueUnassignContracts,
  resetAssignUnassign,
  saveBufferedAssignUnassignActions
}

export default connect(mapStateToProps, actions)(AssignUnassignContracts)
