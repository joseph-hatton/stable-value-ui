import _ from 'lodash'
import { browserHistory } from 'react-router'
import { change } from 'redux-form'
import { apiPath } from '../config'
import http from '../actions/http'
import { closeDialog, openSnackbar, openConfirmationAlert } from '../components/redux-dialog'

const CONTACTS_HOME = '/contacts'

export const loadAllContacts = () => (dispatch, getState) => {
  return dispatch({
    type: 'LOAD_ALL_CONTACTS',
    payload: http.get(`${apiPath}/contacts`).then(response => {
      if (!_.isEmpty(response.results)) {
        const {referenceData: {contactTypes}} = getState()
        const contactTypeMap = _.keyBy(contactTypes, 'id')
        const contacts = response.results.forEach(contact => {
          contact.contactTypeName = contactTypeMap[contact.contactType].text
          contact.name = `${contact.lastName}${contact.firstName ? `, ${contact.firstName}` : ''}`
        })
      }
      return response
    })
  })
}

export const loadAllContractsWithContactCompanies = () => (dispatch, getState) => {
  return dispatch({
    type: 'LOAD_ALL_CONTACTS_WITH_CONTACT_COMPANIES',
    payload: http.get(`${apiPath}/contracts`, {contactCompanies: true})
  })
}

export const saveContact = () => (dispatch, getState) => {
  const {form: {AddEditContactForm}, contacts: {selectedContact}} = getState()
  const method = selectedContact.contactId ? 'put' : 'post'
  const url = method === 'put' ? `${apiPath}/contacts/${selectedContact.contactId}` : `${apiPath}/contacts`
  const newReceives = _.pick(AddEditContactForm.values, ['receiveStatement', 'receiveInvoice', 'receiveScheduleA'])
  const oldReceives = _.pick(selectedContact, ['receiveStatement', 'receiveInvoice', 'receiveScheduleA'])

  const saveContact = () =>
    dispatch({
      type: 'SAVE_CONTACT',
      payload: http[method](url, _.omit(AddEditContactForm.values, 'companyName', 'name'), {successMessage: 'Contact Saved Successfully'}, 'addEditContact')
        .then((contact) => {
          dispatch(selectContact({}))
          loadAllContacts()(dispatch, getState)
          return contact
        })
    })

  if (_.isEqual(oldReceives, newReceives)) {
    saveContact()
  } else {
    dispatch(openConfirmationAlert({
      message: 'Do you want to apply changes to all existing contract assignments?',
      onOk: () => {
        Promise.all(_.map(selectedContact.contracts, ({contractId}) => http.get(`${apiPath}/contracts/${contractId}/contacts`).then((result) => {
          const contractContact = _.find(result.results, (contact) => contact.contactId === selectedContact.contactId)
          console.log('Found contact', {contractContact})
          return http.put(`${apiPath}/contracts/${contractId}/contacts/${contractContact.contactContractId}`, _.assign(contractContact, newReceives))
        }))).then(saveContact)
      },
      onCancel: saveContact,
      style: {
        zIndex: 1501
      }
    }))
  }
}

export const clearEditContactState = () => {
  return {
    type: 'CLEAR_EDIT_CONTACT'
  }
}
export const setAddContractState = () => {
  return {
    type: 'ADD_CONTACT'
  }
}

export const loadContact = (contactId) => (dispatch) => {
  dispatch({
    type: 'LOAD_CONTACT',
    payload: http.get(`${apiPath}/contacts/${contactId}`)
  })
}

export const populateCompanyAddress = (companyName) => (dispatch, getState) => {
  const {companies: {allCompanies: {results}}} = getState()
  const newCompany = _.find(results, company => company.name === companyName)
  if (newCompany) {
    dispatch(change('AddEditContactForm', 'companyId', newCompany.companyId))
    dispatch(change('AddEditContactForm', 'address1', newCompany.address1))
    dispatch(change('AddEditContactForm', 'address2', newCompany.address2))
    dispatch(change('AddEditContactForm', 'city', newCompany.city))
    dispatch(change('AddEditContactForm', 'state', newCompany.state))
    dispatch(change('AddEditContactForm', 'zipCode', newCompany.zipCode))
  }
}

export const deleteContact = (contact) => (dispatch) => {
  const {contactId, contracts} = contact
  dispatch({
    type: 'DELETE_CONTACT',
    payload: http.delete(`${apiPath}/contacts/${contactId}`, null, {successMessage: 'Contact Deleted Successfully'})
  })
}

export const selectContracts = ({contracts}) => ({
  type: 'SELECT_CONTRACTS',
  contracts
})

export const selectContact = (contact) => ({
  type: 'SELECTED_CONTACT',
  contact
})

export const selectAndLoadContact = ({contactId}) => ({
  type: 'SELECTED_CONTACT',
  payload: http.get(`${apiPath}/contacts/${contactId}`)
})

export const selectContractRow = (contracts = []) => ({
  type: 'SELECT_CONTRACT_ROW',
  contracts
})

export const queueUnassignContracts = () => (dispatch, getState) => {
  const {contacts: {selectedContracts}} = getState()
  dispatch({
    type: 'QUEUE_UNASSIGN_CONTRACTS',
    payload: selectedContracts
  })
}

export const queueAssignContracts = () => (dispatch, getState) => {
  const {contacts: {selectedContracts}} = getState()
  dispatch({
    type: 'QUEUE_ASSIGN_CONTRACTS',
    payload: selectedContracts
  })
  dispatch(closeDialog('AssignContracts'))
}

const CONTRACT_CONTACT_ASSIGNED_MESSAGE = {
  successMessage: 'Contact Assigned Successfully'
}

export const assignContactToContract = (contractId, contactId, selectedContact) => {
  const body = {contractId: contractId * 1, contactId: contactId * 1, receiveInvoice: selectedContact.receiveInvoice, receiveStatement: selectedContact.receiveStatement, receiveScheduleA: selectedContact.receiveStatement}
  return http.post(`${apiPath}/contracts/${contractId}/contacts`, body)
}

const CONTRACT_CONTACT_UNASSIGNED_MESSAGE = {
  successMessage: 'Contact Unassigned Successfully'
}

export const unAssignContactFromContract = (contractId, contactContractId) =>
  http.delete(`${apiPath}/contracts/${contractId}/contacts/${contactContractId}`, {})

const unassignContracts = (contracts) => {
  return Promise.all(_.map(contracts, ({contractId, contactContractId}) =>
    unAssignContactFromContract(contractId, contactContractId)
  ))
}
const assignContracts = (contracts, contactId, selectedContact) => {
  return Promise.all(_.map(contracts, ({contractId}) =>
    assignContactToContract(contractId, contactId, selectedContact)
  ))
}

export const saveBufferedAssignUnassignActions = () => async (dispatch, getState) => {
  const {contacts: {
    bufferedActions: {unassignContracts: uContracts, assignContracts: aContracts},
    selectedContact,
    selectedContact: {contactId}}
  } = getState()

  let successMessage
  let errorMessage = 'There was a problem.'
  if (uContracts.length > 0 && aContracts.length > 0) {
    successMessage = 'Contract(s) assigned and unassigned successfully'
    errorMessage = 'Contract(s) were not assigned and unassigned successfully'
  } else if (uContracts.length > 0) {
    successMessage = 'Contract(s) unassigned successfully'
    errorMessage = 'Contract(s) were not unassigned successfully'
  } else if (aContracts.length > 0) {
    successMessage = 'Contract(s) assigned successfully'
    errorMessage = 'Contract(s) were not assigned successfully'
  }

  try {
    await Promise.all([
      unassignContracts(uContracts),
      assignContracts(aContracts, contactId, selectedContact)
    ])

    if (successMessage) {
      await dispatch(loadAllContacts())
      dispatch(resetAssignUnassign())
      dispatch(openSnackbar({
        type: 'success',
        message: successMessage,
        autoHideDuration: 5000
      }))
    }
    dispatch(closeDialog('AssignUnassignContracts'))
  } catch (err) {
    dispatch(openSnackbar({
      type: 'error',
      message: errorMessage,
      autoHideDuration: 5000 * 1000
    }))
  }
}

export const resetAssignUnassign = () => ({ type: 'RESET_ASSIGN_UNASSIGN' })
