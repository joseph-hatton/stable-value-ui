import React from 'react'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'

export const ContractManagerText = ({managers, managerId}) => {
  console.log(`ContractManagerText ${managerId}`)
  const manager = (managerId) ? managers.find(manager => manager.value.toString() === managerId.toString()) : null
  return <div>{manager ? manager.text : managerId}</div>
}

ContractManagerText.propTypes = {
  managerId: T.string.isRequired,
  managers: T.array
}

ContractManagerText.displayName = 'ContractManagerText'

export const mapStateToProps = ({contacts: {managers = []}}) => {
  return {
    managers
  }
}

export default connect(mapStateToProps)(ContractManagerText)
