import React from 'react'
import _ from 'lodash'
import { connect } from 'react-redux'
import { PropTypes as T } from 'prop-types'
import RaisedButton from 'material-ui/RaisedButton'
import {openDialog, closeDialog} from '../components/redux-dialog'
import ReduxDialog from '../components/redux-dialog/redux-dialog'
import { Grid, CreateGridConfig } from '../components/grid'
import { selectContractRow, queueAssignContracts, loadAllContractsWithContactCompanies } from './ContactActions'
import { loadAllCompanies } from '../companies/CompaniesActions'
import { AssignUnassignContractsColumns } from './AssignUnassignContractsColumns'
import AssignContractsFilters from './AssignContractsFilters'

const config = {
  selectable: true,
  enableSelectAll: true,
  multiSelectable: true,
  deselectOnClickaway: false,
  showHeaderToolbar: false,
  height: 400,
  paginate: false
}

class AssignContracts extends React.Component {
  componentDidMount () {
    const {contracts, loadAllContractsWithContactCompanies, companies, loadAllCompanies} = this.props
    if (_.isEmpty(contracts)) {
      loadAllContractsWithContactCompanies()
    }
    if (_.isEmpty(companies)) {
      loadAllCompanies()
    }
  }
  render () {
    const {selectedContact: {firstName, lastName}, contracts, closeDialog, selectContractRow, queueAssignContracts} = this.props

    const options = CreateGridConfig(contracts, config)

    const actions = [
      <RaisedButton key="Save" label="Assign Contracts" primary={true} style={{marginRight: 20}} onClick={queueAssignContracts} />,
      <RaisedButton key="Cancel" label="Cancel" onClick={() => { closeDialog('AssignContracts') }} />
    ]
    return (<ReduxDialog contentStyle={{maxWidth: 1200}} dialogName="AssignContracts" title={`Assign Contracts to ${firstName ? firstName + ' ' : ''}${lastName}`} actions={actions}
    modal={true} autoScrollBodyContent={true}>
      <AssignContractsFilters/>
      <Grid sortBy="contractNumber" sortDirection={'asc'} options={options} data={contracts} columns={AssignUnassignContractsColumns()} onRowSelection={selectContractRow} />
    </ReduxDialog>)
  }
}

AssignContracts.propTypes = {
  selectedContact: T.object,
  contracts: T.array,
  companies: T.array,
  closeDialog: T.func.isRequired,
  selectContractRow: T.func.isRequired,
  loadAllContractsWithContactCompanies: T.func.isRequired,
  loadAllCompanies: T.func.isRequired,
  queueAssignContracts: T.func.isRequired
}

const mapStateToProps = ({form, contacts: {selectedContact, allContacts: contacts, allContracts: contracts}, companies: {allCompanies: {results: companies}}}) => {
  const filterValues = _.get(form, 'AssignContractsFiltersForm.values')

  if (!_.isEmpty(filterValues)) {
    if (filterValues.status != null) {
      contracts = _.filter(contracts, _.pick(filterValues, ['status']))
    }

    if (filterValues.shortPlanName != null) {
      filterValues.shortPlanName = filterValues.shortPlanName.toLowerCase()
      contracts = _.filter(contracts, ({shortPlanName}) => shortPlanName.toLowerCase().includes(filterValues.shortPlanName))
    }

    if (filterValues.companyName != null) {
      contacts = _.filter(contacts, _.pick(filterValues, ['companyName']))
    }

    if (filterValues.contactType != null) {
      contacts = _.filter(contacts, _.pick(filterValues, ['contactType']))
    }

    if (filterValues.lastName != null) {
      filterValues.lastName = filterValues.lastName.toLowerCase()
      contacts = _.filter(contacts, ({lastName}) => lastName.toLowerCase().includes(filterValues.lastName))
    }

    // Merge in Contacts if needed
    if (!_.isEmpty(_.pick(filterValues, ['companyName', 'contactType', 'lastName']))) {
      const contractsFromContacts = _.reduce(contacts, (results, contact) => results.concat(contact.contracts), [])
      contracts = _.intersectionBy(contracts, contractsFromContacts, 'contractId')
    }
  }

  return {
    selectedContact,
    contracts,
    companies
  }
}

const actions = {
  openDialog,
  closeDialog,
  selectContractRow,
  loadAllContractsWithContactCompanies,
  loadAllCompanies,
  queueAssignContracts
}

export default connect(mapStateToProps, actions)(AssignContracts)
