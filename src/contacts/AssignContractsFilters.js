import React from 'react'
import _ from 'lodash'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import {reduxForm, reset, Field} from 'redux-form'
import RaisedButton from 'material-ui/RaisedButton'
import {TextField} from 'redux-form-material-ui'
import Card from 'material-ui/Card/Card'
import ContractStatusesSelectField from '../components/forms/ContractStatusesSelectField'
import MaterialSelectField from '../components/forms/MaterialSelectField'
import ContactTypesFormField from '../components/forms/ContactTypesFormField'

const formId = 'AssignContractsFiltersForm'

const style = {
  wrapper: {
    backgroundColor: '#fff',
    height: 180
  },
  options: {
    display: 'grid',
    gridTemplateColumns: '33% 33% 33%',
    gridGap: 10,
    padding: '0 20px'
  },
  clear: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
}

export const AssignContractsFilters = ({clearAll, companies}) => {
  return (
    <Card style={{margin: '20px 0'}}>
      <form id={formId}>
        <div style={style.wrapper}>
          <div style={style.options}>
            <ContractStatusesSelectField name="status"/>
            <Field fullWidth name="shortPlanName" component={TextField} floatingLabelText="Short Plan Name"/>
            <MaterialSelectField name="companyName" floatingLabelText="Contact Company" options={companies} />
            <ContactTypesFormField name="contactType" floatingLabelText="Contact Type" />
            <Field fullWidth name="lastName" component={TextField} floatingLabelText="Contact Last Name"/>
            <div style={style.clear}>
              <RaisedButton label="Clear All" onClick={clearAll}/>
            </div>
          </div>
        </div>
      </form>
    </Card>
  )
}

AssignContractsFilters.propTypes = {
  clearAll: T.func.isRequired,
  companies: T.array
}

const AssignContractsFiltersForm = reduxForm({ form: formId })(AssignContractsFilters)

export const actions = {
  clearAll: () => (dispatch) => {
    dispatch(reset('AssignContractsFiltersForm'))
  }
}

export const mapStateToProps = ({companies: {allCompanies: {results: companies}}}) => ({
  companies: companies.map((company) => ({text: company.name, value: company.name}))
})

export default connect(mapStateToProps, actions)(AssignContractsFiltersForm)
