import React from 'react'
import { PropTypes as T } from 'prop-types'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import AssignIcon from 'material-ui/svg-icons/content/link'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import { blue500, red500 } from 'material-ui/styles/colors'
import { connect } from 'react-redux'
import { Popover, Menu, MenuItem } from 'material-ui'
import { openDialog, closeDialog, openConfirmationAlert } from '../components/redux-dialog/redux-dialog'
import { selectContact, deleteContact } from './ContactActions'
import { browserHistory } from 'react-router'

const MenuLink = ({cell, openDialog, row, selectContact}) => {
  return (
    <a href="" onClick={(e) => {
      e.nativeEvent.preventDefault()
      selectContact(row)
      openDialog('contactMenu', {
        anchorEl: e.currentTarget
      })
    }}> {cell} </a>
  )
}

MenuLink.propTypes = {
  cell: T.node,
  row: T.object.isRequired,
  openDialog: T.func.isRequired,
  selectContact: T.func.isRequired
}

export const ContactMenuLink = connect(null, {openDialog, selectContact})(MenuLink)

const Divider = () => (<hr style={{margin: 0}}/>)

const ContactMenu = ({open, anchorEl, closeDialog, openDialog, selectedContact, deleteContact, openConfirmationAlert}) => (
  <Popover
    open={open}
    anchorEl={anchorEl}
    anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
    targetOrigin={{horizontal: 'left', vertical: 'top'}}
    onRequestClose={() => {
      closeDialog('contactMenu')
    }}
  >
    <Menu>
      <MenuItem id="ContactMenu-viewEditContactButton" primaryText="View/Edit Contact" onClick={() => {
        closeDialog('contactMenu')
        openDialog('addEditContact')
      }} leftIcon={<EditIcon color={blue500}/>}/>
      <Divider/>
      <MenuItem id="ContactMenu-AssignUnassignContractsButton" primaryText="Assign/Unassign Contracts" onClick={() => {
        closeDialog('contactMenu')
        openDialog('AssignUnassignContracts')
      }} leftIcon={<AssignIcon color={blue500}/>}/>
      <Divider/>
      <MenuItem id="ContactMenu-DeleteContactButton" disabled={selectedContact.contracts && selectedContact.contracts.length > 0} primaryText="Delete Contact" onClick={() => {
        closeDialog('contactMenu')
        openConfirmationAlert({
            message: 'Are you sure you want to delete this contact?',
            onOk: () => deleteContact(selectedContact)
        })
      }} leftIcon={<DeleteIcon color={red500}/>}/>
    </Menu>
  </Popover>
)

ContactMenu.propTypes = {
  open: T.bool.isRequired,
  anchorEl: T.object,
  closeDialog: T.func.isRequired,
  openDialog: T.func.isRequired,
  deleteContact: T.func.isRequired,
  openConfirmationAlert: T.func.isRequired,
  selectedContact: T.array.isRequired
}

export const mapStateToProps = ({dialog: {contactMenu}, contacts: {selectedContact}}) => ({
  open: !!contactMenu,
  anchorEl: contactMenu ? contactMenu.anchorEl : null,
  selectedContact
})

const actions = {
  closeDialog,
  openDialog,
  deleteContact,
  openConfirmationAlert
}

export default connect(mapStateToProps, actions)(ContactMenu)
