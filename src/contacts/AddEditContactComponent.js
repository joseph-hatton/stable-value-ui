import React from 'react'
import _ from 'lodash'
import { PropTypes as T } from 'prop-types'
import { Link } from 'react-router'
import { Card } from 'material-ui/Card'
import RaisedButton from 'material-ui/RaisedButton'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import ReduxDialog from '../components/redux-dialog'
import { closeDialog } from '../components/redux-dialog/redux-dialog'
import IconButton from 'material-ui/IconButton'
import SendIcon from 'material-ui/svg-icons/content/send'
import { red500 } from 'material-ui/styles/colors'
import styles from '../styles/styles'
import {saveContact, loadContact, populateCompanyAddress, setAddContractState, selectContact} from './ContactActions'
import {
  AutoComplete,
  TextField
} from 'redux-form-material-ui'
import { Required, IsValidZipCode, Email, NormalizePhone, phoneNumber } from '../components/forms/Validators'
import StatesFormField from '../components/forms/StatesFormField'
import ContactTypesFormField from '../components/forms/ContactTypesFormField'
import BooleanSelectField from '../components/forms/BooleanSelectField'
import CaseInSensitiveFilter from '../components/forms/CaseInSensitiveFilter'
import { loadAllCompanies } from '../companies/CompaniesActions'

const sectionStyle = {
  padding: '10px 20px 10px 20px',
  marginTop: '20px'
}

export class AddEditContact extends React.Component {
  componentWillUpdate ({companies, loadAllCompanies, setAddContractState, selectedContact: {contactId}}) {
    if (_.isEmpty(companies)) {
      loadAllCompanies()
    }
  }

  onCompanyChange (event, companyId) {
    this.props.populateCompanyAddress(companyId)
  }

  render () {
    const {submitting, companies, handleSubmit, closeDialog, saveContact, emailAddress, selectContact} = this.props

    const actions = [
      <RaisedButton key="save" form='AddEditContactForm' label="Save" type="submit" primary={true} style={{marginRight: 15}} disabled={submitting}/>,
      <RaisedButton key="cancel" label="Cancel" onClick={() => { closeDialog('addEditContact'); selectContact({}) }}/>
    ]

    return (
      <ReduxDialog contentStyle={{maxWidth: 1000}} dialogName='addEditContact' title='Add/Edit Contact' modal={true} autoScrollBodyContent={true} actions={actions}>
        <form id='AddEditContactForm' onSubmit={handleSubmit(saveContact)} className="stable-value-form">
          <div className="row">
            <div className="col-lg-8">
              <Field
                fullWidth
                id="companyNameId"
                name="companyName"
                component={AutoComplete}
                floatingLabelText="Select Company"
                dataSource={companies}
                filter={CaseInSensitiveFilter}
                onChange={::this.onCompanyChange}
                multiLine={true}
                validate={Required}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-lg-4">
              <div className="row">
                <div className="col-lg-4">
                  <Field fullWidth name="salutation" component={TextField} floatingLabelText="Salutation"/>
                </div>
                <div className="col-lg-8">
                  <Field fullWidth component={TextField} name="firstName" floatingLabelText="First Name"/>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <Field fullWidth component={TextField} name="middleName" floatingLabelText="Middle Name"/>
            </div>
            <div className="col-lg-4">
              <Field fullWidth component={TextField} name="lastName" floatingLabelText="* Last Name"
                      validate={Required}/>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-4">
              <Field fullWidth component={TextField} name="address1" floatingLabelText="* Address 1"
                      validate={Required}/>
            </div>
            <div className="col-lg-4">
              <Field fullWidth component={TextField} name="address2" floatingLabelText="Address 2"/>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-4">
              <Field fullWidth component={TextField} name="city" floatingLabelText="* City" validate={Required}/>
            </div>
            <div className="col-lg-4">
              <StatesFormField name="state"/>
            </div>
            <div className="col-lg-4">
              <Field fullWidth component={TextField} name="zipCode" floatingLabelText="* Zip Code"
                      validate={[Required, IsValidZipCode]}/>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-4">
              <Field fullWidth component={TextField} normalize={NormalizePhone} name="primaryPhone"
                      floatingLabelText="* Phone Number"
                      validate={[Required, phoneNumber]}/>
            </div>
            <div className="col-lg-4">
              <Field fullWidth component={TextField} normalize={NormalizePhone} name="cellNumber"
                      floatingLabelText="Cell Number" validate={phoneNumber}/>
            </div>
            <div className="col-lg-4">
              <Field fullWidth component={TextField} normalize={NormalizePhone} name="fax1"
                      floatingLabelText="Fax Number" validate={phoneNumber}/>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <Field fullWidth component={TextField} name="emailAddress" floatingLabelText="* Email Address"
                      validate={[Required, Email]}/>
            </div>
            <div style={styles.topMargin} className="col-lg-1">
              <a href={`mailto:${encodeURIComponent(emailAddress)}`}>
                <IconButton touch={true}>
                  <SendIcon color={red500} />
                </IconButton>
              </a>
            </div>
            <div className="col-lg-4">
              <ContactTypesFormField name="contactType" validate={Required}/>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-4">
              <BooleanSelectField name="receiveStatement" floatingLabelText="* Receive Statement"/>
            </div>
            <div className="col-lg-4">
              <BooleanSelectField name="receiveInvoice" floatingLabelText="* Receive Invoice"/>
            </div>
            <div className="col-lg-4">
              <BooleanSelectField name="receiveScheduleA" floatingLabelText="* Receive Schedule A"/>
            </div>
          </div>
        </form>
      </ReduxDialog>)
  }
}

AddEditContact.propTypes = {
  companies: T.array,
  selectedContact: T.object,
  saveContact: T.func.isRequired,
  populateCompanyAddress: T.func.isRequired,
  loadAllCompanies: T.func.isRequired,
  submitting: T.bool,
  asyncValidating: T.bool,
  initialValues: T.object,
  handleSubmit: T.func.isRequired,
  loadContact: T.func.isRequired,
  closeDialog: T.func.isRequired,
  setAddContractState: T.func.isRequired,
  selectContact: T.func.isRequired,
  emailAddress: T.string
}

export const AddEditContactForm = reduxForm({
  form: 'AddEditContactForm',
  enableReinitialize: true
})(AddEditContact)

export const mapStateToProps = ({form, contacts: {selectedContact}, companies: {allCompanies}}) => ({
  emailAddress: _.get(form, 'AddEditContactForm.values.emailAddress'),
  selectedContact,
  initialValues: selectedContact,
  companies: _.map(allCompanies.results || [], 'name')
})

const actions = {
  closeDialog,
  saveContact,
  loadContact,
  loadAllCompanies,
  populateCompanyAddress,
  setAddContractState,
  selectContact
}

export default connect(mapStateToProps, actions)(AddEditContactForm)
