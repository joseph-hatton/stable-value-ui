import React from 'react'
import { PropTypes as T } from 'prop-types'
import MaterialSelectField from '../components/forms/MaterialSelectField'
import { connect } from 'react-redux'
import { loadAllContacts } from './ContactActions'
import _ from 'lodash'

export class ManagersFormField extends React.Component {
  componentDidMount () {
    const {options, loadAllContacts} = this.props
    if (_.isEmpty(options)) {
      loadAllContacts()
    }
  }

  render () {
    return (<MaterialSelectField {..._.omit(this.props, ['loadAllContacts'])} options={this.props.options}/>)
  }
}

ManagersFormField.propTypes = {
  name: T.string.isRequired,
  options: T.array,
  loadAllContacts: T.func.isRequired
}

ManagersFormField.displayName = 'ManagersFormField'

export const mapStateToProps = ({contacts: {managers}}) => {
  return {
    options: managers || []
  }
}

export default connect(mapStateToProps, {loadAllContacts})(ManagersFormField)
