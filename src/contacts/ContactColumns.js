import React from 'react'
import { Link } from 'react-router'
import { ContractsMenuLink } from './ContractsMenu'
import { ContactMenuLink } from './ContactMenu'
import { selectContact } from './ContactActions'

const BooleanFormatter = (cell) => {
  return cell ? 'Yes' : 'No'
}

export const ContactColumns = [
  {
    field: 'id',
    customProps: {
      hidden: true,
      isKey: true
    }
  },
  {
    field: 'name',
    sort: true,
    label: 'Name',
    customProps: {
      textAlignHeader: 'left',
      width: '20%'
    },
    renderer: (cell, row) => { // eslint-disable-line react/display-name
      return (
        <ContactMenuLink row={row} cell={cell} />
      )
    }
  },
  {
    field: 'contactType',
    sort: true,
    label: 'Type',
    customProps: {
      textAlignHeader: 'left',
      width: '15%'
    }
  },
  {
    field: 'companyName',
    sort: true,
    label: 'Company',
    customProps: {
      width: '30%',
      textAlign: 'left'
    }
  },
  {
    field: 'receiveStatement',
    sort: true,
    label: 'Statement',
    renderer: BooleanFormatter,
    customProps: {
      width: '10%',
      textAlign: 'center'
    }
  },
  {
    field: 'receiveInvoice',
    sort: true,
    label: 'Invoice',
    renderer: BooleanFormatter,
    customProps: {
      width: '7%',
      textAlign: 'center'
    }

  },
  {
    field: 'receiveScheduleA',
    sort: true,
    label: 'Schedule A',
    renderer: BooleanFormatter,
    customProps: {
      width: '10%',
      textAlign: 'center'
    }
  },
  {
    field: 'contracts',
    sort: true,
    label: 'Contracts',
    customProps: {
      width: '10%',
      textAlign: 'center'
    },
    renderer: (cell, row) => ( // eslint-disable-line react/display-name
      <ContractsMenuLink row={row} cell={cell.length}/>
    )
  }
]
