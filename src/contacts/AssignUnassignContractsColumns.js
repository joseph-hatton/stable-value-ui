import React from 'react'
import _ from 'lodash'

const customProps = {
  textAlign: 'center',
  paddingRight: 20
}

export const AssignUnassignContractsColumns = () => ([
  {
    field: 'contractId',
    customProps: {
      hidden: true,
      isKey: true
    }
  },
  {
    field: 'contractNumber',
    sort: true,
    label: 'Contract #',
    customProps: _.assign({}, customProps, {width: '18%'})
  },
  {
    field: 'shortPlanName',
    sort: true,
    label: 'Short Plan Name',
    renderer: (cell) => (<span title={cell}>{cell}</span>), // eslint-disable-line react/display-name
    customProps: _.assign({}, customProps, {width: '18%'})
  },
  {
    field: 'status',
    sort: true,
    label: 'Status',
    customProps: _.assign({}, customProps, {width: '18%'})
  },
  {
    field: 'contactCompanyNames',
    sort: true,
    label: 'Contact Companies',
    renderer: (cell) => ( // eslint-disable-line react/display-name
      <span title={(cell || []).join(', ')}>{(cell || []).join(', ')}</span>
    ),
    customProps: _.assign({}, customProps, {width: '46%'})
  }
])
