import React from 'react'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import { openDialog } from './redux-dialog'

const createDialogLinkCell = (dialogName, selectDialogData) => {
  const DialogLinkCell = ({row, selectDialogData, openDialog, children}) => (
    <a href="" onClick={(e) => {
      e.nativeEvent.preventDefault()
      selectDialogData(row)
      openDialog(dialogName)
    }}> {children} </a>
  )
  DialogLinkCell.propTypes = {
    row: T.object,
    children: T.element,
    selectDialogData: T.func.isRequired,
    openDialog: T.func.isRequired
  }
  return connect(null, {selectDialogData, openDialog})(DialogLinkCell)
}

export default createDialogLinkCell
