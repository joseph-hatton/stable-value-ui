import React from 'react'
import { PropTypes as T } from 'prop-types'
import localStorage from 'localStorage'
import { connect } from 'react-redux'
import _ from 'lodash'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import withWidth from 'material-ui/utils/withWidth'

import BreadCrumbs from 'react-breadcrumbs'

import Header from '../components/common/Header'
import LeftDrawer from '../components/common/LeftDrawer'

import { Snackbar, ConfirmationAlert, MaterialSpinner, openConfirmationAlert } from '../components/redux-dialog'
import ThemeDefault from '../styles/theme-default'
import menus from './menus'

const LEFT_NAV_DRAWER_STATE = 'left-nav-drawer-state'

class App extends React.Component {
  constructor (props) {
    super(props)
    const savedState = localStorage.getItem(LEFT_NAV_DRAWER_STATE)
    this.state = {
      navDrawerOpen: savedState === null ? true : savedState === 'true'
    }
  }

  componentWillReceiveProps ({timedOut}) {
    const {openConfirmationAlert} = this.props
    if (timedOut) {
      openConfirmationAlert({
        message: 'Your session has terminated.  Please log back in.',
        onOk: () => location.reload(),
        disableCancel: true
    })
    }
  }

  handleChangeRequestNavDrawer () {
    const newState = !this.state.navDrawerOpen
    this.setState({
      navDrawerOpen: newState
    })
    localStorage.setItem(LEFT_NAV_DRAWER_STATE, newState)
  }

  render () {
    let {navDrawerOpen} = this.state
    const paddingLeft = navDrawerOpen ? 220 : 0

    const styles = {
      header: {
        paddingLeft
      },
      container: {
        margin: '60px 5px 20px 15px',
        paddingLeft
      }
    }

    const {routes, params, username, roles, id} = this.props

    const stableValueBreadCrumbs = (<BreadCrumbs routes={_.tail(routes)} params={params}/>)

    return (
      <MuiThemeProvider muiTheme={ThemeDefault}>
        <div>
          <Header styles={styles.header}
                  handleChangeRequestNavDrawer={this.handleChangeRequestNavDrawer.bind(this)}
                  breadCrumbs={stableValueBreadCrumbs}
          />
          <LeftDrawer navDrawerOpen={navDrawerOpen}
                      menus={menus}
                      username={username}
                      roles={roles}
                      id={id}/>
          <div style={styles.container}>
            <MaterialSpinner/>
            {this.props.children}
            <Snackbar />
            <ConfirmationAlert />
          </div>
        </div>
      </MuiThemeProvider>
    )
  }
}

App.propTypes = {
  children: T.element,
  width: T.number,
  username: T.string,
  routes: T.array,
  roles: T.array,
  id: T.string,
  params: T.object,
  openConfirmationAlert: T.func.isRequired,
  timedOut: T.bool.isRequired
}

const actions = {
  openConfirmationAlert
}

const groupToRoles = {
  app_stable_value_ops_admin: 'Admin',
  app_stable_value_it_support: 'Support',
  app_stable_value_compliance: 'Compliance',
  app_stable_value_risk: 'Risk'
}

export const mapGroupsToRoles = (groups) => {
  const mappedRoles = Object.keys(groupToRoles)
  return _(groups)
    .filter((group) => (_.some(mappedRoles, (role) => role === group)))
    .map((item) => ({
      text: groupToRoles[item],
      role: item
    }))
    .value()
}

export const mapStateToProps = ({currentUser: {firstName, lastName, id, groups, session: {timedOut}}}) => ({
  timedOut: Boolean(timedOut),
  username: `${firstName} ${lastName}`,
  roles: mapGroupsToRoles(groups),
  id
})

// export default withWidth()(App)
export default connect(mapStateToProps, actions)(withWidth()(App))
