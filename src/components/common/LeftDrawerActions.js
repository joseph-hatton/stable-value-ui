import _ from 'lodash'
import {canNavigateAwayFromContracts} from '../../contracts/ContractActions'
import {openConfirmationAlert} from '../redux-dialog'
import store from '../../store/store'

export const setOrToggleListItem = (text, value) => ({
  type: 'SET_SUBLIST_VALUE',
  value,
  text
})

export const goToTab = (tab) => ({
  type: 'GO_TO_TAB',
  tab
})

export const onEnterTabbed = (tab) => (nextState) => store.dispatch(goToTab(tab))

export const onNestedListToggle = ({handleStateManually, tab}, sideBar) => (dispatch) => (e) => {
  handleStateManually && dispatch(setOrToggleListItem(tab, !sideBar[tab]))
}
