import React from 'react'
import {PropTypes as T} from 'prop-types'
import {Card, CardHeader, CardText} from 'material-ui/Card'
import Clear from 'material-ui/svg-icons/content/clear'
import localStorage from 'localStorage'

const headerStyle = {cursor: 'pointer'}

export default class Expandable extends React.Component {
  constructor (props) {
    super(props)
    this.localStorageKey = `${props.id}-expanded`
    this.state = {
      expanded: localStorage.getItem(this.localStorageKey) === 'true'
    }
  }

  handleExpandChange (expanded) {
    this.setState({expanded: expanded})
    localStorage.setItem(this.localStorageKey, expanded)
  }

  toggle () {
    const next = !this.state.expanded
    this.setState({expanded: next})
    localStorage.setItem(this.localStorageKey, next)
  }

  render () {
    const {title, icon, children, style = {}, contentStyle = {}} = this.props
    return (
      <Card containerStyle={{paddingBottom: 0}} style={style} expanded={this.state.expanded} onExpandChange={::this.handleExpandChange}>
        <CardHeader onClick={::this.toggle} title={title} showExpandableButton={true} closeIcon={icon}
                    openIcon={<Clear/>} style={headerStyle}/>
        <CardText style={{...contentStyle, display: this.state.expanded ? '' : 'none'}}>
          {children}
        </CardText>
      </Card>
    )
  }
}

Expandable.propTypes = {
  id: T.string.isRequired,
  icon: T.element.isRequired,
  children: T.element.isRequired,
  style: T.object,
  contentStyle: T.object,
  title: T.any.isRequired
}
