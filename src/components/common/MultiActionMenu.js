import React from 'react'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import { Popover, Menu, MenuItem } from 'material-ui'
import { openDialog, closeDialog } from '../../components/redux-dialog/redux-dialog'

const MenuLink = ({cell, openDialog, row, selections, dialogName}) => {
  return (
    <a href="" onClick={(e) => {
      e.preventDefault()
      selections(row)
      openDialog(dialogName, {
        anchorEl: e.currentTarget
      })
    }}> {cell} </a>
  )
}

MenuLink.propTypes = {
  cell: T.node,
  row: T.object.isRequired,
  openDialog: T.func.isRequired,
  selections: T.func.isRequired,
  dialogName: T.string
}

export const MultiActionMenuLink = connect(null, {openDialog})(MenuLink)

const MultiActionMenu = ({open, anchorEl, closeDialog, openDialog, dialogName, dialog, children}) => (
  <Popover
    open={!!dialog[dialogName]}
    anchorEl={dialog[dialogName] && dialog[dialogName].anchorEl}
    anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
    targetOrigin={{horizontal: 'left', vertical: 'top'}}
    onRequestClose={() => {
     closeDialog(dialogName)
    }}
  >
    {children}
  </Popover>
)

MultiActionMenu.propTypes = {
  open: T.bool.isRequired,
  anchorEl: T.object,
  closeDialog: T.func.isRequired,
  openDialog: T.func.isRequired,
  deleteWatchList: T.func.isRequired,
  dialog: T.object,
  dialogName: T.string,
  children: T.node
}

export const mapStateToProps = ({dialog}) => ({
  dialog
})

const actions = {
  closeDialog,
  openDialog
}

export default connect(mapStateToProps, actions)(MultiActionMenu)
