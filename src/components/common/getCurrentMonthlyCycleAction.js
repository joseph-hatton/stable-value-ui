import React from 'react'
import http from '../../actions/http'
import { apiPath } from '../../config'

const getCurrentMonthlyCycleAction = () => (dispatch, getState) => {
    const {monthlyCycle: {currentOpenMonth}} = getState()
    if (!currentOpenMonth) {
        dispatch({
            type: 'LOAD_MONTHLY_CYCLE',
            payload: http.get(`${apiPath}/monthly-cycles`).then((res) => res.results[0])
        })
    }
}

export default getCurrentMonthlyCycleAction
