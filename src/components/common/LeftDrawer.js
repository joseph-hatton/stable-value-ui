import React from 'react'
import { PropTypes as T } from 'prop-types'
import _ from 'lodash'
import { connect } from 'react-redux'
import Drawer from 'material-ui/Drawer'
import { white, red500 } from 'material-ui/styles/colors'
import { Link, withRouter } from 'react-router'
import { List, ListItem } from 'material-ui/List'
import {ConfirmationAlert} from '../redux-dialog'
import style from '../../styles/styles'
import { onNestedListToggle } from './LeftDrawerActions'

const styles = {
  nestedStylesLevel0: {backgroundColor: '#393939', padding: 0},
  nestedStylesLevel1: {backgroundColor: '#3f3f3f', padding: 0}
}

const getSubMenuItems = ({showSubMenus, subMenus, props, level}) => {
  if (!_.isEmpty(subMenus) && showSubMenus && showSubMenus(props.appState)) {
    return subMenus
      .filter(subMenu => subMenu.showSubMenu(props.appState))
      .map(subMenu => createMenuItem({menuItem: subMenu, props, level}))
  }
  return []
}

const createMenuItem = ({menuItem, props, level = 0}) => {
  const {sideBar, appState, onNestedListToggle, location} = props // eslint-disable-line react/prop-types
  const {text, tab, icon, rightIcon, initiallyOpen, showSubMenus, subMenus, link: menuLink} = menuItem

  const link = _.isFunction(menuLink) ? menuLink(appState) : menuLink

  const listItemProps = {
    nestedItems: getSubMenuItems({showSubMenus, subMenus, props, level: level + 1}),
    ...(rightIcon ? rightIcon(appState) : {}),
    className: 'NavListItem',
    primaryText: <Link style={{textDecoration: 'none'}} to={link} onlyActiveOnIndex={true} activeClassName="NavLinkActive"><div style={_.assign({}, style.leftDrawer.menuItem, {paddingLeft: 16 + (18 * level)})}>{text}</div></Link>,
    nestedListStyle: styles[`nestedStylesLevel${level}`],
    hoverColor: '#222',
    leftIcon: icon,
    open: sideBar[tab],
    onNestedListToggle: onNestedListToggle(menuItem, sideBar),
    initiallyOpen,
    innerDivStyle: {marginLeft: 0, padding: 0, paddingRight: 0}
  }

  return (<ListItem key={text} {...listItemProps}/>)
}

createMenuItem.propTypes = {
  menuItem: T.object,
  props: T.object,
  level: T.number
}

const LeftDrawer = (props) => {
  let {navDrawerOpen, sideBar, router} = props

  const styles = style.leftDrawer

  return (
    <div>
      <Drawer
        docked={true}
        open={navDrawerOpen}
        containerStyle={{backgroundColor: '#333'}}>
        <div onClick={() => {
          router.push('/')
        }} style={styles.logo}>
          Stable Value
        </div>
        <div style={styles.avatar.div}>
          <span style={styles.avatar.username}>{props.username}</span>
          <span style={styles.avatar.roles}>{_.map(props.roles, ({text}) => text).join(', ')}</span>
        </div>
        <List className="NavList">
          {props.menus.map((menuItem, index) => createMenuItem({menuItem, props}))}
        </List>
      </Drawer>
      <ConfirmationAlert dialogName="navigateAwayFromContract" />
    </div>
  )
}

LeftDrawer.displayName = 'LeftDrawer'

LeftDrawer.propTypes = {
  navDrawerOpen: T.bool,
  router: T.object,
  menus: T.array,
  username: T.string,
  roles: T.array,
  appState: T.object.isRequired,
  sideBar: T.object.isRequired
}

export const mapStatesToProps = (state) => ({
  appState: state,
  sideBar: state.sideBar
})

const actions = {
  onNestedListToggle
}

export default withRouter(connect(mapStatesToProps, actions)(LeftDrawer))
