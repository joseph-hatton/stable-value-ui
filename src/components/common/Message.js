import React from 'react'
import { PropTypes as T } from 'prop-types'
import { Card, CardText } from 'material-ui/Card'
import { red500, green500, yellow500 } from 'material-ui/styles/colors'
import Clear from 'material-ui/svg-icons/content/clear'

export const ERROR = 'error'
export const SUCCESS = 'success'
export const WARNING = 'warning'

const typeColorMap = {
  [ERROR]: red500,
  [SUCCESS]: green500,
  [WARNING]: yellow500
}

const DISPLAY = { display: true }
const HIDE = { display: false }

class Message extends React.Component {
  constructor (props) {
    super(props)
    this.state = DISPLAY
  }

  hide () {
    this.setState(HIDE)
    if (this.props.onHide) {
      this.props.onHide()
    }
  }

  render () {
    const { type, children } = this.props
    return (
      <div>
        {
          this.state.display && (
            <Card style={{ marginBottom: '20px' }}>
                <Clear onClick={::this.hide} hoverColor={red500} style={{ position: 'relative', float: 'right', right: '10px', top: '20px' }} />
                <CardText style={{ color: typeColorMap[type] }}>
                  { children }
                </CardText>
            </Card>
          )
        }
      </div>
    )
  }
}

Message.propTypes = {
  type: T.string.isRequired,
  children: T.array.isRequired,
  onHide: T.func
}

export default Message
