import React from 'react'
import _ from 'lodash'
import { PropTypes as T } from 'prop-types'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import AppBar from 'material-ui/AppBar'
import IconButton from 'material-ui/IconButton'
import IconMenu from 'material-ui/IconMenu'
import MenuItem from 'material-ui/MenuItem'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import Menu from 'material-ui/svg-icons/navigation/menu'
import ViewModule from 'material-ui/svg-icons/action/view-module'
import Warning from 'material-ui/svg-icons/alert/warning'
import { white, red500 } from 'material-ui/styles/colors'
import { CircularProgress } from 'material-ui'
import moment from 'moment'
import getCurrentMonthlyCycleAction from './getCurrentMonthlyCycleAction'

class Header extends React.Component {
  constructor (props) {
    super(props)
    props.getCurrentMonthlyCycleAction()
  }

  render () {
    const {styles, handleChangeRequestNavDrawer, getCurrentMonthlyCycleAction, startDate, isRunning = false} = this.props
    const style = {
      appBar: {
        position: 'fixed',
        top: 0,
        overflow: 'hidden',
        maxHeight: 57,
        backgroundColor: red500
      },
      menuButton: {
        marginLeft: 30
      },
      textRightContainer: {
        marginRight: 20,
        marginTop: 12,
        color: '#fff'
      },
      cycleRunning: {
        marginRight: '10em'
      }
    }
    return (
      <div>
        <AppBar
          style={{...styles, ...style.appBar}}
          title={this.props.breadCrumbs}
          iconElementLeft={
            <IconButton style={style.menuButton} onClick={handleChangeRequestNavDrawer}>
              <Menu color={white}/>
            </IconButton>
          }
          iconElementRight={
            <div style={style.textRightContainer}>
              {
                isRunning && <span style={style.cycleRunning}><CircularProgress size={18} thickness={2} color={white}/> A cycle is running!</span>
              }
              {
                `Current Open Month: ${moment(startDate).format('MM/YYYY')}`
              }
            </div>
          }
        />
      </div>
    )
  }
}

Header.propTypes = {
  styles: T.object,
  handleChangeRequestNavDrawer: T.func,
  getCurrentMonthlyCycleAction: T.func.isRequired,
  breadCrumbs: T.object,
  startDate: T.string,
  isRunning: T.bool.isRequired
}

const mapStateToProps = ({monthlyCycle, cycles: {isRunning}}) => ({
  startDate: _.get(monthlyCycle, 'current.startDate'),
  isRunning
})

export default connect(mapStateToProps, {getCurrentMonthlyCycleAction})(Header)
