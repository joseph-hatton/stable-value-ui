import React from 'react'
import { PropTypes as T } from 'prop-types'
import _ from 'lodash'
import { connect } from 'react-redux'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import { red500 } from 'material-ui/styles/colors'
import { openDialog } from '../redux-dialog/redux-dialog'

const FABAddButton = ({disabled, dialogName, onClick, openDialog}) => (
  <FloatingActionButton backgroundColor={red500} mini={false} className="floating-add-button"
    disabled={disabled} onClick={() => {
      if (!disabled) {
        if (_.isFunction(onClick)) {
          onClick()
        }
        if (_.isString(dialogName)) {
          openDialog(dialogName)
        }
      }
    }}>
    <ContentAdd/>
  </FloatingActionButton>
)

FABAddButton.propTypes = {
  disabled: T.bool,
  dialogName: T.string,
  onClick: T.func,
  openDialog: T.func.isRequired
}

const actions = {
  openDialog
}

export default connect(null, actions)(FABAddButton)
