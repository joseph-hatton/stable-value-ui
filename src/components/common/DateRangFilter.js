import React from 'react'
import _ from 'lodash'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import {DatePicker} from 'redux-form-material-ui'
import {change, Field} from 'redux-form'
import {IconButton} from 'material-ui/IconButton'
import Clear from 'material-ui/svg-icons/content/clear'
import {dateFormat} from '../utilities/Formatters'

export const ClearDateButton = ({formId, clearDate, dateProp}) => (
  <IconButton id={`${dateProp}-date-clear`} style={{marginTop: 25, marginRight: 25}}
              onClick={() => clearDate(formId, dateProp)}>
    <Clear/>
  </IconButton>)

ClearDateButton.propTypes = {
  clearDate: T.func.isRequired,
  formId: T.string.isRequired,
  dateProp: T.string.isRequired
}

const clearButtonPadding = {paddingLeft: 0}

const getDateValue = (forms, formId, fromFieldName) => _.get(forms, `${formId}.values.${fromFieldName}`)

class DateRangFilter extends React.Component {
  render () {
    const {forms = {}, formId, fromFieldName, label, toFieldName, clearDate} = this.props
    const from = getDateValue(forms, formId, fromFieldName)
    const to = getDateValue(forms, formId, toFieldName)
    return (
      <div className="row">
        <div className="col-lg-5">
          <Field autoOk fullWidth name={fromFieldName} component={DatePicker} firstDayOfWeek={0} floatingLabelText={`From ${label}`}
                 formatDate={dateFormat}/>
        </div>
        <div className="col-lg-1" style={clearButtonPadding}>
          {from && <ClearDateButton formId={formId} dateProp={fromFieldName} clearDate={clearDate}/>}
        </div>
        <div className="col-lg-5">
          <Field autoOk fullWidth name={toFieldName} component={DatePicker} firstDayOfWeek={0} floatingLabelText={`To ${label}`}
                 formatDate={dateFormat}/>
        </div>
        <div className="col-lg-1" style={clearButtonPadding}>
          {to && <ClearDateButton formId={formId} dateProp={toFieldName} clearDate={clearDate}/>}
        </div>
      </div>
    )
  }
}

DateRangFilter.propTypes = {
  clearDate: T.func.isRequired,
  formId: T.string.isRequired,
  fromFieldName: T.string.isRequired,
  toFieldName: T.string.isRequired,
  label: T.string.isRequired,
  forms: T.object
}

export const actions = {
  clearDate: (formId, dateProp) => (dispatch) => {
    dispatch(change(formId, dateProp, null))
  }
}

export const mapStateToProps = ({form: forms}) => {
  return {
    forms
  }
}

export default connect(mapStateToProps, actions)(DateRangFilter)
