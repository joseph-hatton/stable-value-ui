import React from 'react'
import _ from 'lodash'
import { connect } from 'react-redux'
import { PropTypes as T } from 'prop-types'

const Renderer = ({getRefDataText, refDataField, refDataValue}) => (
  <span>{getRefDataText(refDataField, refDataValue)}</span>)

Renderer.propTypes = {
  refDataField: T.string.isRequired,
  refDataValue: T.string.isRequired,
  getRefDataText: T.func.isRequired
}

const mapStateToProps = ({referenceData}) => ({
  getRefDataText: (refDataField, refDataValue) => {
    const referenceDataRow = _.keyBy(referenceData[refDataField], 'id')[refDataValue]
    return referenceDataRow ? referenceDataRow.text : ''
  }
})

export const ReferenceDataTextRenderer = connect(mapStateToProps)(Renderer)

export const createReferenceDataTextRenderer = (refDataField) => (refDataValue) => { // eslint-disable-line react/display-name
  return refDataValue ? <ReferenceDataTextRenderer refDataField={refDataField} refDataValue={refDataValue}/> : ''
}
