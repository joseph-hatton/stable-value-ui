import React from 'react'
import { PropTypes as T } from 'prop-types'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'

/*
  Sample Column Config
  {
    isKey: true
    field: 'effectiveDate',
    renderer: (value) => moment(value).format('MM-DD-YYYY'),
    label: 'Eff Date',
    sort: true
  }
*/

const makeColumn = ({field, label, renderer, sort, customProps = {}}) => { // eslint-disable-line react/prop-types
    const rendererProp = renderer ? {dataFormat: renderer} : {}
    return (<TableHeaderColumn key={field} dataField={field} className="stable-value-table-header"
                               dataSort={!!sort} {...rendererProp} {...customProps}>{label}</TableHeaderColumn>)
}

export const Grid = ({options, data, columns}) => (
  <BootstrapTable data={data} pagination striped hover version='4' options={options}
                  search={true} multiColumnSearch={true}>
    {columns.map(makeColumn)}
  </BootstrapTable>
)

Grid.displayName = 'StableValueGrid'

Grid.propTypes = {
  columns: T.array.isRequired,
  data: T.array,
  options: T.object.isRequired
}

export const CreateGridConfig = (data, config = {}) => ({
  page: 1,
  sizePerPageList: [{
    text: '10', value: 10
  }, {
    text: '20', value: 20
  }, {
    text: '50', value: 50
  }, {
    text: 'All', value: data
  }], // you can change the dropdown list for size per page
  sizePerPage: 100,
  pageStartIndex: 1, // where to start counting the pages
  paginationSize: 3,
  prePage: 'Prev', // Previous page button text
  nextPage: 'Next', // Next page button text
  firstPage: 'First', // First page button text
  lastPage: 'Last', // Last page button text
  paginationShowsTotal: true,
  paginationPosition: 'bottom',
  ...config
})
