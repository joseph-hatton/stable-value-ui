import React from 'react'
import _ from 'lodash'
import {PropTypes as T} from 'prop-types'
import {
  Card,
  Table,
  TableBody,
  TableHeader,
  TableRow,
  TableRowColumn
} from 'material-ui'
import HeaderToolbar from './HeaderToolbar'
import PaginationControls from './PaginationControls'
import SortableTableHeaderCell from './SortableTableHeaderCell'
import LocalPaginator from './LocalPaginator'
import RemotePaginator from './RemotePaginator'
import GridStyles from './GridStyles'
import {ASCENDING} from './Constants'
import {loadGridState, saveGridState} from './GridState'

const {cellStyle, tableRowStyle} = GridStyles

const rowStyleOdd = tableRowStyle

const rowStyleEven = {
  ...tableRowStyle,
  backgroundColor: '#f8f9fa'
}

const getRowStyle = (index) => index % 2 === 0 ? rowStyleEven : rowStyleOdd

const hiddenColumn = ({customProps}) => !customProps || !customProps.hidden

const createCellStyle = (value, customProps) => {
  const style = {...cellStyle, ...customProps}
  const {textAlign} = customProps

  if (!textAlign) {
    style.textAlign = _.isString(value) ? 'left' : 'center'
  }

  return style
}

const GRID_STATE_PROPS = ['currentPageIndex', 'selectedPageSize', 'sortedBy', 'sortDirection']

const DEFAULT_PAGE_SIZE = 200

const TABLE_HEIGHT = window.innerHeight - 320

export class Grid extends React.Component {
  constructor (props) {
    super(props)

    const {id, stickySort, sortBy, sortDirection = ASCENDING} = props

    let storedState = {}
    if (id && stickySort) {
      storedState = loadGridState(id)
    } else if (sortBy) {
      storedState = {sortedBy: sortBy, sortDirection}
    } else {
      console.warn('id is required for Grid to retain Grid State.')
    }

    this.state = {
      currentPageIndex: 0,
      selectedPageSize: DEFAULT_PAGE_SIZE,
      sortedBy: '',
      sortDirection: ASCENDING,
      currentPage: [],
      total: 0,
      ...storedState
    }

    const {options: {paginationType, baseApiUrl}, data = [], searchText = ''} = props
    if (paginationType === 'remote') {
      this.paginator = new RemotePaginator(baseApiUrl)
      this.enableSearch = false
    } else {
      this.paginator = new LocalPaginator(data, searchText)
      this.enableSearch = true
    }
  }

  _setState (newState) {
    this.setState(newState)
    const {id} = this.props
    if (id) {
      const currentGridState = _.pick(this.state, GRID_STATE_PROPS)
      const newGridState = _.pick(newState, GRID_STATE_PROPS)
      saveGridState(id, {...currentGridState, ...newGridState})
    }
  }

  componentDidMount () {
    this.goToPage(this.state.currentPageIndex)
  }

  componentWillReceiveProps (nextProps) {
    const reset = this.paginator.shouldUpdate(nextProps)
    if (reset) {
      this.resetPage(this.state.currentPageIndex)
    }
  }

  refresh () {
    this.goToPage(this.state.currentPageIndex)
  }

  goToPage (nextPageIndex) {
    this.paginator.getPage(nextPageIndex, this.state, this.props.filters).then(({page, total, response = {}}) => {
        this._setState({
          currentPageIndex: nextPageIndex,
          currentPage: page,
          total: total
        })

        const {store, id} = this.props
        if (store && id) {
          this.props.store.dispatch({
            type: 'GRID_DATA_UPDATED',
            id,
            page,
            response
          })
        }
      }
    )
  }

  nextPage () {
    this.goToPage(this.state.currentPageIndex + 1)
  }

  previousPage () {
    this.goToPage(this.state.currentPageIndex - 1)
  }

  pageSizeChanged (pageSize) {
    this._setState({
      currentPageIndex: 0,
      selectedPageSize: pageSize
    })
    this.resetPage()
  }

  resetPage (pageIndex = 0) {
    if (pageIndex > 0 && !this.paginator.isPageIndexValid(pageIndex, this.state)) {
      pageIndex = 0
    }
    setTimeout(() => this.goToPage(pageIndex))
  }

  sort (field) {
    this.paginator.sort(field, this.state).then(({sortDirection, page, newPageIndex}) => {
        this._setState({
          sortDirection,
          sortedBy: field,
          currentPage: page,
          currentPageIndex: newPageIndex
        })
      }
    )
  }

  search (event, searchText) {
    this.paginator.search(searchText)
    this.resetPage()
  }

  clearSearch () {
    this.search()
  }

  onRowSelection (selection) {
    const {onRowSelection} = this.props
    if (onRowSelection) {
      const {currentPage} = this.state
      let selectedRows
      switch (selection) {
        case 'all': {
          selectedRows = currentPage.concat()
          break
        }
        case 'none': {
          selectedRows = []
          break
        }
        default: {
          selectedRows = selection.map(index => currentPage[index])
        }
      }
      onRowSelection(selectedRows)
    }
  }

  render () {
    const {options = {}, columns, onRowDoubleClick} = this.props
    const {
      showHeaderToolbar,
      title,
      paginate,
      pageSizeList,
      height,
      selectable = false,
      enableSelectAll = false,
      multiSelectable = false,
      deselectOnClickaway = false,
      bodyStyle = {}
    } = options
    const columnsToRender = columns.filter(hiddenColumn)
    const {currentPageIndex, selectedPageSize, sortedBy, sortDirection, currentPage = [], total} = this.state

    return (
      <Card>
        {
          showHeaderToolbar &&
          <HeaderToolbar title={title} enableSearch={this.enableSearch}
                         searchTextChanged={::this.search}
                         clearSearch={::this.clearSearch}/>
        }
        <Table fixedHeader={true} height={height || TABLE_HEIGHT}
               headerStyle={{position: 'sticky', top: 0, zIndex: 500}} bodyStyle={{overflow: 'visible', ...bodyStyle}}
               multiSelectable={multiSelectable} selectable={true}
               onRowSelection={::this.onRowSelection}>
          <TableHeader adjustForCheckbox={selectable} displaySelectAll={enableSelectAll}>
            <TableRow>
              {
                columnsToRender.map(column => (
                  <SortableTableHeaderCell sort={::this.sort} sortDirection={sortDirection}
                                           sorted={column.field === sortedBy}
                                           key={column.label} column={column}/>))
              }
            </TableRow>
          </TableHeader>
          <TableBody className='grid-table' displayRowCheckbox={selectable} deselectOnClickaway={deselectOnClickaway}>
            {
              currentPage.map((row, index) => (
                  <TableRow key={index} style={getRowStyle(index)} onDoubleClick={() => onRowDoubleClick(row)}>
                    {
                      columnsToRender.map(({field, customProps = {}, renderer}, index) => {
                        const value = renderer ? renderer(row[field], row) : row[field]
                        return (<TableRowColumn title={_.isObject(value) || _.isArray(value) ? '' : value} key={index} style={createCellStyle(row[field], customProps)}>
                          {value}
                        </TableRowColumn>)
                        })
                    }
                  </TableRow>
                )
              )
            }
          </TableBody>
        </Table>
        {
          paginate && <PaginationControls total={total}
                                          currentPageIndex={currentPageIndex}
                                          selectedPageSize={selectedPageSize}
                                          pageSizeList={pageSizeList}
                                          nextPage={::this.nextPage}
                                          previousPage={::this.previousPage}
                                          pageSizeChanged={::this.pageSizeChanged}
          />
        }
      </Card>
    )
  }
}

Grid.displayName = 'MaterialUIGrid'

Grid.propTypes = {
  id: T.string.isRequired,
  columns: T.array.isRequired,
  data: T.array,
  options: T.object.isRequired,
  searchText: T.string,
  filters: T.object,
  onRowDoubleClick: T.func,
  onRowSelection: T.func,
  store: T.object,
  stickySort: T.bool,
  sortBy: T.bool,
  sortDirection: T.string
}

export const CreateGridConfig = (data, config = {}) => ({
  showHeaderToolbar: true,
  paginate: true,
  pageSizeList: [20, 50, 100, 200],
  paginationType: 'local',
  ...config
})
