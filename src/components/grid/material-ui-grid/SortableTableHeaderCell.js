import React from 'react'
import { PropTypes as T } from 'prop-types'
import _ from 'lodash'
import { TableHeaderColumn } from 'material-ui'
import ArrowUpward from 'material-ui/svg-icons/navigation/arrow-upward'
import ArrowDownward from 'material-ui/svg-icons/navigation/arrow-downward'
import GridStyles from './GridStyles'

const {headerColumnStyle, sortIconWrapper, sortIcon, headerColumnTextContainer} = GridStyles

const createHeaderColumnStyle = (customProps) => {
    const {width, textAlignHeader, textAlign} = customProps
    const style = {padding: '0 20px', ...headerColumnStyle, width}
    if (textAlignHeader) {
      style.textAlign = textAlignHeader
    } else if (textAlign) {
      style.textAlign = textAlign
    } else {
      style.textAlign = 'left'
    }
    return style
}

const SortableTableHeaderCell = ({sorted, sortDirection, sort, column: {label, field, customProps = {}, sort: sortable}}) => {
  const SortIcon = sortDirection === 'asc' ? ArrowUpward : ArrowDownward
  const sortFunc = sortable ? sort : _.noop
  const containerStyle = sortable ? {cursor: 'pointer'} : {}
  return (
    <TableHeaderColumn className="TableHeaderColumn" key={label} style={createHeaderColumnStyle(customProps)} onClick={() => sortFunc(field)}>
      <div style={containerStyle}>
        <div title={label} style={headerColumnTextContainer}>{label}</div>
        {
          sorted &&
          (
            <div style={sortIconWrapper}>
              <SortIcon style={sortIcon}/>
            </div>
          )
        }
      </div>
    </TableHeaderColumn>
  )
}

SortableTableHeaderCell.displayName = 'HeaderToolbar'

SortableTableHeaderCell.propTypes = {
  column: T.object,
  sorted: T.bool,
  sortable: T.bool,
  sortDirection: T.string,
  sort: T.func.isRequired
}

export default SortableTableHeaderCell
