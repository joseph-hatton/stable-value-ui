import React from 'react'
import {PropTypes as T} from 'prop-types'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import {blue500} from 'material-ui/styles/colors'
import TextField from 'material-ui/TextField'

const valueStyle = {marginTop: 5}
const iconStyle = {cursor: 'pointer', marginTop: 4}

class EditableCell extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      editing: false,
      value: props.value
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      value: nextProps.value
    })
  }

  toggleEdit () {
    const editing = !this.state.editing
    this.setState({editing})

    console.log('toggleEdit new Edit', editing)

    if (editing) {
      setTimeout(() => this.refs.editableCell.focus())
    }
  }

  onBlur () {
    this.toggleEdit()
    const {field, row} = this.props
    const {value} = this.state
    this.props.onChange(field, value, row)
  }

  validate (event, newValue, oldValue) {
    console.log('validate', isNaN(newValue), newValue, oldValue)
    if (isNaN(newValue)) {
      this.setState({value: oldValue, error: 'Invalid Number'})
    } else {
      this.setState({value: newValue ? parseFloat(newValue) : 0, error: ''})
    }
  }

  setCursor (e) { // Required to place the cursor at the end of the input on focus.
    const val = e.target.value
    e.target.value = ''
    e.target.value = val
  }

  render () {
    const {editing, value, error} = this.state
    const defaultValue = editing ? {defaultValue: value === 0 ? '' : value} : {}
    const {rowId, editable} = this.props

    return (
      <div className="row">
        <div className="col-lg-8" style={valueStyle}>
          {
            editing &&
            (<TextField id={rowId} ref="editableCell" errorText={error} {...defaultValue}
                        onChange={::this.validate}
                        onBlur={::this.onBlur} onFocus={::this.setCursor}/>)
          }
          {!editing && this.props.renderer(value)}
        </div>
        {
          editable && (
            <div className="col-lg-4" onClick={::this.toggleEdit}>
              <EditIcon color={blue500} style={iconStyle}/>
            </div>
          )
        }
      </div>
    )
  }
}

EditableCell.propTypes = {
  rowId: T.string.isRequired,
  field: T.string.isRequired,
  row: T.object.isRequired,
  editable: T.bool.isRequired,
  value: T.any,
  onChange: T.func.isRequired,
  renderer: T.func.isRequired
}

export default EditableCell
