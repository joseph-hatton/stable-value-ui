import _ from 'lodash'
import http from '../../../actions/http'
import {ASCENDING, DESCENDING} from './Constants'

export default class RemotePaginator {
  constructor (baseApiUrl) {
    this.baseApiUrl = baseApiUrl
  }

  shouldUpdate ({filters}) {
    return !_.isEqual(this.filters, filters)
  }

  _createSort (sortBy, sortDirection) {
    const sort = {}
    if (sortBy && sortDirection) {
      sort.sortBy = sortBy
      sort.sortOrder = sortDirection
    }
    return sort
  }

  getPage (pageIndex, state, filters = {}) {
    const {selectedPageSize, sortedBy, sortDirection} = state
    this.filters = filters
    const query = {
      ...filters,
      ...this._createSort(sortedBy, sortDirection),
      pageNumber: pageIndex + 1,
      pageSize: selectedPageSize
    }
    return http.get(this.baseApiUrl, query).then(response => {
      const {total, results} = response
      const page = sortedBy ? _.orderBy(results, sortedBy, sortDirection) : results
      return Promise.resolve({page, total, response})
    })
  }

  sort (newSortBy, state) {
    const {sortedBy, sortDirection, selectedPageSize} = state
    const newSortDirection = sortedBy !== newSortBy ? ASCENDING : (sortDirection === ASCENDING ? DESCENDING : ASCENDING)

    return this.getPage(0, {
      selectedPageSize,
      sortedBy: newSortBy,
      sortDirection: newSortDirection
    }, this.filters).then(({page}) => ({
      sortDirection: newSortDirection,
      newPageIndex: 0,
      page: page
    }))
  }

  isPageIndexValid () {
    return false
  }
}
