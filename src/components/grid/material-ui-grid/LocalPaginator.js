import _ from 'lodash'
import {ASCENDING, DESCENDING} from './Constants'

export default class LocalPaginator {
  constructor (data = [], searchText) {
    this.data = data
    this.filteredData = []
    if (searchText) {
      this._search(searchText)
    }
  }

  _search (searchText) {
    this.searchText = searchText = searchText.toLowerCase()
    this.filteredData = this.data.filter(record => !_(record).values().filter(value => value && String(value).toLowerCase().includes(searchText)).isEmpty())
  }

  search (searchText) {
    if (searchText) {
      this._search(searchText)
    } else {
      this.searchText = null
      this.filteredData = []
    }
  }

  shouldUpdate ({data}) {
    if (!_.isEqual(this.data, data)) {
      this.data = data
      if (this.searchText) {
        this._search(this.searchText)
      }
      return true
    }
  }

  _getData () {
    return this.searchText ? this.filteredData : this.data
  }

  _getPage (sortedBy, sortDirection, pageSize, pageIndex) {
    const data = this._getData()
    const sortedData = sortedBy ? _.orderBy(data, sortedBy, sortDirection) : data
    const total = data.length
    return _.chunk(sortedData, pageSize < total ? pageSize : total)[pageIndex]
  }

  getPage (pageIndex, state) {
    const {selectedPageSize, sortedBy, sortDirection} = state
    const page = this._getPage(sortedBy, sortDirection, selectedPageSize, pageIndex)
    return Promise.resolve({page, total: this._getData().length})
  }

  sort (newSortBy, state) {
    const {sortedBy, sortDirection, selectedPageSize} = state
    const newSortDirection = sortedBy !== newSortBy ? ASCENDING : (sortDirection === ASCENDING ? DESCENDING : ASCENDING)
    return Promise.resolve({
      sortDirection: newSortDirection,
      newPageIndex: 0,
      page: this._getPage(newSortBy, newSortDirection, selectedPageSize, 0)
    })
  }

  isPageIndexValid (pageIndex, state) {
    const {selectedPageSize} = state
    const data = this._getData()
    const numberOfPages = data.length % selectedPageSize === 0
      ? parseInt(data.length / selectedPageSize)
      : parseInt(data.length / selectedPageSize) + 1

    return numberOfPages > pageIndex
  }
}
