import { white, black } from 'material-ui/styles/colors'

const rowHeight = {
  height: 34
}

const GridStyles = {
  rowHeight,
  headerToolbar: {
    container: {
      backgroundColor: white
    },
    title: {
      color: black,
      paddingLeft: 20
    }
  },
  headerColumnStyle: {
    fontSize: 14,
    color: '#818182',
    fontWeight: 600,
    textAlign: 'center',
    ...rowHeight
  },
  cellStyle: {
    fontSize: 13,
    color: black,
    paddingLeft: 20,
    paddingRight: 20,
    ...rowHeight
  },
  tableRowStyle: {
    ...rowHeight
  },
  footerToolbar: {
    container: {
      backgroundColor: white,
      borderTop: '1px solid #e0e0e0'
    },
    paginationToolbarGroup: {
      marginRight: 50
    }
  },
  sortIconWrapper: {
    display: 'inline-block',
    height: 16,
    width: 16,
    verticalAlign: 'middle',
    marginLeft: 8,
    marginRight: 8,
    cursor: 'pointer'
  },
  sortIcon: {
    height: '100%',
    width: '100%',
    display: 'inline'
  },
  headerColumnTextContainer: {
    display: 'inline-block',
    verticalAlign: 'middle'
  },
  paginationChevStyle: {
    minWidth: 40
  },
  paginationTextStyle: {
    marginLeft: 8,
    marginRight: 8,
    alignItems: 'center',
    display: 'flex',
    fontSize: 13
  },
  pageSizeListDropDownStyle: {
    fontSize: 13
  }
}

export default GridStyles
