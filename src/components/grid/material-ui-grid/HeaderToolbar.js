import React from 'react'
import { PropTypes as T } from 'prop-types'
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'
import SearchIcon from 'material-ui/svg-icons/action/search'
import TextField from 'material-ui/TextField'

import GridStyles from './GridStyles'

const {headerToolbar: {container, title: titleStyle}} = GridStyles

const styles = {
  headerToolbarSearchIcon: {
    marginTop: 12
  },
  searchToolbarGroup: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    marginRight: 20
  },
  searchInputTextField: {
    marginTop: 6,
    marginLeft: 8,
    width: '100%',
    minWidth: 60
  },
  headerToolbarDefaultIcons: {
    display: 'flex',
    alignItems: 'center'
  }
}

export const HeaderToolbar = ({title, enableSearch, searchTextChanged, clearSearch}) => (
  <Toolbar style={container}>
    <ToolbarGroup firstChild={true}>
      <ToolbarTitle text={title} style={titleStyle}/>
    </ToolbarGroup>
    {
      enableSearch && (
        <ToolbarGroup lastChild={true}>
          <div style={styles.searchToolbarGroup}>
            <div>
              <SearchIcon style={styles.headerToolbarSearchIcon}/>
            </div>
            <div style={styles.searchInputTextField}>
              <TextField
                style={{minWidth: 300}}
                hintText="Search"
                onChange={searchTextChanged}
              />
            </div>
          </div>
        </ToolbarGroup>
      )
    }
  </Toolbar>
)

HeaderToolbar.displayName = 'HeaderToolbar'

HeaderToolbar.propTypes = {
  title: T.string,
  searchTextChanged: T.func.isRequired,
  clearSearch: T.func.isRequired,
  enableSearch: T.bool.isRequired
}

export default HeaderToolbar
