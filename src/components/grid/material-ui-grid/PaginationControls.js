import React from 'react'
import { PropTypes as T } from 'prop-types'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import DropDownMenu from 'material-ui/DropDownMenu'
import FlatButton from 'material-ui/FlatButton'
import MenuItem from 'material-ui/MenuItem'
import ChevronLeft from 'material-ui/svg-icons/navigation/chevron-left'
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right'

import GridStyles from './GridStyles'

const {
  footerToolbar: {container},
  paginationToolbarGroup,
  paginationChevStyle,
  paginationTextStyle,
  pageSizeListDropDownStyle
} = GridStyles

const handlePageSizeChanged = (pageSizeChanged) => (event, index, value) => pageSizeChanged(value)

const currentRange = (total, currentPageIndex, selectedPageSize) => {
  if (total === 0) {
    return '0 of 0'
  }
  const start = (currentPageIndex * selectedPageSize) + 1
  const end = start + selectedPageSize
  return `${start} - ${end > total ? total : end} of ${total}`
}

const isLastPage = (total, currentPageIndex, selectedPageSize) => {
  const div = total / selectedPageSize
  const lastPage = total % selectedPageSize === 0 ? div : parseInt(div) + 1
  return currentPageIndex + 1 === lastPage
}

export const FooterToolbar = ({total, currentPageIndex, selectedPageSize, pageSizeList, pageSizeChanged, nextPage, previousPage}) => {
  return (
    <Toolbar style={container}>
      <ToolbarGroup firstChild={true} style={{paddingLeft: 25}}>
        <div style={{display: 'flex'}}>
          <div style={paginationTextStyle}>
            <div>Page Size:</div>
          </div>
        </div>
        <DropDownMenu
          value={selectedPageSize}
          onChange={handlePageSizeChanged(pageSizeChanged)}
          style={pageSizeListDropDownStyle}
        >
          {pageSizeList.map((pageSize) => {
            return (
              <MenuItem
                key={pageSize}
                value={pageSize}
                primaryText={pageSize}
              />
            )
          })}
        </DropDownMenu>
        <div style={paginationTextStyle}>
          <div> {currentRange(total, currentPageIndex, selectedPageSize)} </div>
        </div>
        <div style={{paddingRight: 50}}>
          <FlatButton
            icon={<ChevronLeft/>}
            onClick={previousPage}
            disabled={currentPageIndex === 0}
            style={paginationChevStyle}
          />
          <FlatButton
            icon={<ChevronRight/>}
            onClick={nextPage}
            disabled={isLastPage(total, currentPageIndex, selectedPageSize)}
            style={paginationChevStyle}
          />
        </div>
      </ToolbarGroup>
    </Toolbar>
  )
}

FooterToolbar.displayName = 'FooterToolbar'

FooterToolbar.propTypes = {
  total: T.number,
  currentPageIndex: T.number,
  selectedPageSize: T.number,
  pageSizeList: T.array,
  pageSizeChanged: T.func.isRequired,
  nextPage: T.func.isRequired,
  previousPage: T.func.isRequired
}

export default FooterToolbar
