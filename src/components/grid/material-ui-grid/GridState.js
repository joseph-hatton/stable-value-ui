import localStorage from 'localStorage'

export const clearGridState = (id) => {
  try {
    localStorage.removeItem(id)
  } catch (err) {
    console.error('Error in clearing Grid State', id, err)
  }
}

export const loadGridState = (id) => {
  let state = {}
  try {
    const stateText = localStorage.getItem(id)
    if (stateText) {
      state = JSON.parse(stateText)
    }
  } catch (err) {
    console.error('Error in loading grid config, will clear the state.', err)
    clearGridState(id)
  }
  return state
}

export const saveGridState = (id, state) => {
  try {
    localStorage.setItem(id, JSON.stringify(state))
  } catch (err) {
    console.error('Error in saving Grid State.', err)
  }
}
