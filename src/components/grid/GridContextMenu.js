import React from 'react'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import {Popover, Menu, MenuItem} from 'material-ui'
import {openDialog, closeDialog} from '../../components/redux-dialog/redux-dialog'

const ContextMenuLink = ({menuName, cell, row, openDialog, rowSelected, rowSelectionActionType, createPayload}) => {
  return (
    <a href="" onClick={(e) => {
      e.nativeEvent.preventDefault()
      rowSelected(row, rowSelectionActionType, createPayload)
      openDialog(menuName, {
        anchorEl: e.currentTarget
      })
    }}> {cell} </a>
  )
}

ContextMenuLink.propTypes = {
  menuName: T.string.isRequired,
  cell: T.node,
  row: T.object.isRequired,
  openDialog: T.func.isRequired,
  rowSelected: T.func.isRequired,
  createPayload: T.func,
  rowSelectionActionType: T.string.isRequired
}

const ContextMenuReduxActions = {
  openDialog,
  rowSelected: (row, rowSelectionActionType, createPayload) => (dispatch, getState) => {
    dispatch({
      type: rowSelectionActionType,
      payload: createPayload ? createPayload(row, getState()) : row
    })
  }
}

export const GridContextMenuLink = connect(null, ContextMenuReduxActions)(ContextMenuLink)

const Divider = () => (<hr style={{margin: 0}}/>)

const GridContextMenu = ({dialog, menuName, menus, closeDialog, executeAction}) => {
  const dialogStatus = dialog[menuName]
  const isMenuOpen = !!dialogStatus
  const anchorEl = isMenuOpen && dialogStatus.anchorEl
  return anchorEl ? (
    <Popover
      id='GridContextMenu-Popover'
      open={isMenuOpen}
      anchorEl={anchorEl}
      anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
      targetOrigin={{horizontal: 'left', vertical: 'top'}}
      onRequestClose={() => {
        closeDialog(menuName)
      }}
    >
      <Menu>
        {
          menus.map((menu, index) => (
            [
              <MenuItem disabled={menu.disabled ? menu.disabled() : null} key={menu.label} primaryText={menu.label} onClick={() => {
                closeDialog(menuName)
                executeAction(menu.action)
              }} leftIcon={menu.icon}/>,
              index < menus.length - 1 ? <Divider key={`${menu.label}-divider`}/> : <span/>
            ]))
        }
      </Menu>
    </Popover>
  ) : (<span></span>)
}

GridContextMenu.propTypes = {
  menuName: T.string.isRequired,
  menus: T.array.isRequired,
  dialog: T.object.isRequired,
  anchorEl: T.object,
  closeDialog: T.func.isRequired,
  executeAction: T.func.isRequired
}

export const mapStateToProps = ({dialog}) => ({dialog})

const actions = {
  closeDialog,
  executeAction: (menuAction) => (dispatch, getState) => {
    menuAction(dispatch, getState)
  }
}

export default connect(mapStateToProps, actions)(GridContextMenu)
