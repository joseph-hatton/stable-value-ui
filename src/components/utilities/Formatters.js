import React from 'react'
import numeral from 'numeral'
import {FormattedNumber} from 'react-intl'
import Moment from 'moment'
import {exists, isNaNincludeFormattedNumber} from '../forms/Validators'

export const dateFormat = (date) => {
  return date ? Moment(date).utc().format('MM/DD/YYYY') : ''
}

export const toUtc = (date) => date ? Moment(date).utc().toDate() : ''

export const currencyFormat = (number) => {
  return numeral(number).format('(0,0.00)')
}

export const percentFormat = (number) => {
  return (<FormattedNumber value={number || 0} style='percent' minimumFractionDigits={2}/>)
}

export const formatDecimal = (number) => {
  return numeral(number).format('(0.00)')
}

export const normalizeNumberField = (value, prevValue) =>
  isNaNincludeFormattedNumber(value)
    ? (prevValue && !isNaNincludeFormattedNumber(prevValue) ? prevValue : null) : value

export const formatNumberField = (active) => (value) => {
  if (!exists(value)) {
    return ''
  } else if (!active) {
    return numeral(value).format('(0,0.00[000000])')
  } else {
    return value
  }
}
