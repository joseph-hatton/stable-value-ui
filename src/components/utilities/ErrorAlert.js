import React from 'react'
import {PropTypes as T} from 'prop-types'

const toMessage = (error) => (error && error.response && error.response.body && error.response.body.message) ? (error.response.body.message) : error.toString()

const ErrorAlert = ({error}) =>
  error
    ? <div className="alert alert-danger" role="alert">{toMessage(error)}</div>
    : <div />

ErrorAlert.displayName = 'ErrorAlert'

ErrorAlert.propTypes = {
  error: T.any
}

export default ErrorAlert
