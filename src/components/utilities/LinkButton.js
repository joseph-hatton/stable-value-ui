import React from 'react'
import T from 'prop-types'
import classnames from 'classnames'

const LinkButton = ({className, onClick, disabled, children}) => {
  if (disabled) {
    return (
      <span className={classnames('disabled-link', className)}>
        {children}
      </span>
    )
  } else {
    const handleOnClick = event => {
      event.preventDefault()
      if (onClick) {
        return onClick(event)
      }
    }
    return (
      <a href='#' className={className} onClick={handleOnClick}>
        {children}
      </a>
    )
  }
}

LinkButton.propTypes = {
  className: T.string,
  onClick: T.func,
  disabled: T.bool,
  children: T.array
}

LinkButton.displayName = 'LinkButton'

export default LinkButton
