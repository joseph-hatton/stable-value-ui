import React from 'react'
import Spinner from './Spinner'

const SpinnerModal = () =>
  <div className="modal-backdrop">
    <Spinner overlay={true}/>
  </div>

SpinnerModal.displayName = 'SpinnerModal'

export default SpinnerModal
