import React from 'react'
import {PropTypes as T} from 'prop-types'
import Icon from './Icon'

const getClassName = (inline, overlay) => {
  if (inline) {
    return 'spinner-inline'
  } else if (overlay) {
    return 'spinner-overlay'
  } else {
    return 'spinner'
  }
}

const Spinner = ({inline, overlay}) =>
  <div className={getClassName(inline, overlay)}>
    <Icon name='spinner' className='fa-spin'/>
  </div>

Spinner.displayName = 'Spinner'

Spinner.propTypes = {
  inline: T.bool,
  overlay: T.bool
}

export default Spinner
