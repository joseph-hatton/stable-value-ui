import React from 'react'
import {PropTypes as T} from 'prop-types'

const Dialog = ({children}) =>
  <div className='custom-modal-dialog'>
    <div className='custom-backdrop'/>
    <div className='dialog'>
      {children}
    </div>
  </div>

Dialog.displayName = 'Dialog'

Dialog.propTypes = {
  children: T.any
}

export default Dialog
