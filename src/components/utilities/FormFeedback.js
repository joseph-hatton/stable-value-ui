import React from 'react'
import {PropTypes as T} from 'prop-types'

const FormFeedback = ({validationResult, property, className = ''}) => {
  const hasFeedback = validationResult[property] && validationResult[property].feedback
  const classNameToApply = (hasFeedback) ? `form-control-feedback ${className}` : ''
  const display = (hasFeedback) ? validationResult[property].feedback : ''
  return (<div className={classNameToApply}>{display}</div>)
}

FormFeedback.displayName = 'FormFeedback'

FormFeedback.propTypes = {
  validationResult: T.object.isRequired,
  property: T.string.isRequired,
  className: T.string
}

export default FormFeedback
