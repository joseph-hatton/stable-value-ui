import React from 'react'
import {PropTypes as T} from 'prop-types'
import classnames from 'classnames'

const Icon = ({name, className, size, onClick}) =>
  <i
    className={classnames('fa', `fa-${name}`, size ? `fa-${size}` : undefined, className)}
    onClick={onClick} aria-hidden={true}
  />

Icon.displayName = 'Icon'

Icon.propTypes = {
  name: T.string.isRequired,
  className: T.string,
  size: T.string,
  onClick: T.func
}

export default Icon
