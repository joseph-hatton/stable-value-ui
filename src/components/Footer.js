import React from 'react'

const Footer = () =>
  <div className="footer">
    <nav className="navbar navbar-expand rga-footer bg-faded">
      <div className="navbar-brand"><strong >Stable Value 2017 &#169; Reinsurance Group of America</strong></div>
    </nav>
  </div>

Footer.displayName = 'Footer'

Footer.propTypes = {}

export default Footer
