const filter = (searchText = '', text = '') => {
  return text.toLowerCase().indexOf(searchText.toLowerCase()) > -1
}

export default filter
