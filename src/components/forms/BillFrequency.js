import createRefDataFormFieldComponent from './createRefDataFormFieldComponent'

export default createRefDataFormFieldComponent('billingFrequencyTypes', {floatingLabelText: 'Bill Frequency Type', type: 'text'})
