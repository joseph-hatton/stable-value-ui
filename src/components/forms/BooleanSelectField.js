import React from 'react'
import { PropTypes as T } from 'prop-types'
import { Field } from 'redux-form'
import MenuItem from 'material-ui/MenuItem'
import {
  SelectField
} from 'redux-form-material-ui'

class BooleanSelectField extends React.Component {
  render () {
    return (
      <Field component={SelectField} fullWidth={true} {...this.props} >
        <MenuItem key={true} value={true} primaryText="Yes"/>
        <MenuItem key={false} value={false} primaryText="No"/>
      </Field>
    )
  }
}

BooleanSelectField.displyName = 'BooleanSelectField'

export default BooleanSelectField
