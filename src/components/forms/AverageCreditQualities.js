import createRefDataFormFieldComponent from './createRefDataFormFieldComponent'

export default createRefDataFormFieldComponent('averageCreditQualities', {floatingLabelText: 'Average Credit Quality', type: 'text'})
