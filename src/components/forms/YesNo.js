import React from 'react'
import { Field } from 'redux-form'
import { Toggle } from 'redux-form-material-ui'

const YesNo = (props) => (
  <Field labelPosition="right" component={Toggle} {...props} parse={value => !!value}/>
)

export default YesNo
