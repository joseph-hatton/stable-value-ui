import createRefDataFormFieldComponent from './createRefDataFormFieldComponent'

export default createRefDataFormFieldComponent('benefitResponseTypes', {floatingLabelText: 'Benefit Response Type', type: 'text'})
