import createRefDataFormFieldComponent from './createRefDataFormFieldComponent'

export default createRefDataFormFieldComponent('contractStatuses', {
  floatingLabelText: 'Contract Status',
  type: 'text'
})
