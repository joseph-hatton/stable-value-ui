import createRefDataFormFieldComponent from './createRefDataFormFieldComponent'

export default createRefDataFormFieldComponent('notificationTypes', {floatingLabelText: 'Notification Type', type: 'text'})
