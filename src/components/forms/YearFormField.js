import React, {Component} from 'react'
import {Field} from 'redux-form'
import {SelectField} from 'redux-form-material-ui'
import MenuItem from 'material-ui/MenuItem'
import {PropTypes as T} from 'prop-types'
import moment from 'moment'

const today = moment()

const yearMenuItems = () => {
  const menuItems = []
  for (let year = 2010; year <= today.year(); year++) {
    menuItems.push(<MenuItem value={year} primaryText={year}></MenuItem>)
  }
  return menuItems
}

class YearFormField extends Component {
  render () {
    return (
      <Field {...this.props} component={SelectField}>
        { yearMenuItems() }
      </Field>
    )
  }
}

YearFormField.propTypes = {
  floatingLabelText: T.string.isRequired,
  name: T.string.isRequired,
  onChange: T.func
}

export default YearFormField
