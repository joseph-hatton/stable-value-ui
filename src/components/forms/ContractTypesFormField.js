import createRefDataFormFieldComponent from './createRefDataFormFieldComponent'

export default createRefDataFormFieldComponent('contractTypes', {floatingLabelText: '* Contract Type', type: 'text'})
