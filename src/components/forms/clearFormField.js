import {change} from 'redux-form'
import store from '../../store/store'

export default function (formId, field) {
  store.dispatch(change(formId, field, ''))
  store.dispatch(change(formId, field, null))
}
