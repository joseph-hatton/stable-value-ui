import React from 'react'
import _ from 'lodash'
import {Checkbox} from 'redux-form-material-ui'
import { Field } from 'redux-form'
import { PropTypes as T } from 'prop-types'

const CheckboxList = (props) => (
    <div className="row">
        <div className="col-xs-12">
            <h2>{props.title}</h2>
            <br/>
        </div>
        <div className="col-xs-12">
        {
        _.chunk(props.list, 3).map((subLists, index) => (
            <div key={index} className="row">
            {
            subLists.map((item, index) => (
                <div key={'sub' + index} className="col-lg-4">
                    <Field name={item[props.name]} component={Checkbox} label={item[props.label]} />
                </div>
            ))
            }
            </div>
        ))
        }
        </div>
    </div>
)

CheckboxList.propTypes = {
    title: T.string.isRequired,
    list: T.array.isRequired,
    name: T.string.isRequired,
    label: T.string.isRequired
  }

export default CheckboxList
