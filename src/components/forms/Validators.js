import numeral from 'numeral'

const NUM_START = ['.', '-', '-.']

export const exists = value => (value !== undefined && value !== null && value !== '')

export const isNaNincludeFormattedNumber = (value) => NUM_START.includes(value) ? false : isNaN(String(value).replace(/,/g, ''))

export const Required = value => exists(value) ? undefined : 'Required'

export const MaxLength255 = value => value && value.toString().length > 255 ? `Cannot exceed ${255} Characters` : undefined

export const MaxLength = (max) => value => value && value.toString().length > max ? `Cannot exceed ${max} Characters` : undefined

export const isInteger = value => !value || Number.isInteger(Number(value)) ? undefined : 'Must be an Integer'

export const isNumber = (value) => value && !isNaN(value)

export const toInteger = value => isNumber(value) ? parseInt(value) : null

export const isNegativeNumber = value => isNumber(value) && parseFloat(value) < 0 ? '' : 'Must be a negative number'

export const phoneNumber = value => {
  return value && value.length !== 14 ? `Must be 10 digit` : undefined
}

export const IsValidZipCode = value => (value && !/(^\d{5}$)|(^\d{5}-\d{4}$)/i.test(value)
  ? 'Must be a valid Zip code'
  : undefined)

export const Email = value => (value && !/^[A-Z0-9._%&+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
  ? 'Invalid email'
  : undefined)

export const NormalizePhone = (value) => {
  const withPara = firstThree => `(${firstThree}) `

  if (!value) {
    return value
  }
  const onlyNums = value.replace(/[^\d]/g, '')
  if (onlyNums.length <= 3) {
    return withPara(onlyNums)
  }
  if (onlyNums.length <= 7) {
    return `${withPara(onlyNums.slice(0, 3))}${onlyNums.slice(3)}`
  }
  return `${withPara(onlyNums.slice(0, 3))}${onlyNums.slice(3, 6)}-${onlyNums.slice(6, 10)}`
}

export const toNumber = (text) => {
  if (text) {
    const val = numeral(text).value()
    return val < 0 ? val * -1 : val
  }
  return null
}

export const toNumberAcceptsNegative = (text) => text ? numeral(text).value() : null

export const FormatCurrency = (text) => text ? numeral(toNumber(text)).format('0,0.00') : ''

export const FormatRate = (format) => (text) => text ? numeral(toNumber(text)).format(format) : ''
