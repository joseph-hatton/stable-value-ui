import createRefDataFormFieldComponent from './createRefDataFormFieldComponent'

export default createRefDataFormFieldComponent('payerTypes', {floatingLabelText: '* Payer', type: 'text'})
