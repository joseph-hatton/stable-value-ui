import React from 'react'
import {PropTypes as T} from 'prop-types'
import _ from 'lodash'
import MaterialSelectField from './MaterialSelectField'
import {connect} from 'react-redux'

export const idTextMapper = ({id, text}) => ({text, value: id})

function createRefDataFormFieldComponent (fieldNameFromState, commonProps, mapper = idTextMapper, nullable = false) {
  class RefDataFormFieldComponent extends React.Component {
    render () {
      const propsToPass = _.omit(this.props, [fieldNameFromState])
      return (
        <MaterialSelectField {...commonProps} {...propsToPass} options={this.props.options} nullable={nullable} />)
    }
  }

  RefDataFormFieldComponent.propTypes = {
    name: T.string.isRequired,
    options: T.array
  }

  RefDataFormFieldComponent.displayName = `${fieldNameFromState.toUpperCase()}FormField`

  const mapStateToProps = ({referenceData}) => {
    const options = referenceData[fieldNameFromState] ? referenceData[fieldNameFromState].map(mapper) : []
    return {
      options
    }
  }

  return connect(mapStateToProps)(RefDataFormFieldComponent)
}

export default createRefDataFormFieldComponent
