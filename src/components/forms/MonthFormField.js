import React, {Component} from 'react'
import {Field} from 'redux-form'
import {SelectField} from 'redux-form-material-ui'
import MenuItem from 'material-ui/MenuItem'
import {PropTypes as T} from 'prop-types'
import moment from 'moment'

const monthObj = moment('2018-01-01')
const MONTH_FORMAT = 'MMMM'

const monthMenuItems = () => {
  const menuItems = []
  for (let month = 1; month <= 12; month++) {
    menuItems.push(<MenuItem value={month} primaryText={monthObj.format(MONTH_FORMAT)}></MenuItem>)
    monthObj.add(1, 'month')
  }
  return menuItems
}

class MonthFormField extends Component {
  render () {
    return (
      <Field {...this.props} component={SelectField}>
        { monthMenuItems() }
      </Field>
    )
  }
}

MonthFormField.propTypes = {
  floatingLabelText: T.string.isRequired,
  name: T.string.isRequired,
  onChange: T.func
}

export default MonthFormField
