import React, {Component} from 'react'
import {Field} from 'redux-form'
import {SelectField} from 'redux-form-material-ui'
import MenuItem from 'material-ui/MenuItem'
import {PropTypes as T} from 'prop-types'
import moment from 'moment'

const MONTH_FORMAT = 'MM - MMMM'

const dayMenuItems = (y, M) => {
  const menuItems = []
  const endOfMonth = moment({y, M, d: 1}).endOf('month')
  console.log('endOfMonth', {endOfMonth, y, M})
  for (let day = 1; day <= endOfMonth.date(); day++) {
    menuItems.push(<MenuItem value={day} primaryText={day}></MenuItem>)
  }
  return menuItems
}

class DayFormField extends Component {
  render () {
    return (
      <Field {...this.props} component={SelectField}>
        { dayMenuItems(this.props.year, this.props.month) }
      </Field>
    )
  }
}

DayFormField.propTypes = {
  floatingLabelText: T.string.isRequired,
  name: T.string.isRequired,
  onChange: T.func,
  year: T.obj,
  month: T.obj
}

export default DayFormField
