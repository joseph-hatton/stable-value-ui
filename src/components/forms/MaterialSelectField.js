import React from 'react'
import {PropTypes as T} from 'prop-types'
import _ from 'lodash'
import {Field} from 'redux-form'
import MenuItem from 'material-ui/MenuItem'
import {
  SelectField
} from 'redux-form-material-ui'

class MaterialSelectField extends React.Component {
  render () {
    const {nullable, options} = this.props
    const propsToPassOn = _.omit(this.props, 'options')
    return (
      <Field
        fullWidth={true}
        component={SelectField}
        {...propsToPassOn}
      >
        {
          nullable && <MenuItem key="_empty" value={null} primaryText=""/>
        }
        {
          options.map(({text, value}) => (<MenuItem key={value} value={value} primaryText={text}/>))
        }
      </Field>
    )
  }
}

MaterialSelectField.displyName = 'MaterialSelectField'

MaterialSelectField.propTypes = {
  options: T.array.isRequired,
  nullable: T.bool
}

export default MaterialSelectField
