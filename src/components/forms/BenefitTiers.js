import createRefDataFormFieldComponent from './createRefDataFormFieldComponent'

export default createRefDataFormFieldComponent('tiers', {floatingLabelText: 'Tier', type: 'text'})
