import createRefDataFormFieldComponent from './createRefDataFormFieldComponent'

export default createRefDataFormFieldComponent('monthTypes', {floatingLabelText: 'Month', type: 'text'})
