import createRefDataFormFieldComponent from './createRefDataFormFieldComponent'

export default createRefDataFormFieldComponent('marketValueEvents', {floatingLabelText: 'Market Value Event', type: 'text'})
