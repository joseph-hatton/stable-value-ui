import createRefDataFormFieldComponent from './createRefDataFormFieldComponent'

export default createRefDataFormFieldComponent('feeDailyRateFormulae', {floatingLabelText: '* Fee Daily Rate Formula', type: 'text'})
