import React from 'react'
import { Field } from 'redux-form'
import { TextField } from 'redux-form-material-ui'

const StableValueTextField = (props) => (<Field {...props} fullWidth={true} component={TextField} type="text"/>)

StableValueTextField.displayName = 'StableValueTextField'

export default StableValueTextField
