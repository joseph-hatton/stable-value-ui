import createRefDataFormFieldComponent from './createRefDataFormFieldComponent'

export default createRefDataFormFieldComponent('calendarTypes', {floatingLabelText: 'Calendar Type', type: 'text'})
