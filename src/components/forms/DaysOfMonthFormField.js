import createRefDataFormFieldComponent from './createRefDataFormFieldComponent'

export default createRefDataFormFieldComponent('daysOfMonth', {floatingLabelText: 'Day', type: 'text'})
