import createRefDataFormFieldComponent from './createRefDataFormFieldComponent'

export default createRefDataFormFieldComponent('states', {
  floatingLabelText: '* State',
  type: 'text'
})
