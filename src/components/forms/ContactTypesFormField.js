import createRefDataFormFieldComponent from './createRefDataFormFieldComponent'

export default createRefDataFormFieldComponent('contactTypes', {floatingLabelText: '* Contact Type', type: 'text'})
