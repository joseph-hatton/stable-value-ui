import React from 'react'
import {PropTypes as T} from 'prop-types'

class LogOutIFrame extends React.Component {
  componentDidMount () {
    setTimeout(() => location.reload(), this.props.reloadDelay)
  }

  render () {
    const {url, height, width} = this.props
    return (
      <iframe src={url} height={height} width={width}>
        <div className='alert alert-danger ml-3 mt-3 mr-3'>
          <strong>Unable to Log out!</strong>
          {' This browser does not support internal frames'}
        </div>
      </iframe>
    )
  }
}

LogOutIFrame.propTypes = {
  url: T.string.isRequired,
  height: T.number.isRequired,
  width: T.number.isRequired,
  reloadDelay: T.number
}

LogOutIFrame.displayName = 'LogOutIFrame'

LogOutIFrame.defaultProps = {
  reloadDelay: 1000
}

export default LogOutIFrame
