import React from 'react'
import { PropTypes as T } from 'prop-types'

const styles = {

  itemContainer: {
    position: 'absolute',
    right: 0,
    top: -10,

    WebkitTransform: 'translateY(-50%)',
    MozTransform: 'translateY(-50%)',
    MsTransform: 'translateY(-50%)',
    OTransform: 'translateY(-50%)',
    transform: 'translateY(-50%)'
  }

}

const effects = {
  'none': (visible, index) => ({
    display: visible ? '' : 'none'
  }),

  'fade-staggered': (visible, index) => ({
    transition: visible ? '450ms' : '300ms',
    transitionDelay: visible ? (index * 0.025) + 's' : '',
    transform: 'translateY(' + (visible ? 0 : '5px') + ')',
    opacity: visible ? 1 : 0
  }),

  'fade': (visible, index) => ({
    transition: visible ? '450ms' : '300ms',
    opacity: visible ? 1 : 0
  }),

  'slide': (visible, index) => ({
    transition: visible ? '250ms' : '300ms',
    transform: 'translateY(' + (visible ? 0 : getYPos(index) + 'px') + ')',
    opacity: visible ? 1 : 0
  })
}

function getYPos (index) {
  return 81 + index * 56
}

class SpeedDialItem extends React.Component {
  handleTouchTap (ev) {
    this.props.onClick()
    this.props.toggle()
  }

  render () {
    const { index, visible } = this.props

        let style = {
          pointerEvents: visible ? '' : 'none',
          position: 'absolute',
          whiteSpace: 'nowrap',
          right: 0,
          bottom: getYPos(index)
        }

        let fx = effects[this.props.effect]

        if (!fx) {
          fx = effects.none
        }

        style = { ...style, ...fx(visible, index) }

        return <div style={style}>

          <div className="speedDialItemLabel" onClick={::this.handleTouchTap} style={styles.itemContainer}>
            {this.props.label}
          </div>

        </div>
  }
}

SpeedDialItem.propTypes = {
  index: T.number,
  visible: T.bool,
  effect: T.string,
  label: T.string,
  fabContent: T.element,
  onClick: T.func.isRequired,
  toggle: T.func.isRequired
}

export default SpeedDialItem
