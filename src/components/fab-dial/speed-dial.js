import React from 'react'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import FabSpinner from './fab-spinner'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import { red500 } from 'material-ui/styles/colors'

const styles = {
  container: {
    position: 'relative',
    display: 'inline-block'
  }
}

class SpeedDial extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      internalOpen: false
    }
    this.toggle = this.toggle.bind(this)
  }

  toggle () {
    this.setState({
      internalOpen: !this.state.internalOpen
    })
  }

  render () {
    let { open, effect, style } = this.props

        if (open === undefined) {
          open = this.state.internalOpen
        }

        if (effect === undefined) {
          effect = 'fade-staggered'
        }

        let enhancedChildren = React.Children.map(this.props.children,
          (child, index) => React.cloneElement(child, {
            effect,
            index,
            visible: open,
            onCloseRequest: this.handleCloseRequest,
            toggle: this.toggle
          })
        )

        return <div style={{...styles.container, ...style}}>

          <FloatingActionButton
            {...this.props.fabProps}
            onClick={::this.toggle}
            backgroundColor={red500}
          >
            <FabSpinner
              aContent={this.props.fabContentOpen}
              bContent={this.props.fabContentClose || this.props.fabContentOpen}
              showB={open}
            />
          </FloatingActionButton>

          {enhancedChildren}

        </div>
  }
}

SpeedDial.propTypes = {
  open: T.bool,
  effect: T.string,
  style: T.object,
  children: T.array,
  fabProps: T.object,
  fabContentOpen: T.element,
  fabContentClose: T.element,
  onOpenCloseRequest: T.func
}

const actions = {}

export default connect(null, actions)(SpeedDial)
