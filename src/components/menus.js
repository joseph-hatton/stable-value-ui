import React from 'react'
import DoneIcon from 'material-ui/svg-icons/action/done'
import PortfolioIcon from 'material-ui/svg-icons/action/work'
import { activatePortfoliosTab } from '../wrap-portfolio/PortfolioActions'

const showContractSubMenus = ({contracts: {contract}, form: {ContractSpecifications}}) => !!(contract || ContractSpecifications)

const showContractSubMenu = ({contracts: {contract}}) => !!(contract && contract.contractNumber)

const showScheduleSubMenus = ({form: {scheduleFunding}}) => !!scheduleFunding

const makeContractTabLink = (path) => ({contracts: {contract}}) => {
  return contract && contract.contractId
    ? `/contracts/${contract.contractId}/${path}`
    : `/contracts/${path}`
}

const getReadyToSubmitIcon = (contractSubMenu) => (appState) => {
  const icon = {}
  const {contracts: {contractLifeCycleStatus = {}}} = appState
  const readyToSubmitStatus = contractLifeCycleStatus.readyToSubmit || {}
  if (readyToSubmitStatus.isPending && readyToSubmitStatus[contractSubMenu]) {
    icon.rightIcon = <DoneIcon/>
  }
  return icon
}

const menus = [
  {
    text: 'Contracts',
    tab: 'contracts',
    link: '/contracts',
    handleStateManually: true,
    showSubMenus: showContractSubMenus,
    subMenus: [
      {
        text: 'Specifications',
        link: makeContractTabLink('specifications'),
        rightIcon: getReadyToSubmitIcon('specifications'),
        showSubMenu: () => true
      },
      {
        text: 'Base Info',
        link: makeContractTabLink('info'),
        rightIcon: getReadyToSubmitIcon('info'),
        showSubMenu: showContractSubMenu
      },
      {
          text: 'Schedule',
          tab: 'schedule',
          link: makeContractTabLink('schedule'),
          rightIcon: getReadyToSubmitIcon('schedule'),
          showSubMenu: showContractSubMenu,
          handleStateManually: true,
          showSubMenus: () => true,
          subMenus: [
            {
              text: 'Funding',
              link: (state) => `${makeContractTabLink('schedule')(state)}/funding`,
              showSubMenu: () => true,
              goToTab: true
            },
            {
              text: 'Crediting Rate',
              link: (state) => `${makeContractTabLink('schedule')(state)}/crediting-rate`,
              showSubMenu: () => true,
              goToTab: true
            },
            {
              text: 'Withdrawal',
              link: (state) => `${makeContractTabLink('schedule')(state)}/withdrawal`,
              showSubMenu: () => true,
              goToTab: true
            },
            {
              text: 'Investment Guidelines',
              link: (state) => `${makeContractTabLink('schedule')(state)}/investment-guidelines`,
              showSubMenu: () => true,
              goToTab: true
            },
            {
              text: 'Termination',
              link: (state) => `${makeContractTabLink('schedule')(state)}/termination`,
              showSubMenu: () => true,
              goToTab: true
            }
          ]
      },
      {
        text: 'Underwriting',
        tab: 'underwriting',
        link: makeContractTabLink('underwriting'),
        rightIcon: getReadyToSubmitIcon('underwriting'),
        showSubMenu: showContractSubMenu,
        handleStateManually: true,
        showSubMenus: () => true,
        subMenus: [
          {
            text: 'Model Inputs',
            link: (state) => `${makeContractTabLink('underwriting')(state)}/model-inputs`,
            showSubMenu: () => true,
            goToTab: true
          },
          {
            text: 'Stable Value Fund',
            link: (state) => `${makeContractTabLink('underwriting')(state)}/stable-value-fund`,
            showSubMenu: () => true,
            goToTab: true
          },
          {
            text: 'Additional Details',
            link: (state) => `${makeContractTabLink('underwriting')(state)}/additional-details`,
            showSubMenu: () => true,
            goToTab: true
          },
          {
            text: 'Plan Types',
            link: (state) => `${makeContractTabLink('underwriting')(state)}/plan-types`,
            showSubMenu: () => true,
            goToTab: true
          }
        ]
      },
      {
        text: 'Risk Scorecard',
        link: makeContractTabLink('risk-scorecard'),
        rightIcon: getReadyToSubmitIcon('risk-scorecard'),
        showSubMenu: showContractSubMenu
      },
      {
        text: 'Crediting Rates',
        link: makeContractTabLink('crediting-rates'),
        rightIcon: getReadyToSubmitIcon('crediting-rates'),
        showSubMenu: showContractSubMenu
      },
      {
        text: 'Contacts',
        link: makeContractTabLink('contacts'),
        rightIcon: getReadyToSubmitIcon('contacts'),
        showSubMenu: showContractSubMenu
      }
    ]
  },
  {text: 'Transactions', link: '/transactions'},
  {text: 'Companies', link: '/companies'},
  {text: 'Contacts', link: '/contacts'},
  {text: 'Wrap Portfolio',
    tab: 'wrapPortfolio',
    link: '/wrap-portfolio',
    showSubMenus: () => true,
    handleStateManually: true,
    showSubMenu: () => true,
    subMenus: [
      {
        text: 'Allocations',
        link: '/wrap-portfolio/allocations',
        showSubMenu: () => true,
        goToTab: true
      },
      {
        text: 'Portfolios',
        link: '/wrap-portfolio/portfolios',
        showSubMenu: () => true,
        goToTab: true
      }]},
  {text: 'Market Values', link: '/market-values'},
  {text: 'Book Values', link: '/book-values'},
  {text: 'Fees', link: '/fees'},
  {text: 'Watch Lists', link: '/watch-lists'},
  {text: 'Cycles', link: '/cycles'}
]

export default menus
