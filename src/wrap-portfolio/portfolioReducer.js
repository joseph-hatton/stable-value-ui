import _ from 'lodash'

const initialState = {
  portfolios: [],
  editPortfolio: undefined,
  portfolioId: undefined,
  initialValues: undefined,
  activePortfolioTab: 'ALLOCATIONS'
}
export const addOrUpdatePortfolio = (portfolios, savedPortfolio) => {
  const existingPortfolio = portfolios.find(portfolio => portfolio.portfolioId === savedPortfolio.portfolioId)
  if (existingPortfolio) {
    return [savedPortfolio].concat(_.without(portfolios, existingPortfolio))
  } else {
    return [savedPortfolio].concat(portfolios)
  }
}
export const deletePortfolio = (portfolios, deletePorfolioId) => {
  const existingPortfolio = portfolios.find(portfolio => portfolio.portfolioId === deletePorfolioId)
  if (existingPortfolio) {
    return _.without(portfolios, existingPortfolio)
  }
}
export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_ALL_PORTFOLIOS_FULFILLED':
      return {
        ...state,
        portfolios: action.payload.results
      }

    case 'ACTIVATE_PORTFOLIOS_TAB': {
      return {
        ...state,
        activePortfolioTab: action.label
      }
    }
    case 'SAVE_PORTFOLIO_FULFILLED':
      const savedPortfolio = action.payload
      const portfolios = state.portfolios || []
      return {
        ...state,
        portfolios: addOrUpdatePortfolio(portfolios, savedPortfolio),
        editPortfolio: undefined
      }
    case 'LOAD_PORTFOLIO_FULFILLED':
      return {
        ...state,
        editPortfolio: action.payload
      }
    case 'CLEAR_EDIT_PORTFOLIO':
      return {
        ...state,
        editPortfolio: undefined,
        portfolioId: undefined
      }
    case 'SELECT_PORTFOLIO_INPUT': {
      return {
        ...state,
        portfolioId: action.payload ? (action.payload.portfolioId ? action.payload.portfolioId : state.portfolioId) : null,
        editPortfolio: action.payload
      }
    }
    case 'DELETE_PORTFOLIO_FULFILLED': {
      let {portfolios} = state
      return {
        ...state,
        portfolios: deletePortfolio(portfolios, action.payload.id),
        editPortfolio: undefined,
        portfolioId: undefined,
        initialValues: undefined
      }
    }
      case 'SELECT_PORTFOLIO': {
          return {
              ...state,
              portfolioId: action.payload ? (action.payload.portfolioId ? action.payload.portfolioId : state.portfolioId) : null,
              editPortfolio: action.payload
          }
      }
    default:
      return state
  }
}
