import React from 'react'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import AddEditPortfolioAllocationComponent from './AddEditPortfolioAllocationComponent'
import { saveAllocation, loadAllocation, resetAllocations } from './PortfolioAllocationActions'
import { loadPortfolioByContract } from './PortfolioActions'

export class AddEditPortfolioAllocationContainer extends React.Component {
  componentDidMount () {
    const {params, loadAllocation, contractId} = this.props
    if (params.portfolioAllocationId) {
      loadAllocation(params.portfolioAllocationId)
      loadPortfolioByContract(contractId)
    }
  }

  render () {
    return (<AddEditPortfolioAllocationComponent {...this.props} />)
  }
}

AddEditPortfolioAllocationContainer.displayName = 'AddEditPortfolioContainer'

AddEditPortfolioAllocationContainer.propTypes = {
  router: T.object.isRequired,
  params: T.object,
  loadAllocation: T.func.isRequired,
  loadPortfolioByContract: T.func.isRequired,
  resetAllocations: T.func.isRequired,
  contractId: T.number
}

const actions = {
  loadAllocation,
  saveAllocation,
  loadPortfolioByContract,
  resetAllocations
}

export const mapStateToProps = ({portfolioAllocations: {editAllocation = {}, contractId}}) => {
  const props = {
    initialValues: editAllocation,
    contractId: contractId || editAllocation.contractId,
    editAllocation
  }
  return props
}

export default connect(mapStateToProps, actions)(AddEditPortfolioAllocationContainer)
