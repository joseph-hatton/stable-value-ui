import React from 'react'
import { PropTypes as T } from 'prop-types'
import {Card} from 'material-ui'
import RaisedButton from 'material-ui/RaisedButton'
import { Field, reduxForm } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import { Required } from '../components/forms/Validators'
import ManagersFormField from '../contacts/ManagersFormField'
import ReduxDialog from '../components/redux-dialog/redux-dialog'
import styles from './../styles/styles'
import { savePortfolio } from './PortfolioActions'
import { connect } from 'react-redux'
import { hasEntitlement } from '../entitlements'

const formId = 'AddEditPortfolioForm'

export class AddEditPortfolioComponent extends React.Component {
  render () {
    const {onClose, handleSubmit, savePortfolio, initialValues} = this.props
    const portfolioId = initialValues ? initialValues.portfolioId : undefined
    const title = portfolioId ? 'Edit Portfolio' : 'Add Portfolio'
     const actions = [
      <RaisedButton
        key="Save"
        label="Save"
        primary={true}
        style={{marginRight: 20}}
        type="submit"
        form="AddEditPortfolioForm"
        disabled={!hasEntitlement('MODIFY_PORTFOLIO')}
      />,
      <RaisedButton
        key="Cancel"
        label="Cancel"
        onClick={onClose}
      />
    ]
    return (
      <ReduxDialog
        dialogName="portfolioInput"
        title={title}
        actions={actions}
        modal={true}
        autoScrollBodyContent={true}
        contentStyle={styles.smallModal}
      >
      <div>
        <form id={formId} onSubmit={handleSubmit(savePortfolio)}>
          <Card>
            <div style={{padding: '25px'}}>
              <div className="row">
                <div className="col-lg-12">
                  <Field name="portfolioName" component={TextField} type="text" floatingLabelText="* Portfolio Name"
                         validate={Required}/>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12">
                  <ManagersFormField disabled={portfolioId} name="portfolioManagerId" floatingLabelText="* Manager"/>
                </div>
              </div>
            </div>
          </Card>
        </form>
    </div>
      </ReduxDialog>
    )
  }
}

AddEditPortfolioComponent.propTypes = {
  onClose: T.func.isRequired,
  savePortfolio: T.func.isRequired,
  submitting: T.bool,
  initialValues: T.object,
  handleSubmit: T.func.isRequired
}

export const actions = {
  savePortfolio
}

export const AddEditPortfolioForm = reduxForm({
  form: formId
})(AddEditPortfolioComponent)

export const mapStateToProps = ({portfolios: { editPortfolio }}) => ({
  initialValues: editPortfolio,
  editPortfolio
})

export default connect(mapStateToProps, actions)(AddEditPortfolioForm)
