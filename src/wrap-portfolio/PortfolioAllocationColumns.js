import React from 'react'
import _ from 'lodash'
import { dateFormat, currencyFormat, percentFormat } from '../components/grid'
import { Link } from 'react-router'
import ChipPortfolioArray from './PorfoliosChip'
import { ViewEditAllocationMenu } from './ViewEditAllocationMenu'
import {mappedPortfolios} from './PortfolioAllocationActions'

const chipStyle = {
  chip: {
    margin: 0
  },
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap'
  }
}

export const PortfolioAllocationColumns = [
  {
    label: 'portfolioAllocationId',
    field: 'portfolioAllocationId',
    customProps: {
      isKey: true,
      hidden: true
    }
  },
  {
    label: 'Date',
    sort: true,
    field: 'effectiveDate',
    renderer: (cell, row) => ( // eslint-disable-line react/display-name
      <ViewEditAllocationMenu cell={dateFormat(cell)} row={row} />
    ),
    customProps: {
      textAlignHeader: 'left',
      textAlign: 'left',
      width: 120
    }
  },
  {
    field: 'contractNumber',
    sort: true,
    label: 'Contract',
    customProps: {
      textAlign: 'left',
      width: 150,
      textAlignHeader: 'left'
    }
  },
  {
    field: 'shortPlanName',
    sort: true,
    label: 'Short Plan Name',
    customProps: {
      textAlign: 'left',
      width: 180,
      textAlignHeader: 'left'
    }
  },
  {
    field: 'manager',
    sort: true,
    label: 'Manager',
    customProps: {
      textAlign: 'left',
      width: 200
    }
  },
  { field: 'portfolios',
    sort: false,
    label: 'Portfolios',
    customProps: {
      textAlign: 'left',
      width: 'Calc(100% - 650px)'
    },
    renderer: (cell, row) => _.map(mappedPortfolios(row) || [], ({name, rate}) => `${name} (${rate}%)`).join(', ') // eslint-disable-line react/display-name
  }

]
