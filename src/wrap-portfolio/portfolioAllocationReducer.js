import _ from 'lodash'

const initialState = {
  allocations: [],
  initialValues: undefined,
  editAllocation: undefined,
  portfoliosByContract: {
    results: []
  }
}

export const deleteAllocation = (allocations, deletePorfolioAllocationId) => {
  const existingAllocation = allocations.find(allocation => allocation.portfolioAllocationId === deletePorfolioAllocationId)
  if (existingAllocation) {
    return _.without(allocations, existingAllocation)
  }
}

const addOrUpdateAllocation = (allocations, savedAllocation) => {
  const existingAllocation = allocations.find(allocation => allocation.portfolioAllocationId === savedAllocation.portfolioAllocationId)
  if (existingAllocation) {
    return [savedAllocation].concat(_.without(allocations, existingAllocation))
  } else {
    return [savedAllocation].concat(allocations)
  }
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_ALL_PORTFOLIO_ALLOCATIONS_FULFILLED':
      return {
        ...state,
        allocations: action.payload.results
      }
    case 'SAVE_PORTFOLIO_ALLOCATION_FULFILLED':
      const savedAllocation = action.payload
      const allocations = state.allocations || []
      return {
        ...state,
        allocations: addOrUpdateAllocation(allocations, savedAllocation),
        editAllocation: undefined
      }
    case 'LOAD_PORTFOLIO_ALLOCATION_FULFILLED':
      return {
        ...state,
        editAllocation: action.payload.allocation,
        portfoliosByContract: action.payload.portfolios
      }
    case 'CLEAR_EDIT_ALLOCATION':
      return {
        ...state,
        editAllocation: undefined,
        allocationId: undefined,
        initialValues: undefined,
        contractId: undefined,
        portfoliosByContract: {
          results: []
        }
      }
    case 'SELECT_ALLOCATION_INPUT': {
      return {
        ...state,
        allocationId: action.payload ? (action.payload.portfolioAllocationId ? action.payload.portfolioAllocationId : state.portfolioAllocationId) : null,
        editAllocation: action.payload,
        initialValues: action.payload,
        contractId: action.payload.contractId
      }
    }
    case 'LOAD_PORTFOLIO_BY_CONTRACT_FULFILLED':
      const {portfolios: portfoliosByContract, contractId} = action.payload
      return {
        ...state,
        portfoliosByContract,
        contractId
      }
    case 'DELETE_ALLOCATION_FULFILLED': {
      let {allocations} = state
      return {
        ...state,
        allocations: deleteAllocation(allocations, action.payload.id),
        editAllocation: undefined,
        allocationId: undefined,
        initialValues: undefined,
        contractId: undefined,
        portfoliosByContract: {
          results: []
        }
      }
    }
    default:
      return state
  }
}
