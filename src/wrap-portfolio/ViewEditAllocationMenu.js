import React from 'react'
import { PropTypes as T } from 'prop-types'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import { blue500, red500 } from 'material-ui/styles/colors'
import { connect } from 'react-redux'
import { Popover, Menu, MenuItem } from 'material-ui'
import { openDialog, closeDialog } from './../components/redux-dialog/redux-dialog'
import { selectAllocationInputRow, deleteAllocation } from './PortfolioAllocationActions'
import {hasEntitlement} from '../entitlements'
import { openConfirmationAlert, openSnackbar } from '../components/redux-dialog'

const MenuLink = ({cell, openDialog, row, selectAllocationInputRow}) => {
  return (
    <a href="" onClick={(e) => {
      e.nativeEvent.preventDefault()
      selectAllocationInputRow(row)
      openDialog('allocationMenu', {
        anchorEl: e.currentTarget
      })
    }}> {cell} </a>
  )
}

MenuLink.propTypes = {
  cell: T.node,
  row: T.object.isRequired,
  openDialog: T.func.isRequired,
  selectAllocationInputRow: T.func.isRequired
}

export const ViewEditAllocationMenu = connect(null, {openDialog, selectAllocationInputRow})(MenuLink)

const Divider = () => (<hr style={{margin: 0}}/>)

export const allocationMenu = ({open, anchorEl, closeDialog, selectAllocationInputRow, openDialog, editAllocation, openConfirmationAlert, deleteAllocation}) => (
  <Popover
    id='allocationMenu-Popover'
    open={open}
    anchorEl={anchorEl}
    anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
    targetOrigin={{horizontal: 'left', vertical: 'top'}}
    onRequestClose={() => {
      closeDialog('allocationMenu')
    }}
  >
    <Menu>
      <MenuItem primaryText="View/Edit Allocation" onClick={() => {
        openDialog('allocationInput')
        closeDialog('allocationMenu')
      }} leftIcon={<EditIcon color={blue500}/>}/>
      <Divider/>
      <MenuItem primaryText="Delete Allocation"
                onClick={() => {
                  closeDialog('allocationMenu')
                  openConfirmationAlert({
                    message: 'Are you sure you want to delete this Allocation?',
                    onOk: () => {
                      deleteAllocation(editAllocation.portfolioAllocationId)
                    }
                  })
                }}
                leftIcon={<DeleteIcon color={red500}/>}/>
    </Menu>
  </Popover>
)

allocationMenu.propTypes = {
  open: T.bool.isRequired,
  anchorEl: T.object,
  closeDialog: T.func.isRequired,
  openDialog: T.func.isRequired,
  selectAllocationInputRow: T.func.isRequired,
  openConfirmationAlert: T.func.isRequired,
  editAllocation: T.object,
  deleteAllocation: T.func.isRequired
}

export const mapStateToProps = ({dialog: {allocationMenu}, portfolioAllocations: { editAllocation }}) => {
  return {
    open: !!allocationMenu,
    anchorEl: allocationMenu ? allocationMenu.anchorEl : null,
    editAllocation
  }
}

const actions = {
  closeDialog,
  selectAllocationInputRow,
  openDialog,
  openConfirmationAlert,
  deleteAllocation
}

export default connect(mapStateToProps, actions)(allocationMenu)
