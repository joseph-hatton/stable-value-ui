import React from 'react'
import _ from 'lodash'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import {reduxForm, reset, change, Field} from 'redux-form'
import {RaisedButton, IconButton} from 'material-ui'
import Clear from 'material-ui/svg-icons/content/clear'
import {DatePicker, TextField} from 'redux-form-material-ui'
import {dateFormat} from '../components/utilities/Formatters'
import ContractPicker from '../contracts/ContractPicker'
import ManagersFormField from '../contacts/ManagersFormField'

const formId = 'PortfolioAllocationFiltersForm'

export const noPendingContracts = ({status}) => ['PENDING_TERMINATION', 'ACTIVE', 'TERMINATED'].indexOf(status) > 0

const CLEAR_ALL_BUTTON_STYLE = {
    alignItems: 'flex-end',
    display: 'flex',
    justifyContent: 'center',
    marginBottom: 20
}

export const ClearDateButton = ({clearDate, dateProp}) => (
    <IconButton id={`${dateProp}-date-clear`} style={{marginTop: 25}} onClick={() => clearDate(dateProp)}>
      <Clear/>
    </IconButton>)

ClearDateButton.propTypes = {
    clearDate: T.func.isRequired,
    dateProp: T.string.isRequired
}

export const PortfolioAllocationFilter = ({clearAll, clearDate, from, to}) => {
  return (
    <form id={formId}>
      <div style={{display: 'grid', gridTemplateColumns: 'Calc(100% / 3) Calc(100% / 3) Calc(100% / 3)', gridGap: 15}}>
        <ContractPicker nullable={true} name="contractId" type="text"
          filterContract={noPendingContracts}
          floatingLabelText="Contract" />
        <ManagersFormField nullable={true} name="portfolioManagerId" floatingLabelText="Manager" />
        <Field fullWidth name='searchPortfolios' component={TextField} floatingLabelText='Search Portfolios'/>
      </div>
      <div style={{display: 'grid', gridTemplateColumns: 'Calc((100% / 3) - 70px) 70px Calc((100% / 3) - 70px) 70px Calc((100% / 3) - 60px)', gridGap: 15}}>
        <Field autoOk fullWidth name="from" component={DatePicker} firstDayOfWeek={0} floatingLabelText="From" formatDate={dateFormat} />
        <ClearDateButton dateProp="from" clearDate={clearDate} />
        <Field autoOk fullWidth name="to" component={DatePicker} firstDayOfWeek={0} floatingLabelText="To" formatDate={dateFormat} />
        <ClearDateButton dateProp="to" clearDate={clearDate} />
        <RaisedButton style={{maxWidth: 200, maxHeight: 36, margin: '18px auto 0'}} id="clearButtonid" label="Clear All" onClick={clearAll} />
      </div>
    </form>
  )
}

PortfolioAllocationFilter.propTypes = {
    clearAll: T.func.isRequired,
    clearDate: T.func.isRequired,
    from: T.func,
    to: T.func
}

const PortfolioAllocationFiltersForm = reduxForm({
    form: formId
})(PortfolioAllocationFilter)

export const actions = {
    clearAll: () => (dispatch) => {
        dispatch(reset('PortfolioAllocationFiltersForm'))
    },
    clearDate: (dateProp) => (dispatch) => {
        dispatch(change('PortfolioAllocationFiltersForm', dateProp, null))
    }
}

export const mapStateToProps = ({form}) => {
    return {
        from: _.get(form, 'PortfolioAllocationFiltersForm.values.from'),
        to: _.get(form, 'PortfolioAllocationFiltersForm.values.to')
    }
}

export default connect(mapStateToProps, actions)(PortfolioAllocationFiltersForm)
