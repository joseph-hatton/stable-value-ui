import React from 'react'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import AddEditPortfolioComponent from './AddEditPortfolioComponent'
import { savePortfolio, loadPortfolio } from './PortfolioActions'

export class AddEditPortfolioContainer extends React.Component {
  componentDidMount () {
    const {params, loadPortfolio} = this.props
    if (params.portfolioId) {
      loadPortfolio(params.portfolioId)
    }
  }

  render () {
    return (<AddEditPortfolioComponent {...this.props} />)
  }
}

AddEditPortfolioContainer.displayName = 'AddEditPortfolioContainer'

AddEditPortfolioContainer.propTypes = {
  router: T.object.isRequired,
  params: T.object,
  loadPortfolio: T.func.isRequired
}

const actions = {
  loadPortfolio,
  savePortfolio
}

export const mapStateToProps = ({portfolios: {editPortfolio}}) => {
  const props = {
    editPortfolio
  }
  return props
}

export default connect(mapStateToProps, actions)(AddEditPortfolioContainer)
