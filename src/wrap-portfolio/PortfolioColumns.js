import React from 'react'
import { ViewEditPortfolioMenu } from './ViewEditPortfolioMenu'
import { DeletePortfolioButton } from './DeletePortfolioButton'

const commonPropsLarge = {
  customProps: {
    textAlignHeader: 'left',
    textAlign: 'left',
    width: '150'
  }
}
const commonPropsSmall = {
  customProps: {
    textAlignHeader: 'left',
    textAlign: 'left',
    width: '50'
  }
}

export const PortfolioColumns = [
  {
    label: 'id',
    field: 'portfolioId',
    customProps: {
      isKey: true,
      hidden: true
    }
  },
  {
    field: 'portfolioName',
    sort: true,
    label: 'Portfolio Name',
    customProps: {
      textAlignHeader: 'left',
      textAlign: 'left',
      width: 'Calc(100% / 3)'
    },
    renderer: (cell, row) => ( // eslint-disable-line react/display-name
      <ViewEditPortfolioMenu cell={cell} row={row} />
    )
  },
  {
    field: 'managerName',
    sort: true,
    label: 'Manager',
    customProps: {
      textAlignHeader: 'left',
      textAlign: 'left',
      width: 'Calc(100% / 3)'
    }
  },
  {
    field: 'active',
    sort: true,
    label: 'Active',
    customProps: {
      textAlignHeader: 'left',
      textAlign: 'left',
      width: 'Calc(100% / 3)'
    }
  }

]
