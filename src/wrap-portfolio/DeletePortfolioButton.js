import React from 'react'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import { red500 } from 'material-ui/styles/colors'
import { PropTypes as T } from 'prop-types'
import { connect } from 'react-redux'
import { openConfirmationAlert, openSnackbar } from '../components/redux-dialog'
import { deletePortfolio } from './PortfolioActions'

export const DeletePortfolio = ({
    row: Portfolio,
    openConfirmationAlert,
    confirmationAlert,
    openSnackbar,
    deletePortfolio
  }) => (
<div>
<a href="" id='deletePortfolioId' onClick={(e) => {
    e.preventDefault()
    if (Portfolio.active === 'NO') {
    openConfirmationAlert({
        message: 'Are you sure you want to delete this Portfolio?',
        onOk: () => deletePortfolio(Portfolio.portfolioId)
    })
    } else {
        openConfirmationAlert({
            message: 'You can not delete this Portfolio',
            onOk: () => {}
        }
    )
}
}} ><DeleteIcon color={red500}/></a>
</div>
)

DeletePortfolio.propTypes = {
row: T.object,
confirmationAlert: T.func,
openConfirmationAlert: T.func.isRequired,
openSnackbar: T.func.isRequired,
deletePortfolio: T.func.isRequired
}

const actions = {
openConfirmationAlert,
openSnackbar,
deletePortfolio
}

export const DeletePortfolioButton = connect(null, actions)(DeletePortfolio)
