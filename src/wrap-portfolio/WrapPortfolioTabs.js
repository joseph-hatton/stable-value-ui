import React from 'react'
import PortfolioAllocationComponent from './PortfolioAllocationComponent'
import PortfolioComponent from './PortfolioComponent'

const WrapPortfolioTabs = [
  {
    label: 'Allocations',
    PortfolioTab: PortfolioAllocationComponent,
    url: 'allocations'
  },
  {
    label: 'Portfolios',
    PortfolioTab: PortfolioComponent,
    url: 'portfolios'
  }
]

export default WrapPortfolioTabs
