import React from 'react'
import { PropTypes as T } from 'prop-types'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import { blue500, red500 } from 'material-ui/styles/colors'
import { connect } from 'react-redux'
import { Popover, Menu, MenuItem } from 'material-ui'
import { openDialog, closeDialog } from './../components/redux-dialog/redux-dialog'
import { selectPortfolioInputRow, deletePortfolio } from './PortfolioActions'
import {hasEntitlement} from '../entitlements'
import { openConfirmationAlert, openSnackbar } from '../components/redux-dialog'

const MenuLink = ({cell, openDialog, row, selectPortfolioInputRow}) => {
  return (
    <a href="" onClick={(e) => {
      e.nativeEvent.preventDefault()
      selectPortfolioInputRow(row)
      openDialog('portfolioMenu', {
        anchorEl: e.currentTarget
      })
    }}> {cell} </a>
  )
}

MenuLink.propTypes = {
  cell: T.node,
  row: T.object.isRequired,
  openDialog: T.func.isRequired,
  selectPortfolioInputRow: T.func.isRequired
}

export const ViewEditPortfolioMenu = connect(null, {openDialog, selectPortfolioInputRow})(MenuLink)

const Divider = () => (<hr style={{margin: 0}}/>)

export const portfolioMenu = ({open, anchorEl, closeDialog, selectPortfolioInputRow, openDialog, editPortfolio, deletePortfolio, openConfirmationAlert}) => (
  <Popover
    id='portfolioMenu-Popover'
    open={open}
    anchorEl={anchorEl}
    anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
    targetOrigin={{horizontal: 'left', vertical: 'top'}}
    onRequestClose={() => {
      closeDialog('portfolioMenu')
    }}
  >
    <Menu>
      <MenuItem primaryText="View/Edit Portfolio" onClick={() => {
        closeDialog('portfolioMenu')
        openDialog('portfolioInput')
      }} leftIcon={<EditIcon color={blue500}/>}/>
      <Divider/>
      <MenuItem primaryText="Delete Portfolio"
      disabled={!hasEntitlement('MODIFY_PORTFOLIO')}
      onClick={() => {
        closeDialog('portfolioMenu')
        if (editPortfolio.active === 'NO') {
          openConfirmationAlert({
            message: 'Are you sure you want to delete this Portfolio?',
            onOk: () => {
              deletePortfolio(editPortfolio.portfolioId)
            }
          })
        } else {
          console.log(editPortfolio)
          openConfirmationAlert({
              message: 'You can not delete this Portfolio',
              onOk: () => {
              }
            }
          )
        }
      }} leftIcon={<DeleteIcon color={red500}/>}/>
    </Menu>
  </Popover>
)

portfolioMenu.propTypes = {
  open: T.bool.isRequired,
  anchorEl: T.object,
  closeDialog: T.func.isRequired,
  openDialog: T.func.isRequired,
  selectPortfolioInputRow: T.func.isRequired,
  deletePortfolio: T.func.isRequired,
  openConfirmationAlert: T.func.isRequired,
  editPortfolio: T.object
}

export const mapStateToProps = ({dialog: {portfolioMenu}, portfolios: { editPortfolio }}) => {
  return {
    open: !!portfolioMenu,
    anchorEl: portfolioMenu ? portfolioMenu.anchorEl : null,
    editPortfolio
  }
}

const actions = {
  closeDialog,
  selectPortfolioInputRow,
  openDialog,
  deletePortfolio,
  openConfirmationAlert
}

export default connect(mapStateToProps, actions)(portfolioMenu)
