import { browserHistory } from 'react-router'
import { apiPath } from '../config'
import http from '../actions/http'
import { change, destroy } from 'redux-form'
import _ from 'lodash'
const PORTFOLIO_ALLOCATION_HOME = '/wrap-portfolio'
const ALLOCATION_DELETED_MESSAGE = 'Successfully deleted'
export const loadAllPortfolioAllocations = () => {
  return {
    type: 'LOAD_ALL_PORTFOLIO_ALLOCATIONS',
    payload: http.get(`${apiPath}/portfolio-allocations`)
  }
}

export const saveAllocation = () => {
  return (dispatch, getState) => {
    const {form: {AddEditPortfolioAllocationForm}, portfolioAllocations: {editAllocation}} = getState()
    const method = editAllocation ? 'put' : 'post'
    const url = editAllocation ? `${apiPath}/portfolio-allocation/${editAllocation.portfolioAllocationId}` : `${apiPath}/portfolio-allocations`
    if (AddEditPortfolioAllocationForm.values.version === null || isNaN(AddEditPortfolioAllocationForm.values.version)) { // Remove NULL fields to Support API validation
        AddEditPortfolioAllocationForm.values.version = 1
      } else {
        AddEditPortfolioAllocationForm.values.version++
      }
    dispatch({
      type: 'SAVE_PORTFOLIO_ALLOCATION',
      payload: http[method](url, AddEditPortfolioAllocationForm.values, {successMessage: 'Allocation saved successfully'})
        .then(result => {
          browserHistory.push(PORTFOLIO_ALLOCATION_HOME)
          return result
        })
    })
  }
}
export const loadAllocation = (portfolioAllocation) => (dispatch) => {
    let portfolioAllocationId
    if (portfolioAllocation.portfolioAllocationId) {
        portfolioAllocationId = portfolioAllocation.portfolioAllocationId
    } else {
        portfolioAllocationId = portfolioAllocation
    }
  dispatch({
    type: 'LOAD_PORTFOLIO_ALLOCATION',
    payload: http.get(`${apiPath}/portfolio-allocation/${portfolioAllocationId}`).then(allocation => {
      return http
        .get(`${apiPath}/portfolios?portfolioManagerId=${allocation.portfolioManagerId}`)
        .then(portfolios => ({allocation, portfolios}))
        .then(browserHistory.push(`/wrap-portfolio/allocation/edit/${portfolioAllocationId}`))
    })
  })
}
export const resetAllocations = (resetFields) => (dispatch, getState) => {
    resetFields.forEach(field => {
        dispatch(change('AddEditPortfolioAllocationForm', field.id, null))
        dispatch(change('AddEditPortfolioAllocationForm', field.name, null))
        dispatch(change('AddEditPortfolioAllocationForm', field.rate, null))
    })
}

export const filterAllocationsByContract = (filters, allocations) => {
    allocations = _.filter(allocations, (allocation) => {
        if (filters.contractId === undefined) {
            return allocation
        } else {
            if (allocation.contractId === filters.contractId) {
                return allocation
            }
        }
    })
    return allocations
}
export const filterAllocationsByManager = (filters, allocations) => {
    allocations = _.filter(allocations, (allocation) => {
        if (filters.portfolioManagerId === undefined) {
            return allocation
        } else {
            if (allocation.portfolioManagerId === filters.portfolioManagerId.toString()) {
                return allocation
            }
        }
    })
    return allocations
}
export const selectAllocationInputRow = (editAllocation) => ({
  type: 'SELECT_ALLOCATION_INPUT',
  payload: editAllocation
})
export const clearModelInputEditForm = () => (dispatch) => {
  dispatch(destroy('AddEditPortfolioAllocationForm'))
  dispatch({
    type: 'CLEAR_EDIT_ALLOCATION',
    payload: undefined
  })
}
export const deleteAllocation = (portfolioAllocationId) => (dispatch) => {
  dispatch({
    type: 'DELETE_ALLOCATION',
    payload: http.delete(`${apiPath}/portfolio-allocation/${portfolioAllocationId}`, null, ALLOCATION_DELETED_MESSAGE)
  })
}

const numbers = [
  'One',
  'Two',
  'Three',
  'Four',
  'Five',
  'Six',
  'Seven',
  'Eight'
]

export const mappedPortfolios = (row) => _.reduce(numbers, (result, number) => {
  if (row[`portfolio${number}Name`]) {
    result.push({
      id: row[`portfolio${number}Id`],
      name: row[`portfolio${number}Name`],
      rate: row[`portfolio${number}Rate`]
    })
  }
  return result
}, [])

export const searchPortfolioAllocations = (allocations) => (dispatch, getState) => {
  const {form} = getState()
  const searchValue = _.get(form, 'PortfolioAllocationFiltersForm.values.searchPortfolios')
  const results = searchValue ? _.filter(allocations, (allocation) =>
    _.some(mappedPortfolios(allocation), ({name}) => _.includes((name || '').toLowerCase(), searchValue.toLowerCase())))
  : allocations
  console.log('vals', {searchValue, allocations, results})
  return results
}
