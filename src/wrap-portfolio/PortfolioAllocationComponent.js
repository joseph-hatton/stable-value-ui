import React from 'react'
import { PropTypes as T } from 'prop-types'
import _ from 'lodash'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import { red500 } from 'material-ui/styles/colors'
import { Grid, CreateGridConfig } from '../components/grid'
import {selectAllocationInputRow, loadAllPortfolioAllocations, loadAllocation,
  filterAllocationsByContract, filterAllocationsByManager, clearModelInputEditForm, searchPortfolioAllocations} from './PortfolioAllocationActions'
import {PortfolioAllocationColumns} from './PortfolioAllocationColumns'
import {Card} from 'material-ui'
import { openDialog, closeDialog } from '../components/redux-dialog/redux-dialog'
import PortfolioAllocationFilter from './PortfolioAllocationFilter'
import moment from 'moment'
import {hasEntitlement} from '../entitlements'
import {serializeDates} from '../actions/serializeDate'
import Expandable from '../components/common/Expandable'
import Filter from 'material-ui/svg-icons/content/filter-list'
import ViewEditAllocationMenu from './ViewEditAllocationMenu'
import AddEditPortfolioAllocationComponent from './AddEditPortfolioAllocationComponent'

export const formatValues = (filters = {}) => {
    const newFilters = _.omitBy(filters, filter => !filter)
    const {from, to} = filters
    if (from) {
        newFilters.from = from
    }
    if (to) {
        newFilters.to = to
    }
    return serializeDates(newFilters)
}

export class PortfolioAllocationComponent extends React.Component {
  componentDidMount () {
    const {editAllocation, allocations, clearEditAllocationState, loadAllPortfolioAllocations} = this.props
    if (editAllocation) {
      clearEditAllocationState()
    }
    if (_.isEmpty(allocations)) {
      loadAllPortfolioAllocations()
    }
  }

  filterAllocations = (filters, allocations) => {
    const {searchPortfolioAllocations} = this.props

    allocations = filterAllocationsByContract(filters, allocations)
    allocations = filterAllocationsByManager(filters, allocations)
    allocations = searchPortfolioAllocations(allocations)

    return _.filter(allocations, (allocation) => {
      if (filters.from && filters.to === undefined) {
        return moment(allocation.effectiveDate).isAfter(filters.from)
      } else {
          if (filters.to && filters.from === undefined) {
              return moment(allocation.effectiveDate).isBefore(filters.to)
          } else {
            if (filters.to && filters.from) {
              return moment(allocation.effectiveDate).isBetween(filters.from, filters.to)
          } else {
                return true
            }
          }
      }
    })
  }

    render () {
    const filters = formatValues(this.props.filters)
    const {allocations, loadAllocation, openDialog, selectAllocationInputRow} = this.props
    const options = CreateGridConfig(null, {sizePerPage: 20, showHeaderToolbar: false})
    return (
      <div>
        {
          !_.isEmpty(allocations)
          && (
            <div>
              <Expandable id="TransactionFilters" title={<span>Allocation Filters</span>} icon={<Filter/>}>
                <PortfolioAllocationFilter/>
              </Expandable>
              <Grid id="allocationGrid"
                    onRowDoubleClick={(row) => {
                      selectAllocationInputRow(row)
                      openDialog('allocationInput')
                    }}
                    options={options}
                    columns={PortfolioAllocationColumns}
                    data={this.filterAllocations(filters, allocations)}
                    />
                <FloatingActionButton disabled={!hasEntitlement('MODIFY_PORTFOLIO')}
                                      backgroundColor={red500} mini={false} className={'floating-add-button'}
                                      onClick={() => {
                                        this.props.clearModelInputEditForm()
                                        openDialog('allocationInput')
                                      }}
                >
                  <ContentAdd/>
                </FloatingActionButton>
              <ViewEditAllocationMenu/>
              <AddEditPortfolioAllocationComponent/>
            </div>)
        }
      </div>
    )
  }
}

PortfolioAllocationComponent.propTypes = {
  allocations: T.array,
  editAllocation: T.object,
  loadAllocation: T.func.isRequired,
  loadAllPortfolioAllocations: T.func.isRequired,
  clearEditAllocationState: T.func.isRequired,
  selectAllocationInputRow: T.func.isRequired,
  clearModelInputEditForm: T.func.isRequired,
  filters: T.object,
  openDialog: T.func.isRequired,
  searchPortfolioAllocations: T.func.isRequired
}

export const actions = {
  loadAllPortfolioAllocations,
  loadAllocation,
  selectAllocationInputRow,
  openDialog,
  closeDialog,
  clearModelInputEditForm,
  searchPortfolioAllocations,
  clearEditAllocationState: () => ({
    type: 'CLEAR_EDIT_ALLOCATION'
  })
}
const NO_FILTERS = {}
export const mapStateToProps = (state) => {
  const { portfolioAllocations = {}, form: { PortfolioAllocationFiltersForm = {} } } = state
    return {
    allocations: portfolioAllocations.allocations,
    editAllocation: portfolioAllocations.editAllocation,
    filters: PortfolioAllocationFiltersForm ? PortfolioAllocationFiltersForm.values : NO_FILTERS,
    contractId: portfolioAllocations.contractId
  }
}

export default connect(mapStateToProps, actions)(PortfolioAllocationComponent)
