import React from 'react'
import { PropTypes as T } from 'prop-types'
import MaterialSelectField from '../components/forms/MaterialSelectField'
import { connect } from 'react-redux'
import { loadPortfolioByContract } from './PortfolioActions'
import _ from 'lodash'

export class PortfolioByContractPicker extends React.Component {
  componentDidMount () {
    const {options, loadPortfolioByContract, contractId} = this.props
    if (_.isEmpty(options) && contractId) {
      loadPortfolioByContract(contractId)
    }
  }

  render () {
    return (<MaterialSelectField {..._.omit(this.props, ['loadPortfolioByContract'])} options={this.props.options}/>)
  }
}

PortfolioByContractPicker.propTypes = {
  name: T.string.isRequired,
  options: T.array,
  loadPortfolioByContract: T.func.isRequired,
  contractId: T.number
}

PortfolioByContractPicker.displayName = 'PortfolioByContractPicker'

export const mapStateToProps = ({portfolioAllocations: {portfoliosByContract: {results}, contractId}}) => ({
    options: (results || []).map(portfolio => ({text: `${portfolio.portfolioName}`, value: portfolio.portfolioId})),
    contractId: contractId
})

export default connect(mapStateToProps, {loadPortfolioByContract})(PortfolioByContractPicker)
