import React from 'react'
import { connect } from 'react-redux'
import WrapPortfolioTabs from './WrapPortfolioTabs'
import { Tabs, Tab } from 'material-ui/Tabs'
import { Card } from 'material-ui/Card'
import * as _ from 'lodash'
import * as T from 'react/lib/ReactPropTypes'
import { goToTab, setOrToggleListItem } from '../components/common/LeftDrawerActions'
import { browserHistory } from 'react-router'

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 900
  },

  tabContainer: {
    marginTop: 20
  },
  tabItemContainerStyle: {
    position: 'sticky',
    top: 60,
    zIndex: 2
  },
  mainTab: {
    backgroundColor: '#0062cc',
    fontWeight: 600
  }
}

export class WrapPortfolioContainer extends React.Component {
  componentDidMount () {
    if (!this.props.wrapPortfolio) {
      this.props.setOrToggleListItem('wrapPortfolio', true)
    }
    const selectedTab = _.findIndex(WrapPortfolioTabs, ['label', this.props.selectedTab]) === -1 ? 'Allocations' : this.props.selectedTab
    this.props.goToTab(selectedTab)
  }
  componentWillUnmount () {
    if (this.props.wrapPortfolio) {
      this.props.setOrToggleListItem('wrapPortfolio', false)
    }
  }
  render () {
    const tabs = WrapPortfolioTabs
    const indexOfLastTab = _.findIndex(WrapPortfolioTabs, tab => tab.label === this.props.selectedTab)
    return (
      <Tabs className="WrapPortfolioTab" value={this.props.selectedTab}
            tabItemContainerStyle={styles.tabItemContainerStyle}
            initialSelectedIndex={indexOfLastTab > -1 ? indexOfLastTab : 0}>
        {
          tabs.map(({label, PortfolioTab, url, loadDynamically}) => (
            <Tab key={label} label={label}
                  onActive={() => browserHistory.push(`/wrap-portfolio/${url}`)}
                  value={label}
                  style={styles.mainTab}>
              {this.props.selectedTab === label && <PortfolioTab {...this.props}/>}
            </Tab>
          ))
        }
      </Tabs>
    )
  }
}

WrapPortfolioContainer.propTypes = {
  selectedTab: T.string,
  goToTab: T.func.isRequired,
  wrapPortfolio: T.bool,
  setOrToggleListItem: T.func.isRequired
}

const mapStateToProps = ({sideBar: { selectedTab, wrapPortfolio }}) => ({
  selectedTab,
  wrapPortfolio
})

export default connect(mapStateToProps, {goToTab, setOrToggleListItem})(WrapPortfolioContainer)
