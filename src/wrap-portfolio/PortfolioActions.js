import { browserHistory } from 'react-router'
import { apiPath } from '../config'
import http from '../actions/http'
import _ from 'lodash'
import { mapContract } from '../contracts/ContractPayloadMapper'
import {destroy, change, initialize} from 'redux-form'
import {openDialog, closeDialog, openConfirmationAlert} from '../components/redux-dialog/redux-dialog'

const PORTFOLIOS_HOME = '/wrap-portfolio'
const PORTFOLIO_DELETED_MESSAGE = 'Successfully deleted'
export const loadAllPortfolios = () => {
  return {
    type: 'LOAD_ALL_PORTFOLIOS',
    payload: http.get(`${apiPath}/portfolios`)
  }
}

export const savePortfolio = () => {
  return (dispatch, getState) => {
    const {form: {AddEditPortfolioForm}, portfolios: {editPortfolio}} = getState()
    const method = editPortfolio ? 'put' : 'post'
    const url = editPortfolio ? `${apiPath}/portfolios/${editPortfolio.portfolioId}` : `${apiPath}/portfolios`
    if (AddEditPortfolioForm.values.version === null || isNaN(AddEditPortfolioForm.values.version)) { // Remove NULL fields to Support API validation
      AddEditPortfolioForm.values.version = 1
    } else {
      AddEditPortfolioForm.values.version++
    }
    dispatch({
      type: 'SAVE_PORTFOLIO',
      payload: http[method](url, AddEditPortfolioForm.values, {successMessage: 'Portfolio saved successfully'})
        .then(result => {
          browserHistory.push(PORTFOLIOS_HOME)
          return result
        })
    })
  }
}
export const loadPortfolio = (portfolioId) => (dispatch) => {
  dispatch({
    type: 'LOAD_PORTFOLIO',
    payload: http.get(`${apiPath}/portfolios/${portfolioId}`)
  })
}
export const loadPortfolioByContract = (contractId) => (dispatch) => {
  console.log(contractId)
  dispatch({
    type: 'LOAD_PORTFOLIO_BY_CONTRACT',
    payload: http
      .get(`${apiPath}/contracts/${contractId}`)
      .then(contract => {
        const mappedContract = mapContract(contract)
        return http
          .get(`${apiPath}/portfolios?portfolioManagerId=${mappedContract.managerId}`)
          .then(portfolios => ({portfolios, contractId}))
      }),
    contractId
  })
}

export const selectPortfolioInputRow = (editPortfolio) => ({
  type: 'SELECT_PORTFOLIO_INPUT',
  payload: editPortfolio
})

export const deletePortfolio = (portfolioId) => (dispatch) => {
  dispatch({
    type: 'DELETE_PORTFOLIO',
    payload: http.delete(`${apiPath}/portfolios/${portfolioId}`, null, PORTFOLIO_DELETED_MESSAGE)
  })
}

export const clearModelInputEditForm = () => (dispatch) => {
    dispatch(destroy('AddEditPortfolioForm'))
    dispatch({
        type: 'CLEAR_EDIT_PORTFOLIO',
        payload: undefined
    })
}

export const openPortfolio = (portfolio) => (dispatch) => {
    dispatch({
        type: 'SELECT_PORTFOLIO',
        payload: portfolio
    })
    dispatch(openDialog('portfolioInput'))
}

export const activatePortfoliosTab = (label) => ({
  type: 'ACTIVATE_PORTFOLIOS_TAB',
  label
})
