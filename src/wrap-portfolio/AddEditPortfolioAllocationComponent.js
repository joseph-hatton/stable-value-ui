import React from 'react'
import _ from 'lodash'
import { PropTypes as T } from 'prop-types'
import { Link } from 'react-router'
import { Card } from 'material-ui/Card'
import RaisedButton from 'material-ui/RaisedButton'
import { Field, reduxForm, change, reset } from 'redux-form'
import { connect } from 'react-redux'
import { DatePicker, TextField } from 'redux-form-material-ui'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import { Required } from '../components/forms/Validators'
import { toUtc } from '../components/utilities/Formatters'
import ContractPicker from '../contracts/ContractPicker'
import PortfolioByContractPicker from './PortfolioByContractPicker'
import { hasEntitlement } from '../entitlements'
import ReduxDialog from '../components/redux-dialog'
import { closeDialog } from '../components/redux-dialog/redux-dialog'
import { loadPortfolioByContract } from './PortfolioActions'
import { resetAllocations, clearModelInputEditForm, saveAllocation } from './PortfolioAllocationActions'
const portfolios = [
  {
    id: 'portfolioOneId',
    rate: 'portfolioOneRate',
    name: 'portfolioOneName',
    label: '* Portfolio 1:'
  },
  {
    id: 'portfolioTwoId',
    rate: 'portfolioTwoRate',
    name: 'portfolioTwoName',
    label: '* Portfolio 2:'
  },
  {
    id: 'portfolioThreeId',
    rate: 'portfolioThreeRate',
    name: 'portfolioThreeName',
    label: '* Portfolio 3:'
  },
  {
    id: 'portfolioFourId',
    rate: 'portfolioFourRate',
    name: 'portfolioFourName',
    label: '* Portfolio 4:'
  },
  {
    id: 'portfolioFiveId',
    rate: 'portfolioFiveRate',
    name: 'portfolioFiveName',
    label: '* Portfolio 5:'
  },
  {
    id: 'portfolioSixId',
    rate: 'portfolioSixRate',
    name: 'portfolioSixName',
    label: '* Portfolio 6:'
  },
  {
    id: 'portfolioSevenId',
    rate: 'portfolioSevenRate',
    name: 'portfolioSevenName',
    label: '* Portfolio 7:'
  },
  {
    id: 'portfolioEightId',
    rate: 'portfolioEightRate',
    name: 'portfolioEightName',
    label: '* Portfolio 8:'
  }
]

const calcTotal = (rates) => rates ? _.reduce(portfolios, (total, portfolio) => total + rates[portfolio.rate], 0) : 0
const formId = 'AddEditPortfolioAllocationForm'

export class AddEditPortfolioAllocationComponent extends React.Component {
    doSomething (event, contractId) {
      if (contractId) {
        this.props.loadPortfolioByContract(contractId)
      }
        this.props.resetAllocations(portfolios)
    }
  render () {
    const {
      closeDialog, handleSubmit, clearModelInputEditForm, editAllocation,
      saveAllocation, contractId, addPortfolioAllocations
    } = this.props
    const ActiveContracts = ({status}) => status === 'ACTIVE'
    const portfolioAllocationId = this.props.initialValues ? this.props.initialValues.portfolioAllocationId : undefined
    const filterContractProp = portfolioAllocationId ? {} : {filterContract: ActiveContracts}
    const _addPortfolioAllocations = (portfolioRate) => (event, value) => {
        addPortfolioAllocations(portfolioRate, value)
    }
    const total = calcTotal(this.props)
    const invalidTotal = total !== 100
    const actionForm = [
      <RaisedButton key='save' label="Save"
                    form={formId} primary={true}
                    disabled={!hasEntitlement('MODIFY_PORTFOLIO') || invalidTotal}
                    onClick={() => {
                      saveAllocation()
                      closeDialog('allocationInput')
                    }}
      />,
      <RaisedButton key='cancel' label="Cancel" onClick={() => {
        this.props.clearModelInputEditForm()
        closeDialog('allocationInput')
      }} />
    ]
    const textClassNameChange = invalidTotal ? 'fontError' : null
    return (
        <ReduxDialog contentStyle={{maxWidth: 1000}} actions={actionForm}
                     title='Add/Edit Portfolio Allocation' dialogName='allocationInput'
                     modal={true} autoScrollBodyContent={true}>
        <form id={formId} onSubmit={handleSubmit(AddEditPortfolioAllocationComponent)}>
          <Card>
            <div style={{padding: '25px'}}>
              <div className="row">
                <div className="col-lg-5">
                  <Field validate={Required} fullWidth name="effectiveDate" format={toUtc} component={DatePicker} firstDayOfWeek={0}
                         floatingLabelText="* Date" autoOk />
                </div>
                <div className="col-lg-4">
                  <ContractPicker id="ContractPickerPortfolio" validate={Required} name="contractId" floatingLabelText="* Contract" {...filterContractProp}
                                  onChange={::this.doSomething} disabled={portfolioAllocationId !== undefined}/>
                </div>
              </div>
              {portfolios.map((portfolio, index) => {
                  return (
                    <div key={index} className="row">
                      <div className="col-lg-5">
                        <PortfolioByContractPicker name={portfolio.id} floatingLabelText={portfolio.label } disabled={!contractId}/>
                      </div>
                      <div style={{
                          marginBottom: 18,
                          alignItems: 'flex-end',
                          display: 'flex',
                          justifyContent: 'center'
                      }} className="col-lg-1">
                          <Field fullWidth={true} parse={_.parseInt} name={portfolio.rate} component={TextField} type="number" disabled={this.props[portfolio.id] === 0}/>
                      </div>
                    </div>
                  )
                })
              }
            </div>
          </Card>
        </form>
        </ReduxDialog>
    )
  }
}

AddEditPortfolioAllocationComponent.propTypes = {
  saveAllocation: T.func.isRequired,
  initialValues: T.object,
  handleSubmit: T.func.isRequired,
  loadPortfolioByContract: T.func.isRequired,
  closeDialog: T.func.isRequired,
  contractId: T.number,
  editAllocation: T.object,
  addPortfolioAllocations: T.func.isRequired,
  portfolioOneRate: T.number,
  resetAllocations: T.func.isRequired,
  clearModelInputEditForm: T.func.isRequired
}

export const actions = {
  closeDialog,
  resetAllocations,
  loadPortfolioByContract,
  clearModelInputEditForm,
  saveAllocation,
  addPortfolioAllocations: (portfolioRate, value) => (dispatch) => {
      dispatch(change('AddEditPortfolioAllocationForm', portfolioRate, value))
  }
}

export const map = (form) => {
  const mappingObj = {}
  portfolios.forEach(({rate}) => {
     mappingObj[rate] = _.get(form, `AddEditPortfolioAllocationForm.values.${rate}`) ? _.get(form, `AddEditPortfolioAllocationForm.values.${rate}`) : 0
  })
  portfolios.forEach(({id}) => {
       mappingObj[id] = _.get(form, `AddEditPortfolioAllocationForm.values.${id}`) ? _.get(form, `AddEditPortfolioAllocationForm.values.${id}`) : 0
  })
  return mappingObj
}

export const mapStatesToProps = ({form, portfolioAllocations: {editAllocation, initialValues, contractId}}) => ({
  initialValues: editAllocation,
  contractId: contractId,
  ...map(form)
})

export const AddEditPortfolioAllocationForm = reduxForm({
  form: 'AddEditPortfolioAllocationForm',
  enableReinitialize: true
})(AddEditPortfolioAllocationComponent)

export default connect(mapStatesToProps, actions)(AddEditPortfolioAllocationForm)
