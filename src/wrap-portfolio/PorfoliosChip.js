import React from 'react'
import Chip from 'material-ui/Chip'

export default class ChipPortfolioArray extends React.Component {
        constructor (props) {
        super(props)

        this.styles = {
            chip: {
                margin: 10,
                size: 0
            },
            wrapper: {
                display: 'flex',
                flexWrap: 'wrap'
            }
        }
    }

    renderChip (data) {
        return (
            <Chip key={data.key} >{data.label}</Chip>
        )
    }

    render () {
        const chipData = (props) => {
            const array = []
            if (props.data.portfolioOneName) {
                array.push({key: 1, label: props.data.portfolioOneName + '  %' + props.data.portfolioOneRate})
            }
            if (props.data.portfolioTwoName !== null) {
                array.push({key: 2, label: props.data.portfolioTwoName + '  %' + props.data.portfolioTwoRate})
            }
            if (props.data.portfolioThreeName) {
                array.push({key: 3, label: props.data.portfolioThreeName + '  %' + props.data.portfolioThreeRate})
            }
            if (props.data.portfolioFourName) {
                array.push({key: 4, label: props.data.portfolioFourName + '  %' + props.data.portfolioFourRate})
            }
            if (props.data.portfolioFiveName) {
                array.push({key: 5, label: props.data.portfolioFiveName + '  %' + props.data.portfolioFiveRate})
            }
            if (props.data.portfolioSixName) {
                array.push({key: 6, label: props.data.portfolioSixName + '  %' + props.data.portfolioSixRate})
            }
            if (props.data.portfolioSevenName) {
                array.push({key: 7, label: props.data.portfolioSevenName + '  %' + props.data.portfolioSevenRate})
            }
            if (props.data.portfolioEightName) {
                array.push({key: 8, label: props.data.portfolioEightName + '  %' + props.data.portfolioEightRate})
            }
            return array
        }
        return (
            <div style={this.styles.wrapper}>
                {chipData(this.props).map(this.renderChip, this)}
            </div>
        )
    }
}
