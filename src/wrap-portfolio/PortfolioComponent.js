import React from 'react'
import { PropTypes as T } from 'prop-types'
import _ from 'lodash'
import { connect } from 'react-redux'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import { red500 } from 'material-ui/styles/colors'
import { Grid, CreateGridConfig } from '../components/grid'
import {loadAllPortfolios, openPortfolio, clearModelInputEditForm} from './PortfolioActions'
import {PortfolioColumns} from './PortfolioColumns'
import ViewEditPortfolioMenu from './ViewEditPortfolioMenu'
import AddEditPortfolioComponent from './AddEditPortfolioComponent'
import { openDialog, closeDialog } from './../components/redux-dialog/redux-dialog'
import { reset } from 'redux-form'
import {hasEntitlement} from '../entitlements'

export class PortfolioComponent extends React.Component {
    componentDidMount () {
        const {portfolios, loadAllPortfolios, editPortfolio, clearEditPortfolioState} = this.props
        if (editPortfolio) {
            clearEditPortfolioState()
        }
        if (_.isEmpty(portfolios)) {
            loadAllPortfolios()
        }
    }
    componentWillReceiveProps (nextProps) {
        if (nextProps.editPortfolio === undefined) {
           this.props.closeDialog('portfolioInput')
          this.props.clearModelInputEditForm()
        }
    }
    render () {
        const {portfolios, openPortfolio} = this.props
        const config = {
            defaultSortName: 'effectiveDate',
            defaultSortOrder: 'asc',
            sizePerPage: 20
        }
        const options = CreateGridConfig(portfolios, config)
        return (
            <div>
                {
                    !_.isEmpty(portfolios)
                    && (
                        <div>
                          <Grid id="portfolioGrid" options={options} data={portfolios} columns={PortfolioColumns}
                                onRowDoubleClick={openPortfolio} />
                          <AddEditPortfolioComponent id='addEditPortfolioId' onClose={() => {
                              this.props.closeDialog('portfolioInput')
                              this.props.clearEditPortfolioState()
                              reset('AddEditPortfolioComponent')
                              this.props.clearModelInputEditForm()
                          }}/>
                            {hasEntitlement('MODIFY_PORTFOLIO') && <FloatingActionButton backgroundColor={red500} mini={false} className="floating-add-button" onClick={() => {
                                this.props.openDialog('portfolioInput')
                            }}>
                              <ContentAdd/>
                            </FloatingActionButton>}
                          <ViewEditPortfolioMenu/>
                        </div>)
                }
            </div>
        )
    }
}

PortfolioComponent.propTypes = {
    portfolios: T.array,
    editPortfolio: T.object,
    clearEditPortfolioState: T.func.isRequired,
    loadAllPortfolios: T.func.isRequired,
    openDialog: T.func.isRequired,
    closeDialog: T.func.isRequired,
    clearModelInputEditForm: T.func.isRequired,
    openPortfolio: T.func.isRequired
}

export const actions = {
    loadAllPortfolios,
    closeDialog,
    openDialog,
    clearModelInputEditForm,
    openPortfolio,
    clearEditPortfolioState: () => ({
        type: 'CLEAR_EDIT_PORTFOLIO'
    })
}

export const mapStateToProps = (state) => {
    const { portfolios = {} } = state
    return {
        portfolios: portfolios.portfolios,
        editPortfolio: portfolios.editPortfolio
    }
}

export default connect(mapStateToProps, actions)(PortfolioComponent)
