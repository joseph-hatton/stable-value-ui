// capture id and arguments of action... this enables easier testing
export default (id, fn, ...args) => {
  const action = (dispatch, getState) => fn(dispatch, getState)
  return Object.assign(action, {id, args: args.filter(x => x !== undefined)})
}
