import fromPairs from 'lodash/fromPairs'
import _get from 'lodash/get'
import isFunction from 'lodash/isFunction'
import omitBy from 'lodash/omitBy'
import agent from 'superagent'
import store from '../store/store'
import { spin, stop, openSnackbar, closeDialog } from '../components/redux-dialog'
import { serializeDates } from '../actions/serializeDate'
import getCurrentUser from './getCurrentUser'
import invokeHttpRequest from './invokeHttpRequest'

export const UNAUTHORIZED_ERROR_MESSAGE = 'Unauthorized. Contact Help Desk for Access.'
export const STANDARD_ERROR_MESSAGE = 'Error in processing your request, please contact support.'

const displaySuccessMessage = (response, {successMessage}) => {
  if (successMessage) {
    store.dispatch(openSnackbar({
      type: 'success',
      message: isFunction(successMessage) ? successMessage(response) : successMessage,
      autoHideDuration: 5000
    }))
  }
}

const displayErrorMessage = (response, {errorMessage = null} = {}) => {
  store.dispatch(openSnackbar({
    type: 'error',
    message: isFunction(errorMessage) ? errorMessage(response) || STANDARD_ERROR_MESSAGE : errorMessage || STANDARD_ERROR_MESSAGE,
    autoHideDuration: 5000 * 1000
  }))
}

const SKIP_DATE_SERIALIZATION = ['createdDate', 'modifiedDate']
const ignoreDate = field => SKIP_DATE_SERIALIZATION.includes[field]

const processPayload = (payload) => {
  return serializeDates(payload, ignoreDate)
}

const showSpinner = () => store.dispatch(spin())

const stopSpinner = () => {
  setTimeout(() => {
    store.dispatch(stop())
  }, 500)
}

const stopSpinnerOnError = (err) => {
  stopSpinner()
  let message = null
  if (err.status && err.status === 401) {
    message = {errorMessage: UNAUTHORIZED_ERROR_MESSAGE}
  } else if (err.response && err.response.body && err.response.body.message) {
    let messages = _get(err.response.body.message, 'messages', [])
    messages = messages.join(' ')
    console.log('here', {messages})
    message = {errorMessage: messages || err.response.body.message}
  }
  displayErrorMessage(err, message)
  return err
}

const apply = async ({method, url, acceptsJson = true, query, requestBody, headers}) => {
  let request = agent[method](url)
  if (acceptsJson) {
    request = request.accept('json')
  }
  if (query) {
    request = request.query(query)
  }
  if (headers) {
    request = request.set(headers)
  }
  if (requestBody) {
    request = request.type('json').send(processPayload(requestBody))
  }
  const {body} = await invokeHttpRequest(request)
  return body
}

const get = async (url, query = {}) => {
  showSpinner()
  try {
    const body = await apply({method: 'get', url, query})
    stopSpinner()
    return body
  } catch (err) {
    console.error(err)
    throw stopSpinnerOnError(err)
  }
}

const wrapModify = async (method, url, requestBody, messageConfig = {}, dialogToClose) => {
  showSpinner()
  try {
    const body = await apply({method, url, requestBody})
    stopSpinner()
    displaySuccessMessage(body, messageConfig)
    if (dialogToClose) {
      store.dispatch(closeDialog(dialogToClose))
    }
    return body
  } catch (err) {
    throw stopSpinnerOnError(err)
  }
}

const updateMethods = fromPairs(
  [
    'put',
    'post',
    'delete',
    'patch'
  ].map(method => [
    method,
    (url, requestBody, messageConfig, dialogToClose) => wrapModify(method, url, requestBody, messageConfig, dialogToClose)
  ])
)

const httpErrorMessage = (error) => {
  return _get(error, 'response.body.message')
}

export default {
  httpErrorMessage,
  get,
  getNoSpin: (url) => apply({method: 'get', url, acceptsJson: false}),
  ...updateMethods
}
