import agent from 'superagent'
import renderLogoutView from './renderLogoutView'

export default (dispatch, getState) =>
  agent.del('/__auth/current-user')
    .then(({body: {logoutUrl}}) => {
      const height = window.outerHeight
      const width = window.outerWidth
      renderLogoutView({url: logoutUrl, height, width})
    })
