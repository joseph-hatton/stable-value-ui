export default (taskTime, task) => {
  setTimeout(task, Math.max(0, taskTime - Date.now()))
}
