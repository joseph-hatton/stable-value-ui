import _ from 'lodash'
import moment from 'moment'

export const serializeDate = (date) => {
  return `${moment(date).format('YYYY-MM-DD')}T00:00:00Z`
}

export function serializeDates (object, ignore) {
  return _.transform(object, (result, value, key) => {
    if (ignore && ignore(key, value, object)) {
      result[key] = value
    } else if (_.isDate(value)) {
      result[key] = serializeDate(value)
    } else if (_.isArray(value)) {
      result[key] = value.map(obj => serializeDates(obj, ignore))
    } else if (_.isObject(value)) {
      result[key] = serializeDates(value, ignore)
    } else {
      result[key] = value
    }
  }, {})
}

export default serializeDate
