import moment from 'moment'
import { apiPath } from '../../config'
import store from '../../store/store'
import http from '../http'
import scheduleTask from '../scheduleTask'
import {loadDailyCycleMessages, loadMonthlyCycleMessages} from '../../cycles/CyclesActions'
import { openSnackbar } from '../../components/redux-dialog'

const url = `${apiPath}/cycles`

const whenToCheckAgain = () => moment().add(10, 'seconds').valueOf()

const isCycleRunning = async (dispatch, getState) => {
  try {
    const {isRunning} = await http.getNoSpin(url)
    const loadCycleMessages = isRunning !== getState().cycles.isRunning
    dispatch({type: 'IS_CYCLE_RUNNING', isRunning})
    if (loadCycleMessages) {
      store.dispatch(loadDailyCycleMessages())
      store.dispatch(loadMonthlyCycleMessages())
      if (isRunning === false) {
        store.dispatch(openSnackbar({
          type: 'success',
          message: 'Cycle completed.',
          autoHideDuration: 60000
        }))
      }
    }
    scheduleTask(whenToCheckAgain(), () => isCycleRunning(dispatch, getState))
    return isRunning
  } catch (err) {
    console.error('cycle error', err)
    scheduleTask(whenToCheckAgain(), () => isCycleRunning(dispatch, getState))
  }
}

export default isCycleRunning
