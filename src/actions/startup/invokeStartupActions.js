import {updateCurrentUser} from '../getCurrentUser'
import loadReferenceData from './loadReferenceData'
import isCycleRunning from './isCycleRunning'

const phaseOne = [
  updateCurrentUser
]

const phaseTwo = [
  loadReferenceData,
  isCycleRunning
]

export default async (dispatch, getState) => {
  const invoke = actions =>
    Promise.all(actions.map(action => action(dispatch, getState)))

  await invoke(phaseOne)
  await invoke(phaseTwo)
}
