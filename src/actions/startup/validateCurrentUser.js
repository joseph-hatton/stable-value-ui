let intervalId = 0
// todo: not currently used, this used to be referenced in invokeStartupActions.  If the timeout popup works, completely delete this.
export const cancel = () => clearInterval(intervalId)

export default (dispatch, getState, interval = 5000) => {
  intervalId = setInterval(() => {
    const {timedOut, expires} = getState().currentUser.session
    const expiresTime = new Date(expires).getTime()
    if (!timedOut && (isNaN(expiresTime) || expiresTime < Date.now())) {
      dispatch({type: 'TIME_OUT_USER'})
    }
  }, interval)
}
