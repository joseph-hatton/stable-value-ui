import { apiPath } from '../../config'
import http from '../http'

const loadReferenceData = dispatch =>
  dispatch({
    type: 'LOAD_REFERENCE_DATA',
    payload: Promise.all([
      http.get(`${apiPath}/reference`),
      http.get(`${apiPath}/derivative-types`)
    ]).then(responses => {
      responses[0].derivativeTypes = responses[1].results
      return responses[0]
    })
  })

export default loadReferenceData
