import getCurrentUser from './getCurrentUser'

export default async request => {
  const {session: {token}, customHeaders} = await getCurrentUser()
  if (token || customHeaders) {
    return request.set({
      ...(token ? {Authorization: `Bearer ${token}`} : {}),
      ...(customHeaders || {})
    })
  } else {
    return request
  }
}
