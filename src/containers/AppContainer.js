import React from 'react'
import { Router, browserHistory } from 'react-router'
import routes from '../routes'

export const App = () =>
  <Router routes={routes} history={browserHistory}>
  </Router>

App.displayName = 'App'

export default App
