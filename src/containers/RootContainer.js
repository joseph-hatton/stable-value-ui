/* eslint-disable no-unused-vars */
import React from 'react'
import {connect} from 'react-redux'
import Footer from '../components/Footer'
import store from './../store/createStore'

export const Root = () => {
  return (
      <Footer/>
  )
}

Root.displayName = 'Root'

export const mapStateToProps = () => ({})

export const mapDispatchToProps = () => ({})

export default connect(mapStateToProps, mapDispatchToProps)(Root)
