import React from 'react'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import Icon from '../components/utilities/Icon'
import LinkButton from '../components/utilities/LinkButton'

const maxMessageLen = 300

const trunc = message => message.length > maxMessageLen ? `${message.slice(0, maxMessageLen)} ...` : message

const normalize = message => trunc(message.replace(/\s+/g, ' '))

const toDisplayString = error => normalize(`${error.status || 'Error'} - ${error.message || '(no detail message)'}`)

export const ErrorPage = ({error}) =>
  <div className='container-fluid error-page'>
    <div className='alert alert-danger'>
      <strong>
        <Icon name='exclamation-circle'/>
        {' whoops!'}
      </strong>
      {' Unable to load PDM console (are you connected to the RGA network?)'}
    </div>
    <hr/>
    <div className='details'>
      {toDisplayString(error)}
    </div>
    <hr/>
    <LinkButton onClick={() => location.reload()}>
      <Icon name='refresh'/>
      {' try again'}
    </LinkButton>
  </div>

ErrorPage.displayName = 'ErrorPage'

ErrorPage.propTypes = {
  error: T.object.isRequired
}

export const mapStateToProps = ({initiative: {error}}) => ({error})

export default connect(mapStateToProps)(ErrorPage)
