import React from 'react'
import { Provider } from 'react-redux'
import store from './store/store'
import AppContainer from './containers/AppContainer'
import invokeStartupActions from './actions/startup/invokeStartupActions'

export default (main, render) => {
  return invokeStartupActions(store.dispatch, store.getState)
    .then(() => {
      const StartupContainer = AppContainer
      render(
        <Provider store={store}>
          <StartupContainer/>
        </Provider>,
        main
      )
    })
}
