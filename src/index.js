/* eslint-disable no-unused-vars */

import './publicPath'
import 'babel-polyfill'
import 'font-awesome/css/font-awesome.css'
import './styles/styles.scss'
import React from 'react'
import {render} from 'react-dom'
import start from './start'

const main = document.getElementById('main')

start(main, render)
