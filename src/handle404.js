/* eslint-disable react/no-unescaped-entities */
import React from 'react'
import {red500} from 'material-ui/styles/colors'

const container = {textAlign: 'center', padding: 100}
const icon = {color: red500, fontSize: '20px'}

const Handle404 = () => (
  <div style={container}>
    <h1 style={{fontSize: '40px'}}>Page Doesn't Exist!</h1>
    <br/>
    <br/>
    <h2 style={{fontSize: '20px'}}>Sorry, we can't find the page you are looking for.<br/>If you feel like this is a problem, please contact support.</h2>
  </div>
)

export default Handle404
