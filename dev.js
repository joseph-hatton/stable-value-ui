const express = require('express')
const devMiddleware = require('webpack-dev-middleware')
const hotMiddleware = require('webpack-hot-middleware')
const webpack = require('webpack')
const getDevServerUser = require('./getDevServerUser')

const config = Object.assign({}, require('./webpack.config.js'), {
  entry: [
    'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true',
    'babel-polyfill',
    './src/index.js'
  ],
  output: {
    path: '/',
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"development"',
        API_BASE: `"${process.env.API_BASE}"`
      }
    })
  ]
})

const compiler = webpack(config)

const createDevApi = () => {
  const router = express.Router()

  const services = {
    initiative: process.env.INITIATIVE_API_URL || 'https://stable-value-api-dev.rgare.net/api/v1'
  }

  router.get('/apps/pdm/paths/admin/services', (req, res) => res.json(services))

  return router
}

const createCurrentUserApi = () => {
  const router = express.Router()

  router.get('/', (req, res, next) =>
    getDevServerUser()
      .then(user => res.json(user))
      .catch(next)
  )
  // log out is a noop
  router.delete('/', (req, res) => res.json({logoutUrl: 'https://stsdev.rgare.com/secureauth31/Logout.aspx'}))

  return router
}

console.log('DEV mode')

module.exports = app => {
  app.use('/__auth/current-user', createCurrentUserApi())
  app.use('/api/v1', createDevApi())
  app.use('/images', express.static('images'))
  app.use(devMiddleware(compiler, {
    stats: {
      colors: true
    },
    noInfo: true
  }))
  app.use(hotMiddleware(compiler))
}
